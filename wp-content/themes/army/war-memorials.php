<?php

$main = "brave";

$page = "memorials";

include('header.php'); ?>


<section class="regiments-and-corps-banner" style="background-image: url(./assets/img/regiments-and-corps-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">War Memorials</h1>
    </div>
</section>

<section class="regiments-and-corps-detail">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9  col-sm-12">
                <h3>War Memorials</h3>
                <p>In the heart of New Delhi, the capital of India, stand two war memorials. The iconic India Gate was built in 1931 to ‘commemorate the Battle Casualty (Fatal) of India during World War I as well as Third Anglo-Afghan War. Out of over 83,000 Indians who laid down their lives, India Gate bears 13,516 names, etched all over the monument.’ The Amar Jawan Jyoti was built under the India Gate arch to commemorate India’s victory in the Indo-Pak War of 1971 and to pay tribute to India’s brave soldiers/sailors/airmen who laid down their lives in service of the nation. The Amar Jawan Jyoti features an inverted bayonet with a helmet structure on a black marble platform. This has now been moved to the National War Memorial.</p>
                <p>Post-independence, India has fought four major wars as well as seen many instances of conflict. Its armed forces have participated in a number of operations both within the borders of the country and beyond, ranging from peacekeeping missions to counter-insurgency and counter-terrorism as well as facing an ongoing proxy war. Over 26,000 personnel from the Indian Armed Forces have laid down their lives for the sovereignty and integrity of the nation.</p>


                <p>There were many local, ‘area/battle specific memorials’ across the country but the need was felt to have a war memorial in the capital to record for posterity the bravery and gallantry of those who have served the nation. While the demand for a National War Memorial has been around since 1961, the process got momentum in 2015 when the ‘Union Cabinet approved construction of a National War Memorial and Museum (NWM&M)…[t]aking into consideration the existing ceremonial practices at India Gate and AJJ, area East of the India Gate around the Canopy at “C” Hexagon in New Delhi was found to be the most suited site for the Memorial.’ The National War Memorial was inaugurated on 25 February 2019 by the Prime Minister.</p>



                <h3>War Memorials: A Brief History</h3>
                <p>The Saragarhi memorials at Amritsar and Ferozpur are some of the earliest modern memorials built in India to honour collective gallantry and sacrifice. These mark the last stand of a detachment of the 36 Sikhs, numbering 21 men that faced a 10,000–12,000 strong tribal lashkar in a battle at Saragarhi, a small outpost on the Samana range in September 1897. The soldiers fought to the last man and each was posthumously admitted to the IOM for gallantry.</p>


                <p>It was, however, World War I (1914–1918) that led to the marking of the sacrifice of individual soldiers of all ranks by universally according an official recognition in the form of a grave or memorial located in the theatre of war where the soldier had died. Of the many Hindu and Sikh soldiers who fought in the war, and whose remains were cremated and who had no known grave, their details were recorded on common collective memorials. Examples of such memorials are the India Gate at New Delhi and the memorial at Neuve Chapelle in France.</p>

                <p>It is the Commonwealth War Graves Commission (CWGC) in the United Kingdom (UK) which ‘has the responsibility for recording details, maintaining the memorials, and the numerous graves and cemeteries. The cost is shared by partner governments—Australia, Canada, India, New Zealand, South Africa and the UK— in proportions based on the numbers of their graves. All members of the forces who died in service or as a result of service within the two war periods designated by the participating governments—between 4 August 1914 and 31 August 1921, and 3 September 1939 to 31 December 1947, are considered as war dead and recorded as such.’</p>

                <p>India’s contribution to both World Wars was immense: over 1.4 million Indians served during the First World War; in the Second World War, between 1939 and 1945, the Indian Army grew from a strength of 1,95,000 in 1939 to over 2.5 million men by the end of the war, the largest volunteer force in the history of human conflict. ‘The 160,000 war dead of undivided India are buried and commemorated in over 60 countries. India’s massive volunteer army suffered by far the heaviest losses, but the Second World War also saw men of the Royal Indian Navy and the Royal Indian Air Force play a significant role, with many of their number also falling in the line of duty.’</p>

                <p>War memorials thus provide a focus for memory and for commemoration both nationally and internationally, as well as a space where friends and relatives can remember the sacrifice of their loved ones. Broadly speaking, three kinds of memorials/war graves exist in India and abroad:</p>


                <ol>
                    <li>War memorials thus provide a focus for memory and for commemoration both nationally and internationally, as well as a space where friends and relatives can remember the sacrifice of their loved ones. Broadly speaking, three kinds of memorials/war graves exist in India and abroad:</li>
                    <li>Memorials of post-Independence era constructed within military stations, cantonments and field areas with government sanction; as well as memorials to Indian soldiers/peacekeepers in other countries.</li>
                    <li>Memorials constructed by civic bodies and state governments within India.</li>
                </ol>

                <p>At the end of World War I, a large number of Memorial Tablets were installed by government in villages and towns across the country. Post-independence, a number of specific memorials were constructed and maintained across the country. Nationally, it was the India Gate that served as the premier war memorial until the inauguration of the National War Memorial in 2019.</p>


                <h2 style=" margin-top: 90px; margin-bottom: -28px;"><b>Pre-Independence Wars and Memorials</b></h2>

                <h3>World War I</h3>
                <p>World War I, also known as the ‘Great War’, was fought between 1914–1918. It was a seminal event in modern world history and changed the social and political map of the world forever. The repercussions of the war can be felt even today, with many current conflicts originating from the impact of the war. As Great Britain entered the war on 4 August 1914 in response to the German violation of the sovereignty of Belgium’, its colonies were also pulled into the war.</p>



                <p>At the time, the only regular forces available to Britain were its own army and the Indian Army. The Indian Army was not organised, trained or equipped for a major war of the type that was to take place, but it was the only reserve that was immediately available. Mobilisation commenced on 8 August 1914, just four days after the declaration of war by Great Britain. Within India, the National Movement was gathering pace and the Indians actively supported the war effort in a bid to gain Dominion status. ‘The overwhelming majority of mainstream political opinion in 1914 was united in the view that if India desired greater responsibility and political autonomy, it must also be willing to share in the burden of Imperial defence. As a result, India contributed immensely to the war effort in terms of both men and material.’</p>

                <p>‘Indian soldiers served in battlefields around the world: in France and Belgium, in Aden, Arabia, East Africa, Gallipoli, Egypt, Mesopotamia, Palestine, Persia, Salonica, Russia, and even in China. By the end of the war 1,100,000 Indians had served overseas at the cost of 60,000 dead. They earned over 9,200 decorations for gallantry including 11 Victoria Crosses. These figures include the contribution of over 26,000 Imperial Service troops who were a part of the Indian States Forces.’ Between 1914 and 1918, the Indian Army expanded sevenfold. Overall, around 1.2 million men were mobilised for the Indian Army and 0.3 million for non-combat duties.</p>

                <p>The Indian Army was divided into seven expeditionary forces and broadly deployed as stated below:</p>

                <ol>
                    <li>Indian Expeditionary Force (IEF) A: Northern Europe, in France and the Low Countries.</li>
                    <li>IEF B: East Africa.</li>
                    <li>IEF C: East Africa.</li>
                    <li>IEF D: Mesopotamia</li>
                    <li>IEF E: Egypt and Palestine</li>
                    <li>IEF F: Egypt</li>
                    <li>IEF G: Gallipoli</li>
                </ol>

                <p>The Indian soldiers were not trained for trench warfare, nor were the troops equipped for the European weather. The army, prior to the war, was designed and trained to police the frontiers of empire in the Indian Subcontinent and in other outposts of the British Empire. Still, they acquitted themselves admirably in the war.</p>

                <p>A number of war memorials commemorate the participation and sacrifice of Indian soldiers and officers in the Great War. Presented below is a brief description of some key memorials.</p>



                <h2 style=" margin-top: 90px; margin-bottom: -28px;"><b>Europe</b></h2>


                <h3>France</h3>
                <p>The Neuve Chapelle Memorial- The Neuve Chapelle Memorial was erected in France by the Imperial War Graves Commission (IWGC) on 7 October 1927 by then Secretary of State for India, Lord Birkenhead. It is in the form of a sanctuary enclosed within a circular wall. Carved Indian symbols as obtained in Indian shrines adorn the railings. In the middle of the railing is a column approximately 15 metres high similar to the columns erected by the Emperor Ashoka all over India. The memorial is located close to the village of Neuve Chapelle in an area that was central to the operations of the Indian Corps during 1914 and 1915. The memorial was designed by Sir Herbert Baker, and its cost shared between Britain, India and the rest of the Dominions.</p>

                <p>The rear of the memorial is a solid wall on which the names of the ‘Missing’ are inscribed including those who were cremated but for whom no grave stone or tablet exists. Where the railing meets the solid wall are two stone ‘chattris ’which are found in sacred burial places in northern and central India.</p>

                <p>In addition to the names of 4,742 of the Indian war dead who fell in France and Flanders are the names of two Indian VC winners—Lieutenant Bruce McCrae of the 59 Scinde Rifles and Rifleman Gabar Singh Negi of the 39 Garhwal Rifles who were killed in the battles of Givenchy and Neuve Chapelle respectively.</p>


                <p>Meerut Military Cemetery- The Meerut Military Cemetery is located towards the south of the French port of Boulogne and is linked to the fact that the Meerut Stationary Hospital was located here and it is here that over 300 Indian casualties are commemorated, including 287 from the 1914–15 period. This memorial also includes a memorial to 32 Indian Army officers and men who were cremated in the cemetery in 1915.</p>

                <p>Zelobes Indian Cemetery- The Zelobes Indian Cemetery is located at Lacouture, north west of Bethune, and was used by Field Ambulances from November 1914 to October 1915. It contains the graves of 73 Indian casualties including that of Subedar Hashmat Dad Khan, who was killed at Windy Corner, south of Festubert and is described as ‘a very fine Indian Officer remarkable for his fatalistic contempt for danger’. He was awarded the Indian Distinguished Service Medal (IDSM) for his courage.</p>

                <p>Mazargues Cemetery- The Mazargues Cemetery is just 3 miles from Marseilles, which was the base for the Indian Army during its stay on the Western Front and thereafter. It contains the graves of 127 Indian soldiers who died between 1914–1915, and many others who died subsequently. These casualties cover a range of regiments and corps fighting in the north and also large numbers from support units such as the Mule Corps, the Indian Veterinary Corps, the Indian Ordnance Department, the Army Bearer Corps, and the Indian Subordinate Medical Corps. The majority of casualties after the departure of the Indian Corps for Mesopotamia, Palestine and Egypt are from the Indian Labour Corps, of which there are 420 graves and memorials. In addition, are the graves of personnel of the Artillery and Cavalry units who were retained on the Western Front after the Indian Corps departed at the end of 1915. This cemetery commemorates a total of 994 Indian dead.</p>

                <p>Gorre British and Indian Cemetery- The Chateau at Gorre was only about two miles from the front in 1914–15 and was occupied during this time by the Indian Corps. The cemetery contains 80 Indian graves from the 1914–15 period.</p>

                <p>Ayette Indian and Chinese Cemetery- This cemetery is located in the village of Ayette in France. It remained in British hands from March 1916 to 27 March 1918 when it was captured by the Germans. The cemetery was begun in September 1917 and used till April 1918, and again in September and October 1918. Thirty five Indians are buried here.</p>

                <p>Etaples Military Cemetery- Seventeen Indian soldiers are buried in this cemetery at Etaples, in France which saw a number of Commonwealth reinforcement camps and hospitals during World War I.</p>

                <p>Merville Communal Cemetery- The headquarters of the Indian Corps, and the Meerut and Lahore Casualty Clearing Stations were located here in 1914–15. Ninety-seven Indians are buried here.</p>

                <p>Neuville-Sous-Montreuil Indian Cemetery- This Indian Cemetery was made between December 1914 and March 1916 by the Lahore Indian General Hospital. Twenty-five Indian soldiers are buried here.</p>




                <h3>Belgium</h3>
                <p>Menin Gate- The Menin Gate war memorial focuses on the soldiers who were listed as missing in the battles at France and Flanders and for whom no grave exists. It was unveiled on 27 July 1927. Menin was chosen for the memorial because it was through here that hundreds of thousands of soldiers passed through on their way to the battlefields of the Ypres Salient.</p>


                <p>The design of the memorial is on the lines of the classic Roman triumphal arch with its three central passageways. The exterior is of red brick and faced with Portland stone which is the material used for the headstones of all graves which are part of all IWGC cemeteries. The central passageway has a semi-circular arch. The central memorial hall, the stairwells and the galleries record the names of soldiers missing, believed killed, in the region before 15 August 1917. Those who died after this date are mentioned in the Tyne Cot Memorial or at the memorial at Ploegsteert. On the south side of the memorial to the missing is the Indian Forces Memorial which was opened in March 2011 and is dedicated to the 130,000 troops of the Indian Forces who served in France and Flanders during the Great War. The existing stone was replaced by the more imposing Ashoka Lions marker in November 2011.</p>


                <h3>Germany</h3>
                <p>Durnbach War Cemetery- The Durnbach War Cemetry contains the graves of 41 Indian soldiers who died as prisoners of war in various places in Germany.</p>

                <p>Zehrensdorf Indian Cemetery- This cemetery contains the graves of 206 Indian soldiers who died during the First World War at a prisoner-of-war camp at Zossen, south of Berlin.</p>



                <h3>Britain</h3>
                <p>The Chattri Memorial, Brighton- Around 12,000 wounded Indian soldiers passed through Brighton and Hove Hospital during the Great War. The Chattri Memorial was erected on the exact spot where 53 Hindu and Sikh soldiers were cremated according to their faith.</p>

                <p>Patcham Down Memorial- This memorial commemorates 53 Indian casualties of the First World War who died in the United Kingdom, many of them at the Indian Army hospital established at the Royal Pavilion in Brighton. These men were cremated in accordance with their faith and were initially commemorated on the Hollybrook and Neuve-Chappelle Memorials to the Missing. However, it was later decided that as these casualties were not ‘missing’ and therefore it would be better to commemorate them on a memorial at their cremation site. The memorial bears an inscription written in Hindi, Punjabi and English that reads: <b>‘IN HONOUR OF THESE SOLDIERS OF THE INDIAN ARMY WHOSE MORTAL REMAINS WERE COMMITTED TO FIRE’.</b></p>


                <h3>Malta</h3>
                <p>Pieta Military Cemetery- The Pieta Military Cemetery commemorates 28 Indian servicemen who died from their wounds in the hospitals of Malta and Gozo chiefly from the campaigns in Gallipoli and Salonika, 20 of whom were prisoners of war.</p>


                <h3>Romania</h3>
                <p>Slobozia Military Cemetery- The Slobozia Military Cemetery was made in 1920 by the Rumanian (Romania) Government for the reburial of those Indian soldiers, captured by the Germans and sent to work in Rumania and who later died in captivity.</p>


                <h3>Greece</h3>
                <p>Monastir Road Indian Cemetery- This cemetery was used from 1916 for the burial of those Indians who served in Salonika and commemorates the remains of Indian servicemen who were cremated in accordance with their faith.</p>




                <h2 style=" margin-top: 90px; margin-bottom: -28px;"><b>Africa</b></h2>
                <h3>Kenya</h3>
                <p>Maktau Indian Cemetery- Maktau was a fortified camp, reinforcement depot, and an Indian clearing hospital during World War I and was established in 1915. The cemetery was used from March 1915 to May 1916.</p>

                <p>Nairobi British and Indian Memorial- In 1914, Indian Expeditionary Force ‘B’ arrived at Mombasa to take part in operations against German East Africa. The memorial commemorates many of those who died before the advance to Rufiji in January 1917 and who have no known grave. The memorial commemorates 1,151 Indian soldiers who fell in this campaign.</p>





                <h2 style=" margin-top: 90px; margin-bottom: -28px;"><b>Middle East</b></h2>
                <h3>Egypt</h3>
                <p>Abbasiya Indian Cemetery- Cairo was the headquarters of the British Garrison in Egypt at the start of World War I. It subsequently became the main base and hospital centre for operations in Gallipoli, Egypt and Palestine. The cemetery at El Abbasiya was established in May 1918 and used till December 1920. Seventy-five Indian soldiers are buried here.</p>

                <p>Port Tewfik Memorial- The original Port Tewfik Memorial stood at the southern boundary of the Suez Canal. The canal itself was perhaps even more important during the two World Wars than it is today—linking the Mediterranean with the Red Sea and beyond, connecting Great Britain with its colonies in East Africa, Asia and the Pacific.</p>

                <p>During the First World War, it was a prime target for the Ottoman Empire, who launched several attacks towards the canal in 1915. These attacks were suc cessfully repulsed, and the canal remained in British hands. British forces, including Australian, Indian and New Zealand units, would go on the offensive in late 1916 and by 1918 had captured large swaths of Ottoman territory in the Eastern Mediterranean.</p>

                <p>Indian troops played a large part in the war in Egypt and the Eastern Mediterranean. In the 1920s, the Port Tewfik Memorial was built to commemorate the Indian servicemen who lost their lives in this theatre of war and who have no known grave. Unfortunately, the memorial suffered severe damage during the Israeli-Egyptian conflict of 1967–73, and was eventually demolished. A replacement memorial was later erected at Heliopolis War Cemetery and commemorates more than 3,700 Indian servicemen of the First World War.</p>



                <h3>Israel</h3>
                <p>Haifa War Memorial- The Haifa War Memorial commemorates those who fell in the cavalry charge by the Jodhpur Lancers of the 15 Imperial Service Cavalry Brigade on 23 September 1918 on fortified infantry defences at Haifa. A commemorative service is now held annually at the Memorial attended by members of the diplomatic corps and representatives of the Indian Armed Forces.</p>

                <p>Jerusalem Indian War Cemetery- This cemetery was used from July 1918 to June 1920 and contains for Indian servicemen killed during the Palestine campaign of World War I.</p>



                <h3>Lebanon</h3>
                <p>Beirut Maronite Cemetery- Beirut was occupied by the 7th (Meerut) Indian Division on 8th October 1918 and the 32nd and 15th Clearing Hospitals were sent to this town. The majority of those buried in this cemetery died in October 1918.</p>



                <h3>Turkey</h3>
                <p>Helles Memorial, Gallipoli- This memorial commemorates the death of 20,834 killed at Gallipoli, 1,516 of who were Indian. The height of the memorial is the same as that of the Colossus of Rhodes so that the monument of the missing dead would be visible by the ships sailing in those seas.</p>



                <h2 style=" margin-top: 90px; margin-bottom: -28px;"><b>India</b></h2>
                <h3>India Gate</h3>
                <p>India Gate is a memorial that is a landmark in New Delhi, India. This memorial commemorates 12,357 Indian soldiers whose names are inscribed on the bricks of the walls of this edifice. The inscription at the top of the arch reads as given below:</p>



                <p>To the dead of the Indian Armies who fell and are honoured in France and Flanders, Mesopotamia and Persia, East Africa, Gallipoli and elsewhere in the Near and Far East and in Sacred Memory also of those whose names are here recorded and who fell in India and on the North West Frontier and during the Third Afghan War.</p>


                <h3>Teen Murti Memorial</h3>
                <p>The Teen Murti memorial stands close to India Gate and is a part of the landscape conceived by Edward Lutyens. ‘Teen Murti’ is the local name given to this memorial and literally means ‘Three Statues’. The memorial commemorates the 15th Imperial Service Cavalry Brigade that fought in the Middle East during the First World War. It also serves as the Indian Cavalry Memorial and a memorial service is held here every year by the Indian Cavalry Association on Cavalry Day. The names of the cavalrymen who fell in the Great War are inscribed on the memorial.</p>


                <h3>Kapurthala State Forces War Memorial</h3>
                <p>This memorial commemorates the war dead of the Kapurthala Imperial Service Infantry who served in East Africa between 1914 and 1917, and also in the Third Afghan War. This monument is also known as the Captain Jhaggar Singh Memorial.</p>



                <h3>Kolkata 49th Bengalis Memorial</h3>
                <p>This memorial pays tribute to the 49th Bengalis—a remarkable unit which happens to be the only army unit to be composed entirely of Bengalis. The unit was raised in 1917 and was deployed on active service in Mesopotamia.</p>


                <h3>Kolkata Lascar Memorial</h3>
                <p>This memorial commemorates the Indian Lascars who replaced British sailors, and made up to 20 per cent of the British maritime force by the end of the war. Their contribution to the war is also recorded in the Tower Hill Memorial, UK which honours allied merchant seamen who lost their lives in both World Wars.</p>


                <h3>Chennai Victory Memorial</h3>
                <p>This memorial is located along the Sea Beach Road locally known as the ‘Marina Beach’, which faces the Bay of Bengal and is close to Fort St George. It was con- structed by prominent citizens of Madras to commemorate the victory of the allied armies during World War I.</p>


                <h3>Kirkee 1914–18 Memorial</h3>
                <p>This memorial commemorates 1,805 casualties of the First World War. It consists of a series of walls on which are inscribed the names of those who fell during the Great War, with the dedicatory inscription in Hindi.</p>


                <h3>Patiala State Forces Memorial</h3>
                <p>The memorial commemorates the dead of the military units of the erstwhile princely state of Patiala during the First World War (1914–18).</p>

                <p>In addition to the memorials mentioned above, memorial plaques and tablets to commemorate those who fell in the First World War have been installed in towns and villages across India. They record the number of men who went to war and the number who did not return. Regimental memorials also occupy pride of place in Regiments and battalions of the Indian Army. They are sacred symbols and figure prominently in units and figure prominently in the ceremonies that celebrate battle honour days.</p>


                <h3>World War II</h3>
                <p>The First World War ended with the armistice on 11 November 1918. During the course of the war, over 9 million soldiers were killed and another 15 million wounded. ‘Four great empires had fallen to pieces: Russia…; Germany…; Austria-Hungary…’ and the Ottoman Empire…. The old international order had gone forever.’ The Treaty of Versailles, signed on 28 June 1919, which formally ended the state of war between Germany and the Allied Powers. It ‘held Germany responsible for the war’ and imposed upon it heavy repatriations to the Allies. This led to resentment by the German people and created conditions that led to the rise of Adolf Hitler who took over as Chancellor of Germany in 1933 and set about rapidly [militarising]…. ‘On 13 March 1938 he annexed Austria and occupied Czechoslovakia. The British and the French, fearful of another world war, took no action but realized that German military expansion had to be stopped.’ On 1 September 1939, Germany attacked Poland; on 3 September, Great Britain declared war on Germany owing to its treaty obligations with Poland, and the Second World War began. The war would last for six years and be the most destructive conflict of its time.</p>

                <p>At the start of World War II in 1939, the Indian Army had a strength of 194,000 personnel, with 96 infantry battalions and 18 cavalry regiments. ‘Modernisation had yet to start. The cavalry had no tanks; the infantry had no mortars as anti-tank weapons. Wireless sets were available only at brigade headquarters and above…. By the time the war ended, the Indian Army had expanded to over 2.5 million men and had fought on both the Western and Eastern fronts. These figures include units of the Indian State Forces. Indian soldiers were deployed in Persia, Iraq, East Africa, North Africa, in Sicily, Italy and France and in Burma, South East Asia and Indonesia. The Indian Army of the Second World War was the largest volunteer army in the history of human conflict.’ An interesting bit of trivia about the war is that ‘four Indian Animal Transport Companies that supported the British divisions in France took part in the retreat at Dunkirk, impressing all with their discipline in difficult circumstances.’</p>

                <p>Indian troops saw action in almost all theatres of the war: in Europe, North Africa, in Burma and South-east Asia. ‘In the Second World War [the Indian Army] had fought against two of the finest armies of the world—the Germans and the Japanese—and had proved its worth. Towards the end of the war it was led by its own officers at unit and sub-unit level…. The myth of the martial classes was also finally broken. The rapid wartime expansion of the Indian Army from a strength of 2,00,000 to 20,00,000 forced recruitment from all classes. The officer cadre expanded from 1000 to 15,740.’ Thus by the end of World War II the Indian Army was truly representative of all areas and classes of the country. The Indian Army earned ‘[n]early 6,300 awards… including 31 Victoria Crosses, 4 George Crosses, 252 Distinguished Service Orders, 347 Indian Orders of Merit and 1,311 Military Crosses.’</p>

                <p>According to The Last Post, published as a commemorative volume during the centenary celebrations of the First World War, over ‘62,000 Commonwealth war dead of the two world wars are commemorated by the CWGC in India. During the First World War, many soldiers died on the North West Frontier or in garrison outposts, and, as it was not possible to maintain all the civil, cantonment and outpost cemeteries in which many of them were buried, their names are recorded on memorials in the war cemeteries at Delhi, Madras and Kirkee, and on the Memorial Arch in New Delhi (today known as the India Gate). During the Second World War, cemeteries for hospitals and lines of communication were established at Ranchi, Kirkee, Madras, Digboi and Guwahati, and for the battlefields at Imphal and Kohima. Delhi War Cemetery was established after independence to accommodate wartime graves from cantonments in Northern, Western and Central India.’</p>




                <h2>World War II Memorials Commemorating Indian Soldiers</h2>
                <h3>Rangoon Memorial</h3>
                <p>Located at the heart of Taukkyan War Cemetery, the Rangoon Memorial bears the names of more than 26,000 World War II servicemen—almost 20,000 of them served with the Indian forces. The Taukyan Cremation Memorial commemorates a further 1,000 World War II casualties whose remains were cremated in accordance with their faith.</p>

                <p>The majority of these men were casualties of the campaign in Burma (now Myanmar). In 1942, in the face of the invading Japanese forces, the British Burma Army retreated from Burma into India. The country would remain under Japanese control until the winter of 1944-45, when a Commonwealth force returned to liber- ate the country from the Japanese.</p>

                <p>A fierce campaign followed, culminating in a race to Rangoon, where Commonwealth troops hurried to secure the vital port before monsoon season set in. Within the week of the recapture of Rangoon, Germany formally surrendered to the Allies—and with Japanese influence in Burma all but eradicated, the end of the war was in sight.</p>

                <h3>Alamein Memorial</h3>
                <p>The two battles of El Alamein arguably feature as one of the key turning points of the Second World War. Not only did they mark one of the first major Allied successes of the war, but they were the first step towards eventual victory in the North African campaign, which in turn was vital for the securing control of the Mediterranean. At times during the conflict the desert was as much an enemy as the opposing troops. Swelting days and freezing nights in a vast bleak desert, which meant supply lines could be precariously long, and populated by biting flies.</p>

                <p>Of the more than 11,000 casualties commemorated on the Alamein Memorial, over 1,800 of them served with Indian forces, who fought in campaigns across North Africa and the Eastern Mediterranean. A further 600 Indian casualties are listed on the Alamein Cremation Memorial as their remains were cremated during the war, in accordance with their faith.</p>

                <h3>Singapore Memorial </h3>
                <p>Japanese forces launched a devastating surprise attack against British Malaya (Malaysia) in December 1942. British and Commonwealth forces where quickly driven back down the peninsular and by February had retreated across the straits to Singapore. Japanese forces besieged the island on 15 February 1943. Singapore, the key stone of British imperial defence in southeast Asia, fell and some 80,000 Commonwealth troops became prisoners of war.</p>

                <p>The Singapore Memorial commemorates more than 24,000 Commonwealth casualties, more than 12,000 of whom are Indian. Some of the those commemorated here died during the defence of Malaya and Singapore, but most died while in captivity as a result of brutal treatment by the Japanese. The Singapore Cremation Memorial commemorates almost 800 casualties, mostly Indian forces, who were cremated in accordance with their religious beliefs.</p>






                <h2>Post-Independence War Memorials</h2>
                <p>As already noted, post-independence, India has fought four major wars with its neighbours, China and Pakistan. In addition, The Indian Armed Forces have participated in a number of key operations both domestically and abroad, including in UN Peacekeeping missions. Counter-insurgency and internal security are some of the other key tasks that have been placed upon the armed forces, in particular the Indian Army.</p>

                <p>Until the inauguration of the National War Memorial in 2019, India Gate and the Amar Jawan Jyoti served as a space of national memorial to honour the sacrifices of the brave Indian men and women who gave their lives in service of the nation. Additionally, there are a number of smaller memorials across the country, many of which are located within cantonments, which commemorate the gallantry and bravery of the Indian Armed Forces. Table 1 is an exhaustive list of such memorials, state-wise.</p>




                <!-- <div class="box-table">
                        <h3>Table 1 Composition of Armoured Regiments</h3>
                        <table class="table regimental-tb">
                            <thead>
                                <tr>
                                    <th scope="col">S.No</th>
                                    <th scope="col">State</th>
                                    <th scope="col">Location serial No</th>
                                    <th scope="col">Location</th>
                                    <th scope="col">Name of War Memorial</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td><b>Andhra Pradesh</b></td>
                                    <td>1</td>
                                    <td>Dundigal, Hyderabad (Air Force Academy)</td>
                                    <td>Air Force Academy Flight Crew Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Hyderabad (Mehdipatnam)</td>
                                    <td>Basantar War Memorial </td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Secunderabad Cantonment (1 EME Centre)</td>
                                    <td>1 EME Centre War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">4</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Secunderabad (AOC Centre)</td>
                                    <td>AOC Centre War Memorial </td>
                                </tr>
                                <tr>
                                    <th scope="row">5</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Secunderabad (Army Training Ground)</td>
                                    <td>Veerula Sainika Smarak</td>
                                </tr>
                                <tr>
                                    <th scope="row">6</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Secunderabad (Bowenpally)</td>
                                    <td>Mighty Bombardiers War Memorial </td>
                                </tr>
                                <tr>
                                    <th scope="row">7</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Visakhapatnam (Ramakrishna Beach)</td>
                                    <td>Victory at Sea War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">8</th>
                                    <td><b> Arunachal Pradesh</b> </td>
                                    <td>1</td>
                                    <td> 18 Km from Walong</td>
                                    <td>Helmet Top War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">9</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Manmao Post</td>
                                    <td>Manmao War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">10</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Nuranang (25 km from Tawang)</td>
                                    <td>Jaswantgarh War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">11</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Tawang</td>
                                    <td>Tawang War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">12</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Walong</td>
                                    <td>Walong War Memorial </td>
                                </tr>
                                <tr>
                                    <th scope="row">13</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Walong</td>
                                    <td>Hut of Remembrance</td>
                                </tr>
                                <tr>
                                    <th scope="row">14</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Walong</td>
                                    <td>Air Force Memorial Walong</td>
                                </tr>


                                <tr>
                                    <th scope="row">15</th>
                                    <td><b>Assam</b></td>
                                    <td>1</td>
                                    <td>Dibrugarh</td>
                                    <td>DAH War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">16</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Guwahati (Air Force Station)</td>
                                    <td>Air Force Station Guwahati War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">17</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Mohanbari (Air Force Station)</td>
                                    <td>Air Force Station Mohanbari War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">18</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Silchar</td>
                                    <td>Amar Jawan War Memorial </td>
                                </tr>
                                <tr>
                                    <th scope="row">19</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Tezpur (Air Force Station)</td>
                                    <td>Air Force Station Tezpur War Memorial </td>
                                </tr>
                                <tr>
                                    <th scope="row">20</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Tezpur (HQ 4 Corps)</td>
                                    <td>4 Corps War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">21</th>
                                    <td><b>Bihar</b></td>
                                    <td>1</td>
                                    <td> Danapur (Bihar Regimental Centre)</td>
                                    <td>Bihar Regiment War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">22</th>
                                    <td><b>Chandigarh</b></td>
                                    <td>1</td>
                                    <td>Chandigarh (Air Force Station, near Air Traffic Control tower)</td>
                                    <td>Air Force Station Chandigarh Yudh Smriti</td>
                                </tr>
                                <tr>
                                    <th scope="row">23</th>
                                    <td><b>Daman & Diu</b></td>
                                    <td>1</td>
                                    <td>Diu (Anti-Submarine Warfare School)</td>
                                    <td>INS Khukri Memoria</td>
                                </tr>

                                <tr>
                                    <th scope="row">24</th>
                                    <td><b>Delhi</b></td>
                                    <td>1</td>
                                    <td>New Delhi (Teen Murti Road)</td>
                                    <td>Teen Murti Memoria</td>
                                   
                                </tr>
                                <tr>
                                    <th scope="row">25</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>New Delhi (Rajpath)</td>
                                     <td>India Gate</td>
                                </tr>
                                <tr>
                                    <th scope="row">26</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>New Delhi (Rajputana Rifles Regimental Centre)</td>
                                    <td>Rajputana Rifles War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">27</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>New Delhi (Near Air Force Museum Palam)</td>
                                    <td>Air Force Station Palam War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">28</th>
                                    <td><b>Goa</b></td>
                                    <td>1</td>
                                    <td>Anjadiv Island, Karwar</td>
                                    <td>Anjadiv Island War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">29</th>
                                    <td><b>Gujarat</b></td>
                                    <td>1</td>
                                    <td>Ahmedabad Cantonment</td>
                                    <td>Golden Katar War Memoria</td>
                                </tr>
                                <tr>
                                    <th scope="row">30</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Ahmedabad (Shahibaugh, near State Guest House)</td>
                                    <td>Ahmedabad War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">31</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Bhuj (Rajaram Park, Military Station Bhuj)</td>
                                    <td>Bhuj War Memorial</td>

                                </tr>
                                <tr>
                                    <th scope="row">32</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Gandhinagar (Chiloda Military Station)</td>
                                    <td>Parbat Ali War Memorial</td>

                                </tr>
                                <tr>
                                    <th scope="row">33</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Jamnagar (Air Force Station)</td>
                                    <td>Vijay Stumbh</td>

                                </tr>
                                <tr>
                                    <th scope="row">34</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Jamnagar (Rozi Island)</td>
                                    <td>INS Valsura War Memorial</td>

                                </tr>
                                <tr>
                                    <th scope="row">35</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Jamnagar (Air Force Station</td>
                                    <td>Air Force Station Jamnagar War Memorial</td>

                                </tr>
                                <tr>
                                    <th scope="row">36</th>
                                    <td><b>Haryana<b></td>
                                    <td>1</td>
                                    <td>Ambala (Air Force Station Ambala)</td>
                                    <td>Air Force Station Ambala ‘Frozen Tear’ Memorial</td>

                                </tr>
                                <tr>
                                    <th scope="row">37</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Chandimandir Cantt</td>
                                    <td>Veer Smriti Western Command War Memorial</td>

                                </tr>
                                <tr>
                                    <th scope="row">38</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Gurgaon (John Hall)</td>
                                    <td>Gurgaon Martyr’s Memorial</td>

                                </tr>
                                <tr>
                                    <th scope="row">39</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Hissar (Inside MD University Campus) </td>
                                    <td>Amar Jawan War Memorial</td>

                                </tr>
                                <tr>
                                    <th scope="row">40</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Jhajjar (At Sainik Rest House) </td>
                                    <td>Jhajjar War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">41</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Jind (Opposite District Court on Gohana Road) </td>
                                    <td>Jind War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">42</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Narwana (Navdeep Stadium) </td>
                                    <td>Narwana War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">43</th>
                                    <td></td>
                                    <td>8</td>
                                    <td>Bahadurgarh (On Rohtak road)</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">44</th>
                                    <td></td>
                                    <td>9</td>
                                    <td>Rohtak (Inside ManasaSarowar Park) </td>
                                    <td>Yudh Shahid Samarak</td>
                                </tr>
                                <tr>
                                    <th scope="row">45</th>
                                    <td><b>Himachal Pradesh</b></td>
                                    <td>1</td>
                                    <td>Dharmasala</td>
                                    <td>Dharamsala Martyrs Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">46</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Jutogh Cantonment, Shimla (Right of gate No 1 bisecting National Highway at Toute)</td>
                                    <td>3rd Mountain Artillery Brigade War Memorial (WW-1)</td>
                                </tr>

                                <tr>
                                    <th scope="row">47</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Subathu (14 GTC)</td>
                                    <td>3rd Mountain Artillery Brigade War Memorial (WW-1)</td>
                                </tr>

                                <tr>
                                    <th scope="row">48</th>
                                    <td><b>Jammu & Kashmir</b></td>
                                    <td>1</td>
                                    <td>Baramulla</td>
                                    <td>1 Sikh War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">49</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Baramulla</td>
                                    <td>Dagger Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">50</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>BB Cantt, Srinagar</td>
                                    <td>Op Rakshak Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">51</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Jammu (Air Force Station) </td>
                                    <td>Condors War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">52</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Jammu (Bahu Wali Rakh) </td>
                                    <td>Balidan Stambh</td>
                                </tr>

                                <tr>
                                    <th scope="row">53</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Jammu (Tiger Park opposite St Mary’s Public School) </td>
                                    <td>Tiger War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">54</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Rezang La, Ladakh</td>
                                    <td>Rezang La War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">55</th>
                                    <td></td>
                                    <td>8</td>
                                    <td>Leh</td>
                                    <td>Leh (1962, 1965, 1971) War Memorial </td>
                                </tr>

                                <tr>
                                    <th scope="row">56</th>
                                    <td></td>
                                    <td>9</td>
                                    <td>Leh</td>
                                    <td>Leh (1947–48) War Memorial </td>
                                </tr>

                                <tr>
                                    <th scope="row">57</th>
                                    <td></td>
                                    <td>10</td>
                                    <td>Machhal</td>
                                    <td>Sahi Memorial Hospital</td>
                                </tr>

                                <tr>
                                    <th scope="row">58</th>
                                    <td></td>
                                    <td>11</td>
                                    <td>Poonch (In front of 171 MH link road from NH-1 to Village Sangwali Mandi) </td> 
                                    <td>Lok Bahadur Stadium Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">59</th>
                                    <td></td>
                                    <td>12</td>
                                    <td>Rajouri</td>
                                    <td>Hall of Fame</td>
                                </tr>

                                <tr>
                                    <th scope="row">60</th>
                                    <td></td>
                                    <td>13</td>
                                    <td>Siachen, Ladakh (Siachen Base Camp)</td>
                                    <td>Siachen War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">61</th>
                                    <td></td>
                                    <td>14</td>
                                    <td>Srinagar (Air Force Station) </td>
                                    <td>Air Force Station Srinagar WarMemorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">62</th>
                                    <td></td>
                                    <td>15</td>
                                    <td>Srinagar (HQ 15 Corps) </td>
                                    <td>15 Corps War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">63</th>
                                    <td></td>
                                    <td>16</td>
                                    <td>Udhampur</td>
                                    <td>Dhruva Shaheed Smarak</td>
                                </tr>

                                <tr>
                                    <th scope="row">64</th>
                                    <td></td>
                                    <td>17</td>
                                    <td>Chushul, Ladakh</td>
                                    <td>Chushul War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">65</th>
                                    <td></td>
                                    <td>18</td>
                                    <td>Dras, Kargil (Bimbat)</td>
                                    <td>Bimbat 8 Div Kargil War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">66</th>
                                    <td></td>
                                    <td>19</td>
                                    <td>Samba (On link road from NH-1A to Village Katli) </td>
                                    <td>3 Madras Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">67</th>
                                    <td></td>
                                    <td>20</td>
                                    <td>Samba (Cremation ground)</td>
                                    <td>2/Lt Arun Khetrapal PVC War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">68</th>
                                    <td></td>
                                    <td>21</td>
                                    <td>Samba (On NH-1, approx seven kms on Samba road towards Pathankot) </td>
                                    <td>Major RS Rajwat Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">69</th>
                                    <td></td>
                                    <td>22</td>
                                    <td>Satwari Cantonment (Manekshaw Marg, opp Tiger CSD Canteen)</td>
                                    <td>Jammu and Kashmir State Forces War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">70</th>
                                    <td><b>Jharkhand</b></td>
                                    <td>1</td>
                                    <td>Ramgarh Cantonment (Sikh Regimental Centre)</td>
                                    <td>Saragarhi War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">71</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Ramgarh Cantonment (Sikh Regimental Centre)</td>
                                    <td>Sikh Regiment War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">72</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Ranchi (Punjab Regimental Centre)</td>
                                    <td>Punjab Regiment War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">73</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Ranchi (Depatoli Cantonment) </td>
                                    <td>Jharkhand War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">74</th>
                                    <td><b>Karnataka</b></td>
                                    <td>1</td>
                                    <td>Bangalore (Along Central Parade Ground, MEG & Centre) </td>
                                    <td>Madras Sappers War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">75</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Bangalore (ASC Centre and College) </td>
                                    <td>Army Service Corps War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">76</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Bangalore (ASC Centre and College) </td>
                                    <td>Army Transport Animals Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">77</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Bangalore (CMP Centre & School) </td>
                                    <td>Corps of Military Police War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">78</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Bangalore (End of Brigade Road, near junction between Brigade & Residency Roads) </td>
                                    <td>Pioneer Corps War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">79</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Bangalore (HQ Training Command) </td>
                                    <td>IAF Training Command Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">80</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Bangalore (MEG & Centre) </td>
                                    <td>British War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">81</th>
                                    <td></td>
                                    <td>8</td>
                                    <td>Bangalore (Parachute Regiment Training Centre) </td>
                                    <td>Parachute Regiment War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">82</th>
                                    <td></td>
                                    <td>9</td>
                                    <td>Belgaum (Maratha LI Regimental Centre) </td>
                                    <td>Maratha Light Infantry War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">83</th>
                                    <td></td>
                                    <td>10</td>
                                    <td>Bidar (Air Force Station) </td>
                                    <td>Air Force Station Bidar Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">84</th>
                                    <td><b>Kerala</b></td>
                                    <td>1</td>
                                    <td>Cannanore District (DSC Centre)</td>
                                    <td>Defence Security Corps Gaurav Sthal</td>
                                </tr>

                                <tr>
                                    <th scope="row">85</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Ezhimala (Indian Naval Academy) </td>
                                    <td>Indian Naval Academy Ezhimala War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">86</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Kochi (Naval base Kochi) </td>
                                    <td>INS Venduruthy War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">87</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Thiruvananthapuram (Pangode: on Vazhuthakad- Thirumala Main Road opposite 91 InfBde)</td>
                                    <td>The Bogra Memorial Hut</td>
                                </tr>

                                <tr>
                                    <th scope="row">88</th>
                                    <td><b>Madhya Pradesh</b></td>
                                    <td>1</td>
                                    <td>Bhopal (3 EME Centre) </td>
                                    <td>No. 3 EME Centre War memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">89</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Saugor</td>
                                    <td>Shahbaaz War Memorial</td>
                                </tr>

                                 <tr>
                                    <th scope="row">90</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Bhopal (Corps HQ Complex) </td>
                                    <td>Sudarshan Chakra War Memorial</td>
                                </tr>

                                 <tr>
                                    <th scope="row">91</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Jabalpur (1 Military Training Regiment, 1 Signal Training Centre) </td>
                                    <td>Corps of Signals War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">92</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Jabalpur (Grenadiers Regimental Centre) </td>
                                    <td>The Grenadiers War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">93</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Jabalpur (Jammu and Kashmir Rifles Regimental Centre) </td>
                                    <td>Jammu and Kashmir Rifles War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">94</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Jabalpur (CMM School)</td>
                                    <td>KanglaTongbi War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">95</th>
                                    <td></td>
                                    <td>8</td>
                                    <td>Mhow</td>
                                    <td>Infantry Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">96</th>
                                    <td><b>Maharashtra</b></td>
                                    <td>1</td>
                                    <td>Ahmednagar (Armoured Corps Centre and School </td>
                                    <td>Armoured Corps War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">97</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Southern Command AOR (Ranjit Ground in front of HQ 94 Armed Brigade)</td>
                                    <td>45 CAV War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">98</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Ahmednagar (Mechanised Infantry Regimental Centre)</td>
                                    <td>Mechanised Infantry Regiment War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">99</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Kamptee (Mahar Regimental Centre) </td>
                                    <td>The Mahar Regiment War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">100</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Khadakwasla, Pune (NDA)</td>
                                    <td>Hut of Remembrance</td>
                                </tr>


                                <tr>
                                <th scope="row">101</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Kirkee (Training Battalion 1 Area, BEG & Centre) </td>
                                    <td>Bombay Pioneers Memorial (WW-1)</td>
                                </tr>

                                <tr>
                                <th scope="row">102</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Kirkee (Entrance to Parade Ground, BEG & Centre) </td>
                                    <td>Bombay Sappers War Memorial</td>
                                </tr>

                                <tr>
                                <th scope="row">103</th>
                                    <td></td>
                                    <td>8</td>
                                    <td>Lonavla District, Pune</td>
                                    <td>INS Shivaji War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">104</th>
                                    <td></td>
                                    <td>9</td>
                                    <td>Mumbai (Indian Navy Sailors’ Institute ‘Sagar’, 30 Wodehouse Road) </td>
                                    <td>Naval Uprising Memorial</td>
                                </tr>


                                <tr>
                                    <th scope="row">105</th>
                                    <td></td>
                                    <td>10</td>
                                    <td>Mumbai (Military Station, facing Colaba Navynagar Road, behind Afghan Church, Colaba) </td>
                                    <td>Subedar Joginder Singh PVC Memorial</td>
                                </tr>


                                <tr>
                                    <th scope="row">106</th>
                                    <td></td>
                                    <td>11</td>
                                    <td>Nasik Road Camp (Artillery Centre)</td>
                                    <td>Artillery Centre War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">107</th>
                                    <td></td>
                                    <td>12</td>
                                    <td>Pune Cantonment (Morwada Junction, MahadjiShinde Road)</td>
                                    <td>Morwada War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">108</th>
                                    <td><b>Manipur</b></td>
                                    <td>1</td>
                                    <td>Imphal (Khenjang Village) </td>
                                    <td>Kangla Tongbi War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">109</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Leimakhong</td>
                                    <td>Shantivan War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">110</th>
                                    <td><b>Meghalaya</b></td>
                                    <td>1</td>
                                    <td>Shillong (58 Gorkha Training Centre) </td>
                                    <td>58 GTC War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">111</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Shillong (Happy Valley, Assam Regimental Centre) </td>
                                    <td>Assam Regiment War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">112</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Shillong</td>
                                    <td>1971 War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">113</th>
                                    <td><b>Nagaland</b></td>
                                    <td>1</td>
                                    <td>Zakhama Military Station</td>
                                    <td>Orchid Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">114</th>
                                    <td><b>Punjab</b></td>
                                    <td>1</td>
                                    <td>Adampur, (Air Force Station) </td>
                                    <td>Air Force Station Adampur Vayu Shakti Sthal</td>
                                </tr>

                                <tr>
                                    <th scope="row">115</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Amritsar (Military Station Khasa) </td>
                                    <td>Dograi War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">116</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Amritsar (Near Rattoke Gurudwara) </td>
                                    <td>5 Gorkha Rifles War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">117</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Amritsar (Pulkanjri, Dhanaya Kalan) </td>
                                    <td>Pulkanjri War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">118</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Asal Uttar (Tarn Taran Sahib) </td>
                                    <td>CQMH Abdul Hamid PVC Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">119</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Asal Uttar (Tarn Taran Sahib, on road Khemkaran-Valtoha) </td>
                                    <td>7 Grenadiers War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">120</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Bathinda (Air Force Station Bhisiana) </td>
                                    <td>Sqn Ldr Ajay Ahuja Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">121</th>
                                    <td></td>
                                    <td>8</td>
                                    <td>Bhura Kuhna, Tarn Taran District</td>
                                    <td>2/Lt JP Gaur Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">122</th>
                                    <td></td>
                                    <td>9</td>
                                    <td>Bhura Kuhna, Tarn Taran District</td>
                                    <td>2 Madras Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">123</th>
                                    <td></td>
                                    <td>10</td>
                                    <td>Bhura Kuhna, (On Road Bhikhiwind Khemkaran near village Bhura Kuhna) </td>
                                    <td>Sapper Harak Singh Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">124</th>
                                    <td></td>
                                    <td>11</td>
                                    <td>Ferozepur Cantt</td>
                                    <td>Sehjra 1971 Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">125</th>
                                    <td></td>
                                    <td>12</td>
                                    <td>Ferozepur</td>
                                    <td>Barki 1965 Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">126</th>
                                    <td></td>
                                    <td>13</td>
                                    <td>Ferozepur</td>
                                    <td>Saragarhi Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">127</th>
                                    <td></td>
                                    <td>14</td>
                                    <td>Ferozepur (Golden Arrow House crossing) </td>
                                    <td>Satluj Campaign War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">128</th>
                                    <td></td>
                                    <td>15</td>
                                    <td>Ferozepur (Mehdipur Village)</td>
                                    <td>6 Mahar War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">129</th>
                                    <td></td>
                                    <td>16</td>
                                    <td>Ferozepur (Golden Arrow House crossing)     </td>
                                    <td>VI K.E.O. Cavalry War Memorial</td>
                                </tr>
                                <tr></tr>
                                    <th scope="row">130</th>
                                    <td></td>
                                    <td>17</td>
                                    <td>Ferozepur (Golden Arrow House crossing)  </td>
                                    <td>VI K.E.O. Cavalry War Memorial</td>
                                <tr>
                                    <th scope="row">131</th>
                                    <td></td>
                                    <td>18</td>
                                    <td>Ferozepur (Golden Arrow House crossing) </td>
                                    <td>Brownlow’s Punjabis War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">132</th>
                                    <td></td>
                                    <td>19</td>
                                    <td>Ferozepur (Near Golden Arrow House crossing)  </td>
                                    <td>19, 22 & 24 Punjabis War Memorial 1914–1919</td>
                                </tr>
                                <tr>
                                    <th scope="row">133</th>
                                    <td></td>
                                    <td>20</td>
                                    <td>Ferozepur (Near Mehdipur Village close to Pir Baba mazar) </td>
                                    <td>Sqn Ldr Raman Uppal Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">134</th>
                                    <td></td>
                                    <td>21</td>
                                    <td>Ferozepur (Cantonment General Hospital Family Wing) </td>
                                    <td>Gallipoli Memorial tablet</td>
                                </tr>
                                <tr>
                                <th scope="row">135</th>
                                    <td></td>
                                    <td>22</td>
                                    <td>Gurdaspur (Batala)</td>
                                    <td>Batala War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">136</th>
                                    <td></td>
                                    <td>23</td>
                                    <td>Gurdaspur (Dera Baba Nanak) </td>
                                    <td>Dera Baba Nanak 1971 War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">137</th>
                                    <td></td>
                                    <td>24</td>
                                    <td>Halwara (Air Force Station) </td>
                                    <td>Air Force Station Halwara War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">138</th>
                                    <td></td>
                                    <td>25</td>
                                    <td>Hussainiwala, Ferozepur</td>
                                    <td>National Martyrs’ Memorial at Hussainiwala  </td>
                                </tr>
                                <tr>
                                <th scope="row">139</th>
                                    <td></td>
                                    <td>26</td>
                                    <td>Kapurthala</td>
                                    <td>Lieutenant Jhaggar Singh War Memorial (WW-1)</td>
                                </tr>
                                <tr>
                                <th scope="row">140</th>
                                    <td></td>
                                    <td>27</td>
                                    <td>Fazilka (Asafwala Village) </td>
                                    <td>Asafwala 1971 War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">141</th>
                                    <td></td>
                                    <td>28</td>
                                    <td>Kapurthala (Kapurthala-Kanjili-Kartarpur Road at Tri junction near Station Headquarters) </td>
                                    <td>Imperial State Forces War Memorial (WW-1)</td>
                                </tr>
                                <tr>
                                <th scope="row">142</th>
                                    <td></td>
                                    <td>29</td>
                                    <td>Pathankot (Air Force Station) </td>
                                    <td>Wall of Silence</td>
                                </tr>
                                <tr>
                                <th scope="row">143</th>
                                    <td></td>
                                    <td>30</td>
                                    <td>Patiala (Stadium Road near Polo Ground) </td>
                                    <td>Patiala State Forces Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">144</th>
                                    <td></td>
                                    <td>31</td>
                                    <td>Patiala (YPS Chowk) </td>
                                    <td>Black Elephant Division Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">145</th>
                                    <td><b>Rajasthan</b></td>
                                    <td>1</td>
                                    <td>Alwar (Company Garden) </td>
                                    <td>Alwar War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">146</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Churu (District Mukhyalaya Sainik Basti, Sector No 2 on main road Churu) </td>
                                    <td>Shahid Smarak Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">147</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Jaipur </td>
                                    <td>Sadhewala War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">148</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Jaipur (MI Road) </td>
                                    <td>Shahid Smarak War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">149</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Jaipur (Raj Path Road near State Legislative Assembly) </td>
                                    <td>Amar Jawan Jyoti War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">150</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Jaisalmer (Air Force Station) </td>
                                    <td>Vijay Stambh & Vijay Smarak</td>
                                </tr>
                                <tr>
                                <th scope="row">151</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Jodhpur (Next to the Umaid Bhavan Palace at the northern entrance to the cantonment)</td>
                                    <td>Konark War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">152</th>
                                    <td></td>
                                    <td>8</td>
                                    <td>Jodhpur (Paota Circle, Jodhpur)</td>
                                    <td>Major Shaitan Singh PVC Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">153</th>
                                    <td></td>
                                    <td>9</td>
                                    <td>Laungewala</td>
                                    <td>Laungewala War Memorial</td>
                                </tr>

                                <tr>
                                <th scope="row">154</th>
                                    <td></td>
                                    <td>10</td>
                                    <td>Laungewala Sector</td>
                                    <td>War Memorial BP 638</td>
                                </tr>

                                <tr>
                                <th scope="row">155</th>
                                    <td></td>
                                    <td>11</td>
                                    <td>Laungewala (Adjacent to Laungewala War Memorial) </td>
                                    <td>168 FD Regt War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">156</th>
                                    <td></td>
                                    <td>12</td>
                                    <td>Sriganganagar (Karanpur Town)</td>
                                    <td>Nagi War Memorial</td>
                                </tr>

                                <tr>
                                <th scope="row">157</th>
                                    <td><b>Tamil Nadu</b></td>
                                    <td>1</td>
                                    <td>Avadi, Chennai (Opposite Depot House 23 ED, AF) </td>
                                    <td>Air Force Station Avadi Kargil War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">158</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Tiruchirapalli (Gandhi Market)</td>
                                    <td>Tiruchirapalli WW-1 Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">159</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Wellington (Madras Regimental Centre)</td>
                                    <td>Madras Regiment War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">160</th>
                                    <td><b>Tripura</b></td>
                                    <td>1</td>
                                    <td>Agartala,</td>
                                    <td>Agartala War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">161</th>
                                    <td><b>Uttar Pradesh</b></td>
                                    <td>1</td>
                                    <td>Allahabad (Hall of Fame located at Old Cantonment)</td>
                                    <td>4 Infantry Division War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">162</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Allahabad (CAC Heritage Museum, Bamrauli) </td>
                                    <td>IAF Central Air Command War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">163</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Bareilly (Jat Regimental Centre)</td>
                                    <td>Jat Regiment War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">164</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Faizabad</td>
                                    <td>CMP & 7 Infantry Brigade War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">165</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Faizabad (Dogra Regimental Centre) </td>
                                    <td>Dogra War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">166</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Faizabad (HQ Madhya UP Sub Area, HQ 7 INF BDE)</td>
                                    <td>HQ 7 Infantry Brigade War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">167</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Farrukhabad, District Fatehgarh (Sikh LI Regimental Centre)</td>
                                    <td>Sikh Light Infantry War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">168</th>
                                    <td></td>
                                    <td>8</td>
                                    <td>Fatehgarh (Rajput Regimental Centre) </td>
                                    <td>Rajput Regiment War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">169</th>
                                    <td></td>
                                    <td>9</td>
                                    <td>Gorakhpur (Air Force Station)</td>
                                    <td>Air Force Station War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">170</th>
                                    <td></td>
                                    <td>10</td>
                                    <td>Jhansi Cantt</td>
                                    <td>White Tiger War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">171</th>
                                    <td></td>
                                    <td>11</td>
                                    <td>Kunraghat, Gorakhpur (Gorkha Recruiting Depot)</td>
                                    <td>Gorkha Brigade War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">172</th>
                                    <td></td>
                                    <td>12</td>
                                    <td>Lucknow, (AMC Centre and College) </td>
                                    <td>Armed Forces Medical Services War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">173</th>
                                    <td></td>
                                    <td>13</td>
                                    <td>Lucknow (Mahatma Gandhi Road)</td>
                                    <td>Smritika War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">174</th>
                                    <td></td>
                                    <td>14</td>
                                    <td>Lucknow (11 GR Regimental Centre)</td>
                                    <td>11 Gorkha Rifles War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">175</th>
                                    <td></td>
                                    <td>15</td>
                                    <td>Mathura (HQ 1 Corps Complex)</td>
                                    <td>1 Corps War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">176</th>
                                    <td></td>
                                    <td>16</td>
                                    <td>Meerut Cantonment</td>
                                    <td>Pine Division War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">177</th>
                                    <td></td>
                                    <td>17</td>
                                    <td>Meerut Cantonment</td>
                                    <td>Saragarhi Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">178</th>
                                    <td></td>
                                    <td>18</td>
                                    <td>NOIDA (Opposite Army Public School)</td>
                                    <td>NOIDA Shaheed Smarak</td>
                                </tr>
                                <tr>
                                <th scope="row">179</th>
                                    <td></td>
                                    <td>19</td>
                                    <td>Sarsawa, Saharanpur (Air Force Station)</td>
                                    <td>Air Force Station Sarsawa War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">180</th>
                                    <td></td>
                                    <td>20</td>
                                    <td>Varanasi (39 Gorkha Training Centre)</td>
                                    <td>39 Gorkha Training Centre War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">181</th>
                                    <td><b>Uttarakhand</b></td>
                                    <td>1</td>
                                    <td>Lansdowne (Garhwal Rifles Regimental </td>
                                    <td>Garhwal Rifles War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">182</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Pithoragarh</td>
                                    <td>Maharajke Memorial Park</td>
                                </tr>
                                <tr>
                                <th scope="row">183</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Ranikhet (Kumaon Regimental Centre)</td>
                                    <td>Kumaon Regiment War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">184</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Roorkee</td>
                                    <td>Bengal Sappers War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">185</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Dehradun (Indian Military Academy)</td>
                                    <td>Indian Military Academy War Memorial</td>
                                </tr>
                                <tr></tr>
                                <th scope="row">186</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Dehradun (Lal Gate)</td>
                                    <td>Lal Gate War Memorial</td>
                                <tr>
                                <th scope="row">187</th>
                                    <td><b>West Bengal</b></td>
                                    <td>1</td>
                                    <td>Binnaguri (Binnaguri Military Station)</td>
                                    <td>Bogra War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">188</th>
                                    <td></td>
                                    <td>2</td>
                                    <td>Darjeeling</td>
                                    <td>Batasia Loop War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">189</th>
                                    <td></td>
                                    <td>3</td>
                                    <td>Hasimara, Jaipaiguri District (Air Force Station, next to Officers’ Mess)</td>
                                    <td>Air Force Station Hasimara War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">190</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Howrah (Bank of River Hooghly)</td>
                                    <td>Lascar War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">191</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Kolkata (Red Road)</td>
                                    <td>The Glorious Dead War Memorial</td>
                                </tr>
                                <tr>
                                <th scope="row">192</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Kolkata (College Square)</td>
                                    <td>49th Bengalis War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">193</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Kolkata (Fort William)</td>
                                    <td>Purvi Kaman Vijay Smarak</td>
                                </tr>
                                <tr>
                                    <th scope="row">194</th>
                                    <td></td>
                                    <td>8</td>
                                    <td>Sukna Military Station (Near Siliguri)</td>
                                    <td>33 Corps Vijay Smarak War Memorial</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                     -->
                <div class="box-table">
                    <h3>Table 1 List of War Memorials in India</h3>
                    <table class="table regimental-tb">
                        <thead>
                            <tr>
                                <th scope="col">S.No</th>
                                <th scope="col">State</th>
                                <th scope="col">Location</th>
                                <th scope="col">Name of War Memorial</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td><b>Andhra Pradesh</b></td>
                                <td>Secunderabad Bison War Memorial</td>
                                <td>Bison War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td></td>
                                <td>Hyderabad (Mehdipatnam)</td>
                                <td>Basantar War Memorial </td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td></td>
                                <td>Secunderabad Cantonment (1 EME Centre)</td>
                                <td>1 EME Centre War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">4</th>
                                <td></td>
                                <td>Secunderabad (AOC Centre)</td>
                                <td>AOC Centre War Memorial </td>
                            </tr>
                            <tr>
                                <th scope="row">5</th>
                                <td></td>
                                <td>Secunderabad (Army Training Ground)</td>
                                <td>Veerula Sainika Smarak</td>
                            </tr>
                            <!-- <tr>
                                    <th scope="row">6</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Secunderabad (Bowenpally)</td>
                                    <td>Mighty Bombardiers War Memorial </td>
                                </tr>
                                <tr>
                                    <th scope="row">7</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Visakhapatnam (Ramakrishna Beach)</td>
                                    <td>Victory at Sea War Memorial</td>
                                </tr> -->
                            <tr>
                                <th scope="row">6</th>
                                <td><b> Arunachal Pradesh</b> </td>
                                <td> 18 Km from Walong</td>
                                <td>Helmet Top War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">7</th>
                                <td></td>
                                <td>Manmao Post</td>
                                <td>Manmao War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">8</th>
                                <td></td>
                                <td>Nuranang (25 km from Tawang)</td>
                                <td>Jaswantgarh War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">9</th>
                                <td></td>
                                <td>Tawang</td>
                                <td>Tawang War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">10</th>
                                <td></td>
                                <td>Walong</td>
                                <td>Walong War Memorial </td>
                            </tr>
                            <tr>
                                <th scope="row">11</th>
                                <td></td>
                                <td>Walong</td>
                                <td>Hut of Remembrance</td>
                            </tr>
                            <tr>
                                <th scope="row">12</th>
                                <td></td>
                                <td>Tilam</td>
                                <td>Memorial at Namti</td>
                            </tr>

                            <tr>
                                <th scope="row">13</th>
                                <td></td>
                                <td>Lumpo</td>
                                <td>War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">14</th>
                                <td></td>
                                <td>Hathong La</td>
                                <td>War Memorial Hathong La</td>
                            </tr>

                            <tr>
                                <th scope="row">15</th>
                                <td></td>
                                <td>Tawang</td>
                                <td>War Memorial New Khingzemane</td>
                            </tr>

                            <tr>
                                <th scope="row">16</th>
                                <td></td>
                                <td>Surwa Samba</td>
                                <td>War Memorial Surwa Samba</td>
                            </tr>

                            <tr>
                                <th scope="row">17</th>
                                <td></td>
                                <td>Bum La</td>
                                <td>Sub Joginder Singh, PVC War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">18</th>
                                <td></td>
                                <td>Phudung</td>
                                <td>Brig Hoshiar Singh War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">19</th>
                                <td></td>
                                <td>Rupa</td>
                                <td>Brig Hoshiar Singh War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">20</th>
                                <td></td>
                                <td>Nyukmadung</td>
                                <td>Nyukmadung War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">21</th>
                                <td><b>Assam</b></td>
                                <td>Dibrugarh</td>
                                <td>DAH War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">22</th>
                                <td></td>
                                <td>Likabali</td>
                                <td>War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">23</th>
                                <td></td>
                                <td>Silchar (On top of Kheba Hills)</td>
                                <td>Amar Jawan War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">24</th>
                                <td></td>
                                <td>Silchar</td>
                                <td>Amar Jawan War Memorial </td>
                            </tr>

                            <tr>
                                <th scope="row">25</th>
                                <td><b>Bihar</b></td>
                                <td> Danapur (Bihar Regimental Centre)</td>
                                <td>Bihar Regiment War Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">26</th>
                                <td><b>Delhi</b></td>
                                <td>New Delhi (Teen Murti Road)</td>
                                <td>Teen Murti Memoria</td>

                            </tr>
                            <tr>
                                <th scope="row">27</th>
                                <td></td>
                                <td>New Delhi (Rajpath)</td>
                                <td>India Gate</td>
                            </tr>
                            <tr>
                                <th scope="row">28</th>
                                <td></td>
                                <td>New Delhi (Rajputana Rifles Regimental Centre)</td>
                                <td>Rajputana Rifles War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">29</th>
                                <td></td>
                                <td>New Delhi</td>
                                <td>Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">30</th>
                                <td></td>
                                <td>New Delhi (India Gate, C Hexagon Road) </td>
                                <td>National War Memorial</td>
                            </tr>
                            <!-- <tr>
                                    <th scope="row">28</th>
                                    <td><b>Goa</b></td>
                                    <td>1</td>
                                    <td>Anjadiv Island, Karwar</td>
                                    <td>Anjadiv Island War Memorial</td>
                                </tr> -->
                            <tr>
                                <th scope="row">31</th>
                                <td><b>Gujarat</b></td>
                                <td>Ahmedabad Cantonment</td>
                                <td>Golden Katar War Memoria</td>
                            </tr>
                            <tr>
                                <th scope="row">32</th>
                                <td></td>
                                <td>Ahmedabad (Shahibaugh, near State Guest House)</td>
                                <td>Ahmedabad War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">33</th>
                                <td></td>
                                <td>Bhuj (Rajaram Park, Military Station Bhuj)</td>
                                <td>Bhuj War Memorial</td>

                            </tr>
                            <tr>
                                <th scope="row">34</th>
                                <td></td>

                                <td>Gandhinagar (Chiloda Military Station)</td>
                                <td>Parbat Ali War Memorial</td>

                            </tr>
                            <!-- <tr>
                                    <th scope="row">35</th>
                                    <td></td>
                                    <td>5</td>
                                    <td>Jamnagar (Air Force Station)</td>
                                    <td>Vijay Stumbh</td>

                                    </tr>
                                    <tr>
                                    <th scope="row">34</th>
                                    <td></td>
                                    <td>6</td>
                                    <td>Jamnagar (Rozi Island)</td>
                                    <td>INS Valsura War Memorial</td>

                                    </tr> 
                                    <tr>
                                    <th scope="row">35</th>
                                    <td></td>
                                    <td>7</td>
                                    <td>Jamnagar (Air Force Station</td>
                                    <td>Air Force Station Jamnagar War Memorial</td>

                                </tr>-->
                            <tr>
                                <th scope="row">35</th>
                                <td><b>Haryana<b></td>
                                <td>Chandimandir Cantt</td>
                                <td>Veer Smriti Western Command War Memorial</td>

                            </tr>
                            <tr>
                                <th scope="row">36</th>
                                <td></td>
                                <td>Gurgaon (John Hall)</td>
                                <td>Gurgaon Martyr’s Memorial</td>

                            </tr>
                            <tr>
                                <th scope="row">37</th>
                                <td></td>
                                <td>Hissar (Inside MD University Campus) </td>
                                <td>Amar Jawan War Memorial</td>

                            </tr>
                            <tr>
                                <th scope="row">38</th>
                                <td></td>
                                <td>Jhajjar (At Sainik Rest House) </td>
                                <td>Jhajjar War Memorial</td>

                            </tr>
                            <tr>
                                <th scope="row">39</th>
                                <td></td>
                                <td>Jind (Opposite District Court on Gohana Road) </td>
                                <td>Jind War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">40</th>
                                <td></td>
                                <td>Narwana (Navdeep Stadium) </td>
                                <td>Narwana War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">41</th>
                                <td></td>
                                <td>Bahadurgarh (On Rohtak road)</td>
                                <td>BahadurgarhWar Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">42</th>
                                <td></td>
                                <td>Rohtak (Inside ManasaSarowar Park) </td>
                                <td>Yudh Shahid Samarak</td>
                            </tr>
                            <tr>
                                <th scope="row">43</th>
                                <td><b>Himachal Pradesh</b></td>
                                <td>Dharmasala</td>
                                <td>Dharamsala Martyrs Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">44</th>
                                <td></td>
                                <td>Jutogh Cantonment, Shimla (Right of gate No 1 bisecting National Highway at Toute)</td>
                                <td>3rd Mountain Artillery Brigade War Memorial (WW-1)</td>
                            </tr>

                            <tr>
                                <th scope="row">45</th>
                                <td></td>
                                <td>Subathu (14 GTC)</td>
                                <td>14 GTC War Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">46</th>
                                <td></td>
                                <td>Hamirpur</td>
                                <td>Capt Mridul Park Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">47</th>
                                <td><b>Jammu & Kashmir and Ladakh(UT)</b></td>
                                <td>Baramulla</td>
                                <td>1 Sikh War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">48</th>
                                <td></td>
                                <td>Baramulla</td>
                                <td>Dagger Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">49</th>
                                <td></td>
                                <td>BB Cantt, Srinagar</td>
                                <td>Op Rakshak Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">50</th>
                                <td></td>
                                <td>Karu, Ladakh (UT)</td>
                                <td>Trishul War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">51</th>
                                <td></td>
                                <td>Jammu (Bahu Wali Rakh) </td>
                                <td>Balidan Stambh</td>
                            </tr>

                            <tr>
                                <th scope="row">52</th>
                                <td></td>
                                <td>Jammu (Tiger Park opposite St Mary’s Public School) </td>
                                <td>Tiger War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">53</th>
                                <td></td>
                                <td>Rezang La, Ladakh (UT)</td>
                                <td>Rezang La War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">54</th>
                                <td></td>
                                <td>Leh, Ladakh (UT)</td>
                                <td>Leh (1962, 1965, 1971) War Memorial </td>
                            </tr>

                            <tr>
                                <th scope="row">55</th>
                                <td></td>
                                <td>Leh, Ladakh (UT)</td>
                                <td>Leh (1947–48) War Memorial </td>
                            </tr>

                            <tr>
                                <th scope="row">56</th>
                                <td></td>
                                <td>Machhal</td>
                                <td>Sahi Memorial Hospital</td>
                            </tr>

                            <tr>
                                <th scope="row">57</th>
                                <td></td>
                                <td>Poonch (In front of 171 MH link road from NH-1 to Village Sangwali Mandi) </td>
                                <td>Lok Bahadur Stadium Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">58</th>
                                <td></td>
                                <td>Ladakh (UT)</td>
                                <td>Hall of Fame</td>
                            </tr>

                            <tr>
                                <th scope="row">59</th>
                                <td></td>
                                <td>Siachen, Ladakh (UT) (Siachen Base Camp)</td>
                                <td>Siachen War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">60</th>
                                <td></td>
                                <td>Daulat Beg Oldi, Ladakh (UT)</td>
                                <td>Joint Memorial, DBO, Advance Landing Ground (SSN)</td>
                            </tr>

                            <tr>
                                <th scope="row">61</th>
                                <td></td>
                                <td>Srinagar (HQ 15 Corps) </td>
                                <td>15 Corps War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">62</th>
                                <td></td>
                                <td>Srinagar</td>
                                <td>Ladakh Scouts Regt War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">63</th>
                                <td></td>
                                <td>Leh, Ladakh (UT)</td>
                                <td>Ladakh Scouts Regt War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">64</th>
                                <td></td>
                                <td>Uri, Baramulla</td>
                                <td>War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">65</th>
                                <td></td>
                                <td>Uri, Baramulla</td>
                                <td>Nand Singh War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">66</th>
                                <td></td>
                                <td>Gurez Valley</td>
                                <td>Gurez War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">67</th>
                                <td></td>
                                <td>Tithwal</td>
                                <td>Tithwal War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">68</th>
                                <td></td>
                                <td>Trehgam Garrison</td>
                                <td>Hajipir War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">69</th>
                                <td></td>
                                <td>Sonapindigali</td>
                                <td>Sonapindigali War Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">70</th>
                                <td></td>
                                <td>Machhal</td>
                                <td>Machhal War Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">71</th>
                                <td></td>
                                <td>Chowkibal</td>
                                <td>Chowkibal War Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">72</th>
                                <td></td>
                                <td>Quila Darhal</td>
                                <td>War Memorial at Saheed Garh</td>
                            </tr>


                            <tr>
                                <th scope="row">73</th>
                                <td></td>
                                <td>Poonch</td>
                                <td>War Memorial Naman Sthal</td>
                            </tr>


                            <tr>
                                <th scope="row">74</th>
                                <td></td>
                                <td>Jhangar</td>
                                <td>Usman War Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">75</th>
                                <td></td>
                                <td>Naushera</td>
                                <td>Naik Jadunath Singh, PVC War Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">76</th>
                                <td></td>
                                <td>Naushera</td>
                                <td>Butalia Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">77</th>
                                <td></td>
                                <td>Udhampur</td>
                                <td>Dhruva Shaheed Smarak</td>
                            </tr>


                            <tr>
                                <th scope="row">78</th>
                                <td></td>
                                <td>Chushul, Ladakh (UT)</td>
                                <td>Chushul War Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">79</th>
                                <td></td>
                                <td>Dras, Kargil, Ladakh (UT) (Bimbat)</td>
                                <td>Bimbat 8 Mountain Division Kargil War Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">80</th>
                                <td></td>
                                <td>Gumri, Ladakh (UT)</td>
                                <td>Zoji La War Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">81</th>
                                <td></td>
                                <td>Samba (On link road from NH-1A to Village Katli) </td>
                                <td>3 Madras Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">82</th>
                                <td></td>
                                <td>Samba (Cremation ground)</td>
                                <td>2/Lt Arun Khetrapal PVC War Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">83</th>
                                <td></td>
                                <td>Samba (On NH-1, approx 7 kms on Samba road towards Pathankot)</td>
                                <td>Major RS Rajwat Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">84</th>
                                <td></td>
                                <td>Satwari Cantonment (Manekshaw Marg, opp Tiger CSD Canteen)</td>
                                <td>Jammu and Kashmir State Forces War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">85</th>
                                <td><b>Jharkhand</b></td>
                                <td>Ramgarh Cantonment (Sikh Regimental Centre)</td>
                                <td>Saragarhi War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">86</th>
                                <td></td>
                                <td>Ramgarh Cantonment (Sikh Regimental Centre)</td>
                                <td>Sikh Regiment War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">87</th>
                                <td></td>
                                <td>Ramgarh Cantonment (Punjab Regimental Centre)</td>
                                <td>Punjab Regiment War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">88</th>
                                <td></td>
                                <td>Ranchi (Depatoli Cantonment) </td>
                                <td>Jharkhand War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">89</th>
                                <td><b>Karnataka</b></td>
                                <td>Bangalore (Along Central Parade Ground, MEG & Centre) </td>
                                <td>Madras Sappers War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">90</th>
                                <td></td>
                                <td>Bangalore (ASC Centre and College) </td>
                                <td>Army Service Corps War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">91</th>
                                <td></td>
                                <td>Bangalore (ASC Centre and College) </td>
                                <td>Army Transport Animals Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">92</th>
                                <td></td>
                                <td>Bangalore (CMP Centre & School) </td>
                                <td>Corps of Military Police War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">93</th>
                                <td></td>
                                <td>Bangalore (End of Brigade Road, near junction between Brigade & Residency Roads) </td>
                                <td>Pioneer Corps War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">94</th>
                                <td></td>
                                <td>Bangalore </td>
                                <td>The Parachute Regiment War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">95</th>
                                <td></td>
                                <td>Bangalore (MEG & Centre) </td>
                                <td>British War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">96</th>
                                <td></td>
                                <td>Belgaum (Maratha LI Regimental Centre) </td>
                                <td>Parachute Regiment War Memorial</td>
                            </tr>


                            <!-- <tr>
                                    <th scope="row">83</th>
                                    <td></td>
                                    <td>10</td>
                                    <td>Bidar (Air Force Station) </td>
                                    <td>Air Force Station Bidar Memorial</td>
                                </tr> -->

                            <tr>
                                <th scope="row">97</th>
                                <td><b>Kerala</b></td>
                                <td>Cannanore District (DSC Centre)</td>
                                <td>Defence Security Corps Gaurav Sthal</td>
                            </tr>

                            <tr>
                                <th scope="row">98</th>
                                <td></td>
                                <td>Cannanore</td>
                                <td>Defence Security War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">99</th>
                                <td></td>
                                <td>Thiruvananthapuram (Pangode: on Vazhuthakad- Thirumala Main Road opposite 91 InfBde)</td>
                                <td>The Bogra Memorial Hut</td>
                            </tr>

                            <!-- <tr>
                                    <th scope="row">87</th>
                                    <td></td>
                                    <td>4</td>
                                    <td>Thiruvananthapuram (Pangode: on Vazhuthakad- Thirumala Main Road opposite 91 InfBde)</td>
                                    <td>The Bogra Memorial Hut</td>
                                </tr> -->

                            <tr>
                                <th scope="row">100</th>
                                <td><b>Madhya Pradesh</b></td>
                                <td>Bhopal (3 EME Centre) </td>
                                <td>No. 3 EME Centre War memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">101</th>
                                <td></td>
                                <td>Saugor</td>
                                <td>Shahbaaz War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">102</th>
                                <td></td>
                                <td>Saugor</td>
                                <td>The Mahar Regiment War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">103</th>
                                <td></td>
                                <td>Pachmarhi</td>
                                <td>Army Education Corps War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">104</th>
                                <td></td>
                                <td>Bhopal (Corps HQ Complex) </td>
                                <td>Sudarshan Chakra War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">105</th>
                                <td></td>
                                <td>Jabalpur (1 Military Training Regiment, 1 Signal Training Centre) </td>
                                <td>Corps of Signals War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">106</th>
                                <td></td>
                                <td>Jabalpur (Grenadiers Regimental Centre) </td>
                                <td>The Grenadiers War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">107</th>
                                <td></td>
                                <td>Jabalpur (Jammu and Kashmir Rifles Regimental Centre) </td>
                                <td>Jammu and Kashmir Rifles War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">108</th>
                                <td></td>
                                <td>Jabalpur (CMM School)</td>
                                <td>KanglaTongbi War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">109</th>
                                <td></td>
                                <td>Mhow</td>
                                <td>Infantry Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">110</th>
                                <td><b>Maharashtra</b></td>
                                <td>Ahmednagar (Armoured Corps Centre and School) </td>
                                <td>Armoured Corps War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">111</th>
                                <td></td>
                                <td>Southern Command AOR (Ranjit Ground in front of HQ 94 Armed Brigade)</td>
                                <td>45 CAV War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">112</th>
                                <td></td>
                                <td>Ahmednagar (Mechanised Infantry Regimental Centre)</td>
                                <td>Mechanised Infantry Regiment War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">113</th>
                                <td></td>
                                <td>Khadakwasla, Pune (NDA)</td>
                                <td>Hut of Remembrance</td>
                            </tr>



                            <tr>
                                <th scope="row">114</th>
                                <td></td>
                                <td>Kirkee (Training Battalion 1 Area, BEG & Centre) </td>
                                <td>Bombay Pioneers Memorial (WW-1)</td>
                            </tr>

                            <tr>
                                <th scope="row">115</th>
                                <td></td>
                                <td>Kirkee (Entrance to Parade Ground, BEG & Centre) </td>
                                <td>Bombay Sappers War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">116</th>
                                <td></td>
                                <td>Nasik Road Camp</td>
                                <td>Regiment of Artillery War Memorial </td>
                            </tr>

                            <tr>
                                <th scope="row">117</th>
                                <td></td>
                                <td>Kamptee</td>
                                <td>Brigade of The Guards Regiment War Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">118</th>
                                <td></td>
                                <td>Mumbai (Military Station, facing Colaba Navynagar Road, behind Afghan Church, Colaba) </td>
                                <td>Subedar Joginder Singh PVC Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">119</th>
                                <td></td>
                                <td>Nasik Road Camp (Artillery Centre)</td>
                                <td>Artillery Centre War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">120</th>
                                <td></td>
                                <td>Pune Cantonment (Morwada Junction, MahadjiShinde Road)</td>
                                <td>Morwada War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">121</th>
                                <td></td>
                                <td>Pune</td>
                                <td>Intelligence Corps War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">122</th>
                                <td><b>Manipur</b></td>
                                <td>Imphal (Khenjang Village) </td>
                                <td>Kangla Tongbi War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">123</th>
                                <td></td>
                                <td>Leimakhong</td>
                                <td>Shantivan War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">124</th>
                                <td></td>
                                <td>Khongjam</td>
                                <td>War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">125</th>
                                <td><b>Meghalaya</b></td>
                                <td>Shillong (58 Gorkha Training Centre) </td>
                                <td>58 GTC War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">126</th>
                                <td></td>
                                <td>Shillong (Happy Valley, Assam Regimental Centre) </td>
                                <td>Assam Regiment War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">127</th>
                                <td></td>
                                <td>Shillong</td>
                                <td>1971 War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">128</th>
                                <td><b>Nagaland</b></td>
                                <td>Zakhama Military Station</td>
                                <td>Orchid Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">129</th>
                                <td><b>Orissa</b></td>
                                <td>Gopalpur</td>
                                <td>Army Air Defence War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">130</th>
                                <td><b>Punjab</b></td>
                                <td>Patiala </td>
                                <td>Cenotaph War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">131</th>
                                <td></td>
                                <td>Amritsar (Military Station Khasa) </td>
                                <td>Dograi War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">132</th>
                                <td></td>
                                <td>Amritsar (Near Rattoke Gurudwara) </td>
                                <td>5 Gorkha Rifles War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">133</th>
                                <td></td>
                                <td>Amritsar (Pulkanjri, Dhanaya Kalan) </td>
                                <td>Pulkanjri War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">134</th>
                                <td></td>
                                <td>Asal Uttar (Tarn Taran Sahib) </td>
                                <td>CQMH Abdul Hamid PVC Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">135</th>
                                <td></td>
                                <td>Asal Uttar (Tarn Taran Sahib, on road Khemkaran-Valtoha) </td>
                                <td>7 Grenadiers War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">136</th>
                                <td></td>
                                <td>Bhura Kuhna, Tarn Taran District</td>
                                <td>2/Lt JP Gaur Memorial</td>
                            </tr>


                            <tr>
                                <th scope="row">137</th>
                                <td></td>
                                <td>Bhura Kuhna, Tarn Taran District</td>
                                <td>2 Madras Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">138</th>
                                <td></td>
                                <td>Bhura Kuhna, (On Road Bhikhiwind Khemkaran near village Bhura Kuhna) </td>
                                <td>Sapper Harak Singh Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">139</th>
                                <td></td>
                                <td>Ferozepur Cantt</td>
                                <td>Sehjra 1971 Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">140</th>
                                <td></td>
                                <td>Ferozepur</td>
                                <td>Barki 1965 Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">141</th>
                                <td></td>
                                <td>Ferozepur</td>
                                <td>Saragarhi Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">142</th>
                                <td></td>
                                <td>Ferozepur (Golden Arrow House crossing) </td>
                                <td>Satluj Campaign War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">143</th>
                                <td></td>
                                <td>Ferozepur (Mehdipur Village)</td>
                                <td>6 Mahar War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">144</th>
                                <td></td>
                                <td>Ferozepur (Golden Arrow House crossing) </td>
                                <td>VI K.E.O. Cavalry War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">145</th>
                                <td></td>
                                <td>Ferozepur (Golden Arrow House crossing) </td>
                                <td>Brownlow’s Punjabis War Memorial</td>
                            </tr>

                            <!-- <tr>
                                    <th scope="row">146</th>
                                    <td></td>
                                    <td>Ferozepur (Golden Arrow House crossing)  </td>
                                    <td>VI K.E.O. Cavalry War Memorial</td>
                                </tr> -->
                            <tr>
                                <th scope="row">146</th>
                                <td></td>
                                <td>Ferozepur (Near Golden Arrow House crossing) </td>
                                <td>19, 22 & 24 Punjabis War Memorial 1914–1919</td>
                            </tr>
                            <tr>
                                <th scope="row">147</th>
                                <td></td>
                                <td>Ferozepur (Cantonment General Hospital Family Wing) </td>
                                <td>Gallipoli Memorial tablet</td>
                            </tr>
                            <!-- <tr>
                                    <th scope="row">148</th>
                                    <td></td>
                                    <td>Ferozepur (Near Mehdipur Village close to Pir Baba mazar) </td>
                                    <td>Sqn Ldr Raman Uppal Memorial</td>
                                </tr> -->
                            <tr>
                                <th scope="row">148</th>
                                <td></td>
                                <td>Gurdaspur (Batala)</td>
                                <td>Batala War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">149</th>
                                <td></td>
                                <td>Gurdaspur (Dera Baba Nanak) </td>
                                <td>Dera Baba Nanak 1971 War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">150</th>
                                <td></td>
                                <td>Hussainiwala, Ferozepur</td>
                                <td>National Martyrs’ Memorial at Hussainiwala </td>
                            </tr>
                            <tr>
                                <th scope="row">151</th>
                                <td></td>
                                <td>Kapurthala</td>
                                <td>Lieutenant Jhaggar Singh War Memorial (WW-1)</td>
                            </tr>
                            <!-- <tr>
                                    <th scope="row">139</th>
                                    <td></td>
                                    <td>Halwara (Air Force Station) </td>
                                    <td>Air Force Station Halwara War Memorial</td>
                                </tr> -->
                            <tr>
                                <th scope="row">152</th>
                                <td></td>
                                <td>Fazilka (Asafwala Village) </td>
                                <td>Asafwala 1971 War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">153</th>
                                <td></td>
                                <td>Kapurthala (Kapurthala-Kanjili-Kartarpur Road at Tri junction near Station Headquarters) </td>
                                <td>Imperial State Forces War Memorial (WW-1)</td>
                            </tr>

                            <tr>
                                <th scope="row">154</th>
                                <td></td>
                                <td>Patiala (Stadium Road near Polo Ground) </td>
                                <td>Patiala State Forces Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">155</th>
                                <td></td>
                                <td>Patiala (YPS Chowk) </td>
                                <td>Black Elephant Division Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">156</th>
                                <td><b>Rajasthan</b></td>
                                <td>Alwar (Company Garden) </td>
                                <td>Alwar War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">157</th>
                                <td></td>
                                <td>Churu (District Mukhyalaya Sainik Basti, Sector No 2 on main road Churu) </td>
                                <td>Shahid Smarak Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">158</th>
                                <td></td>
                                <td>Jaipur </td>
                                <td>Sadhewala War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">159</th>
                                <td></td>

                                <td>Jaipur (MI Road) </td>
                                <td>Shahid Smarak War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">160</th>
                                <td></td>
                                <td>Jaipur (Raj Path Road near State Legislative Assembly) </td>
                                <td>Amar Jawan Jyoti War Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">161</th>
                                <td></td>
                                <td>Jodhpur (Next to the Umaid Bhavan Palace at the northern entrance to the cantonment)</td>
                                <td>Konark War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">162</th>
                                <td></td>
                                <td>Jodhpur (Paota Circle, Jodhpur)</td>
                                <td>Major Shaitan Singh PVC Memorial</td>
                            </tr>
                            <tr>
                                <th scope="row">163</th>
                                <td></td>
                                <td>Laungewala</td>
                                <td>Laungewala War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">164</th>
                                <td></td>
                                <td>Laungewala Sector</td>
                                <td>War Memorial BP 638</td>
                            </tr>

                            <tr>
                                <th scope="row">165</th>
                                <td></td>
                                <td>Laungewala (Adjacent to Laungewala War Memorial) </td>
                                <td>168 FD Regt War Memorial</td>
                            </tr>

                            <tr>
                                <th scope="row">166</th>
                                <td></td>
                                <td>Sriganganagar (Karanpur Town)</td>
                                <td>Nagi War Memorial</td>
                            </tr>

                            <th scope="row">167</th>
                            <td><b>Sikkim</b></td>
                            <td>Tumulpur </td>
                            <td>Jaswant Baba, MVC War Memorial</td>
                            </tr>

                            <th scope="row">168</th>
                            <td></td>
                            <td>Nathu La</td>
                            <td>Nathu La BMP Hut War Memorial</td>
                            </tr>

                            <th scope="row">169</th>
                            <td< /td>
                                <td>Sherathang</td>
                                <td>Sherathang War Memorial</td>
                                </tr>

                                <th scope="row">170</th>
                                <td></td>
                                <td>Gangtok </td>
                                <td>Baba Harbhajan Singh Shrine</td>
                                </tr>


                                <tr>
                                    <th scope="row">171</th>
                                    <td><b>Tamil Nadu</b></td>
                                    <td>Tiruchirapalli (Gandhi Market)</td>
                                    <td>Tiruchirapalli WW-1 Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">172</th>
                                    <td></td>
                                    <td>Wellington (Madras Regimental Centre)</td>
                                    <td>Madras Regiment War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">173</th>
                                    <td><b>Tripura</b></td>
                                    <td>Agartala,</td>
                                    <td>Agartala War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">174</th>
                                    <td></td>
                                    <td>Agartala</td>
                                    <td>Albert Ekka War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">175</th>
                                    <td><b>Uttar Pradesh</b></td>
                                    <td>Allahabad (Hall of Fame located at Old Cantonment)</td>
                                    <td>4 Infantry Division War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">176</th>
                                    <td></td>
                                    <td>Agra</td>
                                    <td>Shatrujeet War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">177</th>
                                    <td></td>
                                    <td>Bareilly (Jat Regimental Centre)</td>
                                    <td>Jat Regiment War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">178</th>
                                    <td></td>
                                    <td>Faizabad</td>
                                    <td>CMP & 7 Infantry Brigade War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">179</th>
                                    <td></td>
                                    <td>Faizabad (Dogra Regimental Centre) </td>
                                    <td>Dogra War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">180</th>
                                    <td></td>
                                    <td>Faizabad (HQ Madhya UP Sub Area, HQ 7 INF BDE)</td>
                                    <td>HQ 7 Infantry Brigade War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">181</th>
                                    <td></td>
                                    <td>Farrukhabad, District Fatehgarh (Sikh LI Regimental Centre)</td>
                                    <td>Sikh Light Infantry War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">182</th>
                                    <td></td>
                                    <td>Fatehgarh (Rajput Regimental Centre) </td>
                                    <td>Rajput Regiment War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">183</th>
                                    <td></td>
                                    <td>Lucknow</td>
                                    <td>Mangal Pandey War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">184</th>
                                    <td></td>
                                    <td>Jhansi Cantt</td>
                                    <td>White Tiger War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">185</th>
                                    <td></td>
                                    <td>Kunraghat, Gorakhpur (Gorkha Recruiting Depot)</td>
                                    <td>Gorkha Brigade War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">186</th>
                                    <td></td>
                                    <td>Lucknow, (AMC Centre and College) </td>
                                    <td>Armed Forces Medical Services War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">187</th>
                                    <td></td>
                                    <td>Lucknow (Mahatma Gandhi Road)</td>
                                    <td>Smritika War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">188</th>
                                    <td></td>
                                    <td>Lucknow</td>
                                    <td>War Memorial of Kiranti Lines</td>
                                </tr>
                                <tr>
                                    <th scope="row">189</th>
                                    <td></td>
                                    <td>Lucknow (11 GR Regimental Centre)</td>
                                    <td>11 Gorkha Rifles War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">190</th>
                                    <td></td>
                                    <td>Mathura (HQ 1 Corps Complex)</td>
                                    <td>1 Corps War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">191</th>
                                    <td></td>
                                    <td>Meerut</td>
                                    <td>Dugal Dwar& Naik Dwar</td>
                                </tr>

                                <tr>
                                    <th scope="row">192</th>
                                    <td></td>
                                    <td>Meerut Cantonment</td>
                                    <td>Col KH Sharawat, SM Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">193</th>
                                    <td></td>
                                    <td>Meerut Cantonment</td>
                                    <td>Maj Ranbir Singh, VrC Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">194</th>
                                    <td></td>
                                    <td>Meerut Cantonment</td>
                                    <td>Pine Division War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">195</th>
                                    <td></td>
                                    <td>Meerut Cantonment</td>
                                    <td>Saragarhi Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">196</th>
                                    <td></td>
                                    <td>NOIDA (Opposite Army Public School)</td>
                                    <td>NOIDA Shaheed Smarak</td>
                                </tr>
                                <tr>
                                    <th scope="row">197</th>
                                    <td></td>
                                    <td>Shahjahanpur</td>
                                    <td>Nk Jadunath Singh, PVC Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">198</th>
                                    <td></td>
                                    <td>Shahjahanpur</td>
                                    <td>Nk Jadunath Singh, PVC Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">199</th>
                                    <td></td>
                                    <td>Shahjahanpur</td>
                                    <td>Shahjahanpur War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">200</th>
                                    <td></td>
                                    <td>Varanasi (39 Gorkha Training Centre)</td>
                                    <td>39 Gorkha Training Centre War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">201</th>
                                    <td></td>
                                    <td>Baghpat</td>
                                    <td>War Memorial, Baoli</td>
                                </tr>

                                <tr>
                                    <th scope="row">202</th>
                                    <td><b>Uttarakhand</b></td>
                                    <td>Lansdowne (Garhwal Rifles Regimental) </td>
                                    <td>Garhwal Rifles War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">203</th>
                                    <td></td>
                                    <td>Pithoragarh</td>
                                    <td>Maharajke Memorial Park</td>
                                </tr>
                                <tr>
                                    <th scope="row">204</th>
                                    <td></td>
                                    <td>Ranikhet (Kumaon Regimental Centre)</td>
                                    <td>Kumaon Regiment War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">205</th>
                                    <td></td>
                                    <td>Roorkee</td>
                                    <td>Bengal Sappers War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">206</th>
                                    <td></td>
                                    <td>Dehradun (Indian Military Academy)</td>
                                    <td>Indian Military Academy War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">207</th>
                                    <td></td>
                                    <td>Dehradun (Lal Gate)</td>
                                    <td>Lal Gate War Memorial</td>
                                <tr>

                                <tr>
                                    <th scope="row">208</th>
                                    <td></td>
                                    <td>Dehradun</td>
                                    <td>Khalanga War Memorial</td>
                                <tr>

                                <tr>
                                    <th scope="row">209</th>
                                    <td></td>
                                    <td>Dehradun</td>
                                    <td>Memorial of Ghagora</td>
                                <tr>

                                <tr>
                                    <th scope="row">210</th>
                                    <td></td>
                                    <td>Tehri Garh</td>
                                    <td>War Memorial Late Rfn Kuldeep Singh, VrC</td>
                                <tr>

                                    <th scope="row">211</th>
                                    <td><b>West Bengal</b></td>
                                    <td>Binnaguri (Binnaguri Military Station)</td>
                                    <td>Bogra War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">212</th>
                                    <td></td>
                                    <td>Darjeeling</td>
                                    <td>Batasia Loop War Memorial</td>
                                </tr>

                                <tr>
                                    <th scope="row">213</th>
                                    <td></td>
                                    <td>Howrah (Bank of River Hooghly)</td>
                                    <td>Lascar War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">214</th>
                                    <td></td>
                                    <td>Kolkata (Red Road)</td>
                                    <td>The Glorious Dead War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">215</th>
                                    <td></td>
                                    <td>Kolkata (College Square)</td>
                                    <td>49th Bengalis War Memorial</td>
                                </tr>
                                <tr>
                                    <th scope="row">216</th>
                                    <td></td>
                                    <td>Kolkata (Fort William)</td>
                                    <td>Purvi Kaman Vijay Smarak</td>
                                </tr>
                                <tr>
                                    <th scope="row">217</th>
                                    <td></td>
                                    <td>Sukna Military Station (Near Siliguri)</td>
                                    <td>33 Corps Vijay Smarak War Memorial</td>
                                </tr>
                        </tbody>
                    </table>
                </div>

                <!-- <div class="note">
                    <span>Notes</span>
                    <ol>
                        <li>Rana TS Chhina, ‘Introduction’, in The Last Post, n. 1, p. 6.</li>
                        <li>Ibid.</li>
                        <li>Ibid., p. 16.</li>
                        <li>Chhina, The Last Post, n. 1, p. 18.</li>
                        <li>Kaushik Roy, The Indian Army and the First World War: 1914-1918, New Delhi: Oxford University Press, Kindle edition.</li>
                        <li>The description text for the memorials has been sourced from Maj Gen (Retd) Ian Cardozo, AVSM, SM, The Indian Army in World War I, 1914-1989, New Delhi: USI and Manohar, 2019, pp. 216–231; and from Chhina, The Last Post, n. 1.</li>
                        <li>Simon Doherty and Tom Donovan, The Indian Corps on the Western Front- A Handbook and Battlefield Guide, New Delhi: United Service Institution of India, New Delhi, 2014.</li>
                        <li>Ibid., p. 156.</li>
                        <li>Ibid.</li>
                        <li>Ibid., p. 155.</li>
                        <li>Ibid., p. 156.</li>
                        <li>Chhina, The Last Post, n. 1, p. 61.</li>
                        <li>Ibid. p. 62</li>
                        <li>Ibid., p. 66.</li>
                        <li>Ibid., p. 70.</li>
                        <li>Doherty and Donovan, The Indian Corps on the Western Front, n. 12, pp. 154, 155.</li>
                        <li>Chhina, The Last Post, n. 1, p. 72.</li>
                        <li>Ibid., p. 73.</li>
                        <li>Ibid., p. 121.</li>
                        <li>Ibid., p. 108.</li>
                        <li>Ibid., p. 111.</li>
                        <li>Ibid., p. 78.</li>
                        <li>Ibid. p. 105.</li>
                        <li>Ibid., p. 106.</li>
                        <li>Ibid., p. 55.</li>
                        <li>Ibid., p. 96.</li>
                        <li>Ibid., p. 97.</li>
                        <li>Ibid., p. 107.</li>
                        <li>Ibid., p. 119.</li>
                        <li>Ibid., p. 83.</li>
                        <li>Ibid., p. 88.</li>
                        <li>Ibid., p. 90.</li>
                        <li>Ibid., p. 77.</li>
                        <li>Ibid., p. 85.</li>
                        <li>Ibid., p. 92.</li>
                        <li>Ibid., p. 8.</li>
                        <li>MargaretMacMillan, The War that Ended Peace: How Europe Abandoned Peace for the First World War, London: Profile books, Kindle Edition.</li>
                        <li>Ibid.</li>
                        <li>Chhina, The Last Post, n. 1, p. 30.</li>
                        <li>Ibid.</li>
                        <li>Ibid.</li>
                        <li>Ibid., p. 40.</li>
                        <li>Ibid., p. 43.</li>

                    </ol>

                </div> -->


            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>