
<?php if(have_rows('banner_image')): ?>
    <?php while(have_rows('banner_image')): the_row(); ?>
        <section class="iconic-leaders-detail-banner"
                style="background-image: url('<?php echo get_sub_field('banner_bg'); ?>')">
                <div class="container">
                    <h1 class="banner-content"><?php echo get_sub_field('banner_heading'); ?></h1>
                </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>