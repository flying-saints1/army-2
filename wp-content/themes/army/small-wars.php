<?php 

$main ="wars";

$page="pre";

$subPage = "small";


include('header.php'); ?>


<section class="operations-banner" style="background-image: url(./assets/img/post-independence-war-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">Small Wars</h1>
    </div>
</section>




<section class="operation-details" id="faq-section">
    <div class="container">
        <div class="row">

            <?php include('sidebar/post-indeep-sidebar.php'); ?>

            <div class="col-md-9">
                <div class="war-content">
                    <div class="content">
                        <img src="assets/img/small-war.png"  width="100%" class="img-fluid small-banner" 
                            alt="" loading="lazy">
                        <h4>Small Wars</h4>
                    </div>
                </div>


                <div id="about-war" class="accordion-details">
                    <h3>About war</h3>
                    
                    <div class="war-details">

                        <h4>Introduction</h4>
                        <p>Col C.E. Callwell defines ‘little’ or ‘small’ wars as ‘…campaigns other than those where both
                            the opposing sides consist of regular troops.’ He goes on to state: ‘The expression small
                            war has in reality no particular connection with the scale on which any campaign may be
                            carried out; it is simply used to denote, in default of a better, operations of regular
                            armies against irregular, or comparatively speaking irregular, forces.’ Small wars were also
                            a reflection of the times, where irregular opponents were often deemed as ‘savage’ or
                            ‘uncivilised’.</p>


                        <p>The Indian Army waged numerous small wars in the second half of the nineteenth century and
                            leading up to World War I, mainly against irregular armies. ‘It usually fought alongside
                            British army and Imperial Service units within small Army in India expeditionary forces.’ It
                            did so in Africa (Sudan) and in China. The stage of the Indian Army’s small wars was mostly
                            the 6,000 mile northern border of British India, running from the Makran desert by the
                            Arabian Sea, past the eastern marches of Afghanistan, across the southern face of the
                            Himalayas, and on to jungles of Assam and Burma.</p>

                        <p>The purposes of India’s small wars were normally to inflict short and sharp punishment for
                            acts against imperial authority—a raid on British territory, a murder of a government
                            official or a hostile detention of a diplomatic mission—and to facilitate an imposed treaty,
                            commercial or otherwise. The policy beneath them all was ultimately defensive, that is, to
                            safeguard imperial interests on and near the subcontinent. To that end, the necessity of
                            keeping an Indian army on standby to fight irregular or semi-regular enemies was never in
                            question at the India Office or in the Viceroy’s Council. </p>



                        <p> As the North-West Frontier was India’s principal small war theatre, the Indian Army fought a
                            particular type of small war there: ‘hill warfare’. Between 1851 and 1854, in particular,
                            the British in India undertook a number of minor campaigns and military expeditions on the
                            North-West Frontier, in which the Indian troops participated. These included operations
                            against the Waziris in the 1850s, expeditions in the vicinity of Peshawar and a 1853
                            expedition against the Jowaki Afridis. The Frontier, again, was the focus of the British
                            between 1855 and 1863.</p>
                        <p>According to George Morton-Jack, India’s other sorts of small wars—in India’s north-eastern
                            jungles, on the Tibetan Plateau or in African desert and bushland— were in many respects
                            similar to hill warfare. The ground was dense with natural obstacles; the irregular enemies
                            avoided gathering in large numbers for decisive battles and had no artillery; the Indian
                            forces spread out in regions that had few roads and no railways; and the battle casualties
                            were low.</p>
                        <p>Beginning in the 1860s, there were a number of campaigns in Africa. While the scene for small
                            wars in the early nineteenth century was the Indian subcontinent and Asia, it later shifted
                            to Africa. In the British expedition to Abyssinia (1868), conducted in retaliation to the
                            Absynnian emperor’s imprisoning of missionaries and British government representatives as
                            hostages, the task was given to the army of the Bombay Presidency. The force consisted of
                            13,000 British and Indian soldiers, 26,000 camp followers and over 40,000 animals, including
                            the elephants. Indian troops, such as the 23rd Punjab Pioneers, were present at the Battle
                            of Magdala.</p>
                        <p>Indian regiments had been sent by the British to China in the period leading up to the Opium
                            War. The war culminated with the Treaty of Nanking in 1842, and the opening up of Chinese
                            ports to the British.81 Half a century later, in the Boxer Rebellion (1900–01), Chinese
                            rebels besieged over 400 foreigners in Beijing’s Foreign Legation Quarter. Again, it was
                            troops from the Indian Army that played a crucial role in lifting the siege; and Indian
                            troops were the first to come to the aid of the besieged foreigners.</p>
                            
                        <p>On 4 August 1900, a relief force of more than 3,000 soldiers from Sikh and Punjabi regiments
                            left Tianjin, part of the larger eight-nation alliance that was dispatched to aid the
                            besieged quarter, where 11 countries had set up legations…‘[they crawled] through the
                            Imperial sewage canals’, undetected by the Boxers…Indian troops were also dispatched to
                            guard churches and Christian missionaries, the targets of the Boxer uprisings. </p>


                            <div class="war-2-map-2">
                                <img src="assets/img/47th-Sikhs-march.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                                <p style="color: #D90000;">47th Sikhs (now 5 Sikh) march through the streets of Tientsein in North China, 1906</p>
                            </div>

                        <p>Later, the British deployed Sikh soldiers as law enforcement officers in ports like Shanghai
                            and Hong Kong, where their trading companies had set up a large presence by the early
                            twentieth century.</p>


                        <p>In India’s north-east, such small wars/expeditions included the Lushai Expedition (1871–72)
                            into Assam; the 1888 Sikkim expedition (conducted to force Tibetan forces from the territory
                            of Sikkim); the Anglo-Manipur War (1891); and the Anglo-Burmese War, leading to the capture
                            of Rangoon on 14 April 1852.</p>
                        <p>In the Boer War in South Africa, the British succeeded in extending their control over the
                            Boer republics of Transvaal and Orange Free State. The British victory was aided in large
                            part by Indian troops. Not only did Indian troops serve in the war, but ‘military bases in
                            India were brought into service as detention camps for thou-sands of Boer prisoners of war
                            (PoWs).’ Unlike the small wars mentioned above, the opponents in this case were the white
                            Afrikaner (Dutch) settlers in South Africa. According to Andrew Whitehead, Britain and its
                            colonies deployed around half-a-million combatants in the war. </p>
                        <p>Thousands of officers and soldiers of the British Indian army were deployed in South Africa,
                            particularly cavalry units which were particularly suitable for the terrain. Although these
                            combatants were largely British, cavalry forces require a lot of support staff: veterinary
                            workers, grooms and farriers. It’s been estimated that as many as 9,000 Indians were part of
                            the military contingent that served in the Boer War.</p>
                    </div>
                    <!-- <div class="war-note">
                        <h2>Notes</h2>
                        <ol>
                            <li>C.E. Callwell, Small Wars: Their Principles and Practice, 3rd edition, London: General
                                Staff–War Office, 1906, p. 21.</li>
                            <li>Ibid. </li>
                            <li>George Morton-Jack, ‘Small Wars and Regular Warfare’, in The Indian Army on the Western
                                Front: India’s Expeditionary Force to France and Belgium in the First World War, New
                                York: Cambridge University Press, 2014, p. 43.</li>
                            <li>Ibid. </li>
                            <li>Ibid., p. 44.</li>
                            <li>Ibid., p. 45. </li>
                            <li>See Byron Farwell, Queen Victoria’s Little Wars, UK: Harper & Row, 1972, chapter 2.</li>
                            <li>Ananth Krishnan, ‘The Forgotten History of Indian Troops in China’, The Hindu, 12 July
                                2011.</li>
                            
                            <li>Ibid.</li>
                        </ol>
                    </div>
                </div> -->

                
                <div id="gallery" class="row image-gallery">
                    <div class="col-md-12">
                        <h3>Gallery</h3>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/gallery-1.png" class="img-fluid" alt="" loading="lazy">
                            <p>Line of communication in mountian </p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/gallery-2.png" class="img-fluid" alt="" loading="lazy">
                            <p>Evacuation of Refugee by IAF</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/gallery-3.png" class="img-fluid" alt="" loading="lazy">
                            <p>Construction of Bridge</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/gallery-4.png" class="img-fluid" alt="" loading="lazy">
                            <p>Army Engineers working at Jhanger Road </p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/gallery-5.png" class="img-fluid" alt="" loading="lazy">
                            <p>Army doctor's aid to civil</p>
                        </div>
                    </div>


                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/gallery-6.png" class="img-fluid" alt="" loading="lazy">
                            <p>An Army Officer helping a Kashmiri Boy in schooling at Rajaur</p>
                        </div>
                    </div>

                </div>


                <div id="video" class="row image-gallery">

                    <div class="col-md-12">
                        <h3>Video</h3>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="featured-video" >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">
                                <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="featured-video" >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">
                                <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="featured-video" >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">
                                <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
    </div>
</section>





<?php include('footer.php'); ?>