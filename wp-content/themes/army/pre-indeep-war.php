<?php

$main ="wars";
$page="post-indeep";

include('header.php');
?>



<section class="post-independence-war-banner"
        style="background-image: url(./assets/img/post-independence-war-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Post-Independent Wars</h1>
        </div>
    </section>

    <section class="post-independence-war">
        <div class="container">
        <div class="war">
            <div class="row justify">
                <div class="col-md-12 ">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                    <button class="nav-link tab-links active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Pre-Indep Wars</button>
                    </li>
                    <li class="nav-item" role="presentation">
                    <button class="nav-link tab-links" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Post-Indep Wars</button>
                    </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="row">
                        <div class="col-md-6 mt-4 mb-4">
                            <div class="card-detail">
                                <img src="assets/img/war-img1.jpg" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h2 class="card-head">World War I</h2>
                                    <p class="card-body-text">The immediate trigger for the war was the assassination of Archduke Franz Ferdinand, the heir apparent to the throne of the Austro-Hungarian Empire in Sarajevo...</p>
                                    <a href="#" class="read-more">Read More <img src="./assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mt-4 mb-4">
                            <div class="card-detail">
                                <img src="assets/img/war-img2.jpg" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h2 class="card-head">World War II</h2>
                                    <p class="card-body-text">World War I (1914–18) was the most destructive conflict the world had seen until World War II (1939–45). In World War I, though the fighting stopped in November 1918, the war formally...</p>
                                    <a href="#" class="read-more">Read More <img src="./assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mt-4 mb-4">
                            <div class="card-detail">
                                <img src="assets/img/war-img2.jpg" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h2 class="card-head">Small War</h2>
                                    <p class="card-body-text">Col C.E. Callwell defines ‘little’ or ‘small’ wars as ‘…campaigns other than those where both the opposing sides consist of regular troops.’ 1 He goes on to state: ‘The...</p>
                                    <a href="#" class="read-more">Read More <img src="./assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="row">

                        <div class="col-md-6 mt-4 mb-4">
                            <div class="card-detail">
                                <img src="assets/img/post-war1.jpg" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h2 class="card-head">1947-48 Kashmir Conflict</h2>
                                    <p class="card-body-text">The princely State of Jammu and Kashmir (J&K) had been brought under British paramountcy in 1846 via the Treaty of Amritsar, signed between the East India Company...</p>
                                    <a href="#" class="read-more">Read More <img src="./assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mt-4 mb-4">
                            <div class="card-detail">
                                <img src="assets/img/post-war2.jpg" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h2 class="card-head">1962 Sino-Indian Conflict </h2>
                                    <p class="card-body-text">The People’s Republic of China (China) annexed Tibet in 1950-51 and with that an independent India acquired a new neighbour. The two countries also shared a 4,000 km long </p>
                                    <a href="#" class="read-more">Read More <img src="./assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 mt-4 mb-4">
                            <div class="card-detail">
                                <img src="assets/img/post-war3.jpg" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h2 class="card-head">1965 India–Pakistan War</h2>
                                    <p class="card-body-text">In 1965, India was picking itself up from the fallout of the 1962 war; it was heavily dependent on dwindling US exports to battle an acute famine; and it was dealing with...</p>
                                    <a href="#" class="read-more">Read More <img src="./assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 mt-4 mb-4">
                            <div class="card-detail">
                                <img src="assets/img/post-war4.jpg" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h2 class="card-head">1971 India–Pakistan War</h2>
                                    <p class="card-body-text">The 1971 war was a manifestation of domestic political estrangement between the two halves of the Pakistani state. With little beyond a common religious belief and a deep...</p>
                                    <a href="#" class="read-more">Read More <img src="./assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>




<?php include('footer.php'); ?>