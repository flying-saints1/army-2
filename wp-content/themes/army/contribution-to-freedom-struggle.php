<?php


/*
*
* Template Name: contibution freedom template
*
*/


get_header(); ?>


<?php 

get_template_part('template-parts/banner-section');

?>

<?php if(have_rows('customs_and_traditions')): ?>
    <?php while(have_rows('customs_and_traditions')):  the_row(); ?>
        <section class="regiments-and-corps-detail">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9  col-sm-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                        <div><?php echo get_sub_field('content'); ?></div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

    <?php get_footer(); ?>