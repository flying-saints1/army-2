<?php  
/*
*
* Template Name: Contact US
*
*/
$main = "contact";

get_header(); 

?>

<?php 

get_template_part('template-parts/banner-section'); 

?>

<?php if(have_rows('contact_us_details')): ?>
    <?php while(have_rows('contact_us_details')):  the_row(); ?>
        <section class="contact-section">
            <div class="container">
                <div class="row heading">
                    <div class="col-md-12">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                    
                </div>
                <?php if(have_rows('address_and_telephone')): ?>
                    <div class="row contact-page">
                    <?php while(have_rows('address_and_telephone')):  the_row(); ?>
                    <div class="col-md-4">
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                        <p><?php echo get_sub_field('content'); ?></p>

                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>


                <div class="row contact-form">
                    <div class="col-md-12">
                        <?php echo do_shortcode(get_field('form_shotcode')); ?>
                    </div>
                </div>
            </div>
        </section>

    <?php endwhile; ?>
<?php endif; ?>

<section class="google-map">
    <?php echo get_field('map'); ?>
</section>



<?php get_footer();?>