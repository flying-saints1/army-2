<?php

/*
*
* Template Name: Regiment template
*
*/


get_header(); ?>


    <section class="regiments-and-corps-banner"
        style="background-image: url(../assets/img/regiments-and-corps-banner.jpg);">
        <div class="container">
        <h1 class="banner-content">Regimental System in the Indian Army</h1>
        </div>
    </section>

    <section class="regiments-and-corps-detail">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9  col-sm-12">

                    <p>The military in any modern state is a key component of the constituent elements of that state. In the case of a multi-ethnic, plural and democratic polity like India, its armed forces, and in particular the Indian Army, mirrors the diverse society that constitutes the nation.</p>

                    <p>Factors such as Revolution in Military Affairs (RMA), precision firepower, network-centric warfare, command, control, communications, computer, intelligence and interoperability (C4I2), military-industrial complex, space, nuclear weapons and means of delivery, missile, chemical and biological defence, nano-technologies and other emerging technologies are important ‘hard’ factors that are fundamental to the strength of a military organisation. Simultaneously, the presence and use of ‘soft’ or non-material factors such as the human resource, composition, and organisation are equally important.</p>

                    <p>The units of all the regiments and corps of the Indian Army are the building blocks of motivation and fighting spirit. It can be said that the ethos of the regimental system is the soul of the Indian Army. This glue that binds the spirt is best known and evolved trinity of Naam (Honour), Namak (Loyalty) and Nishan (espirit-de-corps). All ranks are devoted to their units and perform their duties in the unique Indic philosophy of Nishkam Karma and propelled by the tradition of the motto of the Indian National Army’s greeting of Jai Hind and the values of Itteefaq (unity), Etmad (confidence), and Qurbani (sacrifice).</p> 

                    <h2>Combat-Effectiveness and Structure</h2>
                    <p>War demands that an army must be capable of both maintaining defence as well as the ability to attack in all types of terrain and conditions. Military capacity implies firepower, manoeuvre, and sustaining of operations, thus all units of the army must be combat-effective. Combat effectiveness is defined as an assessment of the effectiveness of a unit or formation in fulfilling its mission in battle. It is a product of its organisation, state of weapons, equipment, training, morale and leadership. In other words, ‘the only sure measure of combat-effectiveness is the performance of unit in combat’.</p>

                    <p>Thus, in order to have the ‘right’ military capacity and effectiveness, an institution like the army needs to be organised with the appropriate manpower, equipped with various weapon systems, and supported by services which look after the adminis- tration and logistical functions which will enable it to perform its tactical role. In a nutshell, this military system and organisation can be divided into three categories: combat arms, combat support arms, and services. </p>

                    <h2>Combat Arms</h2>
                    <p>The combat arms comprise of the Infantry (basically a flexible foot-based organisation armed with integral weapons systems); the Mechanised Infantry in armoured personal carriers; and the Armoured Corps, with the main battle tanks. The President’s Body Guard is the only cavalry unit in the Indian Army, which largely has a ceremonial role; however, it may also be used, if need be, in war-like situation.</p>


                    <h2>Combat Support Arms</h2>
                    <p>The combat support arms in the Indian Army comprise of regiments and corps such as the Regiment of Artillery, the Corps of Army Air Defence, the Corps of Engineers, and the Corps of Signals.</p>


                    <h2>Services</h2>
                    <p>It is axiomatic that an army is like a society, self-contained in all respects. Thus, the famous saying that ‘an army marches on its stomach’. For the logistic and administrative needs the Indian Army has a variety of services, including Army Service Corps, Army Medical Corps, Army Dental Corps, Military Nursing Service, Army Ordnance Corps, Corps of Electronics and Mechanical Engineers, Remount and Veterinary Corps, Army Education Corps, Corps of Military Police, Pioneer Corps, Army Postal Service Corps, Defence Security Corps, Intelligence Corps, and the Judge Advocate General’s Department.</p>

                    <h2>Unit or Battalion Level Structure</h2>
                    <p>The building block of the Indian Army is based on a ‘unit’. For example, in an infantry battalion, or an armoured/artillery/engineer regiment (unit), three to four sub-units called ‘companies’, ‘squadrons’ or ‘batteries’ form one unit. The sub-units are com- manded by Lieutenant Colonels/Majors and the units are commanded by a Colonel. A Brigade has three to four such units under its command. A Division comprises three to four brigades along with units from other arms and services. A Corps has three or four divisions under its command. An Army Command, for example, the Western or Eastern or Northern Command, may comprise of two or three Corps. All army Field Commands come under the Army HQ.</p>



                    <p>The strength of an infantry battalion (unit) is about 700 to 800 (all ranks). A rifle company numbers about 120 soldiers. An artillery battery comprises six guns and over 100 soldiers (all ranks). Three batteries form a regiment (unit). An armoured regiment has 45 tanks, divided in three sabres and one HQ squadron, while a mechanised infantry battalion is equipped with 52 infantry combat vehicles.</p>

                    <p>Units of the Infantry, Armoured Corps, Artillery, Army Air Defence and Engineers are based on the regimental system, where an officer and other ranks (ORs) are assigned to a unit for their entire career. The unit moves on transfer en block. In other units, there is no permanent affiliation and personnel are rotated.</p>



                    
                    <h2>Class Regiments</h2>
                    <p>The term ‘class’, as used technically in the Indian Army, means a type of Indian recognised as distinct from others by the army authorities for purposes of recruitment and organisation. The basis of the distinction may be difference of race, language, religion, caste, domicile, or any two or more of these. ‘In August 1947 the Government of India decided as a matter of policy that all communal and class compositions should be eliminated from the Indian Army and that all Indian nationals should have equal opportunities of service. In January 1949 orders were officially issued to abolish recruitment by classes in all arms/Services. Administrative instructions were issued in February 1949 to implement this revised policy of [the] Government, by which Artillery, Engineers and Signals were brought more and more into mixed class basis.’</p>

                    <p>Later, the government made a policy decision to terminate the creation of new single-caste regiments. The policy of the Ministry of Defence (MoD) on the class composition of the Indian Army is: ‘[A]ttempts should be made to have “All Class” regiments to remove imbalances which has continued due to historic reasons.’ The units in the Armoured Corps and Infantry (Tables 1 and 2), and Artillery are of one class, mixed class, or an all-class composition.</p>

                    <div class="box-table">
                        <h3>Table 1 Composition of Armoured Regiments</h3>
                        <table class="table regimental-tb">
                            <thead>
                                <tr>
                                    <th scope="col">Serial No</th>
                                    <th scope="col">Unit(s)</th>
                                    <th scope="col">Class Composition</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>President’s Bodyguard</td>
                                    <td>Jats, Sikhs, Rajputs</td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>1 HORSE</td>
                                    <td>Rajputs, Jats, Sikhs</td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>2 LANCERS</td>
                                    <td>Rajputs, Jats</td>
                                </tr>
                                <tr>
                                    <th scope="row">4</th>
                                    <td>3 CAVALRY</td>
                                    <td>Rajputs, Jats, Sikhs</td>
                                </tr>
                                <tr>
                                    <th scope="row">5</th>
                                    <td>4 HORSE</td>
                                    <td>Sikhs, Dogras</td>
                                </tr>
                                <tr>
                                    <th scope="row">6</th>
                                    <td>7 CAVALRY</td>
                                    <td>Jats, Sikhs</td>
                                </tr>
                                <tr>
                                    <th scope="row">7</th>
                                    <td>8 CAVALRY</td>
                                    <td>Jats, Sikhs, Rajputs</td>
                                </tr>
                                <tr>
                                    <th scope="row">8</th>
                                    <td>9 HORSE</td>
                                    <td>Sikhs, Jats, Dogras</td>
                                </tr>
                                <tr>
                                    <th scope="row">9</th>
                                    <td>14 HORSE</td>
                                    <td>Sikhs, Dogras, Rajputs</td>
                                </tr>
                                <tr>
                                    <th scope="row">10</th>
                                    <td>16 CAVALRY</td>
                                    <td>South Indian Classes (SIC)</td>
                                </tr>
                                <tr>
                                    <th scope="row">11</th>
                                    <td>17 HORSE</td>
                                    <td>Rajput, Jats, Sikhs</td>
                                </tr>
                                <tr>
                                    <th scope="row">12</th>
                                    <td>18 CAVALRY</td>
                                    <td>Jats, Rajputs, Muslims</td>
                                </tr>
                                <tr>
                                    <th scope="row">13</th>
                                    <td>20 LANCERS</td>
                                    <td>Jats, Rajputs, Sikhs</td>
                                </tr>
                                <tr>
                                    <th scope="row">14</th>
                                    <td>CENTRAL INDIA HORSE</td>
                                    <td>Jats, Dogras, OIC</td>
                                </tr>
                                <tr>
                                    <th scope="row">15</th>
                                    <td>45 CAVALRY</td>
                                    <td>2/3 SIC, 1/3 Others Indian Classes (OIC)</td>
                                </tr>
                                <tr>
                                    <th scope="row">16</th>
                                    <td>61 CAVALRY</td>
                                    <td>Rajputs, Marathas, Kaimkhani</td>
                                </tr>
                                <tr>
                                    <th scope="row">17</th>
                                    <td>5, 6, 10, 11, 12, 13, 15, 19, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51 and 52 Armoured Regiment</td>
                                    <td>All India All Class</td>
                                </tr>
                                <tr>
                                    <th scope="row">18</th>
                                    <td>62 CAVALRY</td>
                                    <td>Dogras, Jats, Sikhs</td>
                                </tr>
                                <tr>
                                    <th scope="row">19</th>
                                    <td>63 and 64 CAVALRY</td>
                                    <td>Jats, Rajput, Sikhs</td>
                                </tr>
                                <tr>
                                    <th scope="row">20</th>
                                    <td>65 Armoured Regiment</td>
                                    <td>Jats, Dogras, Rajputs</td>
                                </tr>
                                <tr>
                                    <th scope="row">21</th>
                                    <td>66 Armoured Regiment</td>
                                    <td>Ahirs, Marathas, SIC</td>
                                </tr>
                                <tr>
                                    <th scope="row">22</th>
                                    <td>67 Armoured Regiment</td>
                                    <td>Sikhs, Jats, Dogras</td>
                                </tr>
                                <tr>
                                    <th scope="row">23</th>
                                    <td>68 Armoured Regiment</td>
                                    <td>Rajputs, Ahirs, Gujjars, Marathas</td>
                                </tr>

                                <tr>
                                    <th scope="row">24</th>
                                    <td>69 Armoured Regiment</td>
                                    <td>All India All Class</td>
                                </tr>

                                <tr>
                                    <th scope="row">25</th>
                                    <td>70 Armoured Regiment</td>
                                    <td>All India All Class</td>
                                </tr>

                                <tr>
                                    <th scope="row">26</th>
                                    <td>72 Armoured Regiment</td>
                                    <td>Jats, Dogras, Rajputs</td>
                                </tr>

                                <tr>
                                    <th scope="row">27</th>
                                    <td>71 Armoured Regiment</td>
                                    <td>Jats, Dogras, SIC</td>
                                </tr>

                                <tr>
                                    <th scope="row">28</th>
                                    <td>73 Armoured Regiment</td>
                                    <td>Sikhs, Rajputs, Kaimkhanis</td>
                                </tr>
                                <tr>
                                    <th scope="row">29</th>
                                    <td>76, 77, 81, 82, 83, 84, 85, 86, 87, 88, 89, and 90 Armoured Regiment</td>
                                    <td>All India All Class</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-table">
                        <h3>Table 2 Composition of Infantry Regiments</h3>
                        <table class="table regiments-tb">
                            <thead>
                                <tr>
                                    <th scope="col">Serial No</th>
                                    <th scope="col">Unit(s)</th>
                                    <th scope="col">Class Composition</th>
                                    <th scope="col">Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Brigade of the Guards</td>
                                    <td>Zonal</td>
                                    <td>Zonal example:<br>(a) Two battalions are composed of hill tribes<br><br>(b) One battalion is composed of South Indian Classes (SIC) </td>
                                   
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>Parachute Regiment and Special Forces </td>
                                    <td>Fixed class, zonal and All India</td>
                                     <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>Mechanised Infantry Regiment</td>
                                    <td>One class, fixed class and All India</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">4</th>
                                    <td>Punjab Regiment </td>
                                    <td>Sikhs, Dogras</td>
                                    <td>13, 14, 15 and 16 battalions are former State Forces</td>
                                </tr>
                                <tr>
                                    <th scope="row">5</th>
                                    <td>Madras Regiment</td>
                                    <td>South Indian Classes</td>
                                    <td>Including former State Forces of Travancore, Cochin and Mysore</td>
                                </tr>
                                <tr>
                                    <th scope="row">6</th>
                                    <td>GRENADIERS</td>
                                    <td>Rajputs, Khaimkhanis, Hindustani Mussalmans, Dogras, Gujjar, Ahir, Meena, Gujaratis, Jats</td>
                                    <td>Khaimkhani companies are in 5, 6, 8, 13, 16, 17, 21 battalions.<br> Hindustani Mussalman companies are in 4, 20 and 22 battalions. 13 GRENADIERS is a former State Force. </td>
                                </tr>
                                <tr>
                                    <th scope="row">7</th>
                                    <td>Maratha Light Infantry</td>
                                    <td>Marathas, All -India</td>
                                    <td>Some battalions have troops from all India. One has South Indian Classes</td>
                                </tr>
                                <tr>
                                    <th scope="row">8</th>
                                    <td>Rajputana Rifles</td>
                                    <td>Jats, Rajputs (including Gujaratis)</td>
                                    <td>3, 6 and 8 battalions have one coy each of Khemkhanis while the 9th battalion has one company of Gujaratis from Saurashtra </td>
                                </tr>
                                <tr>
                                    <th scope="row">9</th>
                                    <td>Rajput Regiment</td>
                                    <td>Rajputs, Gujjars, Brahmins,</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">10</th>
                                    <td>Jat Regiment</td>
                                    <td>Jats</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">11</th>
                                    <td>Sikh Regiment</td>
                                    <td>Sikhs</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">12</th>
                                    <td>Sikh Light Infantry</td>
                                    <td>Mazhabi and Ramdasia</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">13</th>
                                    <td>Dogra Regiment and Dogra Scouts	</td>
                                    <td>Dogras</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">14</th>
                                    <td>Garhwal Rifles and Garhwal Scouts</td>
                                    <td>Garhwalis</td>
                                    <td>Earlier only Rajputs were enrolled, but now any domicile of Garhwal are eligible for enrolment</td>
                                </tr>
                                <tr>
                                    <th scope="row">15</th>
                                    <td>Kumaon Regiment, Naga Regiment and Kumaon Scouts</td>
                                    <td>Kumaonis, Ahirs, Rajputs, North-East Region (NER)</td>
                                    <td>Kumaon Regiment-75% Kumaonis and 25% Ahirs.<br> Naga Regt- 50% Nagas and 50% Kumaonis </td>
                                </tr>
                                <tr>
                                    <th scope="row">16</th>
                                    <td>Assam Regiment</td>
                                    <td>Nagas, Kukis, Mizos, Lushias, Assamese, Kachari, other North-East Region (NER).	</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">17</th>
                                    <td>Bihar Regiment</td>
                                    <td>Any person from North Bihar, Adivasis from Chottanagpur Plateau (Jharkhand and Orissa) and 5 percent Other Indian Classes (OIC)</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">18</th>
                                    <td>The Mahar Regiment</td>
                                    <td>Mahars, All-India</td>
                                    <td>Five battalions are of pure Mahars, one battalion has troops from border regions and the balance units are on all India mixed class basis.</td>
                                </tr>
                                <tr>
                                    <th scope="row">19</th>
                                    <td>Jammu & Kashmir Rifles (J&K Rifles)- erstwhile State Forces</td>
                                    <td>Dogras, Gorkhas, Sikhs, Muslims</td>
                                    <td>13th battalion of JAK Rifles is composed of only Dogras hailing from Himachal Pradesh, Punjab and J&K. </td>
                                </tr>
                                <tr>
                                    <th scope="row">20</th>
                                    <td>Ladakh Scouts Regiment</td>
                                    <td>Buddhists, Muslims</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">21</th>
                                    <td>Jammu & Kashmir Light Infantry</td>
                                    <td>Muslims, Hindus, Sikhs (from J&K) except one unit having Dogras, Sikhs, Buddhists, Gorkhas, others. Units are organized with 50 percent Muslims and 50 percent ethnic groups of J&K </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">22</th>
                                    <td>1, 3, 4, 5, 8, 9 and 11 Gorkha Rifles	</td>
                                    <td>Gorkhas from Nepal and India including those from Kumaon and Garhwal In approximate ratio of 60:40. From 2020s Garhwalis and Kumaonese are also being enrolled </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                    <h2>Recruitment and Composition</h2>
                    <p>The Officers in all Corps are of an all-India mixed composition. Their selection and intake is based on merit through the Combined Defence Services Exam, conducted by the Union Public Service Commission (UPSC). There is no quota system. Women officers have been recruited into the army for some time now, mostly through the Short Service Commission. Women are now also getting Permanent Commissions, though not in the combat arms. In a small though significant move, the National Defence Academy (NDA), located at Khadakwasla in Pune, has an intake of 19 girl cadets beginning 2022—ten for the army, six for the air force, and three for the navy. Recruitment as Soldiers (Other than Officers) ‘The Armed Forces epitomise the ideals of service, sacrifice, patriotism and India’s composite culture. The recruitment to the Armed Forces is voluntary and every citizen of India, irrespective of his caste, class, religion and community is eligible for recruitment provided he meets laid down physical, and medical criteria. Recruitment is carried out according to the recruit able male population (RMP) of each state which is 10 per cent of the male population.’</p>
                    

                    <h2>Composition of Regiments</h2>

                    <p>As regards troop composition, the Armoured Corps and the Infantry, including the Mechanised Infantry, have single class units (all troops belonging to one ethnic or regional group), fixed class (companies or squadrons based on one class), or an all-India all class or mixed units (Tables 1 and 2). All attached elements to these one-class or fixed class units, like personnel from logistic units, clerks, and tradesmen are on all-India, all-class basis. Thus, even in a one-class or fixed class unit, one would find about 10 per cent of the troops other than that of the said class. Today, as a process of evolution, army units of combat arms are now identified not as those based on the martial races, but on ‘one-class’, ‘fixed’ or ‘mixed class’, or ‘all-India’ basis. This, essentially, is the Regimental System: for example, one-class units are those having troops from one region, such as the Sikhs, Sikh Light Infantry, Dogras, Garhwalis, Madras Regiment (South Indian Classes), and so on. Fixed class units are those that have one linguistic or ethnic group in each company. The Punjab Regiment, for instance, has companies consisting of Sikhs and Dogras, or an armoured unit like the 62nd Cavalry has a squadron each of Dogras, Jats and Sikhs. Since 1978, all new units in the Indian Army are mixed class units raised on an all-India basis.</p>
                    <br>
                    <p>In the combat support arms, the Regiment of Artillery has single class units as well as mixed units. The Corps of Army Air Defence, the Corps of Signals, the Army Aviation Corps, and Military Intelligence are of all-India mixed composition. The Corps of Engineers is based on regional groupings. It has three groups: Bengal, Bombay and Madras, reflecting the old colonial EIC armies in name. The Bengal Engineering Group units have about 20 per cent Sikhs, 20 per cent Rajputs, 20 per cent troops from Bihar, West Bengal, Orissa, and Ahirs from the Indo- Gangetic plains. The balance 20 per cent are recruited from the North-Eastern states.</p>
                    <br>
                    <p>The Bombay Engineering Group enrols 40 per cent Mazhbi and Ramdasia (M and R) Sikhs, 40 per cent Marathas, and 20 per cent others. The Madras Engineering Group recruits, as is the case with the Madras Regiment of the Infantry or one-class South Indian units of the Artillery Regiment, from among the South Indian Classes from Kerala, Tamil Nadu, Telangana/Andhra Pradesh and Karnataka.</p>

                    <p>The Services—Army Ordnance Corps, Army Service Corps, Corps of Electronic and Mechanical Engineers, Army Medical Corps, and so on—are of all-India mixed composition.</p>

                    <p>One unique feature is that officers and troops of the combat arms and the combat support arms (except the Corps of Signals) serve throughout their military career in the same unit of a regiment, barring instances of being posted out on extra-regimental employment. The same is applicable to the officers. The Corps of Signals and the other Services have a fixed tour of duty for all ranks of about three years. However, this does not mean that they lack the regimental spirit when serving with their units.</p>

                    <p>The regimental and class composition system of the Indian Army is one part of the organisational story. In its long, rich history, the army has adopted and evolved many customs and traditions that foster a feeling of cohesiveness and belonging to the organisation. Further, these act as foundations for the esprit-de-corps that is essential to bringing together people from different walks of life, different regions speaking a variety of tongues, and moulding them into a formidable fighting force. In the next sections, some important customs and traditions of the Indian Army are discussed followed by the contribution of the armed forces to the freedom struggle.</p>

                    <!-- <div class="note">
                        <span>Notes</span>
                        <ol>
                            <li>This section has made use of PK Gautam, Composition and Regimental System of the Indian Army: Continuity and Change, New Delhi: Institute for Defence Studies & Analysis/ Shipra Publications, 2008.</li>

                            <li>Anonymous, Leadership, Revised Edn, Shimla: Army Training Command, 2004, p. 11.</li>

                            <li>The Official Home Page of the Indian Army.</li>

                            <li>Reorganization of the Army and Air Forces in India, Report of a Committee set up by His Excellency the Commander-in-Chief in India, Volume One, Part IV, New Delhi: Government of India Press, 1945, p. 208. The committee was chaired by Lt Gen HBD Willcox and had three Brigadiers, one Air Commodore, and one Colonel as its members. Brigadier KM Cariappa, who later became the first Indian Chief of the Army Staff, was also a member.</li>

                            <li>AL Venkateswaran, Defence Organisation in India, New Delhi: Publication Division, Government of India, 1967, pp. 187–88.</li>

                            <li>Venkateswaran, Defence Organisation in India, n. 52, as quoted in Cohen, The Indian Armyn. 3.</li>

                            <li>Ministry of Defence, ‘Defence Force Levels, Manpower, Management and Policy’, Estimates Committee, 19th Report, Tenth Lok Sabha, 1992–1993, p. 50.</li>

                            <li>‘NDA Gearing up to Admit First Batch of 19 Girl Cadets, Training in “Gender Neutral” Manner’, The Indian Express, 22 March 2022.</li>

                            <li>See India 2005, New Delhi: Ministry of Information and Broadcasting, Government of India, Chapter on Defence, p. 180.</li>

                        </ol>           

                    </div> -->

                           
                </div>
            </div>
        </div>
    </section>

    <?php get_footer(); ?>