<?php 

/*
*
* Template Name: History of army template
*
*/



get_header(); ?>




<?php 

get_template_part('template-parts/banner-section');

?>

    <section class="regiments-and-corps-detail">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9  col-sm-12">
                    <h2>Historical Evolution</h2>

                    <h2>Origins</h2>
                    <p>The origins of the modern Indian Army can be traced to the armies of the East India Company (EIC), which was formed in the year 1600 via a royal charter that granted the company the rights to trade with India and East and South East Asia. At the time, the EIC was one of many such European companies jostling with each other for lucrative trade opportunities in these regions.</p>
                            
                     <p>By the end of the seventeenth century and in the early eighteenth century, it was primarily the British and the French that were engaged in attempts to gain control in the Indian Subcontinent, especially in the twilight of the once-mighty Mughal Empire. ‘The outbreak of war with France in 1744 (War of Austrian Succession 1742–1748)…led to a prolonged military and diplomatic conflict in which the English and the French vied to gain the upper hand in the Carnatic and Hyderabad by intervening or taking opposing sides in local dynastic rivalries.’ </p>

                     <p>Beginning with control of Bengal after the Battle of Plassey in 1757, the British had, by the end of the century, wrested control of the peninsula after the defeat of Tipu Sultan and the French in the Anglo-Carnatic Wars. Thus, at the end of the eighteenth century, the British were the dominant European power, with ‘two discoveries’ being ‘central to their ultimate success: first, a few disciplined British troops could defeat large but disorganized Indian armies; second, Indians themselves were amenable to discipline.’</p>

                     <p>The earliest British Forces in India were largely private, raised by the forerunners of the EIC. It was the French, however, rather than the British that were the first to train and use Indians in European methods of war. The lineage of the present Indian Army can be traced back to the mid-1700s, and the East India Company units which were raised by Major Stringer Lawrence5, who emulated French practices and ‘established regular companies which were led by British officers….The British recruited additional sepoy units until a total of 9,000 were in use in 1765, grouped into at least seven battalions, and divided into numerous companies of at least a hundred men.’ Indian battalions were formed into brigades, with Indian troops and British or European officers. Indian officers in the ranks of Subedar, assisted by a Jamadar, were allowed to command the companies, while power and responsibilities remained with British officers above them.</p>



                     <p>The three Presidencies of Bengal, Madras and Bombay had their own armies with their own areas of recruitment. In 1796, a reorganisation of these was undertaken, and the numbers are as given in Table 1. The 1796 reorganisation saw the formation of the Indian cavalry ‘into a cavalry brigade and declared a distinct service…’ The regimental system also came into existence with the infantry undergoing the formation of double battalion regiments; the existing 36 regiments of the Bengal Army, for example, were ‘formed into 12 regiments of two battalions each.’ Artillery battalions were also formed, which were composed mainly of European gunmen and Indian lascars and syces.</p>


                     <p>In the first half of the nineteenth century, the British consolidated their position in India further. Wars were fought with the Marathas, the Nepalese and the Sikh Empire led by Maharaja Ranjit Singh. The British adopted a policy of co-opting troops that the British had fought in these wars, after the cessation of conflict. For example, after the Anglo-Nepalese War/s in 1815, Gurkhas began to be recruited into the army. The Sikhs were similarly recruited after the Anglo-Sikh Wars and the death of Maharaja Ranjit Singh. Over time, Punjab became one of the key areas from which the British recruited troops into the army.</p>


                     <p>By mid-century, a large part of the subcontinent was, directly or indirectly, under British control. ‘The strength of the armies [too] had grown substantially [in the first half of the nineteenth century], and by early 1857 were as given in Table 2.</p>

                     <p>In this time, there was further reorganisation and change in areas of recruitment depending on particular situations. After the Vellore Mutiny in 1806 in the Madras Presidency, in which infantry battalions mutinied over the introduction of leather headgear, the British began to recruit in large numbers from the areas of Awadh and Bihar/Bengal, which is reflected in the size of the Bengal Army as given in Table 2. Patterns of recruitment, however, differed in the three armies. According to Cohen, the Bengal Army saw significant recruitment of ‘high-caste Hindus’ and before the Mutiny had ‘74 regular infantry battalions recruited mainly from Bihar, Oudh and Agra, with a few Gurkha units. Madras had 57 infantry battalions which were drawn entirely from the Presidency of Madras. Both in Madras and Bombay troops were of all castes, heterogeneously mixed together.’</p>


                     <div class="war-2-map-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Madras.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                        <p style="color: #D90000;">‘TROOPS OF INDIAN ARMY – MADRAS’</p>
                    </div>
                           
                     <span>Table 1  Presidency Armies around 1796</span>
                    <table class="presidency">
                    
                        <tbody>
                            <tr>
                                <th>European Soldiers (King’s and Company’s)</th>
                                <td>13,000</td>
                            </tr>

                            <tr>
                                <th>Indian Soldiers (Bengal Presidency)</th>
                                <td>24,000</td>
                            </tr>

                            <tr>
                                <th>Indian Soldiers (Madras Presidency)</th>
                                <td>24,000</td>
                            </tr>

                            <tr>
                                <th>Indian Soldiers (Bombay Presidency)</th>
                                <td>9,000</td>
                            </tr>
                        </tbody>
                    </table>


                    <p>Source: These figures are cited in Major General (Retd) Ashok K Verma, AVSM, ‘Genesis and Evolution’ in Major General (Retd) Ian Cardozo, AVSM, SM (ed.), Indian Army: A Brief History, New Delhi: USI, 2005, p. 14. The author cites a work by Lt FG Cardew, A Sketch of the Services of the Bengal Native Army, Calcutta, 1903, p. 67 as the source of these figures.</p>

                    <span>Table 2  Composition of Armies in the mid-Nineteenth Century </span>

                    <table class="presidency">
                                          
                        <tbody>
                            <tr>
                                <th>Composition</th>
                                <td><b> Number </b></td>
                            </tr>

                            <tr>
                                <th>Bengal Army</th>
                                <td>1,37,500</td>
                            </tr>

                            <tr>
                                <th>Additional local contingents</th>
                                <td>40,000</td>
                            </tr>

                            <tr>
                                <th>Madras Army</th>
                                <td>49,000</td>
                            </tr>

                            <tr>
                                <th>Armed Police</th>
                                <td>39,000</td>
                            </tr>

                            <tr>
                                <th>Bombay Army</th>
                                <td>48,000</td>
                            </tr>

                            <tr>
                                <th>Total</th>
                                <td>3,13,500</td>
                            </tr>

                            <tr>
                                <th>Total European Troops</th>
                                <td>38,000</td>
                            </tr>
                        </tbody>
                    </table>

                    <p>Source: Menezes, The Indian Army, p. 157 as cited in Verma, ‘Genesis and Evolution’, pp. 16–17.</p>
                   
                    <p>The uprising of 1857, also termed by some historians as the First War of Independence, began with the revolt of troops at Meerut on 10 May 1857, ostensibly owing to rumours that the cartridges being used in the new rifles were greased with cow and pig fat, both of which were anathema to Hindu and Muslim troops in the army. This led to the outbreak of other rebellions and mutinies across northern and central India, especially the region of the Indo-Gangetic plains, from where the Bengal Army was recruited. The uprising’s end in 1858 saw a number of administrative changes taking place in India. The rule of the EIC came to an end and the governance and administration of India was formally taken over by the British Crown.</p>


                    
                    <p>Reorganisation of the Presidency armies was but inevitable in the aftermath of 1857. The ‘distinction between the “royal troops” (loaned to the [EIC]) and the [EIC’s] own troops’ was to be done away with and these troops were to be amalgamated into the British Army as regiments. The Peel Commission in 1858 suggested that the Indian Army be composed of Indian troops, but the proportion of these troops to British troops was to be 2:1. The artillery was to remain in British hands. ‘The Commission envisaged an army of a maximum of 190,000 Indian troops and 80,000 British troops…after the reorganization was complete in 1863, the actual number were 135,000 Indian troops and 62,000 British troops.’ The proportion remained until the First World War in 1914. Recruitment patterns, too, became concentrated from those areas that had not rebelled in 1857. Punjab remained a major recruiting area for the army. The experience of 1857 meant that the British increasingly built in a number of checks and balances to ensure that such an event would not occur in the future.</p>


                  
                    <p>From the mid-1800s till World War I, the Indian Army was engaged in a number of conflicts in service of the British Empire. Increasingly, as the British expanded the frontiers of empire in the subcontinent and beyond, Indian soldiers of the army participated in a number of campaigns and expeditions in China, Abyssinia, on the North West Frontier, and in Afghanistan. According to Kaushik Roy and Gavin Rand, the ‘Indian Army… played a crucial role in projecting and asserting British imperial power beyond South Asia…’ The Indian Army allowed the British to consolidate the empire. While ‘Colonial recruiting drew from a pre-existing military labour market…the scale and duration of the Raj’s demand for military labour helped to incentivize and also to concretize the martial identities of various South Asian communities, including the Sikhs, Gurkhas and Pathans.’</p>

                    <div class="war-2-map-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Risaldar Major Ganda Singh 1.jpg"  height="400px" class="img-fluid" alt="" loading="lazy">
                        <p style="color: #D90000;">Risaldar Major Ganda Singh, Sardar Bahadur, OBI, IOM, 19th Fane’s Horse. Ganda Singh enlisted in 1852 in the 4 Punjab Cavalry. He saved the life of Sir Robert Sandeman near Lucknow in 1858 and later during the Second Opium War in 1860 was instrumental in saving the life of Col (later Maj Gen) Sir Charles McGregor, who subsequently founded the USI of India. Ganda Singh served on Lord Robert’s staff as his Indian ADC from 1889–1892, retiring from the army in 1894. <br><br><strong>Source: USI of India</strong></p>
                    </div>
                            
                    <p>By the end of the nineteenth century, there were tentative moves towards Indianization. The first move towards recruiting Indians as commissioned officers was taken in 1901 with the establishment of the Imperial Cadet Corps. Members of princely and noble families were imparted training for the purpose and the Royal Indian Military College (the present Rashtriya Indian Military College [RIMC]) was established for the purpose. A total of 78 Imperial Cadets were trained from various princely states until the disbandment of the Corps in 1914.</p>
                        
                    <p>In 1902-03, a further restructuring of the army took place under the leadership of General Horatio Herbert Kitchener, Commander in Chief of the Indian Army from 1902-1909, which remained in place till 1914. Briefly, the changes brought about by Kitchener included the following: One of [Kitchener’s] first reforms was to reequip the Indian Army with the Lee Enfield Mk.1 .303 magazine rifle. With an acknowledgment of the Indian Army’s requirements along the Afghan frontier, he also replaced the old 2.5-inch mountain gun with the new 2.75-inch mountain gun, a mobile gun that fired a heavier 10-pound shell 6,000 yards and broke up into five mule-transportable loads. In addition to equipment changes, Kitchener restructured the army into nine divisions divided between two commands, northern and southern, the latter having four divisions and the former five. He renumbered the regiments, eliminated defective ones, and dropped the old designations of Bombay, Calcutta, and Madras from the regimental titles.17 He initiated brigade- and division-scale weeklong training manoeuvres with senior commanders in attendance to supervise the training.</p>

                    <p>‘In 1906, during a meeting to coordinate the practices of the British Home Army and the Indian Army, many members of the Army Council favoured dividing the British and Continental armies into larger divisions similar to those in India since they were regarded as being more flexible and better suited to the British military’s imperial requirements.’ The Indian General Staff was established in 1910; by 1911, it was decided that military training would be standardised throughout the British Empire.</p>

                    <p>Kitchener’s tenure coincided with that of Lord Curzon as Viceroy and was witness to a bitter civil-military dispute. According to Cohen, Kitchener and Curzon ‘disagreed sharply’ over the military’s role in India. ‘[Both] attempted to reconcile civilian control with military efficiency, but proceeded to argue from very different assumptions…The overt point of disagreement…was the relative power and influence of the Military Member of the Viceroy’s Council.’ According to Cohen, the British legacy was ‘a theory of civil-military relations which placed great stress on “separate spheres” of military and civilian influence, while ultimate civilian control was always acknowledged.’ ‘The critical weakness in British civil-military relations in India [however] was the failure to set down firm guidelines defining the military sphere and the civilian sphere, and determining who is to indicate when the line between civil and military has shifted.’ The ramifications of this continue to be felt in independent India, in the higher defence organisation that evolved post- independence and in civil-military relations.</p>

                    <p>In the lead-up to World War I in 1914, the Indian Army numbered some 150,000 men. It was a professional, organised and disciplined force. Its mandate was limited to participating in colonial projects of the empire beyond the subcontinent, as has been mentioned above, and to police the North West Frontier, beyond which Britain was engaged in a ‘Great Game’ with the Russian Empire. Kitchener’s reforms in the first decade of the twentieth century had </p>
                    <p>‘…organized the army into nine divisions under two command headquarters. About a fifth of the army was cavalry…. The infantry were experts at mountain warfare…but lacked experience of modern war. [They] were equipped with rifles, a generation behind that of the British Army and had only two machine guns per battalion. There was no regimental system as at present… [and] had no artillery other than a few mountain batteries. There were pioneers and sappers but not much of the other arms and services. There was no motor transport and the army depended on animal transport for carrying its stores.’</p>

                    <p>The assassination of the Archduke Franz Ferdinand, the heir apparent to the throne of the Austro-Hungarian Empire in Sarajevo on 24 June 1914 was the trigger for the war. Among its long-term causes were a series of political and military alliances in the aftermath of the Napoleonic Wars in Europe, the emergence of a unified Germany that sought to race other European powers for colonies, a military arms and naval race between European powers, increasing industrialization and growing economic and military power, and political crises in the Balkans. Great Britain formally entered the war on 4 August 1914. And with it, so did its empire.</p>


                    <p>The Indian Army went to war in 1914, and served in Europe in France and Belgium, in Gallipoli, East Africa, Mesopotamia, Egypt and Palestine, Persia and in North China. By the end of the war, the army of 150,000 men had expanded to over 1 million men. The war thrust the Indian Army into the modern age and it distinguished itself over the course of the next four years. In the aftermath of the war, the army was reduced in size and went back to its policing duties in the subcontinent. In 1939, the Indian Army was once again called to war as World War II broke out in Europe. Over the next six years, the Indian Army fought in every major battle and campaign in Europe, North Africa, Burma and Indo-China. It was the largest volunteer army ever to fight in a war with over 2.5 million men recruited during the war. The impact of World War II on the Indian Army was significant. The war saw the ‘…rapid expansion of the army and led to the theory of “martial classes” being discarded; recruitment was thrown open to the whole of India. Recruitment to the officer cadre was also thrown open and Indianisation gained a fillip.’</p>
                   
                    <p>By 1947, a mere two years after the end of the war, India had moved towards gaining independence from Great Britain. But with independence came the partition of the subcontinent and the emergence of two successor states: India and Pakistan. Along with territorial partition and the subsequent movement of masses of people across new borders, the modern, professional and battle-experienced army would also be divided between the two new nations.</p>
                            
                      

                    
                    <!-- <div class="note">

                        <ul>

                            <li>Lt Gen SL Menezes, The Indian Army: From the Seventeenth to the Twenty-first Century, New Delhi: OUP, 1999, p. 7.</li>

                            <li>Stephen Cohen, The Indian Army: Its Contribution to the Development of a Nation, New Delhi: OUP, 2001 [1990], p. 5.</li>

                            <li>Cohen, The Indian Army, n. 3, p. 5.</li>

                            <li>Menezes, The Indian Army, n. 2, p. 7; Cohen, The Indian Army, n. 3, p. 8.</li>

                            <li>Cohen, The Indian Army, n. 3, p. 8.</li>

                            <li>These figures are cited in Major General Ashok K Verma, AVSM, ‘Genesis and Evolution’ in Major General Ian Cardozo, AVSM, SM (ed.), Indian Army: A Brief History, New Delhi: USI, 2005, p. 14. The author cites a work by Lt FG Cardew, A Sketch of the Services of the Bengal Native Army, Calcutta, 1903, p. 67 as the source of these figures.</li>

                            <li>Verma, ‘Genesis and Evolution’, n. 7, p. 14.</li>
                            
                            <li>Ibid.</li>
                            <li>Menezes, The Indian Army, n. 2, p. 157 as cited in Verma, ‘Genesis and Evolution’, n. 7, pp. 16–17. Cohen cites a figure of 214,000 troops (Indian sepoys) as of 1856. See Cohen, The Indian Army, n. 3, p. 32.</li>

                            <li>Cohen, The Indian Army, n. 3, p. 33.</li>

                            <li>Verma, ‘Genesis and Evolution’, n. 7, p. 18.</li>


                            <li>13. Ibid., p. 19.</li>

                            <li>Kaushik Roy and Gavin Rand, ‘Introduction’, in Kaushik Roy and Gavin Rand (eds), Culture, Conflict and the Military in Colonial South Asia, Abingdon and New York: Routledge, 2018, p. 1.</li>

                            <li>Ibid., p. 2.</li>

                            <li>Verma, ‘Genesis and Evolution’, n. 7, pp. 21. Also see Cohen, The Indian Army, n. 3, 2001 and Menezes, The Indian Army, n. 2, 1999.</li>

                            <li>Pradeep P. Barua, The State at War in South Asia, Lincoln and London: The University of Nebraska Press, 2005, p. 128.</li>

                            <li>Sir George Arthur, The Life of Lord Kitchener, Vol. 2, London: Macmillan, 1920, p. 168.</li>

                            <li>The Meeting of the Army Council, no. 278, June 21, 1906, National Archives, WOP 163/11, as cited in Pradeep Barua, The Late Colonial India Army: From the Afghan Wars to the Second World War, Lexington Books, 2011, Kindle edition.</li>

                            <li>Cohen, The Indian Army, n. 3, pp. 23, 25.</li>

                            <li>Ibid., p. 29.</li>

                            <li>Ibid. Emphasis in original.</li>

                            <li>Lt Gen VK Singh, PVSM, ‘The World Wars and Prelude to Independence’, in Major General, Ian Cardozo, AVSM, SM (ed.), Indian Army: A Brief History, New Delhi: USI, 2007, p. 25.</li>

                            <li>ingh, ‘The World Wars and Prelude to Independence’, n. 23, p. 25.</li>


                        </ul>           

                    </div> -->
    </section>


<?php get_footer(); ?>