<?php

$main ="brave";

$page ="winner";
$subPage="raj";

include('header.php') ?>


<section class="post-independence-war-banner"
    style="background-image: url(./assets/img/iconic-leaders-detail-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">Brave</h1>
    </div>
</section>

<section class="award">
    <div class="container">
        <div class="award-section">
            <div class="row table-data">
                <div class="col-md-12">
                    <div >
                        <form action="">
                            <table  class="border-table">
                                <tbody>
                                    <tr>
                                        <td class="table-img b-none" rowspan="7"><img src="assets/img/" class="img-fluid" alt="" loading="lazy"><br><span>SEPOY BEG RAJ<span></td>
                                        <td class="t-head">SERVICE NUMBER</td>
                                        <td class="t-para">4131140</td>
                                        
                                    </tr>
                                    <!-- <tr>
                                        <td >SERVICE NUMBER</td>
                                        <td >6574258</td>
                                    </tr> -->
                                    <tr>
                
                                        <td class="t-head">RANK <br>  (At the time of Award)</td>
                                        <td class="t-para">Sepoy </td>
                                    </tr>

                                    
                                    <tr>
                                        <td class="t-head">Name</td>
                                        <td class="t-para">Beg Raj</td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="t-head">SON OF</td>
                                        <td class="t-para"></td>
                                    </tr>

                                    <tr>
                                        <td class="t-head">RESIDENT OF (Village/District/State)/ DOMICILE</td>
                                        <td class="t-para"></td>
                                    </tr>

                                    <tr>
                                        <td class="t-head">UNIT/REGIMENT/CORPS</td>
                                        <td class="t-para">3 PARA (KUMAON) [now 3 PARA (SF)]</td>
                                    </tr>

                                    <tr>
                                        
                                        <td class="t-head">ARM/SERVICE</td>
                                        <td class="t-para"> India Army</td>
                                    </tr>

                                    <tr>
                                    <td class="t-none b-none"></td>
                                        <td class="t-head">DATE OF ENROLMENT/ COMMISSION</td>
                                        <td  class="t-para"></td>
                                    </tr>

                                    <tr>
                                    <td class="t-none b-none"></td>
                                        <td class="t-head">AWARD/ DATE OF ACTION</td>
                                        <td class="t-para">Ashok Chakra CL-III (Now Shaurya Chakra) / 10th April 1957</td>
                                    </tr>

                                    <tr>
                                        <td class="t-none b-none"></td>

                                        <td class="t-head">WAR/OPERATION/BATTLE</td>
                                        <td class="t-para">UN Mission in Gaza</td>
                                    </tr>

                                    <tr>
                                        <td class="t-none b-none"></td>
                                        <td class="t-head">OTHER AWARDS WITH DATE</td>
                                        <td class="t-para"></td>
                                    </tr>

                                    <tr>
                                        <td class="t-none b-none"></td>
                                        <td class="t-head">OTHER RECORDS</td>
                                        <td class="t-para"></td>
                                    </tr>

                                    <tr>
                                        <td class="t-none b-none"></td>
                                        <td class="t-head">VIDEOS</td>
                                        <td class="t-para"></td>
                                    </tr>

                                    <tr>
                                        <td class="t-none"></td>
                                        <td class="t-head">GAZETTE NOTIFICATION/ AUTHORITY</td>
                                        <td class="t-para"></td>
                                    </tr>

                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row content">
                <div class="col-md-12">
                    <div class="para">
                        <p>The UN Peacekeeping operation in Gaza was an attempt to resolve the 1956 Arab-Israel War. The creation of United Nations Emergency Force (UNEF) the first United Nations peacekeeping force, represented a significant innovation within the United Nations. It was not a peace-enforcement operation, as envisaged in Article 42 of the United Nations Charter, but a peacekeeping operation to be carried out with the consent and the cooperation of the parties to the conflict. It was armed, but the units were to use their weapons only in self-defense and even then, with utmost restraint. Its main functions were to supervise the withdrawal of the British, French, and Israeli forces. After the withdrawal was completed, UNEF’s responsibility was to act as buffer between the Egyptian and Israeli forces to provide impartial supervision of the ceasefire.  Two officers of Indian army, Major General P S Gyani and Brigadier I J Rikhye, were force commanders for this UN Mission. India was among the top contributors to the UNEF, from November 1956 to May 1967 and eleven Indian infantry battalions served successively with this force.</p>

                        <p>On 10 April 1957, a platoon of the 3 PARA (KUMAON) [Now 3 PARA (SF)] was engaged in clearing the minefields. The mine-clearing parties were attempting to clear two gaps simultaneously at a distance of 20 yards from each other. Sepoy Beg Raj was performing the duties of No.2 of ‘B’ lane clearing party. During the course of performance of his duty Sep Beg Raj heard a massive explosion from lane ‘A’. The visibility had become negligible due to the dense clouds of dust raised after the explosion. Sepoy Beg Raj displayed commendable courage overlooking his own safety, he rushed to the explosion site to help his comrades. He took the shortest route possible and picked a medical haversack on his way to assist the casualties. On his way, he survived two anti-personnel mine explosions.  </p>

                        
                        <p>Sepoy Beg Raj exemplified the essence of brotherhood. Unfortunately, by the time he could reach the site of the explosion his platoon mates were already dead. This valiant act of Sepoy Beg Raj made him an eternal source of inspiration for the generations to come. For his steadfastness and exemplary commitment to duty, Sepoy Beg Raj was awarded Ashoka Chakra CL-III (now Shaurya Chakra).</p>

                    </div>

                    <div class="citation">
                        <h2>Citation</h2>
                        <p>No. 4131140 Sepoy Beg Raj, 3 Para Regt. (Kumaon) (10th April, 1957)</p>

                        <p>On 10th April 1957, the pioneer Platoon of the third battalion Para Regiment was engaged in clearing minefields on the Gaza Strip while employed in the United Nations Emergency Force. The Commander ordered two mine clearing parties to clear two gaps simultaneously at a distance of 20 yards from each other. Sep. Beg Raj was No. 2 of the ‘B’ lane clearing party.</p>

                        <p>At about 1130 hours a big explosion was heard from lane ‘A’ and the entire area was engulfed in clouds of dust. Regardless of the risk involved, Sep Beg Raj rushed towards the place of the incident by the shortest route, with the medical haversack, which was lying in this lane. Sympathetic detonation blew up two more mines. Sep Beg Raj did not stop to take cover but dashed across the minefield hoping to render first aid to his comrades. He reached the site of the first explosion only to find his comrades already dead.</p>

                        <p>The selfless act of Sep Beg Raj in rushing to the rescue of his comrades in complete disregard of his own safety was in the best traditions of our Army. </p>
                    </div>
                </div>
            </div>

            <div class="row link-border">
                <div class="col-md-12">
                    <a href="https://www.gallantryawards.gov.in/awards">https://www.gallantryawards.gov.in/awards</a>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include('footer.php') ?>