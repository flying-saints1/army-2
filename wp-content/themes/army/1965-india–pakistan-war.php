<?php 

$main ="wars";
$page="post-indeep";
$subPage="1965";
$sidebar= "gallery";

include('header.php'); ?>


<section class="post-independence-war-banner"
    style="background-image: url(./assets/img/post-independence-war-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">1965 India–Pakistan War</h1>
    </div>
</section>


<section class="indep-war" id="faq-section">
    <div class="container">
        <div class="row">
            <?php include('sidebar/post-indeep-sidebar.php'); ?>

            <div class="col-md-9">
                <div class="content">
                    <img src="assets/img/small-war.png" width="100%"  class="img-fluid res-image" alt="" loading="lazy">
                    <h3 class="sino-war-text"> 1965 India–Pakistan War</h3>
                </div>


                <div id="about" class="accordion-details">
                    <h3>About war</h3>

                    <div class="war-details">
                        
                        <p>In 1965, India was picking itself up from the fallout of the 1962 war; it was heavily dependent on dwindling US exports to battle an acute famine; and it was undergoing a political transition in the aftermath of Pandit Nehru’s death. Not only did Rawalpindi perceive India to be weak after the defeat of 1962, but an additional driver was the concern that the Americans were supplying India with light arms for troops on the eastern front, especially after the Nehru–Kennedy understanding post-1962. The Soviets, Czechs and the Romanians had offered to send India heavy machinery, including tanks, as they found common ground with India after having fallen out with China since the Sino-Soviet split. On its part, Pakistan had also been consolidating its special friendship with China.</p>

                        <p>The then Pakistani Army Chief General Mohammad Musa’s book, My Version, provides a detailed account of the planning process. Musa says that ‘the Kashmir Cell’—a highly secretive group put together by the Pakistani Army in early 1964, which reported directly to the President—had by now concluded that ‘it was time for Pakistan to take some overt action for reviving the Kashmir issue and “defreezing”, what from Pakistan’s point of view was a dishearteningly quiet and stable situation in Kashmir.’ With these calculations, Pakistan prepared a four-phased strategy for a war with India:</p>

                        <ol >
                            <li style="font-size: 18px;">1.	a probing encounter in some place of Pakistan’s choosing;</li>
                            <li style="font-size: 18px;">2.	an all-out disguised invasion of Kashmir by the Pakistani Army for ‘guerrilla warfare’ camouflaged as a revolt by the local population;</li>
                            <li style="font-size: 18px;">3.	a full-scale assault by the Pakistani Army in the Chhamb sector in Kashmir to cut off the Indian supply line in J&K; and</li>
                            <li style="font-size: 18px;">4.	a massive lightening armoured attack to capture Amritsar and as much of other Indian territory as possible, to be exchanged eventually for Kashmir.</li>
                        </ol>

                        <p>To test the status of Indian capabilities, in April 1965, Pakistan first launched a limited offensive in the Rann of Kutch, while simultaneously planning for a largescale infiltration in Kashmir, namely, ‘Operation Gibraltar’.</p>


                    </div>

                   
                <div id="rann" class="accordion-details">
                    <h3>The Rann of Kutch Incident: The <br> Smokescreen </h3> 
                    <div class="war-details">
                        <h4>Terrain</h4>
                        <p>The Rann of Kutch is a salt waste stretching west to east, north of Kutch. It separates Kutch from Sind (Pakistan) in the north. This area is divided into two parts: the Great Rann to the north of Kutch, about 250 km from east to west and 125 km from north to south; and the Little Rann to the south-east and south of Kutch. It is generally flooded from June to October by the sea waters pushed into it by the south-west monsoon, turning it into a salt lake. In other seasons, it is a desert, flat, firm and bare except for a few islands, sprinkled with scanty herbage. When the monsoon abates, the waters recede, leaving behind a morass. In northern Rann, there are few villages with sparse population, having little contact with the rest of the region. Even the area around Bhuj, the main town of Kutch, is barren, with oppressive heat and forbidding dust most of the year. In 1965, in terms of connectivity, there was a road from Bhuj to Khavda in Kutch, near the border of the Rann. Northward from Khavda, a track led to Diplo (in Pakistan) across the Rann. The northern portion of the Rann was easily accessible from the side of Sind (Pakistan). There was also a road along the border on the Pakistani side, which was motorable during the dry season.</p>

                   

                    <h4>Historical Background</h4>

                    <p>The British annexed Sind in 1843. This necessitated determination of boundary between Sind, the British territory, and Kutch. As there was no immediate urgency, the border remained undemarcated, though some effort in this direction was initiated by planting pillars. During the Partition in 1947, Sind went to Pakistan by virtue of its Muslim majority, whereas Kutch, with its predominantly Hindu population, remained in India. Even before the Partition, the Sind authorities had been disputing the rights of the Kutch government over the entire Rann. In 1938, they had laid claim over half of it. On 14 July 1948, Pakistan wrote to India that the Sind–Kutch ‘boundary was still in dispute and must be settled’. On 10 August 1949, India cited evidence to prove that there was no dispute in this regard. In a note of 9 April 1956, Pakistan put forth the argument:</p>


                    <p>It has been emphasised that the Rann is Dead Sea. According to the international practice also seas are divided equally between the states situated on either side of it. The same principle appears to have been followed while settling the dispute over the little Rann between the Princely states of Morvi and Kutch. The Pakistani claim to the northern portion of the Rann upto Dharamsala is, therefore, supported not only by possession and exercise of authority, but also by international practice and precedent.</p>

                    <p>They also cited maps and letters of the British province of Sind to prove that they had never ceased to claim rights over portions of the Rann. India, in turn, pointed out that the Survey of India maps issued during the British period marked the entire Rann as part of Kutch. The exchange of notes notwithstanding, Pakistan’s intrusions into the Rann and military movements near the border started as early as 1949. To contain this, Indian authorities in Kutch also demanded the strengthening of patrol arrangements. </p>

                    <p>By the end of 1955, Pakistan had built a motor track leading to Gulmamad Talavadi, a small depression in the Rann, and reports said that contingents of Sind Reserve Police (SRP) (about 50 in each) had been stationed at Vingi and Baliari or Jat Tarai, opposite Chhad Bet area. Men of the Sind Reserve Police (SRP) were reported to be patrolling Bet area and instigating Pakistani cattle owners not to pay the grazing fees to the Indian authorities. India lodged a protest with the Government of Pakistan on 12 January 1956, but it did not have any effect. On 17 February 1956, an Indian police patrol found that Pakistani armed personnel had intruded up to Gulmamad Talavadi and taken up position there. They fired some shots in retaliation. Against this background, Pakistan decided on Chhad Bet area for a probing encounter.</p>
                    
                    </div>
                    <div class="war-details">
                        <h4>The Intrusion </h4>
                        <p>In January–February 1965, trouble started in Kanjarkot area. Kanjarkot was a fort in ruins, about 1.5 km south of the Pakistan border on the north-western fringe of the Rann of Kutch. The area south of Kanjarkot was a flat plain and the area to the north consisted of a series of parallel sand dunes. Communications in the area favoured Pakistanis rather than the Indians. The Indian administrative base at Bhuj was about 177 km south of the border; the Bhuj–Khavda road was liable to breaches during rains; and the desert track ran from Khavda to the border. Meanwhile, the position was quite different on the Pakistani side. Badin, a sizeable town, was only 30 km from the border. It had a large airfield, capable of handling all types of fighter and transport aircraft. There was also a road running from Badin to Maro, and further east to Nagar Parkar. Thus, the maintenance of the border posts was much easier for the Pakistanis than the Indians.</p>

                        <p>Pakistan started building up forces in the Kanjarkot area from about the third week of January 1965. On 10 February, this was reported by a routine Indian patrol. Pakistan deployed one company of Indus Rangers at Kanjarkot, 400 Indus Rangers at Rahim-Ki-Bazar and one wing of Indus Rangers along the border. Two battalions of Indus Rangers were kept in reserve at Hyderabad (Sind) and Chhor. Against this, the Indians had five companies of State Reserve Police: two at Vigokot; one at Karim Shahi; and two at Chhad Bet. However, this force was not adequate to dislodge the Pakistani encroachment.</p>


                        <p>On 21 February 1965, the Maharashtra and Gujarat Area Commander, Maj Gen PC Gupta, issued Operation Instruction No. 1 (Operation Kabadi) to Brig SSM Pahalajani, Commander 31 Infantry Brigade Group, to capture Kanjarkot. Headquarters 31 Infantry Brigade Group reached Bhuj on 27 February. The troops of 17 RAJ RIF were already in Bhuj. A parachute battalion was also placed at 24 hours’ notice for move to the area. Pakistan retaliated by ordering Maj Gen Tikka Khan, Commander 8 Division, to assume operational command of the Indus Rangers and to take measures for effective retaliation. He, in turn, alerted 51 Brigade Group to move forward, if necessary. The Pakistanis reinforced the posts at Rahim-Ki-Bazar and Kanjarkot to company strength, and also deployed mortars and machine guns around Kanjarkot. On 13 March, India’s 2 Central Reserve Police (CRP) battalion established Sardar Post, 4,600 m to the south-west of Kanjarkot. This effectively blocked the Pakistani route of ingress. The location was a featureless mudflat with some scrub-covered area around.</p>
                    </div>

                    
                    <div class="war-details">
                        <h4>Desert Hawk-I  </h4>
                        <p>To forestall a likely attack on Kanjarkot by the Indian forces, a pre-emptive operation by 51 Brigade, code-named ‘Operation Desert Hawk’, was formulated by Pakistan’s 8 Infantry Division. The Commander of the 51 Brigade decided to attack Sardar Post with two battalions and the attack was launched on 9 April 1965 at 0100 Hours. After facing the first Pakistan attack boldly, many CRP personnel withdrew to Vigokot. At 1600 Hours, the Pakistan artillery fired shells of coloured smoke, which prompted the rest of the CRP personnel to leave Sardar Post. Soon afterwards, Brig Pahalajani took over the command from Lt Col K Sundarji and asked 1 MAHAR to withdraw from Sardar Post.</p>


                        <p>In February 1965, the area of Kanjarkot (Rann of Kutch) had been seized by a company of Pakistan’s Indus Rangers. Surprised by the move, India assigned the responsibility for removing this encroachment to 31 Infantry Brigade Group. Pakistan retaliated by moving in their 8 Infantry Division under Maj Gen Tikka Khan, and also inducting armour into the area. There were series of land battles between the two forces in March–April 1965, with Pakistan capturing Biar Bet on 27 April 1965.</p>

                        <p>In May 1965, Pakistani troops, bolstered by their success in Kutch, regularly violated the ceasefire along the Jammu and Kashmir border. The road to Leh was frequently targeted by them from their positions at Point 13,260 and Black Rocks. Gen</p>

                        <p>J.N. Chaudhuri and the Western Army commander Lt Gen Harbaksh Singh decided to get proactive in the Kargil sector. Brigadier V.K. Ghai tasked the 4 RAJPUT for capturing Point 13,260 and peaks 1 and 2 of the Black Rocks feature; it was decided to launch the offensive on the night of 16 May.</p>

                        <p>Walking in single file, the Indian troops managed to reach within 90m of the Pakistani bunker but faced heavy mortar attack on being spotted. The Rajputs, however, shot back and captured the peaks.</p>

                        <p>However, the heights captured by the Indians had to be returned on June 30 after the United Nations assured the safety of the Srinagar-Leh road. The assurance did not hold for long as the Pakistanis started firing again from Point 13,260, which had been returned to them in June.</p>

                        <p>The Army then decided that the heights had to be recaptured, and this time the task was assigned to 17 PUNJAB. The commanding officer of the unit was asked to be prepared to capture Point 13,260 complex along with Black Rock peaks and Saddle features.</p>

                        <p>They noticed that the Pakistanis were patrolling the complex only in the daytime. The unit was asked to capture the features by early morning of 15 August. The operation commenced at midnight on 14 August and complete radio silence was maintained to prevent detection. Three platoons under Major Balwant Singh managed to reach the three features and captured the heights without suffering a single casualty.</p>

                        <p>However, due to the intervention of Prime Minister Harold Wilson of the United Kingdom (UK), a ceasefire came into effect on 1 July 1965 and both sides agreed to restore status quo as on 1 January 1965. Though the Kutch affair ended visibly on a peaceful note, in reality it was only a prelude to another effort by Pakistan to annex J&K, which was to unfold later.</p>

                    </div>


                    <div class="war-details">
                        <h4>Operation Ablaze (India) </h4>
                        <p>Operation Ablaze is an important interlude between the operations in the Rann of Kutch and Operation Gibraltar by Pakistan, leading to a full-scale war. This operation included the measures taken by the Indian Army along the western border, in May–June 1965, following Pakistan’s attack in the Rann of Kutch. All the formations in Punjab, mainly under 11 Corps, were ordered to move and deploy in their battle locations. It appeared that India was ready for offensive operations across the international border in Punjab if the situation in the Rann of Kutch escalated further. Following the ceasefire in the Rann of Kutch, Operation Ablaze was called off and troops were ordered to return to their peace locations.</p>

                       
                    </div>

                    <!-- <div class="war-details">
                        <h4>Opening Attacks: 20 October 1962</h4>
                        <p>The Chinese built up a numerical superiority of about 10:1 and fire power superiority of 7:1 before launching any major attack. They also blocked any route of withdrawal available to the defenders. As they had been in a state of armed co-existence with the Indian posts for a long time, they had intimate knowledge of the Indian strength and dispositions. The attacks commenced at first light with intense artillery bombardment for about 30 minutes and the battle was over in two to three hours. No Indian soldier could come back to tell the story. For instance, at Point 5270, 14 J&K Militia suffered 42 killed and 20 wounded/taken prisoners of war (POW). Similarly, at the Galwan post, 5 JAT suffered 36 killed and 32 wounded/taken POW.</p>

                        <p>By 24 Oct, all the forward posts established in the Chip Chap and Nacho Chu River Valleys were either captured or withdrawn. DBO was also abandoned. Similarly, in the Galwan Valley, the Chinese had managed to remove all the six Indian posts established on the northern and southern banks of Galwan River in about 72 hours. Some of these posts were withdrawn even without being attacked as these were neither, tactically viable nor, logistically sustainable.</p>
                    </div>


                    <div class="war-details">
                        <h4>Battles on the Pangong Tso: 21-23 October 1962</h4>
                        <p>The next area of interest for the Chinese was the Pangong Tso, which was under Indian control right up to the Khurnak Fort. Here, Chinese were operating on a very slender margin of superiority as far as the strength of attacking troops was concerned. Hence, for subsequent operations they created ad-hoc Task Forces, especially in the area of Pangong Tso.</p>

                        <p>By 24 Oct, all the forward posts established in the Chip Chap and Nacho Chu River Valleys were either captured or withdrawn. DBO was also abandoned. Similarly, in the Galwan Valley, the Chinese had managed to remove all the six Indian posts established on the northern and southern banks of Galwan River in about 72 hours. Some of these posts were withdrawn even without being attacked as these were neither, tactically viable nor, logistically sustainable.</p>
                    </div> -->



                    <div class="war-details">
                        <h4>Operation Gibraltar (Pakistan)</h4>
                        <p>Even before the ceasefire took effect in the Rann of Kutch, Pakistan had been planning much bigger things in J&K commencing in May 1965.</p>

                        <p>Operation Gibraltar was conceptualised with the objective of creating large-scale disturbances in J&K. The plan was to send Pakistani soldiers and Razakars into J&K disguised as guerrillas. The first phase was to create a shock wave by launching raids on selected targets, thereby preparing the ground for a civil uprising due to the chaos and consternation that would be caused in the state. The second was the fusion of the civil uprising with the infiltration operation. This would compel India to take major political and military steps to tackle the situation, presenting a picture to the world that a problem exists in Kashmir. The armed fighters were meant to tie down Indian forces in a protracted guerrilla war in a manner similar to the war in Vietnam. Maj Gen Akhtar Malik, GOC 12 Division, in charge of the overall operation, felt that Operation Gibraltar would not succeed as a stand-alone operation. Accordingly, he combined it with Operation Grand Slam, which was the plan for an armoured thrust across the ceasefire line (CFL), with a view to capture Akhnoor, thereby threatening the line of supplies from India to Srinagar. The plan, in principle, was accepted by Field Marshal Ayub Khan in May 1965. He was convinced that military action was required to resolve the Kashmir issue. The operation was planned in the beginning of August.</p>

                        <p>Pakistan commenced infiltration of the Gibraltar Force in small groups across the CFL in J&K between 1 August and 5 August 1965. The areas covered were as follows: right from Kargil in the north to Chhamb in the south. Once inside the Valley, they were to mingle unnoticed among the crowds celebrating the festival of Pir Dastgir Sahib on 8 August 1965. Then, they were to engineer an armed uprising and in the process, capture the radio station, Srinagar airfield and some vital installations. Following this, a ‘Revolutionary Council’ was to proclaim itself as the lawful government and broadcast an appeal for recognition and assistance from all countries, especially Pakistan. This was to be the signal for the Pakistan Army to move in for the kill.</p>

                        <p>Lt Gen Harbaksh Singh, the Western Army Commander, read the infiltration campaign correctly. He analysed that apart from eliminating the infiltrators, there was a need to undertake offensive operations at Haji Pir and across the Kishanganga to counter the infiltration. In the ensuing operations, the Indian Army captured critical areas in Haji Pir, the Kishanganga Bulge and Kargil, which helped to contain the infiltration and enhanced the morale of troops in the ensuing operations against Pakistan which commenced on 1 September.</p>

                        <p>Operation Gibraltar came to naught mainly for two reasons. First, the internal conditions in the Valley were not ripe for an insurrection of the kind that Pakistan had hoped to incite. Second, the quick and firm response by India took Pakistan by surprise and thwarted their well-laid-out plans. The operation lost its sting as Pakistan’s ensuing Operation Grand Slam was delayed to 1 September, by which time most of the infiltrators were killed or exfiltrated. India achieved its objectives by capturing areas which dominated the infiltration routes, thereby causing the termination of the entire process.</p>
                    </div>



                    <div class="war-details">
                        <h4>The War: Chhamb–Jaurian Sector </h4>
                        <p>Indian offensive operations in Pakistan-occupied Kashmir provoked Pakistan to move into Chhamb and Jaurian. Pakistan launched Operation Grand Slam on 1 September 1965. It was launched across the southern-most portion of the CFL and was aimed at Akhnoor, thus isolating Indian positions in Naushera, Rajauri and Poonch. Thereafter, an armoured thrust could be developed towards Jammu, the capture of which would have severed all land communications to J&K. This would place Pakistan in a position to dictate terms to India. The surprise offensive made good progress initially, but lost its momentum by 3 September 1965. In this battle the ation of Maj Bhaskar Raj of 20 LANCERS with his squadron of AMX-13 tanks stood out. The Indian Air Force (IAF) played a major role in blunting the Pakistani offensive. The situation in this sector was stabilised by about 10 September and Pakistan remained in control of the areas up to and including Jaurian.</p>

                        
                    </div>



                    <div class="war-details">
                        <h4>Indian Counter-offensives  </h4>

                        <p>As mentioned earlier, the Pakistani offensive in Chhamb–Jaurian sector took the Indians by surprise. India decided to conduct an offensive across the international border against Lahore and Sialkot to relieve the pressure on this sector. The ‘go-ahead’ for an all-out military response had been given by the Emergency Committee of the Cabinet, chaired by Prime Minister Lal Bahadur Shastri, on 3 September 1965.</p>

                        
                    </div>



                    <div class="war-details">
                        <h4>Indian 11 Corps Offensive (Operation Riddle)</h4>

                        <p>The main aim of the offensive was to secure the line of Ichhogil Canal, thus posing a threat to Lahore. The offensives commenced at 0500 Hours on 6 September 1965. The 15 Infantry Division advanced along Amritsar–Lahore axis; 7 Infantry Division advanced along Khalra–Barki axis; and 4 Mountain Division advanced on Khemkaran–Kasur axis. However, a major portion of 4 Mountain Division was also required to occupy defences in Khemkaran area to counter an expected offensive by Pakistani armoured division in the area.</p>

                        <p>The Indian offensives, due to the surprise factor, made good progress initially. Along the Grand Trunk Road axis, 3 JAT, under 15 Infantry Division, crossed the Ichhogil Canal at Dograi and reached Batapore on the outskirts of Lahore. However, this initial success was not exploited and by about the end of day one, the Indian offensive had lost its momentum. Though Ichhogil canal was breached at a few points, no crossings could be secured and held. The situation became particularly grave in the 4 Mountain Division sector where the enemy launched its counter-offensive by 1 Armoured Division on 8 September towards Jandiala Guru, Beas Bridge and Harike. The 4 Mountain Division had to occupy a hastily prepared division-defended sector in area Asal Uttar in the face of this powerful offensive. The defended sector had approximately three-and-a-half battalions of infantry (18 RAJ RIF, 1/9 GORKHA RIFLES less two companies, 4 GRENADIERS and 9 JAK RIF), along with units of 2 Independent Armoured Brigade (3 CAVALRY, 8 CAVALRY less a squadron and 9 HORSE) and artillery support. Fierce battles raged on 8–9 September, in which Indian forces repelled repeated attacks by Pakistan’s armour and infantry. More importantly, personal intervention by the Army Commander, Lt Gen Harbaksh Singh, ensured that there were no withdrawals; Indian troops held firm and the situation was stabilised by 10 September with heavy losses to Pakistan armour. The area later became the graveyard of Pakistani armour. By the time of ceasefire, Indian forces were on the home bank (eastern) of Ichhogil Canal, while Pakistan remained in control of a small enclave in the Khemkaran sector. After the launch of Indian 1 Corps in the Sialkot sector, Pakistan was forced to pull out and divert an armoured brigade from this sector to Sialkot sector to stabilise the situation there.</p>
                        
                    </div>



                    <div class="war-details">
                        <h4>Indian 1 Corps Offensive in Sialkot Sector (Operation Nepal) </h4>

                        <p>As part of the overall strategy, India decided to launch its main counter-offensive by the newly raised 1 Corps in the Sialkot sector in order to relieve pressure in the Chhamb sector, as also to degrade Pakistan’s fighting potential. The offensive was launched on the night of 7–8 September 1965 on a frontage from Suchetgarh in the west to Degh Nadi in the east. The Corps was commanded by Lt Gen PO Dunn and had 1 Armoured Division, 6 Mountain Division, 14 Infantry Division and 26 Infantry Division under its command.</p>

                        <p>The 1 Corps was tasked to secure areas of Bhagowal–Phillaurah–Chawinda–Cross Roads (Badiana) with a view to advance towards the Marala Ravi Link (MRL) Canal and eventually, to the line of Dhalliwali–Wuhilam–Daska–Mandhali. The corps offensive was opposed by Pakistan’s 1 Corps comprising 6 Armoured Division and 15 Infantry Division. Initially, the area into which Indian 1 Corps was launched was the area of responsibility of Pakistan’s 15 Infantry Division, with its HQ in Sialkot. However, as the operations progressed, the responsibility to counter the Indian offensive was given to Pakistan’s 6 Armoured Division, with 24 Infantry Brigade of 15 Infantry Division placed under its command. So, it was a contest between the Indian 1 Corps, which was on the offensive, and the Pakistani 6 Armoured Division with attached troops, who were defending. The battles were fought in general area of Bhagowal–Badiana–Pasrur–Zafarwal.</p>

                        <p>Indian 1 Corps offensive had achieved complete surprise but after initial success, operations came to a near standstill and there was no major operational activity on 9 and 10 September. This gave adequate time to Pakistan’s 6 Armoured Division to organise its defences. The 1 Armoured Division of India commenced its attack on Phillaurah at first light on 10 September from an unexpected direction. It was a well-coordinated attack between armour, lorried infantry and artillery. The high points of the battle were the manoeuvres by 4 Horse and 17 Horse, who were operating on both flanks of Phillaurah. They manoeuvred to draw out enemy armour (11 Cavalry) deployed at Phillaurah. In the tank versus tank battles that ensued that morning, Pakistan lost 28 tanks. After the capture of Phillaurah, there was a lull in the battle on 12 and 13 September, till Indian 1 Corps renewed its offensive for capture of Chawinda on 14 September. Two attempts were made (the first one by 1 Armoured Division and the second one by 6 Mountain Division), but both were a failure. There was lack of coordination between armour and infantry, and air support had not been planned. By now, Pakistan had also moved an armoured brigade from Khemkaran into this sector and the forces, especially armour, were almost evenly matched. The formations came out of these battles badly battered and there followed a stalemate which lasted for the rest of the war.</p>

                        
                    </div>



                    <div class="war-details">
                        <h4>Operations in Barmer Sector (Desert Sector) </h4>

                        <p>In order to contain the Pakistani offensive in Chhamb sector as also pre-empt any Pakistani offensive in the desert sector, India launched a limited offensive by 30 Infantry Brigade of 11 Infantry Division on axis Barmer–Hyderabad (Sind). By the time of the ceasefire, India had captured 390 sq km of Pakistani territory in this sector, while Pakistan held on to the Indian border post at Munabao.</p>

                        
                    </div>


                    <div class="war-details">
                        <h4>War in the Air</h4>

                        <p>On the eve of the war, Pakistan Air Force (PAF) consisted of about 17 squadrons having a total of 260 aircraft of all types approximately. As against this, the IAF had an overall strength of 26 squadrons, with a total holding of about 460 combat aircraft of all types. A few of these squadrons had also to be deployed in the east. The Indian edge in numbers was more than offset by Pakistan’s qualitative superiority. Thus, the two air forces in the west were almost evenly matched.</p>

                        <p>As the war opened with a strong ground offensive by Pakistan in the Chhamb sector on 1 September 1965, the IAF swung into action in support of the army almost immediately and continued to do so day after day, thus playing a major role in blunting the Pakistani offensive. As a reaction to the Pakistani offensive in the Chhamb sector, India launched its counter-offensive by 11 Corps in Lahore sector on 6 September 1965. In accordance with their war plans, PAF responded by a pre-emptive strike on Indian air bases at Pathankot, Adampur, Halwara and Amritsar, commencing at 1740 Hours (time on target).</p>

                        <p>From 7 September onwards, the air war intensified and both the air forces were carrying out air operations in support of ground forces, including counter air, air interdiction and air defence. The IAF carried out a number of attacks against Pakistani air bases, railway yards, logistics installations, radar sites and even a train carrying tanks. A number of search and destroy and tactical reconnaissance missions were also flown by the IAF. However, neither side could establish a favourable air situation over the battle area. Thus, overall, neither side could claim air superiority.</p>

                        
                    </div>



                    <div class="war-details">
                        <h4>War at Sea  </h4>

                        <p>There were no naval battles fought during 1965. The Indian Navy was no doubt larger and stronger, but it had a huge coastline and many island territories to defend. India had only one fleet and at the time of the commencement of hostilities, the fleet was operating off the east coast in the Bay of Bengal. The fleet was able to reach Bombay only by about 8–9 September 1965.</p>

                        <p>During the night of 7–8 September, some Pakistani naval ships, disguised as merchant ships, carried out bombing of the minor port of Dwarka but with little damage as most of the shells landed on the beach.</p>

                        
                    </div>



                    <div class="war-details">
                        <h4>Casualties and Territory Captured</h4>

                        <p>The total number of casualties suffered by the Indian Armed Forces during the Indo- Pak War of 1965 and in the subsequent ceasefire violations amounted to 2,862 killed and 8,617 wounded. According to the Defence Minister of Pakistan, 1,033 Pakistanis were killed during the war. The Indian official sources, however, state that about 5,800 Pakistanis were killed. In the war, India captured approximately 1,920 sq km of Pakistani territory. Pakistan, on the other hand, occupied 540 sq km of Indian territory.</p>

                        
                    </div>





                    <div class="war-details">
                        <h4>Ceasefire and Tashkent Accord </h4>

                        <p>It was not a comfortable situation for the US and the UK that Pakistan, an ally of the US and a member of both South East Asia Treaty Organization (SEATO) and Central Treaty Organization (CENTO), decided to join hands with China against India. In the communist bloc, differences between China and the Soviet Union had also emerged. It did not welcome the growth of China’s influence in South Asia, particularly at the cost of non-aligned India where it had significant economic and political stakes. Hence, persistent efforts were being made by Moscow, Washington, London and UN Security Council for immediate cessation of hostilities.</p>

                        <p>On 4 September 1965, the Security Council adopted a resolution calling for an immediate ceasefire in Kashmir. When neither India nor Pakistan responded to this, the Security Council requested UN Secretary-General to visit both the countries in an effort to bring about a ceasefire. As a result, U. Thant visited Pakistan on 9 September and left for Delhi on 12 September 1965, but without any positive outcome.</p>

                        <p>Meanwhile, China, showing solidarity with Pakistan, issued a warning note to India on 8 September 1965 that blamed the Indian government of expansionist action against it and said that India must dismantle all aggressive military structures it had illegally built on China–Sikkim boundary or else bear the responsibility for all consequences. China issued another ultimatum to India on 16 September and reiterated its support to Pakistan on Kashmir.</p>

                        <p>On 19 September, China issued another note to India, reiterating its allegations and putting off the time limit set in its note of 16 September to ‘before midnight of 22 Sep 1965’. The superpowers and other members of the Security Council were keenly watching China. They wanted India and Pakistan to accept a ceasefire proposal before China’s second ultimatum expired on 22 September. On 20 September, the Security Council adopted a resolution calling upon India and Pakistan to accept a ceasefire on Wednesday, 22 September 1965, at 0700 Hours GMT (1230 Hours IST). The ceasefire was accepted by both the countries and it became effective with effect from 0330 Hours (IST) on 23 September 1965. Soviet Premier Alexei Kosygin, through his earlier letter of 17 September, had invited the Prime Minister of India and President of Pakistan to hold peace talks in Tashkent, to which both sides eventually agreed. Before going to Tashkent, India made it clear that Kashmir could not be made an issue for discussion, while Pakistan said that it would not sign a ‘no war pact’ unless Kashmir problem was resolved. However, the Soviet Premier persuaded them to agree to a joint declaration. The agreement was signed at 1630 Hours on 10 January 1966.</p>


                        <p>The Tashkent Agreement satisfied the leaders of both the countries to some extent. India’s takeaway from Tashkent was that the conference did not make any reference to Kashmir and Pakistan was happy that it got back from India the territories that it had lost to India during the war, especially the Haji Pir Pass.</p>

                        
                    </div>





                    <div class="war-details">
                        <h4>Assessment of the War</h4>

                        <p>The 1965 war was a continuation of the 1947–48 war launched by Pakistan for the annexation of J&K. Pakistan had not given up its dream of annexing the complete state of J&K by any means and was constantly planning for the same.</p>

                        <p>The next opportunity that came Pakistan’s way was the Sino-Indian War of 1962, which was a setback for India. However, Pakistan was restrained from taking advantage of the situation by the Western powers. In the middle of the crisis, India appealed for Western help, especially from the US and the UK. Both the countries readily agreed to help provided India and Pakistan could resolve the Kashmir dispute. Six rounds of talks were held between December 1962 and May 1963, but failed to produce any results. It was a win-win situation for Pakistan. From then on, India faced a two-front scenario. It also emboldened Pakistan to plan its next move for annexation of J&K, which unfolded in the form of Operation Gibraltar commencing on 5 August 1965.</p>


                        <p>There was a change in political leadership in India following the passing away of Pandit Nehru on 27 May 1964. In Pakistan’s perception, India had a weak political leadership and the Indian Army was still recovering from the 1962 debacle. Pakistan, on the other hand, had a new friend in China, which was India’s enemy. In addition, Pakistan’s armed forces had developed a degree of renewed confidence with newly inducted American arms and equipment, which was further reinforced by the outcome of the confrontation in the Rann of Kutch. Thus, Pakistan considered this to be the appropriate time to decide the issue of J&K by force of arms. This, then, was Pakistan’s calculation and thinking which set the stage for the 22-day war that followed.</p>


                        <p>In the Rann of Kutch episode, India’s response left a lot to be desired. When Pakistan Army crossed the international border, the Indian reaction need not have remained confined to the Rann of Kutch alone. Military prudence lies in avoiding headlong collision and adopting a strategy of ‘indirect approach’. Even a limited reaction in the neighbouring desert sector would have produced the desired results and sent a strong signal to Pakistan, that is, not to take India for granted. Also, not using own air force in offensive role when the adversary has crossed the international border does not make strategic sense, especially after a similar experience in 1962 against the Chinese.</p>

                        <p>Soon after the ceasefire in the Rann of Kutch, based on the assurances by the UN Secretary-General of Pakistan’s good behaviour, India called off Operation Ablaze, returned the territories captured in J&K and moved its army formations to their peace locations. Pakistan, meantime, was giving finishing touches to the launch of Operation Gibraltar. Two aspects stand out: at the politico-diplomatic level, India was gullible; and it did not obtain enough intelligence about Pakistan’s intentions and capabilities.</p>

                        <p>War erupted in all its fury on 1 September 1965. It is ironic that prior to the 1962 war, most of the Indian Army was deployed on the western border with Pakistan. So, in the war with China, troops from the plains of Punjab and the deserts of Rajasthan had to be moved straight to the Himalayas to face the Chinese. Now, the reverse happened. Mountain divisions from the northern borders with China had to be moved post-haste to the plains of Punjab and J&K to face Pakistani armour; for example, 4th, 6th and the 23rd Mountain Divisions. There was no comprehensive defence plan taking into account a threat from China and Pakistan, both proven adversaries at that time.</p>

                        <p>On the western sector, once Pakistan had played its hand in Chhamb–Jaurian sector, Indian reaction was swift and decisive. The army was given a free hand to launch its offensives across the international border, as it deemed necessary. However, close air support of ground operations had not been planned and coordinated; hence, the same was not forthcoming to the desired extent. This was not due to any inhibition on the part of the IAF, but due to lack of joint planning and coordination. However, when the crisis had developed on 1 September in Chhamb sector, the IAF, in spite of no prior warning, responded magnificently and was not found wanting.</p>


                        <p>The main Indian counter-offensives had been launched in Punjab (Lahore sector) and in J&K (Sialkot sector). Indian formations in Punjab, which had to move from their peace locations straight into attack, without going into concentration areas where ‘marrying up’ could take place, were at a great disadvantage. As armour and infantry coordination was poor, initial success on the Ichhogil Canal could not be exploited. The 4 Mountain Division offensive ran headlong into a Pakistani offensive by 1 Armoured Division. As a result, a serious crisis developed on 4 Mountain Division front in the Khemkaran sector on 8 September. Had it not been for the tenacity and cool courage displayed by the Western Army Commander, Lt Gen Harbaksh Singh, to stay put and fight, the results could have been disastrous.</p>

                        <p>The main Indian Poonch was delivered by 1 Corps in the Sialkot sector. Though complete strategic surprise was achieved, the corps was not able to make much progress after initial successes. Pakistani defences in the area of operations were quite thin. However, there was complete inactivity by Indian 1 Corps on 9 and 10 September for almost 48 Hours, which gave Pakistan time to rehash its defensive plans and by 10 September, Pakistan’s 6 Armoured Division had set itself up for a mobile defence.</p>

                        <p>In hindsight, instead of getting involved in clearing strong points like Phillaurah/ Chawinda, Indian 1 Corps could have isolated these and projected the armoured division in the area of Badiana–Chawinda–Pasrur. Also, contacting the MRL Canal (in accordance with the mission assigned) in an early time frame would have denied freedom of movement to the Pakistani forces. The infantry divisions, with their integral armoured regiments, could have cleared the axis of maintenance. In the event, as time passed, Pakistan was able to build up its strength by pulling in forces from Chhamb sector and moving an additional armoured brigade from its 1 Armoured Division in the Khemkaran sector. Thus, by about 12 September, there was near parity of forces, especially armour. The initial advantage of the attacker and the momentum of attack had both been lost. There was no cohesive plan for a corps battle, with each division fighting its own battle. Instead of a battle of manoeuvre which should have found the corps leaning on the MRL Canal, it got involved in a battle of attrition for the capture of Chawinda, in which it did not succeed till the very end.</p>

                        <p>At an operational level, the Pakistani attempts to break through the Indian defences at Asal Uttar on 8–9 September and the Indian attacks for the capture of Chawinda, on 14 September and, again, on 18–19 September, were a mirror image of each other. Both were failures, though for different reasons. The Pakistan Army tried to break through Indian positions at Asal Uttar using 1 Armoured Division and suffered very heavy tank casualties. Meanwhile, the Indian 1 Corps tried to capture Chawinda, a strongly held position, using an armoured division and a mountain division, but separately without proper coordination, resulting in heavy attrition to the attacking troops. Both sides (in both cases) knew that they were defending the most politically sensitive territory; the ramifications of a breakthrough by the enemy were serious; and there were no further reserves available to restore an adverse situation. At the end of it, both sides had suffered so much attrition that they were unable to mount any fresh major offensive operations.</p>

                        <p>After the 1962 war, India had undertaken a large-scale expansion of its armed forces, but the emphasis was on the Chinese border. The capabilities required for the western front, including the navy and the air force, were still neglected. The result was that when the 1965 war came, India’s mountain divisions were fighting in the plains and the tank fleet, except for four regiments of Centurions and two regiments of AMX-13 light tanks, consisted of obsolete Shermans which were no match for Patton M-47/48s. The air force, which could have played a major role in the plains and deserts, was superior in numbers but was at a disadvantage qualitatively. Indian Navy, in spite of remonstrations by the Chief of Naval Staff, was not given any offensive task, just like the air force in 1962. In retrospect, one can say that in war, a nation ought to bring to bear all its military strength in a synergised manner and more so, in a short duration war.</p>

                        <p>There was generally a policy of drift towards the security of the CFL and the international border in J&K. Hence, security threats were not seen in advance and the armed forces had to react only after a threat had manifested itself. The Rann of Kutch episode and Operation Gibraltar are prime examples.</p>

                        <p>If a country goes to war, it is important that the politico-military objectives are well defined and once defined, these must be pursued vigorously. In 1965, neither of the above happened. We simply reacted to the situations created by Pakistan. Having gone to war, we accepted a ceasefire without achieving any worthwhile politicomilitary objectives. If the Rann of Kutch had been taken as a warning, a number of measures to upgrade our military capability could have been taken in the ensuing months. For instance, we could have raised a few more infantry divisions for the western front.</p>

                        <p>Lastly and most importantly, should India have accepted the ceasefire when it did? It has been argued that Indian stocks of all types of ammunition had run extremely low, hence there was no alternative but to accept the ceasefire. Post-conflict enquiries have revealed that in overall terms, only about 14–20 per cent of the Indian Army’s ammunition stocks had been used up; and large dumps of unused ammunition were lying in sectors where fighting had not been intensive. On the other hand, in Pakistan, nearly 80 per cent of ammunition stocks had actually been expended. It may be conjectured that it was in India’s interest to continue the war. However, hindsight and an objective assessment would indicate that India was fighting a war that was thrust upon it; furthermore, it was unable to create a favourable diplomatic environment internationally to prolong fighting, unlike the situation in 1971.</p>

                        <p>However, it can be said without hesitation that the Indian soldiers (sailors and airmen included) had, once again, given their best in service of the nation. The soldiers were brave, resolute and steadfast in battle, and units sought to achieve their missions relentlessly against heavy odds. The nation too rallied and stood behind its soldiers in a magnificent manner. In the final analysis, the outcome of the war is a tribute to the Indian soldier.</p>

                        
                    </div>


                    <!-- <div class="note">

                        <p>Notes</p>
                        <ol>
                            <li>This section has been adapted from the official history of the 1965 India-Pakistan War. See BC Chakravorty, History of the Indo-Pak War, 1965, New Delhi: History Division, Ministry of Defence, 1992.</li>

                            <li>Gen Mohammad Musa HJ, My Version: India Pakistan War 1965, Peace Publications, 2018.</li>

                            <li>Chakravorty, History of the Indo-Pak War, 1965, n. 1, Chapter 2, p. 17.</li>

                            <li>Ibid</li>

                            <li>Lt Gen Harbakhsh Singh, VrC, In the Line of Duty, New Delhi: Lancer Publishers and Distributors, 2000, pp. 239–41.</li>

                            <li>For a detailed description see Maj Gen PJS Sandhu, ‘1965 Indo-Pak War – A Critical Appraisal’, Journal of the United Service Institution of India, Vol. CXLV, No. 601, July- September 2015.</li>

                            <li>Ibid.</li>

                        </ol>           

                    </div> -->

                <!-- <div id="gallery" class="row image-gallery">
                    <div class="col-md-12">
                        <h3>Gallery</h3>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery1.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery2.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-2</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery3.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-3</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery4.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-4</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery5.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-5</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery6.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-6</p>
                        </div>
                    </div>
                </div> -->


                <!-- <div id="video" class="row image-gallery">

                    <div class="col-md-12">
                        <h3>Video</h3>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="featured-video" >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">
                                <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="featured-video" >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">
                                <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="featured-video" >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">
                                <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>

                    
                </div> -->

 
            </div>  
        </div>
    </div>
</section>


<?php include('footer.php'); ?>