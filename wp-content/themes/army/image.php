<?php
/*
*
* Template Name: Image Gallery
*
*/
$main = "gallery";

$page = "image";

get_header(); ?>

<section class="iconic-leaders-detail-banner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/blog-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">Image</h1>
    </div>
</section>



<section id="images">
    <div class="container">
        <div class="row height d-flex  align-items-center marg">
            <div class="col-md-12">
                <div class="search">
                    <i class="fa fa-search icon"></i>
                    <input type="text" id="myinput" class="form-control" placeholder="Search">
                    <button class="btn btn-primary">Search</button>
                </div>
            </div>
        </div>

        <div class="row filter">

            <!-- <div class="col-md-2 fill-wrapper">
                <div class="btn-filter">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/filter.png" class="img-fluid" alt="" loading="lazy">
                    <p>&nbsp; Filter Image By</p>
                </div>
            </div>
            <div class="col-md-4 drp-wrapper">
                <div class="btn-group">
                    <button class="btn filter-btn  btn-lg" type="button" data-bs-toggle="dropdown" aria-expanded="false">All  <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/down-arrow.png" class="img-fluid" alt="" loading="lazy"></button>

                    <ul class="dropdown-menu drop">
                        <li><a href="#">War</a></li>
                        <li><a href="#">Post</a></li>
                        <li><a href="#">Pre</a></li>
                    </ul>
                </div>
            </div> -->


            <!-- <div class="filter">
                <div class="btn-group">
                    <img src="assets/img/icons/filter.png" class="img-fluid" alt="" loading="lazy">Filter Image By
                    <button class="btn  btn-lg" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        All <img src="assets/img/icons/down-arrow.png" alt="" loading="lazy">
                    </button>
                    <ul class="dropdown-menu drop">
                        <li><a href=""> War</a></li>
                        <li><a href="">Post</a></li>
                        <li><a href="">Pre</a></li>
                    </ul>
                </div>
            </div> -->
        </div>

        <div class="row">


            <div class="col-md-4 main">
                <div class="image-main img-pop " big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Chattri.png">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Chattri.png" class="img-fluid img-fluid card-img-top " alt="" loading="lazy">
                    <div class=" card-body img-body">
                        <p>Many thousands of Indian soldiers wounded on the Western Front passed through Brighton & Hove Hospitals during the Great War. The Chattri Memorial was erected on the exact size where 53 Hindu and Sikh soldiers were cremated according to their faith, while 21 Muslim soldiers were buried in nearby Woking </p>
                        <p><strong>Source: CWGC.</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Obelisk.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Obelisk.png" class="img-fluid img-fluid card-img-top" alt="" loading="lazy">
                    <div class=" card-body img-body">
                        <p>National War Memorial Obelisk, New Delhi </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Alamein.png">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Alamein.png" class="img-fluid img-fluid card-img-top" alt="" loading="lazy">
                    <div class=" card-body img-body">
                        <p>Of the more than 11,000 casualties commemorated on the Alamein Memorial, over 1,800 of them served with Indian forces, who fought in campaigns across North Africa and the Eastern Mediterranean </p>
                        <p><strong>Source: CWGC</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/ashoka.png">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/ashoka.png" class="img-fluid img-fluid card-img-top" alt="" loading="lazy">
                    <div class=" card-body img-body">
                        <p>Ashok Chakra </p>
                        <p><strong>Source: USI of India.</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Dras.png">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Dras.png" class="img-fluid img-fluid card-img-top" alt="" loading="lazy">
                    <div class=" card-body img-body">
                        <p>Kargil War Memorial at Dras, UT of Kashmir</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/india-gate.png">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/india-gate.png" class="img-fluid img-fluid card-img-top" alt="" loading="lazy">
                    <div class=" card-body img-body">
                        <p>The India Gate, an iconic symbol of New Delhi, was built in 1931 to commemorate Indian battle casualties during World War I as well as the Third Anglo-Afghan War. Out of over 83,000 Indians who laid down their lives, the monument bears 13,516 names.</p>
                        <p><strong>Source: CWGC.</strong></p>
                    </div>
                </div>
            </div>



            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Order.png">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Order.png" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy" style="object-fit: cover;background-position: -3% 80%;object-position: 50% 98%;">
                    <div class=" card-body img-body">
                        <p class="heading">Originally instituted in 1937 as the ‘Order of Merit’ the Indian Order of Merit (IOM) is the oldest gallantry award in the Commonwealth.</p>
                        <p><strong>Source: USI of India</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Kapurthala.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Kapurthala.png" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                    <div class=" card-body img-body">
                        <p class="heading">This memorial commemorates the war dead of the Kapurthala Imperial Service Infantry who served in East Africa between 1914 and 1917, and also in the Third Afghan War </p>
                        <p><strong>Source: Sukhjit Singh</strong></p>
                    </div>
                </div>
            </div>



            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Kirkee.png">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Kirkee.png" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                    <div class=" card-body img-body">
                        <p class="heading">The Kirkee Memorial commemorates 1,805 casualties of World War I </p>
                        <p><strong>Source: CWGC</strong></p>
                    </div>
                </div>
            </div>



            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Kirti-chakra.png">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Kirti-chakra.png" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy" style="object-fit: cover;background-position: -3% 80%;object-position: 50% 100%;">
                    <div class=" card-body img-body">
                        <p class="heading"> Kirti Chakra</p>
                        <p><strong>Source: USI of India</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php // echo $currentDomain;  ?>/assets/img/gallery/Kohima.png">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Kohima.png" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                    <div class=" card-body img-body">
                        <p class="heading">The Kohima War Cemetery, Kohima, Nagaland </p>
                        <p><strong>Source: CWGC</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php // echo $currentDomain;  ?>/assets/img/gallery/Chakra.png">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Chakra.png" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Maha Vir Chakra </p>
                        <p><strong>Source: USI of India</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Neuve.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Neuve.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Located near Neuve Chapelle in an area that was central to the operations of the Indian Corps during 1914 and 1915, the memorial commemorates around 4,700 Indian war dead who fell in France and Flanders </p>
                        <p><strong>Source: CWGC</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php // echo $currentDomain;  ?>/assets/img/gallery/Victoria.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Victoria.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Instituted in 1858 by Queen Victoria to commemorate acts of gallantry in the Crimean War. Indians became eligible to receive the VC in 1911 </p>
                        <p><strong>Source: USI of India.</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Murti.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Murti.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Teen Murti in New Delhi commemorates the 15th Imperial Service Cavalry Brigade that fought in the Middle East during the First World War. It also serves as the Indian Cavalry Memorial</p>
                        <p><strong>Source: CR Elderton</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Shaurya-chakra.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Shaurya-chakra.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Shaurya Chakra </p>
                        <p><strong>Source: Shaurya Chakra </strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/vir-chakra.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/vir-chakra.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Shaurya Chakra </p>
                        <p><strong>Source: USI of India</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/param-vir-Chakra.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/param-vir-Chakra.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Param Vir Chakra </p>
                        <p><strong>Source: USI of India</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Rangoon.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Rangoon.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading"><strong>The Rangoon </strong><br>
                        Memorial bears the names of more than 26,000 World War II servicemen—almost 20,000 of them served with the Indian forces. The Taukyan Cremation Memorial commemorates a further 1,000 World War II casualties whose remains were cremated in accordance with their faith. The majority of these men were casualties of the campaign in Burma (now Myanmar).
                        </p>
                        <p><strong>Source: CWGC</strong></p>
                    </div>
                </div>
            </div>




            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Parakram.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Parakram.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">A mortar platoon training in the desert during Operation Parakram 2001 </p>
                        <p><strong>Source: Army HQ.</strong></p>
                    </div>
                </div>
            </div>




            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/manoeuvre.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/manoeuvre.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Tanks executing a manoeuvre Operation Parakram, 2001 </p>
                        <p><strong>Source: Capt Suresh Sharma.</strong></p>
                    </div>
                </div>
            </div>




            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Siachen.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Siachen.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">View of a post in the Siachen region </p>
                        <p><strong>Source: Army HQ</strong></p>
                    </div>
                </div>
            </div>




            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/CI-operations.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/CI-operations.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Road clearing party takes position during CI operations</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>




            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Crossing-water.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Crossing-water.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Crossing a water body during CI Operations </p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Soldiers.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Soldiers.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Soldiers holding Indian tricolour atop one of the recaptured peaks </p>
                        <p><strong>Source: MoD, DPR</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Gen VP Malik, PVSM, AVSM, then COAS during one of his visits to boost-28.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Malik.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Gen VP Malik, PVSM, AVSM, then COAS during one of his visits to boost morale of the troops during Kargil </p>
                        <p><strong>Source: MoD, DPR.</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Kargil.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Kargil.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Infantry soldiers going for assault during Kargil, 1999 </p>
                        <p><strong>Source: MoD, DPR</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Cheetah.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Cheetah.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Army Aviation Cheetah helicopters during Operation Vijay, Kargil 1999 </p>
                        <p><strong>Source: MoD, DPR</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Dehradun.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Dehradun.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Indian Military Academy, Dehradun</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Manekshaw-1.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Manekshaw.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Field Marshal SHFJ Manekshaw </p>
                        <p><strong>Source: USI of India</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Cariappa-1.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Cariappa.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Field Marshal KM Cariappa</p>
                        <p><strong>Source: USI of India</strong></p>
                    </div>
                </div>
            </div>



            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Operation-Pawan.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Operation-Pawan.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">A BMP during Operation Pawan in Sri Lanka</p>
                        <p><strong>Source: Army HQ</strong></p>
                    </div>
                </div>
            </div>



            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Chetwode.jpg ">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Chetwode.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Chetwode Motto, Indian Military Academy, Dehradun</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>



            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Peacekeeping.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Peacekeeping.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Captured Katangese Staghound, UN Peacekeeping Mission in Congo</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>



            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Goa.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Goa.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Indian troops welcomed in Goa, 1961 </p>
                        <p><strong>Source: MoD, DPR.</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Khadakwasla.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Khadakwasla.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Sudan Block, National Defence Academy, Khadakwasla, Pune </p>
                        <p><strong>Source: MoD, DPR.</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Jonga.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Jonga.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">A Jonga based detachment in Sri Lanka </p>
                        <p><strong>Source: Army HQ.</strong></p>
                    </div>
                </div>
            </div>



            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/UN-mission.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/UN-mission.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Patrolling the Temporary Security Zone (TSZ), during the UN mission in Ethiopia and Eritrea (UNMEE)</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>




            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Lebanon.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Lebanon.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Dental check up of a civilian in a remote village in Lebanon </p>
                        <p><strong>Source: USI of India</strong></p>
                    </div>
                </div>
            </div>



            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Congo- 1962.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Congo- 1962.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Under fire, Congo 1962 </p>
                        <p><strong>Source: USI of India.</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Northern-Command.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Northern-Command.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Repatriated POWs handed over to Northern Command, Neutral Nations Repatriation Commission, Korea 1951–53 </p>
                        <p><strong>Source: USI of India.</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Saltoro-Ridge.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Saltoro-Ridge.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">A soldier of 8 Maratha LI on Saltoro Ridge, 1985 </p>
                        <p><strong>Source: A soldier of 8 Maratha LI on Saltoro Ridge, 1985 </strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/secure-the-territory.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/secure-the-territory.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Indian troops landing in Goa after the liberation to reinforce the initial columns and secure the territory. </p>
                        <p><strong>Source: USI of India</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Shakargarh.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Shakargarh.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Withdrawal of Sikh troops and armaments, Shakargarh</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Farewell-at-Dacca.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Farewell-at-Dacca.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Farewell at Dacca</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Jessore.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Jessore.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Jessore, 1 January 1972</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Dacca.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Dacca.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">COAS’s first visit to Dacca, 26 December 1971  </p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Gen-Manekshaw.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Gen-Manekshaw.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Gen Manekshaw, COAS, addressing 16 MADRAS </p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Shakargarh-Area.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Shakargarh-Area.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Indian Troops, CEC TIVITER, Shakargarh Area, 1971 </p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Chamb-Sector.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Chamb-Sector.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Captured tanks and guns, Chamb Sector </p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Khem-Karan.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Khem-Karan.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">National Flag being hoisted at Khem Karan, 1965 </p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Lahore-Sector.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Lahore-Sector.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Centurion tanks in action, Lahore Sector</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Haji-Pir.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Haji-Pir.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Howitzer guns of Uri facing Haji Pir Pass, 1965</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Bahadur-Shastri.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Bahadur-Shastri.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">PM Lal Bahadur Shastri visits Forward Areas, Punjab, 1965</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Captured- Pakistani.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Captured- Pakistani.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Captured Pakistani Patton tank brought to New Delhi station, 1965 </p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Captured-tank.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Captured-tank.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Captured tank, Forward Area, Lahore Sector, 1965 </p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Parliament-House.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Parliament-House.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Captured Pakistani arms on show at Parliament House </p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Kashmir-Valley.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Kashmir-Valley.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Army jawans patrolling in Kashmir Valley, 1965  </p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/POWs.jpg ">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/POWs.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Arrival of POWs from Kashmir, 11 August 1965 </p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Aurora.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Aurora.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Lt Gen AAK Niazi signs the Instrument of Surrender, Dacca, 16 December 1971. Lt Gen JS Aurora sits to his right </p>
                        <p><strong>Source: MoD, DPR.</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/AAK-Niazi.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/AAK-Niazi.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Lt Gen Sagat Singh, Maj Gen RD Hira, Lt Gen AAK Niazi, and Maj Gen JFR Jacob proceeding for the surrender ceremony at Dacca, December 1971 </p>
                        <p><strong>Source: MoD, DPR.</strong></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Indira-Gandhi.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Indira-Gandhi.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Prime Minister Indira Gandhi being briefed on the Western Front, 1971 </p>
                        <p><strong>Source: MoD, DPR</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Mujibur-Rahman.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Mujibur-Rahman.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Sheikh Mujibur Rahman takes the salute at Dacca, March 1972. On the extreme left is Gen JS Aurora </p>
                        <p><strong>Source: MoD, DPR.</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Longewala.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Longewala.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Destroyed tank of Pakistan’s 22 Cavalry, Longewala, December 1971 </p>
                        <p><strong>Source: MoD, DPR.</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Camel-patrol.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Camel-patrol.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Camel patrol (Grenadiers) in the Thar Desert </p>
                        <p><strong>Source: MoD, DPR.</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Instrument-Surrender.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Instrument-Surrender.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Instrument of Surrender, 1971</p>
                        <p><strong>Source: USI, CMHCS</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Infantry-Division.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Infantry-Division.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Gen Thimayya, DSO, GOC 19 Infantry Division with senior commanders before the assault on Zoji La </p>
                        <p><strong>Source: USI, CMHCS.</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/piqueting.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/piqueting.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Troops of 15 Punjab (1 Patiala) piqueting the heights beyond Zoji La </p>
                        <p><strong>Source: USI, CMHCS.</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Pangong-Lake.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Pangong-Lake.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Soldiers patrolling at a post near Pangong Lake, 1963 </p>
                        <p><strong>Source: MoD, DPR.</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Rezangla.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Rezangla.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Gurkha Memorial at Chushul  </p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Chushul.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Chushul.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Under fire, Congo 1962 Rezangla Memorial Plaque</p>
                        <p><strong>Source: MoD, DPR. </strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/AMX-13-tanks.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/AMX-13-tanks.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">AMX-13 tanks of 20m Lancers operating in the Chushul area, October 1962 </p>
                        <p><strong>Source: MoD, DPR. </strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Indian-troops.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Indian-troops.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Indian troops which moved to high-altitude defences in 1962 were poorly equipped to repulse the Chinese offensive.</p>
                        <p><strong>Source: MoD, DPR. </strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/pounder-gun.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/pounder-gun.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">25 pounder gun in action </p>
                        <p><strong>Source: MoD, DPR</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/impregnable.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/impregnable.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">TThe Sino-Indian Conflict of 1962 shook India’s belief in the ‘impregnable’ Himalayan barrier and led to far reaching changes in defence preparedness and policy. </p>
                        <p><strong>Source: USI, CMHCS. </strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Fatu-La.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Fatu-La.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">A supply convoy winds its way through Fatu La, J&K, 1947–48 </p>
                        <p><strong>Source: USI, CMHCS.</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Bahadur-Pun.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Bahadur-Pun.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Maj Gen KS Thimayya, GOC, 19 Infantry Division with Sub Harka Bahadur Rana, MC and Jem Lal Bahadur Pun, VrC of 1/5 GR, Kargil, 1948 </p>
                        <p><strong>Source: 1/5 GR.</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Zoji-La.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Zoji-La.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Indian troops marching through Zoji La </p>
                        <p><strong>Source: 1/5 GR</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Light-Cavalry.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Light-Cavalry.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Stuart Tanks of 7 Light Cavalry advance through Zoji La, 1948 </p>
                        <p><strong>Source: USI, CMHCS</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Madras.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Madras.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Troops of the Native Army – Madras</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Risaldar Major Ganda Singh 1.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Sardar-Bahadur.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Risaldar Major Ganda Singh, Sardar Bahadur, OBI, IOM, 19th Fane’s Horse. Ganda Singh enlisted in 1852 in the 4 Punjab Cavalry. He saved the life of Sir Robert Sandeman near Lucknow in 1858 and later during the Second Opium War in 1860 was instrumental in saving the life of Col (later Maj Gen) Sir Charles McGregor, who subsequently founded the USI of India. Ganda Singh served on Lord Robert’s staff as his Indian ADC from 1889–1892, retiring from the army in 1894.</p>
                        <p><strong>Source: USI of India.</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Gurkha- Rifles.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Gurkha- Rifles.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Firing a Lewis gun in the trenches 3rd Battalion of 3rd Gurkha Rifles, 1917  </p>
                        <p><strong>Source: IWM Q 12931. </strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/England.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/England.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Wounded Indian soldiers recuperate in England, 1915 </p>
                        <p><strong>Source: Private collection</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Mussalman.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Mussalman.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">The Punjabi Mussalman company of 57th Wilde’s Rifles (FF) takes up positions on the outskirts of Wytschaete, Belgium, in October 1914. A row of houses is visible behind them, showing their proximity to the town </p>
                        <p><strong>Source: IWM 56325. </strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Gallipoli.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Gallipoli.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Indian stretcher bearers at Gallipoli, 1915 </p>
                        <p><strong>Source: 4 Mech.</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Amara.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Amara.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">The 36th Sikhs (now 4 Sikh) in Kut al Amara after the town was abandoned by the Turks in February 1917 </p>
                        <p><strong>Source: Rana Chhina</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/General-Allenby.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/General-Allenby.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Indian soldiers provide a guard of honour for General Allenby near the Jaffa Gate </p>
                        <p><strong>Source: Library of Congress</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Indian-infantry.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Indian-infantry.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Indian infantry clear an Italian street of German snipers during the advance to Rome, 1945 </p>
                        <p><strong>Source: CWGC</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Rangoon-temple.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Rangoon-temple.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Indian soldiers advance past a temple complex in Burma during the advance towards Rangoon </p>
                        <p><strong>Source: MoD, History Division. </strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Gothic-Line.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Gothic-Line.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">An Indian sentry guards a dangerous path somewhere on the Gothic Line in Italy, 1944 </p>
                        <p><strong>Source: USI of India. </strong></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Querrien.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Querrien.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">A Hotchkiss Gun Team practising near Querrien, July 1916</p>
                        <!-- <p><strong>Source: CWGC</strong></p> -->
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Jat-patrol.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Jat-patrol.jpg" class="img-fluid  card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">End of the war for a group of German soldiers surrendering to a Jat patrol of the Central India Horse, Italy 1944</p>
                        <p><strong>Source: USI of India</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Imphal-Kohima.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Imphal-Kohima.jpg" class="img-fluid  card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">A historic moment, Indians from Imphal and British from Kohima meet as 4 and 33 Corps link-up at milestone 109 on the Imphal-Kohima road</p>
                        <p><strong>Source: Rana Chhina</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Command-aircraft.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Command-aircraft.jpg" class="img-fluid  card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">Troops of 5th Indian Division prepare to emplane for Imphal on the Burma front in a US Air Transport Command aircraft, March 1944 </p>
                        <p><strong>Source: Rana Chhina</strong></p>
                    </div>
                </div>
            </div>



            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Marseilles.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Marseilles.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">15 Sikh disembark in Marseilles, France 1914</p>
                        <p><strong>Source: Rana Chhina</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Tientsein.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Tientsein.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">47th Sikhs (now 5 Sikh) march through the streets of Tientsein in North China, 1906 </p>
                        <p><strong>Source: Army HQ</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/North-Africa.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/North-Africa.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">An Indian infantry battalion prepares to embark for service in North Africa, 1940 </p>
                        <p><strong>Source: Rana Chhina</strong></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 main">
                <div class="image-main img-pop" big_src="<?php echo get_template_directory_uri(); ?>/assets/img/Afrika-Korps.jpg">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery/Afrika-Korps.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                     <div class=" card-body img-body">
                        <p class="heading">On the lookout for the Deutsches Afrika Korps. An Indian signaler in North Africa, 1940 </p>
                        <p><strong>Source: MoD, History Division</strong></p>
                    </div>
                </div>
            </div>


           
        </div>
    </div>
</section>


<div class="icons">
    <div class="container">
        <div class="row center">
            <div class="col-md-12">
                <div class="pagination">
                    <a href="#" class="active">1</a>
                    <a href="#">2</a>
                    <a href="#">3</a>
                </div>
            </div>
        </div>
    </div>
</div>




<?php get_footer(); ?>