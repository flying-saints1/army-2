<?php 

$main ="operaitons";

$page="human";



get_header(); ; ?>


<section class="post-independence-war-banner"
    style="background-image: url(../assets/img/post-independence-war-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">Humanitarian and Disaster Relief (HADR)</h1>
    </div>
</section>


<section class="indep-war" id="faq-section">
    <div class="container">
        <div class="row">
            <?php include('../sidebar/operations-sidebar.php'); ?>

            <div id="india-and-un" class="col-md-9">
                <div class="content">
                    <img src="../assets/img/changeable-img.jpg" width="100%" height="400px" class="img-fluid res-image"  alt="" loading="lazy">
                </div>


                <div id="op" class="accordion-details">
                <h3>About Operation</h3>

                    <div class="war-details">
                    
                        <p>The Indian subcontinent is prone to earthquakes, floods, droughts, cyclones, etc. Over the past decade and more, the region has seen massive earthquakes in the Himalayan belt (Kashmir 2005, Nepal 2015), recurrent floods (Assam), cyclones (1999 Orissa), avalanches in the higher Himalayas, and even freak natural and climate-induced disasters such as the one that occurred in Uttarakhand in 2013. Given this, there has been a conscious effort to work out and adopt a multi-dimensional, multi-disciplinary and multi-sectoral approach to disaster management. The Indian Armed Forces constitute a significant resource available to the Government of India for disaster response and providing relief to the affected populace.</p>

                        <p>Thus, beyond its primary duty to protect the nation’s borders, and its role in counterinsurgency and internal security, the Indian Army—along with the other Services and paramilitary and security organisations—also provides aid to civil authority. The Disaster Management Act of 2005 has ensured that the Army’s role in disaster response remain focused on critical issues, with optimal utilisation of dedicated resources. Its structure, organisation and spread, and training ensures that it is capable of reaching out to the remotest of areas and terrain under the most difficult conditions. In terms of disaster relief, the army can respond instantaneously and mobilise self-contained, composite task forces to any part of the country, even overseas, in conjunction with the Indian Navy and the Indian Air Force (IAF).</p>

                        <p>Since Independence the Indian Army has been amongst the first responder in most HADR operations across the country in varied terrain and the performance of the Indian Army has been remarkable and they have always delivered. The presence of the Army conducting HADR operations has raised the morale of the affected people in that area and they have carried out the tasks assigned to them with the utmost professionalism and dedication. Over the past two decades, the Indian Army has provided assistance and relief during the following instances. The range of HADR operations that the army has been involved in is too vast to be discussed in detail here. Therefore, a few operations from the past two decades have been discussed below to provide readers with a brief view of the Indian Army’s role and activities in HADR.</p>

                        
                    </div>


                <div id="tsunami" class="accordion-details">
                <h3>2004 Indian Ocean Tsunami</h3>
                   

                    <div class="war-details">
                        
                        <p>Since Independence the Indian Army has been amongst the first responder in most HADR operations across the country in varied terrain and the performance of the Indian Army has been remarkable and they have always delivered. The presence of the Army conducting HADR operations has raised the morale of the affected people in that area and they have carried out the tasks assigned to them with the utmost professionalism and dedication. Over the past two decades, the Indian Army has provided assistance and relief during the following instances. The range of HADR operations that the army has been involved in is too vast to be discussed in detail here. Therefore, a few operations from the past two decades have been discussed below to provide readers with a brief view of the Indian Army’s role and activities in HADR.</p>

                        <p>Around 27,986 persons were rescued/evacuated under extremely hostile conditions. Around 60 relief camps were established, 41,080 patients were treated in mobile field hospitals/camps, over 990 dead bodies were recovered, debris cleared and essential rations, food packets, medicines, clothing, blankets and kerosene oil provided to the traumatised populace. The task forces were, thereafter, involved in rehabilitation and reconstruction operations for a sustained period of six to nine months, including restoration and manning of essential services, restoration of surface communications to include launching of bailey bridges and operation of ferries, erection of temporary dwelling units and toilets, enforcement of hygiene and sanitation measures to prevent an epidemic outbreak, trauma and psychological counseling, as also informative classes on employment and recruitment opportunities for youth, among others.</p>
                       
                    </div>
                </div>

                <div id="eq" class="accordion-details">
                <h3>Kashmir Earthquake 2005</h3>

                    <div class="war-details">
                        

                        <p>Despite the loss of 45 personnel and many other injured, the Indian Army, by virtue of its location in Kashmir, was able to immediately assume the role of the primary agency for rescue and relief effort after the 2005 earthquake in Kashmir. The army’s rescue teams were the first to reach the isolated, inaccessible areas. A total of 52 columns, comprising 2,600 troops, along with 39 medical teams, and 31 tonnes of medicine and specialised mountaineering equipment, were mobilised for the rescue and relief operation, known as Operation Imdad. The army set up 40 relief camps, rescued 1,200 people, treated 6,000 civilian patients, and distributed around 150 tonnes of rations and 18,000 food packets to the affected populace.</p>

                       
                    </div>
                </div>

                <div id="kerala" class="accordion-details">
                <h3>Disease and Floods in Kerala</h3>

                    <div class="war-details">
                       
                        <p>Consequent to the outbreak of vector-borne diseases like Dengue and Chikunguniya in Kerala in June 2007, the Indian Army’s services were requisitioned by State Government. Medical and Health Teams with the requisite fogging and spray equipment were thus deployed to aid the state authorities. Army personnel carried out extensive fogging and spray of anti-larval pesticides was carried out at Amburi, Patnamthitta, Kollam and Thalachira. Additionally, a Research Team comprising of an Entomologist, a Virologist and and Epidemiologist was also deployed for investi- gation and advice. A Neuro Physician, formerly with the Military Hospital Southern Command, Pune, also visited the affected areas.</p>

                        <p>In 2018, the state was also witness to heavy rainfall in the northern districts which also resulted in landslides. The Indian Army was involved in rescue operations in Idukki, Wayanad, Kannur, and Kozhikode districts of Kerala under Operation Sahyog. The army dispatched ten flood relief columns, ten Engineer Task Forces (ETF), with approximately 40 personnel each were dispatched from Jodhpur, Bhopal, Pune, Bangalore and Secunderabad. Around 53 military boats were used to evacuate civilians from flood affected areas.</p>

                    </div>
                </div>

                <div id="nepal" class="accordion-details">
                <h3>Nepal Earthquake 2015</h3>

                    <div class="war-details">
                      
                        <p>In aftermath of the massive earthquake in Nepal in April 2015, the Government of India and the Indian Armed Forces launched Operation Maitri, a rescue and relief operation. The Indian response was instantaneous and began on 26 April 2015. Within three days of the operation, working in conjunction, the Indian Army and the IAF had evacuated 170 foreign nationals belonging to US, the UK, Russia and Germany from Nepal, and brought back over 5,000 Indians using IAF and civilian aircraft. India sent 18 medical teams (comprising 20 people each), and 12 engineer teams of about 60 people to assist in recovery and relief operations.</p>

                       
                    </div>
                    
                </div>



               


                <!-- <div id="gallery" class="row image-gallery">
                    <div class="col-md-12">
                        <h3>Gallery</h3>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img5.jpg" class="img-fluid"  alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img6.jpg" class="img-fluid"  alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img7.jpg" class="img-fluid"  alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img8.jpg" class="img-fluid"  alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img13.jpg" class="img-fluid"  alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img9.jpg" class="img-fluid"  alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>
                </div> -->

 
            </div>  
        </div>
    </div>
</section>


<?php get_footer(); ; ?>