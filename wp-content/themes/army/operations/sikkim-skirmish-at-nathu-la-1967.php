<?php 

$main ="operaitons";

$page="sikkim";

get_header(); ; ?>


    <section class="operations-banner" style="background-image: url(../assets/img/operations-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Sikkim: Skirmish at Nathu La 1967</h1>
        </div>
    </section>




<section class="operation-details" id="faq-section">
    <div class="container">
        <div class="row">
           <?php include('../sidebar/operations-sidebar.php'); ?>


            <div id="back" class="col-md-9">
                <div class="content">
                    <img src="../assets/img/post-indep-war-2.png" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                </div>


                
                <div id="back" class="accordion-details">
                <h3>Background</h3>

                    <div class="war-details">
                    
                        <p>The Nathu La pass lies on the Old Silk Route between Tibet and India. In 1904, Major Francis Younghusband, serving as the British Commissioner to Tibet, led a successful mission through Nathu La to capture Lhasa. This led to the setting up of trading posts at Gyantse and Gartok in Tibet and gave the British control of the surrounding Chumbi Valley. The following year, China and Great Britain ratified an agreement approving trade between Sikkim and Tibet. In 1947, Sikkim became an Indian protectorate. After China took control of Tibet in 1950 and suppressed a Tibetan uprising in 1959, refugees entered Sikkim through Nathu La. During the 1962 Sino-Indian War, Nathu La witnessed skirmishes between soldiers of the two countries. Shortly thereafter, the pass was sealed and was closed for trade. Five years later, Nathu La was the scene of a ‘border skirmish’ between Indian and China, which resulted in heavy casualties to both sides. Significantly, it was the first instance when the Chinese got a ‘bloody nose’ from the Indians.</p>

                        <p>The Nathu La pass lies on the Old Silk Route between Tibet and India. In 1904, Major Francis Younghusband, serving as the British Commissioner to Tibet, led a successful mission through Nathu La to capture Lhasa. This led to the setting up of trading posts at Gyantse and Gartok in Tibet and gave the British control of the surrounding Chumbi Valley. The following year, China and Great Britain ratified an agreement approving trade between Sikkim and Tibet. In 1947, Sikkim became an Indian protectorate. After China took control of Tibet in 1950 and suppressed a Tibetan uprising in 1959, refugees entered Sikkim through Nathu La. During the 1962 Sino-Indian War, Nathu La witnessed skirmishes between soldiers of the two countries. Shortly thereafter, the pass was sealed and was closed for trade. Five years later, Nathu La was the scene of a ‘border skirmish’ between Indian and China, which resulted in heavy casualties to both sides. Significantly, it was the first instance when the Chinese got a ‘bloody nose’ from the Indians.</p>
                        
                        <p>During the Indo-Pak War of 1965, China gave an ultimatum and demanded that India withdrew her posts at Nathu La and Jelep La. According to HQ 33 Corps, the main defences of 17 Mountain Division were at Changgu, while Nathu La was only an observation post. In the adjoining sector, manned by 27 Mountain Division, Jelep La was also considered an observation post, with the main defences located at Lungthu. In case of hostilities, the divisional commanders had been given the authority to vacate the posts and fall back on the main defences. Accordingly, orders were issued by Corps HQ to both divisions to vacate Nathu La and Jelep La.<p>

                        <p>Maj Gen Sagat Singh, GOC 17 Mountain Division, disagreed with the views of the Corps HQ as both Nathu La and Jelep La were passes on the watershed—the natural boundary—and vacating them would give the Chinese the tactical advantage of observation and fire into India, while denying the same to our own troops. These passes also provided the only means of ingress through the Chumbi Valley. Sagat also reasoned that the discretion to vacate the posts lay with the divisional commander, and he was not obliged to do so, based on instructions from Corps HQ.</p>

                        <p>As a result of orders issued by Corps HQ, 27 Mountain Division vacated Jelep La, which the Chinese promptly occupied. However, Maj Gen Sagat Singh refused to vacate Nathu La, and when the Chinese became belligerent and opened fire, he responded with guns and mortars, though there was a restriction imposed by Corps on the use of artillery. Lieutenant General (later General) G.G. Bewoor, the Corps Commander, was extremely annoyed, and tried to speak to Sagat to ask him to explain his actions. But Sagat was not in his HQ and was with the forward troops. So, it was his GSO 1, Lieut. Colonel Lakhpat Singh, who bore the brunt of the Corps Commander’s wrath.</p>

                        <p>Chinese loudspeakers at Nathu La warned the Indians that if they did not with- draw, they would suffer as they did in 1962. However, Gen Sagat concluded that the Chinese were bluffing. They made threatening postures, such as advancing in large numbers, but on reaching the border, always stopped, and withdrew. They also did not use any artillery, for covering fire, which they would have certainly done if they were serious about capturing any Indian positions. Indian artillery observation posts on Camel’s Back and Sebu La overlooked into the Yatung valley for several kilometres, and could bring down accurate fire on the enemy, an advantage that the Chinese did not have. It would have been a tactical blunder to vacate Nathu La and gift it to the enemy. Ultimately, his fortitude saved the day for India, and his stand was vindicated two years later, when there was a show down there.</p>
                       

                      
                    </div>

                </div>

                <div id="lead" class="accordion-details">
                <h3>Lead-up to the Skirmish</h3>

                
                    <div class="war-details">
                        
                        <p>Vexed by their failure to occupy Nathu La in 1965, the Chinese continued to put pressure on the Indians. In December 1965, the Chinese fired on a patrol of 17 Assam Rifles, in North Sikkim, at a height of 16,000 feet, killing two men. They made regular propaganda broadcasts from loudspeakers at Nathu La, a form of psychological warfare in which the Chinese were adept. Maj Gen Sagat had similar loudspeakers installed on our own side and tape-recorded messages, in Mandarin, which were broadcast daily. Throughout 1966, and early 1967, Chinese propaganda, intimidation and attempted incursions into Indian territory continued. The border was not marked, and there were several vantage points on the crest line which both sides thought belonged to them. Patrols which walked along the border often clashed, resulting in tension, and sometimes even casualties.</p>

                        <p>In the first week of August 1967, the border outposts (BOPs) at Nathu La were occupied by 2 Grenadiers, relieving 18 Rajput. Lt Col Rai Singh was then commanding 2 Grenadiers. Maj Bishan Singh took over as ‘Tiger Nathu La’, as the company commander holding the pass was generally known, with Capt PS Dagar as his second-in-command. The deployment at Nathu La comprised a platoon each on Camel’s Back, South Shoulder, Centre Bump and Sebu La. The battalion headquarters was at Gole Ghar, while the battalion 3-inch mortars were just above Sherathang, which also had the administrative base and forward aid post. 18 Rajput took over the BOP at Yakla where they had a platoon plus. The BOPs at Cho La were occupied by a company of 10 Jammu & Kashmir (J&K) Rifles.</p>

                        <p>Even while 2 Grenadiers was in the process of taking over the defences at Nathu La, Chinese activities increased. They were noticed repairing their bunkers on North Shoulder and making preparations to construct new ones. On 13 August, the observation post at Sebu La reported that the Chinese had arrived on the crest line and dug trenches on the Indian side of the international border. When challenged, they filled up the trenches and withdrew. The same day, they added eight more loudspeakers to the already existing 21 speakers on South Shoulder. Due to this the volume of their propaganda increased and could now be heard at Changgu.</p>

                        <p>In 1967, the Divisional Commander—Maj Gen Sagat Singh—discussed the problem with the Corps Commander, Lt Gen JS Aurora, and suggested that the border at Nathu La should be clearly marked to prevent such incidents. He offered to walk along the crest line to test the Chinese resolve; if they did not object, the line along which he walked could be taken to be acceptable to them. This was agreed to, and Gen Sagat, accompanied by an escort, began walking along the crest. The Chinese commander also walked alongside, accompanied by a photographer, who kept taking pictures. The ‘walk’ ended peacefully.</p>


                        <p>Maj Gen Sagat then obtained the concurrence of the Corps Commander to mark the crest line, ordering a double wire fence to be erected, from Nathu La towards the North and South Shoulders. 2 Grenadiers was ordered to lay a three- strand wire fence along the border from Nathu La towards the North Shoulder. However, as soon as work began on the fence on 20 August 1967, the Chinese asked the Indians to stop. One strand of wire was laid that day, and two more were added over the next two days. This led to an escalation in Chinese activity. On 23 August, at about 1400 Hours, Major Bishan Singh reported that about    75 Chinese in battle dress carrying rifles fitted with bayonets were advancing towards Nathu La. They advanced slowly in an extended line and stopped on reaching the border. At 1430 Hours they started shouting slogans which the Political Commissar read out from a red book, repeated by the rest of the troops. The Indian troops were ‘stand to’, watching and waiting. The Chinese withdrew after standing on the border for around an hour.</p>

                        <p>On 1 September 1967, the Corps Commander and the Divisional Commander visited Nathu La. Despite poor visibility, they went to Centre Bump first and then to South Shoulder. Thereafter, they walked along the border to Four Poles area, where they crossed the border and went a few steps inside. At once, the Chinese Political Commissar came running up to them, shouting ‘Chini, Chini’, indicating that they have crossed the border into China. The two officers immediately withdrew, but the Chinese kept on grumbling. Soon a photographer came and took photographs of their footsteps across the border.    </p>

                        <p>The next morning Maj Gen Sagat went to Nathu La again and directed that the border from Right OP to Camels Back must be patrolled. A patrol of two officers, one JCO, and 15 Other Ranks (OR) was immediately sent out under Maj Bishan Singh. As soon as the patrol reached the U Bump near Tekri, the Chinese surrounded them. Major Bishan Singh tried to explain to the Chinese officer that they had not crossed the border and that, in fact, it was the Chinese who were within Indian territory. However, the Chinese did not budge.</p>
                        
                        <p>On 4 September, Maj Gen Sagat Singh again went to Nathu La and directed that the wire fence be converted into a Cat Wire Type 1 obstacle, using concertina coils. The task was allotted to 2 Grenadiers and a platoon of 70 Field Company Engineers under Maj Cheema was allotted to assist them. Work commenced on 5 September at 0500 Hours, but the Chinese objected. There was an argument between Col Rai Singh and the Chinese Political Commissar over the alignment of the border and the work was stopped at 0800 Hours. However, work on Chinese defences on North and South Shoulder continued. During the night the Chinese came up to the Bump and cut off one shoulder so that if water was poured on the other shoulder it would flow into China. The next morning when our men went to straighten out some wire, a few Chinese came running up to the border with a bucket of water and poured it on the Bump indicating the watershed.</p>

                        <p>On 7 September, the work started again on laying the wire. This time around 100 Chinese came to the fence and there was hand-to-hand fighting between the troops. Realising that they were unequal to the Jats, the Chinese withdrew and began pelting stones, the Indians responding in the same manner. The fighting delayed progress in the laying of the wire. Two Indian soldiers were wounded, and the Chinese suffered a few casualties. Things were relatively quiet on 8 and 9 September 1967, but the Chinese continued work on their defences.</p>

                        <p>On the night of 10 September 1967, Maj Gen Sagat Singh held a conference at HQ 112 Mountain Brigade in Changgu, where he personally briefed everyone on how the operation for laying the wire was to be carried out on 11 September. Additional resources in men and material were moved for this purpose. One company of 18 Rajput was brought in to reinforce the defences. An ad hoc force of 90 men was organised into a protection party to charge the Chinese positions if they opened fire. Maj Bishan Singh was in charge of the work with Captain PS Dagar as his assistant. Apart from the platoon of 70 Field Company, a pioneer platoon was to assist in the construction of the fence.</p>

                        
                    </div>

                   
                </div>



                <div id="sept" class="accordion-details">
                <h3>The Events of 11 September 1967</h3>

                
                    <div class="war-details">
                        
                        <p>Work on the fencing commenced at about 0600 Hours on 11 September 1967. The Chinese immediately came up to the fence and tried to stop its progress. There was a lot of shouting and the Indian troops refused to back down and started fighting with the Chinese. There was a heated discussion between the Chinese commander, who was accompanied by the Political Commissar, and Lt Col Rai Singh. Foreseeing this eventuality, Maj Gen Sagat told Rai Singh not to expose himself but remain in his bunker where the brigade commander, Brig MMS Bakshi, was also present. However, this was not heeded, and Rai Singh came out in the open with an escort to stand face-to-face with the Chinese officers. As the arguments became more heated, tempers rose, with both sides standing their ground. There was a scuffle during which the political commissar fell down and broke his spectacles.</p>

                        <p>At about 0745 Hours, the Chinese started withdrawing to their bunkers. The strength of Chinese opposing the Indian troops working on the fence gradually reduced. The loudspeakers on the Chinese side started blaring a speech of Mao Tse Tung in Hindi. The volume of the loudspeakers shielded the commotion of the hand- to-hand fight along the fence. Then there was a loud whistle, after which the guns of the Chinese on the North and South Shoulders opened fire on Indian troops with medium machine guns. The Indian troops laying the fence, who were in the open, bore the initial brunt of the firing. Lt Col Rai Singh was hit by a Chinese bullet and fell down.</p>


                        <p>Seeing their CO fall, the Grenadiers, led by Capt PS Dagar came out of their trenches and attacked the Chinese post. The company of 18 Rajput, under Maj Harbhajan Singh, and the sappers and pioneers working on the fence had been caught in the open and suffered heavy casualties from the Chinese firing. Realising that the only way to neutralise the Chinese fire was a physical assault, Harbhajan led his men in a charge on the Chinese position. Several of the Indian troops were mowed down by Chinese machine guns, but those who reached the Chinese bunkers used their bayonets and accounted for many of the enemy. Both Harbhajan and Dagar lost their lives in the action, for which they were awarded the MVC and VrC, respectively. Soon after this, Chinese artillery also started firing on Indian positions. By about 0930 Hours, Chinese fire intensified and gradually the brigade head- quarters started getting out of touch with the troops at North and South Shoulders. By 0945 Hours, there was no contact with anyone on the shoulders even on the artillery network. All the lines were down as the B1 wireless link to the pass. The brigade signal officer, Second Lieutenant (2/Lt) NC Gupta was with the brigade commander at the Central Bump. This was a platoon location overlooking the entire pass including North and South Shoulders, H Section, etc. It was around 400 m behind the border and an excellent vantage point. Gupta tried to enter the battalion net and the company net but failed. There was no response on any of the almost dozen frequencies of the battalion in use that day for various nets.</p>

                        <p>Around this time from the vantage position at Central Bump, the brigade commander saw over a dozen troops running down the slopes of South Shoulder towards Sherathang. He also observed that some of them had shed their helmets, packs and even rifles as they ran down. He asked Gupta to call South Shoulder but there was no response. They tried to observe the area of South Shoulder but could see no movement. By this time, the shelling on the South Shoulder had also increased.</p>

                        <p>Under the circumstances there was no other option for the brigade commander but to send someone to South Shoulder to restore communications. Gupta had a line party and spare radio sets with him. It was decided that a radio set be sent to South Shoulder. The linemen with Gupta were new to Nathula and had never gone to South Shoulder. Havildar Bhakuni, the commander’s rover operator had gone there many times. But sending him would have left the commander without communications. Seeing the gravity of the situation and the shelling the commander told Gupta to go himself with a radio set.</p>

                        <p>The distance to South Shoulder was about 500 m. The route was open at places and involved going down around 300 m and then up around 200 m. Gupta reached South Shoulder at around 1000 Hours. He found the post totally abandoned. He informed the brigade commander, who asked him to look around for wounded if any and remain at the post and keep him in picture. From the bunkers on South Shoulder Gupta could see the Chinese in their bunker. By this time intermittent fog had started setting in. He informed the brigade commander that he could see a few dead soldiers in the area ahead of our defences close to the fence and resorted to intermittent firing from his carbine to indicate that the post was still occupied. Soon, he found an LMG in its bunker which he used it to try and depict our presence on the post.</p>

                        <p>At around 1100 Hours, the brigade commander informed Gupta that reinforcements were on their way but would take at least three hours to reach and that he must hold on till then. A little later he asked Gupta to go around 100 m down South Shoulder where he had spotted a few soldiers sitting behind a huge rock. After firing a few salvos of LMG, Gupta went down. He found six soldiers of 2 Grenadiers comprising 2/Lt Attar Singh (fresh from IMA), one Havildar and four Jawans, who then accompanied Gupta back to the South Shoulder.</p>

                        
                        <p>By 1200 Hours, the fog had intensified. As he was watching from one of the bunkers, Gupta saw one of the dead moving next to the fence, barely 10 m from the Chinese bunker. Taking advantage of the fog he went ahead to try and recover him. It was Maj Bishan Singh, ‘Tiger Nathula’, who had been badly injured in the initial firing. Gupta managed to lift him and drag him into our defences with the help of the men of 2 Grenadiers and once inside made him speak to the brigade commander. After having been briefed by Bishan Singh, the brigade commander asked Gupta to evacuate him using the four jawans. He ordered Gupta, Attar Singh and the Havildar to remain at the post. Ten minutes later the brigade commander asked Gupta to return to the area of Bumps leaving the radio set with 2/Lt Attar Singh.</p>
                    </div>

                </div>



                <div id="assessment" class="accordion-details">
                <h3>Assessment</h3>

                
                    <div class="war-details">
                        
                        <p>The skirmish developed into a full-scale battle, lasting five days. Maj Gen Sagat had asked for some medium guns which were moved up to Kyangnosa La, at a height of over 10,000 ft. The artillery observation posts proved their worth in bringing down effective fire on the Chinese. Because of lack of observation, and the steep incline west of Nathu La, most Chinese shells fell behind the forward defences, and did not harm the Indians. During the first day’s action, some troops of 2 GRENADIERS, who were occupying the South Shoulder, vacated their positions. The position was re-occupied, and the troops pushed back into their trenches. The brigade signal officer, 2/Lt NC Gupta played an important role in this operation, for which he was awarded a Sena Medal.</p>

                        <p>The Indian casualties in the action were just over 200, with 65 dead and 145 wounded. The Chinese are estimated to have suffered about 300 casualties. Though the action taken by Maj Gen Sagat Singh in marking the border with a wire fence had the approval of higher authorities, the large number of casualties suffered by both sides created a furore. Perhaps the casualties to Indian troops would have been minimal if they had remained in their defences and not exposed themselves by coming out of their trenches and rushing at the Chinese post. This happened on the spur of the moment, because seeing their CO fall, the troops lost their cool, and rushed forward under the orders of a young officer, who lost his life in the action. The Chinese had already announced that it was the Indians who started the conflict, and the large number of Indian bodies and wounded Indian soldiers in their possession, seemed to support their claim. However, Gen Sagat was not perturbed. For the last two years, the Chinese had been instigating him, and had killed several Indian soldiers. The spectre of the Chinese attack of 1962 had haunted India. The events at Nathu La in 1967, where the Chinese got a bloody nose for the first time since 1962, broke the myth of their invincibility.</p>
                       
                    </div>

                </div>

     
                <!-- <div id="gallery" class="row image-gallery">
                    <div class="col-md-12">
                        <h3>Gallery</h3>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">

                            <img src="../assets/img/img5.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img6.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img7.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img8.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img13.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img9.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>
                </div> -->
            </div> 
        </div>
    </div>
</section>




    
<?php get_footer(); ; ?>