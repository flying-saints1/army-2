<?php

$main = "operaitons";

$page = "lico";


get_header(); ; ?>


<section class="post-independence-war-banner" style="background-image: url(../assets/img/post-independence-war-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">Low-Intensity Conflicts and Counter-terrorism</h1>
    </div>
</section>


<section class="indep-war" id="faq-section">
    <div class="container">
        <div class="row">

            <?php include('../sidebar/operations-sidebar.php'); ?>


            <div id="j&k" class="col-md-9">
                <div class="content">
                    <img src="../assets/img/changeable-img.jpg" width="100%" height="400px" class="img-fluid res-image" alt="" loading="lazy">
                </div>


                <div class="accordion-details">
                    <!-- <h3>J&K</h3> -->

                    <div class="war-details">

                        <p>The Indian Army has the unique distinction of being involved in fighting low-intensity conflicts for most of its pre- and post-independence history. This involvement can be debated on the basis of definitions and descriptions of conflicts ranging from frontier warfare to counter-insurgency. However, the fact remains that insurrections of the type faced in Afghanistan, North-West Frontier Province (NWFP) and north-east India provided the army with early years of experience and expertise, which became the foundation for fighting what is now termed in some quarters as ‘low-intensity conflicts’ or ‘sub-conventional warfare’. Within the wider ambit of the term, it has also been referred to as counter-insurgency and counter-terrorism.</p>

                        <p>The post-independence deployment in Nagaland of the Indian Army to fight an insurgency provided as much an opportunity to learn as it did to unlearn previous battle procedures. Unlike the past when the pre-independence Indian Army fought rebellious regions in the country’s periphery, independent India was now attempting to bring alienated citizens of the country back into the mainstream, despite some of them taking up arms.</p>


                        <p>Though the outbreak of counter-insurgency in Nagaland marked the commencement of low-intensity conflicts in post-independence India, the contagion did not remain restricted to this region. In fact, Nagaland emerged as the mother insurgency for the North-East and continues to remain the oldest continuous violent armed struggle anywhere in the country.</p>


                        <p>Since then, India has experienced similar violent uprisings in Jammu and Kashmir (J&K), Punjab, areas affected by left-wing extremism (LWE), as also Islamic terror- ism within the hinterland, in most cases with support from Pakistan. In addition, the nature and scope of terrorism has undergone change, with the line dividing low and medium intensity—that was more distinct in the past—merging. This is because states like Pakistan have started pursuing their strategic objectives largely through the low-intensity route rather than war, as was the case earlier.</p>


                        <p>It is a challenge to delve into a detailed historical analysis of low-intensity conflicts in India and its multiple facets in a single chapter. Keeping this constraint in view, an attempt will be made to provide an overview of experiences of the Indian Army in counter-insurgency and counter-terrorism operations. As some readers would possibly be aware of this historical survey, the more important specific aspects of this experience will be highlighted. The chapter, therefore, will provide a broad-based perspective of the army’s role over the decades, and simultaneously focus more closely on issues which defined and made this contribution distinct when compared with experiences elsewhere across the world.</p>



                    </div>


                    <div id="perspective" class="accordion-details">
                        <h3>A Historical Perspective</h3>

                        <div class="war-details">

                            <p>The experience of the Indian Army with low-intensity conflicts, which includes a wide variety of more specific violent experiences to include terrorism, insurgency, local uprisings and skirmishes, emerges from the state’s involvement prior to India’s independence. The British Indian Army was involved in expeditionary operations not only beyond the subcontinent but also within it. These experiences in Afghanistan and north-east India, to name a few, led to its encapsulated documentation. In some cases, it became a part of the training curriculum for officers who were trained to operate in these areas. While these experiences may not be directly related to post- independence operations undertaken by the Indian government and more specifically, the Indian Army, the importance of the same deserves to be underlined to highlight the contrast and shift that became evident after India’s independence.</p>

                            <p>The British War Office, General Staff, published Field Service Regulations, Part I, in 1909, dealing with operations. Similar to more recent manuals for the armed forces, the regulations provided an introduction to fundamentals, like the fighting troops, intercommunication and orders, movement by land and sea and different types of quarters. It also highlighted operational aspects, like protective duties, information, battles, siege operations and night operations. In addition, and of greater relevance to this chapter, the regulations included a chapter on ‘Warfare against an Uncivilized Enemy’. The approach to operations becomes evident from not only the title of the chapter but, subsequently, from the manner in which the countries concerned are referred to. A footnote on the very first page of the chapter refers to the relevance of transport and convoys to operations in these areas and in doing so, describes the region as an ‘uncivilized country’.</p>


                            <p>This description does not come as a surprise. It is reinforced by more specific tactics to deal with the ‘enemy’. This includes: ‘in dealing with independent fanatical tribes, an advance against a sacred town or shrine may have the same effect’ (end of organised resistance); and ‘If no such objective is available, the enemy may be brought to oppose the advance by a movement against his wells or sources of supply.’ The manual goes on to further suggest: ‘Should the enemy refuse to make any organized resistance, the occupation of his country, the seizure of his flocks and supplies, and the destruction of his villages and crops may be necessary to obtain his submission.’ This prescribed manual indicates the nature of tactics taught and followed in low- intensity operations by the Indian Army in undivided India, to control the peripheral areas of the country and beyond. The nature of punitive measures undertaken, use of force and the parameters that guided its employment created a foundational understanding within the rank and file of the army about handling such uprisings.</p>

                            <p>These operations were followed by Indian Army’s overseas deployment during World War II. It was during this phase, after a nearly two decades pause, that some Indian officers and men were exposed to the challenges of conventional operations. They acquitted themselves well and the Indian Army emerged amongst the highest decorated forces in the world. With the end of the war in 1945 and independence soon thereafter in 1947, the nascent army of an independent India emerged with an indelible impact of both these phases. The political leadership of the country understood the paradigm shift that the army was required to deal with while being employed to suppress violence and unrest within the country. This became evident when the army was called out in aid of civil authorities to fight Naga insurgents in 1955.</p>

                            <p>The induction of the army into its first major counter-insurgency campaign was accompanied by a guideline both from the then Prime Minister, Jawaharlal Nehru, and the Chief of the Army Staff: ‘Nehru reminded the army that the Nagas were fellow-countrymen who had to be won over, not suppressed.’ The Chief, through an order of the day, provided a more detailed perspective on how the army was expected to conduct itself and the approach towards the insurgents:</p>

                            <p>You must remember that all the people of the area in which you are operating are fellow Indians…Some of these people are misguided and have taken to arms against their own people, and are disrupting the peace of this area…You are not there to fight the people in the area, but to protect them…You must do everything possible to win their confidence and respect and to help them feel that they belong to India.</p>

                            <p>These words were as appropriate then as they are in the present context. Also, these words are not only relevant to India but also in any area where security forces are deployed to fight terrorism and insurgency. In many ways, this remains the essence of the approach to fighting counter-insurgency in India. Therefore, it does not come as a surprise that the evolving nature of India’s counter-insurgency doctrine captured this well, as will be seen later in the chapter.</p>
                        </div>

                    </div>



                    <div id="employment" class="accordion-details">
                        <h3>Post-independence Employment</h3>

                        <div class="war-details">
                            <h4>Nagaland</h4>
                            <p>As mentioned earlier, the first major involvement of the Indian Army in countering insurgency began in Nagaland. The gauntlet was thrown by Angami Zapu Phizo when he declared independence on 14 August 1947, a day prior to India gaining independence. This, however, did not stop negotiations between the Naga National Council (NNC), which Phizo headed, and representatives of the government, including Nehru. By 1953, during a visit by Nehru to Nagaland, the divide only became sharper. On 18 September 1954, Phizo established the ‘Republican Government of Free Nagaland’. The Naga Federal Army (NFA) was created on a structure similar to the army, with weapons provided by the British during World War II, and the die was cast.</p>

                            <p>Two guerrilla wings, under Thungti Chang and Kaito Sema, overwhelmed the local police and Assam Rifles, who were not only caught unaware but also under- prepared for the violence. The raids on the police stations further augmented the armoury of the NFA. As the Assam Rifles were unable to break the resistance of the rebels, the army was inducted after the Disturbed Areas Act was promulgated on 27 August 1955. Counter-insurgency operations began soon thereafter.</p>


                            <p>The insurgency in the area, which went beyond the boundary of Nagaland, incorporating the hilly areas of Manipur and the southern-most districts of Arunachal Pradesh, went through a roller-coaster ride thereafter, witnessing a series of highs and lows. The 1950s and the 1960s saw the Naga insurgency gain strength with support in terms of training and weapons from China. Over time, Pakistan also saw an opportunity in supporting militancy in the region. </p>


                            <p>The conditions along the border, including weak or colluding groups, similarity of ethnic entities on both sides and limitations placed by thickly forested and mountainous terrain, placed constraints on operations by the security forces. However, military pressure and political initiatives continued in a bid to seek reconciliation.</p>


                            <p>Attempts at achieving a breakthrough were made by the government in the mid-1960s through initiatives, like granting statehood to Nagaland in 1963 and negotiating with a group of special emissaries which included Jaiprakash Narain, BP Chaliha and Reverend Michael Scott. However, these negotiations did not succeed and after the breakdown of talks sometime in 1968, operations continued from both sides.</p>


                            <p>A major breakthrough was finally achieved with the surrender of 1,500 Revolutionary Government of Nagaland (RGN) guerrillas led by Scatu Swu and Zuheto. They were recruited into special Border Security Force battalions, com- mencing a very special arrangement to bring underground fighters back into the mainstream. This was followed by an agreement with the NNC in 1975, leading to the Shillong Accord.</p>

                            <p>Hardline groups, however, saw this as a sell-out and rejected the accord. Eventually, this led to the creation of the National Socialist Council of Nagaland (NSCN) in 1980, led by Isak Chisi Swu and Thuingaleng Muivah. The group further divided in 1988 into the Khaplang and Muivah factions, bringing in the element of internecine violence in addition to the existing counter-terrorism operations.</p>

                            <p>Yet another breakthrough was achieved with a ceasefire with the NSCN Isak-Muivah (NSCN-IM) faction in 1997, raising the possibility of peace after decades of violence. This was taken forward eventually through a framework agreement on 3 August 2015 between the NCSN-IM and the Government of India. However, a final settlement on the basis of this agreement it still in the works.</p>
                        </div>

                        <div class="war-details">
                            <h4>Mizoram</h4>

                            <p>A second violent movement, led by Laldenga, that challenged the state began in Mizoram on 28 February 1966. The situation in the area had been simmering with a rise in discontentment over time. However, allegations of neglect of the area came to the fore with the outbreak of ‘Mautam’, a rat famine caused by the flowering of bamboo plants. It began in 1958, but the food grain supplies to supplement the shortfall due to the famine only came in 1960. This was exploited by Laldenga, a fledgling political leader at that time. He started the Mizo National Famine Front (MNFF) and tried his hand at electoral politics in 1962, but reality came to the fore when his party won only two seats.</p>

                            <p>This led to a hardening of approach and the MNFF converted to Mizo National Front (MNF), eventually raising a banner of revolt with the formal outbreak of violent movement in 1966. The MNF launched Operation Jericho, which was unique in many ways. It was a rare instance of an insurgent group commencing hostilities in multiple, simultaneous, near-conventional military raids on Assam Rifle posts and government establishments. The result was a near sweep of major towns, with only the Assam Rifles post at Aizawl standing its ground and denying the rebels a clean sweep. This was quickly followed with the induction of the Indian Army, led by 61 Mountain Brigade to include 8 SIKH and 2 PARA.</p>

                            <p>Given the criticality of the situation, Aizawl was perhaps the only area which saw the employment of air power to control the precarious situation. The employment of air power did not have a severe physical effect in the area, but it did create a psychological impact. On the one hand, the region came face to face with the might of the Indian state and the kind of resources that were available if the need arose. On the other hand, it left an indelible impression on the people who continue to quote it as a harsh response on part of the state against them.</p>

                            <p>After the induction of the Indian Army into the area, the MNF steadily got pushed out of the major population centres and into the outlying regions of the state. It also exploited the porous borders and the presence of Chin tribes across to seek sanctuaries. In addition to Myanmar, Bangladesh too was a safe sanctuary for the guerrillas. This continued for a few years, until the 1971 Indo-Pak War made their presence inside Bangladesh untenable. This came as a big blow to Laldenga, who barely managed to escape the Indian forces and moved to West Pakistan.</p>

                            <p>In the meanwhile, realising the futility of violence, which was unlikely to achieve any substantive results, the civil society, and especially the church, played the role of responsible intermediaries. Channels of communication were also opened with Laldenga, who began negotiations with emissaries. It was a matter of time before the MNF signed an agreement with the government. The settlement also paved the way for elections to be held and Laldenga swept into power as leader of the MNF. It was a rare case of an insurgent leader becoming the elected chief minister of a newly formed state; and an example that continues to reinforce the importance of a political settlement to resolve violent movements in the country.</p>
                        </div>


                        <div class="war-details">
                            <h4>Manipur</h4>

                            <p>The insurgency in Manipur has proved to be the most complicated to resolve over the years. This is not as much because of the core demand of a particular group from the state, but more a result of competing rivalries amongst different groups themselves. The non-monolithic nature of the insurgency in the state has seen terror organisations pushing the interests of ethnic groups, which spill across district and state boundaries. This has further been aggravated by the hill–plains divide that exists between the Naga and the dominant Meitei tribes.</p>

                            <p>Manipur is one of the few states that can boast of a long, uninterrupted documented history and a rich cultural heritage. Post-independence, Manipur merged with India. This was resisted by certain sections, including Hijam Irabot Singh, the then maharaja’s brother-in-law. He clearly had leftist leanings and after the merger slipped away into Myanmar. He eventually took up arms against central rule in the state.</p>

                            <p>The dissatisfaction amongst a section of population was exploited and in 1964, the United National Liberation Front (UNLF) was created by Samarendra Singh, aimed at seeking national self-determination for the people of Manipur.</p>

                            <p>Immediately after the 1971 Indo-Pak War, Manipur was granted statehood in 1972. However, the insurgency that began in 1964 continued. In 1978, the People’s Liberation Army (PLA) was created by a breakaway leader of UNLF, Bisheshwar Singh, with the promise of support from China. A number of other Meitei groups came up over time in addition to the initial ones. The situation further worsened with the NSCN-IM taking up the cause of Nagas in Manipur, who dominated the hilly regions of the state. When one adds to this the Kukis and a number of other smaller tribes, the resultant challenging security situation becomes evident.</p>
                        </div>



                        <div class="war-details">
                            <h4>Assam</h4>

                            <p>Assam has witnessed multiple strains of insurgency in the state with specific and distinct causes. While the most prominent of these struggles was led by the United Liberation Front of Asom (ULFA), the state has also seen violence as a result of the Bodo movement.</p>

                            <p>The ULFA, an offshoot of All Assam Students’ Union (AASU), was created on 7 April 1979. Unlike a number of other organisations which took to violence immediately after their formation, the ULFA did not do so. This was also the time when the AASU was spearheading the agitation against illegal immigration into the state. The Assam Accord, signed in 1985, was the basis for identifying illegal migrants in the state. However, despite the Assam Gana Parishad (AGP)—formed by student leaders of the AASU—coming to power in 1985, the party failed to achieve the promised results.</p>

                            <p>Eventually, by 1988, the ULFA began its violent activities. The group also got in touch with some of the older militant organisations, like the NSCN, to seek weapons and support. What began as a relatively popular movement riding on the indigenous Ahom sentiment fast turned into a profit-making criminal activity. The ULFA started collecting large sums of money from traders, tea factory owners and the people. Over time, it became evident that there was a gap between the stated objectives of the group and the reality of their activities. This led to the launch of military operations by the Indian Army. What began with a moderately successful Operation Bajrang in 1990 was taken forward by a better planned and executed Operation Rhino.</p>

                            <p>A combination of disillusionment with the ULFA, success of the military opera- tions and division within the militant ranks finally led to a weakening of the move- ment. It was further challenged over time with India being able to foster better ties with neighbours, especially Bangladesh and Myanmar. This made it that much more difficult for groups like ULFA to seek shelter in the border areas.</p>

                            <p>While the ULFA fought for their Ahom identity and against foreigners, there were groups in the state which struggled for their respective rights. This includes the oldest inhabitants of the state, the Bodos. The Bodo struggle, though old, gained prominence with the creation of the All Bodo Students Union on 15 February 1967. What began with the demand for an autonomous council snowballed into the group seeking a separate state. By 1987, violent incidents began in the state. Unlike other areas, negotiations began soon thereafter and in 1993, the Bodo Accord was signed and the Bodo Volunteer Force laid down their arms as part of the settlement. In return, an autonomous council was established.</p>

                            <p>However, as has been seen in other areas of the country, dissatisfied splinter groups came to the fore and continued with the violent movement and agitations. The Bodo agitation also saw a similar trajectory, with groups like the Bodoland Liberation Tigers Front and National Democratic Front of Bodoland emerging as the two opposing sides, divided along ethnic lines.</p>
                        </div>

                        <div class="war-details">
                            <h4>J&K</h4>

                            <p>The incidents in J&K may have manifested into a violent movement in 1988, how- ever its roots run deeper. In addition to the chequered history of the state prior to independence, which saw different groups dominate the region over centuries, including the Afghans, Sikhs and Dogra rulers, a sense of victimhood had become an abiding factor. Also, the disputed border between India and Pakistan and the latter’s repeated attempts to employ force as a mechanism to resolve it further complicated the political and security situation in the area.</p>

                            <p>The delicate balance that was struck immediately after independence through the special status that the state was given in accordance with Article 370 was exploited by the local political leadership to retain political influence and question the issue of accession of Kashmir. Simultaneously, the special status of the state was diluted over a period of time. After covert military attempts by Pakistan in 1947–48 and 1965 to seek control over Kashmir failed to create an uprising within the state, conventional force was employed in a bid to wrest the region. This, however, did not yield the desired results. It led Pakistan to seek other options, including terrorism, as alternative proxy measures.</p>

                            <p>It is important to recall that Pakistan’s ability to force a military decision on India was severely denuded after the decisive defeat in the 1971 Indo-Pak War. Commencing from 1979, the Pakistani state perfected the art of exploiting instruments of hybrid warfare in Afghanistan. They further honed it simultaneously against India in Punjab.</p>

                            <p>As a result, by the time a decision was taken in Islamabad to fight India yet again, the primary constituents of choice for waging war had undergone a change. Instead of conventional weapon systems, it was terrorism, subversion, information warfare and financing of terrorism that Pakistan chose to fall back upon. The policy of employing force was replaced by bleeding India through a thousand cuts.</p>

                            <p>Pakistan’s support for violence and separatism began with its backing of groups like the Jammu and Kashmir Liberation Front (JKLF). However, it was soon realised that the agenda of an independent state did not suit Pakistan. Accordingly, Hizbul Mujahideen (HM) was created, which pushed for the merger of the state with Pakistan. The differences between the groups were resolved through internecine clashes, which saw the JKLF virtually decimated.</p>

                            <p>With the HM also finding it difficult to stand up to the pressure exerted by the Indian security forces, a number of different terrorist organisations were created inside Pakistan, with members from both Afghanistan and Pakistan, directly trained, funded, supported and directed by the Pakistan Army. Names like the Lashkar-e-Taiba (LeT), Harakat-ul-Ansar (HuA) and Al-Jihad, followed by the Jaish-e-Mohammed (JeM), came up with the common characteristic of support from Pakistan.</p>

                            <p>The pinnacle of their terror activities was witnessed with the attack on the Indian Parliament in 2001, followed by the sensational terrorist strike at Mumbai in 2008, nearly pushing India to war with Pakistan.</p>

                            <p>Even as intermittent terrorism continued, a shift in the Indian government’s approach became evident through its declaratory policy of counter strikes against Pakistan. While the first strike across the Line of Control (LoC) took place in 2016 as a reaction to Pakistan-guided attack on the Uri army camp, the second took place in 2019 after a suicide attack against a convoy at Pulwama in 2019. The air strike at the JeM terrorist camp at Balakot, after the Pulwama attack, raised the bar of India’s response and made the country’s intent clear. It reinforced the policy of zero tolerance for terrorism.</p>
                        </div>

                        <div class="war-details">
                            <h4>Punjab</h4>

                            <p>The state witnessed the challenge of terrorism that emerged from an attempt to artificially exploit insecurities within a section of the population. These sentiments were promoted for seeking electoral advantage, until a stage was reached wherein the Sikh extremist views were hijacked by separatists, with support from Pakistan, to give rise to violence within the state.</p>

                            <p>The first phase of the uprising got out of hand, after Jarnail Singh Bhindranwale took refuge with amassed weapons and militant volunteers inside the Golden Temple. It culminated with Operation Blue Star. This operation saw the Indian Army being given the unenviable task of evicting terrorists armed with sophisticated weapons, including medium machine guns and rocket launchers, from the Golden Temple at Amritsar in 1984. The operation was successfully completed, though with heavy losses being suffered by the security forces.</p>

                            <p>The follow-up period witnessed the continuation of terrorism, which steadily transformed into a criminal activity, thereby losing the limited support it had. The second attempt at taking over the Golden Temple by terrorists led to a more clinical operation. As part of Operation Black Thunder in 1988, the surrender of terrorists was ensured after their protracted isolation inside the temple.</p>

                            <p>The atrocities by the terrorists, sustained capacity building of the police force under Julio Ribeiro and KPS Gill, and support from the Indian Army and the political leadership saw the terrorism movement being decimated by the early 1990s, bringing the bloody chapter of violence in the state to a closure.</p>


                        </div>
                    </div>



                    <div id="lwe" class="accordion-details">
                        <h3>Left-wing Extremism (LWE)</h3>

                        <div class="war-details">
                            <!-- <h4>Nagaland.</h4> -->
                            <p>After independence, India’s tryst with LWE began immediately with the Telangana uprising, which began in 1946 and continued till 1952. Despite the violence and loss of lives that the struggle caused, it failed to successfully challenge the might of the Indian state, both pre- and post-independence.</p>

                            <p>Though left-wing struggles predate independence in 1947, more often than not, the Naxalbari uprising by a group of poor peasants against their landlords in 1967 is considered as the commencement of LWE in India. This peasant struggle made limited gains, but it became symbolic of peasant revolt against injustice and exploitation. It also led to similar struggles being waged in other states, like Bihar, Jharkhand, Chhattisgarh, Maharashtra, Andhra Pradesh and West Bengal. Over time, a number of splinter groups emerged, with little in terms of synergy in their actions. This changed in 2004, when the Maoist Communist Centre merged with the People’s War Group to form the Communist Party of India (Maoist). This led to a major surge in Maoist ranks and their ability to fight against the security forces. In 2006, Prime Minister Manmohan Singh termed the Maoist challenge as the most serious internal security threat. This was reinforced by a series of major attacks by the Maoists thereafter, over the next few years.</p>


                            <p>The increasing spread of Maoist influence led to the description of a ‘red corridor’ from Pashupati to Tirupati, with a total of 220 districts of the country being affected by its impact at one time. The government launched a two-pronged initiative aimed at development of areas impacted by Naxal violence and widespread deployment of security forces. This succeeded in curbing violence to a great extent commencing 2011. The graph of LWE violence has since been on the decline, even though the threat persists over a reduced yet significant area of the country, covering approximately 53 districts by 2020.</p>

                            <p>The LWE example illustrates the successful employment of alternative security forces instead of the army in a direct role.</p>
                        </div>

                    </div>


                    <div id="terrorism" class="accordion-details">
                        <h3>Analysis of Insurgency and Terrorism in India</h3>

                        <div class="war-details">
                            <!-- <h4>Nagaland.</h4> -->
                            <p>The constraints of space limit the details that can be provided for each of the insurgency or terrorist movements fought by the state and its constituents. However, this brief backdrop of some of the major insurgencies provides some important conclusions. These, in turn, help understand the nature and character of sub-conventional violence that has been witnessed in India, as also the distinguishing characteristics that define them. This is especially relevant since each of these challenges required a distinct approach. Indeed, this is perhaps one of the most important lessons that can be learnt from India’s experience in countering terrorism and insurgency.</p>

                            <p>Past experience with violent movements in India suggests that insurgencies were a preserve of the periphery, which had remained comparatively neglected in the past. Its emergence in other areas of the country clearly suggested that dissatisfaction, real or perceived, irrespective of its location, can potentially give rise to such movements. India’s experience also suggests that the role of the armed forces was far more involved with such movements than should have ideally been the case. In some instances, it continues to remain so. This is despite the fact that a large number of organisations have seen their capacities enhanced, as is the case with the Central Reserve Police Force (CRPF) or even the state police organisations. Other specialist organisations have been raised specifically to fight terrorism, like the Rashtriya Rifles. Yet, the army remains involved in fighting these movements. While fighting terrorism is considered a secondary responsibility of the army, how it affects the primary role of fighting external threats and challenges has often been the subject of discussion over the years.</p>


                            <p>The increasing spread of Maoist influence led to the description of a ‘red corridor’ from Pashupati to Tirupati, with a total of 220 districts of the country being affected by its impact at one time. The government launched a two-pronged initiative aimed at development of areas impacted by Naxal violence and widespread deployment of security forces. This succeeded in curbing violence to a great extent commencing 2011. The graph of LWE violence has since been on the decline, even though the threat persists over a reduced yet significant area of the country, covering approximately 53 districts by 2020.</p>

                            <p>The incidence of foreign involvement in India’s insurgencies is also not new. In the 1960s and 1970s, China provided active support to groups in North-East India. Pakistan too was involved since the same period. While its efforts received a setback after the loss of East Pakistan in 1971, the policy of employing terrorism as a state instrument continues till date. In fact, in the case of Pakistan, this support, from being peripheral to the larger fight against India, became central since the late 1980s. In that context, the involvement of the army and the description of sub-conventional operations underwent a shift. Increasingly, actions of Pakistan in J&K, and other parts of the country as well, were viewed more as a hybrid war against India, than merely as terrorism.</p>

                            <p>Despite the involvement of the army, it needs to be emphasised here that the resolution of such violent movements was always seen through the perspective of a politically negotiated settlement. The employment of the army, or for that matter, the security forces, was only to create conditions for a democratically elected government to undertake its mandated responsibility. This was evident in the case of Mizoram, Punjab and to a great extent, in each of the regions where terrorism emerged in a major way in the country.</p>

                            <p>The nature of India’s federal structure, where law and order are state subjects, created understandable but real challenges to fight violent movements. In a number of cases, the states did not have the requisite capacity in terms of training and equipment profile of the police. This led to a number of movements spiralling out of their control even prior to the central forces being employed. One of the most dangerous situations was created in Mizoram in 1966, when the MNF was able to nearly take over the state security apparatus in a surprise attack. This led to the immediate deployment of the army to control the fast-deteriorating security situation. Further, in a number of cases, the upswing in violence went beyond the ability of even the central police organisations, as was the case in J&K. However, this trend has seen a degree of correction, with police and central police organisations being employed to counter violence in LWE areas with an appreciable degree of success.</p>


                            <p>A closer look at the causes of violent movements in India indicates a variety of reasons for the same. While some movements had their roots in India’s historical legacy prior to its independence, as has been repeatedly asserted by the Nagas and the Kashmiris, others were due to insufficient attention to local sensitivities and demands, as seen in Mizoram and Manipur. In the case of the former, the mishandling of the Mautam famine became the tipping point, while in case of the latter, the failure to recognise the rich cultural and historical legacy of the state provided dissenters with the very reason that they could exploit. Similarly, in Kashmir, the perception of election rigging in 1987 added to the dissatisfaction amongst a section of people within the state.</p>

                            <p>Amongst the differentiating factors associated with different violent movements in India is the manner in which they financially supported themselves. While the model of early insurgencies indicates a localised form of collecting funds, latter-day terrorism has been more globalised in this respect. The insurgencies in the North- East, as also LWE, were supported by collecting funds through an illegal taxation system or different forms of extortion. This included levies on businesses, transportation services, industries and even criminal activities, including kidnapping. On the contrary, in the later years, in areas like J&K, a vast majority of funding emanated from Pakistan through state support, fake Indian currency notes (FICN) printed there, drug money funnelled, charities and fund raisers that were allowed to function with impunity. This suggests peculiar models for specific areas. However, as a country, India has experienced a hybrid model of terror funding with multiple sources of funding.</p>
                        </div>

                    </div>


                    <div id="evolution" class="accordion-details">
                        <h3>Doctrinal Evolution</h3>

                        <div class="war-details">
                            <!-- <h4>Nagaland.</h4> -->
                            <p>Having seen a brief historical perspective of low-intensity conflicts in India, as well as some of the salient takeaways, it would be useful to attempt an understanding of India’s doctrinal thought that accompanied the trajectory of these movements.</p>

                            <p>The absence of open and public articulation of India’s counter-terrorism doctrine raises a challenge for identifying fundamental trends that have guided the actions of the state. Even when attempts have been made to articulate doctrines in the past, these have come at the behest of individual state entities, like the army, without necessarily the complete involvement of other state agencies. The Indian Army brought out its sub-conventional doctrine both within the classified and unclassified domains.</p>


                            <p>Commencing with pre-independence handling of sub-conventional military operations, it becomes evident that the initial focus of operations undertaken by the state was on suppression of rebellious, violent tribes on the periphery of the British Indian Empire. This included not only the Afghans but also North-East India.</p>

                            <p>The tactical guidelines laid down clearly suggest the presence of a punitive policy for dealing with uprisings. The suggested measures addressed those who opposed the British government through violent means, and the villagers too, in a bid to pressurise the militant cadres. The inclusion of measures such as destruction of villages and crops represented steps that were aimed at instilling fear and caution amongst the tribes. These were quite obviously regressive in their intent and objective. It was certainly not the intent of the state to win over the population. The objective was to maintain control through penal policies. This was the first exposure for the British Indian Army in the fight against violent uprisings within the sub-conventional domain.</p>

                            <p>After the interlude of World War II, wherein the Indian Army performed admirably, though within the conventional military sphere, the post-independence period brought the first opportunity to fight internal conflicts in Nagaland. Unlike the guidance in the pre-independence period, this time, as brought out earlier, both the political and military leadership took measures to specially communicate with the troops. They reinforced the government’s humane intent and policy outlook towards such violent uprisings and the people of the region.</p>

                            <p>This, in some ways, became the primary building block for security forces in general, and the Indian Army in particular, for the implementation of their mandate in the region. This is not to suggest that this guidance led to a blemish-free deployment of the security forces thereafter; and it is evident from some of the cases that came to the fore. However, for an army that was used to conventional warfare against an identified enemy, it was important to make this distinction. While the impact of this guidance may not have been immediate, over time its influence started becoming evident.</p>

                            <p>The conventional orientation of the army also became evident in its tactical deployment and operations. From the size of sub-units deployed to the conduct of operations and the tactics employed, the teachings of conventional military operations clearly had an impact on how units operated. Lieutenant General (Lt Gen) Thomas Mathew says that ‘none of the battalions that were from 61 Mountain Brigade had any experience of counter insurgency.’</p>

                            <p>The commencement of army’s deployment in counter-insurgency was characterised by a policy of ‘search and clear’. This implied that the battalions that were deployed in these areas would venture out in a bid to conduct search operations on the basis of either intelligence about insurgent movement or as part of area domination. In either case, the presence of the army patrols would deter insurgent from moving in those parts with impunity. Over time, the balance of domination by insurgents would shift to greater control by the security forces and by co-relation, of the state. This was seen as an essential prerequisite for ensuring the reimposition of state control in some of these areas.</p>

                            <p>However, when it was realised that a temporary—and at times, fleeting—presence of security forces was inadequate to maintain control over areas, this policy shifted to ‘clear and hold’ in the late 1960s and the early 1970s. This implied that now instead of merely staying in an area for a short duration of time, the security forces created a temporary post or a temporary base in that area. It facilitated the presence of security forces at multiple locations, which made the domination of the area that much more effective. This system of creating a grid worked that much more effectively, especially given the dual responsibility of reinforcing state presence and domination in the area, thereby convincing the population of state control. It also facilitated faster reaction to information, given limited availability of fast mobility assets like helicopters. This was all the more relevant in areas of the North-East, with its thick jungles, where ground mobility remained a challenge at most times of the year.</p>

                            <p>Changes in the approach of the army also became evident from a shift in policies that were inspired from successes in Malaya, where the British forces employed the concept of separated villages. This led to villages being created along road arteries, within the direct supervision of security forces. Further, it ensured that guerrilla cad- res could not access these villages for sustenance and support, as was the case with smaller hamlets spread around the countryside.</p>

                            <p>This experiment was also attempted by the Indian government in Nagaland and Mizoram for similar reasons. Experience, however, showed that it caused grave inconvenience to the people, without accruing the intended benefits. Resultantly, the experiment was not repeated, indirectly acknowledging its limitations.</p>

                            <p>Over time, the army began relying more on intelligence-based operations that could be undertaken clinically, instead of merely area domination or large-scale cordon and search. Such operations not only gave better results, but they also reduced the inconvenience to the local population. Also, in the long run, it facilitated better cooperation from the local people and simultaneously made operations more result oriented.</p>

                            <p>In addition to these changes at the operational level, certain clear doctrinal principles were established over a period of time. While subtle changes were made according to the specific area of operations, the broad parameters have remained consistent.</p>

                            <p>The first principle that has guided India’s actions is the primacy of political solutions in resolving internal security conflicts. Military force has been applied only to bring down levels of violence to enable the democratic process to be reinvigorated. Examples of this policy are evident in Nagaland where the government entered into the Naga accord in 1975 with the NNC, even though it did not lead to lasting peace. Efforts in this direction continue with ongoing talks with major Naga militant factions, to seek peace in the region on the basis of the framework agreement signed. Similarly, the recommencement of the political process in Mizoram in 1986 resulted in the MNF getting back into the political mainstream and the rebel leader, Laldenga, getting elected as the Chief Minister of the state.</p>

                            <p>The second principle that has been established over a period of time is that of minimum force. The Indian Armed Forces, possibly with the exception of employing air power fleetingly in Mizoram in the initial phase of operations in an offensive role, have desisted from using all forms of heavy-calibre weapons, including air power and artillery guns. This is in contrast to how armed forces have operated in India’s neighbourhood as well as elsewhere in the world. The positive impact of this policy has been the ability to reduce collateral damage to the very minimum, thereby reducing alienation of the local population. However, often, the junior military leadership has had to pay a heavy price in terms of battle casualties, given the need to get closer to the source of fire to neutralise it.</p>

                            <p>The doctrine of distinction has also been a characteristic of the armed forces response and has fully been supported by successive governments. This has taken place at a number of levels. At the very fundamental level, the guidance of the 1950s remains paramount in counter-insurgency areas, wherein a clear distinction is made between the local population and those who pick up arms against the state and the people. It remains the endeavour of the security forces to limit the inconvenience to the locals, who often tend to become unwilling victims of the circumstances prevailing in disturbed areas.</p>

                            <p>Distinction is also made between local insurgents and foreign terrorists. This is primarily because the locals are seen as those who have deviated from the path of peace and can be brought back into the mainstream. Examples of this option have been quoted earlier. However, foreign terrorists who infiltrate across the borders come into the country only with the intent of causing death and damage to property. They do not have any intent to negotiate. It is the latter who leave little option for the security forces but to neutralise them. In contrast, the former are always provided with the option of surrender or negotiating a settlement.</p>

                            <p>A distinction is also made between the employment of terrorism by an external power to achieve its strategic ends and the use of violence by disgruntled elements from within the country. The example of Pakistan is most appropriate here to illustrate the employment of terror as a strategic tool, which has been evident for decades. To begin with, subversion and terrorism were employed to aid conventional military means, as was witnessed in 1947–48, 1965 and during the Kargil conflict in 1999. However, since 1988, a new reality was acknowledged by the military hierarchy in Pakistan. It was realised that the focus needed to shift towards sub-conventional means, instead of reliance on major wars, to achieve strategic objectives. The ongoing terrorism in J&K, as well as terror incidents across the country over the last three decades, illustrate this shift.</p>

                            <p>It is in the face of such challenges and threats that India’s policy, and more specifically that of the army, has also undergone a change. Commencing from 2016, it was decided that the Indian security forces will employ a punitive approach against cross-border terrorism. This manifested in the cross-border strike against terrorist camps after the attack on the Uri military camp in 2016. Thereafter, in 2019, after the terrorist strike at Pulwama, a cross-border air strike was undertaken at Balakot, yet again against a terrorist camp. These actions were aimed at sending a clear message to terrorists and their handlers that India reserved the right to hit back at the perpetrators of terrorism at a time and place of its choosing.</p>


                            <p>By raising the force level on the escalatory ladder and making such responses a part of its declaratory policy, India changed its approach towards terrorism from reactive to proactive. In the past, the people and the government could only wait for the next major strike, unclear of its location and intensity. As a result of the changed policy, it was Pakistan that was unsure of the scale and scene of India’s strike. India’s actions also called out Pakistan as a state employing terrorism. This was accompanied by a call to major powers to support India’s right to protect its people in self-defence. With the expected exception of China, India succeeded in achieving the support of major powers for its right to strike terrorists in self-defence.</p>

                            <p>India’s diplomatic success has not been limited to support for its counter-terrorism strategy. It has also been evident within the United Nations, wherein major terrorist groups, like the LeT, Harkat-ul-Mujahideen (HuM) and JeM, have been put on the list of proscribed terrorist organisations; in some cases, despite resistance from China.</p>


                            <p>Pakistan also has come under pressure from the Financial Action Task Force (FATF), a body which lays down guidelines to safeguard the international financial system from money laundering and terrorism finance. The mutual evaluation of Pakistan’s existing structures, procedures and enforcement reveals major gaps, as a result of which the country has been consistently placed on the ‘grey list’. This puts pressure on it to not only bridge existing systemic gaps but also take action against proscribed terrorist groups and leaders. The potential financial impact of such listing and the international humiliation suffered for being highlighted repeatedly for its omissions and commissions has achieved appreciable results in the fight against terrorism and its sponsorship by Pakistan.</p>


                        </div>

                    </div>


                    <div id="conclusion" class="accordion-details">
                        <h3>Conclusion</h3>

                        <div class="war-details">
                            <!-- <h4>Nagaland.</h4> -->
                            <p>India has experienced over seven decades of low-intensity conflicts and has been fighting insurgency and terrorism. A brief account of the experiences reveals that despite these being within the confines of the same country, there has been variance in the causes, trajectory and culmination of each. Others continue to remain a challenge despite decades of violence.</p>

                            <p>The historical survey also indicates that in spite of being the instrument of last resort, the Indian Army has often been at the forefront for fighting these movements. In doing so, there has been an evolutionary process to the approach adopted by the army. Valuable lessons have been learnt and these, in turn, have enhanced the quality and impact of the army’s presence in disturbed areas.</p>


                            <p>The increasing spread of Maoist influence led to the description of a ‘red corridor’ from Pashupati to Tirupati, with a total of 220 districts of the country being affected by its impact at one time. The government launched a two-pronged initiative aimed at development of areas impacted by Naxal violence and widespread deployment of security forces. This succeeded in curbing violence to a great extent commencing 2011. The graph of LWE violence has since been on the decline, even though the threat persists over a reduced yet significant area of the country, covering approximately 53 districts by 2020.</p>

                            <p>Despite the evolving nature of the army’s approach, a common thread is evident in how over the decades, the institution has remained an integral contributing element to national security and nation building. Right from the time of India’s independence, there was clarity amongst the army’s senior leadership that its role, in terms of providing aid to civil authorities, was merely to support, aid and strengthen the national framework of democracy. More than 70 years later, this thought is deeply imbibed into the army rank and file and has contributed, to a great extent, to the institution being as successful as it has been. It is looked up to for stability and peace, whenever a crisis situation develops and is beyond the control of the local authorities.</p>

                            <p>Due to the absence of a national sub-conventional doctrine, it may not be easy to formally define the Indian way of fighting such conflicts. Yet, this chapter clearly suggests that an Indian model for seeking peace and reconciliation does exist. It also suggests that India’s model, and the army’s role as a part of it, has a number of lessons that can be drawn for deriving best practices. These do not only emerge from the successes but also from the failures suffered over the years.</p>

                            <p>One of the most important takeaways is the need for an open and free assessment of such experiences and the desirability to document them to ensure that requisite learning value can be derived. It will also assist in building the strengths and eliminating, or at least limiting, the weaknesses observed during army’s deployment in low-intensity conflicts.</p>
                        </div>

                    </div>


                    <!-- <div id="gallery" class="row image-gallery">
                    <div class="col-md-12">
                        <h3>Gallery</h3>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img5.jpg" class="img-fluid" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>


                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img6.jpg" class="img-fluid" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>


                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img7.jpg" class="img-fluid" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>


                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img8.jpg" class="img-fluid" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>


                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img13.jpg" class="img-fluid" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>


                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img9.jpg" class="img-fluid" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                </div> -->


                </div>
            </div>
        </div>
</section>


<?php get_footer(); ; ?>