<?php 

$main ="operaitons";

$page="maldives";



get_header(); ; ?>


<section class="post-independence-war-banner"
    style="background-image: url(../assets/img/post-independence-war-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">Maldives Operation Cactus 1998</h1>
    </div>
</section>


<section class="indep-war" id="faq-section">
    <div class="container">
        <div class="row">
            <?php include('../sidebar/operations-sidebar.php'); ?>

            <div id="india-and-un" class="col-md-9">
                <div class="content">
                    <img src="../assets/img/changeable-img.jpg" width="100%" height="400px" class="img-fluid res-image"  alt="" loading="lazy">
                </div>


                <div id="maldives" class="accordion-details">
                <h3>Maldives: Operation Cactus 1998</h3>

                    <div class="war-details">
                    
                        <p>Maldives is an island nation in the Indian Ocean, composed entirely of about 1,190 low-lying coral islands, grouped in 26 atolls sprawled over nearly 90,000 sq km. Its northern tip is located about 600 km south of the Indian landmass. In 1988, the country had a police force called National Security Service (NSS) but no armed forces.</p>

                        <p>Operation Cactus was a military operation undertaken by the Indian Armed Forces in 1988 in response to an attempted coup in Maldives by a group led by Abdullah Luthufi, in order to overthrow the government of President Maumoon Abdul Gayoom. Luthufi was a Maldivian businessman and Gayoom’s political rival. The group was aided by armed mercenaries of the People’s Liberation Organisation of Tamil Eelam (PLOTE), a Sri Lankan Tamil secessionist organisation. Maldives had seen earlier coup attempts in 1980 and 1983 as well; however, it is coup attempt of 1988 that saw the involvement of the Indian Army.</p>

                        <p>On the dawn of 3 November 1988, an armed party consisting of approximately 80 Sri Lankan Tamil militants from PLOTE, operating as mercenaries for Luthifi had launched a seaborne attack on the capital, Male. By 0700 Hours, they were in con- trol of most of Male, including the Presidential Palace; President Gayoom, however, managed to escape. On failing to secure the President, the mercenaries resorted to taking hostages from among key government functionaries. The NSS Headquarters was also under siege. At 0830 Hours, the Indian High Commissioner in Sri Lanka, JN Dixit, received a call from his counterpart in Maldives, who informed him that some armed men had attacked and taken over Male.  President Gayoom’s request for armed assistance was also conveyed to New Delhi.</p>

                        <p>India accepted President Gayoom’s request swiftly. The Service Headquarters were warned and contingency planning commenced immediately and warning orders were issued to the troops and aircraft likely to be deployed. It must be remembered that this was in the midst of the Indian involvement in Sri Lanka via the IPKF.</p>

                        <p>Given the urgency of the situation, the Indian officials swiftly put together a plan of action. An airborne operation was considered best suitable. Thus, Air Marshal Denzil Keelor was called up and asked to prepare an IAF team for the Maldives. It was agreed that for the operation, soldiers would be sent out from the Parachute regiments; the troops were trained for landing in hostile terrain, being self-reliant and taking quick, effective action in tough conditions. The 50 (Independent) Parachute Brigade (popularly known as the Para Brigade), commanded by Brig Farukh Bulsara, located at Agra, was the only one of its kind in India. As per Bulsara’s initial plan, a company group with Battalion HQ was to airland at the Hulule airport, provided the airfield was in safe hands.</p>


                        <p>A truncated company of 60–65 men would jump from the IL-76 aircraft, if the need arose. Another IL-76 would airlift a company group with a skeleton Tactical HQ. After reviewing and refining several times before the aircrafts got airborne, it was decided to airland 6 Para Battalion and 411 Para Engineers.</p>

                        <p>Government clearance for the operation was received at 1215 Hours and the first two IL-76 Aircraft, with elements of the 6th Battalion of the Parachute Regiment, and the 17th Parachute Field Regiment, took off from Agra between 1556 Hours and 1558 Hours. They flew nonstop over 2,000 km. Of the Para Brigades, 6 PARA was designated to lead the operation but the unit’s troops were scattered far and wide; 7 PARA had been designated for deployment in the northeast; and 3 PARA was commited elsewhere.</p>

                        <p>The very first information that came in suggested that Hulule airfield was under control of the NSS. However, the Indian troops were prepared for an airborne assault to capture the airfield if need arose. </p>

                        <p>The first two aircrafts that landed discharged 316 troops and one RCL Jeep. In the process, while some troops secured the airfield for the purpose of further landing, one of the parties immediately crossed over to Male and by 0235 Hours on 4 November 1988, it had reached the vicinity of President Gayoom’s residence and ensured his safety. Indian Army paratroopers reached Hulhule within a mere nine hours since the appeal from President Gayoom was received. By 0430 Hours, the Presidential Palace was secured and Male was freed, followed by the surrender of attackers during the morning of 5 November 1988. Frigates of the Indian Navy intercepted the hijacked freighter in which the mercenary force had escaped, off the Sri Lankan coast.</p>


                        <p>Though there was limited fighting in Operation Cactus but what remains commendable is the speed and the efficiency with which the Indian Armed Forces reacted and carried out the operation. It was also demonstrative of India’s willingness and ability to aid a neighbouring country. The successful execution of Operation Cactus was instrumental in averting a political crisis in India’s immediate neighbourhood. The operation also strengthened Indo-Maldivian relations as a result of the successful restoration of the President Gayoom’s government.</p>


                        <p>The Indian troops deployed in Operation Cactus were de-inducted between 5-12 November 1998. A small contingent stayed behind at the request of the Maldivian Government to provide security in the immediate aftermath of the attempted coup and to further train the National Security Service, and returned to India on the completion of their tasks.</p>
                        
                    </div>

                </div>

                    <!-- <div id="note" class="accordion-details">
                        <div class="pawan-war-details">
                            <h4>Note</h4>
                            <div class="pawan-note">
                                <ol>
                                    <li>Singh, ‘Times of Trial’, n. 21, p. 178. </li>
                                    <li>Ibid.</li>
                                    <li>Singh, Mission Overseas, n. 48, p. 23.</li>
                                    <li>Ibid., p. 27</li>
                                    <li>Ashok K Chordia, Operation Cactus: Anatomy of One of India’s Most Daring Military Operations, New Delhi: KW Publishers, 2018, p. 83.</li>
                                    <li>Ibid., p. 34. </li>
                                    <li>Singh, ‘Times of Trial’, n. 21, p. 179.</li>
                                    
                                </ol>
                            </div>
                        </div>
                    </div> -->

               


                <!-- <div id="gallery" class="row image-gallery">
                    <div class="col-md-12">
                        <h3>Gallery</h3>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">

                            <img src="../assets/img/img5.jpg" class="img-fluid"  alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>


                    <div class="col-md-4 main">
                        <div class="image-main img-pop">

                            <img src="../assets/img/img6.jpg" class="img-fluid"  alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>


                    <div class="col-md-4 main">
                        <div class="image-main img-pop">

                            <img src="../assets/img/img7.jpg" class="img-fluid"  alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>


                    <div class="col-md-4 main">
                        <div class="image-main img-pop">

                            <img src="../assets/img/img8.jpg" class="img-fluid"  alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>


                    <div class="col-md-4 main">
                        <div class="image-main img-pop">

                            <img src="../assets/img/img13.jpg" class="img-fluid"  alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>


                    <div class="col-md-4 main">
                        <div class="image-main img-pop">

                            <img src="../assets/img/img9.jpg" class="img-fluid"  alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                </div> -->

 
            </div>  
        </div>
    </div>
</section>


<?php get_footer(); ; ?>