<?php

$main ="operaitons";

$page="vijay";

get_header(); ; ?>


    <section class="operations-banner" style="background-image: url(../assets/img/operations-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Operation Vijay 1961</h1>
        </div>
    </section>




<section class="operation-details" id="faq-section">
    <div class="container">
        <div class="row">
           <?php include('../sidebar/operations-sidebar.php'); ?>


            <div id="back" class="col-md-9">
                <div class="content">
                    <img src="../assets/img/post-indep-war-2.png" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                </div>


                
                <div id="back" class="accordion-details">
                <h3>Background</h3>

                    <div class="war-details">
                    
                        <p>The state of Goa became part of the Indian Union in 1961, a decade and a half after independence. Prior to that, it was a Portuguese colony beginning 1510 when Alfonso de Albuquerque gained control of Goa from Sultan Adil Shah of Bijapur. Under Portuguese rule as the Estado da India along with the territories of Daman and Diu and Dadra and Nagar Haveli, Goa was one of the last of colonial possessions in the Indian subcontinent to become part of independent India. The story of how Goa became part of the Union of India is the story of Operation Vijay, an armed, military operation involving the all three Services—the Indian Air Force (IAF), the Indian Navy, and the Indian Army.</p>

                        <p>The beginnings of nationalist stirrings in Goa date to the second decade of the twentieth century when the Goa Congress Committee was formed by Tristão de Bragança Cunha in 1928. In 1938, the Goa Congress Committee was made an affiliate of the Indian National ongress that was spearheading the nationalist movement in India. On 18 June 1946, a large- scale civil disobedience movement against Portuguese rule was organised by by Dr Ram Manohar Lohia and Tristão de Bragança Cunha, among others. The Goa liberation movement took a sharp turn when civilians started to boycott government offices and the Portuguese government took every possible step to quell the Satyagraha movement. After India gained independence in 1947, Goa remained in Portuguese hands. The Government of India approached the Portuguese in 1950 to begin negotiations on the status of Portuguese colonies in India, but the stand taken by the latter was that Portuguese possessions on the subcontinent were part of the territory of Portugal. The next few years saw diplomatic endeavouring on New Delhi’s part with the issuance of multiple aide-de-memoires to the Portuguese government, without much success. With waning chances for negotiations, in 1953, India thus closed its Legation in Lisbon and the door to a diplomatic solution to the issue appeared to have closed. ‘In July 1954, volunteers of the “Free Goa Movement” took possession of the Portuguese enclaves of Dadra and Nagar Haveli.’ 1 This further added to tensions between New Delhi and Lisbon.</p>

                        <p>By the end of the decade, it became evident that there were no diplomatic solutions to the issue and the Indian government increasingly warmed up to the idea that it might need to use force to resolve the issue. Accordingly, plans were drawn up for a possible military operation to handle the Goa issue in November 1961. In November 1961, ‘the Portuguese contingent based at Anjadip island, off the coast of Canacona, fired at the Indian merchant ship Sabarmati in a display of its maritime supremacy in the region. Subsequently, the Portuguese garrisons resorted to indiscriminate firing on Indian fishing boats off Karwar (in present-day Karnataka) and South Goa.’ 2 This provided the spark to move forward on military front. The incidents had precipitated matters to the point where India found no other option than to liberate Goa from the Portuguese by force.</p>

                        <p>Before the official commencement of hostilities on ground on 11 December 1961, the Indian Navy initiated Operation Chutney to re-establish the position of India in sea territory near Goa and retaliate against the Portuguese Naval forces. 3 The operation once again established the Indian Naval presence in area near Goa’s territorial waters. Later, along with Goa the other Portuguese territory of Daman and Diu were also liberated and joined the Indian Union on 19 December 1961.</p>
                        
                        
                    </div>

                </div>

                <div id="daman" class="accordion-details">
                <h3>Daman and Diu</h3>

                
                    <div class="war-details">
                        
                        <p>The advance on the enclave of Daman was conducted by the 1st Maratha Light Infantry Battalion under the command of Lt Col SJS Bhonsle in a pre-dawn operation on 18 December 1961. 1 MARATHA launched an operation under the cover of darkness and planned to overrun the airfield with stealth. The Indian Air Force (IAF) struck using Mystere fighters against enemy mortar positions guns inside Moti Daman Fort. At 0430 hrs, Indian artillery began to bombard Damao Grande. By the evening, the Indians had secured most of the colony, except the airfield and Damao Pequeno. The IAF launched six successive attacks, but the enemy still refused to surrender. The Indian forces then followed up with ground assault on the airfield which was eventually overrun.</p>

                        <p>Diu was a tiny island, off the coast of Gujarat, held by approximately 500 Portuguese soldiers, supported by artillery and the Patrol Boat Vega. While the main defenses were based in the Diu Fort, the Portuguese had covered the crossings with artillery, mortars and machine guns. India, on the other hand, had the advantage of air support and naval gunfire.</p>

                        <p>It was decided by the Indian forces to cross the creek exploiting darkness and launch silent attacks. Diu was attacked on 18 December from the north west along Kob Forte by two companies of the 20 RAJPUT and a company of 4 MADRAS. They attempted initial crossings but proved unsuccessful. It was 4 MADRAS that made the first attack on a police border post at 0130 hrs on 18 December at Gogol. At 0200, another attempt was made by 4 MADRAS which was repulsed. Although India had advantage of air support and naval gunfire, it could not be exploited at night time. Fresh air attacks were made at dawn on 18 December, with the ai of destroying Diu&#39;s fortifications which faced the mainland.</p>

                        
                        
                    </div>

                </div>


                <div id="goa" class="accordion-details">
                <h3>Goa</h3>

                
                    <div class="war-details">
                        
                        <p>D-Day for Goa was set for 18 December 1961, which was planned in two phases. 4</p>

                        <p>Phase I:</p>

                        <ul >
                            <li style="list-style: lower-latin;">Advance of 50 Para Brigade Group under Brigadier Sagat Singh along Dodamarg – Sanquelim – Usgao – Piliem.
                                <ul>
                                    <li style="list-style: lower-roman;">Secure Bicholim and Usgao bridges with a view to advancing on Ponda.</li>
                                    <li style="list-style: lower-roman;"> Secure Mapusa.</li>
                                </ul>
                            </li>
                            <li style="list-style: lower-latin;">Advance of 63 Infantry Brigade Group under Brigadier Kalwant Singh along Mollem – Ponda and Mollem – Collem – Ponda, to capture Ponda.</li>
                        </ul>

                        <p>Phase II:</p>

                        <p>This phase involved orders for the capture of Panjim and Marmagoa.</p>

                        <div class="war-2-map-2">
                            <img src="../assets/img/gallery/secure-the-territory.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 600;">Indian troops landing in Goa after the liberation to reinforce the initial columns and secure the territory.<br>Source: USI of India.</p>
                        </div>

                        <p>Hostilities at Goa operations commenced on 17 December 1961 with the capture of the border town Maulinguem. Brig Sagat Singh’s 50 Para Brigade Group planned an unconventional and bold operation which captured Doddu Morogu by the 1 Para on 17 December. A unit of Indian troops were sent to attack and occupy the town of Maulinguem. At 0400 hrs, the Indian assault commenced with artillery bombardment on Portuguese positions south of Maulinguem, launched on the basis of the false intelligence that the Portuguese had stationed heavy battle tanks in the area. They occupied Maulinguem in the north-east, killing two Portuguese soldiers.</p>

                        <p>A deceptive thrust, in company strength, was to be made from the south along the Majali-Canacona-Margao axis. Despite the 50th Para Brigade being charged with merely assisting the main thrust conducted by the 17th Infantry, its units moved rapidly across minefields, roadblocks and four riverine obstacles to reach Panaji at the first. Lt Col Sucha Singh’s 2 Para, which led the 50 Para Brigade along Bacholim-Ponda axis, secured Bicholim. At 0440 hrs, Portuguese forces destroyed the bridge at Bicholim, followed by the destruction of the bridges at Chapora in Colvale and at Assonora.</p>

                        <p>On the morning of 18 December, the 50th Para Brigade of the Indian Army moved into Goa in three columns. The western column, the main thrust of the attack comprised the 2nd Sikh Light Infantry (Sikh LI) as well as an armoured division, which crossed the border at 0630 hrs and advanced on Tivim. At 0530 hrs, Portuguese troops left their barracks at Ponda in central Goa and marched towards the town of Usgao, in the direction of the advancing eastern column of the Indian 2nd Para Maratha, which was under the command of Maj Dalip Singh Jind and included tanks of the Indian 7th Cavalry. By 0900 hrs, Indian troops had already covered half the distance to the town of Ponda. By 1000 hrs, Portuguese forces of the 1st EREC, faced with the advancing 2 Sikh LI by Lt Col RB Nanda advanced to Mapusa via Assonora, capturing Mapusa. By 1745 hrs, the forces of the 1st EREC and the 9th Cazadores Company of the Portuguese Battlegroup North had completed their ferry crossing of the Mandovi River to Panaji, just minutes ahead of the arrival of the Indian armoured forces. On 18 December, they reached the general line Betim-Piligao in the north and Banastarim-Ponda in the east.</p>
                        
                        <p>The same night, Maj Shivdev Singh Sidhu with a force of the 7th Cavalry decided to take Fort Aguada and obtain its surrender, after receiving information that a number of supporters of the Indian Republic were held prisoners there. On the Mollem Axis, Lt Col JD Bobb’s 3 SIKH led the advance of 63 Infantry Brigade under Brigadier Kalwant Singh Dhillon. The advancement was to be on two routes named the Yellow Axis and the Green Axis. The Yellow Axis was from Mollem to Collem track and then to Ponda. However, the Portuguese defenders of the fort had not yet received orders to surrender and responded by opening fire on the Indian forces, and Maj Sidhu and Capt Vinod Sehgal were killed in the firefight.</p>

                        <p>The order for Indian forces to cross the Mandovi River was received on the morning of 19 December, upon which two rifle companies of the 2 Sikh LI advanced on Panaji at 0730 hrs and secured the town without facing any resistance. The right column, consisting of the 2nd Bihar Battalion, and the left column, consisting of the 3rd Sikh Battalion, linked up at the border town of Mollem and then advanced by separate routes on Ponda.</p>

                        <p>Phase II of the D-Day Plan was to capture Panjim and Marmagoa subsequently.</p>
                    </div>

                </div>



                <div id="action" class="accordion-details">
                <h3>In Action</h3>

                
                    <div class="war-details">
                        
                        <p>Although the battle on Anjadip was in progress, the Indian Army and the IAF began closing in on Goa. Offensive action commenced with four IAF Canberra bombers strafing the Dabolim airfield on 18 December. A three-pronged advance was made by the army into Goa—from the north (Karwar), south (Sawantwadi), and the south-west (Belgaum). The seaward approach was controlled by the Indian Navy frigates, Betwa, Beas and Cauvery. These ships patrolled entrances to the ports of Marmagao and Panjim.</p>

                        <p>Operation Vijay was planned as an army operation with the IAF and the Indian Navy providing air and naval support. Maj Gen KP Candeth, who was commanding the 17th Infantry Division, at the time had 50 Parachute Brigade placed under him. The navy organised four Task Forces in order to accomplish the tasks that were being assigned: Surface Action Group (INS Mysore, Trishul, Betwa, Beas and Cauvery), Carrier Task Group (INS Vikrant, Delhi, Kuthar, Kirpan, Khukri and Rajput), Minesweeping Group (INS Karwar, Kakinada, Cannanore and Bimilipatan), and Support Group (INS Dharini). On the other hand, the air plans were kept flexible.</p>

                        <div class="war-2-map-2">
                            <img src="../assets/img/gallery/Goa.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 600;">Indian troops welcomed in Goa, 1961 <br><strong>Source: MoD, DPR.</strong></p>
                        </div>

                        <p>By the evening of 18 December 1961, most of Goa had been overrun by advancing Indian forces. Operation Vijay had ended in a success in less than 40 hours. About 4,668 people were taken prisoner by the Indian forces, which included both military and civilian personnel, Portuguese, Africans and Goans. The official Portuguese surrender was conducted in a formal ceremony at 2030 hrs on 19 December 1961 with the signing of the Instrument of Surrender by the last Portuguese Governor-General, Antonio Vassalo e Silva. Upon the surrender of the Portuguese Governor General, Goa, Daman and Diu were declared a federally administered Union Territory placed directly under the President of India, and Maj Gen Candeth was appointed as the military governor. Thus, 451 years of Portuguese rule in India came to an end and the Indian armed forces restored Goa and other Portuguese territories to India.</p>

                        
                    </div>

                </div>

                    <!-- <div id="note" class="accordion-details">
                        <div class="pawan-war-details">
                            <h4>Note</h4>
                            <div class="pawan-note">
                                <ol>
                                    <li>Maj Gen (Retd) LS Lehl, ‘Army of the Republic: Phase of Consolidation’, in Maj Gen (Retd) Ian Cardozo, AVSM, SM (ed.), The Indian Army: A Brief History, New Delhi: USI, 2007, p. 88.</li>

                                    <li>‘From Op Chutney to Op Vijay, How Goa was Liberated in 40 Hours’, Telangana Today, 19 December 2021.</a></li>

                                    <li>Newton Sequeira, ‘When the Navy’s Operation Chutney Evicted Portugal from Anjadiv’, The Times of India, 19 December 2020.</a></li>

                                    <li>Lehl, ‘Army of the Republic: Phase of Consolidation’, p. 89.</li>

                                    
                                </ol>
                            </div>
                        </div>
                    </div> -->


               

                
                <!-- <div id="gallery" class="row image-gallery">
                    <div class="col-md-12">
                        <h3>Gallery</h3>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">

                            <img src="../assets/img/img5.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img6.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img7.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img8.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img13.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img9.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>
                </div> -->
            </div> 
        </div>
    </div>
</section>




    
<?php get_footer(); ;?>