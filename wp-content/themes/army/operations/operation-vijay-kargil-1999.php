<?php

$main ="operaitons";

$page="kargil";

 get_header(); ; ?>


    <section class="operations-banner" style="background-image: url(../assets/img/operations-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Operation Vijay: Kargil 1999</h1>
        </div>
    </section>




<section class="operation-details" id="faq-section">
    <div class="container">
        <div class="row">
           <?php include('../sidebar/operations-sidebar.php'); ?>


            <div id="back" class="col-md-9">
                <div class="content">
                    <img src="../assets/img/post-indep-war-2.png" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                </div>


                
                <div id="back" class="accordion-details">
                <h3>Background</h3>

                    <div class="war-details">
                    
                        <p>The Jammu and Kashmir (J&K) region has had political and cultural connections with the Indian heartland for millennia. The state which acceded to India on October 26 1947, had five main regions: Kashmir, Jammu, Ladakh, Pakistan Occupied Kashmir and the Northern areas. Of a total area of 2,22,236 sq km, 78,114 sq km is under Pakistan’s occupation; of this, 5,180 sq km in the Shaksgam Valley was ceded by it to China in 1963 as part of a boundary settlement. Approximately 33,555 sq km in Ladakh (Aksai Chin) remains under Chinese occupation. Before 2019, J&K broadly consisted of three regions: Jammu, Kashmir Valley and the Ladakh region. Kargil is the second largest town of Ladakh after Leh, and lies at the junction of the Suru and Shingo Rivers in the Kargil district. It is roughly 200 km from Srinagar in the Kashmir Valley, located to the northwest of Zoji La with National Highway 1A from Srinagar to Leh passing through it.</p>

                        <p>In 1999, the Kargil sector was subdivided into the Dras, Kaksar, Channigund and Batalik sub-sectors. Kargil was the headquarters of 121 (Independent) Infantry Brigade, responsible for the Kargil Sector, that extends 168 km along the Line of Control (LOC), stretching from Kaobal Gali in the west (near Zoji La) to Chorbat La in the east (on the Ladakh range). The basic deployment of the 121 Infantry Brigade was: one Infantry Battalion at Dras, two Infantry Battalions and a Border Security Force (BSF) Battalion covering Kargil, and one Infantry Battalion at Batalik astride the Indus. The brigade came under 3 Division based at Leh in Ladakh after 1972.</p>

                      
                    </div>

                </div>

                <div id="cris" class="accordion-details">
                <h3>Build-up to the Crisis</h3>

                
                    <div class="war-details">
                        
                        <p>The Kargil crisis had several layers of significance for both Pakistan and India, though these were very different for the two countries. The LOC owes its existence to the Karachi Agreement of 27 July 1949, a consequence of the first India-Pakistan Conflict over Kashmir. The Karachi Agreement was signed by the Indian and Pakistan military commanders as well as by representatives of the United Nations Commission for India and Pakistan (UNCIP). Although both India and Pakistan had celebrated 50 years of independence in August 1997, an increase of artillery exchanges in the Kargil Sector was seen from the beginning of the year followed by heavy exchanges in 1998, especially in the aftermath of the nuclear tests.</p>

                        <p>Kargil had not been immune to the rising militancy in J&K in the 1990s, although the intensity here was less as compared to the Valley. In 1990, two militants were killed and one militant was captured in the west of Kaobal Gali. In November 1993, 27 militants were killed in Mashkoh Valley by 2/5 GR. In June 1997, four militants were killed at Kaobal Gali by 9 RAJ RIF. Also, in 1997, Tashi Namghyal of Garkhun village had reported the presence of unarmed Pakistani personnel in Yaldor, in the Batalik sector, which led to the establishment of a patrol base in Yaldor in the same year.</p>

                        <p>The conflict in 1999 was initiated when, as part of Operation Badr, Pakistani Army soldiers infiltrated into the Indian side of the LoC disguised as Kashmiri militants and covertly captured the vacated winter posts of the Indian Army. The aim was to cut the link between Kashmir and Ladakh, to isolate Indian Army troops on the Siachen Glacier, and force India to negotiate a settlement of the Kashmir dispute.</p>

                        
                       
                    </div>

                    <div class="war-2-map-2">
                        <img src="../assets/img/Kargil 1999-24.jpg"  class="img-fluid"
                            alt="" loading="lazy">
                        <p style="color: #D90000; font-weight: 500;">Kargil 1999</p>
                    </div>
                </div>



                <div id="res" class="accordion-details">
                <h3>Intrusions and Response</h3>

                
                    <div class="war-details">
                        
                        <p>The first indication of Pakistani intrusions into the area was reported and communicated to HQ 3 Division, which, in turn, passed on the information to 15 Corps and Northern Command. Then, on 3 May 1999, a similar intrusion was sighted. A group of three shepherds belonging to the village of Garkhun, located just above Indus River and Garkhun Barnala Junction, saw a group of armed men in Pathan attire digging bunkers in the area of Banju. This activity was reported to 3 PUNJAB, leading to the launch of a series of patrols. Immediate action was taken by the HQ 3 Infantry</p>

                        <p>Division and two battalions that moved from Siachen were positioned in the Batalik sector by 10 May 1999. HQ 70 Infantry Brigade, that had just moved to Dras was diverted to Batalik to take control of operations in this sector. The detection of intrusion in Batalik region increased the alertness in other sectors as well. After the initial detection of intrusion, the Indian Army’s response was rapid. The ‘situation report’ initiated by HQ 15 Corps on May 11 1999 shows that a comprehensive set of action had been initiated in the Yaldor Sector to establish contact with the intruders in order to fix the extent of the intrusion and contain it.</p>


                        <p>On 4 and 6 May 1999, 3 PUNJAB launched two patrols in the Batalik sector to investigate the reported intrusion. It was confirmed on 7 May and one company each of 10 Garhwal Rifles (GARH RIF) and 16 GRENADIERS was immediately moved to contain the intrusion. On 8 May, HQ 70 Infantry Brigade which was taking over at Dras was moved and made responsible for the Batalik sector. Soon thereafter, two battalions, 1/11GR and 12 JAK LI, that had just been de-inducted from Siachen and were readily available, were moved on 9 May and were in position in the Batalik sector by 10 May.</p>

                        <p>Enemy intrusion was detected in the Dras and Mashkoh Sectors on 12 May 1999 by a patrol of Ladakh Scouts, and in the Mashkoh sector by Army Aviation Corps helicopters on 14 May. The Battalion which had been moved from Valley on 9 May 1999 soon after the detections in the Batalik sector, was diverted to Dras and employed from 12 May onwards to contain the intrusion in that area. The 8 SIKH and 28 Rashtriya Rifles (RR) battalions were moved in on 14 May 1999. On 14 May, enemy intrusion was detected in Kaksar sector by a patrol of 4 JAT in the area of Pt 5299 South West Spur, commonly known as Bajrang Post. Initially, one company of 28 RR was released to contain the intrusion and, subsequently, on 21 May 1999, 14 JAK RIF moved for deployment in the Kaksar sector.</p>

                        <p>During the last week of April 1999, seven Pakistani helicopters were seen flying with underslung loads in Turtok. A patrol from 12 JAT sent to monitor activity along the Line of Control (LOC) was ambushed on 6 May. Subsequent patrols sent on 16 May and 19 May confirmed that the enemy had occupied the ridge line along and across the LOC at five locations. The 11 Rajputana Rifles (RAJ RIF) and 9 MAHAR battalions were tasked to occupy defences and the enemy was subsequently evicted by physical assault.</p>

                        <div class="war-2-map-2">
                            <img src="../assets/img/gallery/Kargil.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">Infantry soldiers going for assault during Kargil, 1999 <br><strong>Source: MoD, DPR.</strong></p>
                        </div>

                        <p>Once the enemy intrusion in Kargil and Turtok were established, five units   of Ladakh Scouts were moved to reinforce the Chorbat La sector. They occupied defences along the LOC during the period 18–31 May, and foiled all enemy attempts to intrude into the area. By 17 May 1999, it was confirmed that the armed intruders had occupied heights and were assessed to be about 200–250 in number in the Batalik Sector, between 80–100 in the Kaksar Sector, between 60–80 in the Dras Sector, and around 200–250 in the Mushkoh Sector.</p>

                        <p>All troops in J&K were placed on a state of high alert by the HQ Northern Command on 12 May. Initially, heavy firing and a number of casualties were wit- nessed in the attempt to clear the enemy position. On 22 May, the Indian Navy’s Western and Eastern fleets met up in the North Arabian Sea and began aggressive patrols. They prepared to blockade the Pakistani ports in order to cut off supply routes under Operation Talwar, which was the naval operation carried out by the navy during the Kargil War of 1999. It was one of the three operations carried out by the Indian Armed Forces during the conflict, the other two being Operation Safed Sagar by the IAF and Operation Vijay by the army.</p>

                        <div class="war-2-map-2">
                            <img src="../assets/img/Army Aviation Cheetah helicopters during Operation Vijay, Kargil 1999-26 (1).jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">Army Aviation Cheetah helicopters during Operation Vijay, Kargil 1999  <br><strong>Source: MoD, DPR.</strong></p>
                        </div>
                       
                    </div>

                </div>



                <div id="vij" class="accordion-details">
                <h3>Operation Vijay</h3>

                
                    <div class="war-details">
                        
                        <p>The first priority of Operation Vijay was to recapture peaks that were in the immediate vicinity of National Highway (NH) 1. This resulted in Indian troops first targeting the Tiger Hill and Tololing complex in Dras, which dominated the Srinagar-Leh route. Operation Vijay was a joint Infantry-Artillery endeavour to evict regular Pakistani soldiers of the Northern Light Infantry (NLI) who had intruded across the LoC into Indian territory and had occupied un-held mountain peaks at high-altitude and ridgelines. After many unsuccessful attempts to regain Tololing, the 18 GRENADIERS reached a point about 30 m below the Pakistani position on 10 June. The 2 RAJ RIF used this firm base on 12 June to launch the assault on Tololing. The attack commenced at 2300 Hours followed by securing of Point 4590 by early hours of 13 June 1999. The 18 GRENADIERS further pushed through 12 RAJ RIF to secure position 3 km ahead of Point 4590, which was to be used for attacking Point 5140, the highest feature of Tololing Complex. A concerted multi-directional attack was launched on 19 June 1999 by 18 GARH RIF, 13 JAK RIF and 1 NAGA, and the position was captured by 0335 Hours on 20 June.</p>

                        <p>While the Tololing area was cleared, the point 4075 complex northwest of Dras that dominated some 20 km of NH 1A remained. The task of capturing this feature was given to 79 Mountain Brigade; the attack commenced on the night of 4 July, followed by the capture of the objective by 17 JAT by 0500 Hours on 5 July.</p>

                        <div class="war-2-map-2">
                            <img src="../assets/img/Soldiers holding Indian tricolour atop one of the recaptured peaks-27.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">Soldiers holding Indian tricolour atop one of the recaptured peaks <br><strong>Source: MoD, DPR.</strong></p>
                        </div>

                        <p>Ground troops were supported by the IAF from 11–25 May and tried to contain the threat, judge the positions of the enemy dispositions, and carried out several pre- paratory actions. On 26 May 1999, the IAF came into picture. A series of special oper- ations, limited attacks and patrols were launched to gain information. Identifications revealed enemy brigades each in the Batalik and Kargil-Dras-Mushkoh sectors. Each brigade initially comprised two battalions of the Pakistan Army’s NLI, two companies of the SSG, and about 500-700 Frontier Corps troops. These were supported by some 15 artillery units and had the usual engineer, signal and administrative units. </p>

                        <p>The IAF was tasked to act jointly with ground troops during Operation Safed Sagar. On 27 May 1999, the IAF lost a MiG-27 strike aircraft piloted by Flight Lieutenant (Fl Lt) Nachiketa, which it attributed to an engine failure, and a MiG-21 fighter piloted by Squadron Leader (Sqn Ldr) Ajay Ahuja, which was shot down by the Pakistani army, both over the Batalik Sector. One Indian Mi-8 helicopter was also lost to Stinger surface to air missiles (SAMs). French-made Mirage 2000 jets of the IAF were tasked to drop laser-guided bombs to destroy well-entrenched positions of the Pakistani forces and flew their first sortie on 30 May 1999.</p>

                        <div class="war-2-map-2">
                            <img src="../assets/img/Gen VP Malik, PVSM, AVSM, then COAS during one of his visits to boost-28.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">Gen VP Malik, PVSM, AVSM, then COAS during one of his visits to boost morale of the troops during Kargil <br><strong>Source: Moc, DPR</strong></p>
                        </div>
                       
                    </div>

                </div>


                <div id="conflict" class="accordion-details">
                <h3>Assessment of the Kargil Conflict</h3>

                
                    <div class="war-details">
                        
                        <p>Following the outbreak of armed fighting, Pakistan sought American help in de-escalating the conflict. Following the Washington Accord of 4 July 1999, most of the fighting came to a gradual halt, but some Pakistani forces remained in positions on the Indian side of the LOC. The Indian Army launched its final attacks in the last week of July in coordination with relentless attacks by the IAF, both by day and night. Once the Drass Sub-sector had been cleared of Pakistani forces, the fighting ceased on 26 July 1999. This day has since been marked as Kargil Vijay Diwas in India.</p>

                        <p>The Pakistani establishment was compelled to negotiate a face saving withdrawal from the residual areas on the Indian side of the LOC, thereby restoring its sanctity, as was established in July 1972 in the Simla Agreement. The Pakistan Army with- drew from Kargil due, in part, to the pressure from Washington as the US adopted a unilateral approach which clearly articulated that Pakistan must withdraw from Kargil.</p>

                        <p>The Kargil conflict ended with numerous casualties on both sides. Around 527 officers and men of the Indian Armed Forces laid down their lives in Kargil to protect the integrity of the country and 453 were killed on the Pakistan side. Soon after the conflict, the Government of India set up an inquiry committee to examine its causes and to analyse perceived Indian intelligence failures. The Kargil Review Committee (KRC) report flagged numerous flaws on multiple levels of intelligence collection, operational strategies, and procedural sharing of data. The Committee was initially given a period of three months to submit the report but its deadline was extended to 15 December 1999.</p>

                        <p>The Committee’s report surmised that a Kargil-type situation could perhaps have been avoided had the Indian Army followed a policy of Siachenisation to plug unheld gaps along the 168 km stretch from Kaobal Gali to Chorbat La. The findings of the KRC highlighted grave deficiencies in India’s security management system. The Committee strongly felt that the Kargil experience, the continuing proxy war [with Pakistan], and the prevailing nuclearised security environment justified a thorough review of the National Security System in its entirety.</p>

                        <p>Table 1 below gives a detailed chronology of the events at Kargil in 1999.</p>
                       
                        <p style="text-align: center;">Table 1 Chronology of the Kargil Conflict</p>

                       
                            <div class="box-table">
                           
                            <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col" style="text-align: center;"> <strong>MAY 1999</strong></th>
                                            <th scope="col" style="text-align: center;"><strong>JUNE 1999</strong></th>
                                            <th scope="col" style="text-align: center;"><strong>JULY 1999</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row"><strong> 3 MAY 1999 </strong>Shepherds report Intruders sighted for the first time at Banju Headquarters, 70 Infantry Brigade arrives at Dras.</th>
                                            <td><strong>1 JUNE 1999 </strong>-Headquarters 8 Mountain Division completes take over. Start of the military build-up.</td>
                                            <td><strong>1 JULY 1999- </strong>70 Brigade captures Point 5000.</td>
                                        </tr>

                                        <tr>
                                            <th scope="row"><strong>5 MAY 1999-</strong>Captain Saurabh Kalia Patrol sent out to examine the suspicion of intrusion, the war commences.</th>
                                            <td><strong>3 JUNE 1999- </strong>8 Division assumes responsibility for the part of the Kargil Theatre west of Thasgam.</td>
                                            <td><strong>3 JULY 1999- </strong><br> 1.	70 Brigade captures Point 5287.<br> 2.	End of 8 SIKH siege of Tiger Hill.</td>
                                        </tr>
                                        
                                        <tr>
                                            <th scope="row"><strong>6 MAY 1999-</strong> NH 1A opened to traffic.</th>
                                            <td><strong>12 JUNE 1999- </strong><br>1.	Ongoing talks between Indian and Pakistani Foreign Ministers deadlocked. <br>2.	50 (I) Parachute Brigade arrives in Gumri from Army reserve and comes under command of 8 Mountain Division. </td>
                                            <td><strong>4 JULY 1999-</strong> <br>1.	192 Brigade captures Tiger Hill. <br> 2.	Nawaz Sharif, in Washington, urged by Clinton to start talking with India.</td>
                                        </tr>

                                        <tr>
                                            <th scope="row"><strong>16 MAY 1999-</strong> 56 Mountain Brigade takes over Dras- Mushkoh Sector.</th>
                                            <td><strong>13 JUNE 1999-  </strong>56 Brigade takes Tololing and Point 4590.</td>
                                            <td><strong>5 JULY 1999- </strong>79 Brigade takes Point 4875 complex. By now, virtually the whole of Mushkoh and Dras were clear of enemy.</td>
                                        </tr>

                                        <tr>
                                            <th scope="row"><strong>18MAY 1999-</strong> The Capture of Points 4295 and 4460</th>
                                            <td><strong>14 JUNE 1999-</strong> They take ‘Hump’.</td>
                                            <td><strong>12 -18 JULY 1999- </strong>Cease-fire to allow safe withdrawal of Pakistan Troops across LoC. (They reneged and the war was resumed)</td>
                                        </tr>

                                        <tr>
                                            <th scope="row"><strong>21 MAY 1999- </strong>8 SIKH start siege of Tiger Hill</th>
                                            <td><strong>15 JUNE 1999- </strong>United States President Bill Clinton urges Prime Minister Nawaz Sharif to withdraw.</td>
                                            <td><strong>24 JULY 1999- </strong>192 Brigade takes Zulu Spur Complex</td>
                                        </tr>

                                        <tr>
                                            <th scope="row"><strong>23 MAY 1999- </strong>Chief of Army Staff (COAS) visits Kargil sector and lays down the priorities and doctrine for the conduct of operation.</th>
                                            <td><strong>20 JUNE 1999- </strong>56 Brigade takes Point 5140.</td>
                                            <td><strong>26 JULY 1999- </strong>Kargil War officially ends</td>
                                        </tr>

                                        <tr>
                                            <th scope="row"><strong>24 MAY 1999-</strong>79 Mountain Brigade takes over Mushkoh Sub-Sector.</th>
                                            <td><strong>23 JUNE 1999-  </strong><br>1.	General Zinni, Commanding General United States Central Command visits Pakistan to urge Nawaz Sharif to withdraw.<br>2.	The G-8 Nations call for an end to the intrusion.</td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <th scope="row"><strong>25 MAY 1999- </strong>Operation Vijay Launched.</th>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <th scope="row"><strong>26 MAY 1999-</strong> Indian Air Force begins Air Operations in support of 15 Corps.</th>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                    </tbody>
                                </table>
                                <p>Source: Amarinder Singh, A Ridge Too Far: War in the Kargil Heights, 1999, Patiala: Motibagh Palace, 2001, pp. 100–101.</p>
                            </div>
                        </div>
                  

                </div>


                    <!-- <div id="note" class="accordion-details">
                        <div class="pawan-war-details">
                            <h4>Note</h4>
                            <div class="pawan-note">
                                <ol>
                                    <li>From Surprise to Reckoning: Kargil Review Committee Report, New Delhi: Sage, 2000, p. 37.</li>

                                    <li>Maj Gen Ashok Kalyan Verma, Kargil: Blood on the Snow, New Delhi: Manohar, 2002, p. 85.</li>

                                    <li>Singh, ‘Times of Trial’, n. 21, p. 180.</li>

                                    <li>Ashley J Tellis, C. Christine Fair, and, Jamison Jo Medby, Limited Conflicts Under the Nuclear Umbrella: Indian and Pakistani Lessons from the Kargil Crisis, Santa Monica: RAND Corporation, 2001, p. 5.</li>

                                    <li>From Surprise to Reckoning, n. 67, p. 47.</li>

                                    <li>Ibid., p. 90. </li>

                                    <li>Vijay Oberoi, ‘India’s Wars since Independence: A Concise History’, Journal of the United Service Institution of India, Vol. CL, No. 622, October-December 2020.</a></li>

                                  

                                    <li>Singh, ‘Times of Trial’, n. 21, p. 180.</li>

                                    <li>From Surprise to Reckoning, n. 67, p. 104.</li>

                                    <li>Verma, Kargil: Blood on the Snow, n. 68. p. 96</li>

                                    <li>Ibid.</li>

                                    <li>Ibid.</li>

                                    <li>Ibid.</li>

                                    
                                    <li>Adekeya Adebajo and Chandra Lekha Sriram (eds), Managing Armed Conflicts in the 21st Century, London: Frank Cass, pp. 192–193.</li>
                                    
                                    <li>Singh, ‘Times of Trial’, n. 21, p. 181.</li>
                                    
                                    
                                    

                                    

                                    <li>From Surprise to Reckoning, n. 70, p. 250.</li>
                                    
                                    <li>Ibid., p. 253.</li>
                                </ol>
                            </div>
                        </div>
                    </div> -->


               

                
                <!-- <div id="gallery" class="row image-gallery">
                    <div class="col-md-12">
                        <h3>Gallery</h3>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">

                            <img src="../assets/img/img5.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img6.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img7.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img8.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img13.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img9.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>
                </div> -->
            </div> 
        </div>
    </div>
</section>




    
<?php get_footer(); ; ?>