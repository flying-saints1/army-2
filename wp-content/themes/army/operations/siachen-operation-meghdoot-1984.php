<?php

$main ="operaitons";

$page="siachen";

get_header(); ; ?>


    <section class="operations-banner" style="background-image: url(../assets/img/operations-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Operation Meghdoot, 1984</h1>
        </div>
    </section>




<section class="operation-details" id="faq-section">
    <div class="container">
        <div class="row">
           <?php include('../sidebar/operations-sidebar.php'); ?>


            <div id="back" class="col-md-9">
                <div class="content">
                    <img src="../assets/img/post-indep-war-2.png" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                </div>


                
                <div id="back" class="accordion-details">
                <h3>Background</h3>

                    <div class="war-details">
                    
                        <p>Located in the eastern Karakoram range in the Himalayas, the Siachen Glacier has the distinction of being the world’s highest battlefield. Since 1987, India has had complete control over the heights and passes on the glacier.</p>

                        <p>The origin of the dispute lies in the differing interpretations by both sides of the alignment of the undemarcated portion of the Line of Control (LOC) established by the Simla Agreement signed on 2 July 1972.The LOC itself is a modification of the Cease Fire Line (CFL) demarcated by the Karachi Agreement finalised on 27 July 1949 after the 1947–48 Kashmir war. Both these agreements indicated point NJ9842 as the end of the northern line. The Baltoro and Siachen glaciers, divided by the Saltoro ridge, lie north of this line. NJ9842 is located at the lower edge of the Saltoro ridge and the Indian interpretation was that the logical extension of the line would lie along its crest. Pakistan’s claims, however, took the LOC from NJ9842 north-east to the Karakoram Pass. Thus, the boundary was not clearly demarcated and differing perceptions laid the ground for potential conflict. This region became more significant in the geopolitical calculations of Pakistan once it gave access to China to the Shaksgam Valley in 1963. Further, the glacier is the source for the Nubra River that eventually feeds the Indus river, which is the major water source that irrigates the Punjab plains in Pakistan.</p>

                        <p>Throughout the 1970s, a number of mountaineering expeditions were mounted from the Pakistan side, many of which included foreign participants. These crossed the Saltoro ridge and the Siachen glacier. The 1974 edition of the US Defence Mapping Agency’s operational navigation chart was the first to show an Air Defence Information Zone (ADIZ) separating India and Pakistan in the Karakoram region. The line marking the separation was drawn straight from NJ9842 to the Karakoram Pass. It is impossible to have a straight line boundary in the mountains as these are either formed along the watersheds of mountains or along rivers, and the ADIZ was along neither.</p>

                        <p>In January 1978, Indian authorities became aware of implications of the line joining NJ9842 to the Karakoram Pass and Pakistan-sponsored mountaineering expeditions. The first response was to send a strong military mountaineers’ expedition to climb a series of peaks on either side of the Siachen glacier. Colonel Narinder ‘Bull’ Kumar of the Indian Army led an expedition to Teram Kangri, along with medical officer Captain AVS Gupta. The IAF provided valuable support to this expedition through logistic support and supply of fresh rations. Kumar’s expedition was a success and also revealed evidence of other expeditions having entered the Siachen area from the west. Kumar alerted Lt Gen ML Chibber about the Siachen Mountaineering expeditions from Pakistan and ‘recommended that to ensure the Pakistanis do not intrude into Siachen, India should establish a post in the area which could be manned during the summer.’</p>
                        
                        <p>Bilafond La had been used as a traditional pass, much for climbing from the west. From the north to south, there were four important passes on the Saltoro Ridge: Sia La, Bilafond La, Gyong La and Chulung La. Sia La and Bilafond La were at altitudes in excess of 20,000 ft and 18,000 ft, respectively. In 1983, two Indian Army patrols were sent into the area, a move which saw protests from Pakistan. In January 1984, an attack was launched by Pakistan to drive off the Indian Army from Bilafond La, but it was repelled. In February 1985 another attempt was made to occupy the height overlooking Sia La; this attempt too met with failure. The next major effort to change the situation on the ground took place in 1987, when the Pakistani Special Services Group (SSG) occupied a position overlooking the Bilafond La area in March-April.</p>

                        <p>Under the leadership of Lt Gen ML Chibber, Maj Gen Shiv Sharma and Lt Gen PN Hoon, the Indian Army learned of the plan by the Pakistan Army to seize Sia La and Bilafond La on the glacier. The 26 Sector which was commanded by Brig Vijay Channa was given the task of occupying the Saltoro Ridge. Brig Channa was tasked to launch the Operation between 10–30 April, when he supposedly chose 13 April, considering it to be a lucky day on the occasion of Vaisakhi Day. The military decided to deploy troops from the Northern Ladakh region as well as some paramilitary forces to the glacier area. The operation by the Indian Army, planned to control the Siachen glacier was executed phase-wise. The first phase of the operation began in March 1984 where a full battalion of the Kumaon Regiment led by Capt Sanjay Kulkarni and units from the Ladakh Scouts, marched with full battle packs through an ice-bound Zoji La Pass for days. There were units deployed for the operation, of which, the first unit led by Maj RS Sandhu was tasked with establishing a portion on the heights of the glacier. The first platoon was put down 3 km short of Bilafond La on 13 April 1984. The second platoon was to go to Sia La. The IAF carried out a total of 32 helicopter sorties at Sia La and the troops were put down 5 km from the Sia La Pass.</p>

                        
                    </div>

                </div>

                <div id="comm" class="accordion-details">
                <h3>Commencement of the Operation</h3>

                
                    <div class="war-details">
                        
                        <p>Operation Meghdoot commenced with the airlift of Indian Army soldiers by the IAF. The IAF used IL-76, AN-12, AN-32 planes and MI-17, MI-8 helicopters to transport stores and troops and to airdrop supplies to high altitude airfields. From there Mi-17, Mi-8 and HAL Chetak helicopters carried provisions and personnel to the east of the hitherto unscaled peaks. It was at 0530 Hours on 13 April that the first Chetak helicopter carrying Capt Sanjay Kulkarni of 4 KUMAON and one soldier took off from the base camp, followed by 17 such sorties by noon. Further, 29 soldiers were heli-dropped at Bilafond La. Soon, the weather packed up and the platoon was cut off from the headquarters. The contact was established after three days, when five Cheetah and two Mi-8 helicopters flew a record 32 sorties on April 17 to Sia La. ‘That same day’, according to Sushant Singh, ‘a Pakistani helicopter flew overhead to see Indian soldiers already deployed at the glacier.’ This move highlighted the gravity of the situation and a force named Burzil Force was raised by the Pakistan SSG to conduct Operation Ababeel. Several days later, elements of the force were sighted in Bilafond La. On 22 April, two Pakistani jet aircraft had flown over the Indian position. On 25 April, Burzil Forces, comprising the SSG and the Northern Light Infantry (NLI), started the firefight with small arms and machine guns. Thus, military conflict between India and Pakistan began for control of the glacier.</p>

                        <p>Pakistan responded sharply after being denied the two major passes. Forced attempts were made and getting hold of the next higher place became an operational aim. The Pakistani Army strove to secure any place on Saltoro range, but the inevitable Indian response denied them any foothold on Soltoro. Defending every part of the ridge became a political necessity. The Saltoro experience proved a turning point as it forced the Indian Army to experiment and innovate in all aspects of combat. With the defence of the whole of Saltoro becoming a military responsibility, a doctrine to achieve the desired ends became essential. Lt Gen Chibber wrote in an official note:</p>

                        <p>The two main passes were sealed off. The enemy was taken completely by surprise and an area of approximately 3,300 sq km, illegally shown as part of PoK on the maps published by Pak and USA, was now under our control. The enemy had been preempted in their attempt to occupy the area claimed by them.</p>

                        
                        <p style="margin-left: 20px;">The Siachen glacier continues to be occupied by the Indian Army till date. India and Pakistan have held 13 rounds of negotiations in order to find a solution to the Siachen dispute. The first round of talks were held in 1986 and the last round took place in 2012. The situation on Siachen has been at an impasse since. Although Operation Meghdoot was a major victory for the Indian military. Both India and Pakistan maintain a permanent military presence on the glacier.</p>

                        <div class="war-2-map-2">
                            <img src="../assets/img/A soldier of 8 Maratha LI on Saltoro Ridge, 1985.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 600;">A soldier of 8 Maratha LI on Saltoro Ridge, 1985 <br>Source: Army HQ.</p>
                        </div>
                        
                        
                    </div>

                </div>


                <div id="battle" class="accordion-details">
                <h3>Battle for Bana Post</h3>

                
                    <div class="war-details">
                        
                        <p>In 1987, military operations on the Saltoro ridge reached higher thresholds in terms of both size and intensity. The Pakistan Amy was by then jostling for a foothold on the ridgeline, aiming to secure a position on or near the Bilafond La Pass, which was the main gateway to the Siachen glacier. The strategically important Siachen area had been infiltrated by Pakistani forces, who had captured an important position: this was called Quaid Post, named after Quaid-e-Azam Muhammad Ali Jinnah. The post was located at a height of 6,500 m on the highest peak in the Siachen Glacier area.</p>

                        <p>Pakistan’s establishment of the Quaid Post on this peak threatened Indian movement on the western Siachen Glacier. The Quaid Post had been occupied by Pakistan’s SSG in late March or early April of 1987. An Indian Army patrol was sent to assault the post on 24 May 1987, but it was fired upon by the SSG which was occupying the post. The army then planned yet another operation, which was conducted in June 1987, to capture the post and evict the Pakistanis from there.</p>

                        <div class="war-2-map-2">
                            <img src="../assets/img/View of a post in the Siachen region-21.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 600;">View of a post in the Siachen region<br>Source: Army HQ.</p>
                        </div>

                        <p>The task force launched multiple unsuccessful attacks on the Pakistani troops stationed at the post. On the night of 25 June 1987, a team led by Naib Subedar (now Honorary Captain) Bana Singh of Jammu and Kashmir Light Infantry (JAK LI) moved forward to attack the post. They managed to capture it successfully by 26 June 1987. For his role in capturing this strategically significant post, Bana Singh was awarded the Param Vir Chakra (PVC). The Quaid Post was renamed ‘Bana Post’. The actions of the Indian Army ensured that Pakistan had no further presence on the Siachen Glacier.</p>

                        <p>In September 1987, Pakistan launched a major attack to capture the areas around Bilafond La. The attack failed due to insufficient understanding of the impact of terrain on military operations. This attack ended up as a major defeat for Pakistan, with nearly a hundred soldiers killed on the glacier near the pass.</p>

                       
                    </div>

                </div>



                <div id="glacier" class="accordion-details">
                <h3>Chumik Glacier</h3>

                
                    <div class="war-details">
                        
                        <p>The Chumik Glacier is one of the many that flow west from the Saltoro. The ridgeline in the area is closer to the Siachen and is a difficult part of Saltoro, whose ridgeline cannot be easily scaled either by Pakistan or by India.</p>

                        <p>After Pakistan’s failed attempt in April 1987 to gain a foothold on the Saltoro ridgeline, another attempt was made in March 1989 on the Chumik glacier, 3 km east of Giari. Routine aerial reconnaissance in April 1989 revealed that Pakistan had established a post on the ridge in the Chumik glacier sector. The glacier was difficult to spot and so it gave an opportunity to the Pakistan Army to open artillery fire on part of helicopter supply routes used by the Indian posts. The post on the ridge was unapproachable from the Indian side other than by helicopter and, at the same time, the artillery fire had to be neutralised. The Indian Army conducted Operation Ibex, which set out to seize the Pakistani post overlooking the Chumik Glacier. Although the operation was unsuccessful at dislodging Pakistani troops from their positions, the Indian Army under Brig RK Nanavatty launched an artillery attack on Kauser Base—the Pakistani logistical node in Chumik—and successfully destroyed it. The destruction of Kauser Base induced the Pakistani troops to vacate the Chumik posts, thus concluding Operation Ibex successfully. ‘In May 1989’, as the Indian Army ‘had succeeded in neutralising the supply base supporting the [Pakistani] soldiers on the peak, the post was vacated.</p>

                       
                    </div>

                </div>

                    <!-- <div id="note" class="accordion-details">
                        <div class="pawan-war-details">
                            <h4>Note</h4>
                            <div class="pawan-note">
                                <ol>
                                    <li>Lt Gen (Retd) VK Singh, PVSM, ‘Times of Trial’, in Maj Gen (Retd) Ian Cardozo, AVSM, SM (ed.), The Indian Army: A Brief History, New Delhi: USI), 2007, p. 165. </li>

                                    <li>Ibid.</li>

                                    <li>VR Raghavan, Siachen: Conflict Without End, New Delhi, Penguin Books, 2002, p. 33.</li>

                                    <li>Ibid.</li>
                                    <li>Ibid., p. 47.</li>

                                    <li>Sushant Singh, ‘Operation Meghdoot: 34 Years Ago, How India Won Siachen’, The Indian Express, 13 April 2018.</li>

                                  

                                    <li>Singh, ‘Times of Trial, p. 165.</li>

                                    <li>Singh, ‘Operation Meghdoot: 34 Years Ago, How India Won Siachen’, n. 26.</li>


                                </ol>
                            </div>
                        </div>
                    </div> -->


               

                
                <!-- <div id="gallery" class="row image-gallery">
                    <div class="col-md-12">
                        <h3>Gallery</h3>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">

                            <img src="../assets/img/img5.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img6.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img7.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img8.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img13.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img9.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>
                </div> -->
            </div> 
        </div>
    </div>
</section>




    
<?php get_footer(); ; ?>