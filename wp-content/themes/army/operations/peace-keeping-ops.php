<?php

$main = "operaitons";

$page = "peace";



get_header();  ?>


<section class="post-independence-war-banner" style="background-image: url(../assets/img/post-independence-war-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">Peace Keeping Ops</h1>
    </div>
</section>


<section class="indep-war" id="faq-section">
    <div class="container">
        <div class="row">
            <?php include('../sidebar/operations-sidebar.php'); ?>

            <div id="india-and-un" class="col-md-9">
                <div class="content">
                    <img src="../assets/img/changeable-img.jpg" width="100%" height="400px" class="img-fluid res-image" alt="" loading="lazy">
                </div>


                <div class="accordion-details">
                    <h3>India and UN Peacekeeping</h3>

                    <div class="war-details">
                        <p>Since its inception, the United Nations (UN) has undertaken a total of 71 peacekeeping operations (PKOs). Indian troops have been present in 49 of the 71 UN Peacekeeping missions around the world. These include conventional PKOs to stabilise the security situation between warring states, for example, Egypt and Israel (United Nations Emergency Force [UNEF I]) in 1956, and complex operations, like United Nations Transitional Authority in Cambodia (UNTAC) in 1991; military interventions in Somalia, that is, Unified Task Force (UNITAF) in 1992 and United Nations Operations in Somalia (UNOSOM II) in 1993; and United Nations Assistance Mission for Rwanda (UNAMIR) in 1994. The PKOs have also been established in the Democratic Republic of the Congo (DRC), Liberia and Cote d’Ivoire to contain conflict and pave the way for peace. Unfortunately, not many of these missions can be said to have succeeded.</p>

                        <p>A variety of reasons explain why states contribute to PKOs. National interest has traditionally been the foremost concern influencing a state’s decision to either participate in or support a UN-authorised peace operation. Further, it has been argued that a government’s foreign policy decision is guided by the perceived interests of that government. However, disagreeing with this thought process, David Gibbs states that, among other factors, economic and strategic considerations also play a part while deciding to participate in PKOs. Meanwhile, after examining 14 PKOs from 2001–08, Brian Urlacher concluded that countries had myriad reasons for participating in PKOs, ranging from financial benefits to establishing or advancing a position within the international community, asserting influence on the world stage and preserving peace and security. However, essentially, more than the desire to restore global peace, the self-interest of the states is the primary driver in the decision-making process for participating in PKOs.</p>

                        <p>In terms of numbers, troops from the South Asian region presently dominate the UN peacekeeping missions. The only exception perhaps is the United Nations Interim Force in Lebanon (UNIFIL), where participation from European nations is the highest, that is, nearly half of the entire mission strength.</p>

                        <p>Here we can trace how the Indian Army’s approach to UN peacekeeping has evolved over a period of time, side by side with the difficult journey of UN peace operations over the decades. A brief overview of the country’s participation in various PKOs is given, along with the rigorous training and selection process followed for the Indian peacekeepers as well as a few facts about India’s position on the development of the UN peacekeeping doctrines. </p>

                        <p>Climate change and its impact on security is a new and urgent concern of the international community. Hence, besides peacekeeping, we will also briefly touch upon the Indian Army’s experience in providing assistance to civil authorities during natural disasters and how these can come in handy in conflict zones.</p>

                        <div class="war-details">
                            <h4>India and UN Peacekeeping</h4>
                            <p>Since the inception of the first UN peacekeeping mission in 1948, the Indian Armed Forces have been participating in some of the most difficult peacekeeping missions in the world. Lieutenant General (Lt Gen) P. S. Gyani and Lt Gen Dewan Prem Chand are some of the prominent figures who tenanted appointments in UN. The first peace operation in which India participated was in Korea (1950–54), even though it was not a classical UN PKO. India contributed a medical unit and Parachute Field Ambulance. The Parachute Field Ambulance was tasked to operate as two entities: the principal part commanded by Lieutenant Colonel (Lt Col) AG Rangaraj, to provide medical support to multinational troops in the battle zone and the remaining unit under Major (Maj) NB Banerjee to aid the South Korean Field Hospitals in the strategic town of Taegu. A total of 17 officers, nine junior commissioned officers JCOs, and 300 other ranks ORs participated in this mission. This was followed by a Custodian Force of 231 officers, 203 (JCOs), and 5,696 (ORs) under the command of Major General (Maj Gen); (later Lt Gen) SPP Thorat for the Neutral Nations Repatriation Commission of which the Chairman was Lt Gen (later General) KS Thimayya. While he was engaged in the peacekeeping efforts in Cyprus, General Thimayya suddenly expired on 18 December 1965. The medical unit’s exemplary contribution was recognised by the Minister of War of Great Britain, wherein a tribute was paid to the unit and the Commanding Officer in the House of Commons. The unit had the unique distinction of receiving a ‘Meritorious Unit Citation’ from the United States (US) Army as well. For their gallant and exemplary actions, the personnel of the unit were honoured with numerous awards, including the Maha Vir Chakra (MVC) to Lt Col AG Rangaraj.</p>

                            <p>Korea was followed by participation in Gaza (UNEF I) in 1956 and later in Congo (ONUC) in 1960. Its participation in the initial peace operations led to a pool of military officers seconded to the UN. Maj Gen IJ Rikhye was the first Military Adviser to the UN Secretary-General between 1960–1967. According to Lt Gen Satish Nambiar, ‘India also contributed significantly to the Indo-China Supervisory Commission deployed in Cambodia, Laos and Vietnam from 1954 to 1970; a medical detachment from 1964 to 1968, and 970 officers, 140 JCOs and 6157 ORs over the period 1954 to 1970.’</p>


                            <p>After the end of the Cold War, India participated in several missions only with unarmed observers and with formed units in Angola (UNAVEM), Somalia (UNOSOM), Sierra Leone (UNAMSIL), and Liberia (UNMIL) with a female-formed police unit (FFPU). Currently, Indian peacekeepers are participating in the United Nations Organization Stabilization Mission in the Democratic Republic of the Congo (MONUSCO), United Nations Mission in South Sudan (UNMISS), UNIFIL and the United Nations Security Force in Abyei (UNISFA) with effect from 2022.</p>

                            <p>Like other troop-contributing countries (TCCs), India’s participation in peacekeeping is guided by its strategic interests, which are decided on a case-to-case basis. However, India does believe in adhering to the core principles of peacekeeping, as enshrined in the ‘Capstone doctrine’. Despite inherent ambiguity in the inter- pretation of the principles of peacekeeping, India, by practice, tries to take a clear stand when it comes to applying these principles in mandate implementation. Nevertheless, as every situation is different in armed conflicts, it is problematic to legislate how to deal with each arising situation. Besides, even though enshrined in the Capstone doctrine, the peace operations doctrine is also still evolving.</p>

                            <p>India’s position on important developments that impact the effectiveness of peace operations can be inferred from its stated positions in the UN as well as its practices in the field. The country’s opinion on central issues that are intricately connected to mandate implementation of the peace operation has been summarised by the Permanent Mission of India (PMI) to the UN:</p>

                            <p style="padding-left: 16px;">India is of the view that the international community must grasp the rapid changes that are underway in the nature and role of contemporary peacekeeping operations. The Security Council’s mandates to UN peacekeeping operations need to be rooted in ground realities and correlated with the resources provided for the peacekeeping operation…[T]troop and police-contributing countries must be fully involved at all stages and in all aspects of mission planning. There should be greater financial and Human Resources for peacebuilding in post-conflict societies where UNPKOs have been mandated.</p>
                        </div>

                        <div class="war-details">
                            <h4>Indian Peacekeepers</h4>
                            <p>Asoke Mukerji, a former Indian Ambassador to the UN, has noted that ‘India’s con- tributions to UN peacekeeping Operations (UNPKOs) have been underscored by the experience and professionalism of India’s armed forces.’17 Nearly half of India’s army is involved in internal security duties or counter-terrorist operations within the country. Years of experience in such operations have taught the Indian Army the need for patience, maturity and compassion. Therefore, commanders of the Indian Army contingents prefer the use of deterrence, negotiation, control measures, protection, warning and security of their areas as an alternative to the use of force against belligerents. To this end, a lot of importance is given to the selection, training and equipping of the individual as well as contingent members before their induction into the mission areas.</p>

                            <p>The ‘selection’ process for the Indian Army’s peacekeepers is very elaborate. The officers and personnel below officer rank, who aspire to participate in peacekeeping missions in an individual capacity, undergo a stringent merit-based selection system. Professional competence, grades obtained in various courses of instruction, integrity, loyalty and the potential to hold higher ranks or assignments in the future are only a few of the criteria that decide the nomination of a candidate for UN peacekeeping. Similar criteria are followed while selecting individual police peacekeepers and police contingents. The selection process of troop contingents is equally strict. Past performance of these units in battlefield conditions is an important criterion for them to be called upon to be part of UN peacekeeping missions. Additionally, Indian troop contingents are always based on single homogenous units, thereby retaining their cohesiveness. For example, the core of a battalion group in the Indian Army is always a single infantry battalion. All other non-infantry elements are only by way of sup- port of the infantry for better logistic sustenance.</p>


                            <p>The first stage of peacekeeping training for the officers begins at the military academies—the National Defence Academy, Indian Military Academy, and the Officers Training Academy. This training is gradually augmented as an officer goes through his mandatory and selected courses in various training institutions. For other ranks, a soldier gets his basic training at the battalion level. The second stage or advanced training is conducted as part of the national pre-deployment training curriculum before induction into the mission. In addition, all officers, selected as staff officers unarmed military observers or forming part of a contingent, are made to go through a detailed training programme at the Centre for United Nations Peacekeeping (CUNPK), New Delhi, which is the Indian Army’s premier institution for peace- keeping. For the training to be meaningful, training content and methodologies are carefully worked out and oriented towards the mission.</p>

                            <p>The training and the stringent selection process have proved to be of particular use in the PKOs. It is probably the Indian peacekeepers’ decades of experience and understanding of the nuances of peacekeeping, selection of the peacekeepers and preparation before participation in PKOs that makes them acceptable anywhere in the world. Before discussing India’s position on the UN peacekeeping doctrine, we will take a look at some key missions, the experience of which laid down the foundation of future peacekeeping. India has also participated in some of the missions being discussed here. The background of the missions will help us to better understand the ambiguities related to the doctrinal development of UN peace operations.</p>
                        </div>


                    </div>


                    <div id="peacekeeping" class="accordion-details">
                        <h3>Peacekeeping Missions: <br>Past and Current</h3>

                        <div class="war-details">
                            <h4>UNEF I</h4>
                            <p>The UNEF I was a seminal operation, the strategy of which laid down the framework for future UN PKOs and the identification of three principles of peacekeeping. Post the Arab–Israel War of 1948, the United Nations Truce Supervision Organization (UNTSO), the first UN PKO, was created to preserve ceasefire along the armistice lines. However, UNTSO was found to be ineffective during the Suez Crisis of 1956. This came in the wake of the nationalisation of the Suez Canal by Egyptian President, Gamal Abdel Nasser, on 26 July 1956. Based on a secret agreement between Israel, the UK and France, Israel invaded Egypt on 29 October 1956.</p>

                            <p>Recognising the challenging situation arising due to the crisis, the United Nations Security Council (UNSC) adopted a resolution calling for an emer- gency meeting of the General Assembly. Since a collective enforcement action became politically impossible, on 2 November 1956, the UN General Assembly passed a resolution calling for a ceasefire and withdrawal of forces. On 4 November, the General Assembly passed a landmark resolution—Resolution 998—authorising the Secretary-General to set up ‘an emergency United Nations international force to secure and supervise the cessation of hostility’—UNEF I. While adopting the resolution, the General Assembly did not elaborate on the strategy, or the employability of the force or the composition, which was left to the Secretary-General. </p>

                            <p>The strategy adopted by the Secretary-General was later described in his report to the General Assembly. The key elements of this strategy were the identification of the three cardinal principles of peacekeeping called the ‘holy trinity’—consent, impartiality and use of force. The Indian contingent that participated in UNEF consisted of Headquarters, Infantry Battalion, Platoon of Army Service Corps, Detachments from the Corps of Signals, Ordnance, Medical, Military Police and Army Postal Service. The Indian contribution accounted for the bulk of the UN forces and more than 12,000 Indian troops took part in UNEF, involving 393 officers, 409 JCOs, and 12,383 ORs over the course of 11 years, with Maj Gen PS Gyani and Brig IJ Rikhye as Force Commanders for the mission. The Indian contingents were actively involved in exchange of prisoners, monitoring the withdrawal of Israeli forces and acted as an effective buffer between the opposing forces. In the Indian Sector, several posts were set up to observe the boundary line and the troops had to routinely negotiate anti-personnel and anti-tank mines, and sustained 27 fatal casualties.</p>

                            <p>After the UK, France and Israel pulled back, the Suez Canal—under Egyptian control—was reopened in April 1957 to international traffic, and UNEF settled down into its monitoring role on the Egyptian side of the armistice line. On 17 June 1967, at the request of the Egyptian government, the UNEF operation formally ended.</p>
                        </div>

                        <div class="war-details">
                            <h4>United Nations Operations in the Congo (ONUC)</h4>
                            <p>The Congo was a Belgian colony from 1908 until it gained its independence on 30 June 1960. The internal situation in the Democratic Republic of Congo (DRC) started to deteriorate soon after its independence, with violent clashes in certain areas and mutinies by Congolese soldiers against Belgian officers. Soon, Belgian military reinforcements arrived to restore order and protect Belgian citizens, but this was done without the permission of the Congolese government. Simultaneously, Moise Tshombe, the head of the provincial government of Katanga, declared independence of Katanga on 10 July 1960, further compounding the situation.</p>

                            <p>All these developments prompted the Congo’s new leaders to appeal to the UN on 12 July 1960 for assistance. Recognising the urgency of the situation, Secretary- General Dag Hammarskjold exercised his power under Article 99 of UN Charter XV and called for an immediate meeting of the UNSC. On 17 July, the Congolese President and the Prime Minister informed the Representative of the Secretary- General in the Congo that if the UN was unable to ensure the withdrawal of the Belgium troops within 48 hours, they would be forced to request the intervention of the Soviet Union. This created a decision dilemma in the UNSC. For the West, the reports of Belgium’s involvement were a matter of concern, and so were the political and economic implications of the situation. For the Eastern bloc, more specifically the Soviet Union, it was a good opportunity that could be used to highlight the imperialistic idea of the West if no action was taken. Overall, everybody wanted to do ‘something’. Hammarskjold dominated most of the discussions that followed and indirectly forced the hands of the Security Council to act.</p>

                            <p>On 14 July 1960, the ONUC was established via UNSC Resolution 143, which called for the withdrawal of Belgium troops and authorised the Secretary-General ‘to take all necessary steps’ to provide the government with ‘such military assistance as may be necessary’ until the national security forces are able ‘to meet fully their tasks’. A total of five resolutions were adopted by the UNSC associated with the withdrawal of the Belgian military and mercenaries and the goals of internal stability.</p>


                            <p>The deployment of the peacekeepers was rather quick, with Tunisian troops reaching the area immediately, followed by the Swedish Force Commander Maj Gen Carl Von Horn, who was seconded by UNTSO and troops from Africa. Despite some improvement, the security situation in Katanga deteriorated further and things came to head when, in mid-February 1961, Prime Minister Patrice Lumumba was killed. Eventually, ONUC was forced to launch two military operations—Operation Rumpunch on 28 August 1961 and Operation Morthor on 13 September 1961— and, later, another operation on 28 December 1961, which led to the restoration of order with full control of Katanga achieved on 17 January 1963. The ONUC was terminated on 30 June 1964.</p>

                            <p>India’s contribution to the military operation with a brigade-size group was notable. The 99 Infantry Brigade Group was deployed in Congo, consisting of three Infantry Battalions, an Armoured Squadron, a Heavy Mortar Battery, an Engineer Company, a Machine Gun Company, Field Ambulances as well as six Canberra aircraft of the Indian Air Force (IAF). It was here that Captain (Capt) GS Salaria sacrificed his life in an operation against the Katangese gendarmerie on 28 December 1961. Salaria was posthumously decorated with the Param Vir Chakra, India’s highest gallantry award, for his actions in Congo.</p>

                            <p>The ONUC was the first PKO to use force beyond self-defence. In this mission, 135 personnel lost their lives, which is the highest in any UN military operation so far. On a positive note, Secretary-General Hammarskjold played a hands-on role throughout the crisis. His initiative, personal involvement in all discussions and finally, being able to convince the Security Council to act is something that would probably be never forgotten in the UN’s peacekeeping history. The ONUC’s experience also foreshadowed the current challenges of intra-state conflict to the UN.</p>


                        </div>


                        <div class="war-details">
                            <h4>United Nations Protection Force (UNPROFOR—Bosnia Herzegovina)</h4>
                            The Indian Army did not participate in the peace operations in the former republic of Yugoslavia other than Lt Gen Satish Nambiar, who was the UNPROFOR’s first Head of the Mission and Force Commander (although India contributed to the International Police Task Force [IPTF] from 1996–2001). However, the lessons learnt from the mission with special reference to the application of the principles of peacekeeping, mainly the use of force, are still relevant for the current peace operations. For a good understanding of how UN peacekeeping has evolved over the past several decades, it is important to be aware of the events that gradually unfolded following the extension of the mandate of UNPROFOR from Croatia to Bosnia and Herzegovina, to cope with the growing instability between Bosnian Serbs, Muslims and the Croat population.</p>

                            <p>UNPROFOR had a difficult mandate based on the consent and cooperation of numerous belligerent groups. Its efforts in Bosnia Herzegovina were thwarted by endless hostilities amongst the fighting factions and other operational shortcomings. Subsequently, its mandate was expanded to provide protective support for the United Nations High Commissioner for Refugees (UNHCR) humanitarian convoys but without any mention of Chapter VII. The UNPROFOR was to operate according to the traditional peacekeeping rules of engagement (RoE) and was authorised to use force only in self-defence.</p>

                            <p>The fighting continued to intensify mostly in small pockets of territories around Srebrenica, Zepa and Gordaze, which were crowded with Muslims, many of whom had fled there from the surrounding countryside. Poorly defended by Bosnian government soldiers, these areas were attacked by Bosnian Serb forces assisted by paramilitary units. Such attacks resulted in heavy loss of life among the civilian population and severely impeded UN humanitarian relief efforts in that area. Fighting slowly spread to other parts of the country, with increasing attacks on other ‘safe areas’ and UNPROFOR personnel. The UNPROFOR found itself short of troops to deal with the emerging security situation. Different perceptions about safe areas could also be attributed to the continuous attacks on the safe areas. Safe areas were a temporary mechanism where some vulnerable populations were to be protected pending a comprehensive negotiated political settlement. Since the safe areas contained not only civilians but also Bosnian government troops, the Serb forces considered them to be legitimate targets in the war.</p>

                            <p>The period from March to November 1995 witnessed an unprecedented level of military activities, including offensives by all sides, mass-scale movements of refugees and displaced persons, and gross violations of human rights. Continued lack of diplomatic activities and failure on the part of the international community resulted in a new wave of ‘ethnic cleansing’ by the Bosnian Serbs in western Bosnia, which later spread to central Bosnia and then on to Sarajevo. In Sarajevo, Bosnian Serbs ignored the UN threat of airstrikes and went to the extent of taking 300 UNPROFOR personnel as hostages to use them as human shields to deter any airstrikes. Images of some of the hostages chained and used as human shields were broadcast worldwide. One of the observation posts, lost to Bosnian Serbs during the clash, was later recaptured by UNPROFOR though at the cost of the lives of a few peacekeepers. The hostage crisis was, however, resolved through negotiations.</p>

                            <p>In mid-1995, several events dramatically changed the dynamics of the war. On 11 July 1995, the Bosnian Serb Army overran Srebrenica, taking 150 Dutch peacekeepers as hostages and forcing some 40,000 people to flee. Meanwhile, in the largest massacre in Europe since World War II, Bosnian Serb forces killed some 7,000 people, virtually all of them men or boys, mostly Muslims. Days after the fall of Srebrenica, Serb forces overran Zepa, another so-called safe area. In early August, the Croatian Army retaliated against the Bosnian offensive by a massive military offensive involving some 100,000 troops and overran all Serb-controlled areas in western and eastern Croatia. As a result, some 200,000 Serb civilians fled, most of them going to the Federal Republic of Yugoslavia, with a small number remaining in the Serb- controlled parts of Bosnia and Herzegovina. Paying no heed to the North Atlantic Treaty Organization (NATO) air threats, the Croatian Army’s attack was immediately responded to by Serb forces by firing at the busy marketplace in Sarajevo, killing civilians and injuring dozens more. To deter any more attacks on the safe areas, NATO airstrikes were conducted against Bosnian Serb positions. Bolstered by the airstrikes, Croatian and Bosnian government forces mounted a joint offensive in Bosnia and Herzegovina and took back a third of the territory held by the Bosnian Serb forces. Finally, the continuous diplomatic effort resulted in the Dayton Peace Agreement in Paris on 14 December 1995, and the replacement of UNPROFOR with a 60,000 strong multinational military Implementation Force (IFOR) on 20 December 1995. Subsequently, this new mission in Bosnia Herzegovina came to be known as the United Nations Mission in Bosnia and Herzegovina (UNBIH).</p>
                        </div>


                        <div class="war-details">
                            <h4>UNOSOM II</h4>
                            <p>Following the downfall of President Siad Barre in January 1991, a civil war broke out in Somalia, leading to widespread death and destruction. The UN became engaged in providing humanitarian aid, in cooperation with relief organisations. Given the circumstances of the civil war, there was no effective government to give the consent, so the UN faced difficulties in carrying out humanitarian activities. As a result, the UN, in cooperation with the Organization of African Unity (OAU) and other organisations, sought to resolve the conflict. In January 1992, a UN envoy, with a team of senior officials, visited Somalia. During the visit, all faction leaders expressed support for a UN peace role. This led the Security Council to approve the establishment of UNOSOM I with 50 unarmed observers in April 1992. On 12 August 1992, the Special Representative of the Secretary-General (SRSG) obtained the consent of all parties for the deployment of 500 peacekeepers in Mogadishu.</p>

                            <p>Later, on the recommendation of the Secretary-General, the Security Council approved an enlarged deployment of a total of 3,500 peacekeepers. This enraged the leader of one of the factions in Somalia, who considered it a breach of faith, leading to increased violence. As a result, acting under Chapter VII, the Security Council authorised the US-led coalition—also called the United Task Force (UNITAF)—to use force to secure a conducive environment for the distribution of humanitarian aid in Somalia.</p>

                            <p>In 1993, the UNSC passed a resolution on the transition from UNITAF to UNOSOM II, with a strength of 22,000 and a mandate to cover all of Somalia. Even though the US handed over responsibility to UNOSOM II on 4 May 1993, a sizeable number of its troops stayed back in support of UNOSOM II. India contributed one Infantry Brigade Group with three Infantry Battalions, one Mechanised Infantry Battalion, and an Independent Armoured Squadron, an Artillery Battery, an Engineer Company, a Signals Company, a Field Ambulance and other supporting elements to UNOSOM II. Apart from the Indian Army deployment, the Indian Navy also deployed a maritime task force consisting of six ships for the delivery of humanitarian assistance. The violence continued and clashes with the UN peacekeepers as well as the US forces increased.</p>


                            <p>The UNSC extended the mandate of UNOSOM II till November 1993, allowing more time for an in-depth study of the Somali situation. On 4 February 1994, the Security Council significantly downsized UNOSOM in terms of its strength and mandate, to ‘traditional peacekeeping’ only in the assistance role, till March 1995. The UNOSOM II was finally closed on 2 March 1995.</p>

                        </div>

                        <div class="war-details">
                            <h4>UNIFIL</h4>

                            <p>The Lebanese civil war, which began in 1975, led to a power vacuum and confrontation between Israel and its Lebanese proxies on one side, and the ‘Joint Forces’ of the Palestine Liberation Organization (PLO) and Progressive Lebanese Movement on the other. This resulted in a series of offensive actions by militias supported by Israel along the Lebanon–Israel border, which continued for almost two years. It ended with the US-backed (and Israeli-approved) deployment of the Arab Deterrent Force (ADF), a peacekeeping force mostly comprised of troops from the Syrian Army. However, later, Egypt, under President Sadat, broke away from its Arab allies and made peace with Israel on 26 March 1979, becoming the first Arab nation to forge a diplomatic relationship with Israel. Soon after, Israel increased its cross-border raids on Lebanon.</p>

                            <p>The UNIFIL is the result of Israel’s first major invasion of Lebanon on 14–15 March 1978. On 19 March 1978, the UNSC adopted Resolutions 425 and 426 calling for the immediate withdrawal of Israeli forces and authorising the establishment of UNIFIL. The initial contributions to UNIFIL came from France, Nepal, Norway, Iran, Senegal, Nigeria, Ireland and Fiji. Resolution 425 mandated UNIFIL to: confirm the withdrawal of Israeli forces from Lebanese territory; restore international peace and security in the border area; and assist the Government of Lebanon in ensuring the return of its effective authority in the area. The UNIFIL was thus established as a traditional mission to broker peace between Lebanon and Israel. Further, the UNIFIL was established in a hurry for political reasons, to avoid derailing Camp David Accords that President Carter was pushing at that time.</p>

                            <p>Following Hezbollah’s kidnapping of two Israeli soldiers in a cross border raid, Israel invaded Lebanon again in July 2006, which lasted for 34 days. Finally, amidst immense public outcry and with full support from the US and France, on 11 August 2006, the Security Council unanimously adopted Resolution 1701 (co-drafted by the US and France), giving shape to the present structure of UNIFIL. In addition to the mandate under Resolutions 425 and 426, UNSC Resolution 1701 authorised UNIFIL a strength of 15,000 troops.</p>

                            <p>India has participated in UNIFIL since December 1998. Until now, more than 40,000 peacekeepers have already participated in UNIFIL. Currently, there is a Battalion Group, one Level II Hospital, and more than 20 staff officers deployed in UNIFIL.</p>
                        </div>


                    </div>


                    <div class="accordion-details">
                        <h3>Principles of Peacekeeping</h3>

                        <div class="war-details">
                            <p>Peacekeeping has been undertaken under the aegis of the UN since the 1950s. Each conflict and its political and diplomatic resolution are unique. So too is the nature and requirement of peacekeeping given the specific context. In some cases, UNPKOs have led to the resolution of conflict and the operation is formally closed. In others, such as UNIFIL for example, the requirement of peacekeeping remains as the conflict is ongoing and there appears to be no resolution on the horizon. In the past seven decades, there have been numerous doctrinal developments vis-à-vis UN Peacekeeping and PKOs. The following sections shed light on a few facts about India’s position on the doctrines of UN peacekeeping.</p>


                            <p>The principles which India adheres to while participating in peacekeeping missions are enshrined in the UN’s ‘doctrine’ for PKOs—which is based on strict adherence to the core principles of peacekeeping. These principles are always carefully considered when deciding on India’s participation in any PKO. Once deployed, correct understanding and adherence to the basic principles of peacekeeping—consent, impartiality and neutrality and use of force—continue to remain the central elements that guide the day-to-day operational activities of Indian contingents. However, there have been times when India’s decision to participate, or not, in the PKOs has been guided by other considerations. For instance, India decided against contributing troops to the United Nations–African Union peacekeeping mission in Darfur, possibly to not lose the support of Sudanese President Omar Hassan Ahmad al-Bashir. India’s position has always been consistent with regard to the principles of peace- keeping. There were widespread speculations about the actual motive of India’s pulling out from UNAMSIL. </p>

                            <p>However, according to the Indian official spokesman, though India did pull out of UNAMSIL, the decision was not motivated by any political reason but by the fact that it was part of the routine rotation, to give an opportunity to other TCCs. There was another reason that prompted India to pull out from UNAMSIL. It was related to the UN Charter. When India decided to participate in UNAMSIL, the mission was mandated under UN Charter VI (UNSC Resolution 1270). Later, because of the developing security situation in Sierra Leone, the mandate was revised to act under Charter VII (UNSC Resolution 1289). It was felt that since the revised resolution impacted mandate implementation and those who were to implement it; this revision should have been taken in consultation with major TCCs like India.</p>

                            <p>In May 2000, the security situation deteriorated in Sierra Leone and the rebels took several peacekeepers hostage, which included a sizeable number of Indian peacekeepers. The official reports of the Secretary-General, however, referred to it only as ‘detention’. This notwithstanding, in one of the biggest UN military operations of the time which was launched by the Indian Army—Operation Khukri—all hostages were rescued. Post Operation Khukri, the Indian government pulled out from UNAMSIL. At the time of pulling out India’s position, as stated earlier, was because it would not participate in peace operations mandated under Chapter VII of the UN charter.</p>

                            <p>India considers impartiality as an important ingredient of peacekeeping and its actions are independent of the stand of parties to a conflict; it is based on an assessment of the situation as well as intent to be just, while not taking sides. On the other hand, the neutral entity is much more passive. Former UN Secretary-General Kofi Annan further reinforced this vision in May 1998, during his visit to Rwanda, when he stated: ‘In the face of genocide, there can be no standing aside, no looking away, no neutrality—there are perpetrators and there are victims; it is evil and there is evil’s harvest.’</p>

                            <p>To establish impartiality, ‘use of force’ may be necessary to implement the mandate. However, the history of UN peacekeeping demonstrates that whenever force has been used, there has always been retaliation, endangering the security of peacekeepers. This, however, has not deterred the Indian peacekeepers from using force to save innocent lives and restore peace. For instance, in the DRC, MONUSCO (the current UNPKO in the DRC) used Indian gunships against rebel groups who were determined to derail the peace process. Even in Sierra Leone, force was used to rescue the hostages from rebels. The multinational operation was led by the Indian Force Commander, Major General Vijay Jetley, and the Indian peacekeeping contingent was in the lead.</p>


                        </div>


                    </div>

                    <!-- <div class="note">
                        <p>Notes</p>
                        <ol>
                            <li>UN Department of Peace Operations, ‘Peacekeeping Operations Fact Sheet’, 30 September 2021</li>
                            <li>UN General Assembly, Report of the Panel on United Nations Peace Operations, A/55/305-S/2000/809, 21 August 2000</li>
                            <li>P Bennis, Calling the Shots: How Washington Dominates Today’s UN, New York: Olive Branch Press, 1996</li>
                            <li>D Gibbs, ‘Is Peacekeeping a New Form of Imperialism?’, International Peacekeeping, Vol. 4, No. 1, 1997, pp. 122–28</li>
                            <li>Brian Urlacher, ‘Answering the Call: A Study of Major Contributions to UN Peacekeeping Missions’, 2008, accessed on 17 October 2015</li>
                            <li>UN, ‘United Nations Peacekeeping’, United Nations Security Council (UNSC) Resolutions 425 and 426, 19 March 1978, and Resolution 1701, 11 August 2006</li>
                            <li>This section has been extracted from Joachim A Koops, Norrie Macqueen, Thierry Tardy and Paul D Williams (eds), The Oxford Handbook of United Nations Peacekeeping Operations, Oxford: Oxford University Press, 2015; and The Blue Helmets: A Review of United Nations Peacekeeping, New York: UN, 1996</li>
                            <li>Permanent Mission of India (PMI), ‘India and United Nations: Peacekeeping and Peacebuilding’.</li>
                            <li>Satish Nambiar, ‘India’s Role in UN Peacekeeping Operations’, Ian Cardozo (ed.), Indian Army: A Brief History, New Delhi: USI, 2007, p. 233</li>
                            <li>“Indian Army’s participation in United Nations Peacekeeping Ops”.</li>
                            <li>Ibid.</li>
                            <li>Asoke Kumar Mukerji, ‘UN Peacekeeping: India’s Contributions’, Ministry of External Affairs, 8 November 2019.</li>

                            <li>‘Troop and Police Contributors’.</li>
                            <li>United Nations Peacekeeping Operations: Principles and Guidelines, New York: UN Department of Public Information, 2008; also known as the ‘Capstone doctrine’</li>
                            <li>AK Bardalai, ‘Ambiguity in Normative UN Norms: A Challenge to UN Peace Operations’, under publication by the Integrated Headquarters (HQ) of the Army, Ministry of Defence; and John Karlsrud and Kseniya Oksamytna, ‘Norms and Practices in UN Peacekeeping: Evolution and Contestation’, International Peacekeeping, Vol. 26, No. 3, 2019, pp. 253–54</li>
                            <li>PMI, ‘India and United Nations: Peacekeeping and Peacebuilding’, n. 8</li>
                            <li>Mukerji, ‘UN Peacekeeping: India’s Contributions’, n. 12</li>
                            <li>Briefly, the plan was for Israel to invade Egypt first, followed by an intervention by the UK and France forcing Israel to withdraw, while the latter would stay back and wrest full control of the canal from the Egyptians. For details see Manuel Frohlich, ‘The Suez Story’, in Carsten Stahan and Henning Melber (eds), (Peace Diplomacy, Global Justice and International Agency, Cambridge: CUP, 2015, pp. 305-40</li>
                            <li>UNSC Resolution S/RES/119 (1956), 31 October 1956; and UN General Assembly, ‘Annexure: Decisions deemed Procedural’, A/RES/267 (III), 14 April 1949</li>
                            <li>UN General Assembly, A/RES/997 (ES-1), 2 November 1956</li>
                            <li>UN General Assembly, A/RES/998 (ES-1), 4 November 1956</li>
                            <li>UN, Summary Study of the Experience Derived from the Establishment and Operation of the Force: Report of the Secretary-General, A/3943, 9 October 1958</li>
                            <li>Paul F Diehl, ‘First United Nations Emergency Force (UNEF I)’, in Koops et al. (eds), The Oxford Handbook of United Nations Peacekeeping Operations, n. 7, p. 151</li>
                            <li>‘Indian Army’s participation in United Nations Peacekeeping Ops’.</li>
                            <li>‘Cable dated 12 July 1960 from the President of the Republic of the Congo and Supreme Commander of the National Army and the Prime Minister and Minister of National Defence Addressed to the Secretary-General of the United Nations’, UNSC, S/4382, 13 July 1960</li>
                            <li>‘Questions Relating to the Situation in the Republic of Congo (Leopoldville)’.</li>
                            <li>Eşref Aksu, ‘The UN in the Congo Conflict: ONUC’, in The United Nations, Intra-State Peacekeeping and Normative Change, Manchester, England: Manchester University Press, 2018, pp. 100–29.</li>
                            <li>UNSC Resolutions: S/RES/143, 14 July 1960; S/RES/145, 22 July 1960; S/RES/146, 9 August 1960; S/RES/161, 21 February 1961; and S/RES/169, 24 November 1961</li>
                            <li>Jane Boulden, ‘United Nations Operations in Congo (ONUC)’, in Koops et al. (eds), The Oxford Handbook of United Nations Peacekeeping Operations, n. 7, pp. 167–68</li>
                            <li>For details see, Satish Nambiar, For the Honour of India: A History of Indian Peacekeeping, New Delhi: CAFHR, USI, 2009, p. 494</li>
                            <li>UNSC, S/RES/751 (1992), 24 April 1992; and Report of the Secretary-General on the Situation in Somalia, S/24343, 22 July 1992</li>
                            <li>UNSC, Report of the Secretary-General on the Situation in Somalia, S/24480, 24 August 1992</li>
                            <li>UNSC, S/RES/775 (1992), 28 August 1992</li>
                            <li>UNSC, S/RES/794 (1992), 3 December 1992</li>
                            <li>UNSC, S/RES/814 (1993), 26 March 1993</li>
                            <li>UNSC, S/RES/878 (1993), 29 October 1993; and S/RES/885 (1993), 16 November 1993</li>
                            <li>UNSC, S/RES/897 (1994), 4 February 1994</li>
                            <li>Laurie Zittrain Eisenberg, ‘From Benign to Malign: Israeli Lebanese Relations, 1948–78’, in Clive Jones and Sergio Catignani (eds), Israel and Hezbollah: An Asymmetric Conflict in Historical and Comparative Perspective, New York: Routledge, 2010, p. 21; and Imad Salamey, The Government and Politics of Lebanon, New York: Routledge, 2013</li>
                            <li>Salamey, The Government and Politics of Lebanon, New York: Routledge, 2013.</li>
                            <li>Salamey, The Government and Politics of Lebanon, n. 38</li>
                            <li>UN, ‘UNIFIL Background’, accessed on 18 November 2020</li>
                            <li>United Nations Security Council (UNSC) Resolutions 425 and 426, 19 March 1978</li>
                            <li>Grant T Hammond, ‘The Perils of Peacekeeping for the US: Relearning Lessons from Beirut to Bosnia’, in Edward Moon Brown (ed.), A Future for Peacekeeping, London: Macmillan Press 1998, pp. 74–75</li>
                            <li>Marrack Goulding, ‘The Evolution of United Nations Peacekeeping’, International Affairs, Vol. 69, No. 3, 1993, p. 453. Also see AK Bardalai, United Nations Interim Force in Lebanon: Assessment and Way Forward, 2021.</li>
                            <li>Raffaella A Deal Sarto, Israel under Siege: The Politics of Insecurity and the Rise of the Israeli Neo-Revisionist Right, Washington, DC: Georgetown University Press, 2017.</li>
                            <li>United Nations Peacekeeping Operations: Principles and Guidelines, n. 14.</li>
                            <li>Colum Lynch, ‘India to Withdraw Large UN Force from Sierra Leone’, The Washington Post, 21 November 2000.</li>
                            <li>‘India Pulling Out of Sierra Leone’, CBS News, 20 September 2000.</li>
                            <li>The author, who was then posted at the Army HQs and was handling various subjects related to the Indian Army’s participation in peace operations, was privy to the internal discussions between the Ministry of External Affairs, Ministry of Defence and the Army HQs regarding the decision to pull out of UNAMSIL.</li>
                            <li>UNSC Resolution, S/RES/1270 (1999), 22 October 1999; and S/RES/1289 (2000), 7 February 2000.</li>
                            <li>Douglas Farah, ‘UN Rescues Hostage in Sierra Leone’, The Washington Post, 16 July 2000 and UNSC, Report of the Secretary-General on the United Nations Mission in Sierra Leone, S/2000/455, 19 May 2000.</li>
                            <li>For more details see, VK Jetley, ‘“Op Khukri” – The United Nations Operation Fought in Sierra Leone Part-I and Part-II,’ Journal of the USI of India, Vol. CXXXVII, 2007.</li>
                            <li>D Donald, ‘Neutrality, Impartiality and UN Peacekeeping at the Beginning of the 21st Century’, International Peacekeeping, Vol. 9, No. 4, 2002, p. 22.</li>
                            <li>Ibid., p. 23.</li>
                            <li>AK Bardalai, ‘United Nations Peacekeeping Operations: Causes for Failure and Continuing Relevance’, Journal of Defence Studies, Vol. 12, No. 4, 2018, pp. 5–34.</li>
                            <li>Jonny Hogg and Louis Charbonneau, ‘Insight—Getting Tough in Congo: Can Risk Pay off for UN Forcers?’, Reuters, 29 August 2013.</li>
                            <li>From the time of its establishment in 2011, India’s participation in UNMISS until now has been significant, comprising over 40,000 peacekeepers. The figures for MONUSCO are around 90,000 peacekeepers, approximately.</li>
                            <li>Butros Butros Ghali, An Agenda for Peace, A/47/277-S/24111, New York: The United Nations, 1992.</li>
                            <li>Yeshi Choedon, ‘India’s Engagement in Development and Peacebuilding Assistance in the Post-Conflict States: An Assessment’, Frontiers in Political Science, 31 May 2021.</li>

                        </ol>
                    </div> -->


                    <!-- <div id="gallery" class="row image-gallery">
                        <div class="col-md-12">
                            <h3>Gallery</h3>
                        </div>

                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="../assets/img/img5.jpg" class="img-fluid" alt="" loading="lazy">
                                <p>Image-1</p>
                            </div>
                        </div>


                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="../assets/img/img6.jpg" class="img-fluid" alt="" loading="lazy">
                                <p>Image-1</p>
                            </div>
                        </div>


                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="../assets/img/img7.jpg" class="img-fluid" alt="" loading="lazy">
                                <p>Image-1</p>
                            </div>
                        </div>


                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="../assets/img/img8.jpg" class="img-fluid" alt="" loading="lazy">
                                <p>Image-1</p>
                            </div>
                        </div>


                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="../assets/img/img13.jpg" class="img-fluid" alt="" loading="lazy">
                                <p>Image-1</p>
                            </div>
                        </div>

                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="../assets/img/img9.jpg" class="img-fluid" alt="" loading="lazy">
                                <p>Image-1</p>
                            </div>
                        </div>

                    </div> -->


                </div>
            </div>
        </div>
</section>


<?php get_footer();  ?>