<?php


$main = "operaitons";

$page = "pawan";

get_header(); ; ?>


<section class="operations-banner" style="background-image: url(../assets/img/operations-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">Operation Pawan: Indian Peace Keeping Force, 1987</h1>
    </div>
</section>




<section class="operation-details" id="faq-section">
    <div class="container">
        <div class="row">

            <?php include('../sidebar/operations-sidebar.php'); ?>

            <div id="background" class="col-md-9">
                <div class="content">
                    <img src="../assets/img/operation-p.png" class="small-banner" width="100%" height="400px" class="img-fluid res-image" alt="" loading="lazy">

                </div>


                <div class="accordion-details">
                    <div class="pawan-war-details">
                        <h4>Background</h4>
                        <p>Sri Lanka is an island nation in the Indian Ocean, separated from the Asian main-land via the Palk Strait and the Gulf of Mannar, thus sharing a maritime border with India. Its northern part is roughly contiguous to the state of Tamil Nadu in peninsular India. A British colony like India, Sri Lanka gained independence from British rule in 1948. As with much of South Asia, Sri Lanka too is a multi-cultural and multi-ethnic society. There are three main ethnic groups in the country: the majority Sinhala (largely Buddhist), Sri Lankan Tamils (mainly Hindus), and Sri Lankan Muslims. During the colonial period, the British moved Tamils from the Indian mainland to work on Sri Lankan tea plantations. A sizeable population of these Tamils was located in the northern part of the country.</p>

                        <p>The origins of the conflict in Sri Lanka can be traced to the time of independence. During British rule, the Tamils made better use of available educational facilities and by the time Sri Lanka became an independent Dominion in 1947, the Tamils dominated in the white and blue-collar jobs. With the end of British rule in Sri Lanka, the government was formed by the majority Sinhalese and steps taken to correct ‘their perception of imbalance between the two communities. Sinhalese was declared as the official language, Buddhism the pre-eminent religion and rules were promulgated that made it difficult for Tamils to get admission in institutes for higher learning.’ While the Sri Lankan Tamil Congress was part of the post- independence government, such policies led to resentment among the Tamils; over the years the grievance of the Tamils against the majority Sinhala increased, and with little opportunity for political resolution, it soon turned into a demand for secession.</p>

                        <p>Power in Sri Lanka has been traditionally shared by two principal parties: the Sri Lanka Freedom Party (SLFP) and the United National Party (UNP). Until 1977, each party had been in power for about 15 years. In the 1970s, Tamil parties came together to form the Tamil United Liberation Front (TULF), which articulated the demand for a separate state of Tamil Eelam in the northern and eastern areas of the country and which would give the Tamils greater say in the country’s federal system. The TULF fought in the 1977 general elections and won 18 seats out of the 24 seats in which it contested. However, there were also other Tamil groups in the fray which were impatient with the political process. The cry for creation of Tamil Eelam saw the emergence of various militant groups like Tamil Eelam Liberation Front (TELF), later renamed Tamil Eelam Liberation Organisation (TELO), Liberation Tigers of Tamil Eelam (LTTE), Eelam Revolutionary Organisation of Students (EROS), People’s Liberation Organisation of Tamil Eelam (PLOTE), and the Eelam People’s Revolutionary Liberation Front (EPRLF). The LTTE, originally called the Tamil New Tigers, was formed in 1972 in Jaffna by a group of ten students among whom was Vellupillai Prabhakaran. It had emerged as a leading rebel faction with an aim to carve a Tamil homeland in northern and eastern Sri Lanka. The LTTE was banned by the Sri Lankan Government in 1978; despite this, it continued to grow in strength. On 23 July 1983, it carried out its first major terrorist act by ambushing a Sri Lankan army patrol killing 15. In reprisal, the Sri Lankan Army went on a rampage in Colombo (Black July Riots) attacking Tamils, and the rioting spread to other parts of Sri Lanka with an estimated 3,000–6,000 Tamils killed. As refugees reached Tamil Nadu the impact of political discord in Sri Lanka was felt in India. It is around this time that India also set about trying to resolve the conflict using diplomatic means.</p>

                        <p>In January 1987, the LTTE, by this time the primary militant group in the country, announced that it was taking over the administration of Jaffna. The Sri Lankan government reacted by imposing an economic blockade of the Jaffna Peninsula, resulting in economic hardship; and refugees again moved out to India. In June of that year, Sri Lanka ‘launched an offensive to regain control of Jaffna [using] air and artillery power. There were reports of heavy civilian casualties that led to an adverse reaction in Tamil Nadu’, which put pressure on the Government of India to take some action. On 4 June 1987, the IAF launched Operation Poomalai and air-dropped supplies over Jaffna, following an unsuccessful naval operation to provide supplies via sea.</p>

                        <p>This foreshadowed the possibility of further Indian action and thus the Government of Sri Lanka, under President JR Jayawardene offered to hold talks with the Government of India. The negotiations resulted in the signing of the Sri-Lankan Accord in Colombo on 29 July 1987. JN Dixit, India’s High Commissioner to Sri Lanka from 1985 to 1989, played a major role in drafting the Indo-Sri Lankan Accord in 1987, and the subsequent induction of the Indian Peace Keeping Force (IPKF) to implement it. The Indo-Sri Lankan Accord stipulated that the Sri Lankan government would give the nation’s provinces more autonomy and power, while withdrawing its military. The Tamil rebels, on the other hand, were to surrender their arms. The Accord did not talk of an Indian peacekeeping force but did say that India would assist in the implementation of this accord.</p>

                        <p>After the Accord was signed, President Jayawardene requested then Indian Prime Minister Rajiv Gandhi for armed assistance to help implement it and to maintain law and order as the Sri Lankan Army was to be confined to barracks as per the agree ment. Thus, the 54 Infantry Division, which was to form the IPKF began induction into Sri Lanka.</p>

                        <p>The IPKF’s initial tasks were defined thus: separate the two warring groups, that is, the Sri Lankan armed forces and the LTTE and ensure the cease-fire; take over the weapons and munitions handed over by the LTTE and other militant groups; ensure the dismantling of all camps established by the Sri Lankan Forces after May 1987; and aid the local population in returning to their homes and provide a sense of security.</p>
                    </div>
                </div>

                <div id="air" class="accordion-details">
                    <div class="pawan-war-details">
                        <h4>Operation Pawan</h4>
                        <p>Operation Pawan was an operation by the IPKF to take control of Jaffna from the LTTE, better known as the Tamil Tigers. The initial task was to separate the Sri Lankan Armed Forces and the LTTE and ensure ceasefire, take over weapons and munitions, ensure dismantling of camps, and aid in the safe return of the local Tamil population. Maj Gen Harkirat Singh, who was commanding the Secunderabad- based 54 Infantry Division became the first commander of the IPKF based at Palali. It was originally assumed that the IPKF would carry out peacekeeping activities as done in United Nations (UN) Peacekeeping missions. However, the IPKF ultimately ended up undertaking ‘peace enforcement’ in Sri Lanka, which it was inadequately prepared and equipped for.</p>
                        <div class="war-2-map-2">

                            <img src="../assets/img/Indian Peace Keeping Force in Sri Lanka, 1987–90-17.jpg" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 600;">Indian Peace Keeping Force in Sri Lanka, 1987–90</p>
                        </div>

                        <p>Troop movements for military operations in Sri Lanka by the Indian Army began on 30 July 1987 under Lt Gen Depinder Singh, who was designated Overall Force Commander (OFC) as he was then General Officer Commanding in Chief, Southern Command, close to Sri Lanka. Maj Gen Harkirat Singh commanded the 54 Infantry Division, minus heavy weapons. The division had to distribute its units around the Northern and Eastern Provinces. Only the 91 Infantry Brigade went to Jaffna Sector, while 76 Infantry Brigade and 47 Infantry Brigade occupied the Trincomalee Sector, the former at the port city of Trincomalee and the latter at Vavuniya in the interior. The Indian forces available at the commencement of operations were:</p><br>

                        <div class="small-note">
                            <ol>
                                <li><b>Jaffna Sector:</b> 54 Infantry Division with two infantry brigades, a squadron of armour, light artillery regiment less one battery, one mechanised company and a parachute commando battalion. Three additional infantry brigades were under induction, the first to arrive between 10-12 October, the second by 15 October, and the third by 18 October 1987.</li>

                                <li><b>Trincomalee Sector:</b> 36 Infantry Division with a brigade each at Vavuniya, Batticaloa and Trincomalee. The division also had an armoured regiment less one squadron, an artillery regiment and two companies of mechanised infantry.</li>
                            </ol>
                        </div>
                        <p>Soon after its intervention in Sri Lanka and especially after the confrontation with the LTTE, the IPKF received a substantial commitment from the IAF, mainly transport and helicopter squadrons under the command of Gp Capt MP Premi. The Indian Navy regularly rotated naval vessels through Sri Lanka waters, mostly smaller vessels such as patrol boats.</p>

                        <p>On 9 October 1987, the Chief of Army Staff, General K Sundarji launched Operation Pawan, which was expected to neutralise the operational capability of LTTE in and around Jaffna. The mission was to capture Jaffna first. The opening moves of Operation Pawan took place with a heliborne assault on Jaffna University, which was the headquarters of the LTTE. The attack was led by a detachment of the Indian Para forces on the night of 11/12 October.
                        <p>

                        <p>On 11 October, the 18 Infantry Brigade’s 12 GRENADIERS, who were advancing to Navatkulli, were ambushed seven times in the move up to Navatkuli, while 4 MAHAR established contact with Kopai North. The 91 Infantry’s 5 MAHAR contacted Chunnakam and 8 MAHAR, in a wide loop to the west, contacted Navanthurai. The seaborne landing had to be aborted and 1 MARATHA attempted some forays from the Jaffna Fort. The HQ 72 Infantry Brigade, 4/5 GR (FF) and 13 Sikh LI, less two companies, arrived in Palali between 11 and 12 October 1987.</p>

                        <div class="war-2-map-2">
                            <img src="../assets/img/gallery/Operation-Pawan.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 600;">A BMP during Operation Pawan in Sri Lanka <br>Source: Army HQ.</p>
                        </div>

                        <p>On 12/13 October 1987, 4 MAHAR of 18 Infantry Brigade cleared Kopai North and contacted Kopai South where it came under intense fire from three directions. The 4th Rifle Company that was moving behind helped with extra ammunition and supplies. 12 GRENADIERS that was advancing Navatkuli could advance only 3 km to its west. 4/5 GR(FF) of 72 Infantry Brigade advanced to, and contacted Pond Crossing, with the 13 Sikh LI, still deficient of two rifle companies. 1 Platoon also joined 4/5 GR (FF). 5 MADRAS of 91 Infantry Brigade secured Chunnakam and 8 MAHAR was ordered to assist 5 MADRAS that wheeled towards North East and secured Udiliv.</p>

                        <p>The heliborne operation in Jaffna was planned to raid a house in Kokuvil where the LTTE leadership was believed to be staying. The operation was launched on the night of 11/12 October 1987. The plan was to first land a para commando team to secure the landing zone, which was to be followed by landing a company of 13 Sikh LI that would take over and secure the helipad. Between 0100 and 0230 Hours, 12 October 1987, 103 Para Commandos and 30 personnel of 13 Sikh LI were heli-lifted. The helidrop ended in a catastrophe when the LTTE intercepted the radio transmission of the commando force. The helicopters came under intense anti- aircraft firing that forced them to abandon the mission halfway through. Out of the total 17, the commandos lost two of their members and while 13 Sikh LI lost 29 out of 30 men.
                        <p>

                        <p>Army HQ then inducted more troops into the Jaffna Peninsula. On 15 October 1987, 41 Infantry Brigade was inducted and arrived in Palali, where it was tasked to advance to Jaffna on 17 October. The 115 Infantry Brigade was inducted on 18 October and was ordered to outflank the main Jaffna defence on 19 October. After two weeks of bitter fighting, the IPKF wrested control of Jaffna and other major cities from the LTTE, but operations were to continue well into November, with major operations coming to an end with the fall of Jaffna Fort on 28 November 1987.</p>

                        <p>The IPKF suffered a considerable number of casualties during the operation. The seizure of Jaffna, while successfully ejecting the LTTE and weakening it, did not destroy it. Rather, a new war began in which the LTTE cadre broke up into small groups and began fighting a guerrilla war.</p>

                        <p>By March 1988, two more divisions were inducted and the deployment was as follows:</p>
                        <div class="pawan-note-2">
                            <ol>
                                <li>Jaffna Peninsula: 54 Infantry Division</li>
                                <li>Vavuniya and Manar: 4 Infantry Division</li>
                                <li>Trincomalee: 36 Infantry Division</li>
                                <li>Batticaloa and Amparai: 57 Mountain Division</li>
                            </ol>
                        </div>

                        <div class="war-2-map-2">
                            <img src="../assets/img/A Jonga based detachment in Sri Lanka-19.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 600;">A Jonga based detachment in Sri Lanka <br>Source: Army HQ.</p>
                        </div>

                        <p>In 1988, the IPKF conducted four major operations in the Northern Province and two in the Eastern Province of Sri Lanka. Additional reinforcements arrived from India: the 4 Infantry Division, 57 Infantry Division, and the 55 (Independent) Mechanised Infantry Brigade. The IPKF now effectively moved towards undertaking counterinsurgency operations in Sri Lanka and continued to do so throughout 1988. It was in one such operation that Major Ramaswamy Parmeswaram of the Mahar Regiment was awarded the PVC posthumously.</p>

                        <p>Operation Vajra took place in February-March 1988, which used four brigades to sweep the North. The LTTE lost valuable base camps in this, but their leader escaped. Operation Virat Trishul took place in March 1988, which was a follow-up designed to neutralise the remaining enemy hideouts. Operation Checkmate took place in May-August 1988, which was focused on trapping the LTTE in the jungles around Mullaitivu on the east coast and Vavuniya. Operation Toofan took place in June 1989, which was another attempt to subdue LTTE field strongholds. Operations Tulip Bloom and Sword Edge were similar sweeps in the East.</p>
                    </div>
                </div>

                <div id="sri" class="accordion-details">
                    <div class="pawan-war-details">
                        <h4>Sri Lankan Politics and IPKF Withdrawal</h4>
                        <p>Sri Lanka’s political crisis required political solutions rather than purely military ones. Keeping this in mind, elections were held in keeping with the Indo-Sri Lanka Accord in November 1988. The Eelam People’s Revolutionary Liberation Front (EPRLF) emerged as the largest party in these elections and its leader, Annamalai Varadaraja Perumal, became the Chief Minister of the North-Eastern Province on 10 December 1988. Subsequently the IPKF provided security for the successful conduct of the Presidential elections in December 1988 and Parliamentary elections in February 1989. However, the Sri Lankan government showed little inclination to devolve the necessary powers and to give the requisite administrative support to the North-Eastern Council. The new President, Ranasinghe Premadasa, was opposed to the Accord and worked to undermine it. He began negotiations with the LTTE and encouraged it to act against the Indian Army as well as the Northeast Provincial Council.</p>

                        <p>Premadasa further demanded that the IPKF withdraw from Sri Lanka and return to India as the Sri Lankan government no longer required its presence. This demand was rejected but the IPKF’s staying on in Sri Lanka would not produce any results without the cooperation of the government. Additionally, Opposition parties at the Centre in India as well as all political parties in Tamil Nadu were increasingly clamouring for the IPKF’s pull out.</p>

                        <p>The Sri Lankan Government declared a surprise cease-fire and the LTTE in July 1989 ended all offensives. In the talks between the governments of India and Sri Lanka, held on 12 September 1989, it was agreed that IPKF would be de-inducted from Sri Lanka by 31 March 1990 and in this event, the last Indian troops embarked on their voyage home on 24 March 1990. In all, the IPKF lost 1,155 men in Sri Lanka between 1987, when it moved into Sri Lanka, and 1990, when it withdrew from the country.</p>
                    </div>
                </div>

                <div id="assessment" class="accordion-details">
                    <div class="pawan-war-details">
                        <h4>Assessment</h4>

                        <p>An assessment of the IPKF must recall the context in which was deployed in Sri Lanka. The choice before the Government of India in June 1987 was difficult: the Sri Lankan offensive into Jaffna was affecting the local Tamil population detrimentally, causing destruction, economic hardship and loss of life. The situation in Sri Lanka was leading to a domestic fallout in neighbouring Tamil Nadu and people of the state as well as local political parties were clamouring for action. The Indo-Sri Lanka Accord might have succeeded if all parties to it were sincere about implementing it. India’s mistake was in not realising that neither the Sri Lankan Government nor the LTTE were serious about making the Accord work. When it began to unravel India was faced with a choice to either withdraw and leave Sri Lanka and the Tamils to their own fate or undertake military action to make the LTTE agree to work within the Accord. The choice of the latter by India led to the induction of the IPKF into Sri Lanka. The events in Jaffna in 1987 highlighted the underpreparedness of the army in urban warfare, particularly fighting guerrillas in an urban environment. There was also lack of intelligence on the LTTE’s capabilities despite the fact that ‘the 54 Infantry Division had been in close contact with the LTTE for a few months and should have been able to judge their potential and their methods.’ </p>

                        <p>As the IPKF got further enmeshed in Sri Lanka, troops were being rushed in from India and being pushed into battle almost immediately with little or no time to orient themselves or learn lessons from what had occurred.</p>

                    </div>
                    <!-- <div id="note" class="accordion-details">
                        <div class="pawan-war-details">
                            <h4>Note</h4>
                            <div class="pawan-note">
                                <ol>
                                    <li>Singh, 'Times of Trial', n. 21, p. 171.</li>
                                    <li>Ibid.</li>
                                    <li>Lt Gen Depinder Singh, <i>Indian Peacekeeping Force in Sri Lanka 1987-1989</i>, New Delhi, Natraj Publishers, 2001, p. 14.</li>
                                    <li>Ibid., pp. 14–15.</li>
                                    <li>Singh, 'Times of Trial', n. 21, p. 170.</li>
                                    <li>Ibid.</li>
                                    <li>Ibid.</li>
                                    <li>Ibid., p. 172.</li>
                                   
                                    <li>Singh, 'Times of Trial', n. 21, p. 171.</li>
                                    <li>Sushant Singh, <i>Mission Overseas, Daring Operations by the Indian Military</i>, New Delhi: Juggernaut Books, 2017, p. 85.</li>
                                    <li>Lt Gen Depinder Singh (Retd), <i>The IPKF in Sri Lanka</i>, Delhi: Trishul Publications, p. 41.</li>
                                    <li>Singh, 'Times of Trial', n. 21, p. 172.</li>
                                    
                                    <li>Singh, 'Times of Trial', n. 21, p. 173.</li>
                                    <li>Dagmar Hellman-Rajanayagam, 'The Tamil Militants—Before the Accord and After', <i>Pacific Affairs</i>, Winter 1988–1989, pp. 603–619.</li>
                                    <li>Rajesh Kadian, <i>India's Sri Lanka Fiasco</i>, Delhi: Vision Books Pvt. Ltd., 1990, p. 117.</li>
                                    <li>SD Muni, <i>Pangs of Proximity: India and Sri Lanka's Ethnic Crisis</i>, New Delhi: SAGE Publications Pvt. Ltd., 1993, pp. 138–140.</li>
                                    <li>Singh, 'Times of Trial', n. 21, p. 175.</li>
                                    <li>Singh, <i>Mission Overseas</i>, n. 48, p. 148.</li>
                                    <li>Singh, 'Times of Trial', n. 21, p. 175.</li>
                                </ol>
                            </div>
                        </div>
                    </div> -->

                    <!-- <div id="gallery" class="row image-gallery">
                        <div class="row image-gallery">
                            <div class="col-md-12">
                                <h3>Gallery</h3>
                            </div>

                            <div class="col-md-4 main">
                                <div class="image-main img-pop">
                                    <img src="../assets/img/gallery-1.png" class="img-fluid" alt="" loading="lazy">
                                    <p>Line of communication in mountian </p>
                                </div>
                            </div>


                            <div class="col-md-4 main">
                                <div class="image-main img-pop">
                                    <img src="../assets/img/gallery-2.png" class="img-fluid" alt="" loading="lazy">
                                    <p>Evacuation of Refugee by IAF</p>
                                </div>
                            </div>


                            <div class="col-md-4 main">
                                <div class="image-main img-pop">
                                    <img src="../assets/img/gallery-3.png" class="img-fluid" alt="" loading="lazy">
                                    <p>Construction of Bridge</p>
                                </div>
                            </div>


                            <div class="col-md-4 main">
                                <div class="image-main img-pop">
                                    <img src="../assets/img/gallery-4.png" class="img-fluid" alt="" loading="lazy">
                                    <p>Army Engineers working at Jhanger Road </p>
                                </div>
                            </div>


                            <div class="col-md-4 main">
                                <div class="image-main img-pop">
                                    <img src="../assets/img/gallery-5.png" class="img-fluid" alt="" loading="lazy">
                                    <p>Army doctor's aid to civil</p>
                                </div>
                            </div>


                            <div class="col-md-4 main">
                                <div class="image-main img-pop">
                                    <img src="../assets/img/gallery-6.png" class="img-fluid" alt="" loading="lazy">
                                    <p>An Army Officer helping a Kashmiri Boy in schooling at Rajaur</p>
                                </div>
                            </div>

                        </div>

                    </div> -->
                </div>
            </div>
        </div>
</section>





<?php get_footer(); ; ?>