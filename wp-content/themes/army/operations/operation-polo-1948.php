<?php

$main = "operaitons";

$page = "polo";

get_header(); ; ?>


<section class="operations-banner" style="background-image: url(../assets/img/operations-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">Operation Polo 1948</h1>
    </div>
</section>




<section class="operation-details" id="faq-section">
    <div class="container">
        <div class="row">
            <?php include('../sidebar/operations-sidebar.php'); ?>


            <div id="back" class="col-md-9">
                <div class="content">
                    <img src="../assets/img/post-indep-war-2.png" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                </div>



                <div id="back" class="accordion-details">
                    <h3>Background</h3>

                    <div class="war-details">

                        <p>When India gained independence from the British in 1947, the various princely states were given the option of joining either of the two new nations of India or Pakistan. Many acceded to India, while others to Pakistan. Hyderabad, which was India’s largest princely state ruled by the Nizam, Mir Osman Ali, declined to join either country at the time. The Government of India had a great interest in Hyderabad as the Nizam’s sprawling domains were located at the very centre of India. It had a majority Hindu population, and the capital city of Hyderabad was a major transportation and communication hub.</p>

                        <p>Hyderabad’s refusal to join either India or Pakistan upon independence led the Government of India to offer a ‘Standstill Agreement’ to the Nizam, which was signed in November 1947 by Governor General Lord Mountbatten and the Nizam Mir Osman Ali Khan. The agreement assured was made that the status quo would be maintained without any military action. However, by 1948, while the other princely states had acceded to India, Hyderabad had not done so. Moreover, Hyderabad violated the terms of the agreement. ‘The Nizam appointed an agent in Pakistan and also gave a loan of 20 Crore Rupees to Pakistan secretly. The Razakars, which were ‘a private militia aligned to the seventh Nizam, Mir Osman Ali Khan, were headed by Qasim Razvi…[and] reportedly committed atrocities on Congress supporters, Communists and other people who wanted an end to Nizam’s rule or favoured a merger with India. Between 1946 and 1948, Razvi controlled a force numbering anywhere between 50,000 to 2 lakh men.’ The Razakars were apparently against the signing of the Standstill Agreement while envoys from Hyderabad accused India of attempts to economically isolate the state.</p>

                        <p>In 1948, the decision was taken to move the armed forces into Hyderabad to make it accede to the Indian Union. Home Minister Sardar Vallabh Bhai Patel, who was also the Minister in charge for the accession of the princely states, took the decision to annex Hyderabad. ‘Operation Polo’ was a military operation, by which Hyderabad was to be annexed into the Indian Union. ‘Though termed a “Police Action”, the Army played a stellar role in quelling internal strife in the State and restoring law and order.’</p>

                        <p>The Indian troops marched into Hyderabad in the early hours of 13 September 1948. The 1 Armoured Division, under the command of Major General (Maj Gen) JN Chaudhuri (later Chief of Army Staff) delivered the main thrust against Hyderabad, with the objective of reaching Hyderabad city as quickly as possible. The plan consisted of two thrusts, one from Vijayawada in the east and the other from Sholapur in the west:</p>

                        <p>1. Maj Gen AA Rudra led the attack from Vijayawada which comprised the 2/5 Gurkha Rifles, one squadron of the 17th (Poona) Horse, and troops from the 19th Field Battery along with engineering and ancillary units. The Vijaywada attack was to accomplish the following tasks:</p>

                        <ul class="low-letter">
                            <li>Launching the Eastern Striking Force into Hyderabad territory, so that it could advance to Suriapet and, if necessary up to Chityal;</li>
                            <li>Protecting the line of communication behind the Eastern Striking Force; and</li>
                            <li>Safeguarding the Gannavaram airfield and the strategic railway bridges at Vijayawada and Rajahmundry, together with the rail-line from Rajahmundry to Tenali.</li>
                        </ul>


                        <p>2. The attack from Sholapur was led by Maj Gen Chaudhari. For this purpose, 1 Armoured Division was to be composed of 1 Armoured Brigade and 9 Infantry Brigade, along with the usual divisional complement of artillery, engineers, signals, etc. The armoured units were 17 Horse, 3 Cavalry, and one troop of 18 Cavalry. The artillery component included 1 Field Regiment of 25 pounder self-propelled guns, 34 Anti-Tank Regiment and 9 Para Field regiment. 7 The Sholapur attack was composed of four task forces:</p>

                        <ul class="low-letter">
                            <li>Strike Force comprising a mix of fast-moving infantry, cavalry and light artillery;</li>
                            <li>Smash Force consisting of predominantly armoured units and artillery;</li>
                            <li>Kill Force composed of infantry and engineering units; and</li>
                            <li>Vir Force which comprised infantry, anti-tank and engineering units.</li>
                        </ul>



                        <p>The Indian Army was to be given air support by the Indian Air Force (IAF) which undertook the air operations, the plans for which were chalked out as early as June 1948.</p>




                    </div>

                </div>

                <div id="ground" class="accordion-details">
                    <h3>Ground Operations</h3>


                    <div class="war-details">

                        <p>From commencement on 13 September till completion on 17 September 1948, Operation Polo lasted for a total of five days. On 13 September, Indian forces entered the state early in the morning. The very first battle was fought at Naldurg Fort on the Sholapur-Secunderabad Highway. It was located between a defending force of the 1st Hyderabad Infantry and an attacking force of the 7 Infantry Brigade, which managed to secure a vital bridge on the Bori River intact. This was followed by an assault by 2 Sikh Infantry on the Hyderabadi positions at Naldurg. The Smash Force moved into the town of Jalkot, paving the way for the Strike Force units under Lieutenant Colonel Ram Singh to pass through. By the evening, the forward troops of Strike Force were at Umarga, 1 Armoured Brigade was at Yenegur, 7 Infantry Brigade was near Jalkot, and 9 Infantry Brigade was at a point 6 km south-east of Tuljapur.8 At dawn, when they reached Tuljapur, they encountered resistance from a unit of the 1st Hyderabad Infantry and about 200 Razakars who fought for two hours before surrendering.</p>

                        <p>The forces led by Maj Gen AA Rudra in the East, met with fierce resistance from two armoured car cavalry units of the Hyderabad State Forces, namely, the 2nd and 4th Hyderabad Lancers. However, the forces managed to reach the town of Kodar and reached Mungala by the afternoon.</p>

                        <p>On the second day, the Strike Force under Lieutenant Colonel (Lt Col) Ram Singh remained in the lead. The Smash Force moved just behind the Strike Force, ready to take and destroy any major enemy opposition. The forces that were at Umarga proceeded to the town of Rajeshwar and secured it by the afternoon with the help of airstrikes that were called in from the squadrons of Tempests. Meanwhile, assault forces from the East came under heavy firing by the 1st Lancers and 5th Infantry. At the same time, the 3/11 Gurkha Rifles and a squadron of 8th Cavalry attacked Osmanabad and took the town after combat with Razakars. Further, the city of Aurangabad was captured by a force under the command of Maj Gen DS Brar. Six columns of infantry and cavalry attacked the city, resulting in the surrender of the civil administration.</p>

                        <p>On day three, 3/11 Gurkhas left a company to occupy the town of Jalna and the remaining force moved to Latur. The Strike Force surrounded the town of Humnabad and the Smash Force entered it, where a few Razakars resisted the forces. The Smash Force left Humnabad in the hands of 9 Infantry Brigade and began its advance on the subsidiary road to Bidar. Most of the Hyderabadi defences were cleared by airstrikes at the town of Suryapet. The Razakars resisted the 2/5 Gurkhas that occupied the town. Another incident occurred at Narketpally where a Razakar unit was decimated by the Indian forces.</p>

                        <p>On day four, the Strike Force under Lt Col Ram Singh was directed to capture the road junction to the west of Zahirabad, then advancing to Bidar Road. Smash Force followed closely behind the Strike Force, which was to continue down the main road to capture Zahirabad and go on to Sadaseopet. On reaching the junction of the Bidar Road with the Sholapur-Hyderabad City Highway, the forces encountered gunfire from ambush positions. The bulk of the force moved on, leaving some of the units to handle the ambush. They reached 15 km beyond Zahirabad by nightfall in spite of hit-and-miss resistance along the way. Most of the resistance was from Razakar units who ambushed the Indians as they passed through urban areas. The Razakars were able to use the terrain to their advantage until the Indian Army brought in their 75 mm guns. The army wanted to use minimum force and cause the least destruction of life or property in Hyderabad state, but it was found necessary to allow the tanks to use their main guns of 75 mm at Zahirabad. Maj Gen El Syed Ahmed Edroos, the commander of the Hyderabad forces, was called upon to surrender and avoid needless losses and human suffering.</p>


                        <p>On 17 September 1948, the Strike Force headed to enter Bidar. Smash Force along with 2/1 GR were ordered to capture Sadaseopat. 7 Infantry Brigade was to pass through 9 Infantry Brigade to clear up the major village of Digwal, east of Zahirabad. In the morning, a squadron of 3 Cavalry took up the advance, which was joined by the 1 Armoured Brigade Group that reached Sadaseopet at noon. The forces led by the 1st Armoured Brigade were at the town of Chityal, while another column took over the town of Hingoli. The Main HQ 1 Armoured Division and HQ 9 Infantry Brigade settled down near Zahirabad, while HQ 7 Infantry Brigade was in the village of Digwal.</p>

                        <p>As the Indian forces moved closer towards Hyderabad, on 17 September, Hyderabad Radio announced an offer of surrender by the Hyderabad State Forces. At 1600 Hours on 18 September, Maj Gen JN Chaudhuri accepted the surrender of Maj Gen El Edroos at a simple ceremony and was appointed ‘Military Governor’ of Hyderabad.</p>


                    </div>

                </div>


                <div id="formal" class="accordion-details">
                    <h3>Formal Accession</h3>


                    <div class="war-details">

                        <p>On 18 October 1948, a new Military Government of Hyderabad including two Hyderabad members was appointed. Maj Gen JN Chaudhuri, who led Operation Polo, remained in charge of the state’s armed forces and police, and as Military Governor till December 1949. The Chief Civil Administrator, SD Bakhle, was placed in charge of Home and Judicial Affairs as well as Supply and Railway. The Additional Chief Civil Administrator, DR Pradhan, received the charge of Finance, Revenue, Commerce and Industry. Nawab Zain Yar Jung took over Public Works, Labour and Rural Reconstruction and Raja Dhoondi Raj was to look after Education, Customs and Excise, and Relief and Rehabilitation.</p>

                        <p>On 29 October 1948, the future of Hyderabad State was discussed by Sardar Vallabh Bhai Patel along with VP Menon and Maj Gen Chaudhuri. Prime Minister Jawaharlal Nehru visited Hyderabad and addressed a meeting of nearly 50,000 people on 25 December 1948, in which he stressed on the ending of feudal conditions and the carrying out of agrarian reforms in the state.</p>

                        <p>In a Farman dated 24 November 1949, the Nizam declared: ‘Whereas in the rest of India…in political, economic and other fields, it is desirable that…between the State and the Union of India…I declare and direct that the Constitution of India… shall be the Constitution of Hyderabad.’ The Nizam’s acceptance of the Indian Constitution thus formalised Hyderabad’s accession to the Indian Union.</p>


                    </div>

                </div>

                <!-- <div id="note" class="accordion-details">
                    <div class="pawan-war-details">
                        <h4>Note</h4>
                        <div class="pawan-note">
                            <ol>
                                <li>Srinath Raghavan, War and Peace in Modern India, Ranikhet: Permanent Black, 2010, p. 65.</li>

                                <li>Maj Gen (Retd) LS Lehl, ‘Army of the Republic: Phase of Consolidation’, in Maj Gen (Retd) Ian Cardozo, AVSM, SM (ed.), The Indian Army: A Brief History, New Delhi: USI, 2007, p. 75.</li>

                                <li> Rahul Devulapalli, ‘Tracing Razakar Legacy: When Razvi’s Granddaughter Visited Hyderabad’, The Week, 30 September 2021.</li>

                                <li>RNP Singh, Sardar Patel—Unifier of Modern India, New Delhi: Vitasta, 2018, p. 151.</li>

                                <li>SN Prasad, Operation Polo—The Police Action against Hyderabad, 1948, Delhi: Ministry of Defence, 1972, pp. 74-75.</li>

                                <li>Ibid., p. 44.</li>

                                <li>Ibid., pp. 56-57.</li>

                                <li>Ibid., p. 58.</li>

                                <li>Ibid., p. 65.</li>

                                <li>Ibid., p. 67.</li>

                                <li>Lehl, ‘A Nation Divided and the 1947 Indo-Pak War’, p. 76.</li>

                                <li>Prasad, Operation Polo: The Police Action against Hyderabad, p. 115.</li>

                            </ol>
                        </div>
                    </div>
                </div> -->





                <!-- <div id="gallery" class="row image-gallery">
                    <div class="col-md-12">
                        <h3>Gallery</h3>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">

                            <img src="../assets/img/img5.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img6.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img7.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img8.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img13.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="../assets/img/img9.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>





<?php get_footer(); ; ?>