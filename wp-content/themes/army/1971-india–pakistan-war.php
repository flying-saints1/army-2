<?php 

$main ="wars";
$page="post-indeep";
$subPage="1971";
$sidebar= "gallery";

include('header.php'); ?>


<section class="post-independence-war-banner"
    style="background-image: url(./assets/img/post-independence-war-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">1971 India–Pakistan War</h1>
    </div>
</section>


<section class="indep-war" id="faq-section">
    <div class="container">
        <div class="row">
            <?php include('sidebar/post-indeep-sidebar.php'); ?>

            <div class="col-md-9">
                <div class="content">
                    <img src="assets/img/small-war.png" width="100%"  class="img-fluid res-image" alt="" loading="lazy" loading="lazy">
                    <h3 class="sino-war-text">1971 India–Pakistan War</h3>
                </div>


                <div id="about" class="accordion-details">
                    <h3>Background</h3>

                    <div class="war-details">
                        
                        <p>The 1971 war was a manifestation of domestic political estrangement between the two halves of the Pakistani state. With little beyond a common religious belief and a deep distrust of India to bind them, these two halves slipped towards civil war as discontent in the eastern part (now Bangladesh) grew into an autonomy movement in the late 1960s. The breaking point came in March 1971 when the Bengalis (the ethnic group which constituted the vast majority of East Pakistan’s population) were denied the fruits of their victory in the December 1970 elections. The principal East Pakistani party, the Awami League, had won the majority of seats in the country’s parliament, but the politically dominant West Pakistani leadership—particularly the ambitious head of Pakistan People’s Party, Zulfikar Ali Bhutto—had no desire to relinquish supremacy, and thus stalled on convening the National Assembly. The law-and-order situation deteriorated and the tensions between the two sides reached unbearable levels. Last-minute attempts at negotiations foundered on the irreconcilable differences between Awami Party leader, Sheikh Mujibur Rahman, on the one side, and the military government of West Pakistan under General Agha Mohammed Yahya Khan on the other. </p>

                        <p>The Pakistan Army, overwhelmingly drawn from West Pakistan, indulged in harsh repression in a misguided effort to control the situation through military force. Under a plan called ‘Operation Searchlight’, West Pakistani troops endeavoured to disarm Bengali elements of the armed forces, paramilitary and police, while arresting hundreds of suspected separatists. Predictably, the Bengalis retaliated and open rebellion broke out in the east. The rebels proclaimed the independence of ‘Bangladesh’ on 26 March 1971. By early April, a provisional government had come into existence at ‘Mujibnagar’ in India. The level of violence increased during the spring and summer of 1971, as a result of which hundreds of thousands of refugees began to pour into India, including thousands of former East Pakistani regular soldiers and paramilitary troops. The Pakistan Army managed to re-establish a degree of control over most of the cities and towns in the east by the end of May, but no viable political solution was forthcoming from Islamabad to exploit this momentary military success. As a result, the rebellion, or civil war as some participants termed it, slowly grew in intensity and scope in the monsoon months, between May and October.</p>

                        <p>The civil conflict in East Pakistan presented a major challenge to Indian policy, particularly the enormous burden imposed by millions of refugees who had arrived by midsummer. Beyond the strain on finances and social services, India was deeply concerned about the leftist elements within the Bengali separatist movement. Naxalites had conducted an especially vicious militant campaign in eastern India in the late 1960s and New Delhi had no desire to see that insurgency reignited by Bengali radicals, or see East Pakistan established as an independent state under the auspices of leftist extremists. At the same time, the government had to contend with widespread sympathy for the Bengalis and the resultant outcry for intervention from opposition politicians and the public at large. Many Indians advocated exploiting Pakistan’s predicament, including K Subrahmanyam, a leading commentator and Director of the Institute of Defence Studies and Analyses, who urged military action to take advantage of what he called ‘an opportunity the like of which will never come again’. Though other important Indian writers promoted restraint, Subrahmanyam’s statements received wide publicity in Pakistan, deepening animosity and leaving a lasting impression.</p>

                        <p>Domestic pressure notwithstanding, the initial Indian reaction was relatively cautious. Indian Border Security Force (BSF) troops began providing low-level assistance to Bengali rebels (the Mukti Bahini or Liberation Force) in early April in the form of safe havens, training and limited arms. However, New Delhi chose not to recognise the provisional government of Bangladesh (declared on 17 April 1971), and it also did not authorise direct military action across the border. In a meeting on 29 April, Prime Minister Indira Gandhi apparently considered ordering a military advance, but Foreign Minister Swaran Singh counselled restraint and recommended holding military intervention in reserve in case ‘interim measures did not resolve the East Pakistan crisis’ diplomatically. Similarly, Gen SHFJ Manekshaw, the Chief of the Army Staff and simultaneously the Chairman of the Joint Chiefs of Staff Committee, advised Gandhi that India’s armed forces would need some time to prepare for the conflict. Moreover, the imminent arrival of the monsoon would prevent major operations until November at the earliest. Prime Minister Indira Gandhi agreed to a delay, but ordered Gen Sam Manekshaw to plan for war as a policy option for the future.</p>


                        <p>The Indian leadership, thus, seems to have hoped for a resolution short of open war in the intervening period, as long as the refugees departed and stability returned to the east either through a resolution that gave power to a moderate Awami League regime within the Pakistani state or through the creation of an independent Bangladesh. From spring onwards, however, India began to prepare, both diplomatically and militarily, for the possibility of an armed contest: securing international support; increasing assistance to the Mukti Bahini; and boosting military readiness. During the summer and fall, Pakistan also placed its military on a war footing, transferring significant forces to the east (9th and 16th Divisions as well as several thousand paramilitary and police) and raising new units in the west to take their places (17th and 33rd Divisions). However, the counter-insurgency campaign did not go well for Pakistan. As the months passed, the Mukti Bahini improved with Indian assistance, while the Pakistani security forces, isolated amidst a hostile population, struggled with physical and mental exhaustion. Though relatively secure within their cantonments, they were unable to quell the unrest within East Pakistan; collect reliable intelligence; or execute unrealistic orders to ‘seal’ the long, twisting border with India in what one commander characterised as an exercise in ‘quixotic dreaming’.</p>

                        <p>The Eastern Command’s successes were local and transitory. The civil war became a cruel and seemingly endless cycle of raids, ambushes, bombings, sabotage, assassinations and shellings. Pakistan could not eradicate the guerrillas, and the Mukti Bahini by themselves lacked the strength and coordination to force a Pakistani withdrawal. Brutality was commonplace on both sides and the death toll climbed to thousands. By November 1971, the Indian Army and paramilitary troops were regularly providing artillery support to the Mukti Bahini and, towards the end of the month, the army had even made several small incursions into East Pakistan. Cross-border raids and artillery exchanges by both sides became common. Ultimately, in the evening of 3 December 1971, Pakistan, whose strategy for defence of the east called for offensive in the west, launched its air force against several targets in western India. That night, the Pakistan Army opened an offensive all along the western front, from Poonch in Kashmir to Longewala in the Rajasthan desert.</p>

                        <p>India had been preparing for Pakistan to make the first move and had drafted its plans accordingly. Its principal focus was in the east, where it was hoped that a well-conceived offensive would crush Pakistani resistance quickly and result in the conclusive establishment of Bangladesh as an independent country. India’s Eastern Command overran East Pakistan in two weeks.</p>
                        
                        <p>Despite often spirited resistance, the Pakistani defenders were faced with an impossible strategic situation. Their commander, Lt Gen AAK Niazi, signed an instrument of surrender at 1630 Hours on 16 December 1971 and a ceasefire went into effect on both fronts at 2000 Hours the following day, leaving some 90,000 Pakistani prisoners of war and civilian detainees in Indian hands. In the west, the two armies sparred indecisively, each side’s small advances being balanced by the other side’s gains. The only exception was the dramatic drive by India’s 11th Division into the sandy wastes of Pakistan’s Sindh province, which netted India approximately 4,500 sq km of Pakistani territory, albeit barren desert. Pakistan’s military debacle led to a political upheaval that ended with Zulfikar Ali Bhutto coming to power as the truncated country’s new leader. After prolonged wrangling and some additional skirmishing, Bhutto and Gandhi signed the Simla Accord on 3 July 1972. Innumerable details remained unresolved, however, and the two sides did not come to an agreement on the redefinition of the CFL, from thenceforth to be called the Line of Control (LOC), in Kashmir until December 1972. The first exchange of prisoners also took place that month, but the last of them, held hostage to bitter political machinations, did not return home until April 1974.</p>

                        

                    </div>

                   
                
                    <div class="war-details">
                        <h4>Strategies and Forces</h4>
                        <p>The Indian strategy in 1971 gave primacy to operations in East Pakistan. For decision makers in New Delhi, the aim of the war was to create conditions that would allow the refugees to return and would leave an Awami League government in power in Dacca. Early Indian plans thus focused on support to the Mukti Bahini and diplomatic efforts to bring about a political resolution to the crisis. By autumn, however, when it became clear that neither diplomacy nor insurgency would suffice to achieve India’s objectives, preparations for a military invasion of East Pakistan were accelerated, including seizure of enclaves along the frontier. The focus on the east dictated that the war in the west would be a defensive holding action. No ground was to be lost, especially in Kashmir, and offensive operations were authorised where feasible to gain advantages for use at the negotiating table or to pre-empt a Pakistani advance, but the war would be won or lost in the east.</p>

                        <p>Prime Minister Gandhi rejected suggestions that the conflict be prolonged to permit the conquest of major areas in West Pakistan or the destruction of the Pakistan Army. The time element was also key to Indian planning. Based on the assumption that only a few weeks would be available before foreign pressure brought the war to a halt, Indian plans stressed the need for a quick invasion that could deliver decisive results before the international community could intervene in any significant manner. Finally, the Indian military was to ensure the security of the border with China.</p>


                        <p>Although the political leadership was confident that Beijing would not intervene with armed force, the Indian commanders had to include China in their planning. Indeed, one reason operations were delayed until November/December was to allow winter snow to close the passes leading from Tibet into India. Pakistan’s strategy was almost the exact opposite of India’s. Recognising the near impossibility of successfully holding its eastern half against a determined Indian onslaught, Pakistani strategy was predicated on the conviction that the east would have to be defended in the west. In practical terms, this axiom meant that Pakistan would launch a major offensive into India from the west at the start of any conflict. By threatening vital Indian assets, such as Kashmir and the Punjab, Pakistani planners hoped to draw Indian forces away from the east and gain enough time for outside powers to restrain New Delhi. In the meantime, Pakistan’s Eastern Command, isolated and effectively surrounded, would have to hold on as best it could. Pakistan also hoped for a short war. First, because of the difficulty in defending the east for any extended period of time against a determined foe; and second, because the military services, especially the army, were desperately short of replacement equipment and reserve manpower necessary for a lengthy conflict. Contrary to India’s fairly realistic appraisal of the likelihood of international intervention, many Pakistani officers hopefully assumed that China, and possibly others, would play an active combat role if the situation escalated to war.</p>

                        <p>The forces available to the two sides to execute their strategies were among the largest in the world at the time. India’s army of 833,800 men could field 14 infantry divisions, 10 mountain divisions, two parachute brigades, as well as an armoured division and four independent armoured brigades. Key items of combat equipment included more than 1,450 tanks and 3,000 artillery pieces. Beyond first-line troops, India enjoyed a considerable advantage in reserves of personnel and equipment over Pakistan; it thus had a substantial ability to endure losses and to continue a conflict longer than its adversary.</p>

                        <p>At 365,000 men, the Pakistan Army was about half the size of its Indian counterpart, but was nonetheless a formidable force with two armoured divisions, 13 infantry divisions and three independent armoured brigades with approximately 850 tanks and 800 guns. Two of these infantry divisions (the 17th and 33rd), however, were still being organised and suffered from numerous difficulties attendant upon construction of military formations. Three divisions were in East Pakistan, the original garrison (14th Division) having been joined in March/April by the 9th and 16th Divisions (albeit minus much of their equipment). Lt Gen Niazi organised two additional infantry divisions in the east (the 36th and 39th divisions), but these were divisions in name only—a futile attempt to make the Eastern Command appear more powerful than it was. In fact, each was hardly more than brigade strength, lacking staff, supporting arms and equipment of every sort. It is noteworthy that both armies relied heavily on foot-mobile infantry divisions, having only limited capability to conduct mechanised warfare despite the large overall size of the ground forces. Both sides supplemented their regular troops with extraordinarily large paramilitary establishments (some 280,000 in Pakistan’s case, for example), but, as with regular units, India had a significant numerical advantage in paramilitary forces.</p>


                        <p>Pakistan also attempted to form a considerable body of hastily assembled militia and home guard units for rear area duties and counter-insurgency missions, especially in East Pakistan. The results, however, were disappointing and many of these units in both wings proved unreliable, unless backed by regulars. The air arms of both countries were commensurately large. The IAF, with some 625 combat aircraft and over 450 transport and support planes. outnumbered the 273 fighters and bombers in the PAF. Moreover, the IAF had improved in quality since its controversial showing during the 1965 war. The Indian Navy had also grown since 1965. By 1971, it was not only one of the few navies in the world to feature an aircraft carrier (INS Vikrant) in its order of battle but also included 21 other major surface combatants, four submarines and a number of patrol boats that would play an interesting role in the war. Pakistan’s Navy also had four submarines, but its surface fleet counted only eight major combatants and a few patrol boats.</p>

                        <p>Although India enjoyed a significant quantitative superiority in numbers of soldiers and equipment, the two militaries were fairly equal in qualitative categories, such as maintenance, logistics, training and leadership, as they entered the war. With respect to command and control, however, Pakistan suffered from serious problems. On taking over as President and Chief Martial Law Administrator in early 1969, Gen Yahya Khan had retained his previous position as Commander in Chief of the Army. He thus placed himself at the pinnacle of a tangled bureaucratic structure, with responsibility for domestic and foreign policy as well as army issues and the operational direction of all three services in combat. The burden proved too great. Furthermore, he exacerbated this confused situation by filling the staff of the martial law administration with military officers (mostly army) and appointing serving officers to key positions in the country’s governmental structure. The requirements of administering the martial law regime not only distracted the armed forces from their primary missions but also created an atmosphere in which lines of command and authority were blurred and uncertain, slowing reaction time and fostering confusion. Joint operations among the three services—ground, air and sea—were another weak point for Pakistan’s command and control system. In the words of the official post-war analysis, ‘there was an utter lack of joint planning by the three services’, so that the Pakistanis were not able to generate the synergy inherent in multi-service operations on a consistent basis.</p>

                        <p>Pakistan’s command problems were compounded by poor communications between the different HQ in the eastern and western wings. While Lt Gen Niazi created misleading impressions by submitting rosy appreciations of the deteriorating situation in the east, his superiors in Rawalpindi did not always keep him apprised of plans at the national level. Senior officers in Dacca ‘had no prior knowledge of the start of the war, nor of the developments on the ground and in the air’ on 3 December 1971. Similarly, the Commander in Chief of the Pakistan Navy and other senior officials apparently only learned of the attack on India from news broadcasts. The Indian Army, IAF and Indian Navy also experienced problems in coordinating their actions, especially in the west, but the overall level of joint cooperation was much higher. In the eastern theatre in particular, the three services worked together quite effectively, largely as a result of the senior leaders’ personalities and professionalism. Nonetheless, as an Indian division commander during the war has observed: ‘there were flaws which might have caused serious danger under other circumstances.</p>

                   
                <div id="eastern" class="accordion-details">
                    <h3>Eastern Front</h3>
                    <div class="war-details">
                        <p>East Pakistan presented an extraordinarily challenging theatre of operations for both sides. Marshes, rice paddies and innumerable small lakes and watercourses made movement difficult, effectively prohibiting major military operations during the monsoon (approximately May to late September). The region’s transportation infrastructure was very weak in 1971, with few all-weather roads capable of accepting heavy military traffic and even fewer railroads. Moreover, the larger rivers broke the country into four principal zones: north-west, south-west, centre and east. These sectors were connected by only two railroad bridges: the Hardinge Bridge across the Padma (Ganges) linking the south-west and the north-west; and a bridge at Ashuganj across the Meghna River to tie the east and central zones together. With the exception of these two bridges, neither of which was decked for vehicular traffic, movement across the Padma, Jamuna and Meghna could only be accomplished by water or by air.</p>


                        <p>For Pakistan’s planners, the situation in the east was enormously daunting. Lt Gen Niazi’s Eastern Command was surrounded on its three land sides by the numerically superior Indian Army; its sea flank was dominated by the Indian Navy; and its limited air cover was unlikely to endure beyond the first few days of action. No substantial reinforcements could be expected once the war began. Moreover, movement, security and intelligence collection inside East Pakistan were badly hampered by the activities of the Mukti Bahini. Guerrilla actions, insufficient logistical support and the pervading sense of isolation also contributed to the psychological exhaustion of the Pakistani troops and commanders. Describing the mental state of the troops, a division commander commented that their ‘minds were clogged by an incomprehensible conflict’. Similarly, a brigade commander in 14 Division recorded that 3 December found his men already ‘very near exhaustion’ and burdened by ‘terrible fatigue and sleeplessness’. Further, growing distrust of their Bengali comrades-in- arms exacerbated the anxiety and uncertainty among the West Pakistani soldiers.</p>

                        <p>The forces available were inadequate to cope with these pressing external and internal threats. Lt Gen Niazi had only three regular infantry divisions in Eastern Command, two of which lacked their full complement of artillery, vehicles and signals equipment. Only one armoured regiment (29 Cavalry) was on hand and its tanks were mostly World War II vintage American M24 Chaffees, supplemented by a handful of Russian PT-76s captured from India during the 1965 war. Two additional ‘ad hoc’ division HQ (36th and 39th) and several brigades were created during the autumn of 1971 in an effort to deceive Indian intelligence, but these were cobbled together hastily from existing resources and did little to improve command and control. The problems experienced by these units led one Pakistani commentator to describe the 36th Division as ‘a hoax. A patchwork of a formation put on crutches.’</p>

                        <p>The other services were equally threadbare. The PAF presence was limited to one squadron (No 14) consisting of 16 Sabre VIs and three RT-33s. The Pakistan Navy in the east was capable of some riverine operations, but with only four patrol craft and some two dozen improvised or confiscated river vessels, it could hardly challenge the Indian Navy, especially in the absence of air cover. Curiously, Niazi did not have formal authority over the air and navy components of the force in East Pakistan; they were simply expected to cooperate with him in his guise as Commander, Eastern Command. Under such circumstances, it is perhaps not surprising that one Pakistani General later commented, ‘the defeat in East Pakistan was inevitable’.</p>

                        <p>Lt Gen Niazi’s plans for the defence of the east were formulated in July 1971 and approved by Army HQ the following month. The primary aim was to prevent the Mukti Bahini and the Indians from seizing a parcel of East Pakistan from which an independent state of Bangladesh could be declared. Associated with this strategic aim was the key assumption that India would not launch a full-scale attack in the east, but would limit its actions to steadily increasing support for the Mukti Bahini. Niazi’s general concept, therefore, called for a ‘forward posture of defense’ along the borders to prevent incursions, with the army falling back to so-called ‘strong points’ and ‘fortresses’ if necessary. Thus, if the Indians invaded, the army would supposedly fall back slowly and endeavour to hold out in these fortresses as long as possible to allow the offensive in the west to take effect.</p>

                        <p>There were several serious flaws in this concept. First, the cordon defence along the frontier was brittle because there were insufficient troops to defend the entire border while trying to prosecute the counter-insurgency war. Second, the Pakistani Eastern Command had practically no reserves, making the strong point defence a questionable proposition. Moreover, with limited mobility and faced with a hostile population in difficult riverine terrain, there was, at best, small likelihood that the forward Pakistani troops would be able to withdraw to the fortresses intact. Niazi compounded the withdrawal problem by decreeing that no unit could pull back until it had suffered at least 75 per cent casualties. Third, no regular troops were left to defend Dacca, even though the planners recognised that the capital was the nerve centre of East Pakistan with enormous symbolic importance. One Pakistani veteran, summarising the problem concisely, stated: ‘Pakistan fought the war in East Pakistan with a troop deployment designed for internal security operations.’ The most curious aspect of the plan, however, was Niazi’s desire—with which the Army General HQ initially concurred—to launch offensive operations into India at several points. A subsequent Pakistani commentator regarded this proposal as ‘sheer folly’.</p>

                        <p>Across the border was India’s Eastern Command, under Lt Gen Jagjit Singh Aurora. The orders issued to Lt Gen Aurora instructed him to destroy the bulk of the Pakistani forces in the east and to occupy most of East Pakistan. These two objectives seem to have been allotted equal priority by the Army HQ, and Aurora addressed them by preparing plans to seize territory up to the lines of the major rivers through offensives in the south-western, north-western and eastern sectors. One corps was allotted to each of these sectors: 2 Corps in the south-west; 33 Corps in the north- west; and 4 Corps in the east. Indian offensive operations in the east were to begin in the first week of December, and New Delhi was thus both pleased and relieved when the Pakistani air strikes on the afternoon of 3 December (and ground attacks that night) came in advance of the planned operations. Notably absent from the Indian plans was any formal reference to Dacca. Army planners thought it would be impossible to secure the city within the time frame of a short war. As a result, the best approach to the East Pakistani capital, the central or northern sector, was initially assigned only one regular brigade under an ad hoc HQ called 101 Communications Zone Area. Nonetheless, several senior Indian generals certainly had their eyes on the city.</p>

                        <p>Verbal coordination between Maj Gen Gurbax Singh Gill, GOC 101 Communications Zone, and the Chief of Staff at Eastern Command, Maj Gen JFR Jacob, led to an outline plan for a possible advance on Dacca, including a parachute drop at Tangail in battalion strength. Likewise, Lt Gen Sagat Singh, GOC 4 Corps, clearly intended to head for Dacca if the opportunity arose, even though this was not part of his written orders. More than Dacca, the possibility that Pakistani forces would escape by sea had been on Gen Manekshaw’s mind in New Delhi. The Indian Navy, however, promised to deny Pakistan the use of Chittagong and Chalna Ports, reducing his concern about this eventuality. Manekshaw would later come to worry about the potential for an overland withdrawal into Burma.</p>

                        <p>With six full divisions (plus parts of 6th Mountain Division), four additional brigades and the equivalent of three armoured regiments, India’s Eastern Command decidedly outnumbered Lt Gen Niazi’s defenders. Several additional brigades and the HQ of the 6th Mountain Division would be added to this imposing force as the war progressed and the likelihood of Chinese intervention diminished. Although the mountain divisions composing Eastern Command had few vehicles, a squadron of approximately 12 operational Mi-4 helicopters was available to enhance mobility and there were sufficient transport aircraft to airdrop up to a battalion of 50 Para Brigade at any one time. In combat aircraft, the IAF contributed 11 fighter squad- rons and one squadron of Canberra bombers to the offensive in the east. The Indian Navy’s principal surface combatant, the aircraft carrier INS Vikrant, with her task force, would seal the coast and support inland operations with air strikes and naval gunfire.</p>

                        <p>The various Mukti Bahini groups multiplied the strength of India’s armed forces. Some of these groups had been formed from men with experience in the Pakistan Army, or with various Pakistani paramilitary organisations. During most of the rebellion, the Mukti Bahini guerrillas operated in geographically defined ‘sectors’. As open war approached, however, some of these troops were assembled into three brigade sized groupings called ‘forces’. Designated ‘Zulu’, ‘Kilo’ and ‘Sierra’, according to the initials of their commanders, they provided a total of eight infantry battalions (East Bengal Regiment) and three artillery batteries under the overall command of India’s 4 Corps. These ‘forces’ were capable of limited conventional operations in cooperation with the Indian Army, and were closely intermingled with Indian units during the actual fighting. Although some battalions were attached to Indian brigades individually, Sierra Force and Kilo Force essentially functioned as subordinate brigades under Indian divisions for much of the war. Other Bangladeshi groups constituted ‘Freedom Fighters’, who operated deep within East Pakistan as guerrillas and political cadres. Although the Freedom Fighters seldom inflicted heavy losses on the Pakistani regulars, they represented a ubiquitous menace, constantly harassing their opponents with ambushes, raids, sabotage and propaganda. Their activities exhausted the Pakistani troops while creating an enervating sense of constant uncertainty and danger. They also supplied Indian authorities with invaluable intelligence information. By November 1971, some 100,000 Bengalis could claim membership in these varied elements of the Mukti Bahini, and half of them were already operating inside East Pakistan.</p>
                        
                    </div>
                    <div class="war-details">
                        <h4>South-west Sector</h4>
                        <p>Cut up with innumerable water obstacles, the south-western corner of East Pakistan offered many advantages to the defender. The Pakistani force assigned to this sector, however, was inadequate to its mission. With only nine regular infantry battalions, the 9th Infantry Division could hardly cover its assigned terrain. Of its two brigades, 107th Brigade had to protect the immense frontier from Satkhira to Kotchandpur, while Brigade held the sector from the latter town north to the Padma. Moreover, 9th Division’s commander, Maj Gen MH Ansari, proved incapable of providing the determined, imaginative leadership necessary to compensate for his formation’s numerical inferiority. An additional formation, the ad hoc 314 Brigade of paramilitary troops, was initially formed at Khulna, but moved to Dacca by boat on approximately 7 December 1971.</p>

                        <p>On the Indian side, the newly raised 2 Corps consisted of the 4th Mountain Division and the 9th Infantry Division (the only infantry division in the east, it had more motor vehicles and heavier artillery than its mountain counterparts); the corps was later reinforced by 50 Para Brigade (minus one battalion). Under Lt Gen Tapishwar Narain (Tappy) Raina, the 20 infantry battalions of 2 Corps were to take Khulna, Jessore, Goalundo Ghat, Faridpur, and the Hardinge Bridge. Convinced that Khulna was one of the keys to East Pakistan, Gen Manekshaw placed special stress on its capture. Dacca was not mentioned, except in some contingency plans for crossing the Padma (Ganges) at Faridpur and Goalundo Ghat. Thus, paying little attention to possible operations against the chief city of East Pakistan, Raina planned to advance on two axes, with 4th Division in the north towards Jhenida and the 9th Division aiming for Jessore on the southern approach. Operations by Indian troops and Mukti Bahini in late November had secured a sizable enclave between Bayra and Jessore. An Indian success at Garibpur on 21/22 November 1971 was particularly significant, as it allowed 9 Division to gain considerable ground towards Jessore and resulted in the virtual destruction of the lone Pakistani armoured squadron in the area.</p>



                        <p>The action at Garibpur, however, also alerted the Pakistanis to the 9th Division’s proposed line of advance. As a result, once the full-scale conflict broke out, the division quickly became embroiled in a tough and costly slogging match on 4 and 5 December 1971. This fight took its toll on the Pakistanis too, and the exhausted 107 Brigade abandoned Jessore on the night of 6/7 December, withdrawing south to Khulna in considerable confusion. The Pakistani division HQ and other remnants fled east towards the Madhumati River. A Pakistani officer recalled: ‘The front here had crumbled completely...Withdrawal quickly turned into a rout.’ Riding into Jessore in the dawn hours of 7 December, he noted, ‘It looked like a ghost town, except for sleepy dogs and chickens, not a soul stirred. Doors were wide open, all kinds of personal belongings littered the roads...It looked like the end of East Pakistan.’ The Indians occupied Jessore later that day, but Maj Gen Dalbir Singh, GOC 9th Division, allowed himself to be distracted by Khulna and turned his entire division towards an objective that was supposed to be taken by a brigade. The town held out stoutly for the remainder of the war in the face of repeated attacks.</p>


                        <p>The Indian Navy was also involved in the fighting around Khulna. A small task group of one Indian Navy patrol boat and two Mukti Bahini boats, called ‘Force Alpha’, steamed towards Khulna from Chalna on 9/10 December 1971. Pakistani fire and tragically accurate air strikes by IAF fighters disabled two of the boats and forced the other to withdraw without contributing much to the advance. The 9th Division’s reserve force, 50 Para Brigade, engaged in a brief skirmish at Khajura, north of Jessore, on 8 December, before being pulled out the next day for transfer to the western front. A planned two-company airborne attack by 8 Para near Jhenida was called off as being unnecessary. The ‘Red Eagles’, Indian 4 Division, launched a well-conducted attack north and east from its positions around Jibannagar, skillfully bypassing or overwhelming resistance to enter Jhenida on 7 December.</p>

                        <p>Like the 9th Division, however, the leadership of 4th Division was distracted by a flank objective. In this case, when a hasty attempt to capture Kushtia and the Hardinge Bridge miscarried, the senior commanders overreacted and diverted the entire division to the north. Although the Indian advance helped urge Pakistan’s 57 Brigade in its retreat across the Ganges, by the time 4th Division had returned to the Magura area (14 December), it was too late to participate in the drive for Dacca. The division made a fine crossing of the Madhumati (albeit against light resistance) and took the surrender of the broken remnants of the Pakistani 9th Division at Faridpur on 16 December 1971. Indian Army and BSF troops from the Bengal Area, under Maj Gen P Chowdry, made limited gains on the Satkhira axis.</p>
                    </div>

                    
                    <div class="war-details">
                        <h4>North-west Sector</h4>
                        <p>Somewhat drier than the other operational areas in East Pakistan, the north-west sector offered better terrain for mechanised forces and the possibility of fairly rapid movement. Consequently, both sides allotted the bulk of their armoured units to this sector (the Pakistani 29 Cavalry; Indian 63rd and 69th Armoured Regiments, as well as 20 Maratha Light Infantry in SKOT armoured personnel carriers) and tanks played an important role in the success of India’s 340 Mountain Brigade. Pakistan’s 16th Division had the task to defend the north-west, but its commander, Maj Gen Nazir Hussain Shah, dissipated his strength by creating ad hoc commands and haphazardly mixing troops from various units. The result was a reduction in cohesion and morale.</p>

                        <p>On the Indian side, 33 Corps, under Lt Gen Mohan L Thapan, controlled the 6th and 20th Mountain Divisions and the 71st Mountain Brigade. However, while fighting the war to the south, the corps also had to look north and retained command of the 17th and 27th Mountain Divisions on the Tibetan frontier. Furthermore, Thapan could not commit 6th Mountain Division without permission from New Delhi as it was to be held ready to move to the Bhutanese border in case China intervened in the war. As elsewhere along the border, Indian forces in support of the Mukti Bahini made significant inroads into East Pakistan prior to 3 December. Most notable was Brig Pran Nath Kathpalia’s 71st Mountain Brigade, which had pushed to the outskirts of Thakurgaon by the eve of war. Efforts to capture the heavily fortified border village of Hilli, however, failed repeatedly in a struggle that raged off and on from 24 November to 11 December 1971. Resolutely defended by Pakistani 4 Frontier Force, Hilli blocked the proposed advance of 20th Division across the narrow ‘waist’ of this sector.</p>

                        

                        <p>After suffering heavy losses in front of Hilli, the Indian division solved this problem by swinging around to the north and unleashing 340th Brigade under Brig Joginder Singh Bakshi. Bakshi moved swiftly to control the main north–south road, unhinging the defence of Hilli, splitting Pakistani 16 Division and opening the way to Bogra, a town he effectively controlled by the war’s end. The Pakistani division, despite continued resistance by isolated units, had ceased to exist as a coherent combat formation. Indicative of the chaotic situation, Gen Shah and the commander of 205 Brigade, Brig Tajammul Hussain Malik, were almost captured when Indian forces ambushed their convoy on 7 December 1971.</p>

                        <p>On the other hand, a last-minute Indian move north by the 66th and 202nd Brigades to capture Rangpur proved unsuccessful. In secondary actions, 9 Mountain Brigade secured most of the area north of the Tista River and an ad hoc command of Indian BSF and Mukti Bahini, under Brig Prem Singh, pushed out of Malda to capture Nawabganj in the extreme south-eastern corner of the sector. Despite Bakshi’s performance and the generally successful advance of 71 Brigade, much of 33 Corps’ offensive power was allowed to lie idle far too long and Pakistani troops still held the major towns of the sector—Rangpur, Saidpur, Dinajpur, Nator, Rajshahi—when the ceasefire was announced. Likewise, the ceasefire intervened before the Indians could implement a hastily conceived plan to transfer 340 Brigade, a tank squadron and an artillery battery across the Jamuna via the Phulchari ferry to take part in the advance on Dacca. With the exception of this squadron, all armour was preparing to transfer to the west by the end of the war.</p>

                    </div>


                    <div class="war-details">
                        <h4>Northern and Eastern Sectors</h4>
                        <p>The crucial Indian offensive actions were those undertaken in the northern and eastern sectors. As elsewhere, the innumerable water obstacles and poor infrastructure here presented daunting challenges to manoeuvre and logistics, but the 101 Communications Zone in the north and 4 Corps in the east overcame these hurdles to cut through the defenders and isolate Dacca, leaving Niazi little choice but surrender. The combat in these two sectors generally falls into two phases. In the first phase, from 3 December to 9 December 1971, Indian and Bangladeshi troops broke through the crust of the Pakistani defences in a series of border battles and established themselves on the banks of the major rivers in each area.</p>

                        <p>The second phase, from 10 December to 16 December 1971, was a race to Dacca against increasingly fragmented and dispirited resistance, with the lead Indian elements finally coming to a halt within miles of the city just before the ceasefire. In the north, India’s 101 Communications Zone, an administrative HQ forced into service as a combat command, spent three weeks—from 14 November to 4 December—battering itself against the stubborn resistance of a small Pakistani border post at Kamalpur. When the outnumbered band of Pakistani defenders (from 31 Baloch) finally ran out of food and ammunition, the road to the south was open and 95 Brigade pushed on to Jamalpur. The reduction of this well-defended town consumed several more days, but by the 11 December 1971, it was in Indian hands; and 31 Baloch, half of the regular Pakistani force in this sector, was no longer an effective fighting force. The same day, Indian troops of ‘FJ Sector’ entered Mymensingh unopposed. The stage was set for the drive on Dacca. In the east, Indian 4 Corps, with all eight East Bengal Regiment battalions, had made small gains in the last weeks of November. An enclave south of Akaura served as a springboard for the 57th Division, which advanced along the rail line to Ashuganj. In see-saw fighting that featured several successful Pakistani counter-attacks, 27 Brigade of the Pakistani 14th Division fell back across the Meghna and destroyed part of the rail bridge, blocking immediate passage of the river. Lt Gen Sagat Singh, however, saw Dacca as ‘the final answer’ and decided ‘to go beyond [his] assigned task’. In an impressive display of improvisation, 4 Corps began crossing the broad Meghna on 9 December 1971, in a hastily assembled helicopter lift operation, supplemented by every variety of local water craft. Pakistan’s 14th Division was no longer a hindrance as its 27 Brigade had retired to Bhairab Bazar and its other two brigades (202 and 313) were isolated at Sylhet. Further south, the Indian 23rd Division also reached the Meghna on 9 December, seizing both Daudkandi and Chandpur against light resistance. The isolated Pakistani force at Laksham capitulated the same day, leaving only the garrison of Mayanmati to offer organised resistance east of the Meghna. Pakistan 39 Division had disintegrated.</p>

                        <p>A sense of imminent victory drove the Indians and as the 57th Division painfully built up its strength west of the river, 23th Division—having shed the 83rd Brigade and ‘Kilo Force’ to push towards Chittagong—prepared to make its own improvised crossing. In another colourful, tenuous helicopter and boat operation, 301st Brigade landed at Baidya Bazar on 14 December 1971 and closed on the Lakhya the following day. ‘Like the Mughal army of yore,’ recalled the brigade commander, ‘we marched northwards helped and surrounded by civilians.’ India’s 57th Division was also advancing: 311 Brigade and ‘Sierra Force’ were threatening Demra; and 73rd Brigade had reached the Balu, east of Tungi. Lt Gen Sagat Singh’s decision to ‘go beyond his assigned task’ had paid off. The Indians were also approaching Dacca from the north-west, hindered more by severe logistics constraints than by the near non-existent Pakistani opposition. Indeed, the Pakistan Army’s 93rd Brigade, over the protest of its commander, had been withdrawn towards Dacca in a desperate attempt to shield the unprotected capital against the Indian troops advancing rapidly from the east and the north-east.</p>

                        <p>The Indian airborne drop of 2 PARA at Tangail on 11 December 1971 accentuated the menace to Dacca. Although 2 Para’s appearance made only a marginal contribution to the tactical battle, it helped to unnerve Niazi and others in Eastern Command HQ, already anxious because of the lack of regular combat troops in the capital. Predations of the local Mukti Bahini under Qadir ‘Tiger’ Siddiqi com- pounded Pakistan’s woes, disrupting movements and depleting morale. The Indian paratroopers joined hands with the 95th Brigade on 12 December 1971 and, with the 167th Brigade hastening up from Jamalpur, soon reached and crossed the Turag. By 15 December, the Pakistani situation around Dacca was hopeless: the lone brigade of the 36th Division was broken; the newly arrived 314 Brigade was little more than a paper organisation; the 14th Division was sitting demoralised and useless at Bhairab Bazar; and the 39th Division had ceased to exist. On the morning of 16 December 1971, Maj Gen Mohammed Jamshed Khan drove out of Dacca to arrange the ceasefire.</p>

                       
                    </div>

                    <div class="war-details">
                        <h4>Sylhet Sector </h4>
                        <p>The Sylhet area, surrounded on three sides by Indian territory, was painfully vulnerable to attack. With only two weak brigades (202 and 313 of 14th Division) to cover the long, twisting border, the prospects for a successful defence were dim. As in other areas, there were not enough troops for the task at hand: ‘motley detachments of East Pakistan Rifles, Mujahids, Razakars, and Civil Armed Forces (from West Pakistan) were thrown in to support totally inadequate regular Pakistan Army units to fight in penny packets over vastly extended distances.’ The newly raised 202 Brigade, for example, had to cover some 61 miles of the frontier with only one regular battalion (31 Punjab), some paramilitary troops and five guns. India’s 8th Mountain Division and the three Bangladesh battalions of ‘Z Force’ exacerbated the Pakistani defensive problem by leading the Pakistani Generals to believe that the principal advance would come from the north and east between Jaintiapur and Karimganj. As a result, most of 202 Brigade was deployed along the arc between these two localities and the two regular battalions of Pakistani 313 Brigade were in no position to repel 8 Division’s two-brigade attack when the Indians struck from an unexpected direction.</p>

                        <p>Despite a costly setback at Dhalai (also known as Dhullai), the Indians had secured significant terrain before 3 December 1971, especially in the Karimganj salient and between Shamshernagar and Kalaura. The Indian 59th Mountain Brigade, which had cleared the Karimganj area, shifted south to Dharmanagar and constituted the northern arm of the 8th Mountain Division attack, while the 81st Mountain Brigade pushed from Shamshernagar towards Maulvi Bazar on the southern axis. Pakistan’s 313 Brigade offered spirited resistance but, badly reduced by the earlier fighting, it could not hold on for long. By 7 December 1971, Indian troops in the south were advancing on Fenchuganj and had reached the defences outside Maulvi Bazar. Operations seemed to be progressing satisfactorily and the corps commander, having learned that Sylhet was weakly held and fearing that Pakistani troops would slip south to reinforce the defences around Dacca, jumped 4/5 Gorkha Rifles to Sylhet by heli- copter. Expecting that ‘Echo Sector’ would effect a quick link up with the Gorkhas, he simultaneously withdrew the rest of the 59th Brigade to Kailashahar to prepare to reinforce 57 Mountain Division. The Gorkha battalion established a foothold against near non-existent resistance, but Brig M Wadke’s ‘Echo Sector’ was held up just south of Jaintiapur and Lt Gen Sagat Singh had to send the 59th Brigade hurrying back up to Fenchuganj. The Gorkhas, thus, found themselves in trouble.</p>

                        <p>By 14 December 1971, however, the brigade had reached the southern side of Sylhet and established contact with the Gorkhas. The same day, 81st Brigade troops and 9 Guards of 59 Brigade captured Sylhet railroad station from the south-west. From the north-east, 5/5 Gorkha Rifles were within two miles of Sylhet by 15 December, along the Jaintiapur road. The town was also under pressure by 1 East Bengal Rifles commanded by Maj Ziaur Rahman, a future President of Bangladesh. Finally, 87 BSF was approaching from the east. While the Indians were advancing, the Pakistanis suffered from command problems. Through a series of confused decisions, 313 Brigade withdrew towards Sylhet to be uselessly encircled, instead of withdrawing towards Ashuganj where it might have helped slow the Indian advance.</p>

                        <p>As a result, both Pakistani brigades remained trapped in this remote town until they surrendered on 16 December 1971.</p>
                    </div>


                    <div class="war-details">
                        <h4>Chittagong Sector</h4>
                        <p>This remote sector was remarkable for two reasons: the extensive operations by India’s Special Frontier Force (SFF); and for the only amphibious operation of the war. Although Gen Manekshaw initially placed great emphasis on Chittagong as a possible ingress port for Pakistani reinforcements or escape route for a Pakistani evacuation, the only Indian conventional unit dedicated to this area in early planning was ‘Kilo Force’, a mix of Indian regular and paramilitary troops supported by Mukti Bahini. This force captured Feni and pushed down the coast towards Chittagong, reaching Sitakund by 12 December. Here, it was joined by 83 Brigade which had been diverted south from 23rd Division after the fall of Laksham. The two brigades had advanced as far as Faujdahat by 15 December 1971 and accepted the surrender of the Chittagong garrison the next day.</p>

                        <p>The Pakistani 91st and 97th Brigades played almost no substantial role in the fighting. Operating from four bases in India—Marpara, Demagiri, Bornapansuri and Jarulchari—the SFF conducted successful offensive operations in the central area of the Chittagong Hill Tracts against Pakistani troops and anti-Indian Mizo tribal guerrillas supported by Pakistan. Its most spectacular accomplishments were the destruction of the Dohazari Bridge over the Sangu River and the capture of Rangamati. Thoroughly pleased with this little victory, the SFF commander, Brig SS Uban, transferred his HQ from Demagiri to Rangamati. Though peripheral to the battles taking place further north, Uban and his men displayed determination and resourcefulness in achieving considerable success in a challenging campaign with minimal logistical support. Uban’s men attacked the Dohazari Bridge because Manekshaw was apprehensive that Pakistani troops would escape south into Burma along the old ‘Arakan Road’. </p>


                        <p>This concern also motivated Manekshaw to order an amphibious assault south of Cox’s Bazar towards the end of the war under a plan named Operation Beaver. With almost no planning or reconnaissance, 1/3 Gorkha Rifles and some other troops from Bengal Area Command were loaded aboard two navy landing craft as ‘Romeo Force’ and dispatched across the Bay of Bengal. Predictably, the attempt to land on 14 December failed. A handful of Gorkhas got ashore, but two men drowned when they disembarked in water that was around 7–9 feet deep. Nonetheless, a platoon eventually landed and sent out a patrol that made its way north to Cox’s Bazar and confirmed that there were no Pakistanis in the vicinity. The ceasefire came into effect before the bulk of the men could get ashore, which they did at Cox’s Bazar from 16 December to 18 December 1971 using local lighters.</p>
                    </div>
                </div>

                <div id="western" class="accordion-details">
                    <h3>The Western Front</h3>
                    <div class="war-details">
                       
                        <p>The long western frontier was far more varied than the border in the east. Both ends of the line were desolate. In the extreme north, the CFL in Kashmir traversed a barren, nearly uninhabited land of snow-covered peaks stretching to 23,000 feet in altitude, before shifting to cold, rugged and heavily forested mountains further south. Altitudes gradually decreased along the southern reaches of the CFL, but the terrain remained broken and difficult until the final foothills disappeared into the lush plains of the Punjab. The Punjab, densely populated and cultivated on both sides of this border, had a relatively robust transportation infrastructure and offered some opportunities for mechanised forces. However, its cities and villages could serve as ready-made strong points for a defender and its numerous watercourses, large and small, represented significant impediments to rapid movement. The land grew increasingly arid and the infrastructure less sturdy south of the Punjab as the border passed through the great Thar Desert and ended in seasonal salt marshes barely above sea level at the southern extreme.</p>

                       
                    </div>



                    <div class="war-details">
                        <h4>Pakistani Forces and Plans </h4>
                        <p>Adhering to its grand strategy of defending the east by offensive action in the west, Pakistan retained the bulk of its forces on this lengthy and often difficult frontier. As compared with three underequipped regular divisions and two ad hoc ‘hoax’ divisions in the east, the army held 10 infantry divisions in the west, along with both armoured divisions and three independent armoured brigades. Unlike the east, the tank fleet in the west included relatively modern types, such as American M-48s and Chinese T-59s, along with a number of slightly older but still serviceable American M-47s and some World War II-era models—American M-4 tanks and M-36 tank destroyers, as well as one regiment of Soviet T-34s. A large body of paramilitary and militia units (Razakars) supported the regular army but, as in the east, many of the militiamen proved unreliable. Cumbersome command and control arrangements within the army hampered effective utilisation of these large forces. Although there were three corps HQ to serve as an intermediate level of command between General HQ and the combat manoeuvre divisions, at least three divisions and possibly some separate brigades came under direct control of Army HQ in Rawalpindi.</p>

                        <p>Poor cooperation among the three services compounded the Pakistan Army’s problems. The PAF in the west totalled 12 squadrons. Of these, half were sturdy but outdated F-86/Sabre VI fighters and the remainder were a mix of Chinese, French and American airframes: F-6s, Mirage IIIs, F-104s and B-57s. The F-104 and B-57 squadrons, however, stood at about half of their authorised strength. In addition, Pakistan would use some of its American C-130 transports as bombers during the course of the conflict. All of the Pakistan Navy’s major combatants were located in the west and were based out of the Karachi area. These included a cruiser, five destroyers and three frigates. Also available were two gunboats, seven minesweepers and an oiler. Most important, however, were the three submarines left in the west after PNS Ghazi had sailed for the Bay of Bengal.</p>

                        <p>Pakistan’s plan in the west called for the beginning of offensive operations five or six days after an Indian attack in the east. These, however, were ‘preliminary operations’, essentially distractions, designed ‘to fix the enemy and to divert his attention’ away from the intended site of the main attack. They would thus come in the north, with advances on Poonch and Chhamb. A small probe south-east of Shakargarh by 1 Corps was included to provoke a reaction by Indian armoured reserves. The aggressive impression created by these drives would be reinforced by moving the 7th Infantry Division, a key reserve formation, from its cantonments near Peshawar eastwards to assembly areas, from which it could support either of the northern attacks. In the meantime, the true offensive was to be prepared by 2 Corps. With one armoured and two infantry divisions (including the rapidly shifted 7 Division), 2 Corps would strike into India from the Bahawalnagar area approximately three days after the secondary attacks (and thus some eight or nine days after the Indian invasion of the east). It was hoped that most of India’s armoured reserves would have become entangled in Pakistan’s defences in the Shakargarh salient during this three-day interval between the diversionary attacks and the main effort.</p>

                        <p>The PAF was apprised of the army’s plan and may have kept as many as four squadrons in reserve to support the grand offensive. In central and southern Punjab, Pakistan’s 4 Corps was to conduct several small operations to seize vulnerable enclaves along the twisting border. In the far south, the army’s Chief of Staff, ‘somewhat peremptorily and without due process of staff study’, instructed 18 Division to launch a push towards Ramgarh on the road to Jaisalmer. This would lead to a minor disaster when the war began.</p>

                        
                    </div>



                    <div class="war-details">
                        <h4>Indian Forces and Plans </h4>

                        <p>Despite its concentration on the eastern front and its nagging worries about the Chinese border, India’s forces in the west still outnumbered Pakistan’s in almost every category. In addition to its lone armoured division, India had 13 infantry divisions, four independent armoured brigades, and three unattached infantry brigades on its western border with Pakistan. A host of Territorial Army (reserves) and paramilitary BSF battalions provided critical support both on the border and in the rear areas. Additional ground units were already being transferred from the east to the west when the war ended. The Indian armoured arm was even more variegated than Pakistan’s with British (Centurions), French (AMX-13s), Russian (T-54/55s and PT-76s) and domestic (Vijayantas) models, complicating ammunition, repair parts and training requirements. The Indian Army command structure, however, was superior to that of its adversary. Joint service cooperation, though by no means ideal, was better on the Indian side, and the army’s Western and Southern Commands represented a well-understood echelon of command between the corps HQ and the Chief of the Army Staff in New Delhi.</p>

                        <p>The IAF enjoyed a considerable numerical advantage in the west with some 24 combat squadrons on hand when all-out war began on 3 December 1971. Nearly half of these squadrons were composed of relatively modern Soviet aircraft (Su-7s and MiG-21s), with the remainder being a mixture of compact Gnats, British Hawker Hunters, indigenous HF-24 Maruts, aging French Mysteres and Canberra bombers. This formidable force was quickly reinforced by units from the east as the war there turned increasingly in India’s favour. Beyond these fighters and bombers, India, like Pakistan, used some transports (a squadron of An-12s) in the bomber role during the war. The Indian Navy presence in the Arabian Sea included a cruiser, a destroyer, eight frigates, two patrol vessels and two submarines. There were also eight Osa-class patrol boats that would play a significant role in the conflict.</p>

                        <p>India’s strategy in the west was primarily defensive but, as with Pakistan, Indian planning incorporated a number of small strikes to seize vulnerable salients along the CFL and the border. Two larger offensives were also planned. One was to drive into the Shakargarh salient to unhinge any Pakistani attempt to thrust north between Jammu and Pathankot; an attack in the Chhamb/Sialkot area would support this effort. The other major advance was to take place in the desert along the rail line towards Naya Chor. A smaller push in the direction of Rahimyar Khan may also have been considered, but India never made ‘the long and careful preparations required’ to cut Pakistan in half along its narrow line of communications between Karachi and Lahore. In addition, the army staff developed a contingency plan to launch 1 Armoured Division across the border if Pakistan’s reserves were committed to the Shakargarh area. The lack of reliable intelligence concerning the locations of Pakistan’s reserve formations, especially the two armoured divisions and the 7th Infantry Division, hampered Indian planning prior to the conflict and introduced a measure of caution during the war.</p>

                        <p>As with the army, the primary missions of the air force and the navy were defensive in nature. Both services, however, also included major offensive tasks in their portfolios. For the IAF, ‘support to the Army and Navy, including gaining and maintaining a favorable air situation over the tactical area’ was the top priority after defence of the homeland, but air force commanders also planned a counter-air campaign against Pakistani bases and radars. The navy was responsible for defending the country’s long western and southern coastline, but was to act offensively as quickly as possible either to lock the Pakistan Navy in Karachi or to draw it out and destroy it. Additionally, the navy was tasked with interdicting Pakistani commercial shipping.</p>

                        
                    </div>



                    <div class="war-details">
                        <h4>Turtok Sector</h4>

                        <p>The Turtok area features some of the most difficult terrain in the world; with arid, rocky slopes climbing steeply up to heights of 18,000–23,000 ft, it is almost completely devoid of vegetation. Even the Shyok River valley rests at approximately 9,000 ft. Sub-zero winter temperatures and a near-total absence of motorable tracks exacerbated the effects of the altitude, making military operations extraordinarily challenging. Pakistan defended this desolate northern end of the Kashmir CFL (loosely from Kel to Turtok) with six battalions (called ‘wings’) of paramilitary troops belonging to the Frontier Corps. These units reported to the Director General of the Frontier Corps in Gilgit, who, in turn, reported directly to Army HQ. The Turtok sector itself was held by two or three companies of Karakoram and Gilgit Scouts recruited from mountain peoples on the Pakistan side of the CFL.</p>

                        <p>The Indian troops in the area consisted of three companies of semi-regular Ladakh Scouts and 500 hastily raised local militia called Nubra Guards. In both cases, the Indian troops were hardy men, locals who were at least somewhat inured to its extreme conditions. They operated under the command of the 3rd Infantry Division with its HQ at Leh; the division was also responsible for the boundary with China to the east. The Ladakh Scouts were well-trained and well-led, but the ‘Nubra Guards’ had only received two weeks of instruction in basic weapons handling when the war began. The Pakistani Scouts, on the other hand, were not as well prepared as their foes in training or equipment.</p>

                        <p>The Commandant of the Ladakh Scouts was a local officer, Maj Chowang Rinchen, who had won India’s second highest combat gallantry award in the first Indo-Pak conflict in Kashmir in 1948, but had become infamous for being ‘in constant trouble because he found military discipline irksome’. He was an energetic and resourceful commander who knew his men and the terrain. Under Rinchen’s leadership, the Indian forces attacked on the night of 8 December and quickly captured several Pakistani posts. One of the skirmishes centred on possession of a position at nearly 20,000 ft. Rinchen rapidly exploited his success, frequently using night attacks. By the time the ceasefire was announced, his men had overcome terrain, climate and enemy resistance to reach Thang. Logistical problems precluded further advances, but Rinchen’s superiors had every reason to be pleased with his performance under appalling conditions. Indian losses illustrate the physical challenges of the region: although only three men were wounded by enemy action, Rinchen’s force suffered 45 cases of frostbite during this two-week period.</p>
                        
                    </div>



                    <div class="war-details">
                        <h4>Kargil Sector</h4>

                        <p>As with the Turtok sector, the high-altitude ‘moonscape’ around Kargil presented both sides with enormous challenges. In this sector, India had a specific and important military objective: to clear Pakistani troops from the heights that dominated the Dras–Kargil–Leh road. This road, which was open only from May to September, served as the main supply line for the civilian population as well as the military garrison in remote Ladakh. (It continues to perform this function today, hence the Indian sensitivity to the Pakistani incursion in 1999.) Pushing across the ‘windswept and inhospitable heights’ against several companies of Karakoram Scouts, five battalions of Brig ML Whig’s 121 Independent Infantry Brigade Group successfully secured about 110 sq km north of Kargil between 7 and 17 December. However, the brigade failed to reach its principal objective, the tiny village of Olthingthang near the confluence of the Shingo and Indus Rivers. Contending with fierce weather as well as the enemy, the Indians losses were as follows: 55 killed; 195 wounded; and ‘an inordinately high toll’ of 517 cases of frostbite, more than 300 of these were from a single battalion. India claimed Pakistani casualties of 114 killed and 32 prisoners of war.</p>

                                                
                    </div>



                    <div class="war-details">
                        <h4>Tangdhar and Uri Sectors </h4>

                        <p>The forbidding peaks of the Zanskar Range block precipitation and make Ladakh a high desert, but to the south and west of these crags Kashmir is green and lush from heavy annual rainfall. Though not so high as Ladakh, the thickly wooded slopes of Kashmir are rugged and pose a significant obstacle to military movement. Furthermore, in 1971, neither side planned major offensive operations in the Tangdhar and Uri sectors. Pakistan’s 12th Division, with six infantry brigades, was responsible for almost the entire north–south face of Kashmir, a distance of some 186 miles. Although twice as large as a normal division, the men were spread thinly on the ground to cover this enormous zone. The division commander, Maj Gen Akbar Khan, had to economise even more to scrape together enough men for a strike towards Poonch further south. India’s 19th Division was not as desperately short on manpower, but uncertainty regarding the location of Pakistan’s 7th Infantry Division made Indian commanders chary of committing their own reserves. As a result, this area only witnessed a few small Indian advances as the 19th Division attempted to secure several tactically advantageous features.</p>


                        <p>Despite snow, rough terrain and Pakistani defences, the Indian 104th Brigade made tactical gains on the southern front of the Tangdhar salient, especially in the area of the Tutmari Galli where 9 Sikh performed well. This Indian pressure forced Maj Gen Akbar Khan to transfer a battalion from the Poonch area to the Tangdhar/ Lipa sector on 9 December, thus reducing the capacity of the 12th Division to conduct offensive operations. A second battalion was detached from 12 Division’s reserve on 16 December to reinforce the defenders. A tiny enclave in the Tutmari Galli area was still in Pakistani hands when the ceasefire was announced. This situation occasioned renewed skirmishing in May 1972, when Indian troops launched an abortive attempt to capture it before the final peace agreement was signed. The Indian 161st Brigade’s efforts to seize two small areas on the southern flank of the Uri salient failed to achieve their objectives. Pakistani defenders repulsed 8 Sikh and 7 Sikh Light Infantry on the night of 4/5 December 1971 and action settled to desultory skirmishing for the remainder of the war. Minor skirmishes continued in this sector for several days even after the ceasefire. North-east of Tangdhar, Indian paramilitary BSF troops gained some terrain (Northern Gallies sector), pushing Pakistani paramilitary outposts back towards the Kishanganga (or Neelam) River.</p>

                        
                    </div>


                    <div class="war-details">
                        <h4>Punch Sector</h4>

                        <p>The region in and around the town of Poonch is also rugged and heavily forested. Pakistan included a thrust against Poonch in its overall offensive plan, in part to distract Indian attention from the main attack to be launched by 2 Corps, and in part to exploit the apparent vulnerability of the Poonch salient. India, however, noticed a Pakistani build-up opposite the salient during November and moved the 33rd Brigade of 39 Division into the sector to support 25 Division’s 93rd Brigade. Strong Indian reserves were thus already on hand and prepared when the war broke out. On 3 December 1971, Pakistan’s 12 Division took advantage of the terrain and vegetation to infiltrate two battalions (9 and 16 Azad Kashmir) as part of a two-brigade attack across the northern face of the Poonch salient.</p>

                        <p>Although the two battalions succeeded in threading their way past the Indian outpost line, the Indian defences along the CFL held firm and reinforcements from 33 Brigade soon counter-attacked, forcing the infiltrators to withdraw. A cohesive defence, well-directed at the brigade level and enjoying excellent artillery support, succeeded in repelling the attackers, despite the danger posed to resupply and morale by the two infiltrating Pakistani battalions. By 7 December, the Pakistani troops had returned to their side of the CFL. Heavy casualties among the attacking Pakistani troops, failure to capture a significant portion of the outer Indian defences and the Indian advances further north in the Lipa area were the major factors in convincing the 12 Division commander to call off the Poonch offensive. India struck back on the night of 10/11 December 1971, taking a promontory above the Poonch River opposite Kahuta in a well-conducted assault by 21 Punjab and 9 Rajputana Rifles. Further south at Daruchian, however, a similar night attack by 14 Grenadiers was thrown back, with heavy losses, on 13/14 December (158 Indian casualties).</p>

                    </div>



                    <div class="war-details">
                        <h4>Chhamb and Jammu Sectors </h4>

                        <p>Pakistan made its most significant advances in the Chhamb sector, revisiting a bat- tlefield from the 1965 war to threaten one of India’s key overland links to Kashmir and to deprive India of a launching pad for potential offensive operations. Pressing forward on the night of 3/4 December 1971, the Pakistani 23rd Division, reinforced with the artillery assets of 17 Division and three additional manoeuvre brigades, pushed back the forward brigade (the 191st Brigade under Brig RK Jasbir Singh) of India’s 10th Division.</p>

                        <p>The Indian forces, initially postured to assume the offensive, had been hastily realigned for a defensive mission after the Indian Army’s senior leaders suddenly changed their plans at a meeting on the evening of 1 December 1971. The division was thus ill prepared when the Pakistanis attacked. After three days of heavy fighting which saw, among other things, the capture of two batteries of 216 Medium Regiment by infiltrating Pakistani infantry, the 191st Brigade withdrew behind the river on 6 December. The tenacious defence by 5 Sikh was key in delaying the Pakistani advance. The Indian 68th Brigade hastily took up the defence of the Munnawar Tawi line as the broken 191 Brigade pulled back to the west. Fortunately for the Indians, 23 Division’s mission was only to secure the line of the river, so the Pakistanis did not seize the opportunity to pursue during this moment of Indian vulnerability. On the division commander’s initiative, however, Pakistan’s 23rd Division soon returned to the attack and, by the morning of 10 December, had established a shallow bridgehead across the Manawar Tawi. This proved to be the limit of the 23rd Division’s advance. The accidental death of the energetic division commander in a helicopter crash on 9 December robbed the attack of its impetus and the lodgement on the Indian side of the river was evacuated during the following day (11 December). The sector remained quiet for the remainder of the war and Pakistan retained the area west of the Munnawar Tawi in the peace settlement.</p>

                        <p>South-east of Chhamb, Indian 26th Division scored a success by capturing a narrow salient known as ‘the Chicken’s Neck’ (or Phuklian salient to the Pakistanis) in a quick two-day operation against light resistance (6–7 December 1971). The division also evicted small Pakistani groups that had occupied two Indian border posts. To the frustration of its aggressive commander, Maj Gen ZC (Zoru) Bakshi, the 26th Division was restricted to these minor offensive actions and a pre-war plan to push for Sialkot was cancelled: ‘After the Pakistani thrust at Chhamb, headquarters was in a flap and they took away one of my brigades and told me not to attack.’ Bakshi’s missing brigade (the 168th) ended up with a defensive mission under an ad hoc HQ called ‘X-ray Sector’, with 323 Brigade of 39 Division. ‘X-ray Sector’, thus, became the right flank unit of 1 Corps, protecting the area from the Degh Nadi to the left flank of the 26th Division.</p>

                        
                    </div>



                    <div class="war-details">
                        <h4>Shakargarh Salient</h4>

                        <p>India planned its major western front offensive action for the Shakargarh Bulge area. With three infantry divisions and two armoured brigades, Indian 1 Corps was to push into the salient from the north and east to shield vulnerable parts of India, tie down Pakistani mechanised forces and seize as much territory as possible with an eye towards peace negotiations. Pakistan’s 1 Corps, charged with the defence of the Shakargarh Bulge, had the 15th Division on the left around Sialkot, the 8th Division on the right east of the Degh Nadi and the 8th Armoured Brigade in support of both forward divisions. Also belonging to 1 Corps but held in the rear was Pakistan’s so-called ‘Army Reserve North’, consisting of the 6th Armoured Division and the 17th Infantry Division.</p>

                        <p>Opening on 5 December 1971, the Indian attack fell on Pakistan’s 8th Division but quickly ran into trouble. In the view of many Indian commentators, poor coordination and excessive caution retarded progress, so that the most successful Indian formation (the 54th Division) only managed a small advance of approximately 8 miles in two weeks of operations. Moreover, the Indians spent most of these two weeks negotiating minefields and nudging back the 8 Division’s covering troops (a capably commanded ad hoc grouping called ‘Changez Force’) and only ran up against the main defences as the war was ending. On the other hand, repeated counter-attacks conducted by Pakistan’s 8 Independent Armoured Brigade near Jarpal and Barapind, north-east of Zafarwal, from 15 December to 17 December, were repulsed with heavy losses by Indian 54 Division (notably India’s Poona Horse tank regiment).</p>

                        <p>In a fruitless assault in the dawn hours of 17 December 1971, the last day of the war, 35 Frontier Force alone lost 57 men, with 73 wounded. Pakistani tank losses during the two-day struggle were also high: 40–50 tanks from the two attacking regiments (13 Lancers and 31 Cavalry) of 8 Armoured Brigade. The failure and cost of these assaults led one Pakistani General to comment that ‘the few counter- attacks which 8 Division tried during the war were most noticeable by their lack of planning.’ In the end, India occupied a substantial piece of Pakistani territory, but failed to penetrate its main defences or engage its mechanised reserves. These Pakistani reserves, however, remained well back from the front line and played only a marginal role in the conflict: while the 6th Armoured Division waited for orders near Pasrur, 17 Division found itself reduced to little more than a lone manoeuvre brigade as major detachments were sent off to the 23rd Division on the left and 4 Corps on the right.</p>

                        
                    </div>





                    <div class="war-details">
                        <h4>Northern Punjab Sector</h4>

                        <p>Unlike 1965 and despite a heavy concentration of forces on both sides, this part of Punjab did not see major action by either army. On the Pakistani side, 4 Corps was to stay on the defensive, attempting to cover an immense sector with inadequate resources until ordered to support the main offensive. When Army HQ decided to launch this main offensive, 4 Corps was to attack towards Ajnala in the north as a feint, while seizing Hussainiwala, cutting the road between Ferozepur and Jalalabad, capturing Sulaimanke and establishing a lodgement on the Indian side of the border, south of Bahawalnagar, for 2 Corps to exploit. Given the paucity of forces assigned to 4 Corps, the feasibility of these manifold operations must be questioned. In the event, little happened in this sector. </p>

                        <p>In northern Punjab between Amritsar and Lahore, India’s reinforced 15 Division of 11 Corps captured Pakistani enclaves along the Ravi River at Dera Baba Nanak (usually called Jassar in Pakistani accounts) and Burj. Pakistan occupied an Indian enclave at Kassowal (also known as the Dharam enclave), south-west of Dera Baba Nanak, and made shallow inroads at several points opposite Lahore, most notably near Khalra in the Indian 7th Division zone. A number of minor border posts were also exchanged along the Ravi, but neither side engaged in anything beyond these local offensive operations. On 10 December, Pakistani 4 Corps deposited a quantity of unserviceable bridging equipment in the 88 Brigade sector as a ruse to support the impression that a major attack would indeed be launched across the Ravi near Maqboolpur. There is no indication, however, that the Indians either noticed or gave much heed to this deception.</p>

                       
                    </div>





                    <div class="war-details">
                        <h4>Central Punjab Sector </h4>

                        <p>Concerned about the possibility of a Pakistani thrust emanating from Bahawalnagar, India initially kept a significant reserve in this area: the 14th Infantry Division and 1st Armoured Division. After the first few days of the war, however, 14th Division found itself committed to the border in several locations and was thus no longer available as a true reserve. India’s lone armoured division, on the other hand, remained in this area throughout the conflict, but took no active part in the fighting. As in northern Punjab, this area was the scene of several dramatic struggles for small enclaves along a major river, the Sutlej. The 48 Brigade of India’s 7th Division cleared a protrusion known as the Sehjra Bulge on 5/6 December and 35 Brigade of 14th Division occupied a fairly large enclave near Mamdot just prior to the ceasefire. Pakistan’s 106th Brigade, however, succeeded in overwhelming stout resistance by Indian 15 Punjab to take a significant piece of ground near Hussainiwala, thereby controlling a key dam and threatening the important border town of Ferozepur. Further south, the Indian 116th Brigade (also 14th Division) captured 13 Pakistani border posts in its zone along the Sutlej.</p>

                       
                    </div>





                    <div class="war-details">
                        <h4>Fazilka Sector</h4>

                        <p>As at Hussainiwala, Pakistan achieved surprise against the Indian defenders west of Fazilka across the border from the Sulaimanke headworks where Pakistan occupied Beriwala bridge, despite repeated and costly counter-attacks. Fortunately for India, indecision on the part of Pakistan’s high command kept its potentially powerful 2 Corps from executing a planned offensive against India’s ‘Foxtrot Sector’ (or simply ‘F Sector’). This area was intended to be the zone of attack for Pakistan’s 2 Corps in the grand offensive to relieve pressure on East Pakistan and exploit presumed Indian weakness in the west.</p>


                        <p>The Pakistani plan, called ‘Changez Khan’, was approved in September 1971. It called for 105th Brigade and 25th Brigade to come under 2 Corps as it drove east from the vicinity of Bahawalnagar to cross the international border before turning to the north-east to push for Bhatinda and Ludhiana. Once 2 Corps had secured its objectives, 4 Corps was to advance east from its positions along the Ravi. In addition to reserving several squadrons to support the planned attack, the PAF had also positioned mobile radar and pre-stocked forward airfields in the proposed battle area. Army HQ issued the order for 2 Corps to shift to its forward assembly areas on 14 December and major elements of the 1st Armoured Division began to move the following day, while the 7th Division was also concentrating south of the Sutlej, its officers hoping for ‘an end to the agony of suspense’.</p>


                        <p>By this time, however, the other major component of 2 Corps, namely, 33 Division, had already been detached from the reserve. Army HQ had responded to Indian advances elsewhere by breaking up 33 Division to reinforce 1 Corps in the north and 18 Division in the south. As a consequence, the 2 Corps offensive was deprived of approximately one-third of its striking power before it had even begun. At 1845 Hours on 16 December 1971, new instructions arrived from Army HQ ‘freezing all movements’ until further notice. Nine trains carrying the 1st Armoured Division were laboriously unloaded, the equipment was dispersed, and 2 Corps, with its constituent pieces on both sides of the Sutlej, settled in to await orders. Had the operation proceeded according to schedule, the corps probably could have opened its attack in the early morning hours of 17 December 1971, but the ‘freeze’ order meant that it was still on the Pakistan side of the border when the ceasefire went into effect at 2000 Hours on 17 December.</p>

                        <p>Although it is doubtful that 2 Corps could have achieved its optimistic operational goals, nothing in Pakistan’s conduct of the war in the west has generated more controversy among subsequent commentators than the failure to launch the 2 Corps counter-offensive. The details of the delay in beginning the attack and the issuance of the ‘freeze’ order are unclear, but most writers point to confusion and vacillation prevalent within the high command. One source contends that the lack of adequate PAF support was a major contributing factor; he avers that the Commander in Chief of the Air Force retracted previous promises of assistance a few days before the operation was to begin. There was no significant action south of Fazilka, although Indian 4 PARA of the 51st Para Brigade conducted a costly assault to evict an intruding Pakistani platoon from a nameless sand dune near the village of Nagi several days after the ceasefire (26/27 December 1971). This action cost 4 PARA 21 killed and 60 wounded.</p>

                        
                    </div>


                    <div class="war-details">
                        <h4>Jaisalmer Sector</h4>

                        <p>The great desert that sweeps across the southern portion of India–Pakistan border presents a formidable obstacle to operations by major military formations. In addition to climatic extremes and lack of water, road and rail connections are scarce and vehicular movement is generally restricted to the area’s few tracks. Nonetheless, both sides committed significant forces to this barren region and planned advances across the border. Pakistani Army HQ had allotted this enormous zone to a single division, supported by some paramilitary and irregular troops. This formation—18—Division, had two brigades opposite Jaisalmer and one in the southern portion of its sector near Naya Chor. On the Indian side of the border, Lt Gen Gopal Gurunath Bewoor’s Southern Command, with two regular divisions and two sector HQ, was responsible for the area.</p>


                        <p>In the Jaisalmer sector, both sides planned to take the offensive. Although Pakistan’s 18 Division had neither the training, the equipment, nor the logistical support necessary to sustain a major advance, it was expected to capture Ramgarh and press on to neutralise the airfield at Jaisalmer. Unfortunately for 18 Division, no proper coordination was arranged with the PAF. The Pakistani 51st and 206th Brigades, reinforced by two tank regiments (22 and 38 Cavalry), duly headed for the western face of the Jaisalmer Bulge on 3 December 1971. The Indian 12th Division, methodically preparing its own offensive, was caught by surprise when the Pakistanis appeared around Longewala. Pakistani indecision and staunch resistance by the Indian Army’s 23 Punjab, however, stalled the attack and provided an opportunity for the IAF to come to the assistance of 23 Punjab. With no opposition in the air to trouble them, marauding IAF Hunters from Jaisalmer wreaked havoc on the vulnerable Pakistani tank columns strung out in the open desert. By 7 December 1971, the attacking Pakistani brigades were in full retreat having suffered heavy losses, including at least 20 tanks and more than 100 other vehicles destroyed or abandoned.</p>


                        <p>India’s 12th Division did not pursue very far (‘mercifully’, wrote Pakistani Maj Gen Fazal Muqeem Khan) and, other than the capture of Islamgarh on 4 December, 12 Division spent the remainder of the war screening its sector. Pakistan’s leadership, alarmed by the serious reverse at Longewala, sent a brigade of 33 Division to reinforce 18 Division in this sector, while the division HQ, artillery and 60 Brigade hurried south to take control of the Naya Chor sector. North of Jaisalmer was India’s Bikaner sector, also known as ‘K or ‘Kilo Sector’. In this desolate landscape, 13 Grenadiers, a camel battalion, supported by two BSF battalions, occupied a number of Pakistani border posts.</p>

                                              
                    </div>



                    <div class="war-details">
                        <h4>Barmer Sector</h4>

                        <p>With only one brigade of regulars in this area—55 Brigade of 18 Division—Pakistani offensive operations were out of the question. India’s 11 Division, however, struck across the desert on the evening of 4 December 1971 in an effort to reach the ‘green belt’ along the Indus near Hyderabad. While a camel-mounted battalion—17 Grenadiers—shielded the division’s northern flank, two brigades—the 85 and 330—pushed for Naya Chor astride the rail line and 31 Brigade advanced on Chachra. Facing little resistance at first, the Indians quickly achieved major territorial gains and constructed a link between the Indian and Pakistani railroads (Munabao to Khokhrapar) to support further operations. Despite the rail connection, logistical problems and prolonged hesitation brought the division’s progress to a halt before it reached Naya Chor on its northern axis of advance.</p>


                        <p>Persistent air attacks by the PAF increased the difficulty of supplying 11 Division’s forward elements. By the time the advance resumed, the Pakistani troops had been reinforced by the 60th Brigade from the 33rd Division. Moreover, 33rd Division had taken responsibility for the sector, assuming command of 55 Brigade in the process. Thus bolstered, the Pakistanis easily repelled 11 Division’s renewed attempts to push forward. On the southern axis, Chachra fell to 31 Brigade after a brief fight, but a battalion probing towards Umarkot— 18 Madras—found itself unsupported and was forced back by a Pakistani counter- attack. The ceasefire precluded further Indian advances, but 11 Division had seized almost 3,000 sq miles of Pakistani territory, more than any other formation on the western front. Moreover, anxiety following 18 Division’s failure at Longewala and the Indian drive on Naya Chor caused Pakistan’s high command to deplete 2 Corps by sending 33 Division south with two of its brigades. As mentioned earlier, this move, combined with the detachment of 33 Division’s other brigade to 1 Corps, effectively diminished 2 Corps’ offensive power by one-third, thus reducing the threat it could pose to India in the Ferozepur–Fazilka area. On the other hand, Lt Gen Bewoor was in the process of shifting the 322nd Brigade of 12 Division to reinforce 11 Division almost at the same time as the Pakistani moves were taking place.</p>

                    </div>




                    <div class="war-details">
                        <h4>Kutch Sector </h4>

                        <p>The Rann of Kutch, an inhospitable salt waste, separates India and Pakistan at the southern extremes of their border. Although no regular troops were available on the Indian side at the start of hostilities in this sector, two BSF battalions succeeded in making considerable advances against light opposition from Pakistani Rangers. Thanks to their efforts, by 17 December 1971, India controlled two large islands of elevated desert above the salt flats—Chhad Bet and the area south of Virawah. The Indian 10 PARA Commando Battalion conducted successful raids as far as Islamkot in this sector. In all, Southern Command ended the war in occupation of a significant chunk of Pakistani territory, albeit unproductive desert, and had distracted key elements of Pakistan’s strategic reserve. It had not, however, reached Pakistan’s ‘green belt’ along the Indus, as had been hoped by some Indian planners at the war’s beginning.</p>

                    </div>
                </div>


                    <!-- <div id="note" class="accordion-details">
                        <h3>Notes</h3>

                        <div class="war-details">
                            <ol>
                                <li>Richard Sisson and Leo E Rose, War and Secession: Pakistan, India, and the Creation of Bangladesh, Berkeley: University of California Press, 1990, pp. 149–50. Subrahmanyam was quoted in Sisson and Rose, pp. 149–50. His opinions appeared in several Indian newspapers starting on 31 March and 5 April; some remarks were then reported in the Times (London) in July. Sisson and Rose offer a thorough discussion of the facts surrounding Subrahmanyam’s statements, citing the incident as another example of ‘The tendency in both India and Pakistan to look for “worst case” interpretations of each other’s behavior.’ Also see SN Prasad, The India-Pakistan War of 1971: A History, Delhi: Natraj Publishers, 2019, p. 110, and Maj Gen Sukhwant Singh, The Liberation of Bangladesh, New Delhi: Vikas, 1981, pp. 93–99. To support the contention that India sought to break up Pakistan, Pakistani commentators often cite the so-called ‘Agartala conspiracy’, a case of alleged sedition involving Sheikh Mujib and named for the Indian city of Agartala where he is said to have met with Indian agents during 1966.</li>

                                <li>	HRC Report Supplement, Part III, Chapter VIII; Lt Gen M Attiqur Rahman, Back to the Pavilion, Karachi: Cowasjee, 1990, p. 241; Maj Mumtaz Hussain Shah, ‘The Battle of Sylhet Fortress’, Defence Journal, January 1999; Brig Saadullah Khan, East Pakistan to Bangladesh, Lahore: Lahore Law Times Publications, 1975, pp. 142, 182.</li>

                                <li>	HRC Report, Part IV, Chapters IV, V, and XI; Pakistan Navy Historical Section, Story of the Pakistan Navy (Islamabad, 1991, p. 320; Mansoor Shah, The Gold Bird: Pakistan and Its Air Force—Observations of a Pilot, Karachi: Oxford University Press, 2002, p. 242.</li>

                                <li>	Maj Gen Rao Farman Ali Khan, How Pakistan Got Divided, Lahore: Jang, 1992, pp. 119–20. Brig AR Siddiqi remarked that ‘back in Rawalpindi none appeared to be in command’, adding that there was an atmosphere of ‘forced optimism’ at headquarters. See AR Siddiqi, ‘1971 Curtain-Raiser’, Pakistan Army Journal, December 1977.</li>

                                <li>	Maj Gen Lachhman Singh Lehl, Victory in Bangladesh, Dehra Dun: Natraj, 1991, pp. 55, 64–65, 289, 298. Similar comments are in Air Marshal CV Gole, ‘Air Operations in the Western Sector During 1971 Indo-Pak War’, The Journal of the United Services Institution of India, July-September 1990; also Lt Gen EA Vas, Fools and Infantrymen: One View of History, 1923–1993, Meerut: Kartikeya, 1995, p. 224. For an incisively critical view, see Lt Gen VR Raghavan’s commentary in The Telegraph, dated 28 November 2000. More favourable analyses are in Gen KV Krishna Rao, Prepare or Perish, New Delhi: Lancer, 1991, p. 249; Admiral JG Nadkarni, ‘When the Gods Smiled on India’, Rediff.com, 4 December 2002; Maj Gen Rajendra Nath, Military Leadership in India, New Delhi: Lancer, 1990, p. 475; Maj Gen Joginder Singh, Behind the Scene, New Delhi: Lancer: 1993, p. 247.</li>


                                <li>	Maj Gen Shaukat Riza, The Pakistan Army 1966-71, Dehra Dun: Natraj, 1977, p. 110. Gen Riza commanded the 9 Infantry Division in East Pakistan in early 1971.</li>

                                <li>	Khan, East Pakistan to Bangladesh, n. 9, p. 110. Siddiqi, ‘1971 Curtain-Raiser’, and ‘The War of Self-Attrition’. See also the eminently readable and insightful account by the then- commander of 26 Frontier Force in the Northwestern Sector, Maj Gen Hakeem Arshad Qureshi, The 1971 Indo-Pak War: A Soldier’s Narrative, Karachi: Oxford University Press, 2002, pp. 17–21, 76, 101: ‘Not a day passed when a firefight or an explosion did not take place.’</li>

                                <li>	Lt Gen Kamal Matinuddin, Tragedy of Errors, Lahore: Wajidalis, 1994, p. 415.</li>

                                <li>	Maj Gen Rahim Khan, Commander of 39 Division in 1971, Interview in The Herald, September 2000.</li>

                                <li>	John H Gill, An Atlas of the 1971 India–Pakistan War: The Creation of Bangladesh, National Defense University, Near East South Asia Center for Strategic Studies, 2003, p. 17.</li>

                                <li>	Lt Gen Ali Kuli Khan, interview in Defence Journal, December 2001.</li>
                                
                                <li>	Lt Gen Kamal Matinuddin, Tragedy of Errors, Lahore: Wajidalis, 1994, pp. 342–43, 347–50.</li>
                                
                                <li>	Ten of the squadron’s 12 tanks were captured or destroyed. In addition to the basic sources, see, Col Anil Shorey, ‘The Unique Battle of Garibpur’, Sainik Samachar, 16–30 April 2002. Not all of the Indian probes prospered: Pakistani Lt Gen Imtiaz Waraich, for example, narrates a successful defence by his battalion, 21 Punjab, in ‘Remembering Our Warriors’”, Defence Journal, October 2001.</li>

                                <li>	Lt Col Muhammad Mehboob Qadir, ‘Echoes from the Event Horizon’, Pakistan Army Journal, March 1991.</li>

                                <li>	Lt Gen Sagat Singh’s notes cited in Cdr Suresh D Sinha, Sailing and Soldiering in Defence of India, New Delhi: Chanakya, 1990, p. 167.</li>

                                <li>	The destruction of the rail bridge actually did little to slow the Indian advance as IV Corps did not have adequate material to lay decking for vehicular traffic. For this and other details of engineering support to IV Corps as well as vivid scenes from the drive to the Meghna, see Brig Jagdev Singh, The Dismemberment of Pakistan, New Delhi: Lancer, 1988, pp. 147–66.</li>

                                <li>	Brig HS Sodhi, ‘Operation Windfall’: Emergence of Bangladesh, New Delhi: Allied, 1980, p. 280.</li>

                                <li>	Col Iqbal M Shafi, letter to the editor, Defence Journal, February 1999.</li>

                                <li>	Maj Mumtaz Hussain Shah, ‘The Battle of Sylhet Fortress’, Defence Journal, January 1999.</li>

                                <li>	To augment its limited aviation assets, the Pakistan Army commandeered aircraft from flying clubs and other civilian associations, gave them a hasty coat of desert camouflage paint, and stenciled them with military markings. See John Fricker, ‘The Tree-top Warriors’, Air Enthusiast, October 1972, published on the web by the Pakistan Military Consortium at www.pakdef.info.</li>

                                <li>	Maj Gen Fazal Muqeem Khan, Pakistan’s Crisis in Leadership, New Delhi: Alpha & Alpha, 1973, p. 114.</li>
                                   
                                <li>	Ibid., p. 115.</li>

                                <li>	SN Prasad (ed.), History of Indo-Pak War, 1971, New Delhi: Government of India, 1992, p. 298.</li>

                                <li>	Maj Gen Sukhwant Singh, Defence of the Western Border, New Delhi: Lancer, 1998, p. 12.</li>

                                <li>	Prasad, History of Ind-Pak War 1971, p. 298.</li>

                                <li>	HRC Report, Part IV, Chapter VIII.</li>

                                <li>	Singh, Defence of the Western Border, n. 31, p. 26.</li>

                                <li>	Ibid., p. 28.</li>

                                <li>	Owing to the lack of troops, only one regular company (C/2 Frontier Force) was available in the Tangdhar area at the start of the conflict and some portions of the Lipa valley were defended by ad hoc collections of regulars, paramilitary men and local levies. See Maj Gen Fazal Muqeem Khan, History of the 2nd Battalion (Guides) Frontier Force Regiment, Rawalpindi: The Army Press, 1996, pp. 123–26.</li>

                                <li>	Col RD Palsokar, History of the Sikh Light Infantry, Fategarh: The Sikh Light Infantry, 1997, pp. 304–309. The commander of 8 Sikh was relieved of command as a result of this action. Also see Maj Gen Jagjit Singh, With Honour and Glory, New Delhi: Lancer, 2001, p. 79. Some Indian authors are quite critical of the entire enterprise; see Singh, Defence of the Western Border, n. 30, pp. 41–44; Col MN Gulati, Pakistan’s Downfall in Kashmir, New Delhi: Manas, 2001, pp. 261–62.</li>

                                <li>	Brig Asif Haroon, Kashmir Battles of 1948-1965-1971 and Ongoing Freedom Struggle, Lahore: Takhleeqat, 1995, pp. 155–59. Pakistan’s 12 Division reportedly held an exercise in September/October to prepare for the assault on Poonch; See Gulati, Pakistan’s Downfall in Kashmir, n. 37, p. 264.</li>

                                <li>	23 Division’s artillery practiced some innovative deception techniques such as deploying guns well forward of doctrinal norms to avoid counterbattery fire and using a few heavy guns from the Artillery School to simulate the presence of corps artillery support. See interview with the divisional artillery commander (and 111 Brigade commander from 5 December), Maj Gen Naseerullah Khan Babar in Defence Journal, April 2001.</li>

                                <li>	Singh, With Honour and Glory, n. 37, p. 75–90.</li>

                                <li>	According to the HRC Report, Part IV, Chapter VIII, 23 Division had accomplished its mission by gaining the line of the river. The division commander, however, decided to press across after reforming his troops.</li>

                                <li>	Additional sources on the fight for Chhamb include: Brig Safdar Ali, ‘Chhamb--A Battle Won Twice’, Pakistan Army Journal, September 1992; Maj Agha Humayun Amin, ‘The Battle of Chhamb 1971’, Defence Journal, September 1999; Brig Shamin Yasin Manto, ‘Remembering Our Warriors]’ (interview), Defence Journal, February 2002; Lt Col Ahmad Saeed, The Battle of Chhamb 1971, Rawalpindi: Army Education Press, 1973; Lt Gen RK Jasbir Singh, ‘Battle of Chhamb: Indo-Pak War 1971’, Journal of the United Services Institution of India, July-September 1990; Maj Gen Prem K Khanna and Pushpindar Singh Chopra, Portrait of Courage: Century of the 5th Battalion, The Sikh Regiment, New Delhi: Military Studies Convention, 2001, pp. 193–232. Manto avers that the division’s attempts to cross the Manawar Tawi were undertaken without sufficient preparation: ‘The effort to achieve speed actually turned into haste’ (emphasis in original).</li>

                                <li>	Quoted in ‘Defending the West’, from 1971 India-Pakistan War: Remembering a Liberation War, at www.bharatrakshak.com.</li>

                                <li>	Brig Harinder Singh Sodhi, Top Brass: A Critical Appraisal of the Indian Military Leadership, Noida: Trishul, 1993. Sodhi calls the advance ‘pathetic’, averring that it demonstrated ‘pedestrian thinking’ (pp. 130, 147). Likewise, Jagjit Singh, With Honour and Glory, n. 36, pp. 92–93, 104; Maj Gen Kuldip Singh Bajwa, The Falcon in My Name: A Soldier’s Diary, New Delhi: South Asia Publications, 2000, pp. 200–14; Lal, pp. 237–40; Sukhwant Singh, Defence of the Western Border, n. 30, pp. 106–9; Lt Col JR Saigal, Pakistan Splits: The Birth of Bangladesh, New Delhi: Manas, 2000, pp. 155–68.</li>

                                <li>	Sources for the Changez Force: Lt Col Asif Duraiz Akhtar, ‘My Memories of War: The Battle of Tola’, Pakistan Army Journal, June 1988; Col MY Effendi, ‘Smouldering Embers of Ten December Days’,” Pakistan Army Journal, Winter 1994; Brig Nasir Ahmed Khan, ‘Covering Troops Battle’, Pakistan Army Journal, September 1984. Some Indian participants aver that the Pakistani minefields were much overrated by the Indians and should have been crossed with greater expedition; see Bajwa, pp. 206, 212; Saigal, pp. 65–76.</li>

                                <li>	Maj Agha Humayun Amin, ‘Battle of Barapind-Jarpal 16 Dec 1971’, Defence Journal, October 1999; HRC Report, Part IV, Chapter VIII; Lt Gen Hanut Singh, ‘Fakhr-e-Hind’: The Story of the Poona Horse, Dehra Dun: Agrim, 1993, pp. 259–90; also Anil Bhat, ‘Taken by the Gathering Storm’, Sainik Samachar, 16–28 February 1997. The other regiment of 8 Armoured Brigade, 27 Cavalry, played only a limited role in the fighting. The engagement is variously known as the ‘Battle of Barapind-Jarpal’ and the ‘Battle of Basantar’.</li>

                                <li>	Many Indian writers (notably Sukhwant Singh, HS Sodhi, and KC Praval) are very crit- ical of the performance of Indian I Corps in the Shakargarh fighting. Colonel Ranjit Sengupta, on the other hand, points out that the corps did succeed in protecting a vulner- able segment of India’s border with Pakistan; see ’Battlefield Management and Logistics: Shakargarh 1971’, Indian Defence Review, April 1994.</li>

                                <li>	For two views of one of these engagements, see Lt Col Ghulam Qadir, ‘The Maqboolpur Enclave Encounter’, Pakistan Army Journal, September 1982; and ‘Attack on Pak Fatehpur Post (8 Sikh LI),’ Sainik Samachar, 26 May 1991.</li>

                                <li>	HRC Report, Part IV, Chapter VIII.</li>

                                <li>	Major General Ghulam Umar, then Secretary of the National Security Council, has since disparaged this plan: ‘We always chanted that the defense of the East lies in the West. It was a bogus theory.’ Quoted in Muntassir Mamoon, The Vanquished Generals and the Liberation War of Bangladesh, Somoy Prakashan, 2000, p. 92.</li>

                                <li>	Shah, The Golden Bird, n. 10, 2002, p. 256.</li>

                                <li>	Lt Gen Jahan Dad Khan, Pakistan Leadership Challenges, Karachi: Oxford University Press, 1999, p. 121; Arif, p. 124. </li>

                                <li>	The bulk of this narrative and the quote from the Army orders are taken from HRC Report, Part IV, Chapter VIII. Lt Gen Candeth, commander of India’s Western Command, opined that a Pakistani attack between Jammu and Gurdaspur during October would have stood a good chance of success as Indian I Corps was not in position there until late in the month. A recent Pakistani commentator suggests that the time to attack was late November after the Indian incursions into East Pakistan that resulted in the engagements around Garibpur and other locales: Lt Col Mukhtar Ahmad Gilani, “Lost Opportunity—A Military Analysis 1971,” Defence Journal, January 2003.</li>

                                <li>	Among others, see Gul Hassan Khan, pp. 329–39; Jahan Dad Khan, p. 121; Pakistan Air Force, pp. 467–74; Gilani, ‘Lost Opportunity’ (citing interviews with Gul Hassan Khan); Maj Agha Humayun Amin, ‘The Anatomy of Indo-Pak Wars: A Strategic and Operational Analysis’, Defence Journal, August 2001; Maj Agha Humayun Amin, ‘The Western Theatre in 1971: A Strategic and Operational Analysis,’ Defence Journal, February 2002; Brig AR Siddiqi, ‘The War of Self-Attrition’; and ‘National Defence and Security: A Reappraisal’, The Nation, 8 January 1990. </li>

                                <li>	Maj Gen Aboobaker Osman Mitha, Fallacies & Realities: An Analysis of Lt Gen Gul Hassan’s ‘Memoirs’, Lahore: 99 Maktaba Fikr-o-Danish, 1994, pp. 66–67.</li>

                                <li>	There is disagreement over who ordered the attack on Jaisalmer. Gul Hassan Khan and other Pakistani writers state that the operation was a favourite project of the Chief of the Army Staff, General Abdul Hamid Khan, and that the attack was arranged at the last minute despite the strenuous objections of 18 Division’s commander, Maj Gen BM Mustafa. Furthermore, they claim that the PAF was not apprised of the scheme. Brig ZA Khan (commander of 38 Cavalry at the time), however, claims that the push to Jaisalmer was planned in October and that the Chief of the Air Staff knew of the plan weeks before the war, at least in outline form. See his autobiography The Way It Was, Karachi: Ahbab, 1998, and an interview ‘Remembering Our Warriors: Brig (Retd). Zahir Alam Khan’, Defence Journal, April 2002. Khan also writes that the plan to take Jaisalmer was abandoned just prior to the attack. Another veteran, however, questions many of the details in ZA Khan’s accounts: Lt Col HK Afridi, ‘Letter to the Editor’, Defence Journal, February 1999. Similarly, Mansoor Shah makes a convincing case that general contingency plans to support the army in Sindh/Rajasthan did exist, but that the army did not call on the PAF until the disaster at Longewala was in progress (Shah, The Golden Bird, n. 10, pp. 257–63).</li>

                                <li>	The defeat of the Pakistani attack on Longewala has drawn the attention of numerous Indian writers. In addition to the standard war histories, see Air Marshal MS Bawa, ‘Saga of Longewala’, Indian Air Force Journal, 1997, at www.bharat-rakshak.com/IAF/ History; Brig VP Dev, ‘The Desert War—1971’, Indian Defence Review, January 1994; James Hattar, ‘Taking on the Enemy at Longewala’, The Tribune, 16 December 2000; Virendra Verma, Hunting Hunters: Battle of Longewala December 1971, Dehra Dun: Youth Education Publications, 1992; Lt Col Jai Singh, Tanot Longewala and Other Battles of the Rajasthan Desert 1965 & 1971, Dehra Dun: Palit & Palit, 1973; Wg Cdr Kukke Suresh, ‘Battle of Longewala’,at www.bharat-rakshak.com/IAF/History.</li>

                                <li>	Fazal Muqeem Khan, p. 212. Among other things, 12 Division cited inadequate artillery and armour support; see Jagjit Singh, With Honour and Glory, n. 37, pp. 118–19.</li>



                                <li>	The inclusion of the divisional artillery is from V. Longer, Red Coats to Olive Green, Bombay: Allied Publishers, 1974, p. 497.</li>

                                <li>	Mansoor Shah, pp. 264–65, 284–85; Pakistan Air Force, p. 464; Prasad, pp. 399–400; Singh, Defence of the Western Border, n. 31, pp. 215–42.</li>

                                <li>	Singh, Western Border, pp. 176–245, is very critical of Indian operations in Rajasthan. Also see Maj Agha Humayun Amin, ‘Remembering Our Warriors: Brig Muhammad Taj’, Defence Journal, September 2002.</li>

                                <li>	The BSF advances began on 13 December. Some limited information is in ‘Kutch during the 1971 Indo-Pak War,’ www.kutchinfo.com.</li>

                            </ol>
                        </div>
                    </div> -->

                    <!-- <div id="additional" class="accordion-details">
                        <h3>Additional Sources</h3>
                        <div class="war-details">
                            <p>SN Prasad and UP Thapliyal (ed.), The India–Pakistan War of 1965: A History., New Delhi: Ministry of Defence, 2011.</p>

                            <p>RD Pradhan, 1965 War—The Inside Story: Defence Minister’s Diary of the India–Pakistan War, New Delhi: Atlantic Publishers and Distributors, 2007.</p>

                            <p>PK Chakravorty and Gurmeet Kanwal, ‘Operation Gibraltar: An Uprising that Never Was’, Journal of Defence Studies, Vol. 9, No. 3, July–September 2015, pp. 33–52. </p>

                            <p>Shruti Pandalai, ‘Recounting 1965: War, Diplomacy and Great Games in the Subcontinent’, Journal of Defence Studies, Vol. 9, No. 3, July–September 2015, pp. 7–32.</p>

                            <p>Rahul K. Bhonsle, ‘Indian Army’s Continuity and Transformation: Through the Prism of the Battle of Dograi’, Journal of Defence Studies, Vol. 9, No. 3, July–September 2015, pp. 75–94.</p>

                            <p>Maj Gen Kuldip Bajwa, India–Pakistan War 1971: Military Triumph and Political Failure, New Delhi: Har-Anand Publications, 2012.</p>

                            <p>Col Anil Shorey, Pakistan’s Failed Gamble: The Battle of Laungewala, New Delhi: Manas, 2004.</p>

                            <p>Lt Gen JFR Jacob, Surrender at Dacca: Birth of a Nation, New Delhi: Manohar, 2011.</p>

                            <p>Srinath Raghavan, 1971: A Global History of the Creation of Bangladesh, New Delhi: Orient Blackswan, 2015.</p>

                            <p>PVS Jagan Mohan and Samir Chopra, Eagles Over Bangladesh: The Indian Air Force in the 1971 Liberation War, New Delhi: Harper Collins, 2013.</p>

                            <p>Sarmila Bose, Dead Reckoning: Memories of the 1971 Bangladesh War, Gurgaon: Hachette India, 2016.</p>
                        </div>
                    </div> -->


                <!-- <div id="gallery" class="row image-gallery">
                    <div class="col-md-12">
                        <h3>Gallery</h3>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery1.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery2.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                            <p>Image-2</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery3.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                            <p>Image-3</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery4.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                            <p>Image-4</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery5.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                            <p>Image-5</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery6.jpg" class="img-fluid card-img-top" alt="" loading="lazy" loading="lazy">
                            <p>Image-6</p>
                        </div>
                    </div>
                </div> -->


                <!-- <div id="video" class="row image-gallery">

                    <div class="col-md-12">
                        <h3>Video</h3>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="featured-video" >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">
                                <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="featured-video" >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">
                                <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="featured-video" >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">
                                <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>

                    
                </div> -->

 
            </div>  
        </div>
    </div>
</section>


<?php include('footer.php'); ?>