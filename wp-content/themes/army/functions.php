<?php
/**
 * Functions and definitions
 */


 // Exit-if accessed directly
if(!defined('ABSPATH')) exit;



/* ADD THEME SCRIPTS AND STYLE FILE */
function add_theme_scripts()
{

    /* Styles */
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', false, '2.3.4', 'all');
    wp_enqueue_style('slick', get_template_directory_uri() . '/assets/slick/slick.css', false, '6.4', 'all');
    wp_enqueue_style('slick-theme', get_template_directory_uri() . '/assets/slick/slick-theme.css', false, '1.0', 'all');
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/js/font-awesome.min.js', false, '5.0', 'all');

    wp_enqueue_style('style', get_template_directory_uri() .  '/assets/css/style.css', false, '1.0', 'all');
    wp_enqueue_style('responsive', get_template_directory_uri() .  '/assets/css/responsive.css', false, '1.0', 'all');
    wp_enqueue_style('main-responsive', get_template_directory_uri() .  '/assets/css/main-responsive.css', false, '1.0', 'all');
    
    wp_enqueue_style('main', get_template_directory_uri() .  '/assets/css/main-css.css', false, '1.0', 'all');

    



    /* Scripts */

    if (!is_admin()) {
        //Call JQuery
        //wp_deregister_script('jquery');
    }

    wp_enqueue_script('jquery-js', get_template_directory_uri() .  '/assets/js/jquery.min.js', array(), null, true);
    wp_enqueue_script('bootstrap-js',  get_template_directory_uri() .  '/assets/js/bootstrap.bundle.min.js', array(), null, true);

    wp_enqueue_script('slick-js', get_template_directory_uri() .   '/assets/slick/slick.js', array(), null, true);

    
    wp_enqueue_script('font-awesome-JS', get_template_directory_uri() . '/assets/js/font-awesome.min.js', array(), null, true);
    wp_enqueue_script('main-JS', get_template_directory_uri() . '/assets/js/main.js', array(), null, true);
}
add_action('wp_enqueue_scripts', 'add_theme_scripts');


/* REGISTER PRIMARY MENUS */
add_action('after_setup_theme', 'register_primary_menu');
function register_primary_menu()
{
    register_nav_menu('Header', __('Header Menu', 'pearl'));
	register_nav_menu('Footer Link', __('Footer Menu', 'pearl'));
	register_nav_menu('Links 2', __('Links 2 Menu', 'pearl'));
	register_nav_menu('Services', __('Services Menu', 'pearl'));
}


function pearl_init()
{

    register_sidebar(
        array(
            'name'          => esc_html__('Recent Post Sidebar', 'auretics'),
            'id'            => 'recent-post-sidebar',
            'description'   => esc_html__('Add widgets here to appear in details blog page.', 'auretics'),
            'before_widget' => '',
            'after_widget'  => '',
            'before_title'  => '',
            'after_title'   => '',
        )
    );
}
add_action('widgets_init', 'pearl_init');


/* THEME OPTIONS PAGE - HEADER, FOOTER, FAQS, CONSULTATION */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-options',
		'capability'	=> 'edit_posts',
        'parent_slug'	=> '',
        'position'		=> false,
        'icon_url'	    => false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Header',
		'menu_title'	=> 'Header',
		'menu_slug' 	=> 'theme-options-header',
		'capability'	=> 'edit_posts',
        'parent_slug'	=> 'theme-options',
        'position'		=> false,
        'icon_url'	    => false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Footer',
		'menu_slug' 	=> 'theme-options-footer',
		'capability'	=> 'edit_posts',
        'parent_slug'	=> 'theme-options',
        'position'		=> false,
        'icon_url'	    => false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Common Section',
		'menu_title'	=> 'Common Section',
		'menu_slug' 	=> 'theme-options-common',
		'capability'	=> 'edit_posts',
        'parent_slug'	=> 'theme-options',
        'position'		=> false,
        'icon_url'	    => false
	));

	
}

// To allow svg file upload in admin
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');