<?php

$main = "brave";

$page = "award";

include('header.php'); ?>

<section class="iconic-leaders-detail-banner" style="background-image: url(assets/img/blog-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">Gallantry Awards </h1>
    </div>
</section>

<section class="gallantry-award">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9  col-sm-12">
                <h2>Introduction</h2>
                <p>Conflict and warfare are undeniable realities of human history and societies. Similarly, remembrance is also part of human nature and all human societies, from prehistoric times till date, have come up with unique ways and means of remembrance and memorialisation. Victories in battles have been commemorated in legends, stories and ballads, on stone and paper, in the memories of civilisations. Memorials to victory as well as sites of battles and tombs or cenotaphs marking the sites of cremation/ burial of kings and warriors are found throughout the length and breadth of India in the form of commemorative stambhas (pillars), dwaras (gates), chhatris (cupolas) and ‘hero stones’. The rise of the individual, and the accompanying modernisation and democratisation of society, led to the role, valour and sacrifice of the individual soldier becoming significant and finding an expression of acknowledgement in official despatches and rewards. We begin to see this during colonial times, in particular, when two significant practices: of marking the valour of a soldier in battle by the grant of an official gallantry award, and recording the names of individual ‘other ranks’ who had performed exceptionally well in the field in the official dispatch sent by the commander to government, now known as being ‘mentioned in despatches’, both originated in the Indian Army.</p>


                <p>The ‘Indian Order of Merit’ (IOM), originally instituted by the East India Company in 1837 as the ‘Order of Merit’, has the distinction of being the oldest gallantry award in the Commonwealth. It was later taken over by the British Crown in 1858. The IOM predates the Victoria Cross (VC) by 17 years. The IOM was the Indian equivalent of the VC and the highest gallantry award that could be won by an Indian soldier until 1911, when Indians were made eligible for the latter decoration following King George Vs proclamation at the Delhi Durbar that year. By this time, around 3,000 Indian soldiers had been awarded the IOM, including 47 awards of the first class of the Order, equivalent of being awarded the VC with two bars.</p>

                <p>Post-independence, the British Honours System was replaced with an Indian system of gallantry awards beginning 1950 with the adoption of the Constitution that made India a sovereign republic. A series of awards were instituted to recognise the bravery of those who made the supreme sacrifice for the nation and to inspire the next generation to serve honourably. Gallantry Awards in India are classified into two categories: those awarded in wartime or for gallantry in the face of the enemy; and those awarded during peace time or for gallantry other than facing the enemy. Of the former, the first three gallantry awards—the Param Vir Chakra (PVC), the Maha Vir Chakra (MVC) and the Vir Chakra (VrC)—were instituted by the Government of India on 26 January 1950 with retrospective effect from the 15 August 1947. The PVC replaced the pre-independence Victoria Cross, and was designed by Savitri Khanolkar on the request of then Adjutant General, Major General Hira Lal Atal. Khanolkar went on to design other bravery medals, including the Mahavir Chakra, the Vir Chakra and peacetime gallantry medals such as the Ashok Chakra, Kirti Chakra, and the Shaurya Chakra.</p>

                <p>Awards for bravery not only honour and commemorate the actions of those who have served beyond the call of duty but also inspire each generation to pick up the mantle to defend our treasured freedoms.</p>

                <p>Apart from gallantry awards the Yuddh Seva and Vishishth Seva series of medals in three grades recongnised distinguished services in times of war and peace respectively, while the Sena Medal can be awarded for gallantry as well as for distinguished services.</p>


                <h1>Gallantry Awards in India</h1>


                <h2 class="heading-border">British Honours System</h2>
                <p>Prior to independence in 1947, the Indian government as well as the military/army followed the British Honours System. As mentioned earlier, the oldest military and civilian decoration instituted in India was by the East India Company—the IOM in 1837. This was taken over by the British Crown in 1858. Its name was changed to the ‘Indian Order of Merit’ in 1902. The IOM had three classes—the first, second and third—in the military decoration until Indians became eligible for other medals such as the VC. The IOM classes were as follows: </p>

                <div class="list-classes">
                    <p class=""><strong>1. Third Class:</strong> : This was an eight-pointed dull silver star with blue circle, surrounded by silver laurels, in the middle, with crossed swords and the words ‘Reward of Valour’. The medal featured a dark blue ribbon flanked by two red stripes of about a sixth of the width. The IOM Third Class was awarded for a ‘conspicuous act of individual gallantry on the part of any Native Officers or Soldiers, in the Field or in the attack or defence of a Fortified place, without distinction of rank or grade.’</p>

                    <p><strong>2. Second Class:</strong>This featured an eight-pointed shiny silver star with blue circle, surrounded by gold laurels in the middle, with crossed swords and the words ‘Reward of Valour’. This was changed to ‘Reward of Gallantry’ in 1944. The medal featured a dark blue ribbon flanked by two red stripes of about a sixth of the width. The IOM Second Class could be obtained by those who already possessed the third class till 1911, and for similar services thereafter.</p>

                    <p><strong>3. First Class:</strong>: This was an eight-pointed gold star with blue circle, surrounded by gold laurels in the middle, with crossed swords and the words ‘Reward of Valour’. This was changed to ‘Reward of Gallantry’ in 1944. The medal featured a dark blue ribbon flanked by two red stripes of about a sixth of the width. This class of IOM could be obtained in like manner only by those who possess the third and second classes.</p>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="image-place">

                            <img src="./assets/img/award/Indian Order of Merit 1.png" alt="" loading="lazy" class="img-fluid">

                        </div>
                        <span class="img-des" style="color: #D90000; font-weight: 500;">Originally instituted in 1937 as the ‘Order  of Merit’ the Indian Order of Merit (IOM) is the oldest gallantry award in the Commonwealth. <br> <strong>Source: USI of India</strong></p>
                    </div>
                </div>

                <p>The VC was originally instituted by Queen Victoria in 1856 to honour acts of valour during the Crimean War. The medal is awarded for an act of the most conspicuous bravery, or some daring or pre-eminent act of valour or self-sacrifice, or extreme devotion to duty in the presence of the enemy. The medal is a bronze cross pattée, 41 mm high and 36 mm wide, bearing the crown of Saint Edward surmounted by a lion, and the inscription ‘for valour’, with a 38 mm wide crimson ribbon. Until 1911, when Indian servicemen became eligible for the VC, the IOM was the highest decoration which Indians serving in the Indian Army could be awarded.</p>
                <p>The first Indian to be awarded the VC was Subedar Khudadad Khan for acts of bravery during the Battle of Ypres in 1914. The official citation of Khan’s award published in the London Gazette dated 7 December 1914 reads:</p>

                <div class="list-classes">
                    <p>His Majesty the KING-EMPEROR has been graciously pleased to approve of the grant of the Victoria Cross to the undermentioned soldier of the Indian Army for conspicuous bravery whilst serving with the Indian Army Corps, British Expeditionary Force: —</p>
                    <p> 4050, Sepoy Khudadad, 129th Duke of Connaught’s Own Baluchis.On 31st October, 1914, at Hollebeke, Belgium, the British Officer in charge of the detachment having been wounded, and the other gun put out of action by a shell, Sepoy Khudadad, though himself wounded, remained working his gun until all the other five men of the gun detachment had been killed.</p>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="image-place">

                            <img src="./assets/img/award/Victoria Cross 1.png" alt="" loading="lazy" class="img-fluid">

                        </div>
                        <span class="img-des" style="color: #D90000; font-weight: 500;">Instituted in 1858 by Queen Victoria to commemorate acts of gallantry in the Crimean War. Indians became eligible to receive the VC in 1911 <br> <strong>Source: USI of India.</strong> </span>
                    </div>

                    <div class="col-md-6">
                        <div class="image-bg">

                            <img src="./assets/img/Sepoy Khudadad Khan.jpg" alt="" loading="lazy" class="img-fluid">

                        </div>
                        <span class="img-des" style="color: #D90000; font-weight: 500;">Sepoy Khudadad Khan, 129th Duke ofConnaught’s Own Baluchis, was the first Indian to receive the Victoria Cross for acts of bravery during the Battle of Ypres in 1914.</span>
                    </div>
                </div>

                <p>The Distinguished Service Order (DSO) was instituted by Queen Victoria in 1886. It is awarded for meritorious or distinguished service by officers of the armed forces during wartime, typically in actual combat. General KS Thimayya, Chief of Army Staff of the Indian Army from 1957–1961 was awarded a DSO during the Second Arakan Campaign in Burma in the Second World War. </p>


                <div class="row">
                    <!-- <div class="col-md-6">
                        <div class="image-place">

                            <img src="" alt="" loading="lazy" class="img-fluid">

                        </div>
                        <span class="center style="color: #D90000; font-weight: 500;"" style="color: #D90000; font-weight: 500;">DSO medal</span>
                    </div> -->

                    <div class="col-md-6">
                        <div class="image-bg">

                            <img src="assets/img/thimayya.jpg" alt="" loading="lazy" class="img-fluid" height="400px">

                        </div>
                        <span class="center style="color: #D90000; font-weight: 500;"" style="color: #D90000; font-weight: 500;">KS Thimayya</span>
                    </div>
                </div>

                <div class="box-table">
                    <h3>Table 1 Victoria Cross Awardees from Indian Army</h3>
                    <table class="table regimental-tb">
                        <thead>
                            <tr>
                                <th scope="col"><b>S.No</b></th>
                                <th scope="col"><b>Name</b></th>
                                <th scope="col"><b>Unit</b></th>
                                <th scope="col"><b>Year</b></th>
                                <th scope="col"><b>Battle</b></th>
                                <th scope="col"><b>Place</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td><b>Khudadad Khan</b></td>
                                <td>129th Duke of Connaught’s Own Baluchis</td>
                                <td>1914</td>
                                <td>First World War</td>
                                <td>Hollebeke, Belgium</td>
                            </tr>

                            <tr>
                                <th scope="row">2</th>
                                <td><b>Darwan Singh Negi</b></td>
                                <td>39th Garhwal Rifles</td>
                                <td>1914</td>
                                <td>First World War</td>
                                <td>Festubert, France</td>
                            </tr>

                            <tr>
                                <th scope="row">3</th>
                                <td><b>Mir Dast</b></td>
                                <td>55th Coke’s Rifles (Frontier Force)</td>
                                <td>1915</td>
                                <td>First World War</td>
                                <td>Wieltje, Belgium</td>
                            </tr>

                            <tr>
                                <th scope="row">4</th>
                                <td><b>Kulbir Thapa</b></td>
                                <td>3rd Queen Alexandra’s Own Gurkha Rifles</td>
                                <td>1915</td>
                                <td>First World War</td>
                                <td>Fauquissart, France</td>
                            </tr>

                            <tr>
                                <th scope="row">5</th>
                                <td><b>Gabbar Singh Negi</b></td>
                                <td>39th Garhwal Rifles</td>
                                <td>1915*</td>
                                <td>First World War</td>
                                <td>Neuve Chapelle, France</td>
                            </tr>


                            <tr>
                                <th scope="row">6</th>
                                <td><b>Shahamad Khan</b></td>
                                <td>89th Punjabis</td>
                                <td>1916</td>
                                <td>First World War</td>
                                <td>Beit Ayeesa, Mesopotamia</td>
                            </tr>


                            <tr>
                                <th scope="row">7</th>
                                <td><b>Lala</b></td>
                                <td>41st Dogras</td>
                                <td>1916</td>
                                <td>First World War</td>
                                <td>El Orah, Mesopotamia</td>
                            </tr>


                            <tr>
                                <th scope="row">8</th>
                                <td><b>Chatta Singh</b></td>
                                <td>9th Bhopal Infantry</td>
                                <td>1916</td>
                                <td>First World War</td>
                                <td>Wadi, Mesopotamia</td>
                            </tr>


                            <tr>
                                <th scope="row">9</th>
                                <td><b>Gobind Singh</b></td>
                                <td>2nd Lancers</td>
                                <td>1917</td>
                                <td>First World War</td>
                                <td>Épehy, France</td>
                            </tr>


                            <tr>
                                <th scope="row">10</th>
                                <td><b>Karanbahadur Rana</b></td>
                                <td>3rd Queen Alexandra’s Own Gurkha Rifles</td>
                                <td>1918</td>
                                <td>First World War</td>
                                <td>El Kefr, Egypt</td>
                            </tr>


                            <tr>
                                <th scope="row">11</th>
                                <td><b>Badlu Singh</b></td>
                                <td>14th Murray’s Jat Lancers</td>
                                <td>1918*</td>
                                <td>First World War</td>
                                <td>River Jordan, Palestine</td>
                            </tr>


                            <tr>
                                <th scope="row">12</th>
                                <td><b>Ishar Singh</b></td>
                                <td>28th Punjabis</td>
                                <td>1921</td>
                                <td>Waziristan campaign (1919-1920)</td>
                                <td>Haidari Kach, India</td>
                            </tr>


                            <tr>
                                <th scope="row">13</th>
                                <td><b>Premindra Bhagat</b></td>
                                <td>Royal Bombay Sappers and Miners</td>
                                <td>1941</td>
                                <td>Second World War</td>
                                <td>Metemma, Abyssinia</td>
                            </tr>

                            <tr>
                                <th scope="row">14</th>
                                <td><b>Richhpal Ram</b></td>
                                <td>6th Rajputana Rifles</td>
                                <td>1941</td>
                                <td>Second World War</td>
                                <td>Keren, Eritrea</td>
                            </tr>


                            <tr>
                                <th scope="row">15</th>
                                <td><b>Gaje Ghale</b></td>
                                <td>5th Gurkha Rifles</td>
                                <td>1943</td>
                                <td>Second World War</td>
                                <td>Chin Hills, Burma</td>
                            </tr>



                            <tr>
                                <th scope="row">16</th>
                                <td><b>Parkash Singh</b></td>
                                <td>8th Punjab Regiment</td>
                                <td>1943</td>
                                <td>Second World War</td>
                                <td>Donbaik, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">17</th>
                                <td><b>Lalbahadur Thapa</b></td>
                                <td>2nd Gurkha Rifles</td>
                                <td>1943</td>
                                <td>Second World War</td>
                                <td>Rass-es-Zouai, Tunisia</td>
                            </tr>


                            <tr>
                                <th scope="row">18</th>
                                <td><b>Chhelu Ram</b></td>
                                <td>6th Rajputana Rifles</td>
                                <td>1943*</td>
                                <td>Second World War</td>
                                <td>Djebel Garci, Tunisia</td>
                            </tr>


                            <tr>
                                <th scope="row">19</th>
                                <td><b>Ganju Lama</b></td>
                                <td>7th Gurkha Rifles</td>
                                <td>1944</td>
                                <td>Second World War</td>
                                <td>Ningthoukhong, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">20</th>
                                <td><b>Tulbahadur Pun</b></td>
                                <td>6th Gurkha Rifles</td>
                                <td>1944</td>
                                <td>Second World War</td>
                                <td>Mogaung, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">21</th>
                                <td><b>Agansing Rai</b></td>
                                <td>5th Gurkha Rifles</td>
                                <td>1944</td>
                                <td>Second World War</td>
                                <td>Bishenpur, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">22</th>
                                <td><b>Bhandari Ram</b></td>
                                <td>10th Baluch Regiment</td>
                                <td>1944</td>
                                <td>Second World War</td>
                                <td>Arakan, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">23</th>
                                <td><b>Kamal Ram</b></td>
                                <td>8th Punjab Regiment</td>
                                <td>1944</td>
                                <td>Second World War</td>
                                <td>Gari River, Italy</td>
                            </tr>


                            <tr>
                                <th scope="row">24</th>
                                <td><b>Nand Singh</b></td>
                                <td>11th Sikh Regiment</td>
                                <td>1944</td>
                                <td>Second World War</td>
                                <td>Maungdaw- Buthidaung Road, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">25</th>
                                <td><b>Umrao Singh</b></td>
                                <td>Royal Indian Artillery</td>
                                <td>1944</td>
                                <td>Second World War</td>
                                <td>Kaladan Valley, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">26</th>
                                <td><b>Yeshwant Ghadge</b></td>
                                <td>5th Mahratta Light Infantry</td>
                                <td>1944*</td>
                                <td>Second World War</td>
                                <td>Upper Tiber Valley, Italy</td>
                            </tr>


                            <tr>
                                <th scope="row">27</th>
                                <td><b>Thaman Gurung</b></td>
                                <td>5th Gurkha Rifles</td>
                                <td>1944*</td>
                                <td>Second World War</td>
                                <td>Monte San Bartolo, Italy</td>
                            </tr>


                            <tr>
                                <th scope="row">28</th>
                                <td><b>Abdul Hafiz Khan</b></td>
                                <td>9th Jat Infantry</td>
                                <td>1944*</td>
                                <td>Second World War</td>
                                <td>Imphal, India</td>
                            </tr>

                            <tr>
                                <th scope="row">29</th>
                                <td><b>Ram Sarup Singh</b></td>
                                <td>1st Punjab Regiment</td>
                                <td>1944*</td>
                                <td>Second World War</td>
                                <td>Kennedy Peak, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">30</th>
                                <td><b>Netrabahadur Thapa</b></td>
                                <td>5th Gurkha Rifles</td>
                                <td>1944*</td>
                                <td>Second World War</td>
                                <td>Bishenpur, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">31</th>
                                <td><b>Sher Bahadur Thapa</b></td>
                                <td>9th Gurkha Rifles</td>
                                <td>1944*</td>
                                <td>Second World War</td>
                                <td>San Marino, Italy</td>
                            </tr>


                            <tr>
                                <th scope="row">32</th>
                                <td><b>Bhanbhagta Gurung</b></td>
                                <td>2nd Gurkha Rifles</td>
                                <td>1945</td>
                                <td>Second World War</td>
                                <td>Snowdon East, Tamandu, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">33</th>
                                <td><b>Lachhiman Gurung</b></td>
                                <td>8th Gurkha Rifles</td>
                                <td>1945</td>
                                <td>Second World War</td>
                                <td>Taungdaw, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">34</th>
                                <td><b>Ali Haidar</b></td>
                                <td>13th Frontier Force Rifles</td>
                                <td>1945</td>
                                <td>Second World War</td>
                                <td>Fusignano, Italy</td>
                            </tr>


                            <tr>
                                <th scope="row">35</th>
                                <td><b>Namdeo Jadhav</b></td>
                                <td>5th Mahratta Light Infantry</td>
                                <td>1945</td>
                                <td>Second World War</td>
                                <td>Senio River, Italy</td>
                            </tr>


                            <tr>
                                <th scope="row">36</th>
                                <td><b>Gian Singh</b></td>
                                <td>15th Punjab Regiment</td>
                                <td>1945</td>
                                <td>Second World War</td>
                                <td>Kamye, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">37</th>
                                <td><b>Fazal Din</b></td>
                                <td>10th Baluch Regiment</td>
                                <td>1945*</td>
                                <td>Second World War</td>
                                <td>Meiktila, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">38</th>
                                <td><b>Karamjeet Judge</b></td>
                                <td>15th Punjab Regiment</td>
                                <td>1945*</td>
                                <td>Second World War</td>
                                <td>Meiktila, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">39</th>
                                <td><b>Sher Shah</b></td>
                                <td>16th Punjab Regiment</td>
                                <td>1945*</td>
                                <td>Second World War</td>
                                <td>Kyeyebyin, Burma</td>
                            </tr>


                            <tr>
                                <th scope="row">40</th>
                                <td><b>Prakash Singh</b></td>
                                <td>13th Frontier Force Rifles</td>
                                <td>1945*</td>
                                <td>Second World War</td>
                                <td>Kanlan Ywathit, Burma</td>
                            </tr>

                        </tbody>
                    </table>
                    <p>Source: Indian Army.<b>Note: </b> * indicates posthumous award.</p>
                </div>

                <h2 class="heading-border">Indian Honours System</h2>
                <p>In the Indian Honours System, the following paragraphs briefly describe on the series and order of gallantry awards. Post-independence, over 4,000 members of the Indian Armed Forces have been awarded for acts of bravery and gallantry during times of war and peace.</p>


                <h2 class="heading-border">Param Vir Chakra</h2>
                <p>The PVC is the highest gallantry award in India and was instituted to replace the VC, which was awarded prior to independence. The medal is made of bronze and is in the form of a circle with a diameter of 1.4 inches. On the obverse side, it features four replicas of Indra’s Vajra, which encloses India’s national emblem. On the reverse, the words ‘PARAM VIR CHAKRA’ are embossed in Hindi and English. Between the words in Hindi and English on the reverse of the medal, lotus flowers are visible on each side. The medal ribbon is purple in colour.</p>

                <p>The PVC is awarded for most conspicuous bravery or some daring or pre-eminent act of valour or self-sacrifice, in the presence of the enemy, whether on land, at sea, or in the air. The decoration may be awarded posthumously. Major Somnath Sharma of 4 KUMAON was the first awardee of the PVC (posthumous) on 26 January 1950. Only 21 PVCs have been awarded since 1947 of which 20 have been earned by the Indian Army and one by the Indian Air Force.</p>


                <div class="row">
                    <div class="col-md-6">
                        <div class="image-place">

                            <img src="assets/img/award/madel1.png" alt="" loading="lazy" class="img-fluid">

                        </div>
                        <span class="center" style="color: #D90000; font-weight: 500;">Param Vir Chakra<br><strong>Source: USI of India</strong></span>
                    </div>

                    <div class="col-md-6">
                        <div class="image-bg">
                            <img src="assets/img/Major Somnath.jpg" alt="" loading="lazy" class="img-fluid" height="400px">
                        </div>

                        <span class="img-des" style="color: #D90000; font-weight: 500;">Major Somnath Sharma of 4 KUMAON was awarded the first PVC (posthumous) on 26 January 1950. </span>
                    </div>
                </div>



                <h2 class="heading-border">Mahavir Chakra</h2>
                <p>The MVC is the second-highest gallantry award in the country, awarded for bravery in the face of enemy. The award replaced the former British DSO. The MVC fea- tures a circular medal of 1.4 inches diameter, which is made of standard silver. On the obverse, the medal features a five-pointed heraldic star along with the national emblem embossed on the domed central piece. On the reverse, the medal features the words ‘MAHA VIR CHAKRA’ in Hindi and English, separated by lotus flowers. The medal ribbon is half-white and half-orange.</p>

                <p>The medal is awarded for gallantry in the presence of the enemy on land, at sea or in the air. The decoration may be awarded posthumously. Lt Col Dewan Ranjit Rai of 1 SIKH was the first recipient (posthumous) of the Maha Vir Chakra for his courageous actions in the 1947–48 India-Pakistan War.</p>


                <div class="row">
                    <div class="col-md-6">
                        <div class="image-place">

                            <img src="assets/img/award/madel2.png" alt="" loading="lazy" class="img-fluid">

                        </div>
                        <span class="center" style="color: #D90000; font-weight: 500;">Mahavir Chakra<br><strong>Source: USI of India</strong></span>
                    </div>

                    <!-- <div class="col-md-6">
                        <div class="image-place">

                            <img src="" alt="" loading="lazy" class="img-fluid">

                        </div>
                        <span class="center" style="color: #D90000; font-weight: 500;">Lt Col Dewan Ranjit Rai</span>
                    </div> -->
                </div>




                <h2 class="heading-border">Vir Chakra</h2>
                <p>The VrC is the third-highest gallantry award in India. The medal, made of standard silver, is circular in shape with a diameter of 1.4 inches. It features a five-pointed heraldic star and the state emblem along with the motto at the centre on the obverse. On the reverse, the words ‘Vir Chakra’ are written in Hindi and English and sepa- rated by a flower on each side. The medal ribbon is half-blue and half-orange. The VrC is awarded for acts of gallantry in the presence of the enemy, whether on land or at sea or in the air, and may be awarded posthumously.</p>

                <div class="row">
                    <div class="col-md-6">
                        <div class="image-place">

                            <img src="assets/img/award/madel3.png" alt="" loading="lazy" class="img-fluid">

                        </div>
                        <span class="center" style="color: #D90000; font-weight: 500;">Vir Chakra <br><strong>Source: USI of India</strong></span>
                    </div>

                </div>

                <h2 class="heading-border">Ashok Chakra</h2>
                <p>The Ashok Chakra was instituted on 4 January 1952 and renamed on 27 January 1967. It is the highest peacetime gallantry award in the country that recognises most conspicuous bravery or some act of daring or pre-eminent act of valour or self- sacrifice other than than in the face of the enemy, and can be awarded posthumously. The Ashok Chakra can be awarded to both uniformed personnel as well as civilians. On the obverse, the chakra features a replica of the Ashoka Chakra encircled by a lotus garland. On the reverse, words ‘Ashoka Chakra’ are imprinted in English and Hindi, with lotus flower between them on each side. The medal ribband is dark-green silk of 3.2 cm with an orange vertical line through the centre. </p>


                <div class="row">
                    <div class="col-md-6">
                        <div class="image-place">

                            <img src="assets/img/award/madel4.png" alt="" loading="lazy" class="img-fluid">

                        </div>
                        <span class="center" style="color: #D90000; font-weight: 500;">Ashok Chakra<br><strong>Source: USI of India</strong></span>
                    </div>

                </div>


                <h2 class="heading-border">Kirti Chakra</h2>
                <p>The Kirti Chakra is the second-highest peacetime gallantry award in peacetime and was first instituted on 4 January 1952 as the Ashoka Chakra Class-II. It was redesignated as the Kirti Chakra on 27 Jan 1967. The silver medal is circular with a diameter of 1.38 inches, with rims on both sides. The obverse features the Ashoka Chakra enclosed by a lotus wreath, and on the reverse the medal is imprinted with the words ‘Kirti Chakra’ in English and Hindi. The words are separated by a lotus flower on each side. The ribband is green, of 3.2 inches width with two orange vertical lines divide the riband into three equal parts. The medal is awarded for conspicuous gallantry otherwise than in the face of the enemy. The decoration may be awarded posthumously.</p>


                <div class="row">
                    <div class="col-md-6">
                        <div class="image-place">

                            <img src="assets/img/award/madel5.png" alt="" loading="lazy" class="img-fluid">

                        </div>
                        <span class="center" style="color: #D90000; font-weight: 500;">Kirti Chakra<br><strong>Source: USI of India</strong></span>
                    </div>

                </div>


                <h2 class="heading-border">Shaurya Chakra</h2>
                <p>The Shaurya Chakra was instituted on 4 January 1952 as the Ashoka Chakra Class-III and renamed on 27 January 1967. It is the third-highest peacetime gallantry award, awarded for gallantry otherwise than in the face of the enemy. The decoration may be awarded posthumously. The Shaurya Chakra medal is circular, with a diameter of 1.4 cm made of bronze with the obverse similar to that of the Ashoka Chakra. On the reverse, the words ‘Shaurya Chakra’ are imprinted in Hindi and English, and separated by lotus flowers. The 3.2 inch wide riband accompanying the medal is divided into four equal parts by three orange vertical lines.</p>


                <div class="row">
                    <div class="col-md-6">
                        <div class="image-place">

                            <img src="assets/img/award/madel6.png" alt="" loading="lazy" class="img-fluid">

                        </div>
                        <span class="center" style="color: #D90000; font-weight: 500;">Shaurya Chakra<br><strong>Source: USI of India</strong></span>
                    </div>

                </div>


                <div class="box-table">
                    <h3>Table 2 Param Vir Chakra Awardees from the Indian Army</h3>
                    <table class="table regimental-tb">
                        <thead>
                            <tr>
                                <th scope="col"><b>S.No</b></th>
                                <th scope="col"><b>Name</b></th>
                                <th scope="col"><b>Rank</b></th>
                                <th scope="col"><b>Regiment</b></th>
                                <th scope="col"><b>Date</b></th>
                                <th scope="col"><b>Battle</b></th>
                                <th scope="col"><b>Place</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Somnath Sharma</td>
                                <td>Major</td>
                                <td>Kumaon Regiment</td>
                                <td>3 November 1947*</td>
                                <td>Battle of Badgam</td>
                                <td>Badgam, Jammu and Kashmir, India</td>
                            </tr>

                            <tr>
                                <th scope="row">2</th>
                                <td>Jadunath Singh</td>
                                <td>Naik</td>
                                <td>Rajput Regiment</td>
                                <td>6 February 1948*</td>
                                <td>Indo- Pakistani War of 1947</td>
                                <td>Naushera, Jammu and Kashmir, India</td>
                            </tr>

                            <tr>
                                <th scope="row">3</th>
                                <td>Rama Raghoba Rane</td>
                                <td>Second Lieutenant</td>
                                <td>Bombay Sappers</td>
                                <td>8 April 1948</td>
                                <td>Indo- Pakistani War of 1947</td>
                                <td>Naushera, Jammu and Kashmir, India</td>
                            </tr>

                            <tr>
                                <th scope="row">4</th>
                                <td>Piru Singh</td>
                                <td>Company Havildar Major</td>
                                <td>Rajputana Rifles</td>
                                <td>17 July 1948*</td>
                                <td>Indo- Pakistani War of 1947</td>
                                <td>Tithwal, Jammu and Kashmir, India</td>
                            </tr>

                            <tr>
                                <th scope="row">5</th>
                                <td>Karam Singh</td>
                                <td>Lance Naik</td>
                                <td>Sikh Regiment</td>
                                <td>13 October 1948</td>
                                <td>Indo- Pakistani War of 1947</td>
                                <td>Tithwal, Jammu and Kashmir, India</td>
                            </tr>


                            <tr>
                                <th scope="row">6</th>
                                <td>Gurbachan Singh Salaria</td>
                                <td>Captain</td>
                                <td>1 Gorkha Rifles[d]</td>
                                <td>5 December 1961*</td>
                                <td>Congo Crisis</td>
                                <td>Élisabethville, Katanga, Congo</td>
                            </tr>


                            <tr>
                                <th scope="row">7</th>
                                <td>Dhan Singh Thapa</td>
                                <td>Major</td>
                                <td>8 Gorkha Rifles</td>
                                <td>20 October 1962</td>
                                <td>Sino- Indian War</td>
                                <td>Ladakh, Jammu and Kashmir, India</td>
                            </tr>


                            <tr>
                                <th scope="row">8</th>
                                <td>Joginder Singh</td>
                                <td>Subedar</td>
                                <td>Sikh Regiment</td>
                                <td>23 October 1962*</td>
                                <td>Sino- Indian War</td>
                                <td>Tongpen La, North-East Frontier Agency, India</td>
                            </tr>


                            <tr>
                                <th scope="row">9</th>
                                <td>Shaitan Singh</td>
                                <td>Major</td>
                                <td>Kumaon Regiment</td>
                                <td>18 November 1962*</td>
                                <td>Sino- Indian War</td>
                                <td>Rezang La, Jammu and Kashmir, India</td>
                            </tr>


                            <tr>
                                <th scope="row">10</th>
                                <td>Abdul Hamid</td>
                                <td>Company Quarter Master Havildar</td>
                                <td>The Grenadiers</td>
                                <td>10 September 1965*</td>
                                <td>Battle of Asal Uttar</td>
                                <td>Khemkaran, India</td>
                            </tr>


                            <tr>
                                <th scope="row">11</th>
                                <td>Ardeshir Tarapore </td>
                                <td>Lieutenant Colonel</td>
                                <td>Poona Horse</td>
                                <td>11 September 1965*</td>
                                <td>Battle of Chawinda</td>
                                <td>Phillora, Sialkot, Pakistan</td>
                            </tr>


                            <tr>
                                <th scope="row">12</th>
                                <td>Albert Ekka</td>
                                <td>Lance Naik</td>
                                <td>Brigade of the Guards</td>
                                <td>3 December 1971*</td>
                                <td>Battle of Hilli</td>
                                <td>Gangasagar, Agartala, India</td>
                            </tr>


                            <tr>
                                <th scope="row">13</th>
                                <td>Arun Khetarpal</td>
                                <td>Second Lieutenant</td>
                                <td>Poona Horse</td>
                                <td>16 December 1971*</td>
                                <td>Battle of Basantar</td>
                                <td>Barapind- Jarpal, Shakargarh, Pakistan</td>
                            </tr>

                            <tr>
                                <th scope="row">14</th>
                                <td>Hoshiar Singh Dahiya</td>
                                <td>Major</td>
                                <td>The Grenadiers</td>
                                <td>17 December 1971</td>
                                <td>Battle of Basantar</td>
                                <td>Basantar River, Shakargarh, Pakistan</td>
                            </tr>


                            <tr>
                                <th scope="row">15</th>
                                <td>Bana Singh</td>
                                <td>Naib Subedar</td>
                                <td>Jammu and Kashmir Light Infantry</td>
                                <td>23 May 1987</td>
                                <td>Operation Rajiv</td>
                                <td>Siachen Glacier, Jammu and Kashmir, India</td>
                            </tr>



                            <tr>
                                <th scope="row">16</th>
                                <td>Ramaswamy Parameshwaran</td>
                                <td>Major</td>
                                <td>Mahar Regiment </td>
                                <td>25 November 1987*</td>
                                <td>Operation Pawan</td>
                                <td>Sri Lanka</td>
                            </tr>


                            <tr>
                                <th scope="row">17</th>
                                <td>Manoj Kumar Pandey</td>
                                <td>Lieutenant</td>
                                <td>11 Gorkha Rifles</td>
                                <td>3 July 1999*</td>
                                <td>Operation Vijay</td>
                                <td>Khaluber / Juber Top, Jammu and Kashmir, India</td>
                            </tr>


                            <tr>
                                <th scope="row">18</th>
                                <td>Yogendra Singh Yadav</td>
                                <td>Grenadier</td>
                                <td>The Grenadiers</td>
                                <td>4 July 1999</td>
                                <td>Battle of Tiger Hill</td>
                                <td>Tiger Hill, Jammu and Kashmir, India</td>
                            </tr>


                            <tr>
                                <th scope="row">19</th>
                                <td>Sanjay Kumar</td>
                                <td>Rifleman</td>
                                <td>Jammu and Kashmir Rifles</td>
                                <td>5 July 1999</td>
                                <td>Kargil War</td>
                                <td>Kargil, Jammu and Kashmir, India</td>
                            </tr>


                            <tr>
                                <th scope="row">20</th>
                                <td>Vikram Batra</td>
                                <td>Captain</td>
                                <td>Jammu and Kashmir Rifles</td>
                                <td>7 July 1999*</td>
                                <td>Operation Vijay</td>
                                <td>Kargil, Jammu and Kashmir, India</td>
                            </tr>

                        </tbody>
                    </table>
                    <p>Source: Indian Army.<b>Note: </b> * indicates posthumous award.</p>
                </div>



                <div class="box-table">
                    <h3>Table 3 Ashok Chakra Awardees from the Indian Army</h3>
                    <table class="table regimental-tb">

                        <tr>
                            <th>S/No</th>
                            <th>Year</th>
                            <th>Rank</th>
                            <th>Name</th>
                            <th>Organisation</th>
                        </tr>
                        <tr>
                            <td>1.</td>
                            <td>1952</td>
                            <td>Naik</td>
                            <td>Narbahadur Thapa</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>1952</td>
                            <td>Havildar</td>
                            <td>Bachittar Singh*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>1956</td>
                            <td>Lance Naik</td>
                            <td>Sundar Singh</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td>1957</td>
                            <td>Lieutenant Colonel</td>
                            <td>Jagannath Raoji Chitnis*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>5.</td>
                            <td>1957</td>
                            <td>Second Lieutenant</td>
                            <td>Pollur Mutthuswamy Raman*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>6.</td>
                            <td>1957</td>
                            <td>Havildar</td>
                            <td>Joginder Singh*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>7.</td>
                            <td>1958</td>
                            <td>Captain</td>
                            <td>Eric James Tucker*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>8.</td>
                            <td>1962</td>
                            <td>Subedar Major</td>
                            <td>Kharka Bahadur Limbu*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>9.</td>
                            <td>1962</td>
                            <td>Captain</td>
                            <td>Man Bahadur Rai</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>10.</td>
                            <td>1962</td>
                            <td>Cadet</td>
                            <td>Shri Amrat Lal Mehra</td>
                            <td>NCC</td>
                        </tr>
                        <tr>
                            <td>11.</td>
                            <td>1969</td>
                            <td>Captain</td>
                            <td>Jas Ram Singh</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>12.</td>
                            <td>1972</td>
                            <td>Captain</td>
                            <td>Ummed Singh Mahra*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>13.</td>
                            <td>1974</td>
                            <td>Naib Subedar</td>
                            <td>Gurnam Singh*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>14.</td>
                            <td>1981</td>
                            <td>Second Lieutenant</td>
                            <td>Cyrus Addie Pithawalla</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>15.</td>
                            <td>1985</td>
                            <td>Lance Havildar</td>
                            <td>Chhering Mutup</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>16.</td>
                            <td>1985</td>
                            <td>Naik</td>
                            <td>Nirbhay Singh*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>17.</td>
                            <td>1985</td>
                            <td>Naik</td>
                            <td>Bhawani Datt Joshi*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>18.</td>
                            <td>1985</td>
                            <td>Lieutenant</td>
                            <td>Ram Prakash Roperia*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>19.</td>
                            <td>1985</td>
                            <td>Captain</td>
                            <td>Jasbir Singh Raina</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>20.</td>
                            <td>1985</td>
                            <td>Major</td>
                            <td>Bhukant Misra*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>21.</td>
                            <td>1992</td>
                            <td>Captain</td>
                            <td>Sandeep Sankhla*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>22.</td>
                            <td>1993</td>
                            <td>Second Lieutenant</td>
                            <td>Rakesh Singh*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>23.</td>
                            <td>1994</td>
                            <td>Colonel</td>
                            <td>Neelakantan Jayachandran Nair*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>24.</td>
                            <td>1995</td>
                            <td>Major</td>
                            <td>Rajiv Kumar Joon*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>25.</td>
                            <td>1995</td>
                            <td>Subedar</td>
                            <td>Sujjan Singh*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>26.</td>
                            <td>1995</td>
                            <td>Lieutenant Colonel</td>
                            <td>Harsh Uday Singh Gaur*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>27.</td>
                            <td>1996</td>
                            <td>Captain</td>
                            <td>Arun Singh Jasrotia*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>28.</td>
                            <td>1997</td>
                            <td>Second Lieutenant</td>
                            <td>Puneet Nath Datt*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>29.</td>
                            <td>1997</td>
                            <td>Lieutenant Colonel</td>
                            <td>Shanti Swarup Rana*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>30.</td>
                            <td>2000</td>
                            <td>Major</td>
                            <td>Sudhir Kumar Walia*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>31.</td>
                            <td>2002</td>
                            <td>Subedar</td>
                            <td>Surinder Singh*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>32.</td>
                            <td>2002</td>
                            <td>Naik</td>
                            <td>Rambeer Singh Tomar*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>33.</td>
                            <td>2003</td>
                            <td>Subedar Major</td>
                            <td>Suresh Chand Yadav *</td>
                            <td>National Security Guard</td>
                        </tr>
                        <tr>
                            <td>34.</td>
                            <td>2004</td>
                            <td>Lieutenant</td>
                            <td>Triveni Singh*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>35.</td>
                            <td>2004</td>
                            <td>Paratrooper</td>
                            <td>Sanjog Chhetri*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>36.</td>
                            <td>2007</td>
                            <td>Captain</td>
                            <td>Radhakrishnan Nair Harshan*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>37.</td>
                            <td>2007</td>
                            <td>Naib Subedar</td>
                            <td>Chuni Lal*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>38.</td>
                            <td>2007</td>
                            <td>Colonel</td>
                            <td>Vasanth Venugopal*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>39.</td>
                            <td>2008</td>
                            <td>Major</td>
                            <td>Dinesh Raghu Raman*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>40.</td>
                            <td>2009</td>
                            <td>Havildar</td>
                            <td>Bahadur Singh Bohra*</td>
                            <td>Indian Army</td>
                        </tr>

                        <tr>
                            <td>41.</td>
                            <td>2009</td>
                            <td>Havildar</td>
                            <td>Gajender Singh Bisht*</td>
                            <td>NSG</td>
                        </tr>

                        <tr>
                            <td>42.</td>
                            <td>2009</td>
                            <td>Major</td>
                            <td>Sandeep Unnikrishnan*</td>
                            <td>NSG</td>
                        </tr>

                        <tr>
                            <td>43.</td>
                            <td>2009</td>
                            <td>Colonel</td>
                            <td>Jojan Thomas*</td>
                            <td>Indian Army</td>
                        </tr>

                        <tr>
                            <td>44.</td>
                            <td>2010</td>
                            <td>Captain</td>
                            <td>Mithlesh Kumar Sharma*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>45.</td>
                            <td>2011</td>
                            <td>Lieutenant</td>
                            <td>Mohit Sharma*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>46.</td>
                            <td>2012</td>
                            <td>Naib Subedar</td>
                            <td>Anup Thapa*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>47.</td>
                            <td>2014</td>
                            <td>Major</td>
                            <td>Rajesh Singh Adhikari*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>48.</td>
                            <td>2016</td>
                            <td>Colonel</td>
                            <td>Vikrant Prasher*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>49.</td>
                            <td>2017</td>
                            <td>Major</td>
                            <td>Shrikant Yadav*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>50.</td>
                            <td>2018</td>
                            <td>Major</td>
                            <td>Prashant Siwach*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>51.</td>
                            <td>2018</td>
                            <td>Major</td>
                            <td>Rishi Dhanu Yadav*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>52.</td>
                            <td>2018</td>
                            <td>Naib Subedar</td>
                            <td>Vijay Kumar*</td>
                            <td>Indian Army</td>
                        </tr>
                        <tr>
                            <td>53.</td>
                            <td>2019</td>
                            <td>Lance Naik</td>
                            <td>Sandeep Singh*</td>
                            <td>Indian Army</td>
                        </tr>



                    </table>
                    <p>Source: Indian Army.<b>Note: </b> * indicates posthumous award.</p>
                </div>



                <!-- <div class="note">
                    <p>Notes</p>
                    <ul>
                        <li>Rana TS Chhina, The Last Post: Indian War Memorials around the World, New Delhi: USI, 2014, p. 1.</li>

                        <li>Ibid.</li>

                        <li>Ibid.</li>

                    </ul>

                </div> -->
            </div>
        </div>
    </div>
</section>




<?php include('footer.php'); ?>