<?php

$main ="wars";
$page="post-indeep";



include('header.php')?>


    <section class="post-independence-war-banner"
        style="background-image: url(./assets/img/post-independence-war-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Post-Independent Wars</h1>
        </div>
    </section>

    <section class="post-independence-war">
        <div class="container">
            <div class="row">
                <div class="col-6 full-wdt">
                    <div class="war-box">
                        <div class="war-image">
                            <img src="./assets/img/post-war1.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="war-details">
                            <h2 class="head">1947-48 Kashmir Conflict</h2>
                            <p class="text">The 1948 (or First) Arab–Israeli War was the second and final stage of the
                                1947–1949 Palestine war. It formally began following the end ...</p>
                            <a href="post-indep-wars-details.php">
                                <div class="war-right-arrow">
                                    <h3 class="sm-text">Read More</h3>
                                    <img src="./assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 full-wdt">
                    <div class="war-box">
                        <div class="war-image">
                            <img src="./assets/img/post-war2.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="war-details">
                            <h2 class="head">1962 Sino-Indian Conflict </h2>
                            <p class="text">The Sino-Indian War between China and India occurred in October–November
                                1962. A disputed Himalayan border was the main cause of the war...</p>
                            <a href="1962-sino-indian-war.php">
                                <div class="war-right-arrow">
                                    <h3 class="sm-text">Read More</h3>
                                    <img src="./assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 full-wdt">
                    <div class="war-box">
                        <div class="war-image">
                            <img src="./assets/img/post-war3.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="war-details">
                            <h2 class="head">1965 India–Pakistan War</h2>
                            <p class="text">The 1948 (or First) Arab–Israeli War was the second and final stage of the
                                1947–1949 Palestine war. It formally began following the end ...</p>
                            <a href="post-indep-wars-details.php">
                                <div class="war-right-arrow">
                                    <h3 class="sm-text">Read More</h3>
                                    <img src="./assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 full-wdt">
                    <div class="war-box">
                        <div class="war-image">
                            <img src="./assets/img/post-war4.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="war-details">
                            <h2 class="head">1971 India–Pakistan War</h2>
                            <p class="text">The Indo-Pakistani War of 1971 was a military confrontation between India
                                and Pakistan that occurred during the Bangladesh Liberation War...</p>
                            <a href="post-indep-wars-details.php">
                                <div class="war-right-arrow">
                                    <h3 class="sm-text">Read More</h3>
                                    <img src="./assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <?php include('footer.php')?>

