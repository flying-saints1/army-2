<?php 
/*

$currentDomain = $_SERVER['SERVER_NAME']; 

if(isset($_SERVER['HTTPS'])){
    $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
}
else{
    $protocol = 'http';
}

$currentDomain = $protocol.'://'.$currentDomain.'/indian-army';
*/
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Indian-Army</title>

   <?php wp_head(); ?>
</head>
<body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v14.0&appId=276105052412931&autoLogAppEvents=1" nonce="F4pwYSmq"></script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-62a1a8f6fcbbe849"></script>

<header>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-lg">
                    <div class="nav-swaper">
                    <a class="navbar-brand order-2 order-lg-1" href="<?php echo get_site_url(); ?>"><img src="<?php echo get_field('header_logo','option');?>" alt=""></a>
                        <button class="navbar-toggler order-1 order-lg-4" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fa-solid fa-bars"></i>
                        </button>
                        <a href="" class="order-3 order-lg-3 d-none search-icon-header" ><i class="fa-solid fa-magnifying-glass search-icon  "></i></a>
                        </div>

                    <!--<a href=""><i class="fa-solid fa-magnifying-glass search-icon  block-dis"></i></a>-->
                    <?php
                        // Header Menu
                        if (function_exists('register_primary_menu')) :
                            wp_nav_menu(array(
                                'theme_location' => 'Header',
                                'menu_class' => 'navbar-nav ml-auto',
                                'container_id' => 'navbarSupportedContent',
                                'container_class' => 'collapse navbar-collapse',
                                'add_li_class'  => 'nav-item',
                                'add_a_class'  => 'nav-link',
                            ));
                        endif;
                    ?>

                    <a href=""><i class="fa-solid fa-magnifying-glass search-icon  "></i></a>
                    
                    
                </nav>
            </div>
        </div>
    </div>
</header>


