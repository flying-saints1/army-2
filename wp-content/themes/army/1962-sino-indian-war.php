<?php

$main = "wars";
$page = "post-indeep";
$subPage = "sino-war";
$sidebar = "gallery";

include('header.php'); ?>


<section class="post-independence-war-banner" style="background-image: url(./assets/img/post-independence-war-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">1962 Sino-Indian War</h1>
    </div>
</section>


<section class="indep-war" id="faq-section">
    <div class="container">
        <div class="row">
            <?php include('sidebar/post-indeep-sidebar.php'); ?>

            <div class="col-md-9">
                <div class="content">
                    <img src="assets/img/post-indep-war.jpg" width="100%" class="img-fluid res-image" alt="" loading="lazy">
                    <h3 class="sino-war-text">1962 Sino-Indian Conflict</h3>
                </div>


                <div id="about-war" class="accordion-details">
                    <h3>About war</h3>

                    <div class="war-details">
                        <h4>Introduction</h4>
                        <p>The People’s Republic of China (China) annexed Tibet in 1950–51 and with that an independent India acquired a new neighbour. The two countries also shared a 4,000 km long land border which was neither delineated nor mutually defined. India was quick to recognise Chinese sovereignty over Tibet and hoped that the two countries would have friendly relations. The ‘Agreement on Trade and Intercourse between the Tibet Region of China and India’, commonly known as the Panchsheel Agreement, signed on 29 April 1954, was the high point of friendly relations and gave rise to the slogan Hindi-Chini, Bhai-Bhai. Through this Agreement, India also gave up the special rights and privileges in Tibet which it had inherited from British India without negotiating anything substantial in return. There was no reference to the unsettled boundary. India presumed that the absence of the same meant Chinese acceptance of India’s perception of the boundary. That this was a fallacy as was to be proved soon after.</p>

                        <p>It did not take long for the first Chinese incursion into Barahoti in Uttar Pradesh (now Uttarakhand), which occurred in June 1954. From then on, there were hundreds of incursions leading to the conflict in 1962. More than a thousand memoranda, notes and letters were exchanged between the two governments over the next 10 years. These have been published by the Government of India in a series of white papers. Gradually, these diplomatic exchanges became shriller and accusatory in nature.</p>

                        <p>The first bloodshed occurred at Longju in erstwhile North East Frontier Agency or NEFA (now Arunachal Pradesh) on 25 August 1959. While the fallout of this incident was still being resolved, another, more serious, incident took place on 20/21 October 1959 in the area of Kongka Pass in Ladakh. An Indian Police (now the Central Reserve Police Force or CRPF) patrol was ambushed by the Chinese border guards in which 10 Indian policemen were killed and nine were taken prisoner. The net result of these incidents was that the defence of India’s northern borders—until then the responsibility of Indian Police under the Ministry of Home Affairs and Assam Rifles under the Ministry of External Affairs—was handed over to the Indian Army in 1959.</p>


                    </div>

                    <div class="war-details">
                        <h4>Nehru’s Forward Policy and Mao’s Armed Co-existence</h4>
                        <p>At the beginning of 1960, Prime Minister Jawaharlal Nehru decided to make one more effort to resolve the boundary dispute through talks. On 5 February 1960, he invited Chinese Premier Zhou Enlai to visit New Delhi to explore avenues that might lead to a peaceful settlement. The Nehru-Zhou talks were held in New Delhi from 19–25 April 1960, but were unsuccessful. After the failure of these talks, China started to establish additional posts on Indian Territory, inside their ‘Claim Line’. In order to check this unrelenting forward movement by Chinese troops, Nehru issued orders on 2 November 1961 for certain measures to be adopted—and that came to be known as the ‘Forward Policy’. In essence, it involved the establishment of forward posts, close to the Chinese posts previously established, to prevent any further encroachment into Indian Territory. The Chinese responded by establishing more of their own posts, at times encircling the Indian posts. Mao defined this policy as ‘armed co-existence’. Thus, Indian and Chinese posts virtually came to be interlocked with each other, especially in the Ladakh region.</p>

                    </div>

                    <div class="war-details">
                        <h4>End of Diplomacy</h4>
                        <p>By the end of 1961, Chinese had concluded that their objective to prevent any fur- ther Indian advances without bloodshed was not being met. They began to consider whether a sudden blow might force India to the negotiating table. On 26 July 1962, the Government of India offered to send a ministerial level delegation to Beijing to discuss, without preconditions, all bilateral problems and disputes. In a meeting with the Indian Charge d’Affaires, PK Banerjee on 4 August 1962, Zhou Enlai responded that China was willing to hold talks but only on Chinese terms. By early September, Beijing was warning New Delhi that if India played with fire, it would be consumed by fire.</p>
                    </div>


                    <div class="war-details">
                        <h4>The Spark </h4>
                        <p>In early June 1962, India had established an Assam Rifles post called ‘Dhola’ in the area of Bridge III on the southern bank of Namka Chu. Chinese reacted on 8 September and surrounded the Indian post, warning the troops to vacate the area as that was Chinese territory. For 12 days, the troops of both sides remained eye ball to eye ball in confrontation but there was no firing by either side. However, an armed clash took place on 21 September 1962 on the Namka Chu resulting in casualties on both sides. Thereafter, the events moved fast. On the Indian side, 7th Infantry Brigade moved forward from Tawang to Namka Chu with orders to evict the Chinese from Thagla Ridge. On the Chinese side, the attacking troops were already on the move to their assembly areas. The stage was now set for the events that were to unfold with a full-scale Chinese offensive on 20 October 1962.</p>
                    </div>
                </div>

                <div id="chinese" class="accordion-details">
                    <h3>Chinese War Aim and Grand Strategy</h3>
                    <div class="war-details">

                        <p>The overall aim set by the Central Military Commission (CMC) for the People’s Liberation Army (PLA) was to inflict a crushing military defeat on Indian troops deployed on the border. While the real strategic aim of the Chinese lay in the Western Sector, the main military effort was to be directed in the Eastern Sector, specifically against the Kameng Frontier Division because the terrain here facilitated employment of large forces.</p>


                        <p>Further, the operations in the Western Sector were directed at securing the 1960 Claim Line but the aim set for the PLA forces in the Kameng Frontier Division was to annihilate the Indian forces deployed there. Marshal Liu Bocheng, a seasoned PLA commander who had successfully fought the Japanese and the Kuomintang Nationalist Army, was assigned the responsibility to formulate military strategy and approve operational plans for the campaign.</p>


                        <h4>Western Sector: War in Ladakh</h4>



                        <div class="war-2-map-2">
                            <img src="assets/img/gallery/Aksai-Chin.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">1962 Aksai Chin</p>
                        </div>


                        <p>Soon after their annexation of Xinjiang in November 1949 and Tibet in 1950–51, the Chinese set about developing a road to connect Xinjiang with Tibet. This road, called Highway 215, was announced as having been completed in September 1957 and cut across about 160 km of Indian Territory on the Aksai Chin plateau. The construction of the highway in this remote and inaccessible region of India took New Delhi by surprise.</p>


                        <p>The Chinese soon began to establish posts to provide security to the above high- way. In turn, India too established posts to stake claim to its rightful border and to prevent the Chinese from further encroaching upon Indian Territory. This policy brought the two forces in eye ball to eye ball confrontation, which finally led to the armed conflict in October 1962.</p>

                    </div>
                    <div class="war-details">
                        <h4>Indian Dispositions on the Eve of the War</h4>
                        <p>India had deployed the 114 Infantry Brigade comprising six battalions from North to South, namely, 14 J&K Militia, 5 JAT, 1/8 Gorkha Rifles (GR), 13 KUMAON, 1 JAT, and 7 J&K Militia for the defence of Ladakh. A large proportion of the Indian troop strength was distributed over 70 posts across a wide geographical area, aimed at showing the Indian flag.</p>
                    </div>


                    <div class="war-details">
                        <h4>China Prepares for the Onslaught</h4>
                        <p>China had started active preparations for the attack from about July of 1962. Specific instructions were issued by the CMC to avoid any premature commencement of hostilities while responding to Indian actions. The overall command and control were to be exercised by the Xinjiang Military Command through a forward HQ at Kangshiwar (south of Kunlun Mountain range). The whole area of operations was divided into four defence areas, from North to South as under:</p>

                        <p>1. Tianwendian Defence Area.</p>


                        <p>2. Heweitan Defence Area.</p>


                        <p>3. Kongka Pass Defence Area.</p>


                        <p>4. Ali Defence Area (part of Tibet Region).</p>

                        <p>Chinese troops in the area of operations consisted of the 2nd Infantry Regiment which was deployed North of Pangong Tso, and the Ali Detachment (a regiment sized force) which was deployed South of Pangong Tso and in the Indus Valley. Prior to the operations, Xinjiang Military Command inducted additional troops: two infantry battalions, a cavalry regiment and other combat support elements from the 4th Infantry Division based in Xinjiang. Thus, the total troops amounted to roughly a division strength but these were strong in terms of reconnaissance, artillery and engineer elements. The attacking troops were primarily based on two infantry battalions from the 4th Infantry Division, which were recycled time and again after rest and reorganisation.</p>


                        <div class="war-2-map-2">
                            <img src="assets/img/25 pounder gun in action 1.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">25 pounder gun in action <br> <strong>Source: MoD, DPR.</strong> </p>
                        </div>

                    </div>


                    <div class="war-details">
                        <h4>The Chinese Plan</h4>
                        <p>Broadly, the Chinese planned to eliminate all the 43 Indian posts which they perceived to be intrusions across their 1960 Claim Line in Ladakh. However, priority was to be given to the two northern defence areas of ‘Tianwendian Defence Area’ (Daulat Beg Oldi [DBO] Sub-sector) and ‘Heweitan Defence Area’ (Galwan River Valley Sub-sector). D Day was scheduled for 20 October 1962. The following two objectives were selected for the initial attacks on that day:</p>

                        <p>1. Point 5270, a commanding hill top in the DBO Sub-sector which was held by a company less a platoon of 14 J&K Militia with a strength of 62 All Ranks.</p>

                        <p>2. Galwan Post was considered as the most dangerous position held by the Indian Army in the ‘Heweitan Defence Area’ because it cut off rearward communication of three Chinese posts. It was held by a company of 5 JAT with strength of 68 All Ranks.</p>
                    </div>

                    <div class="war-details">
                        <h4>Opening Attacks: 20 October 1962</h4>
                        <p>The Chinese built up a numerical superiority of about 10:1 and fire power superiority of 7:1 before launching any major attack. They also blocked any route of withdrawal available to the defenders. As they had been in a state of armed co-existence with the Indian posts for a long time, they had intimate knowledge of the Indian strength and dispositions. The attacks commenced at first light with intense artillery bombardment for about 30 minutes and the battle was over in two to three hours. Indian troops suffered heavy casualties; for instance, at Point 5270, 14 J&K Militia suffered 42 killed and 20 wounded/taken prisoners of war (POW). Similarly, at the Galwan post, 5 JAT suffered 36 killed and 32 wounded/taken POW.</p>

                        <p>By 24 October, all the forward posts established in the Chip Chap and Nacho Chu River Valleys were either captured or withdrawn. DBO was also abandoned. Similarly, in the Galwan Valley, the Chinese had managed to remove all the six Indian posts established on the northern and southern banks of Galwan River in about 72 hours. Some of these posts were withdrawn even without being attacked as these were neither, tactically viable nor, logistically sustainable.</p>
                    </div>


                    <div class="war-details">
                        <h4>Battles on the Pangong Tso: 21–23 October 1962</h4>
                        <p>The next area of interest for the Chinese was the Pangong Tso, which was under Indian control right up to the Khurnak Fort. Here, Chinese were operating on a very slender margin of superiority as far as the strength of attacking troops was concerned. Hence, for subsequent operations they created ad-hoc Task Forces, especially in the area of Pangong Tso.</p>

                        <p>There were two Indian positions (Sirijap I and II) on the northern bank of Pangong Tso held by a company less a platoon of 1/8 GR and together they formed the Sirijap Complex. These two positions were not mutually supporting and were face to face with two Chinese posts. As no uncommitted troops were available, Chinese organised a Task Force of 117 personnel drawn from various detachments defending this area for carrying out this task. The Sirijap Complex was attacked and captured on 21 October after stiff resistance by Indian troops.</p>

                        <div class="war-2-map-2">
                            <img src="assets/img/gallery/Pangong-Lake.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">Soldiers patrolling at a post near Pangong Lake, 1963<br> <strong>Source: MoD, DPR</strong></p>
                        </div>

                        <p>1/8 GR had also established three posts—Youla 1,2 and 3—on the southern bank of Pangong Tso with a total strength of a company. Chinese considered these posts to be about 20 km inside their Claim Line and planned to remove them. The Indian troops located at Youla Complex had seen the battles that raged on the northern bank opposite them on 21 October and would have anticipated Chinese action against them at any moment. They were isolated, dominated by Chinese posts and their route of withdrawal was also cut off. It is not difficult to visualise the hopeless position that they would have felt themselves in. Under the above circumstances, the Indian troops were ordered to withdraw during the night of 21/22 October and move to a high ground North of Gurung Hill which was also held by 1/8 GR. However, they were intercepted during the withdrawal and got involved in a running fight, suffering many casualties. Thus ended the battles on the northern and southern banks of Pangong Tso.</p>
                    </div>




                    <div class="war-details">
                        <h4>Operations in the Indus Valley: 26–28 October 1962</h4>
                        <p>Chinese now turned their attention to the Indus Valley Sector, which lies to the South of Chushul. The Indus River Valley is flanked by low hills to the east with several passes leading to the Tibetan Plateau. Chinese were apparently concerned by a threat emerging from the Indian forces along the Indus Valley approach. Hence, their next set of operations was directed to eliminate such a possibility while also securing their Claim Line in this sector. The 7 J&K Militia was responsible for defence of the Indus Valley Sub-sector. On 22 October, the Xinjiang Military Command issued orders for all the troops of 4th Infantry Division, which had launched major attacks in the north to start moving south to Rituzong in the Ali Defence Area and be ready for the next phase of battle by 26 October.</p>

                        <div class="war-2-map-2">
                            <img src="assets/img/gallery/AMX-13-tanks.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">AMX-13 tanks of 20m Lancers operating in the Chushul area, October 1962 <br><strong>Source: MoD, DPR</strong></p>
                        </div>


                        <p>The troops had to move about 500 km over rough and frozen tracks. It took them three days and two nights of continuous movement. As India had decided not to use the IAF, the PLA was able to move over open and barren terrain of Ladakh, even during day, with impunity.</p>

                        <p>The attacking troops were divided into two groups, each of about a battalion group, and a pincer movement was planned. One group was to advance on the northern flank and cut off the route of withdrawal of the Indian forces. The second group, which was the heavier force, was to advance on the southern flank and launch the main attack along Shiquan River Valley and attempt to surround Indian forces in an area between Zhamagere-Kariguo and annihilate them.</p>

                        <p>The main attack was launched during the night of 27/28 October 1962. Since the southern outflanking move was delayed, the Chinese did not quite succeed in closing the trap and Indian troops were able to withdraw during the night of 27/28 October in fairly good order. However, by the end of the day on 28 October, the Chinese troops had achieved their objectives and had occupied the Kailash Range that dominated the eastern bank of the Indus River. The attacking troops were now ordered to withdraw to Area Rituzong and await further orders.</p>

                        <p>There was a lull in fighting in the Western Sector from 29 October to 17 November 1962. On the Indian side, this period was utilised to organise the defence of Leh and to strengthen the defensive posture in the Chushul Sub-sector. The 3 Himalayan Division was raised on 26 October at Leh. HQ 114 Infantry Brigade was moved from Leh to Chushul on 27 October and took over the responsibility for Chushul and Phobrang Sub-sectors. HQ 70 Infantry Brigade was inducted into Ladakh on 25 October and took over the responsibility for the Indus Valley Sub-sector. The 163 Infantry Brigade was inducted for defence of Leh. Two troops of AMX-13 light tanks of 20 LANCERS were air lifted from Ambala Cantt to Chushul on 24–25 October by AN-12 aircraft of 44 Squadron, IAF, which was a major aviation feat.</p>

                    </div>



                    <div class="war-details">
                        <h4>Second Chinese Offensive: 18–21 November 1962</h4>
                        <p>The Chinese opened their second offensive on 18 November with the aim of securing the Spangur Gap. They identified two objectives: Gurung Hill, which constituted the Northern Shoulder of the Spangur Gap, and the Rezang La Complex in the South Gurung Hill was held by two companies of 1/8 GR, while Rezang La was held by a company of 13 KUMAON.</p>

                        <p>Rezang La was given greater importance of the two objectives as it dominated the Chinese posts and their rear areas to a considerable distance. For the attack on Rezang La, they organised the two infantry battalions of the 4th Infantry Division into two Task Forces, each comprising two companies, along with the 3rd Cavalry Regiment less two companies in reserve. Gurung Hill was to be attacked by an ad-hoc Task Force of about battalion strength with troops drawn from the Ali Detachment, 3rd Cavalry Regiment, and an engineer platoon.</p>

                        <p>Both the attacks commenced soon after first light on 18 November with an intense artillery bombardment. The attacks were stalled time and again, and Chinese had to commit their reserves. As the telephone line to the battalion HQ was cut and the radio set was destroyed in the initial stages, no reinforcements could reach the Indian troops there. For C Company, 13 KUMAON it was a fight to the finish. The guns fell silent around 2200 Hours on 18 November. Chinese accounts indi- cate that out of a total strength of 141 Indian troops, 136 were killed including the Company Commander Major Shaitan Singh, who was awarded the Param Vir Chakra posthumously, and five wounded men were taken POW. The Chinese suffered 21 killed and 98 wounded.</p>

                        <div class="war-2-map-2">
                            <img src="assets/img/Dead bodies of Indian soldiers being recovered in Ladakh-18 1.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">Dead bodies of Indian soldiers being recovered in Ladakh</p>
                        </div>

                        <p>As regards Gurung Hill, only a portion of it could be captured by the Chinese by last light on 18 November. They attacked again on 19 November but did not succeed in dislodging the Gorkhas. Their attack was poorly planned, coordinated and executed. The Gorkhas were also supported by a troop of AMX-13 tanks by indirect fire. The Gorkhas suffered about 50 killed, while the Chinese suffered 81 casualties (killed and wounded).</p>

                        <p>Keeping the overall situation in view, it was decided to withdraw troops from the Kailash Range and to redeploy them on mountains West of Chushul. Thus, despite the fact that some of the defensive positions were still holding out, a withdrawal was ordered on the night of 19/20 November 1962 and with that the whole of the Kailash Range passed into Chinese hands. The Chinese made no attempt to follow the withdrawing Indians, nor did they try to capture the Chushul airfield.</p>

                        <p>The unilateral ceasefire declared by the Chinese came into effect from the midnight of 21/22 November 1962 and there was no further fighting in this Sector. However, Chinese did not carry out any unilateral withdrawal in Ladakh and have maintained their Claim Line since then.</p>


                    </div>
                </div>

                <div id="eastern" class="accordion-details">
                    <h3>Eastern Sector: Kameng Frontier Division</h3>
                    <div class="war-details">


                        <div class="war-2-map-2">
                            <img src="assets/img/eastern-war-map.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">1962: North East Frontier Agency </p>
                        </div>



                        <p>During the official level talks in 1960, the Chinese had raised a dispute about the exact alignment of the McMahon Line in the area of Thagla Ridge. By early September 1962, they had occupied the Thagla Ridge in some strength. Around mid-September, in a meeting held in the Ministry of Defence (MoD) in New Delhi, chaired by then Defence Minister VK Krishna Menon, a decision was taken to throw the Chinese out from Thagla Ridge. This decision set into motion a chain of events which saw the 7th Infantry Brigade commanded by Brigadier JP Dalvi strung along Namka Chu by the end of September 1962. This was the situation when Lieutenant General (Lt Gen) BM Kaul, the newly appointed General Officer Commanding (GOC) of the freshly raised IV Corps arrived on the scene on 5 October and put into fast gear actions aimed at evicting the Chinese from Thagla Ridge.</p>


                    </div>



                    <div class="war-details">
                        <h4>The Trigger</h4>

                        <p>Lt Gen Kaul ordered 2 RAJPUT to move to Yumstola (16,000 ft) on the Thagla Ridge, to sit behind the Chinese. However, after strong remonstrations by the commanding officers (COs), the Brigade Commander, and the GOC of 4 Infantry Division, who were all present on the scene, the order was changed to send a company strength patrol of 9 PUNJAB under Maj Mahander Singh Chaudhry to occupy Tseng-Jong, a herder’s hut half way to the top of the Thagla Ridge in order to ascertain Chinese reaction to our crossing of the Namka Chu.</p>

                        <p>The 9 PUNJAB patrol of about 50 men reached Tseng-Jong before dusk on 9 October and hurriedly deployed as best as they could. The next morning at 0500 Hours Indian time, they were attacked by a battalion of the PLA. The patrol lacked fire support and soon ran out of ammunition. Maj Chaudhry was wounded but continued to lead his men with great determination and courage. They managed to repulse the first attack but were eventually overwhelmed by sheer numbers. The Punjabis suffered six dead, including one officer and 11 wounded. The Chinese admitted to have suffered 11 killed and 22 wounded. However, on this day the Chinese allowed the Indians to withdraw and even take away their wounded without interference. They even cremated our dead. All this was watched in amazement by the Indian troops deployed on the Namka Chu and commanders, including the Corps Commander, who were present there.</p>

                        <p>Shortly thereafter, the GOC left for Delhi; before leaving he ordered 7 Infantry Brigade to continue to hold their positions on the South Bank of Namka Chu and ensure the security of the crossings at all costs. The irony was that the Indian troops deployed on the Namka Chu could see the Chinese preparations opposite them on the dominating slopes of Thagla Ridge but were constrained from taking action until further directions from New Delhi.</p>


                        <p>On 12 October, Prime Minister Nehru, at Chennai airport while on his way to Colombo, told reporters that the Indian Army had been told ‘to throw the Chinese out from the Indian territory’. This, coupled with the incident of 10 October at Tseng-Jong, gave the Chinese the perfect excuse that they had been waiting for to launch a massive military operation to teach India a lesson, while terming their aggression as a ‘Counter Attack in Self Defence’.</p>


                    </div>




                    <!--             <div class="war-details">
                        <h4>Chinese Military Strategy </h4>

                        <p>Marshal Liu Bocheng who had been given the responsibility to approve the operational plans outlined the strategy of concerted attacks by converging columns. He compared the Indian dispositions with an analogy: ‘a copper head with the tail made of tin, a stiff back and a soft underbelly’. The operational concept entailed the following: ‘smashing the head (Se La), cutting off the tail (Bomdi La), snapping at the waist (Road Se La–Dirang Dzong), and dissecting the belly (Dirang Dzong). It was a bold and audacious concept aimed at annihilating the Indian troops deployed in this sector, and not a slow and grinding advance that would ordinarily be expected in mountains.</p>

                      
                       
                    </div> -->





                    <div class="war-details">
                        <h4>Battle of Namka Chu: 20-21 October 1962</h4>


                        <p>The Indian 7 Infantry Brigade of 4 Infantry Division was deployed on the Namka Chu in a linear fashion, strung over 25 km from Tsangle in the West to Drakung Samba Bridge in the East and dominated by Chinese troops on the Thagla Ridge. As there was no road beyond Tawang, the maintenance of the brigade was undertaken by porters or mules from the Logistics Base at Lumpu and by air at a Dropping Zone (DZ) at Tsangdhar, which was not entirely suitable for air drops and was in full view of the enemy. It needs to be noted here that being a river valley, Namka Chu was tactically unsound as a defensive position.</p>

                        <p>For the battle of Namka Chu, the Chinese had concentrated approximately 10,300 troops comprising Force 419 (a divisional sized force), two battalions ex 11 Infantry Division and an infantry regiment of border troops with enhanced complement of artillery and engineers. They struck in the early hours of 20 October on the unsuspecting and ill-prepared 7 Infantry Brigade in a kind of double envelopment manoeuvre with the main thrust being delivered from the left flank of the Indian troops deployed on the Namka Chu. The Chinese had infiltrated through Indian positions during the night as there were large gaps. They had thus occupied higher ground behind Indian forces and were attacking downhill. The Indian defenders were forced to turn around and face the attack. The units fought bravely but the result was a foregone conclusion. By the morning of 21 October, the battle of Namka Chu was over and the 7 Infantry Brigade had ceased to be an effective force.</p>


                    </div>





                    <div class="war-details">
                        <h4>Fall of Tawang: 22–24 October 1962</h4>


                        <p>Buoyed by the success on the Namka Chu, the Chinese High Command ordered their forces to continue the attack for the capture of Tawang. With the 7 Infantry Brigade having been moved forward to Namka Chu, the defence of Tawang was now the responsibility of the Commander, 4 Artillery Brigade with troops from 1 SIKH, 4 GARHWAL RIFLES and an artillery regiment—a most unusual arrangement to defend vital ground. The Chinese commenced their advance to Tawang at first light on 23 October and by last light the same day Tawang had been surrounded and came under attack from many directions. As Tawang had been vacated by the Indian troops during the night of 22/23 October, there was no coordinated resistance from the defenders and Tawang was occupied by the Chinese by first light on 24 October. The Chinese now controlled all areas North of Tawang Chu in the Kameng Frontier Division.</p>


                    </div>






                    <div class="war-details">
                        <h4>Battle of Se La and Bomdi La: 18–20 November 1962</h4>


                        <p>After the fall of Tawang, it was decided to develop Se La and Bomdi La into strong defensive positions. Se La was to be held by 62 Infantry Brigade, comprising five battalions supported by a field regiment and a troop of heavy mortars. Bomdi La was to be defended by 48 Infantry Brigade having three battalions supported by a battery of field guns. HQ 4 Infantry Division and other administrative elements were located in the valley at Dirang Dzong. In addition, two companies ex 62 Infantry Brigade and one company ex 48 Infantry Brigade were also located at Dirang Dzong for protection of the Divisional HQ. The 67 Infantry Brigade located at Missamari was available to reinforce 4 Infantry Division Defended Sector in case of an eventuality.</p>

                        <p>After the fall of Tawang, Lt Gen Harbaksh Singh took over as GOC 4 Corps for a brief period. He assumed command of the Corps on the evening of 25 October and proceeded the next day for reconnaissance and planning for the battle ahead. There was a lull in fighting after 24 October. Hence, on 29 October 1962, Lt Gen Kaul flew back to Tezpur and re-assumed command of the Corps.</p>

                        <div class="war-2-map-2">
                            <img src="assets/img/gallery/Indian-troops.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">Indian troops which moved to high-altitude defences in 1962 were poorly equipped to repulse the Chinese offensive.<br><strong>Source: MoD, DPR.</strong> </p>
                        </div>

                        <p>The Chinese, on the other hand, were busy developing a road from their forward base at Le to Tawang and concentrating forces for the next phase of operations. They had managed to concentrate by 15 November 1962, a force of three divisions— Force 419, 11 and 55 Infantry Divisions—and other combat support elements to undertake further operations. While 55 Infantry Division was to advance along Axis Tawang–Se La, Force 419 was to advance from the west through the narrow corridor between Se La and the Indo-Bhutanese border, assist in the capture of Se La from the South, while simultaneously capturing Dirang Dzong in concert with the troops of 11 Infantry Division who were advancing from the east in a wide outflanking move. The attack was to be launched on 18 November, simultaneously in the Western and Eastern Sectors.</p>

                        <p>It was the advance of 11 Infantry Division that was the most audacious part of the Chinese offensive, and which unhinged the defenders completely. They commenced their advance on 10 November and carried out a wide outflanking move from the east along the Bailley Trail. The Division moved on manpack basis, each soldier carrying about 30 kg of provisions in addition to his personal weapon and ammunition. They were provided about 1,000 porters recruited locally. The Division marched approximately 160 km over six days and seven nights, and secured Thembang (North of Bomdi La) by last light on 17 November. During the night of 17/18 November, they seized a vital bridge on the road to Dirang Dzong–Bomdi La and thus cut off HQ 4 Infantry Division from the South.</p>



                        <p>Thus by the morning of 18 November, the troops of Indian 4 Infantry Division had been split into three isolated pockets at Se La, Dirang Dzong and Bomdi La. Further, the Divisional HQ itself was cut off from the North and South; it was thus rendered incapable of exercising command and control because its own security was threatened. This was to have a disastrous effect on subsequent conduct of the battle. In fact, the battle was lost before it was even joined.</p>



                        <p>The Se La Brigade was ordered to withdraw without a fight and fall back on Bomdi La. The Division HQ itself was in the process of withdrawing and Bomdi La fell without a fight by early hours of 19 November. The Chinese went in pursuit of the withdrawing Indian troops: a large number were killed or taken POW, and a few made their way back through Bhutan after marching for many days without food and water. There were many acts of bravery and heroism but these contributed little to the final outcome. Chaku in the foothills was captured by the Chinese by the morning of 20 November.</p>


                        <p>By this time, the HQ of the Indian 4 Infantry Division had moved to Tezpur and the Division had ceased to exist as an effective fighting force. They had suffered about 5,100 casualties (killed, wounded and captured). Whatever troops could be mustered were deployed South of Brahmputra River. As per Chinese accounts, they had suffered 225 killed (27 officers and 198 men) and 477 wounded (46 officers and 431 men). The PLA General HQ issued orders on 20 November for a unilateral ceasefire and withdrawal (from NEFA only), which was to take effect from the midnight of 21/22 November. The war was over.</p>

                        <p>It would be worth mentioning that the Chinese operational plan for the battle of Se La–Bomdi La can be compared with their plan for the battle of Chosin Reservoir from 27 November–9 December 1950 during the Korean War of 1950-53. However, the outcome of that battle was markedly different. The US 1st Marine Division not only survived as a fighting force but, through the use of air and fire power, inflicted such attrition on the Chinese IX Army Group that they even disappeared from the Korean battlefield for three months.</p>


                    </div>







                    <div class="war-details">
                        <h4>Eastern Sector: Lohit Frontier Division (Battle of Walong)</h4>


                        <p>Walong is located about 30 km from the Tibetan border (McMahon Line) and, in 1962, was approximately 200 km from the road-head at Teju involving a march of 14 stages over a goat track. Walong also had an advance landing ground (ALG) which was only fit for small aircraft—Otters and Caribous—to operate. While the main Chinese offensive in NEFA was directed against the Kameng Frontier Division, a subsidiary offensive was launched in the Lohit Frontier Division. Here again, the Chinese aim was to annihilate the Indian troops deployed in the Walong Sector and, in the process, to advance up to their Claim Line. Traditionally, it was the Assam Rifles which manned the border posts in this Sector. However, due to the increase in Chinese incursions, 6 KUMAON was inducted into this area in March 1962. The overall responsibility for the defence of the whole of NEFA, a frontage of nearly 1,100 km, was that of 4 Infantry Division. By 20 October, the Chinese had built up an infantry regiment plus a reinforced battalion opposite the Walong Sector at Rima.</p>

                    </div>






                    <div class="war-details">
                        <h4>Preliminary Operations: 22–24 October 1962</h4>


                        <p>At about midnight on 22 October, the Chinese launched an attack by a battalion on Kibithoo held by a company of 6 KUMAON. The Chinese were able to ultimately breakthrough by sheer weight of numbers. 6 KUMAON withdrew to occupy a defensive position at Walong, leaving behind a screen position at Ash Hill. Chinese struck again at the screen position in the early hours of 23 October. The screen put up a determined fight and was ordered to withdraw to the main defences after it had served its purpose. By the morning of 24 October, the Chinese had contacted the main defences at Walong. By this time, 4 SIKH had occupied a defended area at Walong along with two companies of 2/8 GORKHA RIFLES who were in the process of being inducted. Hereafter, there was a lull in the battle for some time, except for patrol clashes.</p>

                    </div>


                    <div class="war-details">
                        <h4>Reorganisation by India: 25–31 October 1962</h4>


                        <p>As a sequel to the fall of Tawang, a new formation 2 Infantry Division was raised at Teju under the command of Maj Gen MS Pathania. The newly raised division was assigned the responsibility for the whole of NEFA, less the Kameng Sector. As a result of the reorganisation, 11 Infantry Brigade under Brig NC Rawlley assumed responsibility for the Walong Sector by 31 October. During the same period, 2/8 GORKHA RIFLES which was in the process of being inducted was ordered to be replaced by 3/3 GORKHA RIFLES. It will subsequently be seen that too many changes in the Order of Battle of the Brigade were being carried out in the face of the enemy, which is extremely hazardous under the best of circumstances. This had a disastrous effect on the conduct of subsequent operations.</p>

                    </div>




                    <div class="war-details">
                        <h4>Build-up by China: 25 October–13 November 1962</h4>


                        <p>Subsequent to the induction of additional troops by India, on 29 October, the Chinese ordered the move of 130 Infantry Division located in Sichuan Province for the main offensive in the Walong Sector. This involved a move of nearly 1,800 km to the road head at Shugden (approximately 90 km north of Rima). The concentration of this Division was completed in 10 days which was a great logistics feat. All preparations for the main offensive were completed by 13 November, which was to commence on 18 November. In the event, it was advanced to 16 November.</p>


                    </div>



                    <div class="war-details">
                        <h4>6 KUMAON Attack on 14 November 1962: Prelude to the Main Battle</h4>


                        <p>As part of consolidation and extension of the Firm Base, Chinese had secured Yellow Pimple and Green Pimple by the end of October 1962, thus posing a threat to the Walong defences from the western flank. By about 10 November, it became clear to Brig Rawlley that the enemy’s build up for attack from the western flank was nearing completion. He came to the conclusion that it was essential to wrest the areas of Tri-junction, Yellow Pimple and Green Pimple from the Chinese control, in order to provide security to Walong defences from the West which was the vulnerable flank. Accordingly, he decided to pull out 6 KUMAON from the defences and concentrate them for an attack to secure these areas. However, this weakened the main defences of Walong.</p>

                        <p>6 KUMAON commenced their attack on the morning of 14 November. They managed to secure the Tri-junction and then proceeded to secure Yellow Pimple. The Chinese had by now firmed up on the Yellow Pimple and 6 KUMAON casualties began to mount. 4 DOGRA who were in the process of being inducted were also fed into the attack piecemeal. 6 KUMAON had managed to reach within 50 m of the Yellow Pimple by last light on 14 November, but the attack got stalled there due to very heavy casualties and the stiff resistance by the Chinese. At about 0100 Hours on 15 November, the Chinese counter attacked with a regiment plus under intense artillery bombardment. By first light 15 November, 6 KUMAON, which had been fighting continuously for nearly 24 hours, was pushed back to near the top of the Tri-junction. Thus, ended the brave but ill-timed attack by 6 KUMAON. In the process, the defences of Walong had been laid bare and two companies of 4 DOGRA, the only reserve available to the Brigade had also been used up. It seems that the Brigade Commander at that point of time was unaware that the Chinese had managed to concentrate another division for the main offensive. This would also point to an intelligence failure at the operational level.</p>


                    </div>


                    <div class="war-details">
                        <h4>The Main Battle: 16 November 1962</h4>
                        <p>In order to exploit the success of their counter attack, the Chinese higher command decided to advance D Day for the main offensive from 18 November to 16 November. The offensive by a division plus a regiment commenced on 16 November morning on both flanks of Walong defences with the main effort on the western flank. Already denuded and weakened, by about 1100 Hours, the 11 Brigade Defended Sector had crumbled and the situation became untenable. At that point of time, the Brigade Commander in consultation with the Corps Commander (Lt Gen Kaul) who had been in Walong since 15 November, and had seen the situation first hand, took a decision to withdraw. The orders for withdrawal were issued and it commenced at about 1400 Hours on 16 November.</p>


                        <p>Due to breakdown in communications, some of the sub-units could not receive these orders and continued to fight till the end. The Brigade, though badly battered, managed to withdraw to the next main position at Hayuliang. The Chinese followed up closely as far as Chingwinty but did not succeed in annihilating the Brigade. Thus, ended the battle of Walong.</p>
                    </div>

                    <div class="war-details">
                        <h4>Eastern Sector: Operations in Subansiri and Siang Frontier Divisions</h4>
                        <p>The Central Sector of NEFA was of secondary importance for the Chinese from an operational point of view. Their aim was to tie down maximum troops in this area by launching small forces and, in the process, also capture some territory to stake their territorial claims. They assembled an ad-hoc force of about three battalions by milking troops from Shannan Sub-Area, Linzhi Sub-Area and the Lhasa Area. The overall command was to be exercised by the Political Commissar of the Tibet Military Command. The main attack was to be launched on 18 November in coordination with operations in the Kameng Frontier Division. On the Indian side, 5 Infantry Brigade of 4 Infantry Division with its HQ at North Lakhimpur was responsible for defence of the rest of NEFA less Kameng Frontier Division.</p>
                    </div>




                    <div class="war-details">
                        <h4>Subansiri Frontier Division</h4>
                        <p>In October 1962, the defence of Subansiri Frontier Division was bifurcated into two Sub-sectors—Lower and Upper Subansiri. Lower Subansiri was the responsibility of 9 Assam Rifles and Upper Subansiri that of 2 JAK RIF with its HQ at Daporijo. On 22 October, 5 Infantry Brigade, which till then had been under 4 Infantry Division, was placed directly under command of the newly raised IV Corps. On the same day, 2 JAK RIF and other troops at Daporijo were ordered to move and redeploy at Taliha by 24 October, while the war had already started on 20 October.</p>


                        <p>The Chinese commenced their operations in this Sector on 23 October with an attack by a battalion group against the border posts of Asaphila, Sagamla, Tamala and Potrang. All the border posts were ordered to withdraw to Taliha under the orders of HQ IV Corps issued through the HQ 5 Infantry Brigade. Chinese now started preparing for the main offensive which was set to commence on 18 November.</p>

                        <p>During this period, there was some reorganisation carried out by the Indian side with the raising of HQ 2 Infantry Division and the induction of 192 Infantry Brigade. Another battalion, 1/4 GORKHA RIFLES was inducted and the Brigade was ordered to occupy a cohesive brigade defended sector. Alas, these measures came a bit too late as the Chinese struck on 18 November.</p>

                        <p>The Chinese battalion commenced their advance on 18 November with Limeking as their objective. Their advance was slow due to Indian resistance and the difficult terrain; they made contact with the main defences at Limeking on 21 November. The Indian troops at Limeking had already been ordered to withdraw during the night of 20/21 November to Daporijo. Thus, Chinese were able to secure Limeking by the morning of 21 November without a fight. Thereafter, they continued their advance towards Daporijo till last light on 21 November, at which time they received orders to stop and return to Limeking. There was no further fighting in this Sector.</p>
                    </div>

                    <div class="war-details">
                        <h4>Siang Sector</h4>
                        <p>This Sector was also the responsibility of HQ 5 Infantry Brigade. The troops allotted were 2/8 GORKHA RIFLES, 2 MADRAS and 11 ASSAM RIFLES supported by 70 Heavy Mortar Battery. The above troops were maintained by Kalinga Airways with ALGs at Along, Menchuka and Tuting.</p>


                        <p>Chinese had mustered an ad-hoc force of about 1,650 troops from their line of communication troops. They commenced their probing attacks on 21 October and continued to progress gradually. They had planned a pincer movement towards Tuting and Gelling but were unable to execute the same due to poor planning and ad-hoc nature of the force. By the end of October, Chinese had captured areas up to Lamang, Manigong and Gelling.</p>

                        <p>On the Indian side, the situation was reviewed after the raising of HQ 2 Infantry Division. The responsibility for defence of this Sector now passed from HQ 5 Infantry Brigade to HQ 192 Infantry Brigade, which was effective only by about 13 November. One of its battalions, 2/8 GORKHA RIFLES was in the process of moving piecemeal by air from Walong where they had been in contact with the enemy. On the eve of the battle the Brigade Sector was in the process of reorganisation and could hardly be expected to fight a cohesive defensive battle.</p>


                        <p>Chinese commenced their main offensive on 16 November with a battalion heading for Tuting and another Battalion going for Menchuka. On the Indian side, there was confusion as 2/8 GORKHA RIFLES were still being inducted into Menchuka. There were orders and counter orders. Menchuka was vacated on the night of 18/19 November and the Chinese occupied it by the morning of 19 November. Thereafter, the Chinese occupied Gelling on 21 November. On 22 November, while closing up with Tuting, they learnt that it had already been vacated. However, since China had declared a unilateral ceasefire with effect from midnight 21 November, further operations were halted and they were ordered to fall back to Limeking and Menchuka, and await further orders. Tuting was re-occupied by the Assam Rifles on 25 November and there was no further fighting in this Sector.</p>

                    </div>



                    <div class="war-details">
                        <h4>Air Aspects</h4>
                        <p>No account of the 1962 Sino-Indian Conflict can be complete without touching upon the air aspects. It is well known that India did not use its air force in an offensive role during the conflict. The reasons for the same are to be found in the political domain. The decision might have been influenced by an exaggerated and incorrect assessment given by the Intelligence Bureau (IB) to the political and military leadership about the Chinese air capabilities and the threat that they could pose to Indian cities in the hinterland.</p>


                        <p>There were three factual errors in the assessment given by the IB. Firstly, the Chinese did not have any night fighter in 1962 as MIG-19 P with night fighting capability were inducted into the PLA Air Force (AF) many years later. Secondly, India did have night interception capability with its two squadrons of Vampire night fighters. Thirdly, as far as MIG-21s were concerned, post conflict reports indicate that the Chinese did not have these aircraft in 1962 but got them from erstwhile Soviet Union much later.</p>

                        <p>Post-conflict intelligence also indicated that there were no Chinese combat aircraft deployed in Tibet in 1962. Hence, the PLA AF did not pose any threat to India as it was neither allocated for this operation, nor deployed during the operations. The IAF, if committed into action, would have had practically a free run in the tactical battle area.</p>


                        <p>In the overall assessment, the use of IAF for offensive operations was likely to have made a tremendous difference to the final outcome, especially in Ladakh. Then there is also the psychological effect on own troops when they see their own aircraft above them.</p>

                        <div class="war-2-map-2">
                            <img src="assets/img/gallery/Rezangla.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">Gurkha Memorial at Chushul</p>
                        </div>


                        <div class="war-2-map-2">
                            <img src="assets/img/gallery/Chushul.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">Rezangla Memorial Plaque<br>
                        <strong>Source: MoD, DPR. </strong> </p>
                        </div>

                    </div>
                </div>


                <div id="conclusion" class="accordion-details">
                    <h3>Indian POWs in Chinese Custody </h3>
                    <div class="war-details">
                        <h4></h4>
                        <p>By the end of the war, there were more than 3,900 Indian officers and men in Chinese custody. Thus, Chinese held all the cards as far as repatriation of the POWs was concerned. The handing over of seriously wounded POWs commenced on 5 December 1962 at Bomdi La and the repatriation of all the prisoners was completed by 25 May 1963. As there was no formal declaration of war between India and China, the Chinese termed the captured Indian army personnel as ‘captives’ and not ‘prisoners of war’. The Indian POWs in Chinese custody did not receive any official visit by ICRC officials throughout their stay.</p>


                        <p>The repatriation of POWs was generally handled between the ICRC, the Indian Red Cross (IRC) and the Chinese Red Cross (CRC). Even though diplomatic ties between the two countries were not severed entirely, there were very few direct inter- actions between the two governments on the subject of repatriation of the POWs. Probably, the Indian Prime Minister felt betrayed and he expected the Non-Aligned countries to act on India’s behalf. However, China on its part was insistent on negotiating directly with Delhi. This impasse certainly hampered early repatriation of the POWs.</p>

                        <p>The Chinese also tried to derive propaganda value from their treatment of officer prisoners by taking them on a conducted tour of various cities to showcase the progress made by Communist China and even tried to parade them in Tiananmen Square on 1 May 1963, but Brig Dalvi refused in no uncertain terms and the Chinese had to drop the idea. While the men were handed over at various points at the border, 27 Indian officers were handed over by the CRC to the IRC at Kunming, the capital city of Yunnan on 4 May 1963 and flown to Dum Dum airport in Calcutta (now Kolkata).</p>
                    </div>
                </div>



                <div id="conclusion" class="accordion-details">
                    <h3>Conclusion </h3>
                    <div class="war-details">
                        <h4></h4>
                        <p>In retrospect, one may say that the war was the result of differing geopolitical perceptions on both sides. The Indian political leadership of the post-Independence period was keen to have friendly relations with Communist China. They also presumed that the border between the two neighbours was a settled matter through treaties negotiated in the past. They were proved wrong on both counts as China had differing priorities and perceptions. The Chinese believed that the border was unsettled and required fresh negotiations. The friendly relationship (till 1954) turned into frostiness gradually due to repeated Chinese incursions across the boundary. This was especially the case after the Tibetan rebellion of 1959 and the escape of Dalai Lama to India. The state of armed co-existence which came about between the two armies could result in open hostilities at any moment.</p>


                        <p>Notwithstanding the signals emerging to the contrary (from China), India still continued to believe that it was unlikely to initiate any large-scale military action. On the other hand, China had been actively contemplating and preparing for such an eventuality since 1960, if not earlier. India just failed to recognise the ground realties and continued to neglect defence preparedness. There was over reliance on diplomacy which had run its course by mid-1962. As a result, India was taken by complete surprise and the Indian Army was hustled into a war for which it was not prepared, organised, trained or equipped.</p>
                    </div>
                </div>



                <!-- <div class="note">
                    <p>Notes</p>
                    <ol>
                        <li>Claude Arpi, <i>Tibet: When the Gods Spoke—India Tibet Relations 1947-62, Part 3</i>, Delhi: Vij Books, 2019, p. 16.</li>
                        <li>Henry Kissinger, <i>On China</i>, London: Penguin, 2011, p. 189.</li>
                        <li>John W Garver, 'China's Decision for War with India in 1962', in Alastair Ian Johnston and Robert S Ross (eds), <i>New Directions in the Study of China's Foreign Policy</i>, Stanford: Stanford University Press, 2006, pp. 86–130.</li>
                        <li><i>A History of the Counter Attack War in Self Defence Along the Sino-Indian Border</i>, Beijing: Military Science Publications, 1994, Chapter 4, Section 2. An English translated version of the original book in Mandarin is available with the Centre for Military History and Conflict Studies (CMHCS, formerly the Centre for Armed Forces Historical Research [CAFHR] at the USI of India (archives).</li>
                        <li>Government of India, Ministry of External Affairs, <i>White Paper I (1954–59)</i>, p. 26.</li>
                        <li>For details see Major Gen PJS Sandhu (ed.), <i>A View from the Other Side of the Hill, USI Study</i>, Delhi: Vij Books, 2015, pp. 46–51.</li>
                        <li>22. Ibid., pp. 52–54.</li>
                        <li>Ibid., pp. 55–58.</li>
                        <li>Ibid., pp. 59–62.</li>
                        <li>Ibid., pp. 63–67.</li>
                        <li>Brig JP Dalvi, <i>Himalayan Blunder: The Curtain-Raiser to the Sino-Indian War of 1962</i>, Bombay: Thacker & Co. Ltd., 1969, pp. 72–74.</li>
                        <li>Maj Gen DK Palit, <i>War in High Himalaya: The Indian Army in Crisis, 1962</i>, London: Hurst and Company, 1991, pp. 195–200.</li>
                        <li>The Bailey's Trail dates back to the historic route taken by Lt Col FM Bailey and Captain HT Morshead while on a reconnaissance mission in 1913–14, prior to the Shimla Conference where the McMahon Line delineating the boundary between Tibet and British India was drawn.</li>
                        <li>Sandhu (ed.), <i>A View from the Other Side of the Hill</i>, n. 21, p. 89.</li>
                        <li>For a detailed comparison see Major General PJS Sandhu (Retd), '1962—Battle of Se La and Bomdi La', <i>USI Journal</i>, Volume CXLI, No. 586, October-December 2011, pp. 585–589.</li>
                        <li>Brig NC Rawlley's Papers, held with the CMHCS in the USI Archives.</li>
                        <li>Ibid.</li>
                        <li>Sandhu (ed.), <i>A View from the Other Side of the Hill</i>, n. 21, p. 109. 34. Ibid., pp. 110–111.</li>
                        <li>35. Ibid., pp. 111–115.</li>
                        <li>BN Mullik, <i>My Years with Nehru: The Chinese Betrayal</i>, Bombay: Allied Publishers, 1971, p. 350.</li>
                        <li>Claude Arpi, <i>The End of an Era: India Exits Tibet, India-Tibet Relations 1947-62, Part 4</i>, Delhi: Vij Books, 2020, p. 530.</li>
                        <li>Ibid., p. 531.</li>
                        <li>Ibid., p. 557.</li>
                    </ol>
                </div> -->




                <!-- <div id="gallery" class="row image-gallery">
                    <div class="col-md-12">
                        <h3>Gallery</h3>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery1.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-1</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery2.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-2</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery3.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-3</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery4.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-4</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery5.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-5</p>
                        </div>
                    </div>

                    <div class="col-md-4 main">
                        <div class="image-main img-pop">
                            <img src="assets/img/war-gallery/war-gallery6.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                            <p>Image-6</p>
                        </div>
                    </div>
                </div> -->


                <!-- <div id="video" class="row image-gallery">

                    <div class="col-md-12">
                        <h3>Video</h3>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="featured-video" >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">
                                <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="featured-video" >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">
                                <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="featured-video" >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">
                                <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>

                    
                </div> -->


            </div>
        </div>
    </div>
</section>


<?php include('footer.php'); ?>