<?php 


$main ="iconic";

$page="manekshaw";


get_header(); ; ?>

    <section class="iconic-leaders-detail-banner"
        style="background-image: url(../assets/img/iconic-leaders-detail-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Iconic leaders</h1>
        </div>
    </section>

    <section class="iconic-leaders-detail">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-9 res-leader">
                    <img src="../assets/img/maneksh.jpg" class="img-fluid" width="100%" alt="" loading="lazy">
                    <h2>FIELD MARSHAL SHFJ MANEKSHAW</h2>
                    <p>Sam Hormusji Framji Jamshedji Manekshaw, a popular and colourful military leader of India, led the nation to its first decisive victory in the 1971 Indo-Pakistan War. ‘Sam Bahadur’, as he was popularly known, became a household word. He was India’s first Field Marshal and remains, even today, the most admired and idolised of our Army Chiefs. Vigour, dash and élan, the typical signs of a great soldier, he had them all.
                        <br><br>
                                                
                        Sam Manekshaw, a Parsi, was born on 3 April 1914 in Amritsar. After schooling in Sherwood College, Nainital, he joined the first course at the newly formed IMA at Dehradun on 1 October 1932. The first batch, called &#39;The Pioneers&#39;, had three future Chiefs: Manekshaw
                        rose to head the army in India; Smith Dun headed the army in Burma; and Mohammad Musa
                        headed the Pakistani Army. The batch passed out from the IMA on 22 December 1934.
                        However, they were commissioned on 1 February 1935, with the date of seniority fixed as 4
                        February 1934. This was done in order to make them junior to officers commissioned from
                        Sandhurst a year earlier, after giving them one year’s ante-dated seniority, to account for the difference in duration of training at the two institutions.
                        <br><br>

                        After commissioning, Manekshaw was attached to the Royal Scots at Lahore for a year. In February 1936, he was posted to 4/12 Frontier Force Regiment (FFR), also known as the 54th Sikhs, which became his parent unit. The battalion was then in Ferozepur, but soon moved to Fort Sallop in the NWFP. Sam learned to speak Pashto fluently and because of his complexion and name, was often mistaken by the tribesmen for a Pathan. 
                    </p>
                    <img src="../assets/img/field.jpg" class="img-fluid" width="100%" alt="" loading="lazy">
                    <p>In 1942, Sam's battalion was ordered to move to Burma. Soon after their arrival in Burma, the Japanese offensive commenced. At that time, Sam Manekshaw was a Capt, but was made acting Maj and given command of a Sikh company. During the battle of Sittang Bridge, he was severely wounded and no one thought he would survive. Maj Gen DT Cowan, who was then the Deputy Commander of the British Forces, awarded him a Military Cross for gallantry on the spot. Sam was evacuated from the frontline in a serious condition, but was saved.  
                        <br><br>
                        After doing the Staff Course at Quetta in 1943, he was posted as Brigade Major (BM) of the Razmak Brigade. Soon afterwards, he joined 9/12 FFR in Burma, where he was given the task of supervising the disarming of about 60,000 captured Japanese soldiers and setting up of a prisoner of war (POW) camp. Towards the end of 1945, he was posted to the Military Operations (MO) Directorate as GSO 1. When India achieved independence in 1947, Sam was a Lt Col, posted as GSO 1 in MO-3, the section that dealt with future operations and planning. Shortly afterwards, he received orders posting him as CO 3/5 Gorkha Rifles. Before he could move, fighting broke out in Kashmir and his posting orders were cancelled.
                        <br><br>
                        Manekshaw accompanied VP Menon when he flew to Srinagar with the Instrument of Accession on 25 October 1947. After obtaining the signatures of Maharaja Hari Singh on the document, they flew back the same night, reaching Delhi at 4 am. A Cabinet meeting was held, which was attended by Lord Mountbatten, Pandit Nehru, Sardar Patel, Baldev Singh and several others. After VP Menon handed over the Instrument of Accession, Mountbatten asked Manekshaw to explain the military situation. He gave the Cabinet a rundown on the latest developments, pointing out that the Pakistani tribesmen were just 9 km from Srinagar. If the airfield was taken, Kashmir would be lost since it would not be possible to fly in troops. The very next day, on 27 October 1947, Indian troops were flown into Kashmir.
                    </p>
                    <div class="leader-wrapper">
                    <img src="../assets/img/field-gallery.jpg" class="img-fluid" alt="" loading="lazy">
                    <!-- <img src="../assets/img/field-gallery.jpg" class="img-fluid" alt="" loading="lazy">
                    <img src="../assets/img/field-gallery.jpg" class="img-fluid" alt="" loading="lazy"> -->
                    </div>
                    <p>After Sam's posting as CO 3/5 Gorkha Rifles was cancelled, he could not get out of the MO Directorate, thanks to the crisis in Kashmir, followed by the one in Hyderabad. In fact, he never commanded a battalion and was promoted to the rank of Colonel (Col), and then Brig, in the same office. In September 1948, when the Hyderabad operations took place, he was Director Military Operations (DMO). In April 1952, Manekshaw was given command of 167 Infantry Brigade at Ferozepur. In April 1954, he was posted as Director of Military Training at Army HQ. In January 1955, he was transferred to Mhow as Commandant, Infantry School, and in 1957, he was sent to London to attend the Imperial Defence College course. 
                        <br><br>
                        On his return from the UK in December 1957, Sam was promoted to the rank of Maj Gen and posted as GOC 26 Infantry Division. In September 1959, he was posted to Wellington as Commandant, Defence Services Staff College (DSSC). At Wellington, he was involved in an unsavoury incident, which almost ended his career. Sam often made disparaging remarks about Indian politicians, which led some people to brand him as anti-national and disloyal. As a result, the Army HQ ordered a court of inquiry to investigate. The members of the inquiry included Lt Gen Daulet Singh, GOC-in-C Western Command, and Lt Gen Bikram Singh, GOC 15 Corps. Sam Manekshaw was exonerated of all charges, with the court also recommending disciplinary action against officers who had made the false allegations. 
                        <br><br>
                        The Sino-Indian conflict in 1962 ended in a debacle for the Indian Army. In November 1962, Nehru summoned Manekshaw to Delhi and asked him to assume command of 4 Corps. On being briefed about the general situation on the day he took over, he called his chief of staff and told him that he wanted to issue orders. After his staff had assembled, Sam reportedly declared: ‘Gentlemen, there shall be no more withdrawals.’ He knew that nothing else could restore confidence as quickly as advancing to the positions that had been lost.
                        <br><br>
                        In December 1963, Sam Manekshaw was appointed GOC-in-C Western Command. After a year, he moved to Eastern Command in November 1964, where he remained for almost five years. On 8 June 1969, he was appointed COAS. He was destined to write his name in the history books as India's first Field Marshal and the victor of the 1971 war. As Army Chief, Sam Manekshaw cut a dashing figure, with his side cap and pleated shirts. He was full of beans and his enthusiasm and energy were contagious. This, coupled with his ready wit and sense of humour, made him a popular figure and his visits to formations and units were looked forward to. 
                        <br><br>
                        In 1971, when refugees from East Pakistan began to cross over into India, there was a meeting of the Cabinet on 27 April 1971, to which Manekshaw was invited as the Chairman of the Chiefs of Staff Committee. Considering the situation, PM Indira Gandhi told him that she wanted the army to go into East Pakistan. In response, Manekshaw asked for some time and listed the reasons for his reluctance to go to war with Pakistan immediately, including the imminent onset of  the monsoons, which would make the ground unsuitable for operations. All movement would have to be on roads, which could be blocked, and the air force too would not be able to support the ground troops due to bad weather. Moreover, moving the armoured division and other troops to the East would require time as well as all available rail and road space. He advised postponement of the operations till the winter months. This would give the army enough time to build up infrastructure required for large-scale operations in the east. In addition, during winter, the northern passes would be blocked by snow, eliminating the threat of intervention by the Chinese. 
                        <br><br>
                        The PM, though visibly angry, did not say anything. She closed the meeting, asking everyone to come back at 4 o'clock. As everybody rose to leave, the PM asked Sam to stay back. When they were alone, he offered to resign on physical or mental grounds. ‘Sit down, Sam,’ she said. ‘I don't want your resignation. Just tell me, is everything you said earlier true?’ ‘Yes, it is. Look, it is my job to fight, and fight to win. Today, if you go to war, you will lose. Give me another six months and I guarantee you a hundred percent success.
                        Once the decision to undertake operations was taken, Sam Manekshaw was given the go ahead to begin preparations. 
                        <br><br>
                        The Indo-Pak War of 1971 started on 3 December 1971 after Pakistan Air Force aircraft bombed Indian airfields in the Western Sector. . It lasted for 14 days and ended with a formal surrender ceremony that took place at Dacca on 16 December 1971. 
                        <br><br>
                        During the war, a large number of prisoners were taken. They were lodged in several camps all over the country. Sam Manekshaw ensured that the POWs were well looked after. At several places, Indian troops were asked to vacate their barracks and live in tents so that the POWs could be properly accommodated. They were allowed to celebrate their festivals and given copies of the Koran. The Red Cross and other international agencies were given free access to the POW camps, and they were permitted to receive letters and gift parcels
                        <br><br>
                        During the 1971 war, India won a decisive victory over Pakistan. A new nation, Bangladesh, came into being and Sam Manekshaw, as the prime architect of the victory, became a hero. Apart from capturing 93,000 prisoners, the Indian Army also occupied several hundred square kilometres of Pakistani soil in Ladakh. After a year, when talks were held in Simla between the PMs, it was expected that India would be able to wrest some major concessions from Pakistan and negotiate a permanent solution to the Kashmir problem. Unfortunately, Manekshaw was kept out of the summit and played no part in the negotiations. Bhutto and Indira Gandhi informally agreed to accept the ceasefire line in Kashmir as the international border, but this was not reduced to writing. As a result, the military gains, achieved at great cost in human lives, were frittered away by politicians and bureaucrats. When Indira returned from Simla, Manekshaw reportedly told the her: ‘Bhutto has made a monkey out of you.
                        <br><br>
                        Soon after the end of the war, Indira Gandhi decided to promote Manekshaw to Field Marshal and also appoint him as the Chief of Defence Staff (CDS). In view of strong opposition from the air force, the proposal to create the post of CDS was dropped.  Sam Manekshaw was due to retire in June 1972, but was given an extension of six months. The rank of Field Marshal was formally conferred on Sam at a special investiture ceremony held at the Rashtrapati Bhawan on 3 January 1973.
                        <br><br>
                        Manekshaw’s popularity came at a price. Many people, especially in politics and the bureaucracy, began to perceive him as a threat. In an interview with a young lady reporter, Sam mentioned that during the Partition he had been asked to opt for Pakistan, but had chosen to remain in India. When the reporter asked Sam what would have happened if he had opted for Pakistan and been commanding the Pakistani Army, he replied, ‘they would have won’. Undoubtedly, he made this witty remark without considering the consequences, which were immense. Soon afterwards, there was a question in the Parliament based on the story where the  remark was featured prominently. The PM was in the House, but chose to remain silent. Manekshaw was branded an egotist and soon became persona non grata. Though the government could not take away his rank, it did take away everything else. He was given a salary which was much lower than what he was entitled to after having been an Army Chief. None of the other facilities that a Field Marshal gets, such as secretarial staff, house or car, were given to him.
                        <br><br>
                        After his retirement from active service, Manekshaw settled down in Coonoor in the Nilgiris, very close to Wellington. When President Abdul Kalam came to know that he had not been paid his full salary, he went to Wellington and personally handed over a cheque for Rs 1.3 crores, his arrears of pay for over 30 years. He passed away on 27 June 2008, at the age of 94, and was buried in the Parsi cemetery in Ooty, Tamil Nadu, with military honours. Surprisingly, Manekshaw's funeral was not attended by the top brass from civil, military or political leadership. 
                        Sam Manekshaw will always be remembered as an outstanding military leader, who gave us our first decisive victory against a foreign power in 1971. A living legend, his place is assured in the hall of fame of the Indian Army.

                    </p>
                </div>
            </div>
        </div>
    </section>


    <?php get_footer(); ; ?>