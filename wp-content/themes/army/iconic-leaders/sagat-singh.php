<?php 


$main ="iconic";

$page="sagat";


get_header(); ; ?>

    <section class="iconic-leaders-detail-banner"
        style="background-image: url(../assets/img/iconic-leaders-detail-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Iconic leaders</h1>
        </div>
    </section>

    <section class="iconic-leaders-detail">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-9 res-leader">
                    <img src="./assets/img/sagat.jpg" class="img-fluid" width="100%" alt="" loading="lazy">
                    <h2>LIEUTENANT GENERAL SAGAT SINGH</h2>
                    <p>One of India's most brilliant and audacious military leaders, Sagat Singh’s record as a combat leader is unmatched. He not only succeeded in every operation, but went beyond and achieved more than what he was asked to do. Imbued with an aggressive spirit and the ability to take risks, he was the epitome of the combat leader who leads from the front.
                        <br><br>

                        Sagat Singh was born on 14 July 1919 in Bikaner. After studying at Walter Nobles' School and Dungar College in Bikaner, he joined the Bikaner State Forces. In 1939, he joined the IMA as an Indian State Forces Cadet. After passing out in 1941, he did a short attachment with the South West Borders, in the NWFP, before joining the Bikaner State Forces at Secunderabad. He served with the unit for the next six years, which included moves to Iraq, Syria and Palestine, during which he did two staff courses, at Haifa and Quetta. After the merger of the Indian states with the Indian Union in 1947, he decided to opt for the Indian Army. On 15 January 1949, he was granted a permanent commission in the Indian Army and assigned to 3 Gorkha Rifles. Since he had done staff course, he was posted to HQ Delhi Area as GSO 2 (Ops).

                        <br><br>
                       
                        After a short tenure at Delhi, Sagat was posted as BM to 168 Infantry Brigade, in Chhamb. Subsequently, in 1954, he was reverted to regimental service and posted as second-in-command, 3/3 Gorkha Rifles, at Bharatpur. In February 1955, Sagat was promoted to the rank of Lt Col and was given command of 2/3 Gorkha Rifles, then at Ferozepur. In October 1955, the battalion moved to Jammu and soon thereafter, Sagat was nominated for the senior officers course, at Infantry School, Mhow. After the course, he was posted as CO 3/3 Gorkha Rifles in February 1956. In November 1957, he was posted to the Infantry School as a senior instructor. In 1959, he was promoted to Col and posted as Deputy Director, Personnel Services, in Adjutant General’s Branch at the Army HQ.
                    </p>
                    <img src="../assets/img/marshal-2.jpg" class="img-fluid" width="100%" alt="" loading="lazy">
                    <p>After a short stint in Delhi, Sagat was promoted to the rank of Brig and given command of 50 Para Brigade, at Agra, in September 1961. This was unprecedented since he was not a paratrooper and would have to earn his ‘wings' before he could become one. He did the probation course and to save time sometimes did two jumps a day, getting his 'wings' in record time. It was while commanding 50 Para Brigade that Sagat’s genius as a combat leader came to the fore.
                        <br><br>
                        The operation for the liberation of Goa, code-named 'Vijay', commenced on 18 December 1961. The main force, comprising 17 Infantry Division, was to move into Goa from the east, while 50 Para Brigade, under Brig Sagat Singh, was to mount a subsidiary thrust from the north. The main thrust could not maintain its momentum due to the advance on foot and abnormally large bridging column which was following the leading brigade. Surprise having been lost, the Portuguese had demolished the bridges on the rivers. The paratroopers made excellent progress, undeterred by the obstacles, which were crossed using ingenious methods. They reached Panjim within 24 hours, leading to the surrender of Portuguese forces and capture of Goa. The paratroopers succeeded because of better fighting spirit, morale and leadership. The ability to take risks and seize fleeting opportunities is the hallmark of a successful military leader, and Sagat proved beyond doubt that he had these qualities in ample measure.			After doing the course at the National Defence College, New Delhi, and a short tenure as Brigadier General Staff 11 Corps, in July 1965 he was promoted to Maj Gen and posted to Sikkim as GOC 17 Mountain Division. Soon afterwards, in response to the Chinese ultimatum, the corps HQ gave orders to vacate the post at Nathu La. However, Maj Gen Sagat Singh refused to do so as he felt that the discretion to vacate the post lay with the divisional commander and not the corps HQ.
                        
                    </p>
                    <div class="leader-wrapper">
                    <img src="../assets/img/marshal-3.jpg" class="img-fluid" alt="" loading="lazy">
                    <img src="../assets/img/marshal-3.jpg" class="img-fluid" alt="" loading="lazy">
                    <img src="../assets/img/marshal-3.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>
                    <p>Throughout 1966 and early 1967, Chinese propaganda, intimidation and attempted incursions into Indian territory continued. The border was not marked and patrols which walked along the border often clashed, resulting in tension and even casualties. In 1967, Sagat Singh obtained the concurrence of the corps commander to mark the boundary. He ordered a double wire fence to be erected from Nathu La towards the north and south shoulders. However, as soon as work began on the fence on 20 August 1967, the Chinese became agitated and asked the Indians to stop. There were scuffles between Chinese and Indian troops working on the fence almost daily, but the work continued.
                        <br><br>
                        As Sagat was determined to complete the fence, it was decided to start work early on 11 September and finish the job before first light. All available manpower, including a platoon of engineers and another of pioneers, was deployed for the task. A company of 18 Rajput was also brought in, to reinforce the position and protect the men who were constructing the fence. As soon as work commenced, the Chinese came up to the fence and tried to stop the work. There was a heated discussion between the Chinese commander, accompanied by the political commissar, and Lt Col Rai Singh, CO 2 Grenadiers. As the argument became more heated, tempers rose, but both sides stood their ground. Suddenly, the Chinese opened fire, causing several casualties among the troops working on the fence. Lt Col Rai Singh was also hit by a Chinese bullet and fell down.
                        <br><br>
                        Seeing their CO fall, the Grenadiers were enraged. In a fit of fury, they came out of their trenches and attacked the Chinese post, led by Capt PS Dagar. The company of 18 Rajput, under Maj Harbhajan Singh, and the engineers working on the fence had been caught in the open and suffered many casualties. Harbhajan led his men in a charge on the Chinese position. Several of the Indian troops were mowed down by Chinese machine guns, but those who reached the Chinese bunkers used their bayonets and accounted for many of the enemy. Both Harbhajan and Dagar lost their lives in the action, which developed into a full-scale battle. Sagat had asked for some medium guns and these were moved up to Kyangnosa La, at a height of over 10,000 feet. The artillery observation posts, which Sagat had sited earlier, proved their worth in bringing down effective fire on the Chinese. There were several instances of troops deserting their posts. At one stage, Sagat borrowed a sten from another officer, and with the help of the Subedar Major, pushed the men back into the trenches.
                        <br><br>
                        The skirmish lasted almost three days. Indian casualties in the action were just over 200, that is, 65 dead and 145 wounded. The Chinese, it was estimated, suffered about 300 casualties.  Though the action taken by Sagat—in marking the border with a wire fence—had the approval of higher authorities, the large number of casualties suffered by both sides created a furore. Sagat, however, was not perturbed. For the last two years, the Chinese had been inciting trouble and had killed several Indian soldiers. With the spectre of Chinese attack of 1962 still haunting the military and political leadership in India, no effective action against them had been taken. This was the first time that the Chinese got a bloody nose and the myth of their invincibility was broken.
                        <br><br>
                        In December 1967, Sagat was asked to move post-haste to 101 Communication Zone Area, which was involved in counter-insurgency operations against Mizo hostiles. He came to know later that the Army Commander, Gen Sam Manekshaw, had specifically asked for him to sort out the problem. After visiting each battalion and analysing the encounters that had taken place, he was able to pinpoint three major reasons for the reverses suffered by the Indian troops: lack of intelligence; inability of infantry battalions to adapt to insurgency situations; and ill treatment of the locals by a few post commanders. In his typical style, he devised his own intelligence gathering system by compromising some of the important executives in the organisation of the hostiles. Instead of jail, they were kept near the base at Aizwal, where Sagat had already moved his tactical HQ. Their families were also brought there, and they began to help by giving intelligence reports, identifying hostiles during cordon and search operations and translating captured documents.
                        <br><br>
                        To adjust the training of the infantry battalions to suit the peculiar requirements of counter-insurgency operations, an ad hoc training camp was started with a few officers who had experience of fighting the hostiles. This later became the Counter Insurgency and Jungle Warfare School at Vairangte, Mizoram. All units which were inducted into the area had to, first, undergo an orientation course at this school. He issued strict orders against harassment and ill treatment of the population, and exemplary punishment was meted out to erring post commanders. In order to remove the feeling of neglect due to shortage of food, medical care and other facilities, and also to improve security, Sagat decided to group the villages, astride the only road in the region, between Aizwal and Lungleh. There were strong objections from the civil administration, on legal and administrative grounds, but Sagat was able to overcome these hurdles and carried out the grouping as planned.
                        <br><br>
                        Sagat stayed in Shillong for more than three years. During his tenure, he was able to tackle the problem of Mizo insurgency to a considerable degree. By the time he left the Mizo Hills, peace had returned, with all hostile gangs either being liquidated or having taken shelter in East Pakistan. In recognition of his splendid performance in controlling insurgency in the Mizo Hills, Sagat was awarded the Param Vishisht Seva Medal (PVSM), the highest non-gallantry award for soldiers in India. 
                        <br><br>
                        In November 1970, Sagat was promoted to Lt Gen and given command of 4 Corps, which had its headquarters at Tezpur. This was his third tenure in the east, in succession. By this time, Gen Sam Manekshaw had taken over as COAS and it was again at his behest that Sagat was chosen for this assignment. It proved to be an appropriate choice since 4 Corps, under Sagat's command, was to play a pivotal role a year later. The liberation of Bangladesh in 1971 was one of the Indian Army's finest hours. The lightning campaign, lasting just 14 days, resulted in the total annihilation of Pakistani forces and a magnificent victory for India.
                        <br><br>
                        The operation commenced on 4 December 1971, after Pakistan launched air strikes on a number of Indian airfields in the early hours of the morning of the previous day. The task given to 4 Corps was to advance from the east and capture all territory east of river Meghna. The Pakistanis blew up the bridge over the Meghna to prevent Indian troops from advancing.
                        <br><br>
                        Sagat decided to heli-lift his troops across the Meghna and make for Dacca. The air lift operations began on the afternoon of 9 December and continued for the next 36 hours. A total of 110 sorties were flown, from the Brahmanbaria stadium, across the Meghna, which was 4,000 yards wide, to land at helipads which had been marked by torches, with their reflectors removed. During day, the troops were landed in paddy fields, with helicopters hovering low above the ground. The first battalion of 311 Mountain Brigade, 4 Guards, was landed in Raipura, while 9 Punjab crossed the river using country boats. Next day, the troops were landed directly at Narsingdi. By 11 December, both 311 and 73 Mountain Brigades had crossed the Meghna and were ordered to advance to Dacca, on different axes. Using all modes of transport, including bullock carts and cycle rickshaws, both brigades advanced rapidly, and on 14 December, the first artillery shell was fired on Dacca. On 15 December, orders were received from HQ Eastern Command to halt further advance. On December 16, the ceasefire was declared.
                        <br><br>
                        Sagat’s decision to cross the Meghna proved to be crucial to the entire operation. This was also the first instance in military history of an 'air-bridge' being used for crossing a major water obstacle, by a brigade group. It was a bold decision, fraught with risk, and if he had failed, the responsibility would have been entirely his. However, battles are not won by those with weak hearts. Every military operation is a gamble and stakes are invariably high. Sagat was one of those who played for the jackpot and won. 

                        <br><br>

                        Sagat's contribution in the liberation of Bangladesh was recognised by the award of Padma Bhushan, a non-gallantry award which is normally given to civilians. It was ironical that the most successful corps commander in the 1971 war had to be content with a civilian award, while several others whose performance was much below par were decorated for gallantry and became war heroes.
                        
                        <br><br>
                        In November 1973, after commanding 4 Corps for exactly three years, Sagat was transferred and given command of 1 Corps. He retired in November 1974 and moved to Jaipur. He breathed his last in the Army Hospital, Delhi, on 26 September 2001
                        <br><br>

                        Sagat was a commander who led from the front. He epitomised the traditional image of the military leader, who fights and leads by example. For those who knew him intimately or had the fortune to serve under him, Sagat Singh was the type of military leader who soldiers follow willingly and give their lives for. 
                    </p>
                </div>
            </div>
        </div>
    </section>


    <?php get_footer(); ; ?>