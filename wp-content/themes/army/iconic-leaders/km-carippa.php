<?php 


$main ="iconic";

$page="cariappa";

get_header(); ; ?>

    <section class="iconic-leaders-detail-banner"
        style="background-image: url(../assets/img/iconic-leaders-detail-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Iconic leaders</h1>
        </div>
    </section>

    <section class="iconic-leaders-detail">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-9 res-leader">
                    <img src="../assets/img/kriyappa.jpg" class="img-fluid" width="100%" alt="" loading="lazy">
                    <h2>FIELD MARSHAL K.M.CARIAPPA, OBE</h2>
                    <p>Kodandera Madappa Cariappa has been the epitome of military leadership for generations of Indians. As the first Indian COAS, he laid the foundations of the modern Indian Army. ‘Kipper’, as he was affectionately called, was fiercely patriotic and known for his iron discipline, integrity, forthright views and love for the Indian soldier or jawan. No wonder, the troops loved and worshipped him. For a military leader, there can be no greater approbation.</p>

                            
                    <p>Cariappa was born on 28 January 1900, at Madikeri (Mercara) in Coorg. After his schooling, he joined the Temporary School for Indian Cadets at the Daly College in Indore, which had been opened after the outbreak of World War I. His batch passed out on 1 December 1919, but was granted permanent commission with effect from 17 July 1920, to account for the shortfall in training as compared to the Sandhurst trained officers. Cariappa's batch was the only one to pass out from Indore because subsequent batches were trained at Sandhurst.</p>
                            
                    <p> After doing stints in Mesopotamia and Waziristan with different units, Cariappa joined 1/7 Rajput, which became his parent unit. He was the first Indian officer to attend the course at the Staff College, Quetta, in 1934. In 1942, he was promoted Lieutenant Colonel (Lt Col) and given command of 17/7 Rajput in Secunderabad. Subsequently, he was posted as Assistant Quarter Master General (AQMG), 26 Indian Division, then located near Buthidaung in Burma. It was a staff job, but was on the battlefront. The division was instrumental in pushing the Japanese back from the Arakan. Here, Cariappa did an excellent job and was awarded the Order of the British Empire (OBE) for his services in 1945.</p>

                    <img src="../assets/img/marshal-2.jpg" class="img-fluid" width="100%" alt="" loading="lazy">

                    <p>In November 1945, Cariappa, who had been promoted to the rank of Brig in 1944, was given command of the Bannu Frontier Brigade in Waziristan. In January 1947, he was sent to the United Kingdom (UK) to attend the Imperial Defence College. Cariappa could not complete the course as he was recalled in July to supervise the reorganisation of the army before Partition. On 15 August 1947, the day India became independent, Cariappa was promoted to Maj Gen and appointed Deputy Chief of the General Staff, at Army HQ. He saw, first hand, the traumatic events which followed the Partition of the country.</p>
                            
                        <p>In November 1947, Cariappa was promoted to Lt Gen and took appointed as GOC-in-C Eastern Command. After two months, he was posted as GOC-in-C Delhi and East Punjab Command, which was then located at Delhi. One of Cariappa’s first acts was to rename it as Western Command. He soon took control of the situation and selected Maj Gen KS Thimayya to replace Maj Gen Kalwant Singh, as GOC JAK Force, which was renamed Sri Division (later 19 Division), at Srinagar. Maj Gen Atma Singh was appointed GOC Jammu Division (later 25 Division), at Jammu. Cariappa also moved his own HQ to Jammu and raised a corps HQ at Udhampur, under Lt Gen SM Shrinagesh, to command all operations in Jammu and Kashmir. </p>
                            
                        <p>Cariappa had some of his finest hours during the Kashmir operations. Operation Kipper, for the capture of Naushera and Jhangar, was planned by him, and it succeeded. This was followed by Operation Easy, for the link up with Punch, and Operation Bison, for the capture of Zojila, Dras and Kargil. If he had been given additional troops and the necessary permission by the government, he would have succeeded in pushing the Pakistanis out of Kashmir, for which plans had been made. Unfortunately, this did not come about due to the intervention of the United Nations (UN), after an appeal by India.</p>


                    <div class="leader-wrapper">
                    <img src="../assets/img/marshal-3.jpg" class="img-fluid" alt="" loading="lazy">
                    <!-- <img src="../assets/img/marshal-3.jpg" class="img-fluid" alt="" loading="lazy">
                    <img src="../assets/img/marshal-3.jpg" class="img-fluid" alt="" loading="lazy"> -->
                    </div>


                    <p>It is indeed commendable that Cariappa succeeded in achieving what he did. As a result of its defensive policy, India lost several key objectives in Uri and Tithwal sectors. Since the road to Ladakh could not be opened till Zojila, Dras and Kargil were captured, Cariappa decided to do so, on his own initiative. He took a grave risk by disobeying orders, which forbade all offensive operations. The country owes an eternal debt to Cariappa for the risks he took. If he had failed, it would have ended his career. </p>
                            
                        <p>On 15 January 1949, Cariappa succeeded Gen Roy Bucher as COAS and Commander-in-Chief (C-in-C), Indian Army (the designation of C-in-C was discontinued from 1 April 1955). After almost 200 years of British rule, an Indian had finally assumed command of the Indian Army. To mark this historic occasion, 15 January was declared as the official Army Day in India.</p>
                            
                        <p>Soon after he took over as C-in-C, Nehru told Cariappa that one of his important tasks was to bring the army closer to the people. During British rule, the army, as an instrument of power, had been deliberately kept insulated from the public. Cariappa agreed with Nehru's views and took several measures in this direction. The National Cadet Corps had already been formed in October 1948, but it was Cariappa who gave it his whole-hearted support during its formative years. It was due to his efforts that the Territorial Army was established in 1949. Cariappa also did away with the concept of martial races and within two weeks of his taking over, fixed percentages for recruitment were abolished and it was opened to all classes. However, this was made applicable only to new raisings; the older units were not disturbed. The Brigade of Guards, which was raised in August 1949, was open to all classes. It was an elite force of handpicked men, modelled on the Coldstream Guards in the UK, with whom Cariappa had been attached in 1932. Four of the senior-most battalions of the Indian Army were converted to Guards, which was made the senior-most regiment of infantry.</p>
                            
                        <p>Cariappa served as C-in-C for four years, retiring on 14 January 1953. His greatest achievement was keeping the Indian Army apolitical and establishing healthy traditions. Unlike Pakistan and Burma, which achieved independence from British rule at about the same time, the Indian Army has stayed out of politics, even during times of crisis. Most of the credit for this must go to Cariappa. In fact, he refused to take back Indian National Army (INA) personnel primarily for this reason, since he was convinced that they would bring politics into the army. There was a lot of pressure on him regarding this decision, and Nehru relented only after Cariappa threatened to resign on this issue. However, he adopted the slogan ‘Jai Hind’, used by the INA, and ended all his talks with this. 'Jai Hind' soon became the Indian Army's slogan as well as form of greeting between men and officers</p>
                            
                        <p>Cariappa also had his brushes with Nehru. He had foreseen the Chinese threat and wanted to defend the border more effectively. In May 1951, he presented an outline plan for the defence of the North-East Frontier Agency (NEFA). Nehru dismissed his plans, adding that it was not the business of the C-in-C to tell the PM how to defend the country. He advised Cariappa to only worry about Pakistan and Kashmir; and as far as NEFA was concerned, the Chinese themselves would defend India’s frontiers!</p>
                            
                        <p>Soon after his retirement, Cariappa was offered the job of Indian High Commissioner in Australia by PM Nehru. He accepted it after some deliberation and set sail for Sydney in July 1953. When Cariappa arrived at Canberra, the Governor General, Field Marshal Slim, broke protocol and called on Cariappa at his residence, even before he had presented his credentials. Cariappa returned to India in 1956, and retired to his house in Mercara in Coorg. </p>
                            
                        <p>In 1986, the government decided to confer the rank of Field Marshal on Cariappa. The decision stemmed from the deep sense of respect and esteem in which Cariappa was held by all sections of Indian society. Cariappa graciously accepted the honour. On 28 April 1986, at a special investiture ceremony held at the Rashtrapati Bhawan, he was presented the Field Marshal's baton by President Zail Singh. In deference to his age—he was 86—he was offered a chair while the citation was being read out. True to his character, Cariappa declined the offer and stood ramrod straight throughout the ceremony. </p>
                            
                        <p>Cariappa's health deteriorated after 1991. He was shifted to a cottage in the Command Hospital at Bangalore. He died in his sleep, peacefully, on 15 May 1994. Two days later, his mortal remains were cremated at his ancestral home at Madikeri. The cremation had all the ceremony and pomp which befitted a Field Marshal and the three service Chiefs, along with Field Marshal Sam Manekshaw, were in attendance. Many of the mourners, including some soldiers in uniform, had tears in their eyes as they bade farewell to the man who they called the 'Father of the Indian Army. </p>
                            
                        <p>Cariappa had become a living legend even before he rose to the highest military rank. He had a strong character and sense of values. Although a strict disciplinarian, he was always just and fair, and even those who felt the rough end of his stick vouch for this. The Indian nation owes him an eternal debt for his contributions, which are too numerous to recount.</p>
                        
                </div>
            </div>
        </div>
    </section>


    <?php get_footer(); ; ?>