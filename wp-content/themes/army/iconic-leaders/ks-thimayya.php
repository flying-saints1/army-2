<?php 


$main ="iconic";

$page="thimayya";


get_header(); ; ?>

    <section class="iconic-leaders-detail-banner"
        style="background-image: url(../assets/img/iconic-leaders-detail-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Iconic leaders</h1>
        </div>
    </section>

    <section class="iconic-leaders-detail">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-9 res-leader">
                    <img src="../assets/img/thimayya-1.jpg" class="img-fluid" width="100%" alt="" loading="lazy">
                    <h2>GENERAL KS THIMAYYA</h2>
                    <p>Kodandera Subayya Thimayya is one of India’s most well-known military leaders. The first Indian to command a brigade in battle during World War II, Timmy, as he was popularly known, became a legend in his lifetime.
                        <br><br>
                        Thimayya was born on 31 March 1906 in Mercara, Coorg. He did his schooling in St.
                        Joseph’s College, Coonoor, and Bishop Cotton School, Bangalore. In 1922, he joined Prince of Wales Royal Indian Military College, which had just opened at Dehradun to train Indian
                        cadets for Sandhurst. After passing a stiff written examination, followed by interviews with
                        the Chief of General Staff (CGS) and the Viceroy, Thimayya joined Sandhurst in 1924. He
                        passed out on 4 February 1926 and joined 2nd Battalion, the Highland Light Infantry, in
                        Bangalore, where he was attached for a year. Next, he was posted to 4th Battalion, 19
                        Hyderabad Regiment, then stationed in Baghdad.
                        <br><br>
                        In 1928, Thimayya’s battalion moved to Allahabad, where he came in close contact
                        with the Nehrus and was a frequent guest at Anand Bhawan. He also met Dr Kailash Nath
                        Katju, Sir Tej Bahadur Sapru as well several other prominent citizens of Allahabad. In 1930,
                        the Civil Disobedience Movement was launched by Mahatma Gandhi and there was a general upsurge of nationalist feeling among the people. Thimayya&#39;s battalion was often given duties involving maintenance of law and order and doing flag marches. In the same year, he was appointed Adjutant, a post normally given to a senior Capt or a Maj, while he was still a
                        subaltern. In 1931, the battalion moved to Fort Sandeman, in the North-West Frontier
                        Province (NWFA), and then to Quetta in 1934.
                    </p>
                    <img src="../assets/img/general.jpg" class="img-fluid" width="100%" alt="" loading="lazy">
                    <p>In 1936, Thimayya took up his new assignment in Madras, as Adjutant of 5 <br> Madras

                        <br>
                        University Battalion of the University Training Corps. After spending four years in Madras, he was ordered to rejoin his battalion, 4/19 Hyderabad, which was moved to Singapore as World War II had begun. In August 1941, he was transferred to Agra, to 8/19 Hyderabad Regiment, which was being raised. Two months later, he was transferred to 10/19 Hyderabad Regiment, which was also being raised at the same station.
                        out of Kashmir.
                        <br><br>
                        n July 1942, Thimayya was appointed the second-in-command of the battalion. After a short stint at Agra, he left for Quetta, for the Staff College course, in February 1943.
                        <br><br>
                        After the course, he was posted as General Staff Officer (GSO) 2 (Ops), HQ 25 Indian Division, in Madras. The division soon moved to Burma, to replace 5 Indian Division, which had fought in Kohima and Imphal. In May 1944, he was promoted to the rank of Lt Col and given command of 8/19 Hyderabad, which was part of the ‘All Indian’ 51st Brigade, known thus because all its three battalions and their COs were Indian—16/10 Baluch was commanded by Lt Col LP Sen and 2/2 Punjab by Lt Col SPP Thorat. From Maungdaw, the brigade was ordered to advance to Buthidaung in December 1944. In January 1945, it took part in the famous battle of Kangaw, where the brigade commander and three COs were all awarded Distinguished Service Orders (DSOs). On 25 March 1945, Thimayya was given command of 36 Infantry Brigade, which was part of 26 Indian Division. He became the first Indian to command a brigade in the field.
                        <br><br>

                        After World War II ended, Thimayya was given command of 268 Brigade, which was sent to Japan as part of the British Commonwealth Force. He returned to India in December 1946 to serve on the Indian Armed Forces Nationalisation Committee, which had been set up on 13 November 1946. Soon afterwards it was announced that transfer of power would take place earlier than expected, and the recommendations of the Committee became redundant.
                    </p>
                    <div class="leader-wrapper">
                    <img src="../assets/img/general-gallery.jpg" class="img-fluid" alt="" loading="lazy">
                    <!-- <img src="../assets/img/general-gallery.jpg" class="img-fluid" alt="" loading="lazy">
                    <img src="../assets/img/general-gallery.jpg" class="img-fluid" alt="" loading="lazy"> -->
                    </div>
                    <p>To supervise large-scale movement of people from Punjab after the Partition and

                        
                        prevent violence, a Boundary Force—based on a division—was created, with its HQ at Lahore. Thimayya was appointed commander of 5 Brigade, located at Amritsar, which was part of this force. After a few months, Thimayya was transferred to 11 Brigade, located at Jullunder, which was also part of the Boundary Force. A little later, Thimayya was given command of the force, with the rank of Maj Gen.
                        <br><br>
                        As mentioned earlier, in October 1947, Pakistani raiders entered the Kashmir Valley. Gradually, the momentum of the raids increased, leading to a build-up of Indian troops. Jammu and Kashmir (JAK) Force, comprising two brigades, was created under the command of Maj Gen Kalwant Singh. In April 1948, Thimayya took over from Kalwant, as GOC JAK Force. A few days later, the JAK Force was split and two divisions were created. Sri Div was to be located at Srinagar, to look after the defence of the Kashmir Valley, while JAK division, based at Jammu, was to look after the Jammu region. Thimayya was given command of Sri Div and was moved to Srinagar on 4 May 1948.
                        <br><br>
                        At this juncture, the government decided to cease offensive operations as the case had been referred to the UN. The decision to suspend offensive operations came as a shock and Cariappa protested strongly to the government. Finally, the government approved that ‘defensive operations’ could be undertaken for the link up with Leh and Punch, which had to be held at all costs. However, the road to Leh could be opened only after capture of Dras, Zojila and Kargil, which were held by the enemy. In September 1948, Maj Gen SM Shrinagesh was promoted to Lt Gen and appointed GOC V Corps, which was created to look after all operations in the theatre.
                        <br><br>
                        At this time, Leh was held only by a weak battalion of the state forces. On 22 May 1948, the enemy attacked the bridge at Khalatse, and the detachment guarding it pulled back to Leh. Next day, Maj Prithi Chand, who was in Leh, sent an urgent message that the situation was critical and if reinforcements did not reach next day, Leh would have to be evacuated. No aircraft had ever landed at Leh before, but if the town was to be saved, this was the only answer. Air Commodore Mehar Singh, accompanied by Thimayya, landed at Leh in a Dakota on 24 May 1948, writing their names in the history books of aviation. Leh was saved, and troops were flown in regularly during the next few days.
                        <br><br>
                        The capture of Zojila was another exploit which made Thimayya famous. After two failed attempts, it became necessary to try a different method to achieve success. On 23 September 1948, a conference was held at Srinagar, which was attended by Cariappa, Thimayya, Shrinagesh and Brig KL Atal, in which the decision to use tanks was taken. Thimayya is credited with the suggestion, which Cariappa approved. Lt Col Rajinder Singh ‘Sparrow’, who was commanding 7 Cavalry, was called in and consulted, before the decision was finalised.
                        <br><br>
                        A squadron of Stuart light tanks of 7 Cavalry was moved from Jammu to Srinagar,
                        and then to Baltal. Many bridges had to be reinforced, or rebuilt, by the engineers. To maintain secrecy, the turrets of the tanks were removed and carried separately. A curfew was imposed in Srinagar when the column was passing through the city. The movement was not detected and the tanks reached Baltal, covering a distance of 260 miles, in a fortnight. When the attack was launched, the presence of tanks completely surprised and unnerved the enemy.
                        Thimayya was himself in the first tank, leading the assault. The operation was a complete success and Zoji La was captured by nightfall. Shortly afterwards, Dras and Kargil were secured, and a link up established with a column pushed out from Leh on 24 November 1948.
                        With this, the threat to Leh and the entire Ladakh region had been removed. A ceasefire was ordered on 1 January 1949 after Pakistan agreed to accept UN resolution 47 of 21 April 1948.
                        The war in Kashmir was officially over, after almost 15 months of hard fighting.
                        <br>

                        In November 1949, Thimayya was posted as Commandant, NDA, then located at Dehradun. In September 1951, he moved to Delhi as Quarter Master General (QMG). In January 1953, he was promoted to the rank of Lt Gen and posted as GOC-in-C Western Command. He had hardly settled down before he received orders, in May 1953, appointing him Chairman of the Neutral Nations Repatriation Commission (NNRC) in Korea. Thimayya completed his assignment in April 1954 and returned to India. Both sides agreed that he had been neutral and fair. PM Nehru personally commended Thimayya, who was awarded the Padma Bhushan
                        <br>
                       
                        In May 1955, Thimayya was appointed GOC-in-C Southern Command. In September 1956, he moved to the Eastern Command, thus becoming the first officer to command all three field armies in India. With Gen S.M. Shrinagesh, the COAS, due to retire in May 1957, Thimayya was selected for the top job in the army. On 8 May 1957, he took over as the COAS.
                        <br><br>
                        
                        Thimayya began his tenure as the Army Chief with immense prestige. He enjoyed a close rapport with the PM and was held in high esteem both within and outside the army. His assignment as Chairman of the NNRC had also made him an international figure. However, soon a rift developed between him and the then Defence Minister, V.K. Krishna Menon, who was a close confidant of Nehru. Menon, known not only for his brilliance but also arrogance and acerbic tongue, realised that Thimayya was not as pliant as he had expected him to be.
                        <br><br>

                        The matter came a to head on 31 August 1959, when Thimayya resigned. The PM called Thimayya and playing on his emotions, persuaded him to withdraw his resignation. He also promised to put things right between him and Menon, but this did not happen. News of Thimayya’s resignation somehow leaked to the press and was extensively covered by all newspapers on 1 September 1959. The issue was also raised in the Parliament and several members demanded a statement from the defence minister. On 2 September 1959, Nehru gave a statement in the Parliament. He underplayed the importance of the issues raised by Thimayya, calling them ‘trivial, and of no consequence’. He added that the difficulty seemed to be ‘temperamental’, and went on to say that he had advised Gen Thimayya to have a talk with the defence minister, which he had done. He also defended the actions of the defence minister in the matter of promotions, which appeared to be the real irritant, and clarified that the correct procedures had been followed. In the end, he paid a tribute to Krishna Menon, praising him for his energy and enthusiasm.
                        <br><br>
                        The Prime Minister’s statement did not satisfy the House. Several prominent members, such as Acharya Kripalani, N.G. Ranga, Frank Anthony and Ashok Mehta, were not happy and felt that Nehru had not only belittled Thimayya but, by praising Krishna Menon, had also congratulated the wrong man. Nehru, however, did not budge from his stand. It was widely felt that Nehru had not been fair. The top brass in the army was aghast and expected Thimayya to insist on being relieved. Surprisingly, he did not do so. As a result, his prestige and authority suffered.
                        <br><br>
                        Thimayya retired on 8 May 1961. Though he recommended Lt Gen SPP Thorat as his successor, the government ignored his advice and appointed Lt Gen PN Thapar as the Army Chief. It was an unfortunate choice as Thapar could neither stand up to Krishna Menon nor control the unbridled ambition of Maj Gen BM Kaul, who began to run the army like a fiefdom. On the eve of his retirement, Thimayya spoke to the men in words that proved prophetic. He said, ‘I hope I am not leaving you as cannon fodder for the Chinese...God bless you all.

                        <br><br>

                        In June 1964, the UN Secretary-General, U. Thant, invited Thimayya to become commander of the UN forces in Cyprus. He accepted the appointment and left for Cyprus, via New York, on 30 June 1964. His term of office, which was for three months, continued to be extended whenever it was to expire. He was held in high esteem by the President of Cyprus, Archbishop Makarios. Gradually, even the Turks, who had initially doubted his bona fides, grudgingly admitted that he was fair.

                        <br><br>
                        On 18 December 1965, Thimayya died of a heart attack in Nicosia. His body, along with a 10-man guard of honour drawn from the UN troops, was flown in a special UN aircraft from Nicosia to Beirut. From Beirut, the body was flown to Bombay in an Air India plane, where it was received with due ceremony, before being transferred to an Indian Air Force aircraft for its final journey to Bangalore. He was buried at the Lal Bagh gardens, with full military honours, accompanied by a 17-gun salute.

                        <br><br>

                        Thimayya remains one of the most popular military leaders of India. His no-nonsense approach, sense of humour and moral courage earned him the love and respect of the Indian jawans. To them, he was always ‘Timmy Sahib, whom they loved and respected like an elder brother. Thimayya&#39;s visits to units, known as ‘Timmy tonic’, raised spirits as nothing else could. This was no mean accomplishment and one which few generals have achieved, in India or abroad. A ‘soldiers’ general’, he was a true son of the motherland, who fought and died for her honour.
                    </p>
                </div>
            </div>
        </div>
    </section>


    <?php get_footer(); ; ?>