<?php
/*
*
* Template Name: Operations template
*
*/

$main ="operaitons";

$page="lico";


get_header();?>


    <section class="operations-banner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/operations-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Operations</h1>
        </div>
    </section>

    <section class="operations-war">
        <div class="container">
            <div class="row">
                <div class="col-4 full-wdt">
                    <div class="operations-war-box">
                    <a href="<?php // echo $currentDomain;   ?>/operations/operation-polo-1948.php">
                        <div class="operations-war-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation1.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="operations-war-details">
                            <h2 class="head">Operation Polo <br> 1948</h2>
                       
                            <p class="text">When India gained independence from the British in 1947, the various princely...
                            </p>
                            <div class="war-right-arrow">
                                <h3 class="sm-text">Read More</h3>
                                <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-4 full-wdt">
                    <div class="operations-war-box">
                    <a href="<?php // echo $currentDomain;   ?>/operations/operation-vijay-1961.php">
                        <div class="operations-war-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation2.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="operations-war-details">
                            <h2 class="head">Operation Vijay <br>1961</h2>
                            <p class="text">The state of Goa became part of the Indian Union in 1961, a decade and a half after independence...
                            </p>
                            
                            <div class="war-right-arrow">
                                <h3 class="sm-text">Read More</h3>
                                <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-4 full-wdt">
                    <div class="operations-war-box">
                        <a href="<?php // echo $currentDomain;   ?>/operations/sikkim-skirmish-at-nathu-la-1967.php">
                        <div class="operations-war-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation3.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="operations-war-details">
                            <h2 class="head">Sikkim: Skirmish at Nathu <br>La 1967</h2>
                        
                            <p class="text">The Nathu La pass lies on the Old Silk Route between Tibet and India. In 1904...
                            </p>
                            <div class="war-right-arrow">
                                <h3 class="sm-text">Read More</h3>
                                <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                            </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-4 full-wdt">
                    <div class="operations-war-box">
                    <a href="<?php // echo $currentDomain;   ?>/operations/siachen-operation-meghdoot-1984.php">
                        <div class="operations-war-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation4.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="operations-war-details">
                            <h2 class="head">Siachen: Operation Meghdoot <br> 1984</h2>
     
                            <p class="text">Located in the eastern Karakoram range in the Himalayas, the Siachen Galcier...
                            </p>
                           
                            <div class="war-right-arrow">
                                <h3 class="sm-text">Read More</h3>
                                <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-4 full-wdt">
                    <div class="operations-war-box">
                    <a href="<?php // echo $currentDomain;   ?>/operations/operation-pawan-1987.php">
                        <div class="operations-war-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation5.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="operations-war-details">
                            <h2 class="head">Operation Pawan: Indian Peace Keeping Force, 1987</h2>
                            <p class="text">Sri Lanka is an island nation in the Indian Ocean, separated from the Asian mainland...
                            </p>
                            <div class="war-right-arrow">
                                <h3 class="sm-text">Read More</h3>
                                <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-4 full-wdt">
                    <div class="operations-war-box">
                    <a href="<?php // echo $currentDomain;   ?>/operations/maldives-operation-cactus-1998.php">
                        <div class="operations-war-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation6.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="operations-war-details">
                            <h2 class="head">Maldives: Operation <br>Cactus 1998</h2>
                      
                            <p class="text">Maldives is an island nation in the Indian Ocean, composed entirely of about 1,190...
                            </p>
                            <div class="war-right-arrow">
                                <h3 class="sm-text">Read More</h3>
                                <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-4 full-wdt">
                    <div class="operations-war-box">
                    <a href="<?php // echo $currentDomain;   ?>/operations/operation-vijay-kargil-1999.php">
                        <div class="operations-war-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation7.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="operations-war-details">
                            <h2 class="head">Operation Vijay:  Kargil<br> 1999</h2>
                            <p class="text">The Jammu and Kashmir (J&K) region has had political and cultural connections...
                            </p>
                            <div class="war-right-arrow">
                                <h3 class="sm-text">Read More</h3>
                                <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-4 full-wdt">
                    <div class="operations-war-box">
                    <a href="<?php // echo $currentDomain;   ?>/operations/lico-and-cI.php">
                        <div class="operations-war-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation8.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="operations-war-details">
                            <h2 class="head">LICO and CI Ops - <br> J&K, NE</h2>
                            <p class="text">The incidents in J&K may have manifested into a violent movement in 1988, however its roots...
                            </p>
                            <div class="war-right-arrow">
                                <h3 class="sm-text">Read More</h3>
                                <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-4 full-wdt">
                    <div class="operations-war-box">
                    <a href="<?php // echo $currentDomain;   ?>/operations/peace-keeping-ops.php">
                        <div class="operations-war-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation9.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="operations-war-details">
                            <h2 class="head">Peace Keeping Ops</h2>
                            <p class="text">Since the inception of the first UN peacekeeping mission in 1948...
                            </p>
                            <div class="war-right-arrow">
                                <h3 class="sm-text">Read More</h3>
                                <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-4 full-wdt">
                    <div class="operations-war-box">
                    <a href="<?php // echo $currentDomain;   ?>/operations/humanitarian-and-disaster-relief.php">
                        <div class="operations-war-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation10.jpg" class="img-fluid" alt="" loading="lazy">
                        </div>
                        <div class="operations-war-details">
                            <h2 class="head">Humanitarian and Disaster <br> Relief (HADR)</h2>
                            <p class="text">The Indian subcontinent is prone to earthquakes, floods, droughts, cyclones, etc...
                            </p>
                            <div class="war-right-arrow">
                                <h3 class="sm-text">Read More</h3>
                                <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy">
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <?php get_footer();?>