<?php

$main = "blog-page";

$page ="blog";

include('header.php')?>


<section class="iconic-leaders-detail-banner"
        style="background-image: url(assets/img/blog-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Blogs</h1>
        </div>
</section>


<section class="blogs " id="blog-page">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog1.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020  &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255  </p>
                        <div class="come-in">
                            <a href="blog-details.php" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="blog-details.php" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog2.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020 &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255 </p>
                        <div class="come-in">
                            <a href="blog-details.php" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="blog-details.php" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog3.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020  &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255  </p>
                        <div class="come-in">
                            <a href="blog-details.php" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="blog-details.php" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog1.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020  &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255  </p>
                        <div class="come-in">
                            <a href="blog-details.php" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="blog-details.php" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog2.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020 &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255 </p>
                        <div class="come-in">
                            <a href="blog-details.php" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="blog-details.php" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog3.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020  &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255  </p>
                        <div class="come-in">
                            <a href="blog-details.php" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="blog-details.php" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog1.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020  &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255  </p>
                        <div class="come-in">
                            <a href="blog-details.php" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="blog-details.php" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog2.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020 &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255 </p>
                        <div class="come-in">
                            <a href="blog-details.php" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog3.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020  &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255  </p>
                        <div class="come-in">
                            <a href="blog-details.php" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog1.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020  &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255  </p>
                        <div class="come-in">
                            <a href="" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog2.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020 &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255 </p>
                        <div class="come-in">
                            <a href="" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog3.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020  &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255  </p>
                        <div class="come-in">
                            <a href="" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog1.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020  &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255  </p>
                        <div class="come-in">
                            <a href="" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog2.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020 &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255 </p>
                        <div class="come-in">
                            <a href="" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog3.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020  &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255  </p>
                        <div class="come-in">
                            <a href="" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="assets/img/blog3.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020  &nbsp; <img src="assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255  </p>
                        <div class="come-in">
                            <a href="" class="title">
                                <h4>Lorem Ipsum is  simply <br>dummy ...</h4>
                            </a>
                            <a href="" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="icons">
    <div class="container">
        <div class="row center">
            <div class="col-md-12">
                <div class="pagination">
                    <a href="#"class="active">1</a>
                    <a href="#" >2</a>
                    <a href="#">3</a>
                </div>
            </div>
        </div>
    </div>
</div>



<?php include('footer.php')?>