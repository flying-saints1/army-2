<?php

/*
*
* Template Name: Video Gallery
*
*/

$main = "gallery";

$page ="video";

get_header(); ?>

<section class="iconic-leaders-detail-banner"
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/blog-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Video</h1>
        </div>

</section>


<section class="featured" id="video-page">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-6">
                <div class="featured-video" >
                <iframe id="ytplayer" width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" loading="lazy" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">
                        <p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt=""> &nbsp; April 30,2022 <br><img src="assets/img/icons/view.png" class="img-fluid" alt="" style="margin-top: 4px;"> &nbsp; 255</p>
                        <h5 class="card-title">Retiring Officers Seminar, April 2022</h5>
                        <p class="">General Manoj Pande #COAS conveys deep appreciation to all Retiring Officers for their invaluable contribution to the Nation and #IndianArmy. </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-6">
                <div class="featured-video" >
                <iframe id="ytplayer" width="100%" height="315" src="https://www.youtube.com/embed/3t5M10gBVCc" title="YouTube video player" frameborder="0" loading="lazy" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">
                        <p><img src="assets/img/icons/date.png" class="img-fluid" alt=""> &nbsp; August 15,2015 <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" style="margin-top: 4px;"> &nbsp; 255</p>
                        <h5 class="card-title">Indian Army : A Life Less Ordinary</h5>

                        <p class="">Life In the Indian Army is not an ordinary life. It is full of challenges and opportunities and a chance to serve the motherland. Join us to be a part of this glorious life style.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-6">
                <div class="featured-video" >
                <iframe id="ytplayer" width="100%" height="315" src="https://www.youtube.com/embed/1CCFf4rej2E" title="YouTube video player" frameborder="0" loading="lazy" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">
                        <p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt=""> &nbsp; August 14,2021 <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" style="margin-top: 4px;"> &nbsp; 255</p>
                        <h5 class="card-title">General Bipin Rawat #CDS conveys best wishes</h5>

                        <p class="">General Bipin Rawat #CDS conveys best wishes to the Indian contingent participating in Paralympic Games #Tokyo2020.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-6">
                <div class="featured-video" >
                <iframe id="ytplayer" width="100%" height="315" src="https://www.youtube.com/embed/Y-Nsqg9La3c" title="YouTube video player" frameborder="0" loading="lazy" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">
                        <p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt=""> &nbsp; April 13,2022 <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" style="margin-top: 4px;"> &nbsp; 255</p>
                        <h5 class="card-title">Unfolding Opportunities, Challenges and Risks</h5>
                        <p class="">The growing importance of the Indian and Pacific oceans has given new momentum to the “Indo-Pacific” as a geostrategic construct. </p>
                    </div>
                </div>
            </div>

            <!-- <div class="col-md-4 col-6">
                <div class="featured-video" >
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/3t5M10gBVCc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">
                        <p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt=""> &nbsp; November 5,2022 <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" style="margin-top: 4px;"> &nbsp; 255</p>
                        <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>

                        <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-6">
                <div class="featured-video" >
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/1CCFf4rej2E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">
                        <p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt=""> &nbsp; November 5,2022 <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" style="margin-top: 4px;"> &nbsp; 255</p>
                        <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>

                        <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-6">
                <div class="featured-video" >
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">
                        <p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt=""> &nbsp; November 5,2022 <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" style="margin-top: 4px;"> &nbsp; 255</p>
                        <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                        <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-6">
                <div class="featured-video" >
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/3t5M10gBVCc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">
                        <p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt=""> &nbsp; November 5,2022 <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" style="margin-top: 4px;"> &nbsp; 255</p>
                        <h5 class="card-title" >Lorem Ipsum is simply <br> dummy </h5>

                        <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-6">
                <div class="featured-video" >
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/1CCFf4rej2E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">
                        <p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt=""> &nbsp; November 5,2022 <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" style="margin-top: 4px;"> &nbsp; 255</p>
                        <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>

                        <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</section>

<!-- <div class="icons">
    <div class="container">
        <div class="row center">
            <div class="col-md-12">
                <div class="pagination">
                    <a href="#"class="active">1</a>
                    <a href="#" >2</a>
                    <a href="#">3</a>
                </div>
            </div>
        </div>
    </div>
</div> -->


<?php get_footer(); ?>