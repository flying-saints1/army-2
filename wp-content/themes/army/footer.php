<section class="profile">
    <div class="container">
        <div class="row feed-mob-scrol">
            <div class="col-10 col-md-4 col-sm-12">
                <div class="profile-box">
                    <div class="profile-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/youtube.png" class="img-fluid" alt="" loading="lazy">
                    </div>
                    <div class="profile-desc">
                        <div class="image">
                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/3t5M10gBVCc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="profile-detail">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy">
                            <p class="date">November 5,2020</p>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy">
                            <p class="date">225</p>
                        </div>
                        <h3>Indian Army : A Life Less Ordinary</h3>
                        <p>Life In the Indian Army is not an ordinary life. It is full of challenges and opportunities and a chance to serve the motherland. Join us to be a part of this glorious life style.</p>
                    </div>
                </div>
            </div>

            <div class=" col-10 col-md-4 col-sm-12  foter-mg">
                <div class="tweets-box">
                    <div class="profile-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/twitter.png" class="img-fluid" alt="" loading="lazy">
                    </div>
                    <div class="shadow-box overflow-auto">
                        <div class="tweets-desc">
                            <div class="tweets-detail">
                                <p class="text">Tweets by <br> <span>@adgpi</span> </p>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/error.png" class="img-fluid" alt="" loading="lazy">
                            </div>
                        </div>
                        <div class="tweets-content">
                            <div class="tweets-logo-image">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/usi-logo.png" class="img-fluid" alt="" loading="lazy">
                            </div>
                            <a class="twitter-timeline" data-lang="en" data-theme="light" href="https://twitter.com/adgpi?ref_src=twsrc%5Etfw%22%3ETweets" by adgpi></a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    </div>
                </div>
            </div>

            <div class=" col-10 col-md-4 col-sm-12  foter-mg">
                <div class="facebook-box">
                    <div class="profile-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/facebook.png" class="img-fluid" alt="" loading="lazy">
                    </div>
                    <div class="facebook-combine-box overflow-auto">
                    <div class="fb-page" data-href="https://www.facebook.com/Indianarmy.adgpi" data-tabs="timeline" data-width="500" data-height="500" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Indianarmy.adgpi" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Indianarmy.adgpi%22%3EADGPI" - Indian Army></a></blockquote></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="army-img">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/footer-img.png" class="footer-img-army" alt="" loading="lazy">
</div>

<div class="army-img-mobile">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/footer-mobile.png" class="footer-img-army" alt="" loading="lazy">
</div>

<!-- The image Modal -->
<div id="myModal" class="modal">
  <span class="close" id="close-modal">&times;</span>
  <img class="modal-content-img" id="img01">
  <!-- <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logos/watermark.png" class="img-fluid water-mark" alt="" loading="lazy"> -->
  <div id="caption"></div>
</div>





<footer class="footer-section">
    <div class="container">
        <div class="row footer-row">
            <div class="col-md-3 order-4 order-md-1">
                <div class="footer-logo">
                    <div class="footer-logo-img">
                        <img src="<?php echo get_field('footer_logo', 'option'); ?>" class="footer-logo-img" alt="" loading="lazy">      
                    </div>
                    <!-- <div class="footer-logo-text">
                        <p class="footer-para">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diamnonummy nibh euismod tincidunt Lorem ipsum dolor sit amet, </p>

                    </div> -->
                </div>
                <div class="list">
                    <?php if(have_rows('footer_social','option')): ?>
                    <ul>
                        <?php while(have_rows('footer_social','option')): the_row(); ?>
                            <a href="<?php echo get_sub_field('social_link'); ?>" target="_blank" ><li> <img src="<?php echo get_sub_field('social_icon'); ?>" alt="" loading="lazy"> </li></a>
                        <?php endwhile; ?>
                        
                    </ul>
                    <?php endif; ?>
                    <img src="assets/img/Date-time.png" class ="img-fluid desktop-none"alt="" loading="lazy">

                </div>
            </div>
            
            <div class="col-md-2 order-md-2 order-1">
                <div class="links">
                <?php
                    if (function_exists('register_primary_menu')) :
                        wp_nav_menu(array(
                            'theme_location' => 'Footer Link',
                            'menu_class' => '',
                            'container_id' => '',
                            'container_class' => '',
                            'add_a_class'  => 'accordion-toggle',
                        ));
                    endif;
                ?>
                <!-- <ul>
                    <li>
                        <a href="./indian-army/organization-structure.php" class="accordion-toggle">Indian Army</a>
                        
                    </li>

                    <li>
                        <a href="./pre-indeep-war.php" class="accordion-toggle">Wars</a>

                    </li>

                    <li>
                        <a href="./operations.php" class="accordion-toggle">Operations</a>
                        
                    </li>

                    <li>
                    <a href="./iconic-leaders.php" class="accordion-toggle">Iconic Leaders </a>

                    </li>

                    <li>
                        <a href="./brave.php">The Brave</a>
                        
                    </li>

                    <li>
                    <a href="./gallery/image.php">Gallery</a>
                       
                    </li>
                    
                    

                    <li><a href="./contact-us.php">Contact Us</a></li>


                </ul> -->
                </div>
            </div>

            <div class="col-md-4 order-md-3 order-2">
                <div class="other-links">
                <h4>Other Links</h4>
                <?php
                    if (function_exists('register_primary_menu')) :
                        wp_nav_menu(array(
                            'theme_location' => 'Links 2',
                            'menu_class' => '',
                            'container_id' => '',
                            'container_class' => '',
                            'add_a_class'  => 'accordion-toggle',
                        ));
                    endif;
                ?>
                <!-- <ul>
                    <li><a href="https://indianarmy.nic.in" target="_blank" class="army-links"> https://indianarmy.nic.in</a></li>
                    <li><a href="https://joinindianarmy.nic.in" target="_blank" class="army-links"> https://joinindianarmy.nic.in</a></li>
                    <li><a href="https://www.gallantryawards.gov.in/awards" target="_blank" class="army-links"> https://www.gallantryawards<br>.gov.in/awards</a></li>
                </ul> -->
                </div>
            </div>

            <div class="col-md-3 order-md-4 order-3">
                <div class="contact-links">
                    <h4>Contact Us</h4>
                    <?php if(have_rows('contact_details','option')): ?>
                        <ul>
                            <?php while(have_rows('contact_details','option')): the_row(); ?>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/call.png" alt="" loading="lazy"><a href="tel:<?php echo get_sub_field('phone_number'); ?>"><?php echo get_sub_field('phone_number'); ?></a></li>

                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/email.png" alt="" loading="lazy"><a href="mailto:<?php echo get_sub_field('email'); ?>"><?php echo get_sub_field('email'); ?></a></li>

                                <li style="display: -webkit-inline-box;"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/location_on.png" alt="" loading="lazy"><p><?php echo get_sub_field('address'); ?></p></li>

                                <!-- <li class="mobile-none"><img src="<?php // echo get_template_directory_uri(); ?>/assets/img/Date-time.png" class ="img-fluid "alt="" loading="lazy"></li> -->
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                </div>

            </div>
            
        </div>
    </div>

     <!-- Copyright -->
  <div class="text-center p-4">
    © Company name 2022.  
    <a class="text-reset fw-bold" href="#">All Right Reserved.</a>
  </div>
  <!-- Copyright -->
</footer>


    
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>

<?php wp_footer(); ?>

</body>

</html>