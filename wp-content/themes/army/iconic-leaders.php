
<?php 
/*
*
* Template Name: Iconic leaders template
*
*/

$main ="iconic";



get_header(); ?>

<section class="iconic-leaders-detailed-banner"
    style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/iconic-leaders-detail-banner.jpg);">
    <div class="container">
            <h1 class="banner-content">Iconic leaders</h1>
        </div>
</section>

<section class="iconic-leader" >
    <div class="container">
        <div class="row">
            <div class="col-md-4 post active" data-cat="blog-ux">
                <div class="iconic-leader-box">
                <a href="<?php // echo $currentDomain; ?>/iconic-leaders/km-carippa.php" class="title">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/cariappa.jpg" class="img-fluid" alt="" loading="lazy" />
                    </div>
                    <div class="content">
                        <div class="come-in">
                            
                                <h4>KM Cariappa</h4>
                                <p>Field Marshal</p>
                            <a href="" class="read-more">Read More</a><span>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" alt="" loading="lazy"> </span>
                        </div>
                    </div>
                    </a>

                </div>
            </div>


            <div class="col-md-4 active" data-cat="blog-ux">
                <div class="iconic-leader-box">
                <a href="<?php // echo $currentDomain; ?>/iconic-leaders/shfj-manekshaw.php" class="title">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/manekshaw.jpg" class="img-fluid" alt="" loading="lazy" />
                    </div>
                    <div class="content">
                        <div class="come-in">
                                <h4>SHFJ Manekshaw</h4>
                                <p>Field Marshal</p>
                            <a href="<?php // echo $currentDomain; ?>/iconic-leaders/shfj-manekshaw.php" class="read-more">Read More</a><span>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" alt="" loading="lazy"> </span>
                        </div>
                    </div>
                    </a>
                    
                </div>
            </div>
            <!-- <div class="col-md-4 active" data-cat="blog-ux">
                <div class="iconic-leader-box">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thimayya.jpg" class="img-fluid" alt="" loading="lazy" />
                    </div>
                    <div class="content">
                        <div class="come-in">
                            <a href="iconic-leaders-details.php" class="title">
                                <h4>KS Thimayya</h4>
                                <p> General</p>
                            </a>
                            <a href="ks-thimayya.php" class="read-more">Read More</a><span>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" alt="" loading="lazy"> </span>
                        </div>
                    </div>
                </div>
            </div>

           

            <div class="col-md-4 active" data-cat="blog-ux">
                <div class="iconic-leader-box">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/sagat-singh.jpg" class="img-fluid" alt="" loading="lazy" />
                    </div>
                    <div class="content">
                        <div class="come-in">
                            <a href="" class="title">
                                <h4>Sagat Singh</h4>
                                <p>Lieutenant General</p>
                            </a>
                            <a href="sagat-singh.php" class="read-more">Read More</a><span>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" alt="" loading="lazy"> </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 active" data-cat="blog-ux">
                <div class="iconic-leader-box">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/hanut-singh.jpg" class="img-fluid" alt="" loading="lazy" />
                    </div>
                    <div class="content">
                        <div class="come-in">
                            <a href="" class="title">
                                <h4>Hanut Singh </h4>
                                <p>Lt General</p>
                            </a>
                            <a href="#" class="read-more">Read More</a><span>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" alt="" loading="lazy"> </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 active" data-cat="blog-ux">
                <div class="iconic-leader-box">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bhagat.jpg" class="img-fluid" alt="" loading="lazy" />
                    </div>
                    <div class="content">
                        <div class="come-in">
                            <a href="" class="title">
                                <h4>PS Bhagat</h4>
                                <p>Lieutenant General</p>
                            </a>
                            <a href="#" class="read-more">Read More</a><span>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" alt="" loading="lazy"> </span>
                        </div>
                    </div>
                </div>
            </div> -->

        </div>
    </div>
</section>
 

<?php get_footer();?>