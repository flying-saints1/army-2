<?php

$main = "wars";
$page = "post-indeep";
$subPage = "kasmir";

include('header.php'); ?>


<section class="post-independence-war-banner" style="background-image: url(./assets/img/post-independence-war-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">1947-48 Kashmir Conflict </h1>
    </div>
</section>


<section class="indep-war" id="faq-section">
    <div class="container">
        <div class="row">
            <?php include('sidebar/post-indeep-sidebar.php'); ?>

            <div class="col-md-9">
                <div class="content">
                    <img src="assets/img/post-indep-war.jpg" width="100%" height="400px" class="img-fluid res-image" alt="" loading="lazy">
                    <h3 class="sino-war-text"> 1947-48 Kashmir Conflict</h3>
                </div>


                <div id="about-war" class="accordion-details">
                    <h3>Historical Background</h3>

                    <div class="war-details">

                        <p>The princely State of Jammu and Kashmir (J&K) had been brought under British paramountcy in 1846 via the Treaty of Amritsar, signed between the East India Company (EIC) and Maharaja Gulab Singh, the founder of the Dogra dynasty. Gulab Singh paid 7.5 million Nanakshahi rupees and bought the Kashmir Valley and the Ladakh Wazarat (comprising Baltistan, Kargil and Leh), and added it to Jammu, which was already under his rule. The Gilgit Wazarat (including Gilgit and Pamiri areas) was later conquered from the Sikhs by the Dogras. Princely J&K had five main regions—Jammu, Kashmir, Ladakh, Gilgit Wazarat, and the Gilgit Agency, which was on a 60 year lease to the British. In the nineteenth and early twentieth centuries, till the Russian Revolution of 1917, the British Government perceived Imperial Russia and, later, Communist Soviet Union as a threat to their most significant colony: India. Thus they thought it fit to administer the Gilgit Agency directly and accordingly took it on lease in 1935. As the Indian Independence Act was passed by the British Parliament on 13 July 1947 and the date of transfer of power to India and Pakistan was set to 15 August 1947, the Viceroy Lord Mountbatten decided to let go of the Gilgit Agency lease. On 01 August 1947, the administration of Gilgit thus passed back into the hands of Maharaja.</p>


                        <!-- <div class="map-section" style="margin: 30px 0px;">

                            <img src="assets/img/indian-pk-war.jpg" style="width: 100%" class="img-fluid" alt="" loading="lazy">
                        
                        </div> -->


                        <p>When it came to accession, the principle of geographic contiguity could not be applied in the case of J&K as it was contiguous to both India and Pakistan. The principle of the Ruler deciding accession based on the aspirations of his people was then adhered to. Sheikh Mohammed Abdullah, the foremost Muslim leader in the state and organiser of the Jammu and Kashmir National Conference (known as the National Conference), was in favour of accession to India. However, the ruler of Kashmir at the time, Maharaja Hari Singh, covertly harboured visions of independence ignoring Mountbatten’s explicit injunction. Just three days before the announcement of independence, Maharaja Hari Singh offered a Standstill Agreement on 12 August 1947 to India and Pakistan saying that he would need some time before reaching a final decision on the status of J&K. Pakistan signed the agreement but negotiations with India were never completed.4 By doing so, Maharaja Hari Singh tried delaying the issue of accession. Due to partition there was communal rioting in the whole of Punjab and a number of refugees moved from either side, a large number of them moving through J&K. However the state remained a safe haven for these refugees. On 3 September, a band of raiders attacked Kotha south-east of Jammu and while the raiders were securing the disposal of the state’s army by the hit and run tactics, Pakistan’s Government was carrying out economic blockade.</p>

                        <p>The state of J&K had its own armed force. The military administration of J&K comprised an Army HQ at Srinagar, and four brigades. The Army HQ was headed by a Chief of Staff, who formerly used to be a retired British officer. Maj Gen HL Scott, CB, DSO, MC, was the last British Chief of Staff, and was succeeded after independence by Brigadier Rajendra Singh of the J&K State Force. The four brigades were the Jammu Brigade under Brig NS Rawat (HQ at Jammu), the Kashmir Brigade with Body Guard Cavalry, the Mirpur Brigade under Brig Chattar Singh (HQ at Jhangar), and the Punch Brigade under Brig Krishna Singh in the Punch-Rawalkot area. Between them, the four brigades had only eight infantry battalions, with some garrison police companies. The State Force had no artillery or armour. The force was dependent on the arsenals from Northern Command Headquarters, Rawalpindi, for arms, ammunition and equipment. There was a wireless link with Rawalpindi, but none with New Delhi. As a part of the British Empire, Kashmir had had no reason to fear a foreign invasion, therefore the state forces performed border security, constabulary and parade functions.</p>

                        <p>The new government of Pakistan intended to take full advantage of this situation to tilt the balance in favour of the state joining Pakistan. The leading politicians of the Muslim League and senior officers of the armed forces prepared a plan which promised a swift resolution, yet kept the risk of open conflict with India at a minimum. The plan very nearly succeeded.</p>


                    </div>

                    <div class="war-details">
                        <h4>Geography and Administration</h4>
                        <p>Jammu and Kashmir covers an area of 222,000 sq km; its north-south extent is 640 km and east-west extent is 480 km. In 1947, the infrastructure of the state was quite primitive. The all-weather roads followed the river courses and led through Pakistani territory. From India, there were only two fair weather roads into Kashmir: the dirt road from Pathankot to Jammu to Srinagar, which ran through the Banihal Pass, however there was no bridge across the Ravi linking Jammu to Pathankot; and the caravan trail from Manali to Ladakh. Srinagar could be accessed from Muzaffarabad. Gilgit and Skardu were both accessed through the Rawalpindi-Abbottabad Road, which crossed into Gilgit agency at the 4,200 m Babusar pass. It went on to Skardu, and further along Indus Valley joined up with Kargil. The northern road along Hunza valley led to the vassal states of Hunza and Nagar. The present Karakoram Highway is located along this alignment going further into China over the Khunjerab Pass.</p>


                        <p>Only a spur of India’s well-developed rail network ran into Kashmir. Jammu had a light railway railhead. The rail line ran from the Wazirabad Junction on the main Lahore-Rawalpindi line through Sialkot to Jammu. The throughput capacity of the roads and the weight bearing capacity of the bridges strictly limited the numbers, equipment and supply of the forces that could be deployed. The Srinagar and Jammu airfields were suitable for landing transport aircraft, but only in daylight.</p>

                        <p>Unlike roads or railways, the state was fairly well-provided with telegraph and wireless facilities. A network of telegraph stations covered the state, penetrating even into the wild Shyok Valley in order to send warning of floods. The most important villages and tourist resorts had telegraph offices. Wireless communications were mainly under military authorities, but Gilgit and Naushera had civil wireless and telegraph stations, which served in some measure to compensate for the lack of modern means of transport. Most of the far-flung garrisons in the state could communicate with Srinagar by wireless telegraphy, which proved a crucial advantage when the tribal hordes poured into the state in October 1947.</p>

                        <p>The administration of J&K, both civil and military, was headed by the Maharaja, who was the repository of all powers, and could lay down or amend the state’s constitution by his own orders. He was the Head of Government and was the C-in-C of the State Army. The only check on his authority was the British Resident acting as the Crown Representative, and paramountcy had, in the twentieth century reached a stage where the British Resident could command or influence every action of the State Government in any field, if the Viceroy so desired. The appointment of the Prime Minister of the J&K required the approval of the Viceroy conveyed through the Resident. The state had two capitals: Srinagar was the summer capital while Jammu was the winter capital. J&K comprised of four provinces: Jammu, Srinagar, Gilgit and Ladakh, each under a governor. As mentioned earlier, Gilgit had been held on lease by the Government of India, and was handed back to the Maharaja only in August 1947.</p>


                    </div>

                    <div class="war-details">
                        <h4>Division of Armed Forces in 1947</h4>
                        <p>The end of British rule in India, and the partition of the country led to the division of the armed forces and the civil administration. Field Marshal Sir Claude Auchinleck oversaw the division of this force. According to the Independence of India Act, two thirds of the personnel and material resources of the armed forces were to go to India and one third to Pakistan. Around 260,000 personnel, mainly Hindus and Sikhs, went to India and 140,000 personnel, mainly Muslims, went to Pakistan. Many British officers stayed on to assist in the transition, including General Sir Robert Lockhart, India’s first Commander-in-Chief (C-in-C), and General Sir Frank Messervy, who became Pakistan’s first C-in-C. As the various Indian principalities had their own armed forces prior to independence, as the princely states joined either India or Pakistan, their forces were also incorporated into the two national armies. Due to the shortage of the available time a fair and equitable division of materiel (and particularly of installations) was impossible. The airbases were mostly in Pakistani territory, but most of the training establishments were in India. The arsenals, supply dumps, repair installations and naval bases were also mostly in Indian Territory.</p>


                        <p>In the division of British India’s army, the new Indian Army acquired 88 infantry battalions, 12 armoured regiments, and 19 artillery regiments. These were organised into 10 divisions (nine infantry and one armour). The Indian Air Force (IAF) was a respectable force, even after some of its equipment and personnel were transferred to Pakistan. It was equipped with modern aircraft (Tempest and Spitfire fighters and C47 Dakota transports). These forces were organised into seven fighter squadrons and one transport squadron. It is these two Services that featured in the war that is discussed in detail here.</p>

                        <p>Pakistan acquired 33 infantry battalions, six armoured and nine artillery regiments at independence. In August 1947 a two-brigade division (the 7th) was formed in Rawalpindi. By October that year there were 10 infantry brigades and one armour brigade guarding Pakistan’s borders. The brigades were organised into four divisions in West Pakistan and one division in East Pakistan. Three aviation squadrons formed the nucleus of the Pakistani Air Force (PAF). The equipment consisted of modern fighter (Tempests and Spitfires) and transport (C47 Dakota) aircraft. The air force was initially organised into two fighter squadrons and one transport squadron. As Pakistan’s territory encompassed what had been British India’s western border region and its initial line of defence against a Russian threat, the three squadrons had a choice of seven airbases. The PAF played only a limited role in the course of events: Pakistan wanted to maintain the pretence of having nothing to do with events in Kashmir.</p>

                        <p>The state of J&K had its own state forces. The military administration of J&K comprised an Army HQ at Srinagar, and four brigades. The Army HQ was headed by a Chief of Staff, who formerly used to be a retired British officer. Major General (Maj Gen) HL Scott, CB, DSO, MC, was the last British Chief of Staff, and was succeeded after independence by Brigadier (Brig) Rajendra Singh of the J&K State Force. The four brigades were the Jammu Brigade under Brig NS Rawat (HQ at Jammu), the Kashmir Brigade with Body Guard Cavalry and 7th J&K Rifles at Srinagar commanded by Brig Faqir Singh, the Mirpur Brigade under Brig Chattar Singh (HQ at Jhangar), and the Punch Brigade under Brig Krishna Singh in the Punch-Rawalkot area. Between them, the four brigades had only eight infantry battalions, with some garrison police companies. The State Force had no artillery or armour. The force was dependent on the arsenals from Northern Command Headquarters, Rawalpindi, for arms, ammunition and equipment. There was a wireless link with Rawalpindi, but none with New Delhi. As a part of the British Empire, Kashmir had no reason to fear a foreign invasion, therefore the state forces performed border security, constabulary and parade functions. However, they had taken part in the two World Wars and were battle hardened.</p>


                    </div>

                    <div class="war-details">
                        <h4>The Plan </h4>
                        <p>Pakistan made its designs to force accession of J&K clear when within 12 days of agreeing to the Standstill Agreement, on 24 August 1947, it threatened Maharaja Hari Singh by writing to him that ‘The time has come for Maharaja of Kashmir that he must take his choice and choose Pakistan. Should Kashmir fail to join Pakistan, the gravest possible trouble will inevitably ensue’. Having sensed the Maharaja’s intention to hold out on joining either of the two countries, Pakistan conceived a military plan to attack J&K to force it to accede to Pakistan; the operation was codenamed ‘Gulmarg’. The British C-in-C of the Pakistan Army issued orders signed personal and top secret regarding the operation, one of which was received by Maj Onkar Singh Kalkat, then Brigade Major of Bannu Brigade.</p>


                        <p>The broad plan was that every Pathan tribe was required to enlist at least one lashkar consisting of 1,000 tribesmen. After enlistment, these lashkars were to be concentrated at Bannu, Wana, Peshawar, Kohat, Thal, and Nowshera by the first week of September 1947. The brigade commanders at these locations were to issue them arms, ammunition and some essential items of clothing. Each tribal lashkar was also to be guided by a major, a captain and 10 Junior Commissioned Officers (JCOs) of the regular Pakistan Army. The entire force was to be commanded by Maj Gen Akbar Khan, who was given the code name ‘Tariq’. He was to be assisted by Brig Sher Khan. All lashkars had to be concentrated at Abbottabad by 18 October 1947. They were to be moved in civil buses which had been commandeered for this task. A separate area 16 km outside Abbottabad was earmarked for the the lashkars. The broad outline of the operational plan was for six lashkars to advance along the main road from Muzaffarabad to Srinagar via Domel, Uri and Baramula, with the specific task of capturing the Srinagar aerodrome, and subsequently advance to the Banihal Pass. Two lashkars were to advance from the Haji Pir Pass direct on to Gulmarg, thereby securing the right flank of the main force advancing from Muzaffarabad. A similar force of two lashkars was to advance from Tithwal through the Nastachhun Pass for capturing Sopore, Handwara and Bandipur. Another force of 10 lashkars was to operate in the Poonch, Bhimbar and Rawalkot area with the intention of capturing Poonch and Rajauri and then advancing to Jammu. D-day for Operation Gulmarg was fixed as 22 October 1947, on which date the various lashkars were to cross into J&K territory. The 7 Infantry Division of the Pakistan Army was to concentrate in area Murree-Abbottabad by last light on 21 October, and was ordered to be ready to move immediately into J&K to back up the tribal lashkars and consolidate their hold on the Kashmir Valley. One infantry brigade was also held in readiness at Sialkot to move on to Jammu.</p>



                    </div>



                </div>

                <div id="event" class="accordion-details">
                    <h3>Sequence of Events </h3>
                    <p>The sequence of events can be divided into the following phases.</p>
                    <div class="war-details">
                        <h4> August – October 1947 </h4>

                        <p>The partition of India was accompanied by mass migration across the new border and considerable violence. In August 1947, open revolt broke out in Poonch and the first Muslim militia was raised. Attacks on police stations and army garrisons became more frequent, and the Azad Kashmir (Free Kashmir) Government was formed on 24 October of that year.</p>


                        <p>In early September, attacks from Pakistani territory commenced. Pathan groups of several hundred men raided Hindu and Sikh villages along the border, then fled back to Pakistan when government forces responded. The Pakistan Army supported the raiders with arms, ammunition, communications equipment and transport, and its patrols entered Kashmiri territory on several occasions. The Pakistani authorities feigned ignorance of the incidents and of the role of the army, and, at the same time, accused the Kashmiri government of atrocities against the Muslim population. By the middle of October, the situation was ripe for a general offensive. The incidents were becoming more serious along the 300 km long border, and they were occurring in many locations at unpredictable times. The state forces could not protect every village and every vulnerable point; they could only react to the attacks, but could not prevent them. Their forces gradually became fragmented and they became incapable of concerted action.</p>


                    </div>
                    <div class="war-details">
                        <h4>Late October 1947</h4>
                        <p>The general offensive commenced on two fronts on October 22, 1947. Six Pathan lashkars (6,000 men) broke into the Kashmir Valley from the direction of Garhi Habibullah and attacked Muzaffarabad. The Muslims serving in the Kashmiri state forces mutinied, killed their non-Muslim fellow soldiers and joined the Pathans or the local militias. The attackers destroyed a State Forces battalion, but met with stiff resistance near Uri: the State Forces withstood their attacks for a while, then destroyed the bridge over the Uri River and withdrew towards Baramulla. However, the lashkars were warlike tribal militias, not disciplined, regular forces. Their weakness—lack of discipline—became apparent in the operations around Uri. For disciplined, well- trained and well-led troops the destroyed bridge over the Uri River would have been a challenge, but not an obstacle to success: crossing the river without vehicles was no problem, and the distance between Uri and Srinagar was around 102 km. However, the lashkars refused to give up their vehicles (which they needed more for carrying their plunder, than for transporting personnel), and thus they delayed the advance for nearly four days and they were also delayed due to the engagement with the State Forces troops. As a result of this delay, they reached Baramulla (50 km from Srinagar) only on 26 October. Once they seized the town, the only force between them and Srinagar was a roadblock manned by 50 men of the State Forces, 5 km east of Baramulla. The situation and the lashkars’ lack of discipline resulted in them indulging in looting and massacre. These delays gave the Maharaja time to make up his mind and appeal to India for assistance. The Indian government was ready to provide it, but only on the condition that Kashmir accedes to India. The Instrument of Accession was signed on 26 October 1947 and accepted by Lord Mountbatten, the Viceroy, on 27 October; Indian forces entered the conflict the next day.</p>

                        <p>On the other front, the militias attacking in the Jammu region were also met with stiff resistance, and they achieved their objectives only after some serious fighting. They seized border towns like Bagh, surrounded Poonch and cut-off the Poonch- Jammu Road; but, by the time they reached Jammu, Indian forces were already on the move. Jammu remained in Indian hands and subsequently served as the base of Indian operations in the south.</p>

                        <p>Soon after the attacks commenced on 24 October, the Azad Kashmir government was formed under the leadership of Mohamed Ibrahim. Henceforth, two governments existed in the state, each claiming full jurisdiction over the entire territory. The operations in the south (in the Jammu region) were useful because they distracted the defender’s attention and forced him to split his forces. But the key to Kashmir was the Kashmir Valley, and the key of the Valley was Srinagar—thus, seizing the city was the most important goal of the invasion.</p>

                        <p>The Indian rescue effort had to overcome challenges from the outset. The closest point on the Indian border was more than 480 kms away from Srinagar. Dealing with the refugees and upholding law and order were issues for the troops in East Punjab. As a result, air travel was the only option. Even worse, Srinagar’s airport was not suitable for receiving fully loaded transport planes. However, that was the only choice; therefore it had to be made. Operation Jak was the name of the rescue effort. 1 SIKH, which was then stationed in Gurgaon and was commanded by Lieutenant Colonel Dewan Ranjit Rai, was the first battalion to enter the area. Four Dakota aircraft were used to transport the troops, and they departed from Safdarjung Airport in Delhi at 0500 hour on 27 October and arrived in Srinagar at 0830 hour that same day. On 27 October, the initial hostilities commenced. After learning that the enemy had reached the outskirts of Baramulla, he promptly dispatched his first Company on arrival at Srinagar to the area to bolster the Dogra soldiers of the J&K State Forces’ defensive posture. On 28 October, Lt Col Rai was the first Indian officer to be killed at Baramulla during the war to free Jammu and Kashmir from the raiders, who numbered four thousand. He was the first person to receive the Maha Vir Chakra, which was given to him posthumously. The advance of the army had protected Srinagar. The threat to Srinagar was eliminated with the recapture of the Uri area on 13 November 1947. The attackers had taken control of a huge area of land close to the Pakistani border. In truth, the raiders’ lust and covetousness had hindered their progress. The attackers had also left a path of rape, robbery, and plunder. The Battle of Badgam, the battle of Shalateng, the capture and recapture of Jhangar, the Battle of Naushara, the Advance to Tithawal, the Relief in Punch, and the Fight of Chhamb to Tithwal were some of the key engagements that took place in Jammu and Kashmir.</p>
                    </div>


                    <div class="war-details">
                        <h4>The Battle of Badgam</h4>
                        <p>Three companies of the 4 KUMAON were stationed in the Badgam region on No- vember 3 to conduct patrols. At around 1430 Hours, shots were fired at Major Som- nath Sharma’s D Company from nearby houses in Badgam. He was the first awardee of the Param Vir Chakra. Unexpectedly, a tribal lashkar of 700 infiltrators headed upon Badgam from Gulmarg. He rallied his company to fight boldly under heavy fire and a seven to one disadvantage. The position held by the company had already been overrun by the time a relief company from the 1 KUMAON arrived in Badgam. However, the 200 casualties that the tribe infiltrators sustained stopped them from moving forward.</p>

                        <div class="war-2-map-2">
                            <img src="assets/img/gallery/Kashmir-Conflict.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">1947–48, Kashmir Conflict</p>
                        </div>

                    </div>
                    <div class="wat-details">
                        <h4>The Fall of Jhangar and Recapture of Jhangar</h4>
                        <p>On 24 December 1947, the enemy conquered Jhangar. It significantly benefited the adversary. Therefore, recapturing Jhangar was essential to Indian strategy. To regain control of Jhangar, Operation Vijay was started. By 05 March, both Kaman Gosha Gala and Ambli Dhar had been liberated from the enemy. Now was the right time to launch the last attack on Jhangar. Two phases were planned for the completion of Operation Vijay. 19 Independent Brigade, made up of 1 RAJPUT, 4 DOGRA, 1 KUMAON, and ancillary forces, was tasked with securing Pts. 3327 and 3283 in the initial phase. The 50 Para Brigade Group commanded by Brig Usman, made up of the 3 (Para) MARATHA LI, the 3 (Para) RAJPUT, the 1 PATIALA, and other units, was tasked with securing Pts. 2701, Jhangar, 3399, and 3374 in Phase two. The operation also included the use of armour. Pir Thil, which the enemy held, was one of the crucial strategic locations. 3 MARATHA LI was dispatched on offensive reconnaissance on March 15. The Pir Thil Nakka had been taken by 17 March, clear- ing the way for the last assault on Jhangar. On 18 March, the offensive got under way. The 50 Para Brigade, the 3 MARATHA LI, and the 1 PATIALA concentrated at this feature after the 3 (Para) RAJPUT took Pt. 3477 on that day.</p>

                        <h4>Naushera Victory </h4>


                        <p>The fight of Naushera, which took place on February 6, 1948, was important and cleared the path for the eventual recapture of Jhangar. Operation Satyanas was given to Lt Col R G Naidu, CO of 2 JAT, to lead in removing the enemy from the vicinity of Beri Pattan. On January 23, the army entered Tung, and the following morning they crossed the Thandapaniwali Tawi. Early on 25 January, as they moved forward to attack Siot and Pt. 2502, they came under heavy enemy fire and were forced to retreat, but not before inflicting significant casualties—an estimated 100 dead and wounded—on the enemy.</p>



                        <!-- <div class="war-2-map-2">
                            <img src="assets/img/Fatu La-JK-1947–48-13.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                            <p style="color: #D90000; font-weight: 500;">A supply convoy winds its way through Fatu La, J&amp;K, 1947–48 <br> Source: USI, CMHCS.</p>
                        </div> -->

                        <h4>Battle of Tithwal</h4>
                        <p>A fierce and protracted combat that took place in the conflict of 1947–1948 was the Battle of Tithwal. Initial border crossings by Pashtun tribal militia resulted in the vital Indian settlement of Tithwal being taken. Their main objective was to seize the Richhmar Gali, which was controlled by the Indian Army, and Nastachun Pass, which was located east of Tithwal. On October 13, 1948, the Pakistani Army launched a huge attack in an attempt to seize the stations held by Indian troops. The attack, however, was utterly unsuccessful and resulted in terrible deaths due to the Indian troops’ unwavering courage and valour. The Indian Army’s unwavering zeal and tenacity contributed to India’s success in keeping the position of Tithwal. 1 SIKH, 1 MADRAS, and 6 RAJPUTANA RIFLES were the involved battalions. In order to attack and seize an enemy-occupied hill feature at Tithwal on July 18, 1948, CHM Piru Singh of the 6 RAJPUTANA RIFLES was given the mission. He received the Param Vir Chakra for performing the operation with the utmost bravery, unwavering gallantry, and supreme sacrifice (Posthumous).</p>

                        <p>Central Kashmir Valley is divided from Gilgit and Baltistan to the North and North-West by the Burzil Mountains, which are a component of the Great Himalayan Nanga Parbat range. The historic caravan route connecting Srinagar and Gilgit crosses these mountains at a height of 4,000 metres through the Burzil Pass. This westernmost region of the Ladakh province was originally ruled by the Dogras up until the early 20th century. Maharaja Hari Singh then leased it to the British, who used it as a defensive high-altitude buffer between British India and the Russian Empire as the “Great Game” played out in the area. The Sectors of Gurais, Skardu, Dras and Kargil, as well as Leh, were part of the Northern Front of the Kashmir Campaign. During the period of November 1947 to August 1948, the enemy made significant gains in the area. They were encouraged by the early success in the South and by the radical narrative that the tribal laskhars were heavily involved in planning a coup in Gilgit on 31 October 1947. The force was commanded by a British commander by the name of Major Brown and was supplemented by substantial portions of the Gilgit Scouts, a Regiment with a local Shia Muslim majority, and a portion of the J&K State Forces. This was only one of many alleged occasions in which British officers serving in the Pakistani Army actively supported the raiders and then the regular Pakistan Army during the conflict’s later phases. The Indian forces’ bravery in holding their posts despite all odds was equally impressive. In particular, the Skardu siege was significant. The actual soul of a regular Indian soldier was first revealed in Skardu. Lt Col Sher Jung Thapa and his soldiers from the 6 J&K Infantry were tasked with defending Skardu after Gilgit was captured by the enemy in November 1947. On December 3rd, the forces arrived at Skardu and took up position inside a fort. Lt Col Thapa defended his picket against a 500-man adversary who was armed with modern rifles, 2” and 3” mortars, and was led by experienced fighters with a total strength of roughly 285 soldiers. On 11 February 1948, the enemy launched an offensive. From that day until 13 August 1948, when Skardu ultimately fell to the enemy after resisting for six months against insurmountable odds, it was a tale of defiance, bravery, and tenacity. At its lowest point, the Ladakh Valley is located at a height of about 10,000 feet. Mountains range in height from 17,000 to 25,000 feet, while settlements are scattered between 12,000 and 15,000 feet above sea level. Skardu to Leh is around a 360 km drive, as is the trip from Srinagar to Leh through the Zoji La Pass. Therefore, from a tactical standpoint, the Indian Army and the raiders were in a race to Leh. Forty brave Lahaulis from the 2 DOGRA, which had arrived to support 161 Brigade, were led by Captain Prithi Chand and Captain Kushal Chand, and they left Srinagar in the middle of February 1948 to attempt to breach the 16,000-foot-high Zoji La Pass.</p>

                        <p>As the raiders proceeded across Dras and Kargil to attack Leh at the beginning of May, the Kashmir Infantry troops of the Kargil garrison and the small Leh garrison managed to organise defences ahead of the town to thwart their repeated assaults. Even with well-planned defences, the situation became unstable by the third week of May, and Leh could only be rescued by receiving reinforcements from the air. On 24 May 1948, Air Commodore Meher Singh and Major General Thimayya touched down in Leh on a historic day for Indian military aviation. This accomplishment served as a practice run for later missions that involved four and six aircraft landing in Leh over the course of the following two weeks to induct a Company of the 2/4 GORKHA RIFLES. One of the four pilots who received the Maha Vir Chakra for their daring transport flying achievements and motivational leadership was Air Commodore Meher Singh. Men from the 2/8 GORKHA RIFLES were then enlisted. Reports of the massacre of Buddhist monks in Kargil by the Gilgit Scouts and Pakistan Frontier Rifles surfaced in the summer of 1948 when the raiders reinforced their hold on the regions near Kargil and Dras. The lama of Ganskar Padam monastery was shot dead and the Rangdom Gompa, the second largest monastery in Ladakh, was desecrated and razed to the ground. General Thimayya devised an audacious operation that involved the covert movement of Stuart light tanks of the 7th Light Cavalry Regiment from Jammu to Srinagar in the middle of October 1948. This was done in response to the frustration of the 77 Para Brigade’s infantry assaults to retake Zoji La Pass in August and September 1948. These tanks were taken apart in Srinagar and moved to Baltal, which is located roughly 80 kms away. They received commendable assistance from two companies of engineers from the Madras Engi- neering Group (MEG) under the leadership of Major Thangaraju, who not only made it possible for the tanks to travel from Jammu to Srinagar across flimsy wooden bridges, but also built tracks from Baltal to Zoji La Pass that were friendly to tanks while enduring enemy fire for the final few yards of the road. The unit led by Lieu- tenant Colonel Rajinder Singh Sparrow acclimated for a few days while the track was being constructed before shocking the enemy in subfreezing conditions. On 1 November 1948, Pakistani raiders were driven out of Zoji La Pass after being taken by surprise over the course of the following two weeks by a combined arms assault by tanks and fighter aircraft. After fierce hand-to-hand combat, the Indian Army took back Kargil and Dras.</p>
                    </div>





                    <div class="war-2-map-2">
                        <img src="assets/img/gallery/Bahadur-Pun.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                        <p style="color: #D90000; font-weight: 500;">Maj Gen KS Thimayya, GOC, 19 Infantry Division with Sub Harka Bahadur Rana, MC and Jem Lal Bahadur Pun, VrC of 1/5 GR, Kargil, 1948<br><strong>Source: 1/5 GR.</strong></p>
                    </div>

                    <div id="event" class="accordion-details">
                        <h3>January 1949: Ceasefire</h3>
                        <div class="war-details">
                            <p>After protracted negotiations, both countries agreed to a cease-fire. The terms of the cease-fire, laid out in a UN Commission resolution on 13 August 1948, were adopted by the commission on 05 January 1949. This required Pakistan to withdraw its forces, both regular and irregular, while allowing India to maintain minimal forces within the state to preserve law and order. The belligerents retained their positions: the Ceasefire Line (CFL) of January 1949—later the Line of Control (LOC). The two countries fought twice more for the possession of Kashmir, in 1965 and 1999.</p>

                        </div>
                    </div>

                    <div id="event" class="accordion-details">
                        <h3>The War’s Balance Sheet</h3>
                        <div class="war-details">
                            <h4>Casualties</h4>
                            <p>The war caused many casualties to Indian Armed Forces personnel, State Forces as well as civilians. As far as the Indian armed forces are concerned: they lost 1,500 dead and 3,500 were wounded. There is no reliable data on the Pakistani losses; due to the nature of the Pakistani forces in this conflict, there could have been no reliable record keeping. The Kashmiri civilian population sustained the heaviest losses.</p>


                            <h4>Land Forces</h4>
                            <p>On both sides, the infantry played the dominant role. This was due to three principal factors: the terrain, the inventory, and the nature of the enemy. The rugged terrain, especially that outside the Kashmir Valley, restricted vehicle movement to a few bad roads. Most off-road movement had to be carried out on foot, with the equipment either man-packed or on mules. Also, the equipment available in the belligerents’ inventories was nowhere near the scale of contemporary European (much less American) forces: brigade-scale operations were routinely supported by a few mountain howitzers and mortars, even for the better equipped Indian forces. In any case, artillery was of limited utility against the Pathans who appeared unexpectedly as ghosts among the rocks, and disappeared just as quickly. The Pathan and Azad militias occasionally deployed a few mortars and, in the last months of the conflict, Pakistani artillery also appeared, but also on a very limited scale.</p>

                            <p>Artillery did play a significant role in the defence of Poonch: in the early phase of the siege the IAF flew in a light mountain battery, which kept the besiegers’ mor- tars away from the emergency airstrip. In larger engagements—such as the Battle of Shalateng, the recapture of Baramulla, and the relief of Poonch—artillery again played a significant role. The number and calibre of the available guns, however, limited the effectiveness of their fire. The Pakistani planners had counted on quick successes and did not expect the instantaneous reaction of the Indian armed forces; consequently, they failed to prepare the militias to fight against armoured forces, and to provide them with anti-tank weapons. As a result, armoured formations could play a decisive role on three occasions—the Battle of Shalateng, the relief of Poonch, and in the Zoji La. This was despite the fact that the forces were very small in every case, and their equipment—consisting of Daimler armoured cars and M5 Stuart light tanks—were already obsolescent in 1947–48.</p>

                            <p>The experience gained in the war had a great influence on the development of Indian tactics in later Indo-Pakistani wars.</p>

                        </div>
                    </div>



                    <div class="war-details">
                        <h4>Use of Air Forces</h4>
                        <p>From the very first day of the conflict to the last, the IAF was in complete control of the skies. As soon as the first Indian ground forces secured the Srinagar airfield, Spitfire and Tempest fighters were flown in and commenced air operations, even though there was hardly any aviation fuel available locally. Control of the skies guaranteed air supply to remote garrisons as long as an airfield was available. In addition to maintaining air supremacy, the fighters were also used for reconnaissance and for ground support to land forces, for example, prior to the Battle of Shalateng, although due to the extreme dispersal of the Pakistani forces these functions were not nearly as useful as they would have been in more conventional theatre.</p>

                        <p>Indian control of the skies hampered Pakistani air reconnaissance and concentration of forces, and on the rare occasions when the belligerents’ aircraft met in the air, the Indians prevailed. The Air Transport Squadron (No 12) played a particularly prominent role in the conflict. Its pilots flew in nearly impossible weather conditions, undertook missions that promised certain death—they landed on airfields under enemy fire and lit only by the torches of local citizens. Since the squadron’s lift capacity (eight C47 Dakota aircraft) was not sufficient to support for any length of time the forces deployed in Kashmir, the air force leased some 30 civilian airliners. The Indians also tried to compensate for their lack of bombers by using large explosive devices rolled out of the doors of C47 transports. These did little or no damage, hardly any to the enemy, at any rate, but they served as a morale booster to the Indian forces. Bomber squadrons were organised late in 1948 (using refurbished American B17 bombers), but these came too late to play a role in the war. The PAF did not see participation in the conflict, partly for political reasons, in order to maintain the deniability of the country’s involvement in Kashmir, and perhaps partly to husband a scarce, but very valuable resource.</p>

                    </div>

                    <div class="war-details">
                        <h4>Militias</h4>
                        <p>The employment of militias proved to be advantageous for Pakistan. They could be deployed in offensive operations when the combat readiness of the regular forces was still very low. Also, the militias were quite expendable. Playing the roles of ‘freedom fighting forces spontaneously organised by the local population’, and ‘volunteers helping their Muslim brothers suffering under Hindu oppression’, the militias also served to conceal Pakistan’s role or provide an element of plausible deniability. To independent observers, the Indians and the soldiers on the ground, it was obvious that Pakistan was orchestrating the events, but ‘obvious’ was not sufficient reason to start an international conflict. Since they had no doctrinal foundations and had little experience in the employment of irregular forces, the Pakistani officers directing the operations had unrealistic expectations about the endurance, discipline and combat readiness of the militias, and had to improvise when the latter failed to perform as expected. Although the lashkars’ lack of discipline deprived Pakistan of success in the first operation (and thereby deprived it of outright victory), Pakistan still managed to seize the strategically significant one-third of J&K’s territory.</p>

                    </div>


                    <!-- <div class="note">
                        <p>Notes</p>
                        <ol>
                            <li>Hau Khan Sum, Ravichandran Moorthy, Guido Benny, 'The Genesis of Kashmir Dispute', Asian Social Science, Vol. 9, No. 11, 2013, p. 7.</li>
                            <li>P Dawson, <i>The Peacekeepers of Kashmir: The UN Military Observer Group in India and Pakistan</i>, London: C. Hurst & Co., 1994.</li>
                            <li>Sumit Ganguly, <i>The Crisis in Kashmir: Portents of War, Hopes of Peace</i>, Cambridge: Cambridge University Press, 1997.</li>
                            <li>Dawson, <i>The Peacekeepers of Kashmir</i>, n. 2.</li>
                            <li>Maj KC Praval, 'Parting of the Ways', in <i>Indian Army After Independence</i>, Delhi: Lancer Publishers, 2013.</li>
                            <li>Peter Kiss, 'The First Indo-Pakistani War, 1947-48', unpublished draft paper, n.d., 1 Academia.edu, p. 6.</li>
                            <li>For a detailed description, see LP Sen, <i>Slender Was the Thread: Kashmir Confrontation 1947-48</i>, Hyderabad: Orient Longman, 1969, pp. 78–100.</li>
                            <li>At 3,300 m altitude, this was the world's highest airstrip at the time and it was doubtful that the C47 Dakotas of the IAF would be able to land and take off at such altitude. To dispel these doubts, the first C47 landed piloted by Colonel Mehar Singh, commander of air operations. Maj Gen Thimayya, the commander of the Srinagar Division sat in the co-pilot's seat (24 May 1948).</li>
                            <li>Subsequently, the J&K Division became the 26th Division, the Srinagar Division became the 19th Division, and the J&K Force became 5 Corps.</li>
                            <li>Muhammad Akbar Khan, <i>Raiders in Kashmir</i>, Lahore: Jang Publishers, 1970, pp. 82-92.</li>
                            <li>The Zoji La is a narrow, 3 km long corridor at an altitude of 3,500 m above sea level, squeezed on either side by mountains of over 5,000 m.</li>
                            <li>Maj Gen Thimayya was a firm believer in leading from the front: in the attack on the pass, he was directing operations from the lead tank's turret.</li>
                            <li>Sufficient fuel stocks were eventually built up. Initially, however, the transport aircraft landing at Srinagar airfield had their tanks drained until they had only sufficient fuel left to return to base.</li>

                        </ol>
                    </div> -->


                    <!-- <div id="gallery" class="row image-gallery">
                        <div class="col-md-12">
                            <h3>Gallery</h3>
                        </div>

                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="assets/img/Troops Landing at Srinagar 4.2.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                                <p>Troops at the Srinagar Airfield after the initial landings. The planning, organisation, and execution of the landings at Srinagar airfield on 27 October remain one of the great achievements of not only the Indian Army which mobalised 1 Sikh, which was deployed in the vicinity of Delhi along with troops of 13 Field Regiment, but also of the aviation sector. The Herculean effort witnessed aircrafts take off non stop from the Willingdon (Safdarjang) and Palam airports with over 30 Dakotas sourced for the task. Thus a spirit of jointsmanship in operations marked the very beginning of the Nation’s tryst with destiny.</p>
                            </div>
                        </div>

                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="assets/img/Gun position in forward area 4.2.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                                <p>An artillery gun position in the Uri Sector. Artillery played a battle winning role throughout the campaign in Jammu and Kashmir. They provided intimate fire support for a number of battles giving Indian soldiers the requisite edge in combat. Their made significant contribution during the battles at Uri, Naoshera on 6/7 February and yet again during one of the heaviest duels in December 1948, which saw Captain Dar Dinshaw Mistri display valour and determination against relentless enemy shelling to bring pin point fire. He died as a result of a 75 mm enemy shell and was decorated with the Maha Vir Chakra.<br> Source : </p>
                            </div>
                        </div>

                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="assets/img/Stuart Tanks enroute to Zojila 4.2.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                                <p>Stuart tank of 7 Light Cavalry without its turret on its way to Zoji La. Indian commanders had the ingenuity and flexibility to employ armour with imagination and daring. One of the most audacious examples of the employment of armour was executed by Lieutenant Colonel Rajinder Singh (Sparrow) when he stormed the Zoji La pass on 1 November 1948 surprising the scampering enemy. In true traditions of the army, Major General (later General) Thimayya moved with the leading tank scripting the victory with aplomb.</p>
                            </div>
                        </div>

                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="assets/img/BGS brief CinC HQ westrn Cmd 1948 4.2 1.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                                <p>Briefing by the BGS HQ Western Command in 1948. The Indian Army faced the difficult challenge of undertaking an entire military campaign just two months after independence with few officers having the experience of higher directions of war. However, it goes to their credit that they not only accomplished the given tasks with professionalism, they also displayed vision, maturity and the ability to orchestrate a campaign with the three services as well as other organs of the state with dexterity.</p>
                            </div>
                        </div>

                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="assets/img/Fighting in Uri Sector 4.2.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                                <p>Fighting in the Uri Sector. The Uri Sector saw fierce fighting in difficult operational and weather conditions. The cold weather conditions, high altitude and a desperate enemy made the task of taking forward the thrust a challenge, which was undertaken relentlessly. The initial success on 13 November 1947 after the capture of Uri ensured the safety of Srinagar Valley.</p>
                            </div>
                        </div>

                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="assets/img/Nehru Reviewing Troops at Srinagar 4.2.jpg" class="img-fluid card-img-top" alt="" loading="lazy">
                                <p>Prime Minister Nehru reviewing troops at Srinagar, May 1948. The guard of honour was presented by 1 Sikh, one of the oldest battalions of the Indian Army. By May 1948, it was officially confirmed by the Pakistani establishment that their regulars were involved in the war, thought uniformed regulars had been seen as early as December 1947. It was units like 1 Sikh which had been battle hardened during some of the most fierce battles in Italy and Burma and their experience stood them in good stead during the entire war as was evident through their achievements.</p>
                            </div>
                        </div>
                    </div> -->


           


                </div>
            </div>
        </div>
</section>


<?php include('footer.php'); ?>