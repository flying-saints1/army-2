<?php include('header.php')?>

<section class="iconic-leaders-detail-banner"
        style="background-image: url(assets/img/blog-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Blog Details</h1>
        </div>
</section>


<section class="blog-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12 order-2 order-md-1">
                <h2>Combat-Effectiveness and Structure</h2>
                <div class="row">
                    <div class="col-md-12 col-10">
                        <div class="b-img">
                            <img src="assets/img/icons/date.png" alt="Date-icon">
                            <p>November 5, 2021</p>
                        </div>
                    </div>
                </div>

                

                <div class="addthis_inline_share_toolbox socail-icons"></div>
                <div class="blog-para">
                    <div class="row">
                        <div class="col-12">
                            <p>War demands that an army must be capable of both defence and attack in all types of terrain and conditions.  Military capacity implies both firepower, manoeuvre and sustaining of operations.  All units of the army must be combat -effective.  Combat effectiveness is defined   as an assessment of   the effectiveness of a unit or formation in fulfilling its mission in battle. It is a product of its organisation, state of weapons, equipment, training, morale and leadership. </p>

                            <img src="assets/img/blog-detail.jpg" alt="blog-img">

                            <h2>Combat Support Arms</h2>
                            <p>Combat support arms comprises of Regiments and Corps such as the Regiment of Artillery, the Corps of Army Air Defence, the Corps of Engineers, the Corps of Signals, and  the Army Aviation Corps. </p>


                            <h2>Services</h2>
                            <p>It is axiomatic that an army is like a society, self-contained in all respects. Thus, the famous saying that an army marches on its stomach. For the logistic and administrative needs the Indian Army has a variety  of services  such as  Army Service Corps, Army Medical Corps, Army Dental Corps, Military Nursing Service, Army Ordnance Corps, Corps of Electronics and Mechanical Engineers, Remount and Veterinary Corps, Army Education Corps, Corps of Military Police, Pioneer Corps, Army Postal Service Corps, Defence Security Corps, Intelligence Corps, Judge Advocate General's Department. </p>


                            <h2>Unit or Battalion Level Structure</h2>
                            <p>The building block of the Indian Army is based on a unit. For example, in an infantry battalion or an armoured regiment or an artillery or an engineer regiment (unit) three to four subunits called companies, squadrons or batteries form one unit. Subunits are commanded by Lieutenant Colonels/Majors and the units are commanded by a Colonel. A Brigade has three to four units under its command.  A Division has three to four brigades with units from other arms and services. A corps has three or four division under its command. An army command (like Western or Eastern) may comprise of two or three corps. All army field commands come under the Army HQ. The overall structure of the Indian Army is at Table 1. This section will primarily focus at unit level. </p><br>

                            <p>The strength of an infantry battalion (unit) is about 700 to 800 all ranks. A rifle company is about 120.  An artillery battery has six guns and over a 100 all ranks. Three batteries form a regiment (unit). An armoured regiment has 45 tanks in three sabre and one Headquarter squadron and a mechanised infantry battalion is equipped with 52 infantry combat vehicles.</p><br>

                            <p>Units of the infantry, armoured corps, artillery, army air defence   and engineers have the regimental system where an officer and other ranks are assigned to a unit for their entire career. The unit moves on transfer in block.  In other units no permanent affiliation is given and personnel are rotated.  </p>
                        </div>
                    </div>
                </div>

                <div class="row pagejump">
                    <div class="col-6">
                        <div class="previous">
                            <img src="assets/img/previous.jpg" alt="">
                            
                            <p>
                            <a href="">Previous Post</a><br>
                            Lorem Ipsum is simply <br>dummy text of the printing
                            </p> 
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="next">
                            <img src="assets/img/next.jpg" alt="">
                            
                            <p>
                            <a href="">Next Post</a><br>
                            Lorem Ipsum is simply<br> dummy text of the printing
                            </p> 
                        </div>
                    </div>
                </div>


                <div class="row Comment-box">
                    <div class="col-12">
                        <div class="comment-row">
                            <h3>10 Comments</h3>

                            <div class="user-comment">
                                <div class="reply">
                                <h5>Rohit Rao <p>November 5,2020 - 2:08</p></h5>
                                
                                <a href="" class="reply-btn">Reply</a>
                                </div>
                                
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.  
                                </p>
                            </div>
                        </div>

                        <div class="comment-row">   
                            <div class="user-comment">
                                <div class="reply">
                                <h5>Rakesh kumar <p>November 5,2020 - 2:08</p></h5>
                                
                                <a href="" class="reply-btn">Reply</a>
                                </div>
                                
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.  
                                </p>
                            </div>
                        </div>

                        <div class="comment-row">
                            <div class="user-comment">
                                <div class="reply">
                                <h5>Amit Chauhan<p>November 5,2020 - 2:08</p></h5>
                                
                                <a href="" class="reply-btn">Reply</a>
                                </div>
                                
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.  
                                </p>
                            </div>
                        </div>

                        <div class="comment-row">
                            <div class="user-comment">
                                <div class="reply">
                                <h5>Kuldeep Rawat <p>November 5,2020 - 2:08</p></h5>
                                
                                <a href="" class="reply-btn">Reply</a>
                                </div>
                                
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.  
                                </p>
                            </div>
                        </div>

                        <div class="comment-row">
                            <div class="col-12">
                                <div class="load-more">
                                    <a href="">Lord More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row comment-form">
                    <div class="col-12">
                        <h3 class="head">Leave A Comment</h3>
                        
                        <form action="">
                            <textarea name="comment" id="area" placeholder="Comment*"  rows="12" ></textarea>
                            <div class="input-field">
                                <input type="text" class="form-control" name="name" placeholder="Name*">
                                <input type="text" class="form-control form-margin" name="phone" placeholder="Phone No*">
                                <input type="email" class="form-control form-margin" name="email" placeholder="Email*">
                            </div>
                            <input type="submit" name="submit"  class="form-control post-btn" value="Post Comment">
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-10 recent-posts order-1 order-md-2">
                <div class="recent-post-sidebar">
                    <h4>Recent Posts</h4>
                    <hr>
                    <!-- <p class="recent" > -->
                        <ul>
                            <li>Class Regiments.</li>
                            <li>Recruitment and Comp.</li>
                            <li>Recruitment as Soldiers (Other than Officers).</li>
                            <li>Composition of Regiments.</li>
                            <li>Conclusion.</li>
                        </ul>
                    <!-- </p> -->
                </div>
            </div>
        </div>
    </div>
</section>


<?php include('footer.php')?>