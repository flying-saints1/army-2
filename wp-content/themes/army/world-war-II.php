<?php

$main = "wars";

$page = "pre";

$subPage = "war-2";

include('header.php'); ?>


<section class="post-independence-war-banner" style="background-image: url(./assets/img/post-independence-war-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">World War II</h1>
    </div>
</section>


<section class="indep-war" id="faq-section">
    <div class="container">
        <div class="row">
            <?php include('sidebar/post-indeep-sidebar.php'); ?>

            <div class="col-md-9">
                <div class="war-content">
                    <div class="content">
                        <img src="assets/img/small-war.png" width="100%" class="img-fluid res-image small-banner" alt="">
                        <h4> World War II</h4>
                    </div>
                </div>


                <div id="causes" class="accordion-details">
                    <h3>Causes of World War II</h3>

                    <div class="war-details">
                        <h4>Causes of World War II</h4>
                        <p>World War I (1914–18) was the most destructive conflict the world had seen until World War II (1939–45). In World War I, though the fighting stopped in November 1918, the war formally ended with the signing of the Treaty of Versailles, on 28 June 1919, between Germany and the Allied powers led by Britain and France. The treaty laid the blame for the war on Germany, which had to pay reparations to certain countries that were part of the Triple Entente, estimated in 1921 to be 132 billion gold marks. Further, Germany had to disarm and could not retain armed forces that were capable of offensive actions. It lost significant territory, covering 65,000 sq km containing 7 million people, as well as control of the Saar coal mines for a period of 15 years. France regained the provinces of Alsace-Lorraine from Germany as the older treaties of 1871 were rescinded.</p>

                        <p>The conditions imposed on Germany by the Treaty of Versailles were particularly harsh and led to considerable resentment amongst the German people. It also bankrupted Germany, especially in the aftermath of the 1929 financial crisis. The growing resentment amongst the Germans, coupled with the ruined economy, paved the way for the rise of the National Socialist German Workers’ Party (known as the Nazi Party) and its leader, Adolf Hitler. In 1933, Hitler was appointed Chancellor of Germany, a mere 10 years after failing to capture power in a putsch in Munich. </p>

                        <p>Almost immediately upon coming to power, Hitler went about setting aside the provisions of the Treaty of Versailles: he took back the Saar mines; rearmed the German military and rapidly built up the war machine; and embarked on a policy of expansionism to take back the territories lost in 1919.</p>

                        <p>By early 1938, the Nazis annexed Austria; the turn of Czechoslovakia was next with the invasion of the Sudetenland with its majority German-speaking people. With no push-back from the British or the French in the Munich Conference, Hitler was further emboldened. In August 1939, Germany signed a treaty with the Soviet Union; by this time, it was clear that war was on the horizon. On 1 September 1939, German troops crossed the border into Poland. On 3 September 1939, Britain declared war on Germany and World War II began.</p>

                        <p>In the Far East, Japan had risen as a power, especially following the Meiji Restoration in the mid-nineteenth century. In 1904–05, Japan went to war with Imperial Russia and defeated the latter’s forces. Though it was on the Allied side in World War I, Japan had its own imperial ambitions in the region. It invaded Manchuria in 1931, and by 1937, Japanese forces were engaged in conflict in China. It allied with the Axis Powers—Germany and Italy—in World War II. It was the Japanese Army that the Indian Army faced in the jungles of Burma in the course of the war, the details of which are discussed later.</p>
                    </div>

                    <div class="war-details">
                        <h4>Indian Army on the Eve of World War II</h4>
                        <p>India’s entry into World War II was announced by the Viceroy, Lord Linlithgow, on 3 September 1939 from his summer residence in Simla, the same day that Britain declared war on Nazi Germany. When the war commenced, the Indian Army numbered 194,373 men, a meagre increase over its 150,000 strength in 1914. It comprised of ‘96 Infantry Battalions and 18 Cavalry regiments’. Modernisation had been recommended but was yet to begin. The cavalry was mounted on trucks and had no tanks; and the Infantry lacked mortars or anti-tank weapons. Wireless sets were available only at Brigade HQ and above. The defined role for the Indian Army continued to focus on policing duties and security of India’s frontiers. However, by 1945, when the war ended, the Indian Army had expanded to strength of over 2 million men, the largest volunteer army in history. During the war, it was engaged in operations stretching from Hong Kong to Italy. It also provided the bulk of the forces in the Burma campaign and played a significant role in the campaigns in North Africa and in Italy.</p>

                        <p>It is to the Indian Army’s credit that despite going to war with the abovementioned deficiencies, it held its own against two of the finest armies of the world— the Germans and the Japanese. Towards the end of the war it was led by its own officers at unit and sub-unit level. Brigadier KS Thimayya successfully commanded a Brigade in operations with Indian officers commanding Battalions in the Brigade and was awarded a DSO for conduct of operations in Burma.</p>
                    </div>
                    <div class="table-wrap">
                        <div class="box-table">
                            <h4>Table 1 Indianisation during the War</h4>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Date</th>
                                        <th scope="col">Indian Officers</th>
                                        <th scope="col">Ration of British<br>to Indian Officers</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1 October 1939</th>
                                        <td>396</td>
                                        <td>10.1: 1</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">1 January 1940</th>
                                        <td>415</td>
                                        <td>9.7: 1</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">1 January 1941</th>
                                        <td>596</td>
                                        <td>12:1</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">1 January 1942</th>
                                        <td>1,667</td>
                                        <td>8.3:1</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">1 January 1943</th>
                                        <td>3,676</td>
                                        <td>6.9:1</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">1 January 1944</th>
                                        <td>6,566</td>
                                        <td>4.5:1</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">1 January 1945</th>
                                        <td>7,546</td>
                                        <td>4.2:1</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">1 September 1945</th>
                                        <td>8,340</td>
                                        <td>4.1:1</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="font-size: 14px; font-weight: 500; color: #45464B;">Source: Gautam Sharma, Nationalisation of the Indian Army, 1885–1947, New Delhi: Allied <br> Publishers, 1995, p. 184.</p>
                        </div>
                    </div>
                    <div class="war-details">
                        <p>One of the key results of the army’s participation in the war was the breaking of the ‘martial classes’ myth. There was forced recruitment from all classes due to rapid wartime expansion of the Indian Army. ‘The officer cadre expanded from 1000 to 15,740. Thus by the end of World War II the Indian Army was truly representative of all areas and classes of the country’44 (see Table 1). In the six years of war, the army earned nearly 6,300 awards, 31 VCs, four George Crosses, 252 Distinguished Service Orders (DSOs), 347 Indian Orders of Merit and 1,311 Military Crosses.</p>

                        <p>After the British concession to Hitler at the Munich Conference in 1938, it was apparent that war clouds were looming on the horizon. By 1939, elements of the Indian Army were deployed overseas: </p>

                        <p>‘[E]ven before the British Government had formally declared war in September 1939, India had despatched nearly 10,000 troops to Egypt, Aden, Singapore, Kenya and Iraq.’ Headquarters 4th Indian Division and 5th Indian Infantry Brigade joined the 11th Infantry Brigade in Egypt in September 1939. The period between September 1939 and April 1940 saw little fighting and is consequently termed the ‘Phoney War’. Its end, and the subsequent declaration of war on Britain by Italy on 8 June 1940, led to the beginning of intense fighting in North Africa and other war theatres. However, before moving on to discuss these actions and the linked ones in East Africa and the Middle East, a mention needs to be made of a small Indian Army detachment that took part in battles in France. There were four Animal Transport Companies that supported the British divisions and took part in the retreat to Dunkirk. This was ‘Force K6’:</p>

                        <p>[a] cluster of Animal Transport Companies that were formed in the later months of 1939 in anticipation of transport and logistical requirements on the snowbound supply routes to military posts along the Franco-German borders and in Belgium. These Animal Transport Companies were composed largely of ‘muleteers’ or mule drivers and service providers such as bellows-boys, blacksmiths, hammermen, farriers, cooks, dhobis, and so on.</p>

                        <br><br>
                        <p></p>
                        <div class="war-2-content">
                            <ol>
                                <li>‘In the first eight months of the war, expansion was slow. Only some 50,000 troops were added to the pre-war number [including] the Indian territorial Battalions raised for internal security duties.’</li><br>

                                <li>‘In the first eight months of the war, expansion was slow. Only some 50,000 troops were added to the pre-war number [including] the Indian territorial Battalions raised for internal security duties.’</li><br>

                                <li>‘In the first eight months of the war, expansion was slow. Only some 50,000 troops were added to the pre-war number [including] the Indian territorial Battalions raised for internal security duties.’</li><br>
                            </ol>
                        </div><br>
                        <p>In the second phase, in particular, it was proposed to raise:</p>
                        <br>
                        <div class="war-details">
                            <p>18 Infantry Battalions and three field artillery regiments as well as supporting services such as engineers and signals, organizational and logistical units for six army divisions…Simultaneously…raise an equal number of units to replace the ones that might be sent out of the country…In August 1940, New Delhi offered to prepare four Infantry divisions and one armoured division for over- seas deployment—if the British government agreed to equip these forces… [eventually], it was agreed that India would raise four new Infantry divisions by December 1941 and a fifth by mid-1942.</p>

                            <p>Recruitment grew considerably in the second phase, with monthly recruitment exceeding 50,000 men a month. Recruitment of technical personnel also expanded. In addition, troops from India’s princely states were pressed into service in the war. ‘In early 1942, 264,000 Indian troops were serving overseas, including 91,000 in Iraq, 20,000 in the Middle East, 56,000 in Malaya and 20,000 in Burma…at the end of 1942, the Indian army stood at almost 1.5 million men…’, with ‘Indian state forces and British units in India [adding] up to another 1,70,000 men.’</p><br>
                        </div>




                        <div id="theatre" class="accordion-details">
                            <h3>Indian Army World War II Theatres</h3>

                            <div class="war-details">
                                <h4>North and East Africa and the Middle East</h4>
                                <p>At the beginning of World War II, the British Middle East Command stretched from the Persian Gulf to Egypt and thence along the coast of North Africa. In June 1939, Gen Archibald Wavell was appointed General Officer Commanding-in-Chief (GOC-in-C), Middle East Command. Following Italy’s declaration of war against the British in 1940, the Italian forces in Libya—comprising of around eight divisions—became a major threat to the command. The Italians were located some 250 miles from the Egyptian port of Alexandria. Apart from Libya, Italy’s colonies in Africa included Somaliland and Abyssinia (Ethiopia), which Benito Mussolini had annexed in 1935.</p><br>

                                <p>Gen Wavell had the following troops available: the British 7th Armoured Division, later gaining fame as the ‘Desert Rats’; and the 4th Indian Division, less a Brigade. The 4th Indian Division was the first formation of the Indian Army to serve on the frontline and had been in the desert since September 1939. The Middle East Command also included Brigades of the Australian and New Zealand divisions, the first of which reached Egypt by February 1940 and were under training,</p><br>


                                <div class="war-2-map-2">
                                    <img src="assets/img/gallery/North-Africa.jpg" width="100%" height="400px" style="margin-bottom: 20px;" class="img-fluid" alt="">
                                    <p style="color: #D90000;">An Indian infantry battalion prepares to embark for service in North Africa, 1940 <br><strong>Source: Rana Chhina</strong></p>
                                </div>

                                <p>After declaring war on Britain on 8 June 1940, Italy crossed the Egyptian frontier only in early September. It had reached Sidi Barani by the end of the month and dug into a series of fortified defensive camps. Gen Wavell decided to attack the Italians with the 7th Armoured Division and 4th Indian Division, along with the 16th British Infantry Brigade and 7th Royal Tank Regiment also under his command. The attack commenced on 9 December 1940 with 11th Indian Infantry Brigade attacking the first set of camps, which were captured quickly by the 2nd Cameron Highlanders and 1/6th Rajputana Rifles. The 5th Indian Infantry Brigade followed up and captured the camp at Tumar West by the evening of the same day. On 10 December, 16th British Infantry Brigade captured Sidi Barani. On 11 December 1940, 4th Indian Division was pulled out to reinforce the troops in Sudan. Italian East Africa (Somaliland) was important as it abutted onto the Red Sea, a major supply route, and Italian ships and submarines based at Massawa could interfere with shipping. Gen Platt, who was the C-in-C in Sudan, initially had limited forces; these were reinforced by the 5th Infantry Division in September 1940. At the end of December 1940, 11th Infantry Brigade of 4th Indian Division also arrived in Sudan, with the rest of the division coming together by the end of January 1941. Faced by these additional troops, the Italians withdrew from forward positions by mid-January, with British forces moving in pursuit and crossing the border on 19 January 1941. Second Lieutenant (2/Lt) PS Bhagat was the first Indian officer to be awarded the VC in the war for continuous mine clearing operations under enemy fire from 1 February to 4 February 1941.
                                <p><br>

                                <div class="war-2-map-2">
                                    <img src="assets/img/gallery/Afrika-Korps.jpg" height="400px" style="margin-bottom: 20px;" class="img-fluid" alt="">
                                    <p style="color: #D90000;">On the lookout for the Deutsches Afrika Korps. An Indian signaler in North Africa, 1940 <br><Strong>Source: MoD, History Division</Strong></p>
                                </div>

                                <p>The advance halted at Keren, where the Italians made a determined stand, with severe fighting between 2 February to 27 March 1941 when Keren finally fell. Subedar Richpal Ram of 4/6th Rajputana Rifles won his VC in this operation. On 8 April 1941, Massawa fell to the British and the Indians, and on 19 May, the Duke of Aosta and the remainder of the Italian forces also surrendered. After the fall of Keren, the 4th Indian Division was recalled to Egypt.
                                <p>

                                <p>Meanwhile, the situation had changed in North Africa. The destruction of the Italian Army had compelled the Germans to reinforce this front. Gen Erwin Rommel was dispatched North Africa along with German armoured formations. Facing Rommel and the Afrika Korps in Libya were British forces comprising the 2nd Armoured Division, less a Brigade, 9th Australian Division and 3rd Indian Motor Brigade that arrived in the area in March 1941. The 3rd Motor Brigade consisted of three cavalry regiments—the 2nd Royal Lancers, 11th Prince Albert Victor’s Own Cavalry and 18th Cavalry—mounted in un-armoured trucks, armed only with Bren guns and no artillery support. The Brigade was ordered to hold a position at Mechili, 100 miles west of Tobruk. On 31 March 1941, Rommel launched an attack and surrounded Tobruk by 11 April 1941. The 3rd Indian Motor Brigade, by holding on to Mechili till 8 April, had delayed the German advance. Once the 4th Indian Division came back from East Africa, Gen Wavell launched a counter-attack, termed Operation Battle Axe, on 15 June 1941; however, faced with superior German tanks, the attack failed. Wavell was replaced by Gen Claude Auchinleck in North Africa.
                                <p><br>

                                <p>Gen Auchinlek, on taking over the Middle Eastern Command, was exhorted by British Prime Minister Winston Churchill to go on the offensive. Operation Crusader was eventually launched on 18 November 1941; it achieved some initial success but British armour was eventually outgunned by German Panzers. The Germans then counter-attacked and the 4th Indian Division, forming part of XIII Corps, generally held on to its positions. Auchinleck continued with the attack and the Germans started falling back, with the 4th Indian Division taking part in the pursuit to El Aghelia.
                                <p><br>

                                <p>Rommel struck back while the British were reorganising prior to resuming the offensive. On 21 January 1942, three German columns broke through the British defences and captured Benghazi on 29 January 1942. The 4th Indian Division was holding the area of Benghazi, with 11th Brigade detached at Tobruk. The 7th Brigade, though cut off by the Germans in the area of Benghazi, broke out and managed to get back to reserve positions. The British positions were at Gazala, with the 29th Indian Infantry Brigade of 5th Indian Division and 3rd Indian Motor Brigade deployed towards the desert flank of this position. Rommel launched his attack on 26 May 1942, with the 3rd Indian Motor Brigade being attacked on the morning of 27 May by two armoured divisions; it put up a strong resistance with the field artillery playing a major role. Major (Maj) PP Kumaramangalam (later Chief of Staff of the Indian Army) extricated five of the six troops after the ammunition was over, and was awarded a DSO for his brave actions. The Brigade managed to destroy around 52 enemy tanks before it was overrun. This was followed by a month of fighting in which the 4th, 5th and 10th Indian Divisions took part. In this, 10th Brigade and half of 9th Brigade of the 5th Division were destroyed and 11th Brigade of the 4th Indian Division was trapped in Tobruk and forced to surrender. The British Army retreated to El Alamein on the outskirts of Alexandria, where it decided to hold the German advance into Egypt.
                                <p><br>
                            </div>
                            <div class="map">
                                <div class="box">
                                    <img src="assets/img/war-2-map.png" width="100%" height="400px" class="img-fluid" alt="">
                                    <span  style="color: #D90000;"><strong>North Africa, 1942 </strong></span>
                                </div>
                                <p>Gen Auchinleck was relieved by Gen Alexander as GOC-in-C, Middle East Comm- and, while Gen Bernard Montgomery took over as GOC-in-C of the 8th Army. Montgomery launched his attack on the Germans at El-Alamein on 23 October 1942. Though the 4th Indian Division was present here, it did not have a large role in the initial operations. When the 8th Army found itself held up by the Germans on the Mareth Line (based on a range of hills), the expertise of Indian troops in mountain warfare came in handy. The 4th Indian Division was called up to break through the Mareth Line, which it did by going through the middle of the moun- tains, thus surprising the defenders. It then went on to capture the Fatnassa Feature, the key to the Wadi Akarit position. For his leadership and courage during this attack, Subedar Lal Bahadur Thapa of 1st Battalion, 2nd Gurkhas, was awarded the VC. By this time, the US Army had reached North Africa and were located in Tunisia. The 8th Army was closing into a junction with the Americans. The 4th Indian Division and 7th Armoured Division were transferred to the 1st British Army and took part in the final attack on Enfidaville in the Tunisia campaign. On 12 May 1943, Gen Hans-Jurgen von Arnim, C-in-C of the Axis forces in Tunisia, surrendered to Lieutenant Colonel LCJ Showers, Commanding Officer of the 1st Battalion, 2nd Gurkhas.</p><br>

                                <p>Events in the region of the Persian Gulf were also unfolding along with those in North Africa. In March 1941, a pro-German group seized power in Iraq. The 8th Indian Division was sent to Basra to secure the city. Once it was secured, the division advanced to Baghdad, facing little resistance. It reached Baghdad on 30 April 1941, and the previous government was reinstated. The 8th Indian Division was joined by the 10th Indian Division in Iraq to undertake operations in Syria. During the first week of June 1941, operations were launched to secure Syria, in which 5th Infantry Brigade of the 4th Indian Division participated.</p><br>


                                <p>In June 1941, the German Army invaded the Soviet Union as part of Operation Barbarossa. This made it imperative to secure Iran as a gateway to the Soviet Union. The ruler of Iran, Shah Reza I, was perceived to be pro-German; therefore the Soviets and the British jointly invaded Iran on 25 August 1941. The 8th and 10th Indian Divisions from Iraq formed the British compliment of the invading force. Subsequent to these events, though no actual operations took place in Syria, Iraq or Iran, six Infantry divisions and one armoured division were deployed in the area for most of the war. The 203 and 204 Companies of the Royal Indian Army Service Corps manned 10-tonne trucks, conveying supplies from Baluchistan to Tabruz in Russia through blizzards, snow and ice. The Soviets awarded the Order of the Red Star to Subedar Narayan Rao Nikkam and Havaldar Gajender Singh in appreciation of their efforts.</p><br>
                            </div>
                            <div class="war-details">
                                <h4>Southeast Asia</h4>

                                <p>Japan comprises of four main islands with limited natural resources. It had been modernising since the beginning of the twentieth century and had developed both a strong, modern military and economy. Its industries were dependent on outside sources for oil, raw materials and other requirements. Its imperial ambitions needed colonies to supply its industries. For this, Japan invaded Manchuria in September 1931; Inner Mongolia in 1936; and in 1937, it went to war with China (the Second Sino-Japanese War), which predates the war in Europe. Japan’s entry into World War II took place on 22 September 1940 with the invasion of French Indochina; five days later, Japan signed the Tripartite Pact with Germany and Italy on 27 September 1940. On 7 December 1941, the Japanese naval fleet launched an attack on the US naval base at Pearl Harbour in the Hawaiian Islands, which brought the US formally into World War II. On 8 December, the Japanese moved in deeper into Southeast Asia, landing in Malaya. Post its attack on the US, Tokyo declared war on the US as well as on Great Britain.</p><br>

                                <p>Hong Kong was the nearest British outpost to Japan at the time, and was garrisoned by six Battalions. Two of the six Battalions were the 5/7th Rajputs and 2/14th Punjabis. The Japanese attacked Hong Kong on 8 December 1941. With no air sup- port or hope of reinforcement, the small garrison fought gallantly against at least two divisions of the Japanese before surrendering on 25 December 1941, having taken around 3,000 casualties while inflicting many more on the Japanese.</p><br>

                                <p>The Japanese simultaneously invaded the Malay Peninsula. Pre-war plans catered for the defence of the peninsula and Singapore against a sea-borne invasion employing a combination of naval and air power, with limited attention to land advance along the length of the peninsula. The first Indian troops deployed in Malaya were the 12th Indian Infantry Brigade, which reached Malaya in August 1939. By December 1941, two Indian divisions were present in Malaya—the 9th and the 11th Indian Divisions, grouped under the Indian III Corps. The 9th Indian Division was on the eastern coast, with a Brigade each holding Kota Bahru and Kuantan, 150 miles to the south. The 11th Division was on the west coast of the Malay Peninsula, in the area of Jitra, to cover the Alor Star group of airfields. Both divisions had been training for deployment in the western desert and had little to no experience of jungle warfare.</p><br>

                                <p>The Japanese opened their attack on night 7–8 December 1941, with landings at Kota Bahru and an advance on to Jitra by troops landed in Thailand. At Kota Bahru, the 3/17th Dogras put up strong resistance but were finally forced out; counter-attacks to regain lost positions were a failure. On 9 December 1941, British warships—the Prince of Wales and Repulse—sailed up to Kota Bahru to try and disrupt the landings but were sunk by Japanese air attacks. As their position was now untenable, the Brigade withdrew. On the western side of the peninsula, positions of the 28th Brigade were broken when the Japanese attacked with tanks, against which the troops had no defence. By 31 January 1942, all Allied troops had withdrawn into Singapore, which fell to the Japanese on 14 February 1942. The fall of Singapore led to 85,000 British, Indian and Australian troops being taken prisoner by the Japanese. The fall was one of the largest defeats suffered by the British and, to an extent, shook the confidence of the Indian soldier in his British officers. Many of the prisoners of war (POWs) later joined the Indian National Army.</p><br>

                                <p>Simultaneously, the Japanese attacked Burma by advancing through the jungles on the Thai–Burma border and capturing the airfield at Victoria Point by 10 December 1941. The British forces in Burma, at the time, consisted of the 1st Burma Division with two Brigades, 13th Indian and 1st Burma, and the 17th Indian Division, with 6th and 46th Indian Brigades and 2nd Burma Brigade. The 17th Indian Division was responsible for the area of the Tenasserim Peninsula, its primary task being to delay the enemy advance to Rangoon so as to allow reinforcements to be built up through the port. The natural defence line was the Sittang River, which the 17th Indian Division intended to hold. On 22 February 1942, the Japanese launched an attack on the Sittang defences. The bridge on the Sittang was demolished on 23 February to ensure that the Japanese would not capture it intact; however, the demolition left nearly two Brigades stranded on the wrong side of the river.</p><br>

                                <p>Elements of these Brigades swam across during the next night but had to abandon their vehicles and equipment. All the division could muster the next day were 148 officers and 3350 men and only 1420 rifles. The enemy, nevertheless, was delayed for fourteen days giving time for the 7 Armoured Brigade to be landed at Rangoon.</p><br>


                                <div class="war-2-map-2">
                                    <img src="assets/img/gallery/Rangoon-temple.jpg" width="100%" height="400px" style="margin-bottom: 20px;" class="img-fluid" alt="">
                                    <p style="color: #D90000;">Indian soldiers advance past a temple complex in Burma during the advance towards Rangoon <br><strong>Source: MoD, History Division. </strong></p>
                                </div>



                                <p>On 5 March 1942, Gen Sir Harold Alexander took over as C-in-C, Burma, and decided to group the two divisions as a corps (‘Burcorps’) to be commanded by Lt Gen William Slim. Rangoon was evacuated on 7 March and the retreat continued by way of Prome and then on to Yenangyaung, where the oil installations were destroyed before they could fall into Japanese hands. On 28 April 1942, orders were issued to withdraw to India and by May, the remnants of the army in Burma had reached India.</p><br>


                                <p>The Japanese campaign in Malaya and Burma bested the British and Indian Army, and showed up their deficiencies in terms of training and logistics, equipment and air support. A massive effort now began in India to raise new formations, as also conduct training in jungle warfare. The Chota Nagpur Plateau was the training ground and it was here that Gen Slim trained his XV Corps.</p><br>

                                <p>In the meantime, Army HQ ordered the 14th Indian Division to advance down the Mayu Peninsula in the Arakan; the 6th British Division was ordered to capture Akyab from the sea. The advance progressed well initially, but was held up by 6 January 1943, with unsuccessful efforts to break through the Donbaik and Rathedaung positions. In April 1943, the Japanese crossed the Mayu range using jungle tracks, and set up roadblocks behind the 14th Division. The division was forced to withdraw and by 14 May 1943, it was back to where it had started from.</p><br>

                                <p>While a failure, the first Arakan campaign was invaluable in giving an insight into Japanese tactics, especially their outflanking moves through jungles to cut the lines of communication of opposing troops. From these came the directions that from now on troops cut off would not retreat but would stay and fight and they would be supplied by air.</p><br>

                                <p>The army raised and trained new formations throughout 1943. As its strength increased, new corps were raised and they were grouped into the 15th Army under Gen William Slim. The IV Corps held Imphal and the border beyond it, while XV Corps held the Arakan front. In February 1943, Maj Gen Orde Wingate’s first Chindit expedition was launched, boosting troop morale. In August 1943, the South East Asia Command was established with Admiral Lord Louis Mountbatten as Supreme Commander. This command was henceforth responsible for all operations in the area, and Army HQ in India now became responsible for administration of the army in Burma.</p><br>

                                <p>In November 1943, XV Corps was launched in another effort to clear the Arakan Peninsula. The 5th Indian Division advanced along the right of the Mayu Peninsula and 7th Indian Division, with 81st West African Division protecting the flank. Maungdaw was captured by 10 January 1944, and a road across the Mayu range, through the Ngakyedauk Pass, was constructed. However, efforts to capture Razabil and Buthidaung did not succeed. Between 3 and 4 February 1944, the Japanese infiltrated three columns through XV Corps positions to cut off various elements in the expectation that the surrounded forces would break out and withdraw as they had always done before. But these forces took up all round defence and stood fast. Among the most remarkable of these boxes was the 7 Infantry Division’s Administrative Box, manned initially by headquarter staff and a miscellany of administrative units who defended themselves stoutly till reinforced by elements of 9 Infantry Brigade of the 5 Division.</p><br>

                                <p>With air supplies beginning on 10 February, the Japanese found themselves stranded. The 26th Indian and 36th British Divisions moved up in support and by 24 February 1944, the Japanese were retreating back through the jungle.</p><br>


                                <div class="war-2-map-2">
                                    <img src="assets/img/gallery/Command-aircraft.jpg" width="100%" height="400px" style="margin-bottom: 20px;" class="img-fluid" alt="">
                                    <p style="color: #D90000;">Troops of 5th Indian Division prepare to emplane for Imphal on the Burma front in a US Air Transport Command aircraft, March 1944 <br><strong>Source: Rana Chhina</strong></p>
                                </div>



                                <p>‘Even while the Arakan campaign was going on, Lieutenant General Renya Mutaguchi, Commander of the 15th Japanese Imperial Army in Burma, had been planning an offensive to secure Imphal (in Manipur) and Kohima (in Nagaland) and then to break out into the Assam plains.’ Mutaguchi had three divisions—the 15th, 31st and the 33rd Divisions—and planned to cut Imphal off using the 15th Division, while the 31st Division moved on through the jungle to seize Kohima. The 33rd Division was to advance along the Tiddim–Imphal axis. Imphal was the HQ and base of IV Corps, consisting of the 17th, 20th and 23rd Indian Infantry Divisions. Of these, 17th and the 20th Divisions were operating ahead of Imphal towards Tiddim and Tamu.</p><br>



                                <p>The Battles of Imphal and Kohima were a major turning point in the Southeast Asian theatre of World War II. It was at these battles that the Japanese advance into India was halted. Subsequently, in 1945, the Japanese were finally driven back.</p><br>

                                <p>British military historian Robert Lyman describes the importance of the twin battles: ‘It is clear that Kohima/Imphal was one of the four great turning-point battles in the Second World War, when the tide of war changed irreversibly and dramatically against those who initially held the upper hand.’ The battles at Stalingrad, El Alamein and in the Pacific between the US and Japanese navies were the other three. In April 2013, the Battles of Imphal and Kohima were together named ‘Britain’s Greatest Battle’ by the National Army Museum in the United Kingdom (UK).</p><br>

                                <p>The Japanese build-up opposite Imphal was clear by early 1944. Gen William Slim, commander of the 14th Army, decided that if Imphal was attacked it would be held and the enemy attackers destroyed as a preliminary to his own offensive into Burma. He was, however, not sure of when the enemy attack would take place and delayed orders for the outlying forces of IV Corps to withdraw to Imphal.</p><br>

                                <div class="war-2-map-2">
                                    <img src="assets/img/gallery/Imphal-Kohima.jpg" width="100%" height="400px" style="margin-bottom: 20px;" class="img-fluid" alt="">
                                    <p style="color: #D90000;">A historic moment, Indians from Imphal and British from Kohima meet as 4 and 33 Corps link-up at milestone 109 on the Imphal-Kohima road<br><strong>Source: Rana Chhina</strong></p>
                                </div>


                                <p>The Japanese attack on 6 March 1944 cut off the 17th Division from Imphal, which fought its way back assisted by two Brigades of 23rd Indian Division that came forward from Imphal. Although the Japanese had surrounded Imphal, they were faced by a full British corps that could still be supplied by air from airfields at Imphal and Palel.</p><br>

                                <p>In Kohima, a garrison of only the 1st Battalion, Assam Regiment, and a few other units was surrounded by an entire Japanese division. On 29 March 1944, 161st Infantry Brigade of the 5th Indian Division flew into Dimapur and was immediately sent forward to relieve the garrison in Kohima. By 15 April 1944, a Brigade of 2nd (British) Division moved up and the 161st Indian Infantry Brigade relieved Kohima on 18 April 1945. The 2/5th Royal Gurkha Rifles (FF) made history in Kohima, winning three VCs, two of them on a single day. Indeed, Kohima can boast of the distinction of being one of the bloodiest battles of World War II, with Japanese and British troops virtually eyeballing each other.</p><br>

                                <p>Fierce fighting also continued around Imphal and the 5th Indian Division was flown in from Arakan to reinforce the garrison here. ‘It was only on 8 July 1944 that General Mutaguchi accepted that his offensive had failed and ordered the remnants of his force to fall back across the Chindwin.’</p><br>
                                <div class="war-2-map-2">
                                    <img src="assets/img/war-2-map-2.png" width="70%" height="400px" class="img-fluid" alt="">
                                    <p style="color: #D90000;">1945 Burma Campaign</p>
                                </div>
                            </div>
                            <div class="war-details">
                                <p>With the Japanese retreat, Gen Slim was ready to launch his operations to regain Burma. The XXXIII Corps and IV Corps crossed the Chindwin on 3 and 4 December 1944. The Japanese steadily retreated, with the aim of fighting behind the Irrawady River. By 14 January 1945, the 19th Indian Division had secured two bridgeheads across the river, which drew the bulk of the Japanese forces to this area, thus diverting their attention from the main crossings by IV Corps fur- ther south on 14 February 1945. Meiktila was secured on 5 March and Mandalay on 20 March 1945. The next stop was Rangoon. With lines of communication and supply stretched, only IV Corps was sent forward. The 17th Indian Division reached Pegu on 1 May 1945, but could not advance further due to the mon- soon. Meanwhile, the Japanese had vacated Rangoon and the city was secured by the 26th Indian Division landing from the sea. The 50th Indian Parachute Brigade secured Elephant Point via an air-borne assault. Simultaneously, XV Corps cleared the Arakan. While mopping-up operations continued, the forces were reorganised. The 12th Army was raised for Burma and the 14th Army was pulled back to train and prepare for an amphibious assault to recapture Malaya and Singapore.</p>

                                <p>The war in Europe ended in May 1945, but that in the Southeast Asian theatre continued. It ended only after the atomic bombs were dropped over Hiroshima and Nagasaki in Japan by the Americans on 6 and 9 August 1945. On 15 August 1945, Japanese Emperor Hirohito announced the surrender of Japan and the surrender document was signed formally on 2 September 1945.</p><br><br>
                               

                            </div>
                            <div class="war-details">
                                <h4>Italy</h4>

                                <p>Once North Africa was secured by May 1943, the Allies decided to press on to Europe via Italy. The first step in this invasion was to secure the island of Sicily. The 15th Army Group of Gen Sir Harold Alexander, which comprised the British 8th Army under Gen Bernard Montgomery and the American 5th Army under Gen Mark Clark, took part in the invasion. British and American troops landed in Sicily on 10 July 1943 and by mid-August, the island was secured. On 3 September 1943, the 8th Army landed at the toe of Italy (Operation Baytown) and on 9 September, the 5th Army landed at Salerno (Operation Avalanche). After heavy fighting, both beachheads were secured.</p><br>

                                <p>The 3/10th Baluchis and 3/12th Frontier Force landed as beachhead troops during the Sicilian landings, while the Jodhpur Sardar Light Infantry played a similar role at Salerno, where Major Ram Singh was awarded a DSO.</p><br>

                                <div class="war-2-map-2">
                                    <img src="assets/img/war-2map-3.png" width="70%" height="400px" class="img-fluid" alt="">
                                    <p style="color: #D90000;">Italian Campaign in World War II, 1943–45 </p>
                                </div>
                                <p>The Allied advance into Italy met with stiff resistance at the Gustav Line, one of a series fortified lines intended to defend a western section of Italy, focused around the town of Monte Cassino. An important highway to Rome ran through the town, which made it a strategic target. With the Allied forces held up at the Gustav Line, the 8th Indian Division joined the 8th Army on 19 September 1943. The first operation was the successful crossing of the Trigno River on 2 November 1943, before moving on to the Sangro River. The 8th Indian division managed to get a foothold across the river on 25 November 1943, but opposition was only cleared by 31 November. The advance then continued to the Moro River. Meanwhile, the 4th Indian Division arrived in Italy.</p>
                                <br>


                                <div class="war-2-map-2">
                                    <img src="assets/img/Western-Command.jpg" height="400px" style="margin-bottom: 20px;" class="img-fluid" alt="">
                                    <p style="color: #D90000;">Indian troops in Italy <br><strong>Source: Western Command, Indian Army</strong></p>
                                </div>


                                <p>Monte Cassino and the monastery on top of the hill dominated the advance beyond. In January 1944, American efforts to capture this position failed. Early in February 1944, the 4th Indian and 2nd New Zealand Divisions were tasked to capture the position. The attack commenced on 16 February but they were unable to make progress, with the 4/6th Rajputana Rifles losing 196 men and the 2nd Gurkha Rifles also suffering heavy casualties. A second attack in March partially succeeded in securing some of the approaches to Monte Cassino, but the advance was stalemated by 25 March 1944. The 4th Indian Division had lost 4,000 men by this time. It was pulled back and the Polish corps along with the 8th Indian Division and the 2nd Canadian Cavalry Brigade were brought forward. The 8th Indian Division was given the task to secure a bridgehead over the Gari River. The attack commenced on 12 May and after fierce fighting, the bridgehead was secured by 14 May 1944. It was in these operations that Sepoy Kamal Ram of 3/8th Punjabis won a VC for clearing four enemy machine gun positions.</p><br>


                                <div class="war-2-map-2">
                                    <img src="assets/img/Gothic-Line.jpg" width="100%" height="400px" style="margin-bottom: 20px;" class="img-fluid" alt="">
                                    <p style="color: #D90000;">An Indian sentry guards a dangerous path somewhere on the Gothic Line in Italy, 1944 <br> <strong>Source: USI of India. </strong></p>
                                </div>


                                <div class="war-2-map-2">
                                    <img src="assets/img/gallery/Indian-infantry.jpg" width="100%" height="400px" style="margin-bottom: 20px;" class="img-fluid" alt="">
                                    <p style="color: #D90000;">Indian infantry clear an Italian street of German snipers during the advance to Rome, 1945 <br> <strong>Source: MoD, History Division</strong></p>
                                </div>

                                <p>The Allies now closed in on the Gothic Line. ‘The 4 and 10 Indian Divisions and 43 Gurkha Lorried Brigade took part in the battles to clear this line and the subsequent pursuit.’ The 10th Indian Division arrived in Italy at the end of March 1945. ‘In one of these actions Naik Yashwant Gadge of 3 Marathas won a posthumous Victoria Cross for single-handedly charging and destroying an enemy position when his Company Commander and six other NCOs had been killed.’</p><br>

                                <p>The next major action was the crossing of the Senio River on 9 April 1944, which involved 19th and 21st Brigades of the 8th Indian Division. During this attack, Sepoy Ali Haider of 6/13th Frontier Force Rifles and Sepoy Namdeo Jadhav of 1/5th Marathas were awarded VCs. Through this bridgehead, the 8th Indian Division broke through and carried out the pursuit till the end of the surrender of German forces in Italy on 3 May 1945. During this period, Rifleman Thaman Gurung of 1/5th Royal Gurkha Rifles (FF) was awarded a posthumous VC for action at Monte San Bartolo.</p><br>
                            </div>
                        </div>


                        <!-- <div id="note" class="accordion-details">
                            <h3>Notes</h3>

                            <div class="war-details">
                                <ol>
                                    <li>Ibid., p. 42.</li>
                                    <li>Rana Chhina, The Last Post: Indian War Memorials around the World, New Delhi: USI, 2014, p. 40.</li>
                                    <li>Ibid.</li>
                                    <li>Raghavan, India’s War: The Making of Modern South Asia, 1939–1945, n. 36, p. 33.</li>
                                    <li>Raghavan, India’s War: The Making of Modern South Asia, 1939–1945, n. 36, p. 63.</li>
                                    <li>Ibid., p. 64.</li>
                                    <li>Sri Nandan Prasad, Expansion of the Armed Forces and Defence Organization, 1939–45, p. 73, cited in ibid., p. 69.</li>
                                    <li>Raghavan, India’s War: The Making of Modern South Asia, 1939–1945, n. 36, p. 69.</li>
                                    <li>Ibid., p. 67.</li>
                                    <li>Ibid., p. 68.</li>
                                    <li>This section has been adapted from Singh, ‘The World Wars and Prelude to Independence’, n. 25, pp. 42–45. 92 Valour and Honour</li>
                                    <li>See Singh, ‘The World Wars and Prelude to Independence’, n. 25, pp. 43–44.</li>
                                    <li>Ibid., p. 44.</li>
                                    <li>Ibid., p. 45.</li>
                                    <li>This section has been adapted from Singh, ‘The World Wars and Prelude to Independence’, n. 25, pp. 48-53.</li>
                                    <li>Ibid., p. 50.</li>
                                    <li>Ibid.</li>
                                    <li>Ibid.</li>
                                    <li>Ibid., p. 51.</li>
                                    <li>Ibid.</li>
                                    <li>Ibid.</li>
                                    <li>Ibid.</li>
                                    <li>Robert Lyman, Japan’s Last Bid for Victory, p. 256, cited in Hemant Singh Katoch, ‘The Battle of Imphal: March–July 1944’, Journal of Defence Studies, Vol. 8, No. 3, July–September 2014, p. 102.</li>
                                    <li>See Katoch, ‘The Battle of Imphal: March–July 1944’, n. 65, p. 102.</li>
                                    <li>Singh, ‘The World Wars and Prelude to Independence’, n. 25, p. 51. Also see Katoch, ‘The Battle of Imphal: March–July 1944’, n. 65, pp. 101–120.</li>
                                    <li>Singh, ‘The World Wars and Prelude to Independence’, n. 25, p. 52.</li>
                                    <li>This section has been adapted from Singh, ‘The World Wars and Prelude to Independence’, n. 25, pp. 46–48.</li>
                                    <li>Ibid., p. 46.</li>
                                    <li>Ibid.</li>
                                    <li>Ibid., p. 48.</li>
                                    <li>Ibid.</li>
                                    <li>Ibid.</li>
                                </ol>
                            </div>
                        </div> -->


                        <!-- <div id="gallery" class="row image-gallery">
                            <div class="col-md-12">
                                <h3>Gallery</h3>
                            </div>

                            <div class="col-md-4 main">
                                <div class="image-main img-pop">
                                    <img src="assets/img/gallery-1.png" class="img-fluid" alt="">
                                    <p>Line of communication in mountian </p>
                                </div>
                            </div>


                            <div class="col-md-4 main">
                                <div class="image-main img-pop">
                                    <img src="assets/img/gallery-2.png" class="img-fluid" alt="">
                                    <p>Evacuation of Refugee by IAF</p>
                                </div>
                            </div>


                            <div class="col-md-4 main">
                                <div class="image-main img-pop">
                                    <img src="assets/img/gallery-3.png" class="img-fluid" alt="">
                                    <p>Construction of Bridge</p>
                                </div>
                            </div>


                            <div class="col-md-4 main">
                                <div class="image-main img-pop">
                                    <img src="assets/img/gallery-4.png" class="img-fluid" alt="">
                                    <p>Army Engineers working at Jhanger Road </p>
                                </div>
                            </div>


                            <div class="col-md-4 main">
                                <div class="image-main img-pop">
                                    <img src="assets/img/gallery-5.png" class="img-fluid" alt="">
                                    <p>Army doctor's aid to civil</p>
                                </div>
                            </div>


                            <div class="col-md-4 main">
                                <div class="image-main img-pop">
                                    <img src="assets/img/gallery-6.png" class="img-fluid" alt="">
                                    <p>An Army Officer helping a Kashmiri Boy in schooling at Rajaur</p>
                                </div>
                            </div>

                        </div> -->

                        <!-- <div id="video" class="row image-gallery">

                        <div class="col-md-12">
                            <h3>Video</h3>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="featured-video" >
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <div class="card-body">
                                    <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                    <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                    <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="featured-video" >
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <div class="card-body">
                                    <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                    <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                    <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="featured-video" >
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <div class="card-body">
                                    <p><i class="fa fa-calendar"></i> November 5,2022 <i class="fa fa-message"></i> 255</p>
                                    <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                    <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>


                    </div> -->

                    </div>

                </div>
</section>


<?php include('footer.php'); ?>