<?php
$main ="indian-army";
$page="growth-page";

get_header(); ?>


    <section class="regiments-and-corps-banner"
        style="background-image: url(../assets/img/regiments-and-corps-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Growth Of Indian Army - Post 1947</h1>
        </div>
    </section>

    <section class="regiments-and-corps-detail">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9  col-sm-12">
                    <h2>The Indian Army Since 1947</h2>
                    <p>The end of World War II in 1945 signalled the beginning of a new era. It was apparent during the course of the war that Britain would no longer be able to hold on to its empire/dominions in the aftermath of the war. The first of its colonies to declare independence was India, which gained independence formally from the British Empire in August 1947. Independence, however, came at a cost—the subcontinent was to be partitioned into two nations, India and Pakistan. Along with the partition, the armed forces were also to be divided between the newly independent countries of India and Pakistan. This was to take place simultaneously with the movement of masses of civilian population between the two countries, a time of great strife.</p>
                      
                                            
                    <p>The division of the Indian Army itself was a task of mammoth proportions. It was agreed that by 15 August 1947, India and Pakistan should each have, within their territories, effective forces predominantly of non-Muslims and Muslims, respectively, under their own control. A large portion of the Army consisted of mixed classes and involved a major re-organisation of practically all units, which were cantoned on an all-India basis. Both Dominions selected Commanders-in-Chief (Cs-in-C) of the army, navy and air force from amongst British officers, to be directly responsible to them and to exercise executive control of all forces located in their territory after 15 August. The navy and the air force did not pose a serious problem due to their small size, but the Indian Army had swelled during the Second World War to a massive 2.5 million men.</p>

                      
                    <p>An Armed Forces Reconstitution Committee, with representatives of India and Pakistan, was set up under the Chairmanship of Field Marshal Auchinleck for the purpose, to divide the units and stores in the ratio of 2:1 between India and Pakistan, respectively. As the Supreme Commander of the pre-independence Indian Army, Auchinleck was made responsible for the division and dispatch of units and stores to reach their allotted dominions on a timed programme. Most of the army units contained men of different classes and religion at the sub-unit level. Under the Army Partition Scheme, a Muslim soldier/officer domiciled in Pakistan and a non-Muslim domiciled in the rest of India had no choice other than to serve his respective dominion. A Muslim from India or a non-Muslim from Pakistan could, however, elect which dominion he would serve. The task was stupendous and time woefully short. Moreover, given the size of the new countries, the proportion of armed forces retained by each was in the ratio of 2:1, with India retaining the larger chunk of both officers and soldiers at the end of the division.</p>
                      

                    <p>It was soon clear that the task of moving troops and stores could not be completed by 15 August 1947. The time schedule was further delayed due to complete break- down of law and order in the aftermath of independence, the unforeseen mass migration of minorities in Punjab and Bengal, and the inadequacy of the railways. The problem was compounded by large numbers of demobilised soldiers who retained the combat experience they had gained during the war. And almost immediately, India was embroiled in a 14-month long conflict with Pakistan over the territory of Jammu & Kashmir (J&K). Soon after independence in 1947, hordes of tribals (‘lashkars’) from the North West Frontier Province (NWFP) in Pakistan, were infiltrated into J&K as part of Operation Gulmarg. By late 1947, the lashkars were virtually at the gates of Srinagar. The actions by Pakistan led the ruler of J&K, Maharaja Hari Singh, to sign the Instrument of Accession with India and seek New Delhi’s help in dealing with the situation. The Indian Army, along with the Indian Air Force, was dispatched to Srinagar to deal with the situation.</p>
                      
                    <p>The war in 1947–48 is a saga of bold actions led by aggressive and highly professional commanders. The reactions of the Indian Army were spontaneous, quick and responsive to a highly fluid situation, reflecting on the skill, motivation and morale of a fledging and nascent Indian Army, called to save its motherland at its very inception. Although the actions in 1947–48 were predominantly infantry-led, commanders were innovative in their use of artillery and tanks/armoured cars, like the amazing feat by 7 Cavalry in crossing the Zozila Pass.</p>


                    <div class="war-2-map-2">
                        <img src="../assets/img/Fatu La-JK-1947–48-13.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                        <p style="color: #D90000;">A supply convoy winds its way through Fatu La, J&K, 1947–48 <br> Source: USI, CMHCS.</p>
                    </div>

                    <p>In the past seven decades, the Indian Army has seen phases of consolidation, organisational restructuring, operational and doctrinal reorientation, increased variation of roles owing to regional factors.</p>



                    <h2 class="heading-border">1950s: Consolidation</h2>
                    <p>The decade of the 1950s can be viewed as a phase of consolidation for the Indian Army. With the coming into force of the Constitution of India on 26 January 1950, India became a sovereign democratic republic, with the President of India as the Supreme Commander of the Armed Forces. As with the newly independent nation, its armed forces too looked forward to a new beginning. In the 1950s, the Indian Army looked to expand and build structures and institutions keeping in mind the threat from Pakistan as well as the myriad needs of state building. This was also the time of a renewed focus on the Higher Defence Organisation (HDO). Newly independent India’s ‘strategic thinking rested on four pillars: to maintain conventional military superiority over Pakistan; to maintain friendly relations with China; to stay free of Cold War politics and entanglements; and to promote solidarity and cooperation among developing countries.’ This formed the context for the army’s consolidation phase.</p>
                      
                    <p>The decade started with the successful defence of J&K from Pakistani designs. India had also taken the momentous step of declaring itself a constitutional republic in 1950, thus setting out a clear agenda of the future. In its strategic outlook at the time, ‘India’s principal threat perception was from Pakistan…’. At the same time, the Indian Army too was moving ahead from its pre-war role, which was in service of empire, to its role of guardians of the nation’s borders in the face of a belligerent enemy. The Indian Army during World War II was a volunteer-driven army, and its expansion at the time was necessitated by the demands of a war that spanned the globe. However, in the immediate aftermath of the war, it was downsized. In the 1950s, however, fresh from a war with Pakistan immediately upon gaining independence from British rule, the Indian Army underwent another expansion in light of the immediate security concerns in the subcontinent. However, owing the diplomatic endeavours of the Government of India both regionally as well as globally in the first half of the decade, ‘apart from Pakistan, Indian leaders did not perceive a direct military threat to India’s security from any other neighbouring state, including China.’</p>
                      
                    <p>This decade also saw India’s foray into peacekeeping activities under the aegis of the United Nations (UN). The UN was formed just after the end of the Second World War, the most destructive conflict of the twentieth century at the time. India, though still under the British Crown, was an original founder member of the UN and a signatory to its charter in 1945. Its armed forces have been participating in UN Peacekeeping missions since the inception of the first UN peacekeeping mission in 1948. In the 1950s alone, the Indian Army participated in three major peacekeeping operations: Korea (1950–54), Indo-China (1954–1970), and West Asia (1956–67). A few peacekeeping missions are discussed subsequently.</p>
                      
                    <p>The decade of the 1950s were, however, not entirely uneventful. With the Cold War in play, the decade saw instances of conflict across the globe. Closer home, India increasingly moved towards a situation of potential conflict with the People’s Republic of China (PRC). The Chinese People’s Liberation Army (PLA) marched into Tibet in 1950 taking control of the region. India acknowledged Chinese control of the region in 1954 and recognised Tibet as an ‘autonomous’ region of China. However, as the Chinese ‘gained a stranglehold on Tibet, the Dalai Lama fled to India in 1959 where he was welcomed and settled in Dharamshala. After that, relations with China deteriorated and culminated in the 1962 war.’</p>
                    </p>


                    <h2 class="heading-border">1960s: Conflict on Two Fronts</h2>
                    <p>The decade of the 1960s saw the Indian Army fight two wars on different fronts, terrains, with different outcomes for both and lessons learnt. Sino-India relations in the 1950s deteriorated considerably by the end of the decade, especially after India gave sanctuary to the Dalai Lama. Furthermore, India had inherited from the British un-demarcated boundaries with Tibet in the north and the east, which increasingly became a point of concern over the course of the decade. India and China saw border clashes in 1959 and failed diplomatic negotiations thereafter in 1960. ‘In late 1961, however, the Government of India opted for…the so-called Forward Policy decision. By the middle of 1962, small numbers of lightly armed Indian infantry established several ‘forward posts’ deep inside unoccupied but dis- puted border areas.’</p>

                      
                    <p>While the 1962 Sino-Indian conflict officially began with Chinese attacks in October of that year, events earlier in the year foretold the conflict. In early June 1962, India had established an Assam Rifles post called ‘Dhola’ in the area of Bridge III on the southern bank of Namka Chu. The Chinese reacted in September and surrounded the Indian Post. The troops of both countries ultimately came to blows resulting in casualties on both sides. Subsequently, the 7 Infantry Brigade moved forward from Tawang to Namka Chu on the Indian side, with orders to evict the Chinese from Thagla Ridge. On the Chinese side, the attacking troops were already on the move to their assembly areas. The stage was now set for the events for the breakout of war on 20 October 1962.</p>

                      
                    <p>Soon after the war was over, Pakistan saw an opportunity to exploit the situa- tion. By this time, Pakistan had joined the Central Treaty Organisation (CENTO) and the South East Asia Treaty Organisation (SEATO) and was firmly ensconced in the US-led bloc. Sino-Pakistan relations began to deepen in the aftermath of the 1962 war, with both seeing a common adversary in India. In 1963, Pakistan allowed China access to the Shaksgam Valley, further bolstering ties between the two countries. By 1965, the Pakistan Army felt it was opportune to attempt to wrest Kashmir from India’s hands. ‘The operations in J&K in August–September 1965 were preceded by the operations in Kutch which commenced in February of that year. The terrain offered advantage to Pakistan to launch offensive operations, which were soon brought to a halt by the Indian Army. As a strategic response, the Indian Army deployed on the border in Punjab and captured three posts in Kargil on 16 and 17 May 1965. A ceasefire agreement was signed on 1 July 1965.’</p>

                                         
                    <p>The main war was fought between the two countries in August-September 1965 and the Indian Army found itself stretched, for the first time, on two fronts. Operation Gibraltar was conceptualised with the objective of creating large-scale disturbances in J&K. The plan was to send about 8,000 Pakistani soldiers and Razakars into J&K disguised as guerrillas. The first phase was to create a shock wave by launching raids on selected targets, thereby preparing the ground for a civil uprising due to the chaos and consternation that would be caused in the state. The second was the fusion of the civil uprising with the infiltration operation. Operation Gibralter was combined with Operation Grand Slam, which was the plan for an armoured thrust across the ceasefire line (CFL), with a view to capture Akhnoor, thereby threatening the line of supplies from India to Srinagar. The plan, in principle, was accepted by Field Marshal Ayub Khan in May 196532 and put into action in July 1965. This is discussed in greater detail subsequently.</p>


                    <div class="war-2-map-2">
                        <img src="../assets/img/Army jawans patrolling in Kashmir Valley, 1965-17 1.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                        <p style="color: #D90000;">Army jawans patrolling in Kashmir Valley, 1965</p>
                    </div>

                      
                    <p>The losses incurred during the 1962 war as well as the 1965 war made the government realise need to modernise the army and to expand it to be able to face the threat on two fronts, Pakistan and China. Plans were thus initiated to expand the army with six new divisions, which would also entail increase in the manpower from 5.5 lakh to around 8.25 lakh. Scout Battalions were raised for deployment on the China border, in the northern and eastern sectors. Of the six new divisions raised initially, four were designated as mountain divisions and an existing plain division was converted into a mountain division. With the help of the United States (US) and the United Kingdom (UK), equipment modernisation also commenced, particularly of small arms and signal equipment. An emphasis on mountain and jungle warfare was laid. To allow for greater attention to the eastern frontiers, Headquarters Eastern Command was moved to Calcutta (Kolkata) and a new Central Command was raised at Lucknow to cater for the threat from China. To address the need for increased requirement of officers, a scheme for Emergency Commissions was started and Officers Training Schools were opened at Madras (Chennai) and Pune to train the increased intake.</p>

                      

                    <p>Within Army Headquarters (HQ), three important directorates were also reorganised to entail greater efficiency. ‘The Weapons and Equipment Directorate was shifted from the Master General of Ordnance to the General Staff Branch as was Military Survey from the Engineer-in-Chief Branch.’ Intelligence, which had emerged as a key area of concern, especially in 1962, was also addressed.</p>
                      
                    <p>The 1965 operations had shown voids in the plains orbat, particularly in armour, artillery and engineer support. The raising of the necessary units and formations was now taken into hand. The 1965 operations of the Armoured Division had brought out the incompatibility of Infantry mounted on trucks accompanying tanks into bat- tle. Steps were taken to obtain Armoured Personnel Carriers (APCs) and by 1969 the infantry battalion in the Armoured Division and Independent Armoured Brigades were receiving these. These battalions are the genesis of the Mechanised Infantry Regiment that was formally raised in 1979.</p>
                      
                    <p>On the training side, stress was laid on realistic individual and collective training. The lessons of both 1962 and 1965 were incorporated into syllabi of the various courses at schools of instruction. The mountain formations were trained to operate on man pack and animal transport basis for limited periods and to sustain themselves when isolated. In the plains, training stressed ability and skills required to deal with armoured attack and in infantry-tank cooperation. The improvement in battle inoculation practices and in army, air cooperation had already been put into practice. ‘A Directorate of Combat Development was established under the General Staff Branch to review tactical concepts; develop organisations and mate- rials in light of new tactical concepts; and for conduct of trials in formations and experimental formations.’</p>
                      

                    <p>As a follow up of lessons learnt from the 1965 war a number of manufacturing facilities were set up; the most prominent of these were the Heavy Vehicle Factory at Avadi, near Madras to manufacture the Vijyant Main Battle Tank, and Bharat Electronics Ltd at Bangalore to manufacture communication equipment. In effect, by the end of the 1960s, the Indian Army had begun the necessary organisational changes that would take it greater heights in the coming decade. Weapons and equipment were also sought to be modernised, with moves to induct the 7.62 mm self-loading rifle, introduction of short and medium range mortars, and mountain guns with high-trajectory firing capability</p>
                    </p>



                    <h2 class="heading-border">1970s: Victory and Move to Modernise</h2>
                    <p>In the late 1960s, developments especially in East Pakistan, foretold of yet another war in the subcontinent. From 1947 till 1971, Pakistan comprised territories in the western and eastern part of the subcontinent that were culturally and linguistically different. Moreover, the population of East Pakistan was far greater in number than in the west. Elections held in December 1970 in Pakistan gave a majority to Sheikh Mujibur Rahman. The Pakistani establishment viewed with alarm the rise of Bengali nationalism in the east and moved to supress it. The Pakistan Army put into action Operation Searchlight on 25 March 1971. Genocide was conducted by the Pakistani government and army against their own citizens. Over the course of the year, and till the outbreak of war in December, 10 million refugees poured into India, escaping the excesses of the regime. India, on its part, began pre- paring for war while putting in considerable diplomatic efforts to bring the events in its neighbourhood to the attention of the world. The last major war between India and Pakistan was fought from 3–16 December 1971 and led to the creation of Bangladesh. This was the Indian Armed Forces’ finest hour, in particular that of the Indian Army, which had managed to throw off the yoke of defeat less than a decade previously to emerge triumphant.</p>


                    <div class="war-2-map-2">
                        <img src="../assets/img/Indian Troops, CEC TIVITER, Shakargarh Area, 1971-18 1.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                        <p style="color: #D90000;">Indian Troops, CEC TIVITER, Shakargarh Area, 1971 </p>
                    </div>

                          
                    <p>Soon after the 1971 Indo-Pak war, commanders down to unit level got down to review performance and conduct of operations to analyse the performance of both sides so that they could learn from the experience of victory and defeat, and to build on strengths and eliminate weaknesses. Among the major lessons that were learnt were the need for obtaining continuous intelligence about one’s adversaries; the requirement for visualising the various contingencies that may arise in which the armed forces may get involved in; the need for planning and training for such contingencies in peace-time; ingenuity of plans and their dynamic execution; the use of unexpected axes of advance, outflanking envelopment including vertical envelopment of the enemy; the use of technology to improve logistics and administration and to upgrade weapons and equipment; the importance of inter-service cooperation and closer interaction with the civil services; and, most important of all, improvement in the training and morale of the officers and men of the army. The importance of outstanding civil and military leadership was obvious. In 1975, the Government of India constituted what came to be known as the ‘Experts Committee’ to go into the future of the defence of the country, essentially undertake a long-term perspective planning exercise. The purpose behind constituting the Committee was to carry out an analysis of the changing world scenario; the need for requisite defence preparedness to maintain credibility; the escalating defence budget; greater sophistication of weaponry and equipment; the concurrent increase in manpower costs; the ‘ad hoc’ manner in which defence matters were dealt with; and, finally, the need for ensuring maximum cost effectiveness while endeavoring to improve the fighting capacity of the defence forces. The Experts Committee had as its chairman Lieutenant General (Lt Gen) KV Krishna Rao with Major Generals (Maj Gen) ML Chibber and K Sundarji as members, and Brigadier (Brig) AJM Homji as secretary. The Committee was constituted with effect from 1 November 1975 and was to submit its report within nine months. The Committee decided that the time-frame for consideration should be sufficiently far ahead and should cover a period of 25 years, that is, from 1975 to 2000.</p>
                          
                    <p>The Committee’s report comprised nine volumes and covered all important aspects of defence preparedness; threats to national interests; future strategy; force levels over different time-frames; future battlefield environment; acquisition and production of sophisticated equipment; improvement of command and control and intelligence; and measures to re-deploy resources for an improved and more cost-effective utili- sation. The report’s recommendations resulted in considerable improvement of the teeth-to-tail ratio and ultimate attainment of a reasonable deterrence capability, a saving of about 1 lakh personnel and 20,000 vehicles of all types within the army, and, in the process, leading to a saving of Rs 200 crore on the initial and Rs 100 crore on the recurring expenditure.</p>

                          
                    <p>The Experts Committee Report was, however, not properly implemented. It is only when Lt Gen KV Krishna Rao, the Chairman of the Committee, was posted as Deputy Chief of Army Staff that the recommendations began to be implemented and were dove-tailed with the 1979–84 Five Year Plan and subsequent Five Year Plans. The net effect of measures taken by Rao as Deputy Chief of Army Staff was that the teeth to tail ratio improved by approximately 70:30 and the amount spent on the army’s modernisation went up from 3 percent to 20 percent.</p>
                          
                    <p>The institution of the Expert Committee in 1975 brought out for the first time the need for deliberate long-term planning. Till this time, expansions were carried out intermittently to meet unforeseen contingencies and crisis situations. The following conclusions emerged in respect of the long-term needs of the army:</p>

                          
                    <!-- <p>    The institution of the Expert Committee in 1975 brought out for the first time the need for deliberate long-term planning. Till this time, expansions were carried out intermittently to meet unforeseen contingencies and crisis situations. The following conclusions emerged in respect of the long-term needs of the army:</p> -->
                          
                        <p>    1.	Adequate infantry formations would be required to hold the defences, take the initial brunt of the enemy’s attack, contain his thrusts, and inflict maximum attrition. Sufficient mobile reserves would be needed. In the plains, they would have to be based on armour and mechanised infantry.</p>

                          
                        <p>2.	Sufficient strike capability based on integrated teams of armour, mechanised infantry, air defence artillery, assault engineers, and armed helicopters would need to be developed. Adequate vertical envelopment capability would also be required with the strike forces.</p>
                          
                        <p>3.	Close air-support, closely integrated, for both offensive and defensive operations.</p>


                          
                        <p>4.	Adequate air transport for airborne operations as well as for strategic move of troops.</p>
                          
                        <p>5.	Amphibious capability for carrying out limited amphibious operations</p>

                          
                        <p>6.	Electronic warfare capability to include acquisition and surveillance, guidance of weapons and communications as well as electronic and counter-counter measures and satellite surveillance.</p>

                          
                        <p>7.	Enhanced night fighting capabilities to make night fighting as efficient as that by day.</p>
                          
                        <p>8.	Logistic support to match mobility of the forces.</p>


                    <h2 class="heading-border">1980s: Challenges of Modernisation and Domestic Turmoil</h2>
                    <p>Subsequent to the recommendations of the Lt Gen Krishna Rao-led Expert Committee, the Indian Army moved towards mechanisation of its forces. This was further aided by the increase in defence expenditure. One of the first moves was the raising of the Mechanised Infantry Regiment on 2 April 1979. By the end of the 1980s, especially during the tenure of General (Gen) Sundarji as Army Chief, 23 mechanised battalions had been raised, equipped with infantry combat vehicles; moreover, [Sundarji’s] tenure also provided moorings for the employment of these forces.’ The Army Aviation Corps was raised in 1986, in keeping with the require- ments of modernisation and technological innovation. Additionally, the decade saw the induction of the 155mm Bofors gun, the ‘redesignating [of ] Infantry Division as an Air Assault Division and [the] raising of Reorganised Army Plains Infantry Division (RAPID) with an enhanced component of armour and mechanised infantry.’</p>
                          
                    <p>The 1980s saw the implementation of futuristic organisational changes in the Indian Army. ‘The impact of [Gen] Sundarji’s drive, strategic vision and close work- ing relationship with the [Government] created substantial changes in the army’s organisational structure. Besides the acquisition of assets, it laid the foundation for mobile warfare and simultaneously propelled a change in the thinking of the army’s leadership. These were based on a change in the army’s doctrine as well. This shifted from defensive deterrence, witnessed prior to the 1971 war, to “deterrence by punishment” during the 1980s, bordering on compellence. This shift reflected the signs of a changing strategic culture in the army, which was injected by offensive thinking and a more robust approach to potential adversaries.’</p>

                          

                    <p>The tenure of Gen Sundarji as the Chief of Army Staff also saw the beginnings of what would eventually evolve into the ‘Cold Start’ doctrine. In 1986, the army undertook a massive exercise in the western sector—Operation Brasstacks—to gauge the viability of this doctrine. This was in line with a more assertive approach of the army as a whole, reflecting both in the long-term planning undertaken by HQ and the modernisation undertaken at the organisational level.</p>
                          


                     <p>India’s extended neighbourhood saw turmoil in this decade, with the Iranian Revolution in 1979 followed by the eight year-long Iran-Iraq War. The year also saw the Soviet Union invade Afghanistan ostensibly to aid the Communist Government in Kabul in its fight with anti-Communist guerrillas. The war in Afghanistan would lead to the eventual disintegration of the Soviet Union as also provide Pakistan with the means and ability to rebound from its defeat in 1971. Domestically too, events were occurring that had a profound impact on the Indian Army in the twentieth century. By the late 1970s, reports began to emerge of Pakistani activities in the region of the Siachen glacier and the surrounding mountains. Meanwhile, in J&K itself, a movement had started for the establishment of madarsas teaching a funda- mentalist version of Islam as opposed to the Sufi tradition based on a tolerant Islam, which was prevalent till then in the Valley. There was also a growing resentment and disillusionment with the government led by Farooq Abdullah. In Punjab, militancy was at its peak. In the north-east, the All Assam Students Union started an agitation against ‘foreigners’, that is, any non-Assamese residing in the state as also for greater autonomy. In neighbouring Sri Lanka, long-standing political differences between the Sinhala and Tamil populations led to the start of a violent movement for Tamil Eelam or Tamil homeland in 1975. While still at a reasonably low-key level in 1981, this Tamil movement had numerous sympathisers in Tamil Nadu. In 1987, under the mandate of the Indo-Sri Lanka Accord, India undertook a peacekeeping operation in Sri Lanka with the aim to disarm various military groups that were involved in the conflict. The Indian Army was a key part of the Indian Peace Keeping Force (IPKF). The IPKF was soon involved in a battle with the LTTE in contravention to its initially envisaged role.</p>
                          

                     <p>One of the most significant operations that the Indian Army was involved in in this decade was the control of the Siachen Glacier. In April 1984, reports of Pakistani attempts to control the glacier was countered with Indian troops landing near Bilafond La, a pass on the Saltoro ridge, west of the Siachen glacier. The origin of the dispute lies in differing interpretations by both sides of the alignment of the un-demarcated portion of the Line of Control established by the Shimla Agreement signed on 2 July 1972 after the 1971 Indo-Pak War. In January 1985, Pakistan launched an attack to try and capture Bilafond La but failed. A similar effort in February 1985 to occupy the heights overlooking Sia La was also foiled. In late March/early April 1987 the Pakistani Special Services Group (SSG) occupied a position overlooking the Bilafond La area at a height of 21,156 ft atop a vertical cliff. The occupation came to light when Indian helicopters approaching Bilafond La came under fire from the post, which was called the Quaid Post. The Indian Army launched an operation to wrest control of the post. On the night of 25–26 June 1987, a force of one JCO and six other ranks inched forward and attacked the post with grenades and small arms fire. Naib Subedar Bana Singh of 8 JAK LI who led the assault was awarded the Param Vir Chakra and the post was renamed as Bana Post.</p>
                          

                     <p>By the end of the decade, Pakistan had fine-tuned its strategy of ‘bleeding India with a thousand cuts’. The flames of political turmoil in the Kashmir Valley were sought to be fanned and the Pakistani Inter-Services Intelligence (ISI), fresh from its successes in Afghanistan, opened a new chapter of asymmetrical warfare against India with Operation Tupac.</p>


                          
                     <!-- <p>    By the end of the decade, Pakistan had fine-tuned its strategy of ‘bleeding India with a thousand cuts’. The flames of political turmoil in the Kashmir Valley were sought to be fanned and the Pakistani Inter-Services Intelligence (ISI), fresh from its successes in Afghanistan, opened a new chapter of asymmetrical warfare against India with Operation Tupac.

                        
                    </p> -->





                    <h2 class="heading-border">1990s: Internal Security Gains Momentum</h2>

                    <p>The primary task of the army is to defend India’s borders, though as part of the military arm of the state it can be (and has been) called in to aid civilian authorities as well as the government in times of crisis. In light of the growing militancy in Kashmir in the early 1990s, encouraged and supported by Pakistan, in this decade the internal security (IS) duties of the Indian Army increased manifold. To address this, the Rashtriya Rifles (RR) was created to handle this aspect. ‘The RR units were raised late in 1990 by the then Army Chief Gen V N Sharma with Lt Gen P C Mankotia as its first Director-General. Initially, there were six such battalions, three in J&K and three in Punjab. Later, all the Rashtriya Rifles battalions were moved to Jammu and Kashmir as the valley got entangled in a bloody conflict with Pakistan-trained militants creating havoc in the region.’ As of today, the RR has 63 battalions.</p>

                      
                    <p>As mentioned earlier, the Indian Army was divided in 1947 between India and Pakistan. Steven I. Wilkinson contends that the ‘…socioeconomic, strategic, and (especially) military inheritance that Pakistan received in 1947…’ was worse than what India received. This was ‘…in its trained officers and officials, state institutions, army and the stocks of capital and goods it received as part of the division of the country.’ Added to that were the Pakistan Army and political establishment’s aims being thwarted by the Indian Army in 1947–48, 1965 and in 1971, when the country was dismembered with the creation of Bangladesh. With the coming to power of General Mohammad Zia-ul-Haq in Pakistan in 1977, the process of Islamisation of the military and society was championed. Further, given the Pakistan military’s obvious asymmetry with India, with its larger and more superior military force, a policy of asymmetrical warfare against India was adopted. The thwarting of its ambitions in Siachen in the 1980s only added to the above.</p>

                      
                    <p>Operation Tupac initiated in 1980s under Gen Zia-ul-Haq but conducted mostly after his death in a plane crash in 1988, is a military intelligence operation conceived to foment unrest in the Kashmir Valley by providing support to separatists and militants. The programme has been run by the Pakistani ISI. The stated aim was to aid separatist forces in the Valley. The operation took advantage of political turmoil in the Valley in the late 1980s. ‘The aim was to annex the state of J&K through a proxy war by infiltrating militants to foment trouble in the state under the garb of a jehad, take militancy to uncontrollable levels, and, at an opportune time, strike with regulars, if necessary to finally integrate J&K with Pakistan.’ Estimates suggest that around ‘15,000-20,000 persons exfiltrated from J&K to POK [Pakistan Occupied Kashmir]/Pakistan for arms training in 1987–1988.’ The intent was to support a low-level insurgency and the Pakistan Army had established contact with the Jammu and Kashmir Liberation Front (JKLF), ‘which agreed to cooperate on the condition that Kashmir would be granted independence soon after Indian control ceased on the state.’ As the war in Afghanistan wound down in the early 1990s, fighters moved into Kashmir adding to the growing levels of insurgency. Pakistan too scaled up the insurgency infrastructure between 1990–1994, and provided insurgents with sophisticated weaponry and training.</p>
                      

                    <p>Beginning January 1990, militants began targeting the minority Kashmiri Pandit population, which resulted in an exodus of Pandits from the Valley. The civil administration and law and order had virtually collapsed in the state. The mantle of countering the growing insurgency in Kashmir thus fell on the Indian Army, especially on the 15 and 16 Corps. Aiding the army in this endeavour were the RR battalions, CRPF and BSF also contributed. Insurgency in the Valley peaked around the mid-1990s and saw a declining trend thereafter. However, India and Pakistan would soon see yet another conflict before the end of the decade.</p>
                      

                    <p>On 11 May 1998, India conducted a nuclear test in the Pokhran range in Rajasthan. Pokharan II not only advertised India’s status as a nuclear power, it impacted regional geopolitics in a major way. Pakistan, India’s all-weather rival in South Asia, followed by conducting its own nuclear test. The conduct of nuclear tests in 1998 not only made South Asia a nuclearised zone, it also meant that any military conflict between India and Pakistan would now be conducted under a ‘nuclear overhang’. The spectre of two nuclear armed countries going to war was a matter of deep concern both regionally and globally.</p>

                      

                    <p>The nuclear tests of 1998 marked a significant shift in the context of conflict and warfare in the subcontinent: not only was there now a nuclear overhang, given the rapidly changing technology as well as its adoption by militaries, the character of war also underwent change. With nuclear deterrence in the fray, the likelihood of large-scale conventional wars between the two countries took a backseat. What were expected were short, localised and intense technologically-driven campaigns. And Pakistan’s actions in the summer of 1999 in the Kargil sector were a real time example.</p>
                      


                    <p>Pakistan’s domestic politics have been tenuous since its creation. Beginning in the 1950s with the military coup by Gen Ayub Khan, Pakistani politics see- sawed between civilian and military rule. After the death of Gen Zia-ul-Haq in 1988, the reins of power in Islamabad passed between the premiership of Benazir Bhutto of the Pakistan People’s Party (PPP) and Nawaz Sharif of the Pakistan Muslim League (PML). In February 1999, the premiers of India and Pakistan, Atal Bihari Vajpayee and Nawaz Sharif signed the Lahore Declaration seeking to regulate the development of nuclear weapons and avoid accidental or unauthorised operational use of such weapons. However, the Pakistan Army had different plans.</p>
                      

                    <p>In 1998, the Pakistan Army surreptitiously occupied the vacated winter posts of the Indian Army in the Kargil sector of the Line of Control (LOC). The aim was to cut the link between Kashmir and Ladakh, to isolate Indian Army troops on the Siachen Glacier, and force India to negotiate a settlement of the Kashmir dispute. The Pakistani intrusions were detected on 3 May 1999, when local shepherds reported the intrusion of armed men into the area of 3 PUNJAB, the Indian Army unit stationed there. 3 PUNJAB launched a series of patrols and confirmed the intrusions. On 5 May 1999, Operation Vijay was launched to evict the Pakistanis from Indian territory. Two months of violent confrontation later, a ceasefire was declared on 26 July 1999. The Kargil intrusion was yet another attempt by the Pakistan Army, under the leadership of General Pervez Musharraf to change the position of the LOC, but was thwarted by the stellar role played by the Indian Army, along with the IAF and the Indian Navy.</p>


                    <div class="war-2-map-2">
                        <img src="../assets/img/KARGIL WAR PD DPR MOD (1) 1.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                        <p style="color: #D90000;">Kargil, 1999</p>
                    </div>
                      

                    <p>Operation Vijay was virtually fought live on television. The events in Kargil not only ‘captured the imagination of people in India, the conflict also raised the morale of the armed forces…firing an entire generation of soldiers.’</p>

                      
                    <p>In October 1999, a few months after seeing defeat in Kargil, General Pervez Musharraf engineered a coup against the Nawaz Sharif government. The Prime Minister was exiled to Saudi Arabia and the Pakistan Army under Musharraf again took the centre-stage in Pakistani politics.</p>



                    <h2> 2000s: Indian Army in the New Millennium</h2>
                    <p>In the aftermath of the successful execution of Operation Vijay, some key issues, including that of intelligence and on-the-ground surveillance in the region of the LOC, emerged as areas of concern. In the aftermath of Kargil, the government undertook an exercise of assessment of events in the summer of 1999 by appointing a review committee headed by K. Subhramanyam, with Lieutenant General KK Hazari (Retd), BG Verghese and Satish Chandra, Secretary, National Security Council Secretariat (NSCS) as the other members. Chandra was also designated as Member- Secretary. The terms of reference of the Committee were:</p>

                      
                    
                        <p>1.	To review the events leading up to the Pakistani aggression in the Kargil District of Ladakh in Jammu & Kashmir; and</p>
                        <p>2.	To recommend such measures as are considered necessary to safeguard national security against such armed intrusions</p>

                      
                        <!-- <p>Since Kargil also included an element of information warfare, the Kargil Review Committee Report also evaluated ‘…the information and media component. This was followed by a Group of Ministers report, which addressed a number of issues that had been raised during the course of deliberations. The major ones in the Kargil Review Committee report included:</p>
                      
                    <p> Ministry of Defence, “Defence Force Levels, Manpower, Management and Policy”, Estimates
                    Committee, 19th Report, Tenth Lok Sabha, 1992-1993.</p>
                      
                    <p>“The Armed Forces epitomise the ideals of service, sacrifice, patriotism and India’s composite
                    culture. The recruitment to the Armed Forces is voluntary and every citizen of India,
                    irrespective of his caste, class, religion and community is eligible for recruitment provided he
                    meets laid down physical, and medical criteria. Recruitment is carried out according to [the]
                    recruitable male population (RMP) of each state which is 10 per cent of the male population.</p>

                      


                    <p>2.	To recommend such measures as are considered necessary to safeguard national security against such armed intrusions.</p>

                       -->

                    <p>Since Kargil also included an element of information warfare, the Kargil Review Committee Report also evaluated ‘…the information and media component. This was followed by a Group of Ministers report, which addressed a number of issues that had been raised during the course of deliberations. The major ones in the Kargil Review Committee report included:</p>
                      

                        <p>1.	Enhance capacity of media cells at army formations such as Udhampur and Srinagar.</p>
                      

                        <p>2.	Recommence the war correspondent’s course.</p>
                      

                        <p>3.	Incorporate modules on information operations and perception management in courses.</p>
                      

                        <p>4.	Establishment of dedicated radio and TV channels to entertain armed forces personnel. These can also counter false propaganda of the adversary.</p>
                      

                        <p>5.	The government must evolve procedures to keep the people informed on import- ant national security issues.</p>
                      


                        <p>6.	Need to come up with official history of the Kargil conflict and India’s nuclear weapons programme.’</p>

                      


                        <p>Subsequent to other recommendations of the KRC and the Group of Ministers, the decade saw significant moves towards jointness/integration of the three Services. The Integrated Headquarters (IHQ) was established at the Ministry of Defence (MoD) as part of the reorganisation of Army HQ for closer integration with the ministry. Also established were the Strategic Forces Command, Cyber Command, Special Forces Command, and the tri-Service Andaman and Nicobar Command. One of the recommendations of the Kargil Review Committee, and the subsequent Naresh Chandra Committee was the appointment of the Chief of Defence Staff (CDS), as a single point military advisor to the government. This was finally undertaken in 2018, with the appointment of Gen Bipin Rawat as the first CDS. Prior to Gen Rawat’s appointment, the Chiefs of Staff Committee (COSC) functioned as the apex military advisory position, and was helmed in rotation by the longest serving Service Chief. </p>

                      

                        <p>Militaries as a rule are not amenable to change in the status quo, so along with the moves to integrate there were inter-Services turf battles. It remains a fact that given India’s vast and adversarial land borders, the Indian Army remains the largest of the three Services. Also, despite a renewed maritime focus since the 1990s and particularly in the 2000s, events such as Kargil and China flexing its muscles on the border with India means that the army would continue to remain the largest and most important Service.</p>

                      
                        <p>On 13 December 2001, a couple of months after an attack on the J&K Assembly, gun-toting terrorists attacked the Indian Parliament in New Delhi. This brought India and Pak to the brink of war with the launch of Operation Parakram. This was the largest mobilisation of Indian forces, especially on land, since Operation Brasstacks in the 1980s. Operation Parakram continued for a period of 21 months but at the end both sides backed down.</p>
                    
                    </p>


                    <div class="war-2-map-2">
                        <img src="../assets/img/Tanks executing a manoeuvre Operation Parakram, 2001-17 1.jpg" width="100%" height="400px" class="img-fluid" alt="" loading="lazy">
                        <p style="color: #D90000;">Tanks executing a manoeuvre Operation Parakram, 2001<br>Source: Capt Suresh Sharma.</p>
                    </div>


                    <h2 class="heading-border">2010–2020: Looking to the Future</h2>
                    <p>Technology has a momentum of its own. This is especially pertinent to military technology as obsoleteness sets in rapidly. It is a dictum that the army is trained to fight the previous war and this becomes an area of urgent consideration in light of the rapidity with which warfare is becoming technology-driven. Pre-1991, a large segment of military technology manufactured indigenously and also used by the various Services was of Russian origin. While a significant push was given to indigenisation in this decade, the dependence of the Indian military on Russian hardware continues. Yet, in this decade we see an increased variation in the import of technology and military hardware, and a greater leaning away from Russia and towards the West.</p>
                          
                        <p>The army, in particular, moved from a defensive orientation to a more aggressive posturing. This was both necessitated by operating in the grey zone as well as directed by clear political direction. The 2016 surgical strikes are an example of this changed mindset. China continues to be the key adversary for India on the northern front, as evidenced by various instances of its trying to alter the status quo on the Line of Actual Control (LAC), the most recent of which took place in Galwan in 2020, at the height of the Covid-19 pandemic. China remains the key adversary for the Indian Army along the country’s northern borders.</p>
                          
                        <p>In terms of HDO, structurally, the move towards integration has received a fillip. With the appointment of the CDS in 2018, a new Department of Military Affairs was created in the MoD. Military thinking and doctrinal outlook today increasingly points towards integration, as stated in the 2017 Joint Doctrine for the Indian Armed Forces, prepared by HQ IDS.</p>

                    <p>This decade also saw the raising of 17 Corps, headquartered at Panagarh in West Bengal. This is a mountain strike corps built as a quick reaction force and a counter to China. On the side, the debate continues on the scaling up of the army in light of the increasing threat from China and downsizing caused by adoption of and improvement in technology as well as budgetary constraints. Following the brief introduction of the Indian Army’s history and evolution into a modern fighting force in the seven and a half decades since independence, we will now take a look at the organisational underpinnings and traditions that make this vast organisation a cohesive whole. A little known, but important, part of the army’s history—its contribution to the freedom struggle—is also elucidated.</p>


                    <!-- <div class="note">
                        <p>Notes</p>
                        <ul>
                            <li>Rajat Ganguly, ‘India’s Military: Evolution, Modernisation and Transformation’, India Quarterly, 71(3), pp. 187–205.</li>

                            <li>Ibid., pp. 187–205.</li>

                            <li>Ibid.</li>

                            <li>Mandip Singh, ‘Critical Assessment of China’s Vulnerabilities in Tibet’, IDSA Occasional Paper No 30, 2013, p. 12.</li>

                            <li>Johan Skog Jensen, ‘A Game of Chess and a Battle of Wits: India’s Forward Policy Decision in Late 1961’, Journal of Defence Studies, Vol. 6, No. 4, October 2012, pp. 55–70.</li>

                            <li>Farooq Bajwa, From Kutch to Tashkent: The India-Pakistan War of 1965, New Delhi: Pentagon Press, 2014, p. 91, as cited in PK Chakravorty and Gurmeet Kanwal, ‘Operation Gibraltar: An Uprising that Never Was’, Journal of Defence Studies, Vol. 9, No. 3, July–September 2015, pp. 33–52</li>

                            <li>PK Chakravorty and Gurmeet Kanwal, ‘Operation Gibraltar: An Uprising that Never Was’, Journal of Defence Studies, Vol. 9, No. 3, July–September 2015, p. 36.</li>

                            <li>Vivek Chadha, ‘An Assessment of Organisational Change in the Indian Army’, Journal of Defence Studies, Vol. 9, No. 4, October-December 2015, p. 25.</li>

                            <li>Chadha, Ibid., p. 26, citing Ministry of Defence’s Annual Report from 1964–65.</li>

                            <li>Ibid., p. 28.</li>

                            <li>Ibid.</li>

                            <li>Ibid., p. 29.</li>



                            <li>Steven I. Wilkinson, Army and Nation: The Military and Democracy Since Independence, Ranikhet: Permanent Black, 2015, p. 9.</li>

                            <li>Major General (Retd) Ashok Krishna, AVSM, ‘Counter Insurgency and Internal Security’, in Major General (Retd) Ian Cardozo, AVSM, SM (ed.), Indian Army: A Brief History, New Delhi: USI, 2007, p. 217.</li>

                            <li>Ibid., p. 217</li>

                            <li>Ibid.</li>

                            <li>Lt Gen (Retd) Vijay Oberoi, PVSM, AVSM, SM, VSM, ‘India’s Wars since Independence: A Concise History’, Journal of the United Service Institution of India, Vol. CL, No. 622, October-December 2020.</li>

                            <li>Vivek Chadha, KARGIL: Past Perfect, Future Uncertain?, New Delhi: IDSA and Knowledge World, 2019, p. x.</li>

                            <li>Lt Gen (Retd), VK Singh, PVSM, ‘Times of Trial’, in Maj Gen (Retd) Ian Cardozo, AVSM, SM (ed.), The Indian Army: A Brief History, New Delhi: USI, 2007, p. 189.</li>


                            <li>Vivek Chadha, KARGIL: Past Perfect, Future Uncertain?, New Delhi: IDSA and Knowledge World, 2019, p. 83.</li>


                        </ul>           

                    </div> -->
                   
                </div>
            </div>
                  

    
    </section>

    <?php get_footer(); ?>