<?php

$main = "indian-army";

$page = "ethos";


get_header();  ?>


<section class="regiments-and-corps-banner" style="background-image: url(../assets/img/regiments-and-corps-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">Ethos and Values</h1>
    </div>
</section>

<section class="regiments-and-corps-detail">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9  col-sm-12">
                <h2>Ethos and Values</h2>
                <p>The Indian Army represents a diverse nation and draws its ethos from the philosophy and
                    beliefs of the society that it serves and of which it is an integral part. India’s civilisational values have shaped the ethos and moral code of the Indian Army, which is an amalgam of diverse philosophies, traditions and beliefs drawn from the rich tapestry of histories, myths and cultures of various races that make up India’s heterogeneous whole.</p>


                <p>The beliefs and value systems of the Marathas, Sikhs, Dogras, Garhwalis, Tamils, Malayalees, Andhraites, Kashmiris, Kodavas, Assamese, Manipuris, Punjabis, Jats, Rajputs, Nagas, Bengalis, Mizos, Oriyas, Gorkhas, the residents of the Konkan coast, and many other clans, tribes and communities have all contributed to the ethos of the Indian Army to make it truly representative of the Indian nation. It is this fusion—of diverse cultures, philosophies and traditions—that continues to be the bedrock of the attitude and behaviour of the soldier that teaches him how to live and behave in peace and how to fight and die in war.</p>


                <p>The fate of a nation in war depends on how well its soldier’s fight. Indeed, the true quality of an army and the nature of its soldiers becomes evident only in war. It would, however, be foolish to wait for war to discover the proficiency and potential of the army. It is here that ethos plays a vital part in shaping the army before it goes to war. This includes an emphasis on sound leadership, high moral code, good training and high morale.</p>

                <p>How well soldiers fight depends much on how well they are trained, motivated and led. Leadership at every level plays a critical role in translating the ethos of the Indian Army into performance in peace and war. The selection of leaders is thus very important. Recruiting officers and soldiers using time tested methods that analyse their character, sense of duty, commitment, integrity and self-discipline is more likely to yield men of moral and physical courage, weeding out candidates that do not have the necessary characteristics that make a good soldier. Therefore, selection systems have to be built in such a way that they are able to identify persons who possess the requisite qualities—qualities that will make them think and act beyond self and for the good of larger causes and institutions, like the country and the people of India.</p>

                <p>How well soldiers fight depends much on how well they are trained, motivated and led. Leadership at every level plays a critical role in translating the ethos of the Indian Army into performance in peace and war. The selection of leaders is thus very important. Recruiting officers and soldiers using time tested methods that analyse their character, sense of duty, commitment, integrity and self-discipline is more likely to yield men of moral and physical courage, weeding out candidates that do not have the necessary characteristics that make a good soldier. Therefore, selection systems have to be built in such a way that they are able to identify persons who possess the requisite qualities—qualities that will make them think and act beyond self and for the good of larger causes and institutions, like the country and the people of India.</p>

                <p>How well soldiers fight depends much on how well they are trained, motivated and led. Leadership at every level plays a critical role in translating the ethos of the Indian Army into performance in peace and war. The selection of leaders is thus very important. Recruiting officers and soldiers using time tested methods that analyse their character, sense of duty, commitment, integrity and self-discipline is more likely to yield men of moral and physical courage, weeding out candidates that do not have the necessary characteristics that make a good soldier. Therefore, selection systems have to be built in such a way that they are able to identify persons who possess the requisite qualities—qualities that will make them think and act beyond self and for the good of larger causes and institutions, like the country and the people of India.</p>

                <p>How well soldiers fight depends much on how well they are trained, motivated and led. Leadership at every level plays a critical role in translating the ethos of the Indian Army into performance in peace and war. The selection of leaders is thus very important. Recruiting officers and soldiers using time tested methods that analyse their character, sense of duty, commitment, integrity and self-discipline is more likely to yield men of moral and physical courage, weeding out candidates that do not have the necessary characteristics that make a good soldier. Therefore, selection systems have to be built in such a way that they are able to identify persons who possess the requisite qualities—qualities that will make them think and act beyond self and for the good of larger causes and institutions, like the country and the people of India.</p>

                <p>Undoubtedly, the Indian soldier is amongst the best in the world. He is imbued with the quality of putting the country above all else. He follows his officers unquestioningly and undergoes great discomfort, in unbelievably difficult circumstances, without complaint, because he knows that his officer is there right in front facing maximum danger and setting the right example. </p>

                <p>The ethos of the armed forces is the lifeblood of its members and inspires them to carry out extraordinary acts of courage. A few examples of leadership and the outcomes that it generates would be useful to understand the important part that honour, courage, self-sacrifice and personal example play in translating these beliefs into action.</p>


                <h2 class="heading-border">Leading by Example</h2>
                <p>A classic example of leadership linked with the honour of the regiment is the battle of Dograi. In the 1965 Indo-Pak War, 3 JAT, led by Lieutenant Colonel (Lt Col) Desmond Hayde, captured Dograi across the Ichhogil Canal. It was a hard-fought battle in which many soldiers were killed and wounded. However, as the brigade was not able to provide reinforcements, 3 JAT was ordered to withdraw from the area it had captured. The battalion followed the orders, but it was unhappy and considered this to be a slur on its honour. Therefore, when Dograi had to be recaptured, 3 JAT volunteered to be at the forefront. For them, it was a matter of honour!</p>

                <p>However, by then, the Pakistanis had reinforced Dograi with armour and infantry, making its capture even more difficult. On the eve of the battle, the commanding officer (CO) addressed the men in Haryanvi. He made it clear that the battle would be tough and that many would be killed and wounded. Then he said: ‘I will be leading you into battle and if I die, I want you to carry me to Dograi because I want to be there with you—dead or alive!’ He further added: ‘Where will we be tomorrow morning?’, and the battalion roared, ‘In Dograi!’</p>

                <p>In the epic battle that followed, many soldiers were killed and many more wounded, but Dograi was recaptured by the invincible 3 JAT. What was it that made 3 JAT so invincible? The answer is regimental spirit and morale.</p>

                <p>In the epic battle that followed, many soldiers were killed and many more wounded, but Dograi was recaptured by the invincible 3 JAT. What was it that made 3 JAT so invincible? The answer is regimental spirit and morale.</p>

                <p>Self-sacrifice is another characteristic of leadership that inspires the soldier to go beyond the call of duty. During the 1971 war, Indian Navy carried out two attacks on Karachi harbour, sinking a destroyer and a mine-sweeper and a few others, thus restricting the Pakistan Navy’s fleet. However, their submarine arm was far superior to ours and it was successful in sinking INS Khukri. Capt Mahendra Nath Mulla, the captain of Khukri, went down with the ship. He had the choice of saving his own life, but it was not part of his character to do so when his men were trapped on the sinking ship. In fact, he gave his own life jacket to a sailor who was without one. As a leader, he practised what he believed. Personal acts of cold courage like this are rare to come by, and when they do, they shake the world by their heroic content and epitomise the moral code which is so much part of the ethos of the armed forces. The way he lived and died has become part of the folklore of the Indian Navy and a guiding light not only to the officers and sailors of the navy but also to all personnel of India’s armed forces.</p>

                <p>Another example of self-sacrifice is what happened in a raid across the border by the Indian Air Force during the 1965 Indo-Pakistan War. Squadron Leader ‘Tubby’ Devayya set a strong example of cool courage and diehard determination in the face of impossible odds. On an attack on the Pakistani airfield at Sargodha, he was faced with the option of returning to his air base in India or engaging in combat with a supersonic Pakistani Starfighter, which was far superior in weapons and avionics to his subsonic Mystere. His orders were to return to base because his fuel was just enough to hit Sargodha and return. However, being the last aircraft of his wave, it was also his duty to protect the other aircraft of the team. So, he turned around and took on the Pakistani Starfighter in an unequal combat setting. Though the Pakistani pilot was able to damage his aircraft, Devayya continued to take on the Starfighter and managed to destroy it but was killed in the process. He lies today buried in a corner of a farmer’s field in Pakistan. His action is an outstanding example of self-sacrifice of the highest order in keeping with the moral code he was taught as a young pilot officer in the Indian Air Force.</p>

                <p>There are many such stories that exemplify the spirit of the armed forces. This account would, however, be incomplete if one does not look at the conduct of Lieutenant (Lt) Manoj Pandey and Capt Vikram Batra, whose exemplary conduct during the 1999 Kargil War typifies the code of conduct of the armed services officers groomed at the defence academies, the cradles of leadership.</p>

                <p>Lt Manoj Pandey constantly and persistently volunteered for the most difficult missions. In his diary, he had noted before the commencement of the war, ‘If death strikes before I prove my blood—I promise I will kill death.’ Philosophical words from one so young! He continued to lead mission after mission on the snow-covered slopes of Kargil mountains. When he was mortally wounded on his last mission, he said, ‘I regret that I have only one life to give up for my country.’</p>

                <p>Capt Vikram Batra became an icon well before the termination of the Kargil War. Due to his many skirmishes with the enemy, he was nicknamed ‘Sher Shah’ by the Pakistanis. His quote, ‘Dil mange more’ (the heart wants more), typifies the spirit of the Indian Army. Prior to his last mission, he said, ‘Either I will come back after hoisting the tricolour or I will come back wrapped in it but I will be back for sure!’ Prophetic words, because that is what happened. After a series of missions in which he displayed uncommon qualities of leadership, sacrifice and love for his country and men, he died saving the life of another soldier. Both the officers were awarded the Param Vir Chakra, India’s highest award for gallantry in war.</p>




                <h2 class="heading-border">Embracing all Faiths</h2>
                <p>The ethos of the Indian Army is against divisiveness caused by religion. It is spirituality that shapes the attitude and behaviour of the officers and soldiers towards God. With religion being considered as a personal matter, the focus of the army is on the integration of men of all faiths. The army, thus, emphasises unity in diversity and the ability to work closely despite differences.</p>


                <p>In single-class units, like the Sikhs, the Gorkhas, the Garhwalis and the Kumaonis, religion continues to be a motivating factor in both war and peace. In such units, religious functions are attended by officers and men of other faiths as part of their military duties. This helps in cementing regimental bonds and the officer–men relationship. In mixed-class units, mandir, masjid, gurdwara and girja ghar are often seen separately but also together under one roof, with men of different faiths attending each other’s religious functions. In the Indian Army, all religions are respected and there is no difference whatsoever in consideration of creed, cast and community. In fact, in all the wars that India has fought before and after independence, soldiers of different faiths have fought shoulder to shoulder with outstanding results.</p>

                <p>It needs to be remembered that in the Indo-Pak War of 1971, though the majority of the generals were of the Hindu faith, the Indian Army had a Parsi Chief, a Sikh Army Commander, a Jew as the Chief of Staff of Eastern Command, an Anglo- Indian as the Director of Military Operations, a Christian as the commander of a strike corps on the western front, and three Christian officers commanding infantry divisions spearheading offensive operations on both fronts. It is this unity in diversity that makes the Indian Army one of the finest in the world.</p>

                <p>As mentioned earlier, the ethos of the Indian Armed Forces draws its inspiration from the beliefs of its people and therefore, the government elected by the people of India needs to reflect the beliefs and aspirations of the people of India in its policies, programmes and strategies. India is a spiritual country and people of various faiths believe that belief in God and a high moral conduct are essential for progress of the country in peace and war. This belief permeates into the conscious mind of every person of the armed forces, from the Chief to the junior-most soldier, sailor and airman. This consciousness translates into a habit because habits transform attitude, which affects behaviour, and this in turn affects conduct. This ‘conduct’ motivates personnel of the armed forces to put country first, courage beyond fear and death above dishonour.</p>

                <p>This now brings us to the important aspect of training, especially operational training, and the institutes where this training is imparted.</p>



                <h2 class="heading-border">Training in the Indian Army</h2>
                <p>Currently, India boasts not only of the largest standing volunteer army in the world but also one of the most experienced ones. The Indian Army has vast experience ranging from wars to counter-insurgency operations, as well as fighting in and holding terrains as varied as the high mountains to dry deserts and riverine deltas. One of the most well-trained armies, it is in constant readiness to take upon itself any challenge cohesively. Indeed, in the seven decades and more since independence, the readiness of the armed forces has been tested consistently. Since 1947, India has gone to war five times to defend her territorial and national integrity, with practical experience showing that strength in the military dimension is essential to prevent outside forces from taking advantage of a perceived weakness.</p>

                <p>The purpose of training is to prepare for the eventuality of war. Training is, therefore, an important factor to ensure that a country is militarily strong. It focuses not only on the human resource but also on how the capacity of the soldier can be multiplied by other factors. In this ‘information age’, technology has gained primacy, but it is the ‘man’ who continues to be the most critical element in war. Consequently, leadership continues to be an important aspect of soldiering. Quantum jumps in technology have revolutionised warfare and therefore, technological training is being increasingly prioritised. It has also become apparent that we are functioning in a world of interdependency and that military capacity increases exponentially when all branches of the armed forces, the paramilitary forces, civil defence forces and the essential services work as a team. Moreover, as with other militaries the world over, the Indian Armed Forces too are leaning towards jointness and integration, with the move towards theatre commands being one such step. In the present context, training is necessary so that success is achieved in the shortest possible time, with minimum loss of life.</p>

                <p>In India, training has been an essential part of the military way of life since time immemorial. One of the oldest treatises on statecraft, Kautilya’s Arthashastra, mentions the importance of training. Guerrilla warfare was also first conceptualised by Chhatrapati Shivaji. He trained his army in this kind of warfare, to fight the Mughals, and was highly successful. Further, the Marathas won battles against the Mughals, the Portuguese and the other Indian princes because they were mobile, hardy and united. The arrival of the Europeans in India in the mid-eighteenth century led to a new approach towards war and the profession of arms. The British and the French, in particular, supplemented their own very small forces by training Indians in the new methods. This came in rather handy in the two World Wars.</p>

                <p>In 1914, at the start of World War I, the Indian Army was armed with outdated rifles, aircraft were unknown, and it had virtually no artillery. However, it was a highly professional and efficient army. Though mechanisation came to India late, it was one more change in the life of the rank and file. The Indian soldier—adaptable, intelligent and well trained—managed to cope and was soon as good as his British counterpart. By 1945, when World War II ended, the men were trained in jungle and desert warfare, along with trench warfare. They knew how to drive tanks and armoured carriers and operate artillery of all kinds. They were also trained in amphibious operations from ships and in parachute jumps. This Indian Army, once it was properly trained and equipped, showed that its troops were equal to any in the world.</p>

                <p>India’s independence in 1947 came along with the partition of the former colony into two antagonistic states. Partition destroyed part of the ethos of the old Indian Army, that is, the concept of brotherhood between soldiers of all faiths, communities and creeds. Troops who had fought shoulder to shoulder on battlefields all over the world were now to be separated and would soon to be fighting against each other. However, the Indian Army rebuilt its ethos and came together to fight wars with both Pakistan (multiple times) and China.</p>

                <p>In peacetime, the most challenging task for the Indian Army is ‘training’. This training for war, which is the army’s primary role, tends to get disrupted due to other national needs, like counterinsurgency and aid to civil authorities during flood, drought, etc. However, the army takes this as a form of ‘hands-on’ training and as part of its contribution to national development. Every year, the Indian Army inducts about 70,000 soldiers and 1,000 officers to maintain operational capacity against wastages. Their training is intensive and focuses on preparing them for their roles. Training in the Indian Army is thus an ongoing process for both officers and men—an activity which keeps their skills honed at all times.</p>

                <p>Among the many reforms undertaken in the Indian Army in the recent past, the most important reform relating to training in the army has been the setting up of the Army Training Command (ARTRAC). This command is a high-powered organisation that formulates training policies; plans, coordinates and monitors all aspects of training; and reviews, evolves and disseminates concepts and doctrines for the entire army, thereby maximising operational readiness. The ARTRAC, which came into being on 1 October 1991 at Mhow in Madhya Pradesh, was subsequently shifted to Shimla in March 2002. This command is headed by an Army Commander of the rank of Lieutenant General. Apart from formulation and dissemination of concepts and doctrines of warfare in the fields of strategy, tactics, logistics, training and human resource development, ARTRAC also acts as the nodal agency for all institutional training in the army.</p>

                <p>In essence, the training philosophy of the army, as laid down by ARTRAC, is to develop forces that will win in combat; and that no officer or soldier should ever lose his life or limb in combat because he was inadequately trained. It is only intelligent, imaginative and innovative training that produces good soldiers, officers and cohesive units and formations. The training philosophy focuses on building three major components, namely, force structure, material and doctrine, to create a combat-ready force.</p>



                <h2 class="heading-border">Operational Training</h2>

                <p>The Indian Army requires its soldiers, officers and units to be ready to deploy, fight and win in combat at any intensity level, anywhere and at any time. Operational training prepares the army to meet such requirements. It consists of all training in field formations which is planned, coordinated and conducted under formation/unit arrangements. Individual training, sub-unit and unit training, collective training, field firing, all form part of operational training. The training standards achieved during operational training determine the fighting efficiency of the army. The Indian Army’s training cycle is normally on a yearly basis and the training year is from July to June.</p>

                <p>The Army Headquarters (HQ) issues the Army Training Directive, which lays down the training policy. It generally contains the key result areas which are to be achieved in training. The directive is based on training reports of the previous year, annual inspection reports, personal observations and reports from various sources. Based on this, subordinate formation commanders issue their own training directives and instructions. Since conditions vary in each command and formation, Army HQ directs and coordinates training only in very broad terms. The responsibility of implementing training policies devolves on the command HQ. At lower levels, training becomes more and more specific and focuses on the particular needs of formations and units concerned.</p>

                <p>The Indian Army is the fourth largest army in the world with approximately one million personnel in uniform. In the post-independence era, in addition to Kargil, the army has fought four major wars; it continues to hold live and sensitive borders; and it has been engaged in sustained counterinsurgency in certain parts of the country. High standards of effectiveness demand rigorous and imaginative training of our officers and men from the time they join the army as cadets or recruits till they leave the service. Technology has also changed the pace and concept of warfare, which is a major challenge that the army has had to face. This includes taking on board new technological concepts and balancing it against its relevance in our battlefield scenario.</p>



                <h2 class="heading-border">Training Tailored to Specific Needs</h2>
                <p>The subcontinent encompasses a wide variety of terrain, ranging from the thick forests of the North-East to the flat obstacle-ridden plains of the Punjab and from the arid mountains and high-altitude deserts and glaciers of snow and ice of the Ladakh Sector to the sandy deserts of Rajasthan. Training, therefore, has to encompass the environment, technology and the threat perception. This includes the threat of terrorism from across the border that has thrown up a new dimension for the army to tackle.</p>

                <p>As before, training within units and formations continues to be achieved through individual and collective training. Outside of units and formations, training extends to ‘institutional’ training, which includes training imparted on induction of an individual into the army, technical training and tactical training. This is imparted initially at the training centres, and subsequently at schools of instruction, respectively. Over the past few years, officers of the army have been encouraged to go to selected universities and academic institutions in India and abroad, to take advantage of opportunities to acquire the latest advances in the art and science of war..</p>



                <h2 class="heading-border">Training Institutions</h2>
                <p><i><b>National Defence Academy (NDA)</b></i></p>
                <p>The NDA is the premier joint services training institution of India. It is located at Khadakwasla, near Pune. The idea of a joint services academy in India was conceived after World War II and before independence, that is, in 1945. Accordingly, it was designed for the education and joint basic training of future officers of the three services. A sum of 1,000 pounds was given by the Government of Sudan for the construction of a suitable war memorial in appreciation of the services rendered by the Indian Army in liberation of their country. This was used towards the construction of the NDA, which, in fact, is a living war memorial. In 1954, the Joint Services Wing (JSW) shifted from Clement Town to Khadakwasla and the NDA started its first term in January 1955. The syllabus of the NDA meets the required national education format of 10+2+3 and on passing out, the cadets are granted BA or BSc degrees from Jawaharlal Nehru University, New Delhi.</p>

                <p><i><b>Indian Military Academy (IMA)</b></i></p>
                <p>The founding of the IMA is part of India’s struggle for freedom and its birth was the culmination of a long-drawn-out battle fought in forums such as the Central Legislative Assembly and the Round Table Conference, by stalwarts like Sir Sivaswamy Aiyar, Pandit Motilal Nehru, Mohammad Ali Jinnah, and Lala Lajpat Rai, among others. Leaders of all political parties were united and unanimous in their demand for the establishment of a military academy in India. The political significance of this battle is that the army, at last, was open to commissioning Indians as officers in the Indian Army.</p>

                <p>The IMA came into being on 1 October 1932. The first course of 40 gentlemen cadets (GCs), known as the ‘Pioneers’, had on its rolls Sam Manekshaw, Smith Dun and Mohammad Musa, later to be Chiefs of the armies of their respective countries, that is, India, Burma and Pakistan. Field Marshal Sir Philip Chetwode, Bart, GCB, KCMG, GCSI, DSO formally inaugurated the Academy on 10 December 1932. Chetwode delivered the inaugural address in the hall, which has been named after him, and the present IMA credo has been adopted from this address. Between December 1934 and May 1941, 16 regular courses passed out of IMA and 524 GCs were commissioned. Consequent to the German invasion of France, there was a surge in the intake, with 3,387 Indian and British GCs being commissioned between August 1941 and January 1946.</p>

                <p>Independence in 1947 brought about a change in the command of the Academy. The first Indian Commandant was Brigadier Thakur Mahadeo Singh, DSO. The bifurcation of the Indian Army on independence led to redefining training needs. The new scheme for training officers brought about the establishment of the Armed Forces Academy in 1949, with the IMA as its nucleus. As part of the IMA, JSW was opened in 1950, which later became the NDA. In 1954, after the NDA shifted to its present location at Khadakwasla, Pune, the IMA reverted to its original identity and role.</p>

                <p>In the aftermath of the Chinese aggression in 1962, the Academy expanded. From November 1962 to November 1964, a total of 4,051 GCs were granted emergency commission. During this period, peak strength of 3,090 GCs was reached in July 1963. In 1977, Army Cadet College, the progeny of Kitchner College, Nowgong, shifted to the Academy campus; it now functions as a wing of the IMA. The IMA was presented Colours in 1934 in recognition of its services to the British Empire. After independence, the first Indian Colours were presented to the IMA in 1962 by the then President, Dr S. Radhakrishnan. The current Colours were presented to the Academy in 1976 by the then President, Shri Fakhruddin Ali Ahmed.</p>

                <p>The IMA has grown from strength to strength over the years. It celebrated its silver jubilee, golden jubilee and diamond jubilees in 1957, 1982 and 1992, respectively. The spirit of the Academy is symbolised by its crest and colours. The crest is composed of crossed swords with a flaming torch symbolising knowledge, all superimposed in the middle by the Dharama Chakra of Ashoka, against the background of steel grey and blood red. The scroll bears the inscription ‘Veerta aur Vivek’, meaning ‘Valour and Wisdom’. Regarding the colours, the steel grey denotes strength and resilience, and blood red signifies the ultimate sacrifice in devotion to duty.</p>

                <p>The IMA has five main entry streams: the NDA cadets from Khadakwasla, Pune; the Army Cadet College (for servicemen only); the Graduate Direct Entry Scheme from colleges; the Technical Graduates Entry Scheme from technical institutions/colleges; and the University Graduates Entry Scheme. The duration of training is one year, except for the Direct Entry Scheme, for which it is one- and-a-half years.</p>

                <p>Leadership and character development is the prime concern of the Academy. The essence of duty and honour is constantly impressed upon a GC to make him a responsive leader and dignified citizen of the nation.</p>

                <p>The art of war, miniaturised to the level of a platoon, is the essence of service training at the Academy. Whilst gaining knowledge of basic tactical training at the platoon level, a GC learns the use of ground and fire and movement tactics. Supporting arms and services are co-opted to develop the concept of an integrated all-arms battle philosophy. Training in the various nuances of low-intensity conflict operations forms an important segment of the syllabus. Tactical exercises develop leadership and independent thinking, while laying greater stress on endurance and team building.</p>

                <p>The Academy encourages and provides opportunity for adventure activity because it helps GCs to understand the elements of risk, chance, fortune and luck. Each GC is encouraged to pursue at least one adventure sport. The range of activity spans river rafting, para jumping, rock climbing, trekking, mountaineering and desert safaris.</p>

                <p>As all GCs are graduates on joining the Academy, academic training aims at their overall development. The main objectives are to provide a broad education base essential for future development; enhance their powers of analysis, reasoning and expression, both verbal and written, in English and Hindi; and provide scientific orientation to keep abreast of technological advancements in warfare.</p>

                <p>Since 1951, the Academy has trained GCs from friendly countries such as Angola, Afghanistan, Bhutan, Botswana, Burma (Myanmar), Ghana, Iraq, Jamaica, Malaysia, the Maldives, Mauritius, Nepal, Nigeria, the Philippines, Seychelles, Singapore, Sri Lanka, South Africa, Tanzania, Tonga, Uganda, Yemen and Zambia.</p>

                <p>For nine decades, the IMA has produced officers who have served the nation with pride, élan and dedication. To meet new challenges, it has been constantly improving the quality of training, administration and infrastructure. ‘Valour and Wisdom’, the motto of the IMA, is manifested in the deeds of the alumni of the Academy. That the Army has given a good account of itself, both in peace and war, is a tribute to the Indian Army’s gallant officers who have always led from the front. A large share of the credit for the leadership of the army can be given to the IMA.</p>

                <p><i><b>Officers Training Academy (OTA)</b></i></p>
                <p>Originally raised as an Officers’ Training School in 1963, at Madras (Chennai), the institute was redesignated as the Officers Training Academy (OTA) in 1988. OTA Gaya was raised as a new institute in 2011. Initially, the main task of the OTA was to train GCs for Short Service Commission. The OTA has made history by admitting and training lady cadets (LCs) since 1992. The OTA trains 500 cadets every year, of which 50 are LCs. The motto of the OTA is ‘Serve with Honour’, which is signified by the crossed swords that depicts the profession of arms and the Ashoka Chakra that represents honour.</p>

                <p><i><b>Army War College (AWC)</b></i></p>
                <p>The College of Combat, located at Mhow, was renamed as the AWC in 2003. It is one of the premier institutes of the army and the country. It imparts training in the art of leadership, strategy and tactics to army officers, ranging from Captains to Generals. The AWC also imparts integrated all-arms training to officers, promotes inter-service jointmanship and encourages doctrinal research on military subjects. The College trains over 1,200 officers every year. About 100 officers from friendly countries are also trained every year at the middle and joint levels of command. It is the nodal instructional agency for all-arms training at the tactical, operational and higher levels. It provides commanders with knowledge on the employment of all arms and services, so as to facilitate the ability to fuse them together into a single battle-winning machine. It originates tactical doctrines and carries out continuous study to keep abreast of tactical and technological developments, both at home and abroad. It also conducts research on tactics, logistics and contemporary studies.</p>



                <p><i><b>Armoured Corps Centre and School</b></i></p>
                <p>The outbreak of World War II and the consequent mechanisation of the Indian Cavalry necessitated the establishment of a training institute for officers, junior commissioned officers (JCOs) and men of the armoured corps. The Fighting Vehicles School was consequently established at Ahmednagar. The School of Armoured Warfare, which is part of the Armoured Corps Centre and School, trains personnel from all combat and supporting arms so that they have the capability to assume scales of responsibility from troop to regimental levels; it trains all ranks in the tactical and technological aspects of armoured warfare.</p>


                <p><i><b>School of Artillery</b></i></p>
                <p>The School of Artillery for the British Indian Army was established at Quetta in 1919, and later shifted to Kakul in 1923, both now in Pakistan. On 1 January 1941, the School of Artillery moved into its permanent home at Deolali, near Nasik in Maharashtra. The school is the seat of learning of the various disciplines of artillery warfare.</p>

                <p><i><b>Air Defence and Guided Missile School</b></i></p>
                <p>The Indian component of the Anti-Aircraft School which was at Karachi moved to Deolali in 1947, where it was merged with the School of Artillery as the Anti-Aircraft Wing. It was later designated as the Air Defence Wing. The Air Defence Wing moved to Gopalpur, Orissa, in October 1989, to become the nucleus for the raising of the Air Defence and Guided Missile School. It became fully functional on 1 November 1989. The School imparts technical and tactical training in all aspects of air defence gunnery to officers, JCOs and non-commissioned officers (NCOs); evaluates tactical and training doctrines related to army air defence; conducts trials on air defence equipment; and produces suitable notes and pamphlets concerning the handling of army air defence weapons. It also acts as a centre of excellence on all matters concerning air defence artillery.</p>


                <p><i><b>College of Military Engineering</b></i></p>
                <p>The School of Artillery for the British Indian Army was established at Quetta in 1919, and later shifted to Kakul in 1923, both now in Pakistan. On 1 January 1941, the School of Artillery moved into its permanent home at Deolali, near Nasik in Maharashtra. The school is the seat of learning of the various disciplines of artillery warfare.</p>


                <h2 class="heading-border">Inviting of Officers to JCOs Mess and JCOs to Officers’ Mess</h2>
                <p>Prior to 1934, officers of the Corps of Engineers attended courses in military engineering at the School of Military Engineering, Chatham, United Kingdom (UK), and attended a mechanical science course at Cambridge University, UK. From 1934 to 1939, officers commissioned from the IMA, Dehradun, were attached to the Bengal Sappers and Miners and attended a civil engineering course at Thomson College, Roorkee. As the requirement of engineer officers began to increase during the war, Officer Cadet Training Units were established at the three engineer centres at Bangalore, Roorkee and Kirkee, where cadets were given military as well as engineering training for service in engineer units. The School of Military Engineering was established at Roorkee in September 1943.</p>

                <p>After World War II, it was evident that centralised training of officers and other personnel of the corps was essential. Therefore, the School of Military Engineering at Dapodi, Pune, was established. Subsequently, in view of the increased responsibilities and training facilities provided, the name of the school was changed to the College of Military Engineering in November 1951. This was also in keeping with the higher status of the degree engineering courses run by the school and their recognition by the Institution of Engineers (India). The College imparts technical and tactical training on all engineering subjects; carries out trials, analysis of engineer data, testing of equipment and stores relating to engineering matters, research in various engineer projects; and acts as a centre of excellence on improvised explosive devices. In addition, it has a faculty of combat, civil, electric and mechanical engineering; a faculty of nuclear, biological and chemical (NBC) protection; and runs courses on NBC warfare.</p>



                <p><i><b>Military College of Telecommunication Engineering (MCTE)</b></i></p>
                <p>The MCTE at Mhow is the premier training institution of the Corps of Signals. The erstwhile Indian Signal Corps School, established in 1946, was later named as the School of Signals in 1948. In keeping with the advanced technical training being imparted at this institution, it was redesignated as the MCTE in 1967. The MCTE trains all ranks in the tactical and technical aspects of combat communications, electronic warfare, automatic data processing systems, electromagnetic interference and electromagnetic compatibility.</p>


                <p><i><b>Infantry School</b></i></p>
                <p>The Infantry School is the largest and oldest military institution in India. Originating as the School of Musketry at Changla Gali in 1888, it has changed names and locations several times over the years. Some of these are listed below:</p>

                <div class="note">
                    <ul>
                        <li>School of Musketry in Satara and Belgaum;</li>
                        <li>Small Arms School in Pachmari and Ahmednagar;</li>
                        <li>Indian NCO Training School, Jhansi;</li>
                        <li>Indian Platoon Commanders School, Faizabad; and</li>
                        <li>Battle School for Tactics and Administration, Dehradun.</li>
                    </ul>
                </div>
                <p>Since 1948, the Infantry School has been located at Mhow. The College of Combat was carved out of it in February 1971. The Army Marksmanship Unit, which has earned great laurels in India and Asia, was raised in December 1993. The Infantry School conducts various courses of instruction; develops new tactical doc- trines, battle techniques and battle drills pertaining to infantry; studies and evaluates the latest trends in the development of infantry tactics, weapons, ammunition and equipment; prepares training manuals pertaining to infantry; carries out trials on weapons, equipment and ammunition; and trains the army and national shooting teams. The School has been split into two locations. The HQ, the Young Officer’s Wing, the Support Weapons Wing, the Platoon Weapons Wing, the Faculty of Study and Trials, along with the Army Marksmanship Unit, are located at Mhow. The Junior Leaders Wing, comprising the Commando and Platoon Commanders Wings, is located at Belgaum.</p>


                <p><i><b>High Altitude Warfare School</b></i></p>
                <p>The High Altitude Warfare School was initially set up in Gulmarg as a Formation Ski School. The training imparted consisted mainly of skiing techniques, mountain lore and patrolling on skis. On 8 April 1962, the School was designated as a Category ‘A’ establishment and renamed as the High Altitude Warfare School. It has the distinction of being the only training institution in the country which has participated in active military operations during war. It is also the nodal centre for winter sports in the country.</p>

                <p>Qualified mountaineering experts train the officers and soldiers of the Indian Army in high-altitude warfare. The students are trained to traverse crevasses, scale steep ice walls, handle snow craft to rescue companions and handle specialised weapons. The school has trained a large number of mountaineers who have scaled various peaks in the high Himalayas. Training instils confidence and stamina, besides defrosting the fear of the unknown. The men are taught to integrate with the environment so that they can guard the Himalayan frontiers effectively.</p>

                <p><i><b>Counter Insurgency and Jungle Warfare School (CIJWS)</b></i></p>
                <p>The CIJWS was established in 1967 as a Jungle Training School at Mynkre, near Jowai in Meghalaya. This was later designated as the Eastern Command Counter Insurgency Training School in 1968. Two years later, the School became a Category ‘A’ establishment. It was relocated at Vairengte, in Mizoram, on 1 May 1970. Over the years, progressive upgradation in all facilities has made CIJWS the centre of excellence in low-intensity combat. The School, according to its motto, teaches students to ‘fight the guerrilla like a guerrilla’. The CIJWS functions as the Indian Army nodal instructional facility and a centre of excellence for training in counterinsurgency and related aspects of jungle warfare. The CIJWS also participated in the 1971 Indo-Pak War and takes part in counterinsurgency operations and psychological warfare when called upon to do so.</p>


                <p><i><b>Other Training Institutions</b></i></p>
                <p>There are, in addition, a number of other training institutions. These are the Army Service Corps Centre and College, Bangalore; the College of Material Management, Jabalpur; the Military College of Mechanical Engineering (MCEME), Secunderabad; the Military Intelligence Training School, Pune; the Army Medical Corps Centre and School, Lucknow; the Remount and Veterinary Corps Centre and School, Meerut; the Corps of Military Police School, Bangalore; the Institute of Military Law, Delhi the Army Education Corps Training College and Centre, Pachmari; the Army School of Physical Training, Pune; and the Army Airborne Training School, Agra.</p>

                <p>Besides these training establishments, there are joint service training institutions, namely, the Defence Services Staff College, the College of Defence Management, and the National Defence College, that have a considerable impact on the army.</p>


                <p><i><b>Defence Services Staff College (DSSC)</b></i></p>
                <p>The DSSC is one of the most prestigious military institutions in India. It was set up in 1905 at Deolali as the Army Staff College. In 1907, it shifted to Quetta. On Partition, the Indian element moved to Wellington in south India. It has since been imparting training to middle-level officers of the three Services, army, navy and air force. A few officers from the civil services, paramilitary forces and friendly countries also attend the DSSC. The focus of the course is to train officers of the three services in operational and staff functions in peace and war in their own service, inter-service and joint service environment, and generally related education to enable them to perform effectively in staff appointments tenable by Majors to Lieutenant Colonels and their equivalents in the Indian Navy and the Indian Air Force.</p>

                <p>The College enjoys international recognition as a premier military training institution. A large number of developed and developing countries seek vacancies on the course for their defence officers every year.</p>

                <p><i><b>College of Defence Management (CDM)</b></i></p>
                <p>The CDM was set up in Secunderabad in 1970 as the Institute of Defence Management. It was renamed as such in 1980. The College is the national centre for defence management. In addition to the instructional courses that are run, the CDM also carries out research in the field of defence management and provides consultancy services to the three services to find optimal and practical solutions to organisational problems.</p>

                <p><i><b>National Defence College</b></i></p>
                <p>Located in New Delhi, the NDC was established in 1960 on the lines of the Imperial Defence College of the United Kingdom (now the Royal College of Defence Studies). The College is India’s apex institution of learning for the study and practice of National Security and Strategy. The NDC is attended by selected officers of the rank of Brigadier in the Indian Army and its equivalents in the Indian Navy and the Indian Air Force. The NDC course is highly competitive and is also attended by civilian officials of the Government of India as well as military officers from friendly foreign nations. The participants of the NDC course are selected and trained for positions of higher leadership and responsibilities.</p>





                <h2 class="heading-border">Conclusion</h2>
                <p>Most societies and militaries have solemn ceremonies to respect their dead servicemen. For example, the United States has its ceremony of flag draped coffins and salutes from the President. Reverence for soldiers’ bodies is a relatively new historical development. During the two World Wars, those who died during the war were either cremated or buried in nearby locations, and there are war memorials to commemorate the same. The Maharaja of Bikaner, who was the only member of the Imperial War Cabinet during the First World War, insisted that the Hindus be cremated and Muslims be buried. During Op Pawan in Sri Lanka, the bodies of soldiers were cremated or buried in situ. It was during the Kargil War that instructions were issued to transport the remains of martyrs by service aircraft to an airfield nearest to their hometown for last rites.</p>





                <h2 class="heading-border">Salutation</h2>
                <p>When members of the Indian Armed Forces greet each other, they use the term Jai Hind. This was adopted by the Indian Army as the new form of greeting each other, civilian officials and those of the other two Services. The soldiers continue to use their regimental salutation while saluting officers.</p>






                <h2 class="heading-border">Regimental Customs</h2>
                <p>Military customs in the Indian Army have developed primarily along regimental lines, manifesting in long-established regimental colours, insignias, crests, mottos, war cries and distinctive features of the uniform. Some elements of regimental customs, for example, replacing the crown with the National Emblem, are in keeping with the republican character of India. Over time, some customs have diluted, some have disappeared while new customs have taken root. The Indian Navy and the IAF have their own traditions, for example, many naval customs are centered around the hoisting of the flag on the ship’s deck.</p>

                <p>Since independence, recruitment in the Army has been broad-based with comprising of soldiers from different parts of the country professing different faiths. ‘A unit could have a Muslim company, [with] all other companies being Hindu with a sprinkling of Christians and Buddhists and more Sikhs. Such a unit is bound to have “Sarv Dharam Sthal” or a place of worship for each faith, but all under one roof with display of flags of all four faiths.’</p>



                <h2 class="heading-border">Conclusion</h2>
                <p>Customs and traditions in the Indian Army are not always established by regulations. For the most part, they are unwritten practices that are obeyed just the same. These are not timeless or unchanging: it is possible to change certain aspects of traditions and over time some customs have been added while others have been modified or omitted based on experiences and consultations amongst stake holders. Often, the tendency to change or introduce new customs based on the whims of the colonel of the regiments or colonel commandants or Service Chiefs must be avoided. Customs and traditions are the building blocks for fostering esprit de corps and should not be influenced by the fashion of the time.</p>

                <p>The Indian Army training philosophy is designed to:</p>

                <div class="note">
                    <ul>
                        <li>The Indian Army training philosophy is designed to:</li>
                        <li>Maintain a strong dissuasive defence, while retaining strategic offensive capability—deterrence being the key.</li>
                        <li>Ensure defence capability against NBC threats and support the acquisition of a strategic deterrence.</li>
                        <li>Continue to have the capability to combat insurgency and/or proxy war.</li>
                        <li>Ensure out-of-area capability to support friendly littoral states.</li>
                        <li>During peace, the thrust is to enhance military education and to develop bold and competent leaders capable of ‘getting things done’.</li>
                    </ul>
                </div>

                <p>Training is basically a means to ensure that the performance of the soldier in combat is enhanced. The Indian Army faces numerous challenges in its endeavours to conduct quality training. The transitions in society, the changes in technology, coupled with the problems of technological assimilation and the rapid obsolescence of weapons and equipment, shrinking defence budgets and fiscal constraints, are only some of the issues. The variety of operational roles and commitments, the need to conserve expensive equipment, along with the vastly different approaches to train various components of the arms and services demand a new orientation and outlook. The Indian Army has opened windows to the future, to maintain its cutting edge, to sustain its proficiency and to conduct performance-oriented training. Ever in the service of the nation, the Indian Army continues to strive for professional excellence in the controlled application of force.</p>
                
            </div>
        </div>
    </div>

</section>

<?php get_footer();  ?>