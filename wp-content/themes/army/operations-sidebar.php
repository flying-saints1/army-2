<div class="col-md-3">
            <div class="accordion">
                    <h6 class="accordion-toggle">Operation Polo 1948</h6>
                        <div class="accordion-content" style="display: none;">
                            <ul>
                            <a href="#"><li class="active">Annexation of Hyderabad</li></a>
                                <a href="#eastern-sector"><li>Air Operations </li></a>
                                <a href="#western-sector"><li>Ground Operation</li></a>
                                <a href="#operations"><li>Accession</li></a>
                                <a href=""><li>Dummy </li></a>
                            </ul>
                    </div>

                        <h6 class="accordion-toggle">Operation Vijay 1961</h6>
                        <div class="accordion-content" style="display: none;">
                        <ul>
                        <a href="#"><li class="active">Annexation of Hyderabad</li></a>
                                <a href="#eastern-sector"><li>Air Operations </li></a>
                                <a href="#western-sector"><li>Ground Operation</li></a>
                                <a href="#operations"><li>Accession</li></a>
                                <a href=""><li>Dummy </li></a>
                            </ul></div>

                        <h6 class="accordion-toggle">Sikkim: Skirmish at Nathu <br> La 1967</h6>
                        <div class="accordion-content" style="display: none;">
                        <ul>
                        <a href="#"><li class="active">Annexation of Hyderabad</li></a>
                                <a href="#eastern-sector"><li>Air Operations </li></a>
                                <a href="#western-sector"><li>Ground Operation</li></a>
                                <a href="#operations"><li>Accession</li></a>
                                <a href=""><li>Dummy </li></a>
                            </ul></div>

                        <h6 class="accordion-toggle">Siachen: Operation Meghdoot 1984</h6>
                        <div class="accordion-content" style="display: none;">
                            <ul>
                            <a href="#"><li class="active">Annexation of Hyderabad</li></a>
                                <a href="#eastern-sector"><li>Air Operations </li></a>
                                <a href="#western-sector"><li>Ground Operation</li></a>
                                <a href="#operations"><li>Accession</li></a>
                                <a href=""><li>Dummy </li></a>
                            </ul>
                        </div>

                        <h6 class="accordion-toggle">Operation Pawan: Indian Peace Keeping Force, 1987</h6>
                        <div class="accordion-content" style="display: none;"></div>


                        <h6 class="accordion-toggle">Maldives: Operation Cactus<br> 1998</h6>
                        <div class="accordion-content" style="display: none;">
                            <ul>
                            <a href="#"><li class="active">Annexation of Hyderabad</li></a>
                                <a href="#eastern-sector"><li>Air Operations </li></a>
                                <a href="#western-sector"><li>Ground Operation</li></a>
                                <a href="#operations"><li>Accession</li></a>
                                <a href=""><li>Dummy </li></a>
                            </ul>
                        </div>



                        <h6 class="accordion-toggle">Operation Vijay: Kargil 1999 </h6>
                        <div class="accordion-content" style="display: none;">
                            
                        </div>





                        <h6 class="accordion-toggle">LICO and CI Ops - J&K, NE</h6>
                        <div class="accordion-content" style="display: none;">
                            <ul>
                                <a href="#j&k"><li class="active">J&K</li></a>
                                <a href="#punjab"><li>Punjab </li></a>
                                <a href="#north"><li>North East</li></a>
                                <a href="#gallery"><li>Gallery </li></a>

                            </ul>
                        </div>



                        <h6 class="accordion-toggle">Peace Keeping Ops</h6>
                        <div class="accordion-content" style="display: none;">
                            <ul>
                                <a href="#india-and-un"><li class="active">India and UN Peacekeeping</li></a>
                                <a href="#peacekeeping"><li>Peacekeeping Missions: Past and Current </li></a>
                                <a href="#gallery"><li>Gallery </li></a>
                                
                            </ul>
                        </div>


                        <h6 class="accordion-toggle">Humanitarian and Disaster Relief (HADR)</h6>
                        <div class="accordion-content" style="display: none;">
                        </div>
                        
                </div> 
            </div> 