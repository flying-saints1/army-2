<?php 

get_header();
?>

<?php echo the_content(); ?>


<?php 
    get_template_part('template-parts/banner-section')
?>

<?php if(have_rows('iconic_leader')): ?>
    <?php while(have_rows('iconic_leader')): the_row(); ?>
        <section class="iconic-leaders-detail">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-9 res-leader">
                        <img src="<?php echo get_sub_field('leader_image'); ?>" class="img-fluid" width="100%" alt="" loading="lazy">
                        <h2><?php echo get_the_title(); ?></h2>
                        <div><?php echo get_sub_field('content_1'); ?></div>

                        <img src="<?php echo get_sub_field('image2'); ?>" class="img-fluid" width="100%" alt="" loading="lazy">

                        <div><?php echo get_sub_field('content2'); ?></div>


                        <div class="leader-wrapper">
                        <img src="<?php echo get_sub_field('image2'); ?>" class="img-fluid" alt="" loading="lazy">
                        <!-- <img src="../assets/img/marshal-3.jpg" class="img-fluid" alt="" loading="lazy">
                        <img src="../assets/img/marshal-3.jpg" class="img-fluid" alt="" loading="lazy"> -->
                        </div>


                        <div><?php echo get_sub_field('content3'); ?></div>
                            
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>  