<?php

/*
*
* Template Name: Home template
*
*/

$main = "home";

get_header(); ?>


<?php if(have_rows('banner')): ?>
    <section class="home-banner-slider">
        <div class="row home-banner">
            <?php while(have_rows('banner')): the_row(); ?>

            <div class="carousel-item carousel-item-1 active">
                <img src="<?php echo get_sub_field('image'); ?>" class="d-block w-100 bg-image-carousel" alt="..." loading="lazy">
                <div class="container">

                    <div class="banner-text">
                        <div class="banner-head">
                            <h2><?php echo get_sub_field('heading'); ?></h2>
                        </div>
                        <div class="banner-para">
                            <p><?php echo get_sub_field('content'); ?></p>
                        </div>

                        <?php
                            $link = get_sub_field('link');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <div class="row">
                                <div class="col-12">
                                    <a href="<?php echo esc_url($link_url); ?>" class="banner" target="<?php echo esc_attr($link_target); ?>"> <button class="banner-btn"><?php echo $link_title; ?></button></a>
                                </div>
                            </div>
                        <?php endif; ?>
                        <!-- <a href="#"> <button class="banner-btn">Know More</button></a> -->
                    </div>
                </div>
            </div>
            <?php endwhile; ?>

           


        </div>
    </section>
<?php endif; ?>

<?php if(have_rows('about_army')): ?>
    <?php while(have_rows('about_army')): the_row(); ?>
        <section class="about">
            <div class="container">
                <div class="row about-section">
                    <div class="col-md-4">
                        <h2 class="head"><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                    <div class="col-md-8">
                        <p><?php echo get_sub_field('content'); ?></p>
                    </div>
                </div>

                <?php if(have_rows('indian_army')): ?>
                    <div class="row">
                        <div class="col-12 animateUp animated-slider">
                            <div class="p-rel">
                                <div class="partner-slide">
                                    <?php while(have_rows('indian_army')): the_row(); ?>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="box">
                                                <div class="card-info">
                                                    <h2><?php echo get_sub_field('heading'); ?></h2>
                                                    <div class="card-body">
                                                        <p class="text"><?php echo get_sub_field('content'); ?></p>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <img src="<?php echo get_sub_field('image'); ?>" class="card-img" alt="..." loading="lazy">
                                                    <div class="card-body">
                                                        <h2 class="card-text"><?php echo get_sub_field('heading'); ?></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>


                <div class="war">
                    <div class="row justify">
                        <div class="col-md-12 ">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link tab-links active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Pre-Indep Wars</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link tab-links" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Post-Indep Wars</button>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <div class="row">
                                        <div class="col-md-6 mt-4 mb-4">
                                            <div class="card-detail">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/war-img1.jpg" class="card-img-top" alt="..." loading="lazy">
                                                <div class="card-body">
                                                    <h2 class="card-head">World War I</h2>
                                                    <p class="card-body-text">The immediate trigger for the war was the assassination of Archduke Franz Ferdinand, the heir apparent to the throne of the Austro-Hungarian Empire in Sarajevo...</p>
                                                    <a href="#" class="read-more">Read More <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mt-4 mb-4">
                                            <div class="card-detail">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/war-img2.jpg" class="card-img-top" alt="..." loading="lazy">
                                                <div class="card-body">
                                                    <h2 class="card-head">World War II</h2>
                                                    <p class="card-body-text">World War I (1914–18) was the most destructive conflict the world had seen until World War II (1939–45). In World War I, though the fighting stopped in November 1918, the war formally...</p>
                                                    <a href="#" class="read-more">Read More <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-6 mt-4 mb-4">
                                    <div class="card-detail">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/war-img2.jpg" class="card-img-top" alt="..." loading="lazy">
                                        <div class="card-body">
                                            <h2 class="card-head">Small War</h2>
                                            <p class="card-body-text">Col C.E. Callwell defines ‘little’ or ‘small’ wars as ‘…campaigns other than those where both the opposing sides consist of regular troops.’ 1 He goes on to state: ‘The...</p>
                                            <a href="#" class="read-more">Read More <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                        </div>
                                    </div>
                                </div> -->
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    <div class="row">

                                        <div class="col-md-6 mt-4 mb-4">
                                            <div class="card-detail">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/post-war1.jpg" class="card-img-top" alt="..." loading="lazy">
                                                <div class="card-body">
                                                    <h2 class="card-head">1947-48 Kashmir Conflict</h2>
                                                    <p class="card-body-text">The princely State of Jammu and Kashmir (J&K) had been brought under British paramountcy in 1846 via the Treaty of Amritsar, signed between the East India Company...</p>
                                                    <a href="#" class="read-more">Read More <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mt-4 mb-4">
                                            <div class="card-detail">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/post-war2.jpg" class="card-img-top" alt="..." loading="lazy">
                                                <div class="card-body">
                                                    <h2 class="card-head">1962 Sino-Indian Conflict </h2>
                                                    <p class="card-body-text">The People’s Republic of China (China) annexed Tibet in 1950-51 and with that an independent India acquired a new neighbour. The two countries also shared a 4,000 km long </p>
                                                    <a href="#" class="read-more">Read More <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mt-4 mb-4">
                                            <div class="card-detail">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/post-war3.jpg" class="card-img-top" alt="..." loading="lazy">
                                                <div class="card-body">
                                                    <h2 class="card-head">1965 India–Pakistan War</h2>
                                                    <p class="card-body-text">In 1965, India was picking itself up from the fallout of the 1962 war; it was heavily dependent on dwindling US exports to battle an acute famine; and it was dealing with...</p>
                                                    <a href="#" class="read-more">Read More <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mt-4 mb-4">
                                            <div class="card-detail">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/post-war4.jpg" class="card-img-top" alt="..." loading="lazy">
                                                <div class="card-body">
                                                    <h2 class="card-head">1971 India–Pakistan War</h2>
                                                    <p class="card-body-text">The 1971 war was a manifestation of domestic political estrangement between the two halves of the Pakistani state. With little beyond a common religious belief and a deep...</p>
                                                    <a href="#" class="read-more">Read More <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" class="img-fluid" alt="" loading="lazy"></a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if(have_rows('operation_section')): ?>
    <?php while(have_rows('operation_section')): the_row(); ?>
        <section id="operation" style="background-image: url('<?php echo get_sub_field('bg_image'); ?>');">
            <div class="bg-operation">
                <div class="container">
                    <div class="row operation-heading">
                        <div class="col-md-6">
                            <h2 class="head"><?php echo get_sub_field('heading'); ?></h2>
                        </div>

                        <div class="col-md-6">
                            <div class="btn-seciton">
                            <?php
                                $link = get_sub_field('link');
                                if ($link) :
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                <div class="row">
                                    <div class="col-12">
                                        <a href="<?php echo esc_url($link_url); ?>" class="view-btn" target="<?php echo esc_attr($link_target); ?>"> <?php echo $link_title; ?></a>
                                    </div>
                                </div>
                            <?php endif; ?>
                                <!-- <a href="operations.php" class="view-btn">View All Operations</a> -->
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 animateUp animated-slider">
                            <div class="p-rel">
                            <div class="partner-slide">

                                <?php /*
                                    $paged = get_query_var('paged') ? get_query_var('paged') : 1;

                                    $args = array(
                                        'post_type' => 'speaker',
                                        'post_status' => 'publish',
                                        'paged' => $paged,
                                        'posts_per_page' => 8,
                                        'meta_key' => 'name',
                                        'orderby' => 'meta_value',
                                        'order' => 'ASC'
                                    );

                                    $loop = new WP_Query($args);
                                    ?>
                                    <?php if ($loop->have_posts()) : ?>

                               
                                       
                                        <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                                            <?php // $speakersId = get_the_ID(); 
                                            ?>
                                            <?php $speakersName = get_the_title(); ?>
                                            <?php $speakersShortdis = get_the_excerpt(); ?>
                                            <?php $speakersTitle = get_field('speakers_designation'); ?>

                                            <div class="col-12 col-sm-4  col-md-3">
                                                <div class="box show-popup1">
                                                    <picture>
                                                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo $speakersName; ?>" class="img-fluid" loading="lazy">
                                                    </picture>
                                                    <h4><?php echo $speakersName; ?></h4>
                                                    <span><?php echo $speakersTitle; ?></span>
                                                    <p><?php echo $speakersShortdis; ?></p>
                                                    <p class="full-content"><?php echo get_field('full_description'); ?></p>
                                                    <button class="show-popup">Read more</button>
                                                </div>
                                            </div>
                                        <?php endwhile;
                                        wp_reset_postdata();  ?>
                                     
                                    <?php endif; */ ?>
                                    <div class="col-md-4">
                                        <div class="operation-section">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation1.jpg" class="card-img-top" alt="..." loading="lazy">
                                            <div class="card-body opration-card-body">
                                                <h2 class="heading">Operation Polo <br> 1948</h2>
                                                <p class="card-body-text">When India gained independence from the British in 1947, the various princely...</p>
                                                <a href="operation-details.php" class="read-more">Read More <i class="fa fa-angles-right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="operation-section">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation2.jpg" class="card-img-top" alt="..." loading="lazy">
                                            <div class=" card-body opration-card-body">
                                                <h2 class="heading">Operation Vijay <br> 1961</h2>
                                                <p class="card-body-text">The state of Goa became part of the Indian Union in 1961, a decade and a half after independence...</p>
                                                <a href="./operations/operation-vijay-1961.php" class="read-more">Read More <i class="fa fa-angles-right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="operation-section">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation3.jpg" class="card-img-top" alt="..." loading="lazy">
                                            <div class="card-body opration-card-body">
                                                <h2 class="heading">Sikkim: Skirmish at Nathu <br> La 1967</h2>
                                                <p class="card-body-text">The Nathu La pass lies on the Old Silk Route between Tibet and India. In 1904...</p>
                                                <a href="operation-details.php" class="read-more">Read More <i class="fa fa-angles-right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="operation-section">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation4.jpg" class="card-img-top" alt="..." loading="lazy">
                                            <div class="card-body opration-card-body">
                                                <h2 class="heading">Siachen: Operation Meghdoot<br> 1984</h2>
                                                <p class="card-body-text">Located in the eastern Karakoram range in the Himalayas, the Siachen Galcier...</p>
                                                <a href="./operations/siachen-operation-meghdoot-1984.php" class="read-more">Read More <i class="fa fa-angles-right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="operation-section">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation5.jpg" class="card-img-top" alt="..." loading="lazy">
                                            <div class=" card-body opration-card-body">
                                                <h2 class="heading">Operation Pawan: Indian Peace Keeping Force, 1987</h2>
                                                <p class="card-body-text">Sri Lanka is an island nation in the Indian Ocean, separated from the Asian mainland...</p>
                                                <a href="./operations/operation-pawan-1987.php" class="read-more">Read More <i class="fa fa-angles-right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="operation-section">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation6.jpg" class="card-img-top" alt="..." loading="lazy">
                                            <div class="card-body opration-card-body">
                                                <h2 class="heading">Maldives: Operation <br>Cactus 1998</h2>
                                                <p class="card-body-text">Maldives is an island nation in the Indian Ocean, composed entirely of about 1,190...</p>
                                                <a href="./operations/maldives-operation-cactus-1998.php" class="read-more">Read More <i class="fa fa-angles-right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="operation-section">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation7.jpg" class="card-img-top" alt="..." loading="lazy">
                                            <div class="card-body opration-card-body">
                                                <h2 class="heading">Operation Vijay: Kargil <br> 1999 </h2>
                                                <p class="card-body-text">The Jammu and Kashmir (J&K) region has had political and cultural connections...</p>
                                                <a href="./operations/operation-vijay-kargil-1999.php" class="read-more">Read More <i class="fa fa-angles-right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="operation-section">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation8.jpg" class="card-img-top" alt="..." loading="lazy">
                                            <div class=" card-body opration-card-body">
                                                <h2 class="heading">LICO and CI Ops - <br> J&K, NE</h2>
                                                <p class="card-body-text">The incidents in J&K may have manifested into a violent movement in 1988, however its roots...</p>
                                                <a href="lico-and-cI.php" class="read-more">Read More <i class="fa fa-angles-right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="operation-section">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation9.jpg" class="card-img-top" alt="..." loading="lazy">
                                            <div class="card-body opration-card-body">
                                                <h2 class="heading">Peace Keeping <br> Ops</h2>
                                                <p class="card-body-text">Since the inception of the first UN peacekeeping mission in 1948...</p>
                                                <a href="./operations/peace-keeping-ops.php" class="read-more">Read More <i class="fa fa-angles-right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="operation-section">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/operation10.jpg" class="card-img-top" alt="..." loading="lazy">
                                            <div class="card-body opration-card-body">
                                                <h2 class="heading">Humanitarian and Disaster Relief (HADR)</h2>
                                                <p class="card-body-text">The Indian subcontinent is prone to earthquakes, floods, droughts, cyclones, etc...</p>
                                                <a href="./operations/humanitarian-and-disaster-relief.php" class="read-more">Read More <i class="fa fa-angles-right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<section class="iconic-leader" id="leaders">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <h2 class="head">Iconic Leaders</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-12 animateUp animated-slider">
                <div class="p-rel">
                    <div class="partner-slide">
                        <div class="col-md-4 post active" data-cat="blog-ux">
                            <div class="iconic-leader-box">
                                <a href="./iconic-leaders/km-carippa.php" class="title">
                                    <div class="image">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/cariappa.jpg" class="img-fluid" alt="" loading="lazy" />
                                    </div>
                                    <div class="content">
                                        <div class="come-in">

                                            <h4>KM Cariappa</h4>
                                            <p>Field Marshal</p>

                                            <a href="./iconic-leaders/km-carippa.php" class="read-more">Read More</a><span>
                                                <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" alt="" loading="lazy"> </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <!-- <div class="col-md-4 active" data-cat="blog-ux">
                            <div class="iconic-leader-box">
                                <div class="image">
                                    <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/thimayya.jpg" class="img-fluid" alt="" loading="lazy" />
                                </div>
                                <div class="content">
                                    <div class="come-in">
                                        <a href="ks-thimayya.php" class="title">
                                            <h4>KS Thimayya</h4>
                                            <p>General</p>
                                        </a>
                                        <a href="iconic-leaders.php" class="read-more">Read More</a><span>
                                            <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" alt="" loading="lazy"> </span>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="col-md-4 active" data-cat="blog-ux">
                            <div class="iconic-leader-box">
                                <a href="./iconic-leaders/shfj-manekshaw.php" class="title">
                                    <div class="image">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/manekshaw.jpg" class="img-fluid" alt="" loading="lazy" />
                                    </div>
                                    <div class="content">
                                        <div class="come-in">

                                            <h4>SHFJ Manekshaw</h4>
                                            <p>Field Marshal</p>

                                            <a href="./iconic-leaders/shfj-manekshaw.php" class="read-more">Read More</a><span>
                                                <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" alt="" loading="lazy"> </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <!-- <div class="col-md-4 active" data-cat="blog-ux">
                            <div class="iconic-leader-box">
                                <div class="image">
                                    <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/sagat-singh.jpg" class="img-fluid" alt="" loading="lazy" />
                                </div>
                                <div class="content">
                                    <div class="come-in">
                                        <a href="sagat-singh.php" class="title">
                                            <h4>Sagat Singh</h4>
                                            <p>Lieutenant General</p>
                                        </a>
                                        <a href="iconic-leaders.php" class="read-more">Read More</a><span>
                                            <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" alt="" loading="lazy"> </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 active" data-cat="blog-ux">
                            <div class="iconic-leader-box">
                                <div class="image">
                                    <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/hanut-singh.jpg" class="img-fluid" alt="" loading="lazy" />
                                </div>
                                <div class="content">
                                    <div class="come-in">
                                        <a href="" class="title">
                                            <h4>Hanut Singh </h4>
                                            <p>Lt General</p>
                                        </a>
                                        <a href="iconic-leaders.php" class="read-more">Read More</a><span>
                                            <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" alt="" loading="lazy"> </span>
                                    </div>
                                </div>
                            </div>
                        </div>  

                        <div class="col-md-4 active" data-cat="blog-ux">
                            <div class="iconic-leader-box">
                                <div class="image">
                                    <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/bhagat.jpg" class="img-fluid" alt="" loading="lazy" />
                                </div>
                                <div class="content">
                                    <div class="come-in">
                                        <a href="" class="title">
                                            <h4>PS Bhagat </h4>
                                            <p>Lieutenant General</p>
                                        </a>
                                        <a href="iconic-leaders.php" class="read-more">Read More</a><span>
                                            <img src="./<?php echo get_template_directory_uri(); ?>/assets/img/icons/double-right-arrow.png" alt="" loading="lazy"> </span>
                                    </div>
                                </div>
                            </div>
                        </div>   -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- <section class="blogs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="head">Our Blogs</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/blog1.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020 &nbsp; <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> 255 </p>
                        <div class="come-in">
                            <a href="blog-details.php" class="title">
                                <h4>Lorem Ipsum is simply <br>dummy ...</h4>
                            </a>
                            <a href="blog.php" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp;<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/blog2.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020 &nbsp <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> 255 </p>
                        <div class="come-in">
                            <a href="blog-details.php" class="title">
                                <h4>Lorem Ipsum is simply <br>dummy ...</h4>
                            </a>
                            <a href="blog.php" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-4 mb-4">
                <div class="blog-box first animateUp">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/blog3.jpg" class="img-fluid" alt="" loading="lazy">
                    <div class="content">
                        <p class="date"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2020 &nbsp <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> 255 </p>
                        <div class="come-in">
                            <a href="blog-details.php" class="title">
                                <h4>Lorem Ipsum is simply <br>dummy ...</h4>
                            </a>
                            <a href="blog.php" class="read-more">Read More </a><span style=" font-size: 12px; color: #D90000;">&nbsp<i class="fa fa-angles-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section> -->

<section class="featured">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="head">Featured Video</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="featured-video">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">
                        <p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2022 &nbsp;&nbsp; <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255</p>
                        <h5 class="card-title">Retiring Officers <br> Seminar</h5>
                        <p class="">General Manoj Pande #COAS conveys deep appreciation to all Retiring Officers for their invaluable contribution to the Nation and #IndianArmy.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="featured-video">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/3t5M10gBVCc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">
                        <p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy"> &nbsp; November 5,2022 &nbsp; &nbsp;<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy"> &nbsp; 255</p>
                        <h5 class="card-title">Indian Army : A Life Less Ordinary</h5>

                        <p class="">Life In the Indian Army is not an ordinary life. It is full of challenges and opportunities and a chance to serve the motherland. Join us to be a part of this glorious life style.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="featured-video">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/1CCFf4rej2E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="card-body">
                        <p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/date.png" class="img-fluid" alt="" loading="lazy">&nbsp; November 5,2022 &nbsp; &nbsp;<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/view.png" class="img-fluid" alt="" loading="lazy">&nbsp; 255 </p>
                        <h5 class="card-title">Paralympic Games <br> Tokyo</h5>

                        <p class="">General Bipin Rawat #CDS conveys best wishes to the Indian contingent participating in Paralympic Games #Tokyo2020.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>








<?php get_footer(); ?>