<?php

$main = "wars";

$page = "pre";

$subPage = "war-1";

include('header.php'); ?>


<section class="post-independence-war-banner" style="background-image: url(./assets/img/post-independence-war-banner.jpg);">
    <div class="container">
        <h1 class="banner-content">World War I</h1>
    </div>
</section>


<section class="indep-war" id="faq-section">
    <div class="container">
        <div class="row">
            <?php include('sidebar/post-indeep-sidebar.php'); ?>

            <div class="col-md-9">
                <div class="war-content">
                    <div class="content">
                        <img src="assets/img/small-war.png" width="100%" class="img-fluid res-image small-banner" alt="">
                        <h4> World War I</h4>
                    </div>
                </div>


                <div id="about" class="accordion-details">

                    <h3>About war</h3>

                    <div class="war-details">
                        <h4>Causes of World War I</h4>
                        <p>The immediate trigger for the war was the assassination of Archduke Franz Ferdinand, the heir apparent to the throne of the Austro-Hungarian Empire in Sarajevo, on 24 June 1914, by Gavrilo Princip, a Bosnian Serb student. However, Europe had been moving steadily towards war since the nineteenth century. While the Congress of Vienna brought peace to the continent in the aftermath of the Napoleonic Wars, the map of Europe began to change. New nation states started to emerge out of empires; the Italian and German unifications, which culminated around 1871, led to a race for colonies; increasing industrialisation and militarisation led to growing economic and military power; and an arms and naval race ensued, in particular between Britain—the foremost naval power in Europe—and Germany. In the early years of the twentieth century, there were a series of political crises in the Balkans, then a part of the large Austro-Hungarian Empire. Ultimately, the bugle call for war was sounded in Sarajevo.</p>

                        <p>It was a given that being the jewel of the British Empire, India would be involved in its conflicts. Great Britain’s declaration of war on 4 August 1914 drew its colonies into the war. Even before the war began in 1914, around 10,000 Indian troops had been dispatched to Persia to ensure that the oil supplies from the region were not affected. This was significant as the British Navy, the paramount naval power in the world, had converted from coal to oil to power its ships. This was also the time that the nationalist movement in India was gathering momentum. Indians, in the early decades of the twentieth century, were calling for greater autonomy and power from Great Britain. By supporting the British in World War I, it was hoped that India would be granted dominion status.</p>

                        <p>On the eve of World War I in 1914, the British Indian Army comprised 150,000 men. It expanded sevenfold during the course of the war: ‘around 1.2 million men were mobilized for the Indian Army and 0.3 million for non-combat duties.’</p>
                    </div>

                    <div class="war-details">
                        <h4>World War I (1914–18)</h4>
                        <p>When war broke out in Europe, the Indian government offered Britain two infantry divisions composed of three brigades, each with one British and three Indian Battalions, and two cavalry divisions with three brigades, each with one British and two Indian regiments. Subsequent to the British government’s acceptance of the offer, ‘two Indian infantry divisions, the 3rd (Lahore Division) and the 7th (Meerut Division), along with the 9th Cavalry Brigade, embarked from Karachi and Bombay in August. The units began to arrive in Marseilles from September 26.’</p>

                        <p>The Indian Army’s primary war-fighting experience as well as training was for warfare on the frontiers of India, with weapons that were a generation behind that of the West. The men who went to war beginning 1914 were neither equipped nor trained to fight a war against Western armies. They had obsolete weaponry and unsuitable clothing for the extreme cold weather conditions of Europe, and were thrust into a war of attrition in trenches, something they had never faced before. Yet, they fought admirably and gained a stellar reputation for their bravery and conduct in alien terrain in a brutal mechanised war.</p>

                        <p>The army that went to war in 1914 was divided into seven expeditionary forces overseas, with broad deployments as follows.</p>

                        <ol>
                            <li>Indian Expeditionary Force (IEF) A: Northern Europe, in France and Flanders.</li>
                            <li>IEF B: East Africa.</li>
                            <li>IEF C: East Africa.</li>
                            <li>IEF D: Mesopotamia</li>
                            <li>IEF E: Egypt and Palestine</li>
                            <li>IEF F: Egypt</li>
                            <li>IEF G: Gallipoli</li>
                        </ol>


                        <!-- <div class="war-2-map-2">
                            <img src="assets/img/1914-18-war-map.png" width="100%" height="400px" class="img-fluid" alt="">
                        </div> -->

                        <p>The Indian participation in the war was not limited to fighting units alone. Apart from 90,000 combatant personnel, India sent ‘50,000 men from labour companies to France’ alone. Additionally, ‘each regiment had followers such as Hindu and Muslim cooks, bhistis, and ward servants, ward orderlies (high-caste Hindus and Muslims), ambulance kahars (trained bearers), plus sweepers (low-caste Hindus and untouchables) for the ambulance sections.’</p>

                        <p>Apart from the regular Indian Army, the armies of the Indian princely states also contributed significantly to the war effort. A large contingent of these state troops, known as IST, took part in operations in many parts of the world (see Table 1).</p>


                        <div class="table-wrap">
                            <div class="box-table">
                                <h6 style="font-size: 18px; font-weight: 500; color: #45464B;">Table 1 Imperial Service Troops in World War I</h6>
                                <table class="table">
                                    <!-- <thead>
                                        <tr>
                                            <th scope="col"><b>Indian States</b></th>
                                            <th scope="col"><b>No. of Combatants sent Overseas</b></th>
                                            <th scope="col"><b>Remarks</b></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Alwar</th>
                                            <td>1,502</td>
                                            <td>1 squadron of cavalry and 1 battalion of infantry</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Bhawalpur</th>
                                            <td>326</td>
                                            <td>Detachments from the camel corps and camel transport</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Bharatpur</th>
                                            <td>1,581</td>
                                            <td>1 battalion of infantry and 1 transport corps</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Bikaner</th>
                                            <td>444</td>
                                            <td>1 camel corps</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Faridkot</th>
                                            <td>2,597</td>
                                            <td>1 sapper company</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Gwalior</th>
                                            <td>1,075</td>
                                            <td>2 battalions of infantry and detachment of transport corps</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Hyderabad</th>
                                            <td>681</td>
                                            <td>1 regiment of cavalry</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Indore</th>
                                            <td>20</td>
                                            <td>1 squadron of cavalry and 1 transport corps</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Idar</th>
                                            <td>1,256</td>
                                            <td>Despatch riders</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Jaipur</th>
                                            <td>1,116</td>
                                            <td>1 transport corps</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Jhind</th>
                                            <td>1342</td>
                                            <td>1 battalion of infantry</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Jodhpur</th>
                                            <td>472</td>
                                            <td>1 regiment of cavalry</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Kathiawar</th>
                                            <td>689</td>
                                            <td>Detachments of cavalry</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Kapurthala</th>
                                            <td>4,983</td>
                                            <td>1 battalion of infantry</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Kashmir</th>
                                            <td>147</td>
                                            <td>Detachments of cavalry, 1 battery of mountain artillery</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Khairpur</th>
                                            <td>520 </td>
                                            <td>3 battalions of infantry</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Malerkotla</th>
                                            <td>1,355</td>
                                            <td>Detachments of infantry and transport corps</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Mysore</th>
                                            <td>538</td>
                                            <td>1 company and detachments of sappers.</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Nabha</th>
                                            <td>2,695</td>
                                            <td>1 regiment of cavalry and 1 transport corps</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Patiala</th>
                                            <td>567</td>
                                            <td>1 battalion of infantry</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Rampur</th>
                                            <td>5</td>
                                            <td>1 regiment of cavalry and 1 battalion of infantry</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Rutlam</th>
                                            <td>561</td>
                                            <td>1 battalion of infantry</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Sirmur</th>
                                            <td>457</td>
                                            <td>Despatch riders</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Tehri</th>
                                            <td>6</td>
                                            <td>2 companies of sappers, the second company replacing the first which was captured at Kut-el-Amarah</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Total</th>
                                            <td>26,099</td>
                                            <td></td>
                                        </tr>
                                    </tbody> -->
                                    <thead>
                                        <tr>
                                            <th>Indian States</th>
                                            <th>No. of Combatants Sent Overseas</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Alwar</td>
                                            <td>1,502</td>
                                            <td>1 squadron of cavalry and 1 Battalion of Infantry</td>
                                        </tr>
                                        <tr>
                                            <td>Bhawalpur</td>
                                            <td>326</td>
                                            <td>Detachments from the Camel Corps and Camel Transport</td>
                                        </tr>
                                        <tr>
                                            <td>Bharatpur</td>
                                            <td>1,581</td>
                                            <td>1 Battalion of Infantry and 1 Transport Corps</td>
                                        </tr>
                                        <tr>
                                            <td>Bikaner</td>
                                            <td>444</td>
                                            <td>1 Camel Corps</td>
                                        </tr>
                                        <tr>
                                            <td>Faridkot</td>
                                            <td>2,597</td>
                                            <td>1 Sapper Company</td>
                                        </tr>
                                        <tr>
                                            <td>Gwalior</td>
                                            <td>1,075</td>
                                            <td>2 Battalions of Infantry and detachment of Transport Corps</td>
                                        </tr>
                                        <tr>
                                            <td>Hyderabad</td>
                                            <td>681</td>
                                            <td>1 Regiment of Cavalry</td>
                                        </tr>
                                        <tr>
                                            <td>Indore</td>
                                            <td>20</td>
                                            <td>1 Squadron of Cavalry and 1 Transport Corps</td>
                                        </tr>
                                        <tr>
                                            <td>Idar</td>
                                            <td>1,256</td>
                                            <td>Despatch riders</td>
                                        </tr>
                                        <tr>
                                            <td>Jaipur</td>
                                            <td>1,116</td>
                                            <td>1 Transport Corps</td>
                                        </tr>
                                        <tr>
                                            <td>Jhind</td>
                                            <td>1,342</td>
                                            <td>1 Battalion of Infantry</td>
                                        </tr>
                                        <tr>
                                            <td>Jodhpur</td>
                                            <td>472</td>
                                            <td>1 Regiment of Cavalry</td>
                                        </tr>
                                        <tr>
                                            <td>Kathiawar</td>
                                            <td>689</td>
                                            <td>Detachments of Cavalry</td>
                                        </tr>
                                        <tr>
                                            <td>Kapurthala</td>
                                            <td>4,983</td>
                                            <td>1 Battalion of Infantry</td>
                                        </tr>
                                        <tr>
                                            <td>Kashmir</td>
                                            <td>147</td>
                                            <td>Detachments of Cavalry, 1 Battery of Mountain Artillery</td>
                                        </tr>
                                        <tr>
                                            <td>Khairpur</td>
                                            <td>520</td>
                                            <td>3 Battalions of Infantry</td>
                                        </tr>
                                        <tr>
                                            <td>Malerkotla</td>
                                            <td>1,355</td>
                                            <td>Detachments of Infantry and Transport Corps</td>
                                        </tr>
                                        <tr>
                                            <td>Mysore</td>
                                            <td>538</td>
                                            <td>1 Company and detachments of Sappers.</td>
                                        </tr>
                                        <tr>
                                            <td>Nabha</td>
                                            <td>2,695</td>
                                            <td>1 Regiment of Cavalry and 1 Transport Corps</td>
                                        </tr>
                                        <tr>
                                            <td>Patiala</td>
                                            <td>567</td>
                                            <td>1 Battalion of Infantry</td>
                                        </tr>
                                        <tr>
                                            <td>Rampur</td>
                                            <td>5</td>
                                            <td>1 Regiment of Cavalry and 1 Battalion of Infantry</td>
                                        </tr>
                                        <tr>
                                            <td>Rutlam</td>
                                            <td>561</td>
                                            <td>1 Battalion of Infantry</td>
                                        </tr>
                                        <tr>
                                            <td>Sirmur</td>
                                            <td>457</td>
                                            <td>Despatch riders</td>
                                        </tr>
                                        <tr>
                                            <td>Tehri</td>
                                            <td>6</td>
                                            <td>2 Companies of Sappers, the second company replacing the first which was captured at Kut-el-Amarah</td>
                                        </tr>
                                        <tr>
                                            <td>Total</td>
                                            <td>26,099</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p style="font-size: 14px; font-weight: 400; color: #45464B;">Source: India and the Great War: An Overview, n. 4, p. 23.</p>
                            </div>
                        </div>

                        <p>Broadly, thus, the Indians fought in most major theatres of World War I: Northern Europe, Mesopotamia, Africa, Palestine and Gallipoli. A brief discussion of these follows next.</p>
                    </div>
                    <div id="france" class="accordion-details">
                        <h3>Northern Europe: France and Flanders</h3>

                        <div class="war-details">

                            <p>The IEF A (also referred to as the Indian Corps) took part in the First Battle of Ypres in Belgium (19 October–22 November 1914); the attack on Neuve Chapelle (28 October 1914); the Battle of Festubert (November 1914); the Battle of Givenchy (16–22 December 1914); the Battle of Neuve Chapelle (10–13 March 1915); the Second Battle of Ypres (22 April–25 May 1915); the Battle of Aubers Ridge (9 May 1915); the Battle of Festubert (15–25 May 1915); and the Battle of Loos: Action at Piètre (25 and 27 September 1915).</p>

                            <p>When the war began, Germany had set into motion the Schlieffen Plan, which called for the defeat of France through a sweeping manoeuvre through Belgium and northern France. However, the plan saw limited success and the war deteriorated into trench warfare; eventually, the trench lines extended from close to the English Channel to Switzerland. Warfare degenerated into attrition, with a series of attempts to break through the enemy’s defences using firepower. The British Army on the ground soon found itself overstretched with no reserves. The only trained manpower available was the Indian Army, which was called upon. Simon Doherty and Tom Donovan describe the conditions under which the Indian Army was called to action:</p>

                            <p>The Indian Corps came to the Western Front at a most critical point in the First World War…There was a real possibility that the Germans might have broken through the British lines and pushed the British Expeditionary Force (BEF) back to the coast. The Territorial Force was not yet ready to take to the field (with a few exceptions) and the New Army then being raised would take another eight months to train before any of its formations could be sent overseas. Australians, Canadians, and New Zealanders were being mobilised but none would arrive in any theatre of war until 1915. At the end of 1914 one third of the ‘British’ troops in France were Indians.</p>


                            <!-- <div class="war-2-map-2">
                                <img src="assets/img/weston-frount-war-map.png" width="100%" height="400px" class="img-fluid" alt="">
                                <p style="color: #D90000;">Indian Corps on the Western Front, France and Belgium</p>
                            </div> -->

                            <p>Within weeks of the declaration of war on 4 August 1914, Indian troops arrived in France. The Lahore Division arrived in Marseilles on 26 September 1914, followed by the Meerut Division in October 1914, and hit the ground running. These were formed into an Indian Corps under Lieutenant General (Lt Gen) Sir James Wilcox, and were ordered to relieve British divisions stretched from Neuve Chapelle to Givenchy; they held this front for the next 14 months.</p>

                            <div class="war-2-map-2">
                                <img src="assets/img/gallery/Marseilles.jpg" width="100%" height="400px" class="img-fluid" alt="">
                                <p style="color: #D90000;">15 Sikh disembark in Marseilles, France 1914 <br> <strong>Source: Rana Chhina</strong></p>
                            </div>

                            <p>The first major campaign that the Indian Army was involved in was the First Battle of Ypres, which was the culmination of the famous ‘Race to the Sea’. Here, the 129th Baluchis made the first actual attack by an Indian unit on the Western Front when, on 26 October 1914, they attacked German trenches east of Hollebeke. On 31 October, the Battalion’s two machine guns were eventually overrun by the Germans after a severe fight. The men kept the remaining gun in action until the German assault swept over them. Sepoy Khudadad Khan, although severely wounded, continued to fire his gun until prevented from doing so. Feigning death, he waited until dark, and then managed to crawl back from the front to find his regiment. For his brave actions, he was awarded the VC, becoming the first Indian to receive the gallantry medal.</p>

                            <p>The next major engagement at Neuve Chapelle took place on 28 October 1914, where 47th Sikh, supported by 9th Bhopal Infantry and two Companies of Sappers, succeeded in capturing frontline German positions, but had to eventually pull back after heavy German counter-attacks. The Battle of Festubert in November 1914 was essentially a defensive action by the Indian Corps, consequent to a severe German attack resulting in loss of trenches. Its significance was that it was the first time that they fought together as a Corps. The lost trenches were recaptured by the morning of 24 November after heavy fighting. It was here that Naik Darwan Singh of 1/39th Garhwal Rifles became the second Indian to receive the VC.</p>

                            <p>The Battle of Givenchy took place between 16–22 December 1914. Towards the end of 1914, the Indian Corps was ordered to adopt a form of offensive defence. The Ferozepur Brigade, therefore, undertook an attack on the German lines to the north-east of Givenchy on 16 December 1914, with the aim of capturing two German saps opposite the 15th Sikhs and to use this as a firm base to undertake further operations to bite into the German defences. The attack was undertaken by the 129th Baluchis, who were eventually pushed back by strong German counter-attacks. ‘Although ultimately unsuccessful, the Baluchis had fought doggedly during this attack and this was reflected in the high number of casualties—55 men killed and 69 wounded.’</p>

                            <p>The Battle of Neuve Chapelle in March 2015—the first planned British offensive of the war—intended to break through the German salient round the village and move on to capture the Aubers Ridge, just over a mile away. The attack was to be undertaken on the right by the Meerut Division of the Indian Corps and on the left by IV Corps. This attack took place in inclement weather and the initial artillery bombardment was relatively successful. However, the 1/39th Garhwal Rifles on the right of the attack unintentionally veered off to the right and lost all of their British officers and a large number of men. As a result, a gap of 250 yards opened up between the two companies of Garhwal Rifles on the right and the 2nd Leicesters on their left, which the Germans took advantage of. The remainder of the Garhwal Brigade crossed the 200 yards of the no man’s land and succeeded in capturing the German frontline and support trenches.</p>

                            <div class="war-2-map-2">
                                <img src="assets/img/3rd-gurkha-rifles.jpg" class="img-fluid" alt="">
                                <p style="color: #D90000;">Firing a Lewis gun in the trenches 3rd Battalion of 3rd Gurkha Rifles, 1917  <br> <strong>Source: IWM Q 12931. </strong></p>
                            </div>

                            <p>Both 2/3rd Gurkhas and parties of the 2nd Battalion of the Garhwal Rifles took advantage of a favourable opportunity and advanced into Neuve Chapelle, joining up with the 2nd Rifle Brigade and moving through the village. The attackers reached the Smith Dorrien Line, the old British-held trench line before the Germans captured Neuve Chapelle. Finding that this line was flooded, they dug in 50 yards to the rear. Consolidation continued for five more hours and there was no further attempt to advance until 5 p.m., when the Dehra Dun Brigade reached the Bois du Biez. The British planned to continue the attack on the Bois du Biez on 12 March 1915, but this attack was pre-empted by the Germans who opened an intense artillery bombardment, followed by a massed frontal attack. The British and Indian troops held their fire until the German infantry was within 200 yards range, and then opened up with a deluge of artillery, machine gun and small-arms fire that cut swathes in the German formations. Over 2,000 bodies were counted in front of the Meerut Division alone. The following morning, Sir Douglas Haig suspended all further operations and the battle drew to a close.</p>

                            <div class="war-2-map-2">
                                <img src="assets/img/gallery/Mussalman.jpg" width="100%" height="400px" class="img-fluid" alt="">
                                <p style="color: #D90000;">The Punjabi Mussalman company of 57th Wilde’s Rifles (FF) takes up positions on the outskirts of Wytschaete, Belgium, in October 1914. A row of houses is visible behind them, showing their proximity to the town <br><strong>Source: IWM 56325. </strong></p>
                            </div>

                            <p>The Second Battle of Ypres commenced on 22 April 1915 with a ferocious German attack north-east of Ypres, involving the use of gas. In the ensuing days, the situation remained precarious and reserves were so scant that, for the second time in six months, units of the Lahore Division were rushed to Flanders. By 1100 Hours on 26 April, the Lahore Division was in position on a 600 yard front, north of Ypres. The plan was to attack the German trenches some 1,500 yards distant on Mauser Ridge, but the exact position of the German line was unknown. In between the British and the German lines was an intermediate feature, Hill Top Ridge.</p>

                            <p>The Indians attacked at 1400 Hours. Although the attackers were protected by Hill Top Ridge for the first 500 yards, disaster lay ahead. Once they crossed the ridge, they had to advance down the other side to a small stream, in full view of the Germans, then up a bare slope for the final 600 yards. Emphasis had been placed on speed but, as soon became horrifically apparent, the enemy trenches were too far away to charge. It was impossible to stop or reorganise the attack, and the survivors were pinned down. The battle saw many acts of bravery, chief among which was Jemadar Mir Dast, 55th (Coke’s) Rifles, attached to 57th (Wilde’s) Rifles, who reorganised men of the shattered Battalion when no British officers were left, and later helped to carry in no less than eight wounded British and Indian officers as well as a number of men. He was awarded the VC.</p>

                            <p>It was decided to renew the attack the following day. The Germans were still well dug-in. The leading units advanced across Hill Top Ridge and into aimed machine gun and rifle fire from the Germans, meeting the same fate as the previous day. The proposed continuations of the attack were cancelled during the next two days, and on the night of 29 April 1915, the Jullundur and Ferozepore Brigades were withdrawn, leaving only the Sirhind Brigade to take an active part in the operation. On 1 May 2015, the Sirhind Brigade made advances, but the German lines remained impenetrable. Orders were issued to withdraw, and the brigade came out of the battle in the night.</p>

                            <p>The Battle of Aubers Ridge, also known as the Rue du Bois attack, was conceived in April 1915, but postponed due to the Second Battle of Ypres. The plan envis- aged a two-pronged attack either side of Neuve Chapelle. The two attacking forces, 6,000 yards apart, would break through the lines and advance concentrically, meeting on the Aubers Ridge, thereby cutting off the German forces in the triangle of Neuve Chapelle–Aubers–Fauquissart. The Germans, located opposite both ‘prongs’, were well dug-in and had devoted considerable effort to strengthen their lines after the Battle of Neuve Chapelle.</p>

                            <div class="war-2-map-2">
                                <img src="assets/img/gallery/Querrien.jpg" width="100%" height="400px" class="img-fluid" alt="">
                                <p style="color: #D90000;">A Hotchkiss Gun Team practising near Querrien, July 1916</p>
                            </div>

                            <p>As the element of surprise was part of the plan of attack, the artillery, in the preceding days, attempted to inconspicuously register targets. On the morning of the attack, there was to be a 40-minute barrage to cut the wire and destroy the strong points. Preparations were made for the attack, which began early in the morning on 9 May 1915. The attacking Battalions were mowed down by German machine guns as soon as they crossed the parapet; and in the evening, the position of the trenches was identical to when the battle had commenced. However, there were massive casualties: in the Meerut Division alone, casualties included the loss of 69 British officers, 24 Indian officers, and 1,055 British and 823 Indian other ranks; and the Lahore Division lost 122 of all ranks, bringing the total to 2,093.</p>

                            <p>The Battle of Festubert involved a plan to advance in a south-easterly direction towards the town of La Bassée, along a front of 5,000 yards, with the Meerut Division and the British 2nd and 7th Divisions on converging lines. The Indian troops involved were from the Meerut and Lahore Divisions, with the former being tasked to undertake the attack and the latter to hold their front and support the attacking troops with small-arms fire and artillery. The attack was launched from the trenches south of the Rue du Bois at 2330 Hours on the night of 15 May 1915. The troops came under heavy artillery and machine gun fire from the opposing German side.</p>

                            <p>By midnight, it became clear that there was no possibility of success, yet a fresh assault was ordered by the brigade HQ to take place at 0300 Hours by the 2/3rd Gurkhas; this too came under heavy fire.</p>

                            <p>On the morning of 16 May 1915, bombardment of the German trenches resumed. At this stage, it was acknowledged that the defences in front of the Indian Corps were too strong to enable a breakthrough. It was then decided that the Indian troops should attack through the opening adjacent to them held by the 2nd Division. One section of the captured trench held by the 15th Sikhs was known as the ‘Glory Hole’. The two opposing sides were separated here only by a barricade. The 15th Sikhs spent the morning of 18 May resisting heavy bombing attacks, but by the early afternoon they were running short of bombs. At 1530 Hours, Lieutenant (Lt) J.G. Smyth of the 15th Sikhs was ordered to take a bombing party and two boxes of bombs from the old frontline to the captured trench—a distance of 250 yards over open ground. He selected 10 Indian bombers from those who volunteered—four from the 15th Sikhs, four from the 19th Punjabis and two from the 45th Sikhs. By the time Smyth got to a shell hole within 30 yards of their objective, he had only one man left with him—the rest had been killed or wounded—and one of the two boxes of bombs. Under covering fire, they finally reached the captured trench. Smyth was awarded the VC and all the men in his party were decorated for their bravery.</p>

                            <p>The overall result of the Battle of Festubert was mixed, but it must be acknowledged that the attacking troops, including the Indian Army, largely failed to reach their objectives and suffered severe casualties.</p>

                            <p>The Indian Corps last battle was one of four holding actions that took place at a distance from the main battle at Loos. The attack was notable in that it was the first time that British troops used gas, albeit not very successfully. The attack included explosion of a mine under the enemy parapet; a gas and smoke attack immediately prior to the attack; thick smoke barrages on each side of the attacking troops; a smoke screen along the front of the attack; and finally, field guns were placed in the front parapet to destroy the enemy’s parapet and machine guns. A day of gallant endeavour, some confusion and further heavy casualties—the Meerut Division suffered a total of over 3,000 killed, wounded and missing in the battle—ended with the British troops back in their start lines and no German reinforcements, other than local supports, had been drawn into the battle. Though no fault of its own, the Indian Corps failed to influence the outcome of the Battle of Loos.</p>

                            <p>The Indian Corps in France, at the battles of Ypres, Festubert and Neuve Chapelle, fought non-stop for over a year. They had reached the battlefields of France and Flanders in the month of October 1914 with strength of 26,000. By the time they were sent to a different theatre of war after a year, they had suffered 22,000 casualties, that is, they had lost nearly 100 per cent of their original strength. Table 2 lists the original order of battle of the IEF in France and Belgium. </p>


                            <div class="table-wrap">
                                <div class="box-table">
                                    <h6 style="font-size: 18px; font-weight: 500; color: #45464B;">Table 2 Order of Battle of the Original* Indian Army Corps (IEF A)</h6>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width:25%;"><b>Cavalry</b></th>
                                                <th scope="col" style="width:30%;"><b>Infantry</b></th>
                                                <th scope="col" style="width:30%;"><b>Imperial Service Troops </b></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">Mhow Division: <br>• 2nd Lancers</th>
                                                <td>Lahore Division:<br><br>FEROZEPORE BRIGADE:<br>
                                                    • 129th Baluchis<br>
                                                    • 57th (Wilde’s) Rifles (F.F.)<br>
                                                    • 89th Punjabis<br>
                                                    • 9th Bhopal Infantry<br><br>

                                                    JULLUNDUR BRIGADE:<br>
                                                    • 15th Ludhiana Sikhs<br>
                                                    • 40th Pathans<br>
                                                    • 47th Sikhs<br>
                                                    • 59th (Scinde) Rifles (F.F.)<br><br>

                                                    SIRHIND BRIGADE:<br>
                                                    • 1/1st Gurkha Rifles<br>
                                                    • 1/4th Gurkha Rifles<br>
                                                    • 27th Punjabis<br>
                                                    • 125th (Napier’s) Rifles<br><br>

                                                    DIVISIONAL TROOPS:<br>
                                                    • 15th Lancers<br>
                                                    • 23rd Sikh Pioneers<br>
                                                    • 34th Sikh Pioneers<br>
                                                    • 20th and 21st Field Companies, 3rd (Bombay) Sappers and Miners<br>
                                                    • Indian Field Ambulance<br><br>
                                                </td>
                                                <td>CAVALRY:<br>
                                                    • Alwar Lancers<br>
                                                    • 1st Gwalior Lancers<br>
                                                    • Indore Mounted Escort<br>
                                                    • 1st Jodhpur Lancers<br>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Meerut Division: <br><br>
                                                    MEERUT BRIGADE: <br>
                                                    • 3rd Horse<br>
                                                    • 16th Cavalry<br>
                                                    • 18th Tiwan Lancers<br><br>

                                                    DIVISIONAL CAVALRY:<br>
                                                    • 4th Cavalry<br>
                                                    • 11th Lancers<br>
                                                </th>

                                                <td>Meerut Division<br><br>

                                                    DEHRA DUN BRIGADE:<br>

                                                    • 1/9th Gurkha Rifles<br>
                                                    • 2/2nd Gurkha Rifles<br>
                                                    • 6th Jat Light Infantry<br>
                                                    • 28th Punjabis<br><br>

                                                    GARHWAL BRIGADE:<br>

                                                    • 2/3rd Gurkha Rifles<br>
                                                    • 1/39th Garhwal Rifles<br>
                                                    • 2/39th Garhwal Rifles<br><br>

                                                    BAREILLY BRIGADE:<br>

                                                    • 41st Dogras<br>
                                                    • 33rd Punjabis<br>
                                                    • 58th (Vaughan’s) Rifles (F.F.)<br>
                                                    • 69th Punjabis<br>
                                                    • 2/8th Gurkha Rifles<br><br>

                                                    DIVISIONAL TROOPS:<br>

                                                    • 4th Cavalry<br>
                                                    • 107th Pioneers<br>
                                                    • 2nd and 3rd Field Company,<br>
                                                    • Sappers and Miners<br>
                                                    • 4th and 5th Field Companies <br>
                                                    • 3rd (Bengal) Sappers and Miners<br>
                                                    • Indian Field Ambulance<br>
                                                    • Support Services: Supply and Transport Corps, Indian Hospital Corps, Indian Labour Corps<br>
                                                </td>
                                                <td>SAPPERS:<br>
                                                    • Maler Kotla Sappers<br>
                                                    • Tehri-Garhwal Sappers<br>

                                                </td>
                                            </tr>


                                            <tr>
                                                <th scope="row">Lahore Division:<br><br>

                                                    FEROZEPORE BRIGADE:<br><br>

                                                    • 7th Lancers<br><br>

                                                    JULLUNDUR BRIGADE:<br><br>

                                                    • 15th Lancers<br><br>

                                                    DIVISIONAL CAVALRY:<br><br>

                                                    • 23rd Cavalry (F.F)<br><br>

                                                    AMBALA BRIGADE:<br><br>

                                                    • 30th Lancers<br>
                                                </th>
                                                <td></td>
                                                <td>TRANSPORT:<br><br>

                                                    • Bharatpur Transport Corps<br>
                                                    • Gwalior Transport Corps<br>
                                                    • Indore Transport Corps<br>

                                                </td>
                                            </tr>



                                            <tr>
                                                <th scope="row">Poona Division:<br><br>

                                                    LUCKNOW BRIGADE:<br>
                                                    • 29th Lancers<br>
                                                </th>
                                                <td></td>
                                                <td></td>
                                            </tr>




                                            <tr>
                                                <th scope="row">1st Indian Cavalry Division:<br><br>

                                                    • 6th King Edward’s Own Cavalry<br>
                                                    • 9th Hodson’s Horse (3rd Ambala Cavalry Brigade)<br><br>

                                                    <!-- 39th Garhwali riflemen on the march in France, Estaire La Bassée Road. -->
                                                </th>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p style="font-size: 14px; font-weight: 400; color: #45464B;">Source: India and the Great War: France and Flanders, Booklet series, New Delhi: United Service Institution of India (USI), pp. 6–7.<br><br>Note: * other units also served subsequently.</p>
                                </div>
                            </div>

                        </div>

                    </div>


                    <div id="meso" class="accordion-details">
                        <h3>Mesopotamia</h3>

                        <div class="war-details">

                            <p>Before the war began, the British government was cognizant of the fact that its oil supplies were dependent on the security of the Persian oilfields. This was particularly important for the British Navy which had switched from coal to oil to power its ships. The oil supply remained vulnerable unless the oilfields were secured. The task of securing the Persian oilfields fell to the India Office in London as the troops that were used to guard the oilfields were from India. In September 1914, a small force was dispatched to ensure the security of the Anglo-Persian Oil Company’s oilfields and installations at Abadan, at the head of the Persian Gulf, and to show the flag to ensure the loyalty of the sheikdoms in the vicinity. Later, an IEF was sent from India to implement the second phase of the operation—an advance up the Shatt-al-Arab waterway. Success in the initial phases of the operation led to the capture of Basra. A further advance brought the force to the confluence of the Euphrates and the Tigris rivers.</p>
                            <!-- <div class="war-2-map-2">
                                <img src="assets/img/middle-eastern-map.png" width="100%" height="400px" class="img-fluid" alt="">
                                <p style="color: #D90000;">Middle Eastern Fronts (1914-1918)</p>
                            </div> -->

                            <p>In early 1915, an improvised Corps of two infantry divisions and a cavalry brigade was formed under General (Gen) Sir John Nixon of the Indian Army, which mounted offensives along each of the rivers. Major General Charles Townshend, General Officer Commanding (GOC) 6th Indian Division, moved up the less heavily defended Tigris and captured Kut-al-Amara in September 1915. These early successes raised British ambitions to capture Baghdad. However, they needed to capture Ctesiphon before Baghdad could be claimed. In November 1915, Ottoman forces occupied positions at Ctesiphon in order to block the British advance of the IEF 22 miles from Baghdad. In their eagerness to capture Baghdad, the British had overreached. Townshend’s plan for a night march, followed by a flank attack, broke down as they had gone beyond the range of the gunboats that were supporting them from the river. His troops reached the second Ottoman position inside Ctesiphon but could proceed no further due to lack of artillery support. By 22 November 1915, it became clear that the attack was a failure. Both sides faced heavy losses, but the British ultimately had to admit defeat. On 25 November 1915, Townshend’s forces began the retreat to Kut-al-Amara.</p><br>

                            <p>Townshend fell back at Kut-al-Amara and dug a strong defensive position in a loop of the Tigris, confident that he could sit it out until reinforcements reached him. But the Turks under the command of Gen Colmar von der Golt besieged Townshend’s British-Indian force from 7 December 1915. Attempts by relief forces to link up with the garrison failed. By January 1916, supplies were running low and the garrison resorted to slaughtering their horses for food. A fresh attempt on 16 April 1916 also failed, by which time the garrison was on starvation rations. Maj Gen Townshend was given the authority to negotiate surrender. The siege of Kut lasted five months and ended on 29 April 1916 with the surrender of British Indian forces in a humiliating defeat.
                            <p><br>

                            <p>Townshend fell back at Kut-al-Amara and dug a strong defensive position in a loop of the Tigris, confident that he could sit it out until reinforcements reached him. But the Turks under the command of Gen Colmar von der Golt besieged Townshend’s British-Indian force from 7 December 1915. Attempts by relief forces to link up with the garrison failed. By January 1916, supplies were running low and the garrison resorted to slaughtering their horses for food. A fresh attempt on 16 April 1916 also failed, by which time the garrison was on starvation rations. Maj Gen Townshend was given the authority to negotiate surrender. The siege of Kut lasted five months and ended on 29 April 1916 with the surrender of British Indian forces in a humiliating defeat.
                            <p>

                            <p>In August 1916, control of operations in Mesopotamia passed on to the War Office in London and Gen Sir Frederick Maude took over as Commander in Chief (C-in-C). Maude insisted on massive reinforcements and the establishment of an efficient logistics system before resuming the offensive in December 1916. The British retook Kut-al-Amara in February 1917 and entered Baghdad on 11 March 1917. The capture of Baghdad signalled the destruction of one of the main armies of Turkey and except for Palestine, the Turks no longer posed a serious threat to the Allies.</p>
                            <div class="war-2-map-2">
                                <img src="assets/img/gallery/Amara.jpg" width="100%" height="400px" class="img-fluid" alt="">
                                <p style="color: #D90000;">The 36th Sikhs (now 4 Sikh) in Kut al Amara after the town was abandoned by the Turks in February 1917 <br><strong>Source: Rana Chhina</strong></p>
                            </div>

                            <p>The campaign in Mesopotamia was fought mostly by the British Indian Army and Indian troops. Table 3 depicts the units of the Indian Army and Indian state forces that served in Mesopotamia during the war.
                            <p><br>

                            <div class="table-wrap">
                                <div class="box-table">
                                    <h6 style="font-size: 18px; font-weight: 500; color: #45464B;">Table 3 Units of the Indian Army and Indian State Forces that Served in Mesopotamia, 1914–18</h6>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width:25%; text-align: center;"><b>Indian Army</b></th>
                                                <th scope="col" style="width:30%; text-align: center;"><b>Indian State Forces</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">CAVALRY<br><br>
                                                    • 4th Cavalry<br>
                                                    • 5th Cavalry<br>
                                                    • 7th Hariana Lancers<br>
                                                    • 10th Lancers (Hodson’s Horse)<br>
                                                    • 11th KEO Lancers (Probyn’s Horse)<br>
                                                    • 12th Cavalry<br>
                                                    • 13th DCO Lancers (Watson’s Horse)<br>
                                                    • 14th Murray’s Jat Lancers<br>
                                                    • 15th Lancers (Cureton’s Multanis)<br>
                                                    • 16th Cavalry<br>
                                                    • 17th Cavalry<br>
                                                    • 21st PAVO Cavalry, Frontier Force (FF) (Daly’s Horse)<br>
                                                    • 22nd Sam Browne’s Cavalry (FF)<br>
                                                    • 32nd Lancers<br>
                                                    • 33rd QVO Light Cavalry<br>
                                                    • QVO Corps of Guides Cavalry<br>
                                                </th>
                                                <td>Indian State Forces<br>
                                                    • 1st Gwalior Lancers<br>
                                                    • Bharatpur Transport Corps<br>
                                                    • Bhavnagar Lancers<br>
                                                    • Gwalior Transport Corps<br>
                                                    • Indore Mounted Escort<br>
                                                    • Indore Transport Corps<br>
                                                    • Kashmir Lancers<br>
                                                    • Khairpur Mounted Rifles and Camel Corps<br>
                                                    • Maler Kotla Sappers<br>
                                                    • Mysore Transport Corps<br>
                                                    • Nabha Infantry<br>
                                                    • Patiala Lancers<br>
                                                    • Sirmoor Sappers<br>
                                                    • Tehri (Garhwal) Sappers<br>
                                                </td>

                                            </tr>

                                            <tr>
                                                <th scope="row">ARTILLERY<br><br>
                                                    • 21st Kohat Mountain Battery (FF)<br>
                                                    • 23rd Peshawar Mountain Battery (FF)<br>
                                                    • 25th Mountain Battery<br>
                                                    • 26th Jacob’s Mountain Battery<br>
                                                    • 30th Mountain Battery<br>
                                                    • 31st Mountain Battery<br>
                                                    • 34th (Reserve) Mountain Battery<br>
                                                    • 49th (Reserve) Mountain Battery<br>
                                                    • 50th (Reserve) Mountain Battery<br>
                                                </th>

                                                <td></td>

                                            </tr>


                                            <tr>
                                                <th scope="row">SAPPERS AND MINERS<br><br>

                                                    • 1st KGO Sappers and Miners<br>
                                                    • 2nd QVO Sappers and Miners<br>
                                                    • 3rd Sappers and Miners.<br>
                                                </th>
                                                <td></td>

                                            </tr>



                                            <tr>
                                                <th scope="row">INFANTRY AND PIONEERS<br>

                                                    • 1-1st Brahmins<br>
                                                    • 1-2nd QO Rajput Light Infantry<br>
                                                    • 1-3rd Brahmins<br>
                                                    • 1-4th PAVO Rajputs<br>
                                                    • 1-6th Jat Light Infantry<br>
                                                    • 1-7th DCO Rajputs<br>
                                                    • 1-8th Rajputs<br>
                                                    • 1-9, 2-9, 3-9 Bhopal Infantry<br>
                                                    • 1-10th Jats<br>
                                                    • 1-11th Rajputs<br>
                                                    • 13th Rajputs<br>
                                                    • 14th KGO Ferozepore Sikhs<br>
                                                    • 20th DCO Infantry (Brownlow’s Punjabis)<br>
                                                    • 1-22nd Punjabis<br>
                                                    • 24th Punjabis<br>
                                                    • 1-25th Punjabis<br>
                                                    • 1-26th Punjabis<br>
                                                    • 1-27th Punjabis<br>
                                                    • 1-28th Punjabis<br>
                                                    • 31st Punjabis<br>
                                                    • 1-32nd Sikh Pioneers<br>
                                                    • 1-34th Sikh Pioneers<br>
                                                    • 36th Sikhs<br>
                                                    • 37th Dogras<br>
                                                    • 1-39th Garhwal Rifles<br>
                                                    • 1-41st Dogras<br>
                                                    • 1-42nd Deoli Regiment<br>
                                                    • 1-43rd Erinpura Regiment<br>
                                                    • 1-44th Merwara Infantry<br>
                                                    • 45th Rattray’s Sikhs<br>
                                                    • 47th Sikhs<br>
                                                    • 1-48th Pioneers<br>
                                                    • 49th Bengalis<br>
                                                    • 51st Sikhs (FF)<br>
                                                    • 52nd Sikhs (FF)<br>
                                                    • 53rd Sikhs (FF)<br>
                                                    • 1-56th Punjabi Rifles (FF)<br>
                                                    • 59th Scinde Rifles (FF)<br>
                                                    • 62nd Punjabis<br>
                                                    • 64th Pioneers<br>
                                                    • 1-66th Punjabis<br>
                                                    • 1-67th Punjabis<br>
                                                    • 1-73rd Carnatic Infantry<br>
                                                    • 1-76th Punjabis<br>
                                                    • 79th Carnatic Infantry<br>
                                                    • 1-80th Carnatic Infantry<br>
                                                    • 82nd Punjabis<br>
                                                    • 83rd Wallajahabad Light Infantry<br>
                                                    • 84th Punjabis<br>
                                                    • 85th Burman Rifles<br>
                                                    • 87th Punjabis<br>
                                                    • 1-88th Carnatic Infantry<br>
                                                    • 1-89th Punjabis<br>
                                                    • 1-90th Punjabis<br>
                                                    • 1-91st Punjabis<br>
                                                    • 92nd Punjabis<br>
                                                    • 93rd Burma Infantry<br>
                                                    • 1-94th Russell’s Infantry<br>
                                                    • 1-95th Russell’s Infantry<br>
                                                    • 1-96th Berar Infantry<br>
                                                    • 1-97th Deccan Infantry<br>
                                                    • 1-99th Deccan Infantry<br>
                                                    • 1-102nd KEO Grenadiers<br>
                                                    • 1-103rd Mahratta Light Infantry<br>
                                                    • 104th Wellesley’s Rifles<br>
                                                    • 1-10th and 2-10th Gurkha Rifles<br>
                                                    • 1-11th, 2-11th and 3-11th Gurkha Rifles<br>
                                                    • 1st QVO Corps of Guides Infantry<br>
                                                </th>
                                                <td></td>

                                            </tr>




                                            <tr>
                                                <th scope="row">LOGISTIC AND SUPPORT UNITS<br><br>

                                                    • Indian Medical Service<br>
                                                    • Indian Subordinate Medical Department<br>
                                                    • Army Bearer Corps<br>
                                                    • Army Hospital Corps<br>
                                                    • Survey Department<br>
                                                    • Military Accounts Department<br>
                                                    • Army Remount Department<br>
                                                    • Indian Army Postal Service<br>
                                                    • Indian Telegraph Department<br>
                                                    • Supply and Transport Corps<br>
                                                    • Indian Ordnance Department<br>
                                                    • Indian Corps of Clerks<br>
                                                </th>
                                                <td></td>

                                            </tr>
                                        </tbody>
                                    </table>
                                    <p style="font-size: 14px; font-weight: 400; color: #45464B;">Source: India and the Great War: France and Flanders, pp. 6–9.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="africa" class="accordion-details">
                        <h3>East Africa </h3>

                        <div class="war-details">


                            <p>The Germans entered the race for colonies after the unification of Germany in 1871. One of the few large German colonies was in German East Africa, in what is now modern-day Tanzania. With limited road and rail infrastructure, the country was ideal for defence. The Germans were commanded by Gen Paul von Lettow-Vorbeck, who, with a force of some 16,000 local troops—known as the Askaris—kept the British at bay for four years during World War I.</p><br>

                            <p>Military operations in East Africa (IEF B) were officially acknowledged to be a ‘subsidiary enterprise’, almost wholly fought by Indian and African forces rein- forced by troops from South Africa and Rhodesia. The campaign was characterised by extreme privations suffered by men and animals alike, in terrain varying from lowland coastal areas to higher ground inland, alternating between heavy rains and seasons of drought. The terrain proved difficult for the conduct of military operations and there were more casualties due to disease than battle. The outbreak of war had found the colonial authorities in Uganda and British East Africa totally unprepared. As in every previous emergency in East Africa, troops required to supplement local forces had to come from India.</p><br>

                            <p>The two IEFs in East Africa had the following roles:</p><br>

                            <p>1. IEF B was to embark for the capture of the port of Dar es Salaam, a potential hostile naval base; and</p>
                            <p>2. IEF C was to reinforce the small garrison of British East Africa.</p>

                            <p>The objective of IEF B was immediately changed to an amphibious landing to capture the German port of Tanga. The landings at Tanga and the subsequent attempts to capture the town were mismanaged from the start and were a fiasco. The 101st Grenadiers and the 2nd Jammu and Kashmir Rifles were amongst the units that saved the day, with the latter having captured two German trench lines and then covered the withdrawal.</p><br>

                            <p>On 31 December 1914, both IEFs (B and C) were amalgamated and the combined force designated as IEF B. The force was redistributed in two commands: the Mombasa Area, extending up to Kilimanjaro; and the Nairobi Area, including Uganda. The campaign devolved into a series of several small-scale battles and skirmishes in 1914 and 1915, later growing into a multi-divisional theatre of operations with the appointment of South African Gen Jan Smuts. Smuts forces slowly pushed the Germans back to Dar es Salaam.</p><br>

                            <p>Building roads and bridges behind the advance were the engineers including the Faridkot Field Company, officered entirely by Sikhs at a time when there were no other Indians holding commissioned ranks. There were also railway companies of the Indian Sappers repairing and running the railway.</p><br>

                            <p>Although the strength of the Indian forces in East Africa never went beyond 15,000 at any one time, three times that number were sent from India during the course of the campaign. The war in East Africa lasted from 1914 to 1918. The key battles in 1917 were fought around Kilwa and Lindi, near the coast, where the Germans suffered a number of casualties. Von Lettow-Vorbeck captured Namakura in July 1917 and entered Rhodesia in November 1918. On 12 November 1918, the last battle of the war in East Africa was fought between Lettow-Vorbek’s troops and the King’s African Rifles, with Lettow-Vorbeck surrendering formally on the 25 November 1918.</p><br>


                            <p>The number of Indian casualties in East Africa amounted to 5,018, of which 2,972 were killed, 2,003 were wounded and 43 were declared missing. Table 4 lists the units of the Indian Army and Indian volunteer and state forces that served in East Africa. </p><br>

                            <div class="table-wrap">
                                <div class="box-table">
                                    <h6 style="font-size: 18px; font-weight: 500; color: #45464B;">Table 4 Units of the Indian Army, Indian Volunteer and Indian State Forces that Served in East Africa</h6>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width:25%; text-align: center;"><b>Indian Army</b></th>
                                                <th scope="col" style="width:30%; text-align: center;"><b>Indian Volunteer Forces</b></th>
                                                <th scope="col" style="width:30%; text-align: center;"><b>Indian State Forces</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">CAVALRY<br><br>
                                                    • Squadron, 17th Cavalry (January 1915–January 1917)<br>
                                                    • 25th Cavalry (FF) (August 1917–February 1918)<br>

                                                </th>

                                                <td>Artillery:<br>
                                                    • 1st Kashmir Mountain Battery (December 1916–February 1918)
                                                </td>

                                                <td>• Calcutta Volunteer Battery, formed from the Cossipore Artillery Volunteers (September 1914–mid-1917)<br><br>

                                                    • Indian Volunteer Maxim Company (September 1914–mid-1917)<br><br>

                                                    • North-West Railway Volunteers Machine Gun Company (November 1914–early 1918)<br><br>
                                                </td>

                                            </tr>

                                            <tr>
                                                <th scope="row">ARTILLERY<br><br>
                                                    • 22nd Derajat Mountain Battery (FF) (December 1916–November 1918)<br><br>
                                                    • 24th Hazara Mountain Battery (FF) (April 1917–August 1918)
                                                    <br><br>
                                                    • 27th (Bengal) Mountain Battery (September 1914–January 1918)
                                                    <br><br>
                                                    • 28th (Lahore) Mountain Battery (November 1914–January 1917)
                                                </th>

                                                <td>Sappers & Miners:<br><br>

                                                    • 1st Faridkot Field Company, Sappers & Miners (November 1917–February 1918)
                                                </td>
                                                <td></td>

                                            </tr>


                                            <tr>
                                                <th scope="row"> SAPPERS & MINERS<br><br>

                                                    • 14th Company, 2nd QVO Sappers & Miners (February–November 1918)<br><br>

                                                    • 25th Railway Company, Sappers & Miners (November 1914–December 1917)<br><br>

                                                    • 26th Railway Company, Sappers & Miners (November 1914–December 1917)<br><br>

                                                    • 27th Railway Company, Sappers & Miners (April 1916–April 1918)<br><br>

                                                    • 28th Railway Company, Sappers & Miners (April 1916–September 1918)<br><br>

                                                    • No. 5 Pontoon Park, Bombay Sappers & Miners<br><br>

                                                    • No. 4 Engineer Field Park, Madras Sappers & Miners<br><br>

                                                    • No. 3 Photo-Litho Section, Madras Sappers & Miners<br><br>

                                                    • No. 4 Printing Section, Madras Sappers & Miners<br><br>
                                                </th>
                                                <td>
                                                    Infantry:<br><br>

                                                    • Bharatpur Imperial Service Infantry (September 1914–December 1917)<br><br>

                                                    • 3rd Gwalior Maharajah Scindia’s Own Battalion (November 1914–December 1917)<br><br>

                                                    • Jind Imperial Service Infantry (September 1914–January 1918)<br><br>

                                                    • Kapurthala Jagatjit Infantry (September 1914–December 1917)<br><br>

                                                    • 2nd Kashmir Imperial Service Rifles (November 1914–May 1917)
                                                    <br><br>
                                                    • 3rd Kashmir Rifles (Raghunath Regiment) (November 1914–May 1917)<br><br>

                                                    • 1st Rampur Imperial Service Infantry (September 1914–January 1918)<br><br>
                                                </td>

                                                <td></td>

                                            </tr>



                                            <tr>
                                                <th scope="row">INFANTRY<br><br>

                                                    • 5th Light Infantry (March 1916–June 1918)<br><br>

                                                    • 13th Rajputs (Shekhawati Regiment) (November 1914–December 1915)<br><br>

                                                    • 17th Infantry (Loyal Regiment) (February 1916–December 1917)<br><br>

                                                    • 29th Punjabis (September 1914–May 1917)<br><br>

                                                    • 30th Punjabis (December 1916–December 1917)<br><br>
                                                    • 33rd Punjabis (May 1917–February 1918)<br><br>
                                                    • 40th Pathans (January 1916–February 1918)<br><br>

                                                    • 55th Coke’s Rifles (FF) (August 1917–February 1918)<br><br>

                                                    • 57th Wilde’s Rifles (FF) (July 1916–September 1917)<br><br>

                                                    • 58th Vaughan’s Rifles (FF), one company (February 1918–mid-1918)<br><br>

                                                    • 61st (King George’s Own) Pioneers (November 1914–February 1918)<br><br>

                                                    • 63rd Palmacottah Light Infantry (November 1914–January 1917)<br><br>

                                                    • 98th Infantry (November 1914–January 1917)<br><br>

                                                    • 101st Grenadiers (November 1914–August 1916)<br><br>

                                                    • 127th (Queen Mary’s Own) Baluch Light Infantry (August 1917–February 1918)<br><br>

                                                    • 129th Duke of Connaught’s Own Baluchis (January 1916–January 1918)<br><br>

                                                    • 130th (King George’s Own) Baluchis (Jacob’s Rifles) (January 1915–October 1917)<br><br>
                                                </th>
                                                <td></td>
                                                <td></td>

                                            </tr>





                                        </tbody>
                                    </table>
                                    <h6 style="font-size: 14px; font-weight: 400; color: #45464B;">Source: India and the Great War: East Africa, Booklet series, New Delhi: USI, pp. 6–7.</h6>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="palestine" class="accordion-details">
                        <h3>Egypt and Palestine</h3>
                        <div class="war-details">
                            <p>The entry of Ottoman Turkey into the war in November 1914, on the side of Germany, immediately posed a threat to the oilfields in southern Persia and to the vital Suez Canal. The campaigns in Egypt, Palestine and Syria were thus aimed at driving the Turks out of Palestine and Syria and protecting the Suez Canal.</p>

                            <p>The initial defence of the Suez Canal was provided by the Sirhind Brigade of the 3rd Indian Division (Lahore Division), which was detached along with a battery of mountain artillery from the division as it passed through on its way to France; later, it rejoined its parent division in France. By December 1914, the Indian forces in Egypt had built up to two divisions that is, the 10th and the 11th, and included the Imperial Service Cavalry Brigade of state force units, the Bikaner Camel Corps and three batteries of mountain artillery. The 10th Division was made responsible for the defence of the Suez Canal. The first Turkish effort to attack the canal was in January 1915, when a force of about 20,000 men advanced towards the canal. The Turks attempted to cross the canal on 3–4 February 1915, but failed and withdrew to Beersheba. The rest of the year remained quiet for the forces in Egypt and subsequently, one brigade was pulled out and sent to Gallipoli, while the others moved to Basra and Aden.</p>

                            <p>In March 1916, Gen Archibald Murray took command of the Egyptian Expeditionary Force. Not content with a passive defence of the Suez Canal, he decided to advance into the Sinai, simultaneously building a railway line behind his forces as they progressed. The Turks attempted to delay this advance and between April–July 1916, a number of battles were fought forcing the Turks to fall back. By 9 January 1917, the Sinai Peninsula was cleared and in British hands. On 26 March 1917, an attempt was made to capture Gaza, but a strong Turk counter-attack drove the British troops back. A second effort was made on 19 April 1917, but it too failed.</p>

                            <p>Thereafter, Gen Sir Edmund Allenby was appointed to command the Egyptian Expeditionary Force, with a directive to invade Palestine and capture Jerusalem before the end of the year. With direct assaults on Gaza having failed, Allenby decided to capture Beersheba first, and then roll up the Gaza defences from that direction. The summer months were spent in improving transport and administrative arrangements. An elaborate deception plan was employed to lull the Turks into believing that Gaza remained the main objective of the British. The attack was launched on the night of 30–31 October 1917 and Beersheba fell by nightfall on 31 October. Thereafter, Gaza fell on 7 November 1917. The attack on Jerusalem was pulled up and took place on 8 December 1917. By 11 December 1917, Gen Allenby entered Jerusalem. While the attack on Jerusalem was in progress, the XXI Corps on the coast advanced to and captured Jaffa, thus securing the seaport that would aid the receipt of supplies and reinforcements. The next phase of operations aimed at occupying the Jordan Valley and was completed by May 1918.</p>

                            <div class="war-2-map-2">
                                <img src="assets/img/gallery/General-Allenby.jpg" width="100%" height="400px" class="img-fluid" alt="">
                                <p style="color: #D90000;">Indian soldiers provide a guard of honour for General Allenby near the Jaffa Gate <br><strong>Source: Library of Congress</strong></p>
                            </div>

                            <p>During this period, the Egyptian Expeditionary Force was reorganised, becoming primarily an Indian force comprising of the 3rd and 7th Indian Divisions, the 4th and 5th Indian Cavalry Divisions and some 24 other Indian Battalions.</p>

                            <p>The Allied campaign in Syria commenced in September 1918. The XXXI Corps was tasked to break through the Turkish defences, and the 4th and 5th Indian Cavalry Divisions were to break out from the gap created. The attack commenced at 0430 Hours on 19 September 1918, and by 0545 Hours, the leading troops had broken through the Turkish defences. The 4th and 5th Cavalry Divisions were immediately launched and the former advanced a 100 miles in 24 hours to secure El Afule. The 13 Cavalry Brigade of the 5th Division reached Nazareth on 20 September. On 22 September 1918, the troops reached Haifa and the only way in was through a narrow defile. The 15 Imperial Service Brigade was in the lead here, with only two units, the Mysore and Jodhpur Lancers. The brigade launched a mounted attack on the Turks holding the defile and broke through and took the town. The Battle of Haifa was one of the last cavalry charges into defences which was conducted using horses. On 26 September 1918, orders were issued for the advance to Damascus and the troops reached the city on 30 September 1918. Thereafter, the troops advanced to Aleppo, 200 miles from Damascus, and captured it by 25 October 1918, just before the end of World War I.</p>
                            <!-- 
                            <p>Table 5 lists the Indian Army and Indian state forces units that served in Egypt and Palestine during the Great War.</p> -->


                            <div class="table-wrap">
                                <div class="box-table">
                                    <h6 style="font-size: 18px; font-weight: 500; color: #45464B;">Table 4 Indian Army and Indian State Forces Units that Served in Egypt and Palestine During the Great War</h6>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width:25%; text-align: center;"><b>Indian Army</b></th>
                                                <th scope="col" style="width:30%; text-align: center;"><b>Indian State Forces</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">INDIAN ARMY CAVALRY<br><br>

                                                    • 2nd Lancers (Gardner’s Horse)<br>
                                                    • 6th King Edward’s Cavalry<br>
                                                    • 9th Hodson’s Horse<br>
                                                    • 18th (King George’s Own) Lancers<br>
                                                    • 19th Lancers (Fane’s Horse)<br>
                                                    • 20th Deccan Horse<br>
                                                    • 29th Lancers (Deccan Horse)<br>
                                                    • 34th Prince Albert Victor’s Own Poona Horse<br>
                                                    • 36th Jacob’s Horse<br>
                                                    • 38th (King George’s Own) Central India Horse<br>

                                                </th>

                                                <td>INDIAN STATE FORCES CAVALRY AND CAMEL CORPS<br><br>

                                                    • Bhavnagar Lancers (attached to the Mysore Imperial Service Lancers).<br>
                                                    • Bikaner Camel Corps (Ganga Risala)<br>
                                                    • Hyderabad Imperial Service Lancers<br>
                                                    • Jodhpur Imperial Service Lancers<br>
                                                    • Junagadh Lancers (specialist signallers)<br>
                                                    • Kashmir Lancers (attached to the Mysore and Patiala Imperial Service Lancers)<br>
                                                    • Mysore Imperial Service Lancers<br>
                                                    • Nawanagar Lancers (specialist signallers)<br>
                                                    • 1st Patiala (Rajindra) Lancers<br>
                                                    • Ratlam Imperial Service Despatch Riders<br>
                                                </td>



                                            </tr>

                                            <tr>
                                                <th scope="row">INDIAN ARMY MOUNTAIN ARTILLERY<br><br>

                                                    • 21st Kohat Mountain Battery (Frontier Force)<br>
                                                    • 26th Jacob’s Mountain Battery.<br>
                                                    • 29th Mountain Battery<br>
                                                    • 32nd Mountain Battery<br>
                                                    • 38th (Reserve) Mountain Battery<br>
                                                    • 39th (Reserve) Mountain Battery<br>


                                                </th>

                                                <td></td>


                                            </tr>


                                            <tr>
                                                <th scope="row">INDIAN ARMY SAPPERS & MINERS<br><br>

                                                    • 1st (King George’s Own) Sappers and Miners<br>
                                                    • 2nd Queen’s Own Sappers and Miners<br>
                                                    • 3rd Sappers and Miners<br>
                                                    • No. 25 Railway Company, Sappers and Miners<br>
                                                    • No. 29 Railway Company, Sappers and Miners<br>
                                                    • 121st Pioneers<br>
                                                    • 1st Battalion, 123rd Outram’s Rifles<br>
                                                    • 2nd Battalion, 124th Duchess of Connaught’s Own <br>Baluchistan Infantry<br>
                                                    • 1st Battalion, 125th Napier’s Rifles<br>
                                                    • 126th Baluchistan Infantry<br>
                                                    • 2nd Battalion, 127th (Queen Mary’s Own) Baluch Light Infantry<br>
                                                    • 1st Battalion, 128th Pioneers<br>
                                                    • 1st Battalion, 129th Duke of Connaught’s Own Baluchis<br>
                                                    • 1st Battalion, 130th (King George’s Own) Baluchis (Jacob’s Rifles)<br>
                                                    • 2nd Battalion, 151st Indian Infantry3rd Battalion, 151st Punjabi Rifles<br>
                                                    • 1st, 2nd and 3rd Battalions, 152nd Punjabis<br>
                                                    • 1st and 2nd Battalions, 153rd Punjabis.<br>
                                                    • 3rd Battalion, 153rd Rifle.<br>
                                                    • 2nd and 3rd Battalions, 154th Indian Infantry<br>
                                                    • 1st Battalion, 155th Indian Pioneers<br>
                                                    • 2nd Battalion, 155th Indian Infantry<br>
                                                    • 1st Battalion, 1st (King George’s Own) Gurkha Rifles (Malaun Regiment)<br>
                                                    • 2nd Battalion, 2nd King Edward’s Own Gurkha Rifles (Sirmoor Rifles)<br>
                                                    • 2nd and 3rd Battalions, 3rd Queen Alexandra’s Own Gurkha Rifles<br>
                                                    • 1st Battalion, 4th Gurkha Rifles<br>
                                                    • 1st Battalion, 5th Gurkha Rifles (FF)<br>
                                                    • 1st Battalion, 6th Gurkha Rifles<br>
                                                    • 2nd Battalion, 7th Gurkha Rifles<br>
                                                    • 1st and 2nd Battalions, 8th Gurkha Rifles<br>
                                                    • 2nd Battalion, 10th Gurkha Rifles<br>
                                                    • 4th Battalion, 11th Gurkha Rifles<br>
                                                    • 1st and 2nd Battalions, Queen Victoria’s Own Corps of Guides (FF) (Lumsden’s)
                                                </th>
                                                <td>
                                                    INDIAN STATE FORCES SAPPERS & MINERS<br><br>

                                                    • Tehri (Garhwal) Sappers

                                                </td>

                                            </tr>



                                            <tr>
                                                <th scope="row">INDIAN ARMY INFANTRY AND SUPPORT UNITS:<br><br>

                                                    • Indian Medical Service <br>
                                                    • Indian Subordinate Medical Department<br>
                                                    • Indian Army Bearer Corps<br>
                                                    • Indian Mule Corp.<br>
                                                    • 70th Hired Camel Corps<br>
                                                    • Indian Supply and Transport Corps<br>
                                                    • Indian Army Ordnance Department<br>
                                                    • Indian Army Postal Service<br>

                                                </th>
                                                <td>
                                                    INDIAN STATE FORCES INFANTRY<br><br>

                                                    • Alwar Imperial Service Infantry (Jey Paltan)<br>
                                                    • Bahawalpur Mounted Rifles<br>
                                                    • 4th Maharaja Bahadur Battalion Gwalior Infantry<br>
                                                    • Khairpur Imperial Service Mounted Rifles<br>
                                                    • 1st Kashmir Infantry (Raghupratap Regiment)<br>
                                                    • 3rd Kashmir Rifles (Raghunath Regiment)<br>
                                                    • Patiala Imperial Service Infantry<br>
                                                </td>

                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                    INDIAN ARMY LOGISTIC AND SUPPORT UNITS<br><br>

                                                    • Indian Medical Service<br>
                                                    • Indian Subordinate Medical Department<br>
                                                    • Indian Army Bearer Corps<br>
                                                    • Indian Mule Corp.<br>
                                                    • 70th Hired Camel Corps<br>
                                                    • Indian Supply and Transport Corps<br>
                                                    • Indian Army Ordnance Department<br>
                                                    • Indian Army Postal Service<br>
                                                </th>

                                                <td></td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <h6 style="font-size: 14px; font-weight: 400; color: #45464B;">Source: India and the Great War: Egypt and Palestine, Booklet series, New Delhi: USI, pp. 14–19.</h6>
                                </div>
                            </div>


                        </div>

                    </div>

                    <div id="gallipoli" class="accordion-details">
                        <h3>Gallipoli</h3>

                        <div class="war-details">

                            <p>In November 1914, Ottoman Turkey entered the war on the side of Germany, against Britain and its ally, Russia. This move immediately threatened British interests in the Middle East, especially the vital Suez Canal.</p>
                            <br>
                            <p>Turkish troops…in January 1915 actually attacked [the Suez Canal], and British India became embroiled in what proved to be a long and costly campaign in Mesopotamia, an Ottoman province. Russian requests for help in its winter campaigns against the Ottomans in the Caucasus prompted British strategists Winston Churchill and Lord Kitchener to propose a naval operation against the Ottoman capital.27</p><br>

                            <p>The naval operation that intended to force a passage through the Dardanelles for British and French warships to reach Constantinople and force Ottoman Turkey’s surrender, thereby ending the war in the Middle East, failed. This failure led to the idea of landing the Mediterranean Expeditionary Force (MEF) on the Gallipoli Peninsula to capture it. However, the failed naval operation had alerted the Turks who reinforced their troops on Gallipoli to four divisions.
                            <p><br>

                            <p>The invasion of Gallipoli in April 1915 marked the start of a disastrous campaign and by the end of the year, a decision was taken to end the campaign and withdraw the remaining troops. The army which landed in Gallipoli comprised troops from across the British and French empires, including the Australian and New Zealand Army Corps (ANZAC) and Indian troops.
                            <p>

                            <p>The Indian expeditionary force that served in the Dardanelles was not very large in numbers; barely 5,000 men in a campaign that swelled from 75,000, to nearly half a million allied troops engaged by the end of the campaign. Yet it had a significant impact upon the course of the operations, and no account of the campaign can ignore the contribution of the 14th Sikhs in the Third Battle of Krithia on the 4th of June, or the 1-6th Gurkhas in the climactic Battle of Sari Bair on the 9th of August 1915.
                            <p><br>

                            <div class="war-2-map-2">
                                <img src="assets/img/gallery/Gallipoli.jpg" width="100%" height="400px" class="img-fluid" alt="">
                                <p style="color: #D90000;">Indian stretcher bearers at Gallipoli, 1915</p>
                            </div>

                            <p>The expeditionary force at Gallipoli comprised of the 7th Indian Mountain Artillery Brigade, the Indian mule Corps, a medical establishment and the 29th Indian Infantry Brigade. It ‘reflected the composition of the Indian Army at the time, largely drawn from what were regarded the “martial” peoples of India—Sikhs and Muslims…from the Punjab and especially the Gurkhas. ’ As Indian Muslims considered the Ottoman Sultan as the Caliph, two Battalions of Punjabi Muslims were replaced by the Gurkhas. A detailed list of Indian Army units that participated in the Gallipoli campaign is given in Table 6.
                            <p><br>


                            <div class="table-wrap">
                                <div class="box-table">
                                    <h6 style="font-size: 18px; font-weight: 500; color: #45464B;">Table 6 Units of the Indian Army that served in the Dardanelles Campaign, 1915</h6>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width:25%; text-align: center;"><b>Fighting Units</b></th>
                                                <th scope="col" style="width:30%; text-align: center;"><b>Administrative Units</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">7TH INDIAN MOUNTAIN ARTILLERY BRIGADE<br><br>

                                                    Headquarters<br><br>

                                                    • 21st (Kohat) Mountain Battery (FF)<br>
                                                    • 26th (Jacob’s) Mountain Battery<br>
                                                    • Mountain Artillery Section, Divisional Ammunition Column<br>
                                                </th>

                                                <td>TRANSPORT<br><br>

                                                    • Indian Mule Cart Train, comprising the following:<br><br>

                                                    • Mule Corps (each a complete corps): 1st, 2nd, 9th, 11th, 15th, 28th, 31st and 32nd<br>
                                                    • Mule Corps (detachments from): 3rd, 6th, 7th, 8th, 10th, 12th, 14th, 18th, 19th, 20th, 21st, 22nd, 23rd, 24th, 26th, 27th, 29th, 33rd, 34th, 35th, 36th and 37th.<br><br>

                                                    Detachments from:<br><br>

                                                    • Gwalior Imperial Service Transport Corps<br>
                                                    • Bharatpur Imperial Service Transport Corps<br>
                                                    • Indore (Holkar’s) Imperial Service Transport Corps<br>

                                                </td>



                                            </tr>

                                            <tr>
                                                <th scope="row">29TH INDIAN INFANTRY BRIGADE Headquarters<br><br>

                                                    • 14th King George’s Own Ferozepore Sikhs<br>
                                                    • 69th Punjabis<br>
                                                    • 89th Punjabis<br>
                                                    • Rajindra Sikhs<br>
                                                    • 1-5th Gurkha Rifles (FF)<br>
                                                    • 1-6th Gurkha Rifles<br>
                                                    • 2-10th Gurkha Rifles<br>
                                                    • 1-4th Gurkha Rifles and details<br>
                                                    • 2 Double Companies, Patiala Imperial Service Infantry (attached 14th Sikhs)<br>
                                                </th>

                                                <td>HOSPITALS<br><br>

                                                    • 108th Indian Field Ambulance
                                                    (with 29th Indian Infantry Brigade)<br>
                                                    • 110th Indian Field Ambulance
                                                    (Clearing Hospital) (with convalescent depot)<br>
                                                    • ‘C’ Section, (Indian) 137th Combined Field Ambulance (with Indian Mountain Artillery Brigade)<br>
                                                    • ‘C’ Section, (British) 137th Combined Field Ambulance (on a transport).<br>
                                                </td>


                                            </tr>


                                            <tr>
                                                <th scope="row"></th>
                                                <td>
                                                    POSTAL<br><br>

                                                    • Indian Army Postal Service (IAPS), Field Post Office (FPO) No. 34
                                                </td>

                                            </tr>




                                        </tbody>
                                    </table>
                                    <p style="font-size: 14px; font-weight: 400; color: #45464B;">Source: India and the Great War: Gallipoli, Booklet series, New Delhi: USI, pp. 6–7.</p>
                                </div>
                            </div>


                            <p>Two Indian mountain batteries (21st [Kohat] and 26th [Jacob’s]) accompanied the ANZAC on 25 April 1915, the start of the campaign. They were joined by the 29th Indian Infantry Brigade on 1 May 1915.</p>

                            <p>The Indian gunners proved their worth by stopping heavy Turkish assaults on the ANZAC positions. They saw their heaviest action on May 19…At the precariously located Quinn’s post the Indian guns fought it out with Turkish guns only 350 yards away and managed to silence them…and Australian defenders freely admitted that the strategic post could not have been held without the Indian artillery.</p>

                            <p>On Cape Helles, Indian troops fought with British forces. In the Third Battle of Krithia, the 14th Sikhs ‘…attacked the heavily defended Turkish position at Gully Ravine’ on 4 June 1915, and the Battalion lost ‘82 per cent of the men engaged in battle.’</p>

                            <p>The MEF was designated as the IEF G in July 1915, and took part in a fresh effort to break the stalemate with a planned assault on Sari Bair and a landing at Sulva Bay. ‘The column’s movement from Anzac Cove to the ridge was done at night over rocky terrain and steep gullies with no reconnaissance…’ causing confusion and delay, and allowing the Turks to reinforce the strategic heights. After the defeat at Sari Bair, by November 2015, orders were issued to evacuate troops from the peninsula and by mid-December, the Indian troops withdrew from Gallipoli. Out of 57,000 dead, the Indians lost 1,600 in the campaign.</p>


                        </div>



                    </div>



                    <!-- <div id="note" class="accordion-details">
                        <h3>Notes</h3>

                        <div class="war-details">
                            <ol>
                                <li>Ibid.</li>
                                <li>Pradeep Barua, font-size: 14px;The Late Colonial Indian Army: From the Afghan Wars to the Second World Warfont-size: 14px;, Lanham, MD: Lexington Books, 2021, Kindle edition.</li>
                                <li>Corrigan, font-size: 14px;Sepoys in the Trenches: The Indian Corps on the Western Front 1914–15font-size: 14px;, p. ix, quoted in Roy, font-size: 14px;The Indian Army and the First World War: 1914–1918font-size: 14px;, n. 5.</li>
                                <li>War Diary IEFA Army Headquarters, India, 1–31 January 1916, Vol. 18, Appendix 44, from Deputy Adjutant General, Indian Section, Rouen, to the War Section, Army Headquarters, Telegram, 25 January 1916, WWI/176/H; Willcocks, font-size: 14px;With the Indians in Francefont-size: 14px;, p. 9, quoted in Roy, font-size: 14px;The Indian Army and the First World War: 1914–1918font-size: 14px;, n. 5.</li>
                                <li>This section is adapted from Maj Gen Ian Cardozo, AVSM, SM, font-size: 14px;The Indian Army in World War I: 1914–1918font-size: 14px;, New Delhi: USI and Manohar, pp. 94–158.</li>
                                <li>The Schlieffen Plan was a battle plan drawn up by Count von Schlieffen, a German military strategist. It sought to ensure German military victory in the event of a two-front war against both France and Russia, with the aim of invading France from the north (and avoiding fortifications along the Franco-German border, and capturing Paris, thereby ensuring its capitulation within a short span of two months). The plan formed the basis of Germany’s offensive in August 1914, with limited success.</li>
                                <li>Simon Doherty and Tom Donovan, font-size: 14px;The Indian Corps on the Western Front: A Handbook and Battlefield Guidefont-size: 14px;, New Delhi: Centre for Armed Forces Historical Research, USI, 2014, p. 47.</li>
                                <li>Cardozo, font-size: 14px;The Indian Army in World War I: 1914–1918font-size: 14px;, n. 17, p. 102.</li>
                                <li>Peter Simkins, Geoffrey Jukes and Michael Hickey, font-size: 14px;The First World War: The War to End All Warsfont-size: 14px;, UK: Osprey Publishing, 2013, p. 93.</li>
                                <li>This section is adapted from Cardozo, font-size: 14px;The Indian Army in World War I: 1914–1918font-size: 14px;, n. 17, pp. 94–158.</li>
                                <li>Gary Sheffield, font-size: 14px;The First World War in 100 Objectsfont-size: 14px;, London: Andre Deutsch, 2013, p. 108.</li>
                                <li>This section is adapted from Cardozo, font-size: 14px;The Indian Army in World War I: 1914–1918font-size: 14px;, n. 17, pp. 94–158. Pre-independence Era 91</li>
                                <li>Lt Gen, VK Singh, PVSM, Singh, ‘The World Wars and Prelude to Independence’, in Maj Gen Ian Cardozo, AVSM, SM (ed.), font-size: 14px;Indian Army: A Brief Historyfont-size: 14px;, New Delhi: USI, 2007, p. 35.</li>
                                <li>This section is adapted from ibid., pp. 33–34.</li>
                                <li>Peter Stanley, ‘Indians, Anzacs and Gallipoli, 1915’, font-size: 14px;Journal of Defence Studiesfont-size: 14px;, Vol. 8, No. 3, July–September 2014, p. 22.</li>
                                <li>font-size: 14px;India and the Great War: Gallipolifont-size: 14px;, Booklet series, New Delhi: USI, p. 4.</li>
                                <li>Stanley, ‘Indians, Anzacs and Gallipoli, 1915’, n. 27, p. 23.</li>
                                <li>Barua, font-size: 14px;The Late Colonial Indian Army: From the Afghan Wars to the Second World Warfont-size: 14px;, n. 14.</li>
                                <li>Ibid. For a more detailed discussion on Sari Bair, see font-size: 14px;India and the Great War: Gallipolifont-size: 14px;, n. 27, pp. 16–19 and Cardozo, font-size: 14px;The Indian Army in World War I: 1914–1918font-size: 14px;, n. 17, pp. 136–46.</li>

                            </ol>
                        </div>
                    </div> -->


                    <!-- <div id="gallery" class="row image-gallery">
                        <div class="col-md-12">
                            <h3>Gallery</h3>
                        </div>

                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="assets/img/gallery-1.png" class="img-fluid" alt="">
                                <p>Line of communication in mountian </p>
                            </div>
                        </div>


                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="assets/img/gallery-2.png" class="img-fluid" alt="">
                                <p>Evacuation of Refugee by IAF</p>
                            </div>
                        </div>


                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="assets/img/gallery-3.png" class="img-fluid" alt="">
                                <p>Construction of Bridge</p>
                            </div>
                        </div>


                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="assets/img/gallery-4.png" class="img-fluid" alt="">
                                <p>Army Engineers working at Jhanger Road </p>
                            </div>
                        </div>


                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="assets/img/gallery-5.png" class="img-fluid" alt="">
                                <p>Army doctor's aid to civil</p>
                            </div>
                        </div>


                        <div class="col-md-4 main">
                            <div class="image-main img-pop">
                                <img src="assets/img/gallery-6.png" class="img-fluid" alt="">
                                <p>An Army Officer helping a Kashmiri Boy in schooling at Rajaur</p>
                            </div>
                        </div>

                    </div> -->

                    <!-- <div id="video" class="row image-gallery">

                        <div class="col-md-12">
                            <h3>Video</h3>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="featured-video" >
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <div class="card-body">
                                    <p><i class="fa fa-calendar">font-size: 14px; November 5,2022 <i class="fa fa-message">font-size: 14px; 255</p>
                                    <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                    <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="featured-video" >
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <div class="card-body">
                                    <p><i class="fa fa-calendar">font-size: 14px; November 5,2022 <i class="fa fa-message">font-size: 14px; 255</p>
                                    <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                    <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="featured-video" >
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/4yupEjw7Yys" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <div class="card-body">
                                    <p><i class="fa fa-calendar">font-size: 14px; November 5,2022 <i class="fa fa-message">font-size: 14px; 255</p>
                                    <h5 class="card-title">Lorem Ipsum is simply <br> dummy </h5>
                                    <p class="detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                            </div>
                        </div>


                    </div> -->

                </div>

            </div>
</section>


<?php include('footer.php'); ?>