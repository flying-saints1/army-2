<?php
/*
*
* Template Name: Organization template
*
*/
$main ="indian-army";

$page="organization";




get_header(); ?>

<?php 

// get banner from template

get_template_part('template-parts/banner-section');

?>
<?php if(have_rows('organization_chart')): ?>
    <?php while(have_rows('organization_chart')): the_row(); ?>
        <section class="structure-img">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-img">
                            <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid"  alt="" loading="lazy">
                        </div>
                    </div>
                </div>

                <div class="row link-border">
                    <div class="col-md-12">
                        <a href="<?php echo get_sub_field('link'); ?>" target="_blank"><?php echo get_sub_field('link'); ?></a>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>