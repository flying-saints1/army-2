<?php

$main ="brave";

$page ="award";

include('header.php') ?>

<section class="iconic-leaders-detail-banner"
        style="background-image: url(assets/img/blog-banner.jpg);">
        <div class="container">
            <h1 class="banner-content">Gallantry Awards </h1>
        </div>
</section>

<section class="gallantry">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="gallantry-img">
                    <img src="assets/img/gallantry.jpg" height="400px" width="480px" alt="" loading="lazy">
                </div>
            </div>

            <div class="col-md-6">
                <div class="about-award">
                    <h2 class="head">About Gallantry Awards</h2>
                    <p class="para">Gallantry Awards are classified into two Categories</p>

                    <ul>
                        <li class="para"><strong>Gallantry in the Face of Enemy</strong> <br>Post-independence, the first three gallantry awards namely<strong> Param<br> Vir Chakra, Maha Vir Chakra,</strong> and <strong>Vir Chakra </strong>were instituted by <br>the Government of India on 26th January 1950 which were deemed to have effect from the 15th August 1947
                        </li>
                        <li class="para"><strong>Gallantry Other than in the Face of Enemy <br> Ashoka Chakra Class-I, Ashoka Chakra Class-II, and Ashoka <br>Chakra Class-III </strong>were instituted by the GOI on 4th January 1952,<br> which were deemed to have effect from the 15th August 1947. These awards were renamed <strong> Ashoka Chakra, Kirti Chakra,</strong> and <strong>Shaurya<br>  Chakra </strong>respectively in January 1967
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>



<section id="award">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading">Gallantry Awards </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="image-main ">
                    <img src="assets/img/param-vir-chakra.jpg" class="card-img-top" alt="...">
                    <div class=" card-body img-body">
                        <p class="para">Param Vir Chakra </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="image-main ">
                    <img src="assets/img/maha-vir-chakra.jpg" class="card-img-top" alt="...">
                    <div class=" card-body img-body">
                        <p class="para">Maha Vir Chakra </p>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="image-main ">
                    <img src="assets/img/vir-chakra.jpg" class="card-img-top" alt="...">
                    <div class=" card-body img-body">
                        <p class="para">Vir Chakra </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="image-main ">
                    <img src="assets/img/ashoka-chakra.jpg" class="card-img-top" alt="...">
                    <div class=" card-body img-body">
                        <p class="para">Ashoka Chakra</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="image-main ">
                    <img src="assets/img/kirti-chakra.jpg" class="card-img-top" alt="...">
                    <div class=" card-body img-body">
                        <p class="para">Kirti Chakra </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="image-main ">
                    <img src="assets/img/shaurya-chakra.jpg" class="card-img-top" alt="...">
                    <div class=" card-body img-body">
                        <p class="para">Shaurya Chakra</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- param-vir chakra seciton start here  -->


<section class="paramvir-chakra">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about-paramvir">
                    <h5>About</h5>
                    <ul>
                        <li><p> Savitri Khanolkar (Eve Yvonne Maday de Maros) was
                            asked by the Adjutant General Major General Hira Lal
                            Atal to design India’s highest award for bravery in
                            combat, the Param Vir Chakra, which was to replace
                            the Victoria Cross</p>
                        </li>
                        <li><p>Along with the PVC, Savitribai Khanolkar also
                            designed other bravery medals like - The Mahavir
                            Chakra, The Vir Chakra, the Ashok Chakra, Kirti
                            Chakra, and Shaurya Chakra </p>
                        </li>
                        <li><p>Each medal is designed and crafted with the emblems
                            of Virta and Shaurya in mind</p>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-6 ">
                <div class="param-img">
                    <img src="assets/img/param.jpg" height="400px" width="480px" alt="" loading="lazy">
                </div>
            </div>
        </div>

        <div class="row reverse">
            <div class="col-md-6">
                <div class="param-vir-chakra">
                    <h5>Param Vir Chakra</h5>
                    <ul>
                        <li><p> Highest Gallantry Award in the country Made of bronze and is in the form of a circle with a diameter of 1.4 inches
                            </p>
                        </li>
                        <li><p>Major Somnath Sharma was the first army
                            personnel to receive the Param Vir Chakra in
                            1947 posthumously (after death) for his act of
                            bravery in the Kashmir battle in November 1947
                            </p>
                        </li>
                       
                    </ul>
                </div>
            </div>

            <div class="col-md-6 ">
                <div class="param-chakra-img">
                    <img src="assets/img/param-vir-chakra.jpg" height="400px" width="480px" alt="" loading="lazy">
                </div>
            </div>
        </div>

        <div class="row eligible">
            <div class="col-md-12">
                <div class="personnel">
                    <p class="para">Personnel Eligible: The following categories of personnel shall be eligible for the Chakra</p>

                    <ul>
                        <li>Officers, men and women of all ranks of the Army, the Navy and the Air Force, of any of the Reserve Forces, of the Territorial Army, Militia, and of any other lawfully constituted Armed Forces
                        </li>
                        <li> Matrons, Sisters, Nurses, and the staff of the Nursing Services and other Services pertaining to Hospitals and Nursing and Civilians of either sex serving regularly or temporarily under the orders, directions, or supervision of any of the above-mentioned Forces</li>
                    </ul>

                    <p><strong>Conditions of Eligibility: </strong>The Chakra is awarded for most conspicuous bravery or some daring or pre-eminent act of valour or self-sacrifice, in the presence of the enemy, whether on land, at sea, or in the air. The decoration may be awarded posthumously
                    </p>
                </div>
                <div class="row img-row">
                    <div class="col-md-3">
                        <img src="assets/img/param-vir-1.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3">
                        <img src="assets/img/param-vir-2.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3">
                        <img src="assets/img/param-vir-3.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>


<!-- maha-vir chakra seciton start here  -->


<section class="paramvir-chakra" id="maha-vir">
    <div class="container">

        <div class="row reverse">
            <div class="col-md-6">
                <div class="param-vir-chakra">
                    <h5>Maha Vir Chakra</h5>
                    <ul>
                        <li><p>Second-Highest Gallantry Award in the country
                            </p>
                        </li>
                        <li><p>It is made from standard silver and is circular inshape with a diameter of 1.4 inches
                            </p>
                        </li>

                        <li><p>The medal is provided with a ribbon, which is half-white and half-orange

                            </p>
                        </li>

                        <li><p>Naik Raju was the first army personnel to receive the Maha Vir Chakra in 1948 during Jammu & Kashmir Operation
                            </p>
                        </li>
                       
                    </ul>
                </div>
            </div>

            <div class="col-md-6 ">
                <div class="param-chakra-img">
                    <img src="assets/img/maha-vir-chakra.jpg" height="400px" width="480px" alt="" loading="lazy">
                </div>
            </div>
        </div>

        <div class="row eligible">
            <div class="col-md-12">
                <div class="personnel">
                    <p class="para">Personnel Eligible: The following categories of personnel shall be eligible for the Chakra</p>

                    <ul>
                        <li>Officers, men and women of all ranks of the Army, the Navy and the Air Force, of any of the Reserve Forces, of the Territorial Army, Militia, and of any other lawfully constituted Armed Forces
                        </li>
                        <li> Matrons, Sisters, Nurses, and the staff of the Nursing Services and other Services pertaining to Hospitals and Nursing and Civilians of either sex serving regularly or temporarily under the orders, directions, or supervision of any of the above-mentioned Forces</li>
                    </ul>

                    <p><strong>Conditions of Eligibility: </strong> The medal is awarded for gallantry in the presence of the enemy on land, at sea or in the air. The decoration may be awarded posthumously

                    </p>
                </div>
                <div class="row img-row">
                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/maha-vir-1.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/maha-vir-2.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/maha-vir-3.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/maha-vir-4.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/maha-vir-5.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>


<!-- vir chakra seciton start here  -->




<section class="paramvir-chakra" id="maha-vir">
    <div class="container">

        <div class="row reverse">
            <div class="col-md-6">
                <div class="param-vir-chakra">
                    <h5>Vir Chakra</h5>
                    <ul>
                        <li><p>Third-highest Gallantry Award
                            </p>
                        </li>
                        <li><p>It also has a circular shape with a diameter of 1.4 inches and is made of standard silver
                            </p>
                        </li>

                        <li><p>The medal comes with a ribbon, which is blue and half-orange in colo

                            </p>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-6 ">
                <div class="param-chakra-img">
                    <img src="assets/img/vir-chakra.jpg" height="400px" width="480px" alt="" loading="lazy">
                </div>
            </div>
        </div>

        <div class="row eligible">
            <div class="col-md-12">
                <div class="personnel">
                    <p class="para">Personnel Eligible: The following categories of personnel shall be eligible for the Chakra</p>

                    <ul>
                        <li>Officers, men and women of all ranks of the Army, the Navy and the Air Force, of any of the Reserve Forces, of the Territorial Army, Militia, and of any other lawfully constituted Armed Forces
                        </li>
                        <li> Matrons, Sisters, Nurses, and the staff of the Nursing Services and other Services pertaining to Hospitals and Nursing and Civilians of either sex serving regularly or temporarily under the orders, directions, or supervision of any of the above-mentioned Forces</li>
                    </ul>

                    <p><strong>Conditions of Eligibility: </strong> The medal is awarded for gallantry in the presence of the enemy on land, at sea or in the air. The decoration may be awarded posthumously

                    </p>
                </div>
                <div class="row img-row">
                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/vir-chakra-1.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/vir-chakra-2.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/vir-chakra-3.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/vir-chakra-4.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                </div>
            </div>
        </div>


    </div>
</section>


<!-- ashoka-vir chakra seciton start here  -->

<section class="paramvir-chakra" id="maha-vir">
    <div class="container">

        <div class="row reverse">
            <div class="col-md-6">
                <div class="param-vir-chakra">
                    <h5>Ashoka Chakra</h5>
                    <ul>
                        <li><p>The highest peacetime Gallantry Award in the country and can also be awarded to a civilian</p>
                        </li>
                        <li><p>It has a dark-green silk riband of width 3.2 cm with an orange vertical line</p>
                        </li>

                        <li><p>First-people to receive this honor were Havildar Bachittar Singh and Naik Narbahadur Thapa </p>
                        </li>

                        <li><p>Flt. Lt. Suhas Biswas in 1952 was first person to receive this honor from the Indian Air Force</p>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-6 ">
                <div class="param-chakra-img">
                    <img src="assets/img/ashoka-chakra.jpg" height="400px" width="480px" alt="" loading="lazy">
                </div>
            </div>
        </div>

        <div class="row eligible">
            <div class="col-md-12">
                <div class="personnel">
                    <p class="para">Personnel Eligible: The following categories of personnel shall be eligible for the Chakra</p>

                    <ul>
                        <li>Officers, men and women of all ranks of the Army, the Navy and the Air Force, of any of the Reserve Forces, of the Territorial Army, Militia, and of any other lawfully constituted Armed Forces
                        </li>
                        
                        <li> 
                            Members of the Nursing Services of the Armed Forces.
                        </li>
                        
                        <li> Matrons, Sisters, Nurses, and the staff of the Nursing Services and other Services pertaining to Hospitals and Nursing and Civilians of either sex serving regularly or temporarily under the orders, directions, or supervision of any of the above-mentioned Forces</li>
                    </ul>

                    <p><strong>Conditions of Eligibility: </strong> The medal is awarded for gallantry in the presence of the enemy on land, at sea or in the air. The decoration may be awarded posthumously

                    </p>
                </div>
                <div class="row img-row">
                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/ashoka-1.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/ashoka-3.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/ashoka-4.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/ashoka-2.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                </div>
            </div>
        </div>


    </div>
</section>


<!-- kriti-vir chakra seciton start here  -->

<section class="paramvir-chakra" id="maha-vir">
    <div class="container">

        <div class="row reverse">
            <div class="col-md-6">
                <div class="param-vir-chakra">
                    <h5>Kirti Chakra</h5>
                    <ul>
                        <li><p>Second-highest peacetime Gallantry Award. Its shape is like a circle whose diameter is 1.38 inches, made of silver, and has rims on both sides.
                            </p>
                        </li>
                        <li><p>The medal is provided with a green riband that measures 3.2 cm in width with two orange vertical lines dividing the riband into three equal parts </p>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="col-md-6 ">
                <div class="param-chakra-img">
                    <img src="assets/img/kirti-chakra.jpg" height="400px" width="480px" alt="" loading="lazy">
                </div>
            </div>
        </div>

        <div class="row eligible">
            <div class="col-md-12">
                <div class="personnel">
                    <p class="para">Personnel Eligible: The following categories of personnel shall be eligible for the Chakra</p>

                    <ul>
                        <li>Officers, men and women of all ranks of the Army, the Navy and the Air Force, of any of the Reserve Forces, of the Territorial Army, Militia, and of any other lawfully constituted Armed Forces
                        </li>
                        <li> Matrons, Sisters, Nurses, and the staff of the Nursing Services and other Services pertaining to Hospitals and Nursing and Civilians of either sex serving regularly or temporarily under the orders, directions, or supervision of any of the above-mentioned Forces</li>
                    </ul>

                    <p><strong>Conditions of Eligibility: </strong> The medal is awarded for gallantry in the presence of the enemy on land, at sea or in the air. The decoration may be awarded posthumously

                    </p>
                </div>
                <div class="row img-row">
                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/kirti-1.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/kirti-2.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/kirti-3.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/kirti-4.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                </div>
            </div>
        </div>


    </div>
</section>


<!-- shaurya-vir chakra seciton start here  -->


<section class="paramvir-chakra" id="maha-vir">
    <div class="container">

        <div class="row reverse">
            <div class="col-md-6">
                <div class="param-vir-chakra">
                    <h5>Shaurya Chakra</h5>
                    <ul>
                        <li>
                            <p>
                                Third-highest peacetime Gallantry Award.
                            </p>
                        </li>
                        <li>
                            <p>
                                It is also circular in shape with a diameter of 1.4 cm made of bronze with a front side similar to Ashoka Chakra
                            </p>
                        </li>

                        <li>
                            <p>
                                It has a 3.2 cm wide riband that is divided into four equal parts by three orange vertical lines
                            </p>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-6 ">
                <div class="param-chakra-img">
                    <img src="assets/img/shaurya-chakra.jpg" height="400px" width="480px" alt="" loading="lazy">
                </div>
            </div>
        </div>

        <div class="row eligible">
            <div class="col-md-12">
                <div class="personnel">
                    <p class="para">Personnel Eligible: The following categories of personnel shall be eligible for the Chakra</p>

                    <ul>
                        <li>
                            Officers, men and women of all ranks of the Army, the Navy and the Air Force, of any of the Reserve Forces, of the Territorial Army, Militia, and of any other lawfully constituted Armed Forces
                        </li>

                        <li> 
                            Members of the Nursing Services of the Armed Forces.
                        </li>

                        <li> Matrons, Sisters, Nurses, and the staff of the Nursing Services and other Services pertaining to Hospitals and Nursing and Civilians of either sex serving regularly or temporarily under the orders, directions, or supervision of any of the above-mentioned Forces
                        </li>
                    </ul>

                    <p><strong>Conditions of Eligibility: </strong> The medal is awarded for gallantry in the presence of the enemy on land, at sea or in the air. The decoration may be awarded posthumously

                    </p>
                </div>
                <div class="row img-row">
                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/shaurya-1.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/shaurya-2.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/shaurya-3.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                    <div class="col-md-3 mb-4 mt2">
                        <img src="assets/img/shaurya-4.jpg" class="img-fluid" alt="" loading="lazy">
                    </div>

                </div>
            </div>
        </div>


    </div>
</section>

<?php include('footer.php') ?>