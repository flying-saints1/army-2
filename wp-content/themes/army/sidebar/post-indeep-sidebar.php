<div class="col-md-3">
    <div class="accordion">
        <h6 class="accordion-toggle <?php echo ($page == "pre" ? "active" : "")?>">Pre- independent Wars</h6>
            <div class="accordion-content <?php echo ($page == "pre" ? "active" : "")?>" >
            
                <h6 class="accordion-toggle accordion-sub-tab <?php echo ($subPage == "war-1" ? "active" : "")?>">World War I</h6>
                <div class="accordion-content <?php echo ($subPage == "war-1" ? "active" : "")?>" >
            
                    <ul>
                        <a href="world-war-I.php#about" ><li class="sub-class">About War</li></a>
                        <a href="world-war-I.php#france" ><li  class="sub-class">Northern Europe: France and Flanders</li></a>
                        <a href="world-war-I.php#meso" ><li  class="sub-class">Mesopotamia</li></a>
                        <a href="world-war-I.php#africa" ><li  class="sub-class">East Africa </li></a>
                        <a href="world-war-I.php#palestine" ><li  class="sub-class">Egypt and Palestine</li></a>
                        <a href="world-war-I.php#gallipoli" ><li  class="sub-class">Gallipoli</li></a>
                        

                    </ul>

                </div>
        
                <h6 class="accordion-toggle accordion-sub-tab <?php echo ($subPage == "war-2" ? "active" : "")?>">World War II</h6>
                <div class="accordion-content <?php echo ($subPage == "war-2" ? "active" : "")?>" >
                    <ul>
                        <a href="world-war-II.php#causes" ><li class="sub-class">Causes of World War II</li></a>
                        <a href="world-war-II.php#theatre" ><li  class="sub-class">Indian Army World War II Theatres</li></a>
                        

                    </ul>
                </div>

                <!-- <h6 class="accordion-toggle accordion-sub-tab <?php // echo ($subPage == "small" ? "active" : "")?>">Small War</h6>
                <div class="accordion-content <?php // echo ($subPage == "small" ? "active" : "")?>" > 
                    <ul>
                        <a href="small-wars.php#about-war"  ><li  class="sub-class">About War</li></a>
                        <a href="small-wars.php#gallery"  ><li  class="sub-class">Gallery</li></a>
                        <a href="small-wars.php#video"><li  class="sub-class">Video</li></a>

                       
                    </ul>
                </div> -->
               
            </div>
               
        

            <h6 class="accordion-toggle <?php echo ($page == "post-indeep" ? "active" : "")?>">Post- independent Wars</h6>
            <div class="accordion-content <?php echo ($page == "post-indeep" ? "active" : "")?>" >

                <h6 class="accordion-toggle accordion-sub-tab <?php echo ($subPage == "kasmir" ? "active" : "")?>">1947-48 Kashmir Conflict</h6>
                <div class="accordion-content <?php echo ($subPage == "kasmir" ? "active" : "")?>" >
                    <ul>
                        <a href="1947-48-kashmir-conflict.php#about-war"  ><li  class="sub-class">About War</li></a>
                        <a href="1947-48-kashmir-conflict.php#event"  ><li  class="sub-class">Sequence of Events</li></a>
                        
                    </ul>
                </div>

                <h6 class="accordion-toggle accordion-sub-tab <?php echo ($subPage == "sino-war" ? "active" : "")?> ">1962 Sino-Indian Conflict </h6>
                <div class="accordion-content <?php echo ($subPage == "sino-war" ? "active" : "")?>" >
                    <ul>
                        <a href="1962-sino-indian-war.php#about-war"  ><li  class="sub-class">About War</li></a>
                        <a href="1962-sino-indian-war.php#chinese"  ><li  class="sub-class">Chinese War Aim and Grand Strategy</li></a>
                        <a href="1962-sino-indian-war.php#eastern"  ><li  class="sub-class">Eastern Sector: Kameng Frontier Division</li></a>
                        <a href="1962-sino-indian-war.php#conclusion"  ><li  class="sub-class">Conclusion</li></a>

                    </ul>
                </div>

                <h6 class="accordion-toggle accordion-sub-tab <?php echo ($subPage == "1965" ? "active" : "")?>">1965 India–Pakistan War</h6>
                <div class="accordion-content <?php echo ($subPage == "1965" ? "active" : "")?>" >
            
                    <ul>
                        <a href="1965-india–pakistan-war.php#about"  ><li  class="sub-class">About War</li></a>
                        <a href="1965-india–pakistan-war.php#rann"  ><li  class="sub-class">The Rann of Kutch Incident: The Smokescreen</li></a>
                       
                    </ul>

                </div>

                <h6 class="accordion-toggle accordion-sub-tab <?php echo ($subPage == "1971" ? "active" : "")?>">1971 India–Pakistan War</h6>
                <div class="accordion-content <?php echo ($subPage == "1971" ? "active" : "")?>" >
                    <ul>
                        <a href="1971-india–pakistan-war.php#about"  ><li  class="sub-class">About War</li></a>
                        <a href="1971-india–pakistan-war.php#eastern"  ><li  class="sub-class">Eastern Front</li></a>
                        <a href="1971-india–pakistan-war.php#western"  ><li  class="sub-class">Western Front</li></a>
                        <a href="1971-india–pakistan-war.php#additional"  ><li  class="sub-class">Additional Sources</li></a>

                        
                    </ul>
                </div>

            </div>

            
            
    </div> 
</div> 