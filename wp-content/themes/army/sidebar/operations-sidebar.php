<div class="col-md-3">
    <div class="accordion">
        <h6 class="accordion-toggle <?php echo ($page == "polo" ? "active" : "") ?>">Operation Polo 1948</h6>
        <div class="accordion-content <?php echo ($page == "polo" ? "active" : "") ?>">
            <ul>
                <a href="operation-polo-1948.php#back">
                    <li class="active">Background</li>
                </a>
                <a href="operation-polo-1948.php#ground">
                    <li class="">Ground Operations</li>
                </a>
                <a href="operation-polo-1948.php#formal">
                    <li class="">Formal Accession</li>
                </a>
            </ul>
        </div>

        <h6 class="accordion-toggle <?php echo ($page == "vijay" ? "active" : "") ?>">Operation Vijay 1961</h6>
        <div class="accordion-content <?php echo ($page == "vijay" ? "active" : "") ?>">
            <ul>
                <a href="operation-vijay-1961.php#back">
                    <li class="active">Background</li>
                </a>
                <a href="operation-vijay-1961.php#daman">
                    <li class="">Daman and Diu</li>
                </a>
                <a href="operation-vijay-1961.php#goa">
                    <li class="">Goa</li>
                </a>
                <a href="operation-vijay-1961.php#action">
                    <li class="">In Action</li>
                </a>
            </ul>
        </div>

        <h6 class="accordion-toggle <?php echo ($page == "sikkim" ? "active" : "") ?>">Sikkim: Skirmish at Nathu <br> La 1967</h6>
        <div class="accordion-content <?php echo ($page == "sikkim" ? "active" : "") ?>">

            <ul>
                <a href="sikkim-skirmish-at-nathu-la-1967.php#back">
                    <li class="active">Background</li>
                </a>
                <a href="sikkim-skirmish-at-nathu-la-1967.php#lead">
                    <li class="">Lead-up to the Skirmish</li>
                </a>
                <a href="sikkim-skirmish-at-nathu-la-1967.php#sept">
                    <li class="">Events of 11 September 1967</li>
                </a>
                <a href="sikkim-skirmish-at-nathu-la-1967.php#assessment">
                    <li class="">Assessment</li>
                </a>
                <a href="sikkim-skirmish-at-nathu-la-1967.php#incident">
                    <li class="">Cho La Incident</li>
                </a>
            </ul>
        </div>

        <h6 class="accordion-toggle <?php echo ($page == "siachen" ? "active" : "") ?>">Siachen: Operation Meghdoot 1984</h6>
        <div class="accordion-content <?php echo ($page == "siachen" ? "active" : "") ?>">
            <ul>
                <a href="siachen-operation-meghdoot-1984.php#back">
                    <li class="active">Background</li>
                </a>
                <a href="siachen-operation-meghdoot-1984.php#comm">
                    <li class="">Commencement of the Operation</li>
                </a>
                <a href="siachen-operation-meghdoot-1984.php#battle">
                    <li class="">Battle for Bana Post</li>
                </a>
                <a href="siachen-operation-meghdoot-1984.php#glacier">
                    <li class="">Chumik Glacier</li>
                </a>
            </ul>
        </div>

        <h6 class="accordion-toggle <?php echo ($page == "pawan" ? "active" : "") ?>">Operation Pawan: Indian Peace Keeping Force, 1987</h6>
        <div class="accordion-content <?php echo ($page == "pawan" ? "active" : "") ?>">
            <ul>
                <a href="operation-pawan-1987.php#background">
                    <li class="active">Background</li>
                </a>
                <a href="operation-pawan-1987.php#air">
                    <li>Air Operations </li>
                </a>
                <a href="operation-pawan-1987.php#sri">
                    <li>Sri Lankan Politics and IPKF Withdrawal</li>
                </a>
                <a href="operation-pawan-1987.php#assessment">
                    <li>Assessment</li>
                </a>

            </ul>
        </div>


        <h6 class="accordion-toggle <?php echo ($page == "maldives" ? "active" : "") ?>">Maldives: Operation Cactus<br> 1998</h6>
        <div class="accordion-content <?php echo ($page == "maldives" ? "active" : "") ?>">
            <ul>
                <a href="maldives-operation-cactus-1998.php#maldives">
                    <li class="active">About Maldives</li>
                </a>
            </ul>
        </div>



        <h6 class="accordion-toggle <?php echo ($page == "kargil" ? "active" : "") ?>">Operation Vijay: Kargil 1999 </h6>
        <div class="accordion-content <?php echo ($page == "kargil" ? "active" : "") ?>">
            <ul>
                <a href="operation-vijay-kargil-1999.php#back">
                    <li class="active">Background</li>
                </a>
                <a href="operation-vijay-kargil-1999.php#cris">
                    <li>Build-up to the Crisis</li>
                </a>
                <a href="operation-vijay-kargil-1999.php#res">
                    <li>Intrusions and Response</li>
                </a>
                <a href="operation-vijay-kargil-1999.php#vij">
                    <li>Operation Vijay</li>
                </a>
                <a href="operation-vijay-kargil-1999.php#conflict">
                    <li>Assessment of the Kargil Conflict</li>
                </a>
            </ul>
        </div>





        <h6 class="accordion-toggle <?php  echo ($page == "lico" ? "active" : "")
                                    ?>">Low-Intensity Conflicts and Counter-terrorism</h6>
        <div class="accordion-content <?php echo ($page == "lico" ? "active" : "")
                                        ?>">
            <ul>
                <a href="lico-and-cI.php#perspective">
                    <li class="active">A Historical Perspective</li>
                </a>
                <a href="lico-and-cI.php#employment">
                    <li>Post-independence Employment </li>
                </a>
                <a href="lico-and-cI.php#lwe">
                    <li>Left-wing Extremism (LWE)</li>
                </a>
                <a href="lico-and-cI.php#terrorism">
                    <li>Analysis of Insurgency and Terrorism in India</li>
                </a>

                <a href="lico-and-cI.php#evolution">
                    <li>Doctrinal Evolution</li>
                </a>

                <a href="lico-and-cI.php#conclusion">
                    <li>Conclusion</li>
                </a>


            </ul>
        </div>



        <h6 class="accordion-toggle <?php echo ($page == "peace" ? "active" : "") ?>">Peace Keeping Ops</h6>
        <div class="accordion-content <?php echo ($page == "peace" ? "active" : "") ?>">
            <ul>
                <a href="peace-keeping-ops.php#india-and-un">
                    <li class="active">India and UN Peacekeeping</li>
                </a>
                <a href="peace-keeping-ops.php#peacekeeping">
                    <li>Peacekeeping Missions: Past and Current </li>
                </a>

            </ul>
        </div>


        <h6 class="accordion-toggle <?php echo ($page == "human" ? "active" : "") ?>">Humanitarian and Disaster Relief (HADR)</h6>
        <div class="accordion-content <?php echo ($page == "human" ? "active" : "") ?>">
            <ul>
                <a href="humanitarian-and-disaster-relief.php#op">
                    <li class="active">About Operation</li>
                </a>
                <a href="humanitarian-and-disaster-relief.php#tsunami">
                    <li>2004 Indian Ocean Tsunami</li>
                </a>
                <a href="humanitarian-and-disaster-relief.php#eq">
                    <li>Kashmir Earthquake 2005</li>
                </a>
                <a href="humanitarian-and-disaster-relief.php#kerala">
                    <li>Disease and Floods in Kerala</li>
                </a>
                <a href="humanitarian-and-disaster-relief.php#nepal">
                    <li>Nepal Earthquake 2015</li>
                </a>

            </ul>
        </div>

    </div>
</div>