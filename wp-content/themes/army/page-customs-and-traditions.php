<?php 

/*
*
* Template Name: custom tradition template
*
*/

get_header(); ?>


<?php 

get_template_part('template-parts/banner-section');

?>

<?php if(have_rows('customs_and_traditions')): ?>
    <section class="regiments-and-corps-detail">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9  col-sm-12">
                    <?php while(have_rows('customs_and_traditions')):  the_row(); ?>
                        <h2 class="heading-border"><?php echo get_sub_field('heading')?></h2>
                        <div><?php echo get_sub_field('content')?></div>
                        
                        
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>



<?php get_footer(); ?>