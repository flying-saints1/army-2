

$(document).ready(function () {
  $(".partner-slide").slick({
    loop: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    speed: 700,
    autoplay: true,
    autoplaySpeed: 5000,
    dots: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,

        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });



  $(".home-banner").slick({
    loop: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 700,
    autoplay: true,
    autoplaySpeed: 10000,
    dots: true,
    // arrow:false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,

        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });





});






// image pop up modal

// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var modalImg = document.getElementById("img01");
// var captionText = document.getElementById("caption");
$('.img-pop').click(function (e) {
  console.log('abcd');
  $('#myModal').css({'display': 'block'});
  var imgSrc = $(this).attr('big_src');
console.log(imgSrc);
  modalImg.src = imgSrc;
});


// Get the <span> element that closes the modal
var span = document.getElementById("close-modal");

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  modal.style.display = "none";
}



// $('.img-popup').click(function (e) {
//   console.log('efgh');

//   var imgSrc = $(this).attr('src');
//   modal.style.display = "block";
//   modalImg.src = imgSrc;
// });







$(document).ready(function () {
  $("#myinput").on("keyup", function () {
    var value = $(this).val().toLowerCase();
  $(".image-main").filter(function () {
    // var amit =  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
    console.log(value);    
    $(this).closest('.main').toggle($(this).text().toLowerCase().indexOf(value) > -1)
  });
});

  

  // Product FAQ section

  $('.accordion').find('.accordion-toggle').click(function () {
    $(this).next().slideToggle('600');
    $(this).siblings(".accordion-content").not($(this).next()).slideUp('600');
  });
  $('.accordion-toggle').on('click', function () {
    $(this).toggleClass('active').siblings().removeClass('active');
  })


  // $(document).bind("contextmenu",function(e){
  //   return false;
  // });

  // $(document).keydown(function (event) {
  //   if (event.keyCode == 123) { // Prevent F12
  //       return false;
  //   } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
  //       return false;
  //   }
  // });
  // function sidebar(){

  //   const div = document.createElement('div');
  //   let warDetails = document.getElementsByClassName('war-details');

  //   // let title = warDetails[0].getElementsByTagName('h4')[0].innerText;
  //   let accordionContent1962 = document.getElementById('accordion-content-1962');
  //   let ul = document.createElement('ul');
  //   let li  
  //   for(const warDetail in warDetails ){
  //     let title = warDetails[warDetail].getElementsByTagName('h4')[0].innerText;
  //     li = document.createElement('li');

  //     // accordionContent1962.innerHTML= `
  //     // <a href="#" class=""><li class="">${title}</li></a>
  //     // `
  //     li.appendChild(`
  //     <a href="#" class=""><li class="">${title}</li></a>
  //     `);
  //     // console.log(warDetails[warDetail].getElementsByTagName('h4')[0].innerText);

  //   }

  //   console.log(li);

  // }

  // sidebar();


  function updateElements(elements) {
    // loop through all elements
    for (var i = 0; i < elements.length; i++) {
        const currentNode = elements[i].addedNodes;

        for (var j = 0; j < currentNode.length; j++) {
            if (currentNode[j].nodeName.toLowerCase() == "iframe") {
                const myLink = currentNode[j].src;

                // create local HTML code with Youtube link - replace ___data___ with data - dev.to's markdown parser won't let me use it :)
                const localHtml = '<html><body style="background:rgb(200,200,200)"><a href="' + myLink + '" style="font-size:14px;text-align:center;position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);"><img src="___data___:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzMuOTJtbSIgaGVpZ2h0PSIyMy42Mm1tIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAzMy45MiAyMy42MiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KIDxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC04OC4wNCAtMTM2LjcpIj4KICA8cGF0aCB0cmFuc2Zvcm09InNjYWxlKC4yNjQ2KSIgZD0ibTM0MS43IDUxNi42Yy00Ljk0NiAwLTguOTI4IDMuOTgxLTguOTI4IDguOTI4djcxLjQzYzAgNC45NDYgMy45ODEgOC45MjggOC45MjggOC45MjhoMTEwLjRjNC45NDYgMCA4LjkyOC0zLjk4MSA4LjkyOC04LjkyOHYtNzEuNDNjMC00Ljk0Ni0zLjk4MS04LjkyOC04LjkyOC04LjkyOHptNDcuMzIgMjkuNTYgMjYuNTIgMTUuMDktMjYuNTIgMTUuMDl6IiBmaWxsPSIjZjAwIiBzdG9wLWNvbG9yPSIjMDAwMDAwIiBzdHJva2Utd2lkdGg9IjMuMDM0cHgiIHN0eWxlPSJwYWludC1vcmRlcjpzdHJva2UgZmlsbCBtYXJrZXJzIi8+CiA8L2c+Cjwvc3ZnPgo=" width="80" height="55"/><br/>play video</a></body></html>';

                currentNode[j].setAttribute("data-src", myLink);

                // set local HTML
                // replace ___data___ with data - dev.to's markdown parser won't let me use it :)
                currentNode[j].src = "___data___:text/html;charset=utf-8," + localHtml;
            }
        }
    }
    // remove listeners at the end
    removeEventListener(document, updateElements);
}

function removeEvents(obj, callback) {
    if (window.__obs) {
        window.__obs.disconnect();
    }
}

function registerEvents(obj, callback) {
    const MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    if (MutationObserver) {
        const obs = new MutationObserver(function(mutations, observer) {
            callback(mutations)
        });
        obs.observe(obj, {
            childList: true,
            subtree: true
        });
        window.__obs = obs;
    }
}

// register events
registerEvents(document, updateElements);
});

$(document).ready(function() {
  $("img").on("contextmenu",function(){
     return false;
  }); 
  $("iframe").on("contextmenu",function(){
    return false;
 }); 

  
}); 
// $(document).bind("contextmenu", function (e) {
//   e.preventDefault();
//   // alert("Right Click is Disabled");
// });

// $(document).keydown(function (event) {
//   if (event.keyCode == 123) { // Prevent F12
//       return false;
//   } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
//       return false;
//   }
// });


// contact form start

$(".form-submit").click(function (e) {
  // console.log('ji')
  // e.preventDefault();
  var formm = $(this).closest("form");
  var formmclass = formm.attr("jscall");
  var fullname = formm.find("input[name = fullname]").val();
  var email = formm.find("input[name = email]").val();
  var subject = formm.find("input[name = subject]").val();
  var message = formm.find("textarea[name = message]").val();

  // console.log(fullname, email, subject, message);

  if (fullname == "" ) {
    // console.log("all requireds");
    return
  }

  let nameError, subjectError, subjectErrorMsg, emailError, emailErrorMsg;
  let err = {
    nameErr: false,
    emailErr: false,
    subjectErr: false,
    emailErrMsg: false,
  };

  let checkIfPut = () => {
    if (fullname == "") {
      err.nameErr = true;
    }
    if (email == "") {
      err.emailErr = true;
    }
    if (subject == "") {
      err.subjectErr = true;
    }
    return err.nameErr, err.emailErr, err.subjectErr;
  };

  let checkEmail = () => {
    testmail =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
      );
    if (!testmail) {
      err.emailErrMsg = true;
    }
  };

  // let checkPhone = () => {
  //   if (mobile == "") {
  //     subjectError = true;
  //   } else {
  //     testPhone = /^[0-9]{10}$/.test(mobile);
  //     if (!testPhone) {
  //       subjectErrorMsg = true;
  //     }
  //   }
  // };

  checkIfPut();
  // checkPhone();
  checkEmail();


  if (err.nameErr == true && err.emailErr == true && err.subjectErr == true) {
    // console.log($(`.${formmclass} .error_message`));
    $(`#${formmclass} .error_message`).html("All fields are required");
  } else if (err.nameErr == true) {
    $(`#${formmclass} .error_message`).html("FullName is Required");
  } else if (err.emailErr == true) {
    $(`#${formmclass} .error_message`).html("Email is Required");
  } else if (err.emailErrMsg == true) {
    $(`#${formmclass} .error_message`).html("Enter Valid Email Address");
  } else if (err.subjectErr == true) {
    $(`#${formmclass} .error_message`).html("Subject no is Required");
  } else {
    $(`#${formmclass} .error_message`).html("");


    $.ajax({
      url: "mailForm.php",
      method: "POST",
      data: {
        fullname: fullname,
        email: email,
        subject: subject,
        message: message,
      },

      success: function (data) {
        console.log(data);
        if (data == true) {
          $("form").trigger("reset");
          // window.location = "index.html";
          alert("Successfully Submited!");
        } else {
          alert("faliled")
          $("#" + formmclass + " .error_message").html(
            "*There is some problem."
          );
        }
        // $("form").trigger("reset");
        // window.location = "index.html";
        // alert("Success! Our Team will get back to you shortly.");
      },
    });
  }


});